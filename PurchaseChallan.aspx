﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchaseChallan.aspx.cs" Inherits="PurchaseChallan" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Purchase Challan</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    PC No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtpcno" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtpcdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Chlln No.<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtchallanno" runat="server" CssClass="form-control"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    PO No.<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtpono" runat="server" CssClass="form-control" Width="100px" onkeypress="javascript:return isNumber (event)"
                    placeholder="Alt + s"></asp:TextBox>
                <%--OnTextChanged="txtpono_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtpono"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getpono">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtpono1" runat="server" CssClass="form-control" Width="100px" onkeypress="javascript:return isNumber (event)"
                    placeholder="Alt + s"></asp:TextBox>
                <%--OnTextChanged="txtpono1_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server" TargetControlID="txtpono1"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getpono">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-5">
                <%--<asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    User</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Status<span style="color: Red;">*</span></label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtsrtringpono" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox><asp:HiddenField
                    ID="hdnstrpono" runat="server" />
            </div>
            <div class="col-md-1">
                <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="22px"></asp:Label></div>
            <div class="col-md-1">
                <label class="control-label">
                    PO Dt.<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtpodate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <asp:TextBox ID="txtpodate1" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Bill Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Stock Qty</label>(<asp:Label ID="lblcloseqty" runat="server" ForeColor="Red"></asp:Label>)
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Unit</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Amount</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Code
                </label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-3 text-center">
                <label class="control-label">
                    Description 3</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-4" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                    Width="200px" AutoPostBack="true" OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getitemname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbillqty" runat="server" CssClass="form-control" placeholder="Bill Qty"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbillqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtstockqty" runat="server" CssClass="form-control" placeholder="Stock Qty"
                    Width="90px" onkeypress="javascript:return isNumber (event)" AutoPostBack="True"
                    OnTextChanged="txtstockqty_TextChanged"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                    Width="90px"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                    Width="90px" onkeypress="javascript:return isNumber (event)" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" placeholder="Code"
                    Width="90px" AutoPostBack="true" OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-4" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="200px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-4" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvvalpurchasechallan" OnClick="btnadd_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvvalpurchasechallan" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpcitemlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvpcitemlist_RowDeleting" OnRowCommand="gvpcitemlist_RowCommand">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete123();"
                                        CommandArgument='<%# bind("vid") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PI No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VNo." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvno" ForeColor="Black" runat="server" Text='<%# bind("vno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                    <asp:Label ID="lblqtyremain" runat="server" ForeColor="Green" Text='<%# bind("qtyremain") %>'></asp:Label>
                                    <asp:Label ID="lblqtyused" runat="server" ForeColor="Red" Text='<%# bind("qtyused") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' Width="80px"
                                        AutoPostBack="True" OnTextChanged="txtgridqty_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Qty" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvstockqty" ForeColor="Black" runat="server" Text='<%# bind("stockqty") %>'></asp:Label>
                                    <asp:TextBox ID="txtgvstockqty" runat="server" Text='<%# bind("stockqty") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgvstockqty_TextChanged"></asp:TextBox>(<asp:Label
                                            ID="lblgvcloseqty" runat="server" ForeColor="Red"></asp:Label>)
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvunit" runat="server" Text='<%# bind("unit") %>' Width="120px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label><asp:TextBox
                                        ID="txtgridrate" runat="server" Text='<%# bind("rate") %>' Width="120px" AutoPostBack="True"
                                        OnTextChanged="txtgridrate_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridamount" runat="server" Text='<%# bind("basicamount") %>'
                                        Width="120px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <asp:Button ID="btnsave" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                ValidationGroup="valpurchasechallan" OnClick="btnsave_Click" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valpurchasechallan" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter purchase challan date."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="txtpcdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter challan no."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="txtchallanno"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter po no."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="txtpono"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter po date."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="txtpodate"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter po date."
                Text="*" ValidationGroup="valpurchasechallan" ControlToValidate="txtpodate1"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="drpitemname"
                InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter bill qty."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtbillqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter stock qty."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtstockqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtcode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valpurchasechallan" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>
            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter description1."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtdescription1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter description2."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtdescription2"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter description3."
                Text="*" ValidationGroup="valgvvalpurchasechallan" ControlToValidate="txtdescription3"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Pc Date must be dd-MM-yyyy" ValidationGroup="valpurchasechallan"
                ForeColor="Red" ControlToValidate="txtpcdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Po Date must be dd-MM-yyyy" ValidationGroup="valpurchasechallan"
                ForeColor="Red" ControlToValidate="txtpodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="po Date1 must be dd-MM-yyyy" ValidationGroup="valpurchasechallan"
                ForeColor="Red" ControlToValidate="txtpodate1" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
                Width="95%" Style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span style="color: Red;"><b>Purchase Order Selection</b></span></div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>A/C Name :</b></div>
                    <div class="col-md-7">
                        <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
                </div>
                <div class="row">
                    <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                        <asp:Label ID="lblemptypo" runat="server" ForeColor="Red"></asp:Label><asp:HiddenField
                            ID="HiddenField1" runat="server" />
                        <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                        <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                            OnClick="btnselectitem_Click" />
                        <asp:GridView ID="gvpurchaseorder" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered" OnRowCommand="gvpurchaseorder_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                            ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("pono") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Purchase Chlln No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("pono") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpcdate" runat="server" ForeColor="#505050" Text='<%# bind("podate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvpoitemlistselection" runat="server" AutoGenerateColumns="False"
                            Width="1300px" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                            Height="0px" CssClass="table table-bordered">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />--%>
                                        <asp:CheckBox ID="chkselect" runat="server" />
                                    </ItemTemplate>
                                    <%--<HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>--%>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                </div>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtpcdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate1.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });



        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtpcdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpcdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate1.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpodate1.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
