﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockJVIN.aspx.cs" Inherits="StockJVIN" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Stock JV->Stock IN</span><br />
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Challan No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtchallanno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();" AutoPostBack="True" OnTextChanged="txtchallanno_TextChanged"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtjvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="True"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <%--<asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="432px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Stock Qty<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate<span style="color: Red;">*</span></label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-3 text-center">
                <label class="control-label">
                    Description 3</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                    Width="350px" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getitemname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtstockqty" runat="server" CssClass="form-control" placeholder="Stock Qty"
                    Width="90px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="350px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-4" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="350px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="320px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsaves" runat="server" Text="ADD" Height="30px" BackColor="#F05283"
                    ValidationGroup="gvstockjvin" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="gvstockjvin" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="250px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvitem" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvitem_RowDeleting">
                        <Columns>
                            <%-- <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvdescr1" runat="server" CssClass="form-control" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvdescr2" runat="server" CssClass="form-control" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvdescr3" runat="server" CssClass="form-control" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
            ValidationGroup="stockjvin" OnClick="btnsaveall_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="stockjvin" />
        <div class="row">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter challan no."
                Text="*" ValidationGroup="stockjvin" ControlToValidate="txtchallanno"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter date."
                Text="*" ValidationGroup="stockjvin" ControlToValidate="txtjvdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="stockjvin" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="gvstockjvin" ControlToValidate="drpitemname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter stock qty."
                Text="*" ValidationGroup="gvstockjvin" ControlToValidate="txtstockqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="gvstockjvin" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Jv Date must be dd-MM-yyyy" ValidationGroup="stockjvin" ForeColor="Red"
                ControlToValidate="txtjvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtjvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtjvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtjvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
        });
    </script>
</asp:Content>
