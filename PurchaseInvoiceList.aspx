﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchaseInvoiceList.aspx.cs" Inherits="PurchaseInvoiceList" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Purchase Invoice List</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnpurchaseinvoice" runat="server" Text="Add New Purchase Invoice"
                    class="btn btn-default forbutton" OnClick="btnpurchaseinvoice_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpurchaseinvoicelist" runat="server" AutoGenerateColumns="False"
                        Width="100%" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered" OnRowCommand="gvpurchaseinvoicelist_RowCommand"
                        OnRowDeleting="gvpurchaseinvoicelist_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strpino") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PI No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstrpino" ForeColor="Black" runat="server" Text='<%# bind("strpino") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PI No." SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblpino" ForeColor="Black" runat="server" Text='<%# bind("pino") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Party Bill No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbillno" ForeColor="Black" runat="server" Text='<%# bind("billno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PI Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpidate" ForeColor="Black" runat="server" Text='<%# bind("pidate") %>' Visible="false"></asp:Label>
                                    <%#Convert.ToDateTime(Eval("pidate")).ToString("dd-MM-yyyy") %>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Party Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Basic" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotbasicamount" ForeColor="Black" runat="server" Text='<%# bind("totbasicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbillamount" ForeColor="Black" runat="server" Text='<%# bind("billamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Purchase Head" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpurchaseac" ForeColor="Black" runat="server" Text='<%# bind("purchaseac") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("strpino") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
