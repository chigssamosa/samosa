﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepCashPayment.aspx.cs" Inherits="RepCashPayment" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Cash Payment Report</span><br />
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Cash Name</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtacname" runat="server" CssClass="form-control" 
                    Width="200px" AutoPostBack="True" ontextchanged="txtacname_TextChanged"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Voucher NO.</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtvono" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtvono"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getvono">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsaves" runat="server" Text="Preview" class="btn btn-default forbutton"
                    ValidationGroup="val" onclick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter cash name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtacname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter voucher no."
                    Text="*" ValidationGroup="val" ControlToValidate="txtvono"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
</asp:Content>
