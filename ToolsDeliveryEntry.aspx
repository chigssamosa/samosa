﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ToolsDeliveryEntry.aspx.cs" Inherits="ToolsDeliveryEntry" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Tools Delivery Entry</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Voucher No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtvoucherno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="True"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getacname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="150px"
                            AutoPostBack="True" OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtclientcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Despatched By<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtdespatchedby" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Signed Or Not</label></div>
                    <div class="col-md-1">
                        <asp:CheckBox ID="chksigned" runat="server" />
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtsignremark" runat="server" CssClass="form-control" placeholder="Sign Remarks"></asp:TextBox></div>
                </div>
                <div class="row" style="height: 10px;">
                </div>
                <div class="row">
                    <div class="col-md-2 text-center">
                        <label class="control-label">
                            Tool Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1 text-center">
                        <label class="control-label">
                            Qty.<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2 text-center">
                        <label class="control-label">
                            Unit<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2 text-center">
                        <label class="control-label">
                            Description</label>
                    </div>
                    <div class="col-md-2 text-center">
                        <label class="control-label">
                            Remarks</label>
                    </div>
                    <div class="col-md-1 text-center">
                        <label class="control-label">
                            RFS</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <asp:TextBox ID="txttoolname" runat="server" CssClass="form-control" placeholder="Tool Name"
                            AutoPostBack="True" OnTextChanged="txttoolname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttoolname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetToolname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" placeholder="Qty"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                            onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control" placeholder="Description"
                            onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" placeholder="Remarks"
                            onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtrfs" runat="server" CssClass="form-control" placeholder="RFS"
                            onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                            ValidationGroup="valgvtool" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valgvtool" />
                    </div>
                </div>
                <div class="row" style="height: 10px;">
                </div>
                <div class="row table-responsive">
                    <div class="col-md-12">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                            <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                OnRowDeleting="rptlist_RowDeleting" BackColor="White" BorderColor="White" BorderWidth="2px"
                                CellPadding="3" GridLines="None" CellSpacing="1">
                                <Columns>
                                    <%--<asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tool Name" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltoolname" runat="server" ForeColor="#505050" Text='<%# bind("toolname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblqty" runat="server" ForeColor="#505050" Text='<%# bind("qty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblunit" runat="server" ForeColor="Black" Text='<%# bind("unit") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldescr" runat="server" ForeColor="Black" Text='<%# bind("descr") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Returned" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtreturned" runat="server" Text='<%# bind("rfs") %>'></asp:TextBox>
                                            <%--<asp:Label ID="lblremarks" runat="server" ForeColor="Black" Text='<%# bind("remarks") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" SortExpression="Location">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblremarks" runat="server" ForeColor="Black" Text='<%# bind("remarks") %>'></asp:Label>--%>
                                            <asp:TextBox ID="txtremarks" runat="server" Text='<%# bind("remarks") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                                ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                                CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#33276A" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>
                <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                    ValidationGroup="valtool" OnClick="btnsaveall_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valtool" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtvdate"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter name."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter client code."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter despatched by."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtdespatchedby"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter tool name."
                    Text="*" ValidationGroup="valgvtool" ControlToValidate="txttoolname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter qty."
                    Text="*" ValidationGroup="valgvtool" ControlToValidate="txtqty"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter unit."
                    Text="*" ValidationGroup="valgvtool" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Voucher Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                    ControlToValidate="txtvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
