﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AccountMaster.aspx.cs" Inherits="AccountMaster" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .widthofcode
        {
            width: 400px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Account Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Account Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtaccountname" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Contact Person</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtcontactperson" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 1<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress1" runat="server" CssClass="form-control" Width="250px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 2</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress2" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Width="250px" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 3</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress3" runat="server" CssClass="form-control" Width="250px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
            </div>
            <div class="col-md-4" style="border: 2px solid red !important">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Opening</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtopening" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Debit</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtdebit" runat="server" CssClass="form-control" Width="250px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Credit</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtcredit" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Closing</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtclosing" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1" style="width: 12.533333% !important">
                        <label class="control-label">
                            City</label>
                    </div>
                    <div class="col-md-2" style="width: 20.666667% !important">
                        <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" Width="250px" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Due Days</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtduedays" runat="server" CssClass="form-control" Width="50px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Discount %</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtdiscount" runat="server" CssClass="form-control" Width="50px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            State
                        </label>
                    </div>
                    <div class="col-md-1">
                        <%--<asp:TextBox ID="txtactype" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>--%>
                        <asp:DropDownList ID="drpstate" runat="server" CssClass="form-control" Width="250px">
                            <asp:ListItem>ANDAMAN AND NICOBAR</asp:ListItem>
                            <asp:ListItem>ANDHRA PRADESH</asp:ListItem>
                            <asp:ListItem>ARUNACHAL PRADESH</asp:ListItem>
                            <asp:ListItem>ASSAM</asp:ListItem>
                            <asp:ListItem>BIHAR</asp:ListItem>
                            <asp:ListItem>CHANDIGARH</asp:ListItem>
                            <asp:ListItem>CHHATTISGARH</asp:ListItem>
                            <asp:ListItem>DADRA AND NAGAR HAVELI</asp:ListItem>
                            <asp:ListItem>DAMAN AND DIU</asp:ListItem>
                            <asp:ListItem>DELHI</asp:ListItem>
                            <asp:ListItem>GOA</asp:ListItem>
                            <asp:ListItem>GUJARAT</asp:ListItem>
                            <asp:ListItem>HARYANA</asp:ListItem>
                            <asp:ListItem>HIMACHAL PRADESH</asp:ListItem>
                            <asp:ListItem>JAMMU AND KASHMIR</asp:ListItem>
                            <asp:ListItem>JHARKHAND</asp:ListItem>
                            <asp:ListItem>KARNATAKA</asp:ListItem>
                            <asp:ListItem>KERALA</asp:ListItem>
                            <asp:ListItem>LAKSHADWEEP</asp:ListItem>
                            <asp:ListItem>MADHYA PRADESH</asp:ListItem>
                            <asp:ListItem>MAHARASHTRA</asp:ListItem>
                            <asp:ListItem>MANIPUR</asp:ListItem>
                            <asp:ListItem>MEGHALAYA</asp:ListItem>
                            <asp:ListItem>MIZORAM</asp:ListItem>
                            <asp:ListItem>NAGALAND</asp:ListItem>
                            <asp:ListItem>ORISSA</asp:ListItem>
                            <asp:ListItem>PONDICHERRY</asp:ListItem>
                            <asp:ListItem>PUNJAB</asp:ListItem>
                            <asp:ListItem>RAJASTHAN</asp:ListItem>
                            <asp:ListItem>SIKKIM</asp:ListItem>
                            <asp:ListItem>TAMIL NADU</asp:ListItem>
                            <asp:ListItem>TELANGANA</asp:ListItem>
                            <asp:ListItem>TRIPURA</asp:ListItem>
                            <asp:ListItem>UTTAR PRADESH</asp:ListItem>
                            <asp:ListItem>UTTARAKHAND</asp:ListItem>
                            <asp:ListItem>WEST BENGAL</asp:ListItem>
                            <asp:ListItem>OTHER TERRITORY</asp:ListItem>
                            <asp:ListItem>OTHER COUNTRIES</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Phone(o)</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtphoneo" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtphoneo1" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Sr. Tax No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtsrtaxno" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Phone(R)
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtphoner" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            TAN No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txttanno" runat="server" CssClass="form-control" Width="250px" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtphoner1" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            PAN No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtpanno" runat="server" CssClass="form-control" Width="250px" onkeypress="return checkQuote();"
                            MaxLength="10"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Mobile<%--<span style="color: Red; font-size: 20px;">*</span>--%></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-2" style="width: 14.966667% !important">
                        <label class="control-label">
                            Fax
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtfax" runat="server" CssClass="form-control" Width="250px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Dr Code</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdebitegroupcode" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender10" runat="server" TargetControlID="txtdebitegroupcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"                            
                             ServiceMethod="Getcodename">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Cr Code</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtcreditgroupcode" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender11" runat="server" TargetControlID="txtcreditgroupcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getcodename">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            E-Mail
                        </label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            A/C Type
                        </label>
                    </div>
                    <div class="col-md-1">
                        <%--<asp:TextBox ID="txtactype" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>--%>
                        <asp:DropDownList ID="drpactype" runat="server" CssClass="form-control" Width="250px">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            GST TIN No & Date
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtgsttinno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtgstdate" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            CST TIN No & Date
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtcsttinno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtcstdate" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            GST No.
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtgstno" runat="server" CssClass="form-control" MaxLength="15"
                            AutoPostBack="True" OnTextChanged="txtgstno_TextChanged"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            GST Date
                        </label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtgstnodate" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Expense Debit
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkexpensedebit" runat="server" Text="If Expense Export in Excel" />
                    </div>
                    <div class="col-md-2">
                        Exclude From Tax
                    </div>
                    <div class="col-md-2">
                        <asp:CheckBox ID="chkexcludetax" runat="server" Text="Exclude From Tax" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            In Export(GSTR1)
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkinexport" runat="server" Text="In GST Export in GSTR1" />
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-2">
                        <asp:TextBox ID="txtccode" runat="server" CssClass="form-control" Width="100px" placeholder="CCODE"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtopbalance" runat="server" CssClass="form-control" Width="100px"
                            placeholder="OPBAL"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtopdate" runat="server" CssClass="form-control" Width="100px"
                            placeholder="OP Date"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnsave" runat="server" Text="Save" class="btn btn-default forbutton"
                    OnClick="btnaccount_Click" ValidationGroup="valaccount" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valaccount" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter account name."
                    Text="*" ValidationGroup="valaccount" ControlToValidate="txtaccountname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter address 1."
                    Text="*" ValidationGroup="valaccount" ControlToValidate="txtaddress1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter Proper Email ID. e.g. ( test@test.com )"
                    Text="*" ValidationGroup="valaccount" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter mobile."
                    Text="*" ValidationGroup="valaccount" ControlToValidate="txtmobile"></asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Gst Date must be dd-MM-yyyy" ValidationGroup="valaccount" ForeColor="Red"
                    ControlToValidate="txtgstdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Cst Date must be dd-MM-yyyy" ValidationGroup="valaccount" ForeColor="Red"
                    ControlToValidate="txtcstdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Op Date must be dd-MM-yyyy" ValidationGroup="valaccount" ForeColor="Red"
                    ControlToValidate="txtopdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtgstdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtcstdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtopdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtgstdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtgstdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
            $("#<%= txtcstdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtcstdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
            $("#<%= txtopdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtopdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
