﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

public partial class CashPaymentList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForBankReceipt fbrclass = new ForBankReceipt();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.istype = "CP";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectallbr1data(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvcashpaymentlist.Visible = true;
            gvcashpaymentlist.DataSource = dtdata;
            gvcashpaymentlist.DataBind();
        }
        else
        {
            gvcashpaymentlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Bank Receipt Data Found.";
        }
    }

    protected void btncashpayment_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/CashPaymentEntry.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvcashpaymentlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/CashPaymentEntry.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.istype = "CP";
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtitemdata = new DataTable();
                dtitemdata = fbrclass.selectbrdatafromvno(li);
                for (int c = 0; c < dtitemdata.Rows.Count; c++)
                {
                    li.id = Convert.ToInt64(dtitemdata.Rows[c]["id"].ToString());
                    DataTable dtamount = new DataTable();
                    dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
                    if (dtamount.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtamount.Rows.Count; q++)
                        {
                            li.amount = Convert.ToDouble(dtamount.Rows[q]["amount1"].ToString());
                            //li.invoiceno = Convert.ToInt64(dtamount.Rows[0]["invoiceno"].ToString());
                            li.acname = dtamount.Rows[q]["acname"].ToString();
                            li.issipi = dtamount.Rows[q]["issipi1"].ToString();
                            if (li.issipi == "SI")
                            {
                                li.strsino = dtamount.Rows[q]["invoiceno"].ToString();
                                DataTable dtsi = new DataTable();
                                dtsi = fbrclass.selectamountforupdationstring(li);
                                li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
                                li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
                                fbrclass.updatesidata11string(li);
                            }
                            else
                            {
                                li.strpino = dtamount.Rows[q]["invoiceno"].ToString();
                                DataTable dtsi = new DataTable();
                                dtsi = fbrclass.selectamountforupdationpistring(li);
                                li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
                                li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
                                fbrclass.updatepidata11string(li);
                            }
                        }
                    }


                    li.istype = "CP";
                    //fbrclass.deletebrdatafromvno(li);
                    fbrclass.deletebrdata(li);
                    fbrclass.deletefromshadowtable(li);
                    //fbrclass.deletefromledgertable(li);
                }
                fbrclass.deletebr1data(li);
                fbrclass.deleteallfromshadowtable(li);
                fbrclass.deleteallledgerdatacp(li);
                fbrclass.updateisusedncp(li);
                li.strvoucherno = "CP" + li.voucherno.ToString();
                fbrclass.deleteledger_edata(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Cash payment deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            rptdoc = new ReportDocument();
            string Xrepname = "Cash Payment Report";
            Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?vno=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            li.voucherno = vno;
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dada = new SqlDataAdapter("select * from BankACMaster1 where voucherno='" + li.voucherno + "' and cno='" + li.cno + "' and istype='CP'", con);
            DataTable dtdt1 = new DataTable();
            dada.Fill(dtdt1);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from BankACMaster1 inner join BankACMaster on BankACMaster1.voucherno=BankACMaster.voucherno where BankACMaster1.voucherno='" + li.voucherno + "' and BankACMaster1.istype='CP' and BankACMaster1.cno='" + li.cno + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select * from BankACMaster where voucherno='" + li.voucherno + "' and cno='" + li.cno + "' and istype='CP'", con);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable3"]);
            if (imageDataSet.Tables["DataTable3"].Rows.Count > 0)
            {
                //string amount = Convert.ToDouble(dtdt1.Rows[0]["total"].ToString()).ToString("0.00");
                //ftow = changeToWords(amount);
                string amount = Convert.ToDouble(dtdt1.Rows[0]["total"].ToString()).ToString("0.00");
                Int64 a = Convert.ToInt64(amount.Split('.')[0]);
                int b = Convert.ToInt16(amount.Split('.')[1]);
                ftow = NumberToText(a) + " Rupees ";
                if (b == 0)
                {
                    ftow = ftow + " Only.";
                }
                if (b != 0)
                {
                    ftow = ftow + " and " + NumberToText1(b);
                }
            }
            string rptname;
            rptname = Server.MapPath(@"~/Reports/CashPayment.rpt");
            rptdoc.Load(rptname);
            //rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
            rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
            rptdoc.SetDataSource(imageDataSet.Tables["DataTable3"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "cp.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            string path = fname;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('CPPrinting.aspx?&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    protected void gvcashpaymentlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvcashpaymentlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvcashpaymentlist.PageIndex = e.NewPageIndex;
        fillgrid();
    }

    public string NumberToText(Int64 number)
    {
        if (number == 0) return "Zero";
        Int64[] num = new Int64[4];
        int first = 0;
        Int64 u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }
        string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lakh ", "Crore " };
        num[0] = number % 1000; // units
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands
        num[3] = number / 10000000; // crores
        num[2] = num[2] - 100 * num[3]; // lakhs
        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10; // ones
            t = num[i] / 10;
            h = num[i] / 100; // hundreds
            t = t - 10 * h; // tens
            if (h > 0) sb.Append(words0[h] + "Hundred ");
            if (u > 0 || t > 0)
            {
                //if (h > 0 || i == 0) sb.Append("and ");
                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }
            if (i != 0) sb.Append(words3[i - 1]);
        }


        // TextBox2.Text = "Rupees " + sb.ToString().TrimEnd() + " Only";
        return sb.ToString().TrimEnd();
    }
    public string NumberToText1(int number)
    {
        if (number != 0)
        {
            if (number == 0) return "Zero";
            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10; // ones
                t = num[i] / 10;
                h = num[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    //if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            // TextBox2.Text = "Rupees " + sb.ToString().TrimEnd() + " Only";
            return sb.ToString().TrimEnd() + " Paisa Only.";
        }
        else
        {
            return null;
        }
    }

    public String changeToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumber(points).Trim() + " Rupees";

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Lakh ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Core ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

    protected void gvcashpaymentlist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //var gv = (GridView)e.Row.FindControl("gvemember");
            var lblvoucherno = (Label)e.Row.FindControl("lblvoucherno");
            string vno = lblvoucherno.Text;
            string istype = "CP";
            con.Open();
            var ddl = (DropDownList)e.Row.FindControl("drpstatus");
            var lbl = (Label)e.Row.FindControl("lblstatus");
            SqlCommand cmd = new SqlCommand("select * from BankACMaster where voucherno=" + lblvoucherno.Text + " and istype='" + istype + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            da.Fill(ds);
            con.Close();

            var lnkmove = (ImageButton)e.Row.FindControl("imgbtnselect");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                if (ds.Rows[i]["agbill"].ToString() == "Y")
                {
                    lnkmove.Visible = false;
                }
            }
        }
    }
}