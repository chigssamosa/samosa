﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;
using System.Data;
using System.Net;

public partial class Login : System.Web.UI.Page
{
    ForLogin flclass = new ForLogin();
    LogicLayer li = new LogicLayer();
    //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
    public HttpCookie cookie = new HttpCookie("ForLogin");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            fillacyeardrop();
            Page.SetFocus(txtusername);
        }
    }

    public void fillacyeardrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = flclass.selectacyearfromshopid(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacyear.DataSource = dtdata;
            drpacyear.DataTextField = "acyear";
            drpacyear.DataBind();
            drpacyear.Items.Insert(0, "--Select--");
        }
        else
        {
            drpacyear.Items.Clear();
            drpacyear.Items.Insert(0, "--Select--");
        }
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        ////How to use sqlite as database
        //string shopid = "12345";
        //string username = txtusername.Text;
        //string password = txtpassword.Text;
        //string inputFile = Server.MapPath("~/db/alluregarment.s3db");
        //string dbConnection = String.Format("Data Source={0}", inputFile);
        //SQLiteConnection cnn = new SQLiteConnection(dbConnection);
        //SQLiteCommand cmd = new SQLiteCommand("insert into login (username,password,shopid) values ('" + username + "','" + password + "','" + shopid + "')", cnn);
        //cnn.Open();
        //cmd.ExecuteNonQuery();
        //cnn.Close();
        DataTable dtexpiry = new DataTable();
        dtexpiry = flclass.checkexpiry();
        DateTime startdate = System.DateTime.Now;
        DateTime enddate = Convert.ToDateTime(dtexpiry.Rows[0]["enddate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //if (startdate>= enddate)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('License Expired.Please Contact ALLURE SOFTTECH.');", true);
        //    return;
        //}
        if (txtusername.Text.Trim() != string.Empty && txtpassword.Text.Trim() != string.Empty && drpacyear.SelectedItem.Text != "--Select--")
        {
            string dbnameee = drpacyear.SelectedItem.Text.Split('-')[0] + drpacyear.SelectedItem.Text.Split('-')[1];
            cookie.Values.Add("currentyear", dbnameee);
            li.username = txtusername.Text;
            li.password = txtpassword.Text;
            bool result = false;
            result = flclass.userlogin(li);
            //DataTable dtexp = fl.selectexpirydate();
            //DateTime exp = Convert.ToDateTime(dtexp.Rows[0]["expirydate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //string rag = exp.ToString("dd-MM-yyyy");
            //string aje = System.DateTime.Now.ToString("dd-MM-yyyy");
            //if (aje == rag)
            //{
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Contact Allure Softtech.');", true);
            //    fl.updateexpiration(li);
            //    //return;
            //}
            //if (dtexp.Rows[0]["isexpired"].ToString() == "Y")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Contact Allure Softtech.');", true);
            //    return;
            //}
            if (result == true)
            {
                DataTable dtlogindata = new DataTable();
                dtlogindata = flclass.selectlogindatafromlogintable(li);
                if (dtlogindata.Rows.Count > 0)
                {
                    cookie.Values.Add("acyear", drpacyear.SelectedItem.Text);
                    cookie.Values.Add("username", txtusername.Text);
                    cookie.Values.Add("UserRole", dtlogindata.Rows[0]["role"].ToString());
                    SessionMgt.UserRole = dtlogindata.Rows[0]["role"].ToString();
                    li.cno=Convert.ToInt64(dtlogindata.Rows[0]["cno"].ToString());
                    DataTable dtcmast = new DataTable();
                    dtcmast = flclass.selectcmastdatafromcno(li);
                    cookie.Values.Add("cname", dtcmast.Rows[0]["cname"].ToString());
                    //Request.Cookies["ForLogin"]["shopid"]
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Something Went Wrong.Try Again.');", true);
                    return;
                }
                string honey = "";
                honey = System.Net.Dns.GetHostName();
                IPHostEntry ipentry = System.Net.Dns.GetHostEntry(honey);
                IPAddress[] ipadd = ipentry.AddressList;
                ipadd[ipadd.Length - 1].ToString();


                //FormsAuthentication.RedirectFromLoginPage
                //   (txtusername.Text, Persist.Checked);
                SessionMgt.UserName = txtusername.Text;
                SessionMgt.UserID = 1;
                Session["IsLogIn"] = true;
                cookie.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
                HttpContext.Current.Response.AppendCookie(cookie);
                //cookie.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
                //HttpContext.Current.Response.AppendCookie(cookie);
                //Response.Redirect("CompanyList.aspx");
                Response.Redirect("~/CompanySelection.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid username Or Password Or AC Year.Try Again.');", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid username Or Password Or AC Year.Try Again.');", true);
            return;
        }
    }

}