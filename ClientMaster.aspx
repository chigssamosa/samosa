﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClientMaster.aspx.cs" Inherits="ClientMaster" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Client Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Code</label></div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Head AC Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtacname" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtacname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 1<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress1" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 2</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress2" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address 3</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress3" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            City</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Email ID</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtemailid" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Mobile No.<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtmobileno" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Pincode</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            OP Balance</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtopbalance" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Reference Name</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtreference" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Handled By</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txthandledby" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Status<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Date</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Remarks</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="return checkQuote();" TextMode="MultiLine"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valclient" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valclient" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter name."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter head ac name."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtacname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter address 1."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtaddress1"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter mobile no."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtmobileno"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select status."
                            Text="*" ValidationGroup="valclient" ControlToValidate="drpstatus" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter Proper Email ID. e.g. ( test@test.com )"
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtemailid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                            ErrorMessage="Date must be dd-MM-yyyy" ValidationGroup="valclient" ForeColor="Red"
                            ControlToValidate="txtdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
