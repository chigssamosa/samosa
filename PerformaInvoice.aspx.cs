﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.SqlTypes;

public partial class PerformaInvoice : System.Web.UI.Page
{
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForPerformaInvoice fpiclass = new ForPerformaInvoice();
    ForSalesInvoice fsiclass = new ForSalesInvoice();
    ForActivity faclass = new ForActivity();
    ForSalesChallan fscclass = new ForSalesChallan();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            fillacdrop();
            if (Request["mode"].ToString() == "direct")
            {
                Session["dtpitemsper"] = CreateTemplate();
                fillgrid();
                getquono();
                fillitemnamedrop();
                fillstatusdrop();
                countgv();
                //drpdescription1.Items.Insert(0, "--SELECT--");
            }
            else if (Request["mode"].ToString() == "directsi")
            {
                Session["dtpitemsper"] = CreateTemplate();
                fillgridsi();
                getquono();
                fillitemnamedrop();
                fillstatusdrop();
                //countgv();
                //drpdescription1.Items.Insert(0, "--SELECT--");
            }
            else if (Request["mode"].ToString() == "insert")
            {
                chkigst.Enabled = false;
                Session["dtpitemsper"] = CreateTemplate();
                //fillgrid();
                getquono();
                fillitemnamedrop();
                fillstatusdrop();
                //countgv();
                //drpdescription1.Items.Insert(0, "--SELECT--");
            }
            else if (Request["mode"].ToString() == "update")
            {
                //chkigst.Enabled = false;
                fillitemnamedrop();
                fillstatusdrop();
                filleditdata();
                //drpdescription1.Items.Insert(0, "--SELECT--");
            }
            else if (Request["mode"].ToString() == "revised")
            {
                //drpdescription1.Items.Insert(0, "--SELECT--");
                fillitemnamedrop();
                fillstatusdrop();
                //filleditdata();
                Session["dtpitemsper"] = CreateTemplate();
                DataTable dt = (DataTable)Session["dtpitemsper"];
                li.invoiceno = Convert.ToInt64(Request["invno"].ToString());
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectquodatafromquono(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtquotationno.Text = dtdata.Rows[0]["invno"].ToString();
                    txtsodate.Text = (Convert.ToDateTime(dtdata.Rows[0]["invdate"].ToString())).ToString("dd-MM-yyyy");
                    txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    txtbasicamount.Text = dtdata.Rows[0]["basicamount"].ToString();
                    txtvat.Text = dtdata.Rows[0]["vat"].ToString();
                    txtadvat.Text = dtdata.Rows[0]["advat"].ToString();
                    txtcst.Text = dtdata.Rows[0]["cst"].ToString();
                    txttotalso.Text = dtdata.Rows[0]["amount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtquotationno.Text = (Convert.ToDouble(Request["quono"].ToString()) + 0.1).ToString();

                    dt.Clear();
                    DataTable dtitems = new DataTable();
                    dtitems = fpiclass.selectquoitemsdatafromquono(li);
                    for (int d = 0; d < dtitems.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtitems.Rows[d]["id"].ToString();
                        dr["itemname"] = dtitems.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtitems.Rows[d]["qty"].ToString();
                        dr["unit"] = dtitems.Rows[d]["unit"].ToString();
                        dr["rate"] = dtitems.Rows[d]["rate"].ToString();

                        dr["basicamt"] = Convert.ToDouble(dtitems.Rows[d]["basicamt"].ToString());
                        dr["cstp"] = Convert.ToDouble(dtitems.Rows[d]["cstp"].ToString());
                        dr["taxtype"] = dtitems.Rows[d]["taxtype"].ToString();
                        dr["vatp"] = Convert.ToDouble(dtitems.Rows[d]["vatp"].ToString());
                        dr["vatamt"] = Convert.ToDouble(dtitems.Rows[d]["vatamt"].ToString());
                        dr["addtaxp"] = Convert.ToDouble(dtitems.Rows[d]["addtaxp"].ToString());
                        dr["addtaxamt"] = Convert.ToDouble(dtitems.Rows[d]["addtaxamt"].ToString());
                        dr["cstamt"] = Convert.ToDouble(dtitems.Rows[d]["cstamt"].ToString());

                        dr["amount"] = Convert.ToDouble(dtitems.Rows[d]["amount"].ToString());
                        dr["descr1"] = dtitems.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtitems.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtitems.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        //count1();
                    }
                    Session["dtpitemsper"] = dt;
                }
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
                if (dt.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dt;
                    gvslist.DataBind();
                    lblcount.Text = dt.Rows.Count.ToString();
                }
                else
                {
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            hideimage();
            Page.SetFocus(txtsodate);
        }
    }

    public void fillposodatadirestsi()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strsino = Request["strsino"].ToString();
        DataTable dtdata = fpiclass.selectsidatafromstrsino(li);
        if (dtdata.Rows.Count > 0)
        { 
        //li.strscno=dtdata.Rows[][].ToString();
        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvslist.Rows[c].FindControl("imgbtnselect11");
            if (btnfinalsave.Text == "Update")
            {
                img.Visible = true;
            }
            else
            {
                img.Visible = false;
            }
        }
    }

    public void filleditdata()
    {
        li.invoiceno = Convert.ToInt64(Request["invno"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectquodatafromquono(li);
        if (dtdata.Rows.Count > 0)
        {
            txtquotationno.Text = dtdata.Rows[0]["invno"].ToString();
            txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["invdate"].ToString()).ToString("dd-MM-yyyy");
            txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            txtbasicamount.Text = dtdata.Rows[0]["basicamount"].ToString();
            txtvat.Text = dtdata.Rows[0]["vat"].ToString();
            txtadvat.Text = dtdata.Rows[0]["advat"].ToString();
            txtcst.Text = dtdata.Rows[0]["cst"].ToString();
            txttotalso.Text = dtdata.Rows[0]["amount"].ToString();
            txtpono.Text = dtdata.Rows[0]["pono"].ToString();
            txtpodate.Text = dtdata.Rows[0]["podate"].ToString();
            txtannexure.Text = dtdata.Rows[0]["annexure"].ToString();
            txtsono.Text = dtdata.Rows[0]["sono"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            if (dtdata.Rows[0]["isigst"].ToString() == "Y")
            {
                chkigst.Checked = true;
            }
            //txtquotationno.Text = (Convert.ToDouble(Request["quono"].ToString()) + 0.1).ToString();
            DataTable dtitems = new DataTable();
            dtitems = fpiclass.selectquoitemsdatafromquono(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtitems;
                gvslist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            hideimage();
            btnfinalsave.Text = "Update";
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void getquono()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectlastquono();
        if (dtdata.Rows.Count > 0)
        {
            txtquotationno.Text = (Convert.ToDouble(dtdata.Rows[0]["invno1"].ToString()) + 1).ToString();
        }
        else
        {
            txtquotationno.Text = "1";
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillgrid()
    {

        li.remarks = SessionMgt.emailid.TrimEnd(',');
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallitemdata(li);

        DataTable dt = (DataTable)Session["dtpitemsper"];
        dt.Clear();
        for (int d = 0; d < dtdata.Rows.Count; d++)
        {
            DataRow dr = dt.NewRow();
            dr["id"] = dtdata.Rows[d]["id"].ToString();
            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
            dr["qty"] = 1;
            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
            dr["rate"] = dtdata.Rows[d]["salesrate"].ToString();

            dr["basicamt"] = Convert.ToDouble(dtdata.Rows[d]["salesrate"].ToString());
            if (chkigst.Checked == true)
            {
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                    dr["taxtype"] = 0;
                }
                else
                {
                    dr["taxtype"] = "";
                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                }
            }
            else
            {
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                    dr["cstp"] = 0;
                }
                else
                {
                    dr["taxtype"] = "";
                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                }

            }
            dr["vatp"] = 0;
            dr["vatamt"] = 0;
            dr["addtaxp"] = 0;
            dr["addtaxamt"] = 0;
            dr["cstamt"] = 0;

            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
            dr["descr1"] = "";
            dr["descr2"] = "";
            dr["descr3"] = "";
            dr["vid"] = 0;
            dt.Rows.Add(dr);
            //count1();
        }
        Session["dtpitemsper"] = dt;

        if (dt.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvslist.Visible = true;
            gvslist.DataSource = dt;
            gvslist.DataBind();
            lblcount.Text = dt.Rows.Count.ToString();
        }
        else
        {
            lblempty.Visible = true;
            gvslist.Visible = false;
            lblempty.Text = "No Item Found.";
        }
        hideimage();
        countgv();
    }

    public void fillgridsi()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strsino = Request["strsino"].ToString();
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallsiitemsfromsinostring(li);

        DataTable dt = (DataTable)Session["dtpitemsper"];
        dt.Clear();
        for (int d = 0; d < dtdata.Rows.Count; d++)
        {
            DataRow dr = dt.NewRow();
            dr["id"] = dtdata.Rows[d]["id"].ToString();
            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
            dr["rate"] = dtdata.Rows[d]["rate"].ToString();

            dr["basicamt"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
            //if (chkigst.Checked == true)
            //{
            //    if (dtdata.Rows[0]["taxtype"].ToString().IndexOf("-") != -1)
            //    {
            //        dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["taxtype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["taxtype"].ToString().Split('-')[1]), 2)).ToString();
            dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
            //    }
            //    else
            //    {
            //        dr["taxtype"] = "";
            //        dr["cstp"] = dtdata.Rows[d]["taxtype"].ToString();
            //    }
            //}
            //else
            //{
            //    if (dtdata.Rows[0]["taxtype"].ToString().IndexOf("-") != -1)
            //    {
            //        dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
            //        dr["cstp"] = 0;
            //    }
            //    else
            //    {
            //        dr["taxtype"] = "";
            //        dr["cstp"] = dtdata.Rows[d]["taxtype"].ToString();
            //    }

            //}
            dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
            dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
            dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
            dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
            dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
            dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();

            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
            dr["vid"] = 0;
            dt.Rows.Add(dr);
            //count1();
        }
        li.strsino = Request["strsino"].ToString();

        DataTable dtdatam = new DataTable();
        dtdatam = fsiclass.selectallsimasterdatafromsinostring(li);
        if (dtdatam.Rows.Count > 0)
        {
            txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
            drpacname.SelectedValue = dtdatam.Rows[0]["acname"].ToString();
            txtbasicamount.Text = dtdatam.Rows[0]["totbasicamount"].ToString();
            txtcst.Text = dtdatam.Rows[0]["totcst"].ToString();
            txtvat.Text = dtdatam.Rows[0]["totvat"].ToString();
            txttotalso.Text = dtdatam.Rows[0]["billamount"].ToString();
            txtadvat.Text = dtdatam.Rows[0]["totaddtax"].ToString();
        }
        Session["dtpitemsper"] = dt;

        if (dt.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvslist.Visible = true;
            gvslist.DataSource = dt;
            gvslist.DataBind();
            lblcount.Text = dt.Rows.Count.ToString();
        }
        else
        {
            lblempty.Visible = true;
            gvslist.Visible = false;
            lblempty.Text = "No Item Found.";
        }
        hideimage();
        //countgv();
    }

    public void fillacdrop()
    {
        DataTable dtac = new DataTable();
        dtac = fpiclass.selectallacnamedata();
        if (dtac.Rows.Count > 0)
        {
            drpacname.DataSource = dtac;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(drpacname);
    }

    //protected void gvslist_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    string cz = Request.Cookies["Forcon"]["conc"];
    //    cz = cz.Replace(":", ";");
    //    SqlConnection con = new SqlConnection(cz);
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        con.Open();
    //        var ddl = (DropDownList)e.Row.FindControl("drpdescr1");
    //        var lbl = (Label)e.Row.FindControl("lblitemname");
    //        var txtdescr1 = (TextBox)e.Row.FindControl("txtdescr1");
    //        SqlCommand cmd = new SqlCommand("select * from MiscMaster where type='" + lbl.Text + "'", con);
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        DataSet ds = new DataSet();
    //        da.Fill(ds);
    //        con.Close();
    //        ddl.DataSource = ds;
    //        ddl.DataTextField = "name";
    //        ddl.DataBind();
    //        ddl.Items.Insert(0, new ListItem("--SELECT--", "0"));
    //        if (Request["mode"].ToString() == "update" || Request["mode"].ToString() == "revised")
    //        {
    //            ddl.SelectedValue = txtdescr1.Text;
    //        }
    //        //ddl.SelectedValue = lbl.Text;
    //        //var gv = (GridView)e.Row.FindControl("gvproductlist");
    //        //var inqno = (Label)e.Row.FindControl("lblinquiryid");
    //        //Int64 inquiryno = Convert.ToInt64(inqno.Text);
    //        //li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //        //SqlDataAdapter da1 = new SqlDataAdapter("select * from inquiryproduct where inquiryno=" + inquiryno + " and cno=" + li.CNO + "", con);
    //        //DataSet ds1 = new DataSet();
    //        //da1.Fill(ds1);
    //        ////if (dtdata.Rows.Count > 0)
    //        ////{
    //        //gv.DataSource = ds1;
    //        //gv.DataBind();
    //    }
    //}
    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        //countgv();

        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(txtrate);
    }

    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty && lbltaxtype.Text != "0")
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(txtamount);
    }

    public void counttotal()
    {
        double tot = 0;
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            TextBox txtamount1 = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            if (txtamount1.Text.Trim() != string.Empty)
            {
                tot = tot + Convert.ToDouble(txtamount1.Text.Trim());
            }
        }
        txtbasicamount.Text = tot.ToString();
    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemsper"];
        //DataTable dtsoitem1 = (DataTable)Session["dtpitems12"];
        //if (dtsoitem1 != null)
        //{
        //    dtsoitem.Rows.Clear();
        //    dtsoitem.Merge(dtsoitem1);
        //}
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double amount = 0;
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            TextBox txtgvqty1 = (TextBox)gvslist.Rows[c].FindControl("txtqty");
            TextBox txtgvrate1 = (TextBox)gvslist.Rows[c].FindControl("txtrate");
            TextBox txtgvamount1 = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            //            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            TextBox txtgridtaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
            //Label lbltaxtype = (Label)gvslist.Rows[c].FindControl("lbltaxtype");
            TextBox txttax1 = (TextBox)gvslist.Rows[c].FindControl("txttax1");
            TextBox txttax2 = (TextBox)gvslist.Rows[c].FindControl("txttax2");
            TextBox txttax3 = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox txttax1amt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
            TextBox txttax2amt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
            TextBox txttax3amt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
            TextBox txtgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
            baseamt = baseamt + Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text);
            amount = amount + baseamt;
            if (txtgridtaxtype.Text != "" && txtgridtaxtype.Text != "0")
            {
                var cc = txtgridtaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = txtgridtaxtype.Text;
                    vatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[1]);
                    //txtgvvat.Text = vatp.ToString();
                    //txtgvadvat.Text = addvatp.ToString();
                    txttax1.Text = vatp.ToString();
                    txttax2.Text = addvatp.ToString();
                    txttax1amt.Text = (Math.Round((Convert.ToDouble(txttax1.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100, 2)).ToString();
                    txttax2amt.Text = (Math.Round((Convert.ToDouble(txttax2.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100, 2)).ToString();

                }
                else
                {
                    txttax3amt.Text = (Math.Round(((Convert.ToDouble(txttax3.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                }
            }
            else
            {
                txttax3amt.Text = (Math.Round(((Convert.ToDouble(txttax3.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
            }
            if (txttax1amt.Text != "")
            {
                vatamt = vatamt + Convert.ToDouble(txttax1amt.Text);
            }
            if (txttax2amt.Text != "")
            {
                addvatamt = addvatamt + Convert.ToDouble(txttax2amt.Text);
            }
            if (txttax3.Text != "")
            {
                cstp = cstp + Convert.ToDouble(txttax3.Text);
            }
            if (txttax3amt.Text != "")
            {
                cstamt = cstamt + Convert.ToDouble(txttax3amt.Text);
            }
            if (txttax1.Text != "")
            {
                vatp = Convert.ToDouble(txttax1.Text);
            }
            if (txttax2.Text != "")
            {
                addvatp = Convert.ToDouble(txttax2.Text);
            }
            vatp = vatp + vatp + addvatp;
            txtgvamount.Text = Math.Round((Convert.ToDouble(txtgvamount1.Text) + Convert.ToDouble(txttax1amt.Text) + Convert.ToDouble(txttax2amt.Text) + Convert.ToDouble(txttax3amt.Text)), 2).ToString();
        }
        txtbasicamount.Text = baseamt.ToString();
        //if (txtservicep.Text.Trim() != string.Empty)
        //{
        //    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtservicep.Text = "0";
        //    txtserviceamt.Text = "0";
        //}
        //txtfcstp.Text = cstp.ToString();
        txtcst.Text = cstamt.ToString();
        txtvat.Text = vatamt.ToString();
        //txtfvatp.Text = vatp.ToString();
        txtadvat.Text = addvatamt.ToString();
        txttotalso.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtcst.Text) + Convert.ToDouble(txtadvat.Text) + Convert.ToDouble(txtvat.Text)).ToString();
        //double bamt = Convert.ToDouble(txttotalso.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        //roundoff = Convert.ToDouble(txtroundoff.Text);
        //txtbillamount.Text = (Math.Round(bamt)).ToString();
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamt", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    protected void btnfinalsave_Click(object sender, EventArgs e)
    {
        li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.quodate <= yyyear1 && li.quodate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.invoiceno = Convert.ToInt64(txtquotationno.Text);
        li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.ccode = Convert.ToInt64(txtclientcode.Text);
        li.acname = drpacname.SelectedItem.Text;
        li.basicamount = Convert.ToDouble(txtbasicamount.Text);
        li.vatamt = Convert.ToDouble(txtvat.Text);
        li.addtaxamt = Convert.ToDouble(txtadvat.Text);
        li.cstamt = Convert.ToDouble(txtcst.Text);
        li.grandtotal = Convert.ToDouble(txttotalso.Text);
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.strpono = txtpono.Text;
        li.strpodate = txtpodate.Text;
        li.annexure = txtannexure.Text;
        if (chkigst.Checked == true)
        {
            li.isigst = "Y";
        }
        else
        {
            li.isigst = "N";
        }
        li.sono = Convert.ToInt64(txtsono.Text);
        if (btnfinalsave.Text == "Save")
        {
            fpiclass.insertquomasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.invoiceno + " Proforma Invoice inserted.";
            faclass.insertactivity(li);
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                Label lblvid = (Label)gvslist.Rows[c].FindControl("lblvid");
                Label lblitemname = (Label)gvslist.Rows[c].FindControl("lblitemname");
                TextBox lblunit = (TextBox)gvslist.Rows[c].FindControl("txtgvunit");
                TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
                TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
                TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                TextBox txtdescr1 = (TextBox)gvslist.Rows[c].FindControl("txtdescr1");
                TextBox txtdescr2 = (TextBox)gvslist.Rows[c].FindControl("txtdescr2");
                TextBox txtdescr3 = (TextBox)gvslist.Rows[c].FindControl("txtdescr3");


                TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
                TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
                TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
                TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                li.itemname = lblitemname.Text;
                li.unit = lblunit.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.rate = Convert.ToDouble(txtrate.Text);
                li.basicamount = Convert.ToDouble(txtamount.Text);
                li.descr1 = txtdescr1.Text;
                li.descr2 = txtdescr2.Text;
                li.descr3 = txtdescr3.Text;
                li.taxtype = lbltaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lblgvamount.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.vid = Convert.ToInt64(lblvid.Text);
                fpiclass.insertquoitemsdata(li);
            }
        }
        else
        {
            fpiclass.updatequomasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.invoiceno + " Proforma Invoice item updated.";
            faclass.insertactivity(li);
            li.invoiceno = Convert.ToInt64(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                Label lblid = (Label)gvslist.Rows[c].FindControl("lblid");
                Label lblvid = (Label)gvslist.Rows[c].FindControl("lblvid");
                Label lblitemname = (Label)gvslist.Rows[c].FindControl("lblitemname");
                TextBox lblunit = (TextBox)gvslist.Rows[c].FindControl("txtgvunit");
                TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
                TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
                TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                TextBox txtdescr1 = (TextBox)gvslist.Rows[c].FindControl("txtdescr1");
                TextBox txtdescr2 = (TextBox)gvslist.Rows[c].FindControl("txtdescr2");
                TextBox txtdescr3 = (TextBox)gvslist.Rows[c].FindControl("txtdescr3");


                TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
                TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
                TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
                TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                li.id = Convert.ToInt64(lblid.Text);
                li.vid = Convert.ToInt64(lblvid.Text);
                li.itemname = lblitemname.Text;
                li.unit = lblunit.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.rate = Convert.ToDouble(txtrate.Text);
                li.basicamount = Convert.ToDouble(txtamount.Text);
                li.descr1 = txtdescr1.Text;
                li.descr2 = txtdescr2.Text;
                li.descr3 = txtdescr3.Text;
                li.taxtype = lbltaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lblgvamount.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                fpiclass.updatequoitemsdata(li);
            }
        }
        Response.Redirect("~/PerformaInvoiceList.aspx?pagename=PerformaInvoiceList");
    }
    protected void txtgvtaxtype_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(lblvatp);
    }
    protected void txttax3_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(lblcstamt);
    }
    protected void gvslist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.quono = Convert.ToDouble(txtquotationno.Text);
            li.invoiceno = Convert.ToInt64(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblitemname = (Label)currentRow.FindControl("lblitemname");
            TextBox lblunit = (TextBox)currentRow.FindControl("txtgvunit");
            TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
            TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
            TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
            TextBox txtdescr1 = (TextBox)currentRow.FindControl("txtdescr1");
            TextBox txtdescr2 = (TextBox)currentRow.FindControl("txtdescr2");
            TextBox txtdescr3 = (TextBox)currentRow.FindControl("txtdescr3");


            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
            TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
            TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
            TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
            TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
            TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
            TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
            TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
            li.itemname = lblitemname.Text;
            li.unit = lblunit.Text;
            li.qty = Convert.ToDouble(txtqty.Text);
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtamount.Text);
            li.descr1 = txtdescr1.Text;
            li.descr2 = txtdescr2.Text;
            li.descr3 = txtdescr3.Text;
            li.taxtype = lbltaxtype.Text;
            li.vatp = Convert.ToDouble(lblvatp.Text);
            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
            li.cstp = Convert.ToDouble(lblcstp.Text);
            li.vatamt = Convert.ToDouble(lblvatamt.Text);
            li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
            li.cstamt = Convert.ToDouble(lblcstamt.Text);
            li.amount = Convert.ToDouble(lblgvamount.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fpiclass.updatequoitemsdata(li);
            DataTable dtitems = new DataTable();
            dtitems = fpiclass.selectquoitemsdatafromquono(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtitems;
                gvslist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            hideimage();
            Page.SetFocus(txtqty);
        }
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemsper"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvslist.Visible = true;
            lblempty.Visible = false;
            gvslist.DataSource = dtsession;
            gvslist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvslist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            //lblcount.Text = "0";
        }
        hideimage();
    }

    protected void gvslist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemsper"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemsper"] = dt;
            this.bindgrid();
        }
        else
        {
            if (gvslist.Rows.Count > 1)
            {
                li.invoiceno = Convert.ToInt64(txtquotationno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvslist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvslist.Rows[e.RowIndex].FindControl("lblvid");
                li.id = Convert.ToInt64(lblid.Text);
                fpiclass.deletequoitemsdatafromid(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Proforma Invoice item deleted.";
                faclass.insertactivity(li);
                DataTable dtitems = new DataTable();
                dtitems = fpiclass.selectquoitemsdatafromquono(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dtitems;
                    gvslist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        count1();
        hideimage();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty1.Text = "1";
                txtrate1.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                    string vattype = dtdata.Rows[0]["gsttype"].ToString();
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvatamt.Text = "0";
                    txtgvadvat.Text = vattype.Split('-')[1];
                    txtaddtaxamt.Text = "0";
                    count1();
                }
                else
                {
                    string vattype = dtdata.Rows[0]["gsttype"].ToString();
                    txtgvcst.Text = vattype.ToString();
                    txtcstamount.Text = "0";
                    count1();
                }
                li.type = drpitemname.SelectedItem.Text;
                //DataTable dtmisc = new DataTable();
                //dtmisc = fpiclass.selectmiscdataitemwise(li);
                //if (dtmisc.Rows.Count > 0)
                //{
                //    drpdescription1.DataSource = dtmisc;
                //    drpdescription1.DataTextField = "name";
                //    drpdescription1.DataBind();
                //    drpdescription1.Items.Insert(0, "--SELECT--");
                //}
                //else
                //{
                //    drpdescription1.Items.Clear();
                //    drpdescription1.Items.Insert(0, "--SELECT--");
                //}
            }
            else
            {
                fillitemnamedrop();
                txtqty1.Text = string.Empty;
                txtrate1.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtvatamt.Text = string.Empty;
                txtaddtaxamt.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                //drpdescription1.Items.Clear();
                //drpdescription1.Items.Insert(0, "--SELECT--");
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            fillitemnamedrop();
            txtqty1.Text = string.Empty;
            txtrate1.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            //drpdescription1.Items.Clear();
            //drpdescription1.Items.Insert(0, "--SELECT--");
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        Page.SetFocus(txtunit);
    }
    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvat);
    }
    protected void txtqty1_TextChanged(object sender, EventArgs e)
    {
        if (txtqty1.Text.Trim() == string.Empty)
        {
            txtqty1.Text = "0";
        }
        if (txtrate1.Text.Trim() == string.Empty)
        {
            txtrate1.Text = "0";
        }
        count1();
        Page.SetFocus(txtrate1);
    }
    protected void txtrate1_TextChanged(object sender, EventArgs e)
    {
        if (txtqty1.Text.Trim() == string.Empty)
        {
            txtqty1.Text = "0";
        }
        if (txtrate1.Text.Trim() == string.Empty)
        {
            txtrate1.Text = "0";
        }
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvadvat);
    }
    protected void txtgvadvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvcst);
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtcstamount);
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtdescription1);
    }
    protected void txtvatamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtaddtaxamt);
    }
    protected void txtaddtaxamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvamount);
    }
    protected void txtgvamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(btnadd);
    }

    public void count1()
    {
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double qty = 0;
        double rate = 0;
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        if (txtqty1.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty1.Text);
        }
        if (txtrate1.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate1.Text);
        }
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
        if (txtbasicamt.Text.Trim() != string.Empty)
        {
            baseamt = Convert.ToDouble(txtbasicamt.Text);
        }

        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvat.Text = vatp.ToString();
                txtgvadvat.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}

            }
            else
            {
                txtgvvat.Text = "0";
                txtgvadvat.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvat.Text = "0";
            txtgvadvat.Text = "0";
        }

        if (txtgvvat.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvat.Text);
            txtvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvat.Text = "0";
            txtvatamt.Text = "0";
        }
        if (txtgvadvat.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadvat.Text);
            txtaddtaxamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadvat.Text = "0";
            txtaddtaxamt.Text = "0";
        }
        if (Convert.ToDouble(txtgvvat.Text) != 0)
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        else
        {
            if (txtgvcst.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgvcst.Text);
                txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgvcst.Text = "0";
                txtcstamount.Text = "0";
            }
        }
        vatamt = Convert.ToDouble(txtvatamt.Text);
        addvatamt = Convert.ToDouble(txtaddtaxamt.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        if (gvslist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                TextBox lblbasicamt = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                TextBox lblsctamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamt.Text);
                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                totamount = totamount + Convert.ToDouble(lblamount.Text);
            }
            txtbasicamount.Text = totbasicamt.ToString();
            txtvat.Text = totvat.ToString();
            txtadvat.Text = totaddvat.ToString();
            txtcst.Text = totcst.ToString();
            txttotalso.Text = totamount.ToString();
        }
        else
        {
            txtbasicamount.Text = "0";
            txtvat.Text = "0";
            txtadvat.Text = "0";
            txtcst.Text = "0";
            txttotalso.Text = "0";
        }

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemsper"];
            DataRow dr = dt.NewRow();
            dr["id"] = gvslist.Rows.Count + 1;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtqty1.Text;
            dr["unit"] = txtunit.Text;
            dr["rate"] = txtrate1.Text;
            dr["basicamt"] = txtbasicamt.Text;
            dr["taxtype"] = txttaxtype.Text;
            dr["vatp"] = txtgvvat.Text;
            dr["addtaxp"] = txtgvadvat.Text;
            dr["cstp"] = txtgvcst.Text;
            dr["vatamt"] = txtvatamt.Text;
            dr["addtaxamt"] = txtaddtaxamt.Text;
            dr["cstamt"] = txtcstamount.Text;
            dr["amount"] = txtgvamount.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemsper"] = dt;
            this.bindgrid();
            count1();
        }
        else
        {
            li.invoiceno = Convert.ToInt64(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            li.itemname = drpitemname.SelectedItem.Text;
            li.qty = Convert.ToDouble(txtqty1.Text);
            li.unit = txtunit.Text;
            li.rate = Convert.ToDouble(txtrate1.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.taxtype = txttaxtype.Text;
            li.vatp = Convert.ToDouble(txtgvvat.Text);
            li.addtaxp = Convert.ToDouble(txtgvadvat.Text);
            li.cstp = Convert.ToDouble(txtgvcst.Text);
            li.vatamt = Convert.ToDouble(txtvatamt.Text);
            li.addtaxamt = Convert.ToDouble(txtaddtaxamt.Text);
            li.cstamt = Convert.ToDouble(txtcstamount.Text);
            li.amount = Convert.ToDouble(txtgvamount.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fpiclass.insertquoitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "Proforma Invoice item inserted.";
            faclass.insertactivity(li);
            li.quono = Convert.ToDouble(txtquotationno.Text);
            DataTable dtdata = new DataTable();
            dtdata = fpiclass.selectquoitemsdatafromquono(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtdata;
                gvslist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                //lblcount.Text = "0";
            }
            count1();
        }
        fillitemnamedrop();
        txtqty1.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtrate1.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txttaxtype.Text = string.Empty;
        txtgvvat.Text = string.Empty;
        txtgvadvat.Text = string.Empty;
        txtgvcst.Text = string.Empty;
        txtcstamount.Text = string.Empty;
        txtdescription1.Text = string.Empty;
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        txtvatamt.Text = string.Empty;
        txtaddtaxamt.Text = string.Empty;
        txtgvamount.Text = string.Empty;

        Page.SetFocus(drpitemname);

    }

    protected void chkigst_CheckedChanged(object sender, EventArgs e)
    {
        //if (chkigst.Checked == true)
        //{
        //for (int c = 0; c < gvslist.Rows.Count; c++)
        //{
        //    TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
        //    TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //    if (lbltaxtype.Text != string.Empty && lblcstp.Text=="0")
        //    {                
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
        //        return;
        //    }
        //    if (chkigst.Checked == false && lblcstp.Text.Trim() == string.Empty)
        //    {
        //        if (chkigst.Checked == true)
        //        {
        //            chkigst.Checked = false;
        //        }
        //        else
        //        {
        //            chkigst.Checked = true;
        //        }
        //        //if()
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
        //        return;
        //    }
        //}
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            double qty = 0;
            double rate = 0;
            //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
            TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
            TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            if (txtqty.Text != string.Empty)
            {
                qty = Convert.ToDouble(txtqty.Text);
            }
            if (txtrate.Text != string.Empty)
            {
                rate = Convert.ToDouble(txtrate.Text);
            }
            txtamount.Text = Math.Round((qty * rate), 2).ToString();
            //counttotal();
            //countgv();

            TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
            TextBox txtgridcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
            TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
            TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
            TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
            TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
            TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
            double vatp = 0;
            double addvatp = 0;
            double cstp = 0;
            double vatamt = 0;
            double addvatamt = 0;
            double cstamt = 0;
            double baseamt = Convert.ToDouble(txtamount.Text);
            if (lbltaxtype.Text != string.Empty && lblcstp.Text == "0")
            {
                var cc = lbltaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    if (chkigst.Checked == true)
                    {
                        cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                        lblcstp.Text = cstp.ToString();
                        vatp = 0;
                        addvatp = 0;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                    else
                    {
                        li.typename = lbltaxtype.Text;
                        vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                        addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        cstp = 0;
                        lblcstp.Text = cstp.ToString();
                    }
                }
                else
                {
                    if (chkigst.Checked == true)
                    {
                        cstp = Convert.ToDouble(lbltaxtype.Text);
                        lblcstp.Text = cstp.ToString();
                        vatp = 0;
                        addvatp = 0;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                    else
                    {
                        cstp = 0;
                        lblcstp.Text = cstp.ToString();
                        vatp = Convert.ToDouble(lblcstp.Text) / 2;
                        addvatp = Convert.ToDouble(lblcstp.Text) / 2;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                }
            }
            else if (lbltaxtype.Text == string.Empty && lblcstp.Text != "0")
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
                //return;
                if (chkigst.Checked == true)
                {
                    cstp = Convert.ToDouble(lbltaxtype.Text);
                    lblcstp.Text = cstp.ToString();
                    vatp = 0;
                    addvatp = 0;
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lbltaxtype.Text = string.Empty;
                }
                else
                {

                    vatp = Convert.ToDouble(lblcstp.Text) / 2;
                    addvatp = Convert.ToDouble(lblcstp.Text) / 2;
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lbltaxtype.Text = (vatp.ToString()) + "-" + (addvatp.ToString());
                    //lbltaxtype.Text = string.Empty;
                    cstp = 0;
                    lblcstp.Text = cstp.ToString();
                }
            }
            else
            {
                if (chkigst.Checked == true)
                {
                    chkigst.Checked = false;
                }
                else
                {
                    chkigst.Checked = true;
                }
                li.invoiceno = Convert.ToInt64(txtquotationno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtitems = new DataTable();
                dtitems = fpiclass.selectquoitemsdatafromquono(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dtitems;
                    gvslist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
                return;
            }
            if (lblvatp.Text != string.Empty)
            {
                vatp = Convert.ToDouble(lblvatp.Text);
                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            }
            else
            {
                lblvatamt.Text = "0";
                lblvatp.Text = "0";
            }
            if (lbladdvatp.Text != string.Empty)
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
            }
            else
            {
                lbladdvatp.Text = "0";
                lbladdvatamt.Text = "0";
            }
            if (lblcstp.Text != string.Empty)
            {
                cstp = Convert.ToDouble(lblcstp.Text);
                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                lblcstp.Text = "0";
                lblcstamt.Text = "0";
            }
            vatamt = Convert.ToDouble(lblvatamt.Text);
            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
            cstamt = Convert.ToDouble(lblcstamt.Text);
            lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
            countgv();
            Page.SetFocus(txtrate);
        }
        //}
        //else
        //{
        //    for (int c = 0; c < gvslist.Rows.Count; c++)
        //    {
        //        double qty = 0;
        //        double rate = 0;
        //        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        //        TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
        //        TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
        //        TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
        //        if (txtqty.Text != string.Empty)
        //        {
        //            qty = Convert.ToDouble(txtqty.Text);
        //        }
        //        if (txtrate.Text != string.Empty)
        //        {
        //            rate = Convert.ToDouble(txtrate.Text);
        //        }
        //        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //        //counttotal();
        //        //countgv();

        //        TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
        //        TextBox txtgridcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //        TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
        //        TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
        //        TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //        TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
        //        TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
        //        TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
        //        TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
        //        TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
        //        double vatp = 0;
        //        double addvatp = 0;
        //        double cstp = 0;
        //        double vatamt = 0;
        //        double addvatamt = 0;
        //        double cstamt = 0;
        //        double baseamt = Convert.ToDouble(txtamount.Text);
        //        if (lbltaxtype.Text != string.Empty)
        //        {
        //            var cc = lbltaxtype.Text.IndexOf("-");
        //            if (cc != -1)
        //            {
        //                if (chkigst.Checked == true)
        //                {
        //                    cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblcstp.Text = cstp.ToString();
        //                }
        //                else
        //                {
        //                    li.typename = lbltaxtype.Text;
        //                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
        //                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblvatp.Text = vatp.ToString();
        //                    lbladdvatp.Text = addvatp.ToString();
        //                }
        //            }
        //            else
        //            {
        //                if (chkigst.Checked == true)
        //                {
        //                    cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblcstp.Text = cstp.ToString();
        //                }
        //            }
        //        }
        //        if (lblvatp.Text != string.Empty)
        //        {
        //            vatp = Convert.ToDouble(lblvatp.Text);
        //            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lblvatamt.Text = "0";
        //            lblvatp.Text = "0";
        //        }
        //        if (lbladdvatp.Text != string.Empty)
        //        {
        //            addvatp = Convert.ToDouble(lbladdvatp.Text);
        //            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lbladdvatp.Text = "0";
        //            lbladdvatamt.Text = "0";
        //        }
        //        if (lblcstp.Text != string.Empty)
        //        {
        //            cstp = Convert.ToDouble(lblcstp.Text);
        //            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lblcstp.Text = "0";
        //            lblcstamt.Text = "0";
        //        }
        //        vatamt = Convert.ToDouble(lblvatamt.Text);
        //        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        //        cstamt = Convert.ToDouble(lblcstamt.Text);
        //        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        //        countgv();
        //        Page.SetFocus(txtrate);
        //    }
        //}
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            lblpopupacname.Text = drpacname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtpc = new DataTable();
            dtpc = fscclass.selectallsalesorderforpopup(li);
            if (dtpc.Rows.Count > 0)
            {
                lblemptyso.Visible = false;
                gvsalesorder.Visible = true;
                gvsalesorder.DataSource = dtpc;
                gvsalesorder.DataBind();
            }
            else
            {
                gvsalesorder.Visible = false;
                lblemptyso.Visible = true;
                lblemptyso.Text = "No Sales Order Found.";
            }
            btnselectitem.Visible = false;
            gvsoitemlistselection.Visible = false;
            chkall.Visible = false;
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsalesorder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemssach"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(e.CommandArgument);
            DataTable dtdate = new DataTable();
            dtdate = fscclass.selectpodatefrompono(li);
            //if (txtsono.Text.Trim() == string.Empty)
            //{
            if (dtdate.Rows.Count > 0)
            {
                txtpono.Text = dtdate.Rows[0]["pono"].ToString();
                txtpodate.Text = dtdate.Rows[0]["cdate"].ToString();
            }
            txtsono.Text = e.CommandArgument.ToString();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtc = new DataTable();
            dtc = fscclass.selectallsalesordermasterdatafromsono(li);
            if (dtc.Rows.Count > 0)
            {
                //drpacname.SelectedValue = dtc.Rows[0]["acname"].ToString();
                txtclientcode.Text = dtc.Rows[0]["ccode"].ToString();
            }
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                //for (int d = 0; d < dtdata.Rows.Count; d++)
                //{
                //    DataTable dt = (DataTable)Session["dtpitems"];
                //    DataRow dr = dt.NewRow();
                //    dr["id"] = dtdata.Rows[d]["id"].ToString();
                //    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                //    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                //    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                //    dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                //    dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                //    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                //    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                //    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                //    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                //    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                //    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                //    dr["vid"] = 0;
                //    dt.Rows.Add(dr);
                //    Session["dtpitems"] = dt;
                //    //this.bindgrid();
                //    this.bindgrid1();                    
                //}
                gvsalesorder.Visible = false;
                //Session["dtpitems"] = null;
                //Session["dtpitems"] = CreateTemplate();
                gvsoitemlistselection.Visible = true;
                lblemptyso.Visible = false;
                gvsoitemlistselection.DataSource = dtdata;
                gvsoitemlistselection.DataBind();
                chkall.Visible = true;
                ModalPopupExtender2.Show();
                lblempty.Visible = false;
                btnselectitem.Visible = true;
            }
            else
            {
                gvslist.Visible = false;
                gvsoitemlistselection.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            ModalPopupExtender2.Show();
            //}
        }
        hideimage();
    }

    protected void btnselectitem_Click(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            Session["dtpitemsper"] = CreateTemplate();
            if (chkall.Checked == true)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(txtsono.Text);
                if (txtsono.Text.Trim() != string.Empty)
                {
                    DataTable dtdata = new DataTable();
                    dtdata = fscclass.selectallsalesorderitemdatafromsonoforcominv(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        for (int d = 0; d < dtdata.Rows.Count; d++)
                        {
                            DataTable dt = (DataTable)Session["dtpitemsper"];
                            DataRow dr = dt.NewRow();
                            dr["id"] = dtdata.Rows[d]["id"].ToString();
                            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                            dr["basicamt"] = dtdata.Rows[d]["basicamount"].ToString();
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["vatp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                                dr["addtaxp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                                dr["cstp"] = 0;
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                dr["vatp"] = 0;
                                dr["addtaxp"] = 0;
                                dr["cstp"] = 0;
                            }
                            dr["vatamt"] = 0;
                            dr["addtaxamt"] = 0;
                            dr["cstamt"] = 0;
                            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                            dr["vid"] = dtdata.Rows[d]["id"].ToString();
                            dt.Rows.Add(dr);
                            Session["dtpitemsper"] = dt;
                            this.bindgrid();
                            //this.bindgrid1();
                            gvsalesorder.Visible = false;
                            ModalPopupExtender2.Show();
                        }
                    }
                    else
                    {
                        gvslist.Visible = false;
                        gvsoitemlistselection.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Items Found.";
                    }
                }
            }
            else
            {
                string id = "0";
                for (int cc = 0; cc < gvsoitemlistselection.Rows.Count; cc++)
                {
                    Label lblid = (Label)gvsoitemlistselection.Rows[cc].FindControl("lblid");
                    CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[cc].FindControl("chkselect");
                    if (chkselect.Checked == true)
                    {
                        id = id + "," + lblid.Text;
                    }
                }
                li.remarks = id;
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(txtsono.Text);
                if (txtsono.Text.Trim() != string.Empty)
                {
                    DataTable dtdata = new DataTable();
                    dtdata = fscclass.selectallsalesorderitemdatafromsonousinginforcomminv(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        DataTable dt = (DataTable)Session["dtpitemsper"];
                        for (int d = 0; d < dtdata.Rows.Count; d++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["id"] = dtdata.Rows[d]["id"].ToString();
                            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                            dr["basicamt"] = dtdata.Rows[d]["basicamount"].ToString();
                            //if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            //{
                            //    dr["vatp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                            //    dr["addtaxp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                            //    dr["cstp"] = 0;
                            //    dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                            //}
                            //else
                            //{
                            //    dr["taxtype"] = "";
                            //    dr["vatp"] = 0;
                            //    dr["addtaxp"] = 0;
                            //    dr["cstp"] = 0;
                            //}
                            dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                            dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                            dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
                            dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();



                            dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                            dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                            dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();
                            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                            dr["vid"] = dtdata.Rows[d]["id"].ToString();
                            dt.Rows.Add(dr);
                        }
                        Session["dtpitemsper"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                        gvsalesorder.Visible = false;
                        ModalPopupExtender2.Show();
                    }
                    else
                    {
                        gvslist.Visible = false;
                        gvsoitemlistselection.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Items Found.";
                    }
                }
            }
            hideimage();
            ModalPopupExtender2.Hide();
            countgv();
            Page.SetFocus(txtsono);
        }
        else
        {
            string id = "0";
            for (int cc = 0; cc < gvsoitemlistselection.Rows.Count; cc++)
            {
                Label lblid = (Label)gvsoitemlistselection.Rows[cc].FindControl("lblid");
                CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[cc].FindControl("chkselect");
                if (chkselect.Checked == true)
                {
                    id = id + "," + lblid.Text;
                }
            }
            li.remarks = id;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsono.Text);
            if (txtsono.Text.Trim() != string.Empty)
            {
                DataTable dtdata = new DataTable();
                dtdata = fscclass.selectallsalesorderitemdatafromsonousinginforcomminv(li);
                if (dtdata.Rows.Count > 0)
                {
                    DataTable dt = (DataTable)Session["dtpitemscomm"];
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        li.invoiceno = Convert.ToInt64(txtquotationno.Text);
                        li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        li.ccode = Convert.ToInt64(txtclientcode.Text);
                        li.acname = drpacname.SelectedItem.Text;
                        //dr["id"] = dtdata.Rows[d]["id"].ToString();
                        li.itemname = dtdata.Rows[d]["itemname"].ToString();
                        li.unit = dtdata.Rows[d]["unit"].ToString();
                        li.qty = Convert.ToDouble(dtdata.Rows[d]["qty"].ToString());
                        li.rate = Convert.ToDouble(dtdata.Rows[d]["rate"].ToString());
                        li.basicamount = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                        li.taxtype = dtdata.Rows[d]["taxtype"].ToString();
                        //if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                        //{
                        //    dr["vatp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                        //    dr["addtaxp"] = dtdata.Rows[d]["gsttype"].ToString().Split('-')[0];
                        //    dr["cstp"] = 0;
                        //    dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                        //}
                        //else
                        //{
                        //    dr["taxtype"] = "";
                        //    dr["vatp"] = 0;
                        //    dr["addtaxp"] = 0;
                        //    dr["cstp"] = 0;
                        //}
                        li.vatp = Convert.ToDouble(dtdata.Rows[d]["vatp"].ToString());
                        li.addtaxp = Convert.ToDouble(dtdata.Rows[d]["addtaxp"].ToString());
                        li.cstp = Convert.ToDouble(dtdata.Rows[d]["cstp"].ToString());



                        li.vatamt = Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString());
                        li.addtaxamt = Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                        li.cstamt = Convert.ToDouble(dtdata.Rows[d]["cstamt"].ToString());
                        li.amount = Convert.ToDouble(dtdata.Rows[d]["amount"].ToString());
                        li.descr1 = dtdata.Rows[d]["descr1"].ToString();
                        li.descr2 = dtdata.Rows[d]["descr2"].ToString();
                        li.descr3 = dtdata.Rows[d]["descr3"].ToString();
                        li.vid = Convert.ToInt64(dtdata.Rows[d]["id"].ToString());
                        li.uname = Request.Cookies["ForLogin"]["username"];
                        li.udate = System.DateTime.Now;
                        fpiclass.insertquoitemsdata(li);
                    }
                    li.quono = Convert.ToDouble(txtquotationno.Text);
                    DataTable dtdata1 = new DataTable();
                    dtdata1 = fpiclass.selectquoitemsdatafromquono(li);
                    if (dtdata1.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvslist.Visible = true;
                        gvslist.DataSource = dtdata1;
                        gvslist.DataBind();
                        lblcount.Text = dtdata1.Rows.Count.ToString();
                    }
                    else
                    {
                        gvslist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        //lblcount.Text = "0";
                    }
                    count1();
                    //this.bindgrid1();
                    gvsalesorder.Visible = false;
                    ModalPopupExtender2.Show();
                }
                else
                {
                    gvslist.Visible = false;
                    gvsoitemlistselection.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            hideimage();
            ModalPopupExtender2.Hide();
            countgv();
            Page.SetFocus(txtsono);
        }
    }

}