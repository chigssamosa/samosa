﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RepInventory : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string Xrepname = "Inventory Report";
        //string Xrepname = "Trial Balance Report";
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }

}