﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class PurchaseOrder : System.Web.UI.Page
{
    ForPurchaseOrder fpoclass = new ForPurchaseOrder();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                getpono();
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                txtpodate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspo"] = CreateTemplate();
                Page.SetFocus(txtdeliverydays);
            }
            else
            {
                //hideimage();
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                filleditdata();
            }
            //hideimage();

        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpoclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpoclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvpoitemlist.Rows[c].FindControl("imgbtnselect11");
            if (btnfinalsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpoclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void getpono()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fpoclass.selectunusedpurchaseorderno(li);
        dtvno = fpoclass.selectunusedpurchaseordernolaststring(li);
        if (dtvno.Rows.Count > 0)
        {
            txtpurchaseorderno.Text = (Convert.ToInt64(dtvno.Rows[0]["ponum"].ToString()) + 1).ToString();
        }
        else
        {
            txtpurchaseorderno.Text = "";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("vno", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("qtyremain", typeof(double));
        dtpitems.Columns.Add("qtyused", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemspo"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvpoitemlist.Visible = true;
            lblempty.Visible = false;
            gvpoitemlist.DataSource = dtsession;
            gvpoitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvpoitemlist.DataSource = null;
            gvpoitemlist.DataBind();
            gvpoitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    public void bindgrid1()
    {
        DataTable dtsession = Session["dtpitemspo"] as DataTable;
        gvsoitemlistselection.Visible = true;
        chkall.Visible = true;
        if (dtsession.Rows.Count > 0)
        {
            gvsoitemlistselection.DataSource = dtsession;
            gvsoitemlistselection.DataBind();
        }
        else
        {
            gvsoitemlistselection.DataSource = null;
            gvsoitemlistselection.DataBind();
        }
    }

    protected void gvpoitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemspo"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemspo"] = dt;
            this.bindgrid();
            countgv();
        }
        else
        {
            if (gvpoitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvpoitemlist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvpoitemlist.Rows[e.RowIndex].FindControl("lblvid");
                li.id = Convert.ToInt64(lblid.Text);
                DataTable dtcheck = new DataTable();
                dtcheck = fpoclass.selectbillinvoicedatafrompcidag(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Used in Purchase Challan No. " + dtcheck.Rows[0]["pcno"].ToString() + " So You Cant delete this Item.First delete that item from Purchase Challan then try to delete this item.');", true);
                    return;
                }
                else
                {
                    Label lblitemname = (Label)gvpoitemlist.Rows[e.RowIndex].FindControl("lblitemname");
                    Label lblqty = (Label)gvpoitemlist.Rows[e.RowIndex].FindControl("lblqty");
                    //Label lblqtyremain = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblqtyremain");
                    //Label lblqtyused = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblqtyused");
                    li.sono = Convert.ToInt64(txtsalesorder.Text);
                    li.vid = Convert.ToInt64(lblvid.Text);
                    if (li.vid != 0)
                    {
                        DataTable dtqty = new DataTable();
                        dtqty = fpoclass.selectqtyremainusedfromsono(li);
                        if (dtqty.Rows.Count > 0)
                        {
                            li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain1"].ToString()) + Convert.ToDouble(lblqty.Text);
                            li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused1"].ToString()) - Convert.ToDouble(lblqty.Text);
                            li.itemname = lblitemname.Text;
                            fpoclass.updateqtyduringdelete(li);
                        }
                    }

                    li.id = Convert.ToInt64(lblid.Text);
                    fpoclass.deletepoitemsdatafromid(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Purchase Order Item Deleted.";
                    faclass.insertactivity(li);
                    //li.istype = "CR";
                    //flclass.deleteledgerdata(li);
                    li.strpono = txtpurchaseorderno.Text;
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpoitemlist.Visible = true;
                        gvpoitemlist.DataSource = dtdata;
                        gvpoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpoitemlist.DataSource = null;
                        gvpoitemlist.DataBind();
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Purchase Order Items Found.";
                        lblcount.Text = "0";
                    }
                    Session["dtpitemspo"] = dtdata;

                    //update aall data during delete
                    countgv1();
                    li.totbasicamount = Convert.ToDouble(txtbasicamount.Text);
                    li.excisep = Convert.ToDouble(txtfexcisep.Text);
                    li.exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                    li.cstp = Convert.ToDouble(txtfcstp.Text);
                    li.cstamt = Convert.ToDouble(txtfcstamt.Text);
                    li.vatp = Convert.ToDouble(txtfvatp.Text);
                    li.vatamt = Convert.ToDouble(txtfvatamt.Text);
                    li.grandtotal = Convert.ToDouble(txtgrandtotal.Text);
                    fpoclass.updatepomasterdataduringdelete(li);
                    //

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        //count1();
        hideimage();        
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        var cc = txtdescription1.Text.IndexOf("Size:");
        if (cc != -1)
        {
        }
        else
        {
            cc = txtdescription1.Text.IndexOf("Capacity:");
            if (cc != -1)
            {
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Description 1 must contain Size or Capacity so enter size in description 1 of item " + drpitemname.SelectedItem.Text + " and try again.');", true);
                return;
            }
        }
        if (txtsalesorder.Text.Trim() == string.Empty)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Sales Order No and try again.');", true);
            return;
        }
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemspo"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["vno"] = 0;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtqty.Text;
            dr["qtyremain"] = txtqty.Text;
            dr["qtyused"] = 0;
            dr["unit"] = txtunit.Text;
            dr["rate"] = txtrate.Text;
            dr["basicamount"] = txtbasicamt.Text;
            dr["taxtype"] = txttaxtype.Text;
            dr["vatp"] = txtgvvatp.Text;
            dr["addtaxp"] = txtgvadvatp.Text;
            dr["cstp"] = txtgvcst.Text;
            dr["vatamt"] = txtgvvat.Text;
            dr["addtaxamt"] = txtgvadvat.Text;
            dr["cstamt"] = txtcstamount.Text;
            dr["amount"] = txtgvamount.Text;
            dr["descr1"] = txtdescr1.Text;
            dr["descr2"] = txtdescr2.Text;
            dr["descr3"] = txtdescr3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemspo"] = dt;
            this.bindgrid();
        }
        else
        {
            li.strpono = txtpurchaseorderno.Text;
            li.vnono = 0;
            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.qty = Convert.ToDouble(txtqty.Text);
            li.qtyremain1 = li.qty;
            li.qtyused1 = 0;
            li.unit = txtunit.Text;
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.vattype = txttaxtype.Text;
            li.vatp = Convert.ToDouble(txtgvvatp.Text);
            li.addtaxp = Convert.ToDouble(txtgvadvatp.Text);
            li.cstp = Convert.ToDouble(txtgvcst.Text);
            li.vatamt = Convert.ToDouble(txtgvvat.Text);
            li.addtaxamt = Convert.ToDouble(txtgvadvat.Text);
            li.cstamt = Convert.ToDouble(txtcstamount.Text);
            li.amount = Convert.ToDouble(txtgvamount.Text);
            li.descr1 = txtdescr1.Text;
            li.descr2 = txtdescr2.Text;
            li.descr3 = txtdescr3.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.adjqty = 0;
            fpoclass.insertpoitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strpono + " Purchase Order Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtdata = new DataTable();
            dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpoitemlist.Visible = true;
                gvpoitemlist.DataSource = dtdata;
                gvpoitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
                Session["dtpitemspo"] = dtdata;
            }
            else
            {
                gvpoitemlist.DataSource = null;
                gvpoitemlist.DataBind();
                gvpoitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
        }
        countgv();
        fillitemnamedrop();
        txtqty.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txtgvvatp.Text = string.Empty;
        txtgvvat.Text = string.Empty;
        txtgvadvatp.Text = string.Empty;
        txtgvadvat.Text = string.Empty;
        txtgvamount.Text = string.Empty;
        txtgvcst.Text = string.Empty;
        txtcstamount.Text = string.Empty;
        txtdescr1.Text = string.Empty;
        txtdescr2.Text = string.Empty;
        txtdescr3.Text = string.Empty;
        hideimage();
        Page.SetFocus(drpitemname);
    }

    protected void txtgridtaxtype_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        Label txtgvamount1 = (Label)currentRow.FindControl("lblbasicamt");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
            //lbltaxtype.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvpoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvpoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvpoitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txtbasicamount.Text = totbasicamt.ToString();
            txtfvatamt.Text = (totvat + totaddvat).ToString();
            txtfcstamt.Text = totcst.ToString();
            double excisep = 0;
            double exciseamt = 0;
            if (txtfexcisep.Text.Trim() != string.Empty)
            {
                excisep = Convert.ToDouble(txtfexcisep.Text);
                txtfexciseamt.Text = Math.Round(((totbasicamt * excisep) / 100), 2).ToString();
                exciseamt = Convert.ToDouble(txtfexciseamt.Text);
            }
            else
            {
                if (txtfexciseamt.Text.Trim() != string.Empty)
                {
                    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                    txtfexcisep.Text = Math.Round(((100 * exciseamt) / totbasicamt), 2).ToString();
                }
            }
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtfvatamt.Text) + Convert.ToDouble(txtfcstamt.Text) + exciseamt).ToString();
            //countgv();
        }
        else
        {
            txtbasicamount.Text = "0";
            txtfvatp.Text = "0";
            txtfcstp.Text = "0";
            txtfexcisep.Text = "0";
            txtfvatamt.Text = "0";
            txtfcstamt.Text = "0";
            txtfexciseamt.Text = "0";
            txtgrandtotal.Text = "0";
        }
        Page.SetFocus(txtgridcstp);
    }

    protected void txtgridcstp_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        Label txtgvamount1 = (Label)currentRow.FindControl("lblbasicamt");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        var cc1 = txtgridcstp.Text.IndexOf("-");
        if (cc1 != -1)
        {
            txtgridcstp.Text = "0";
        }
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvpoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvpoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvpoitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txtbasicamount.Text = totbasicamt.ToString();
            txtfvatamt.Text = (totvat + totaddvat).ToString();
            txtfcstamt.Text = totcst.ToString();
            txtfcstp.Text = txtgridcstp.Text;
            double excisep = 0;
            double exciseamt = 0;
            if (txtfexcisep.Text.Trim() != string.Empty)
            {
                excisep = Convert.ToDouble(txtfexcisep.Text);
                txtfexciseamt.Text = Math.Round(((totbasicamt * excisep) / 100), 2).ToString();
                exciseamt = Convert.ToDouble(txtfexciseamt.Text);
            }
            else
            {
                if (txtfexciseamt.Text.Trim() != string.Empty)
                {
                    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                    txtfexcisep.Text = Math.Round(((100 * exciseamt) / totbasicamt), 2).ToString();
                }
            }
            txtfcstp.Text = "0";
            txtfvatp.Text = "0";
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtfvatamt.Text) + Convert.ToDouble(txtfcstamt.Text) + exciseamt).ToString();
        }
        else
        {
            txtbasicamount.Text = "0";
            txtfvatp.Text = "0";
            txtfcstp.Text = "0";
            txtfexcisep.Text = "0";
            txtfvatamt.Text = "0";
            txtfcstamt.Text = "0";
            txtfexciseamt.Text = "0";
            txtgrandtotal.Text = "0";
        }
    }

    public void filleditdata()
    {
        li.strpono = Request["pono"].ToString();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        ViewState["pono"] = li.strpono;
        DataTable dtso = new DataTable();
        dtso = fpoclass.selectallPurchaseorderdatafromponostring(li);
        if (dtso.Rows.Count > 0)
        {
            txtpurchaseorderno.Text = dtso.Rows[0]["pono"].ToString();
            txtpodate.Text = dtso.Rows[0]["podate"].ToString();
            txtsalesorder.Text = dtso.Rows[0]["sono"].ToString();
            txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtdeliverydays.Text = dtso.Rows[0]["deldays"].ToString();
            txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
            txtfollowupdate.Text = Convert.ToDateTime(dtso.Rows[0]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
            txtfollowupdetails.Text = dtso.Rows[0]["followupdetails"].ToString();
            txtfollowupdetails1.Text = dtso.Rows[0]["followupdetails1"].ToString();
            txtbasicamount.Text = dtso.Rows[0]["totbasic"].ToString();
            txtfexcisep.Text = dtso.Rows[0]["excisep"].ToString();
            txtfexciseamt.Text = dtso.Rows[0]["exciseamt"].ToString();
            txtfcstp.Text = dtso.Rows[0]["cstp"].ToString();
            txtfcstamt.Text = dtso.Rows[0]["cstamt"].ToString();
            txtfvatp.Text = dtso.Rows[0]["vatp"].ToString();
            txtfvatamt.Text = dtso.Rows[0]["vatamt"].ToString();
            txtgrandtotal.Text = dtso.Rows[0]["grandtotal"].ToString();
            txtdeliverysch.Text = dtso.Rows[0]["delivery"].ToString();
            txtpayment.Text = dtso.Rows[0]["payment"].ToString();
            txtdeliveryat.Text = dtso.Rows[0]["deliveryat"].ToString();
            txttaxes.Text = dtso.Rows[0]["taxes"].ToString();
            txtoctroi.Text = dtso.Rows[0]["octroi"].ToString();
            txtexcise.Text = dtso.Rows[0]["excise"].ToString();
            txtform.Text = dtso.Rows[0]["form"].ToString();
            txttransportation.Text = dtso.Rows[0]["transportation"].ToString();
            txtinsurance.Text = dtso.Rows[0]["insurance"].ToString();
            txtpacking.Text = dtso.Rows[0]["packing"].ToString();
            txtdescription1.Text = dtso.Rows[0]["descr1"].ToString();
            txtdescription2.Text = dtso.Rows[0]["descr2"].ToString();
            txtdescription3.Text = dtso.Rows[0]["descr3"].ToString();
            txtdescription4.Text = dtso.Rows[0]["descr4"].ToString();
            txtdescription5.Text = dtso.Rows[0]["descr5"].ToString();
            txtdescription6.Text = dtso.Rows[0]["descr6"].ToString();
            drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
            drpordertype.SelectedValue = dtso.Rows[0]["type"].ToString();
            txtuser.Text = dtso.Rows[0]["uname"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpoitemlist.Visible = true;
                gvpoitemlist.DataSource = dtdata;
                gvpoitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvpoitemlist.DataSource = null;
                gvpoitemlist.DataBind();
                gvpoitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
            btnfinalsave.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsono(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsono(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getunit(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallunit(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(txtfollowupdate);
    }

    public void count1()
    {
        if (txtqty.Text.Trim() == string.Empty)
        {
            txtqty.Text = "0";
        }
        if (txtrate.Text.Trim() == string.Empty)
        {
            txtrate.Text = "0";
        }
        txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtbasicamt.Text);
        vatp = Convert.ToDouble(txtgvvatp.Text);
        addvatp = Convert.ToDouble(txtgvadvatp.Text);
        txtgvvat.Text = vatp.ToString();
        txtgvadvat.Text = addvatp.ToString();
        if (txtqty.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
        if (txtbasicamt.Text.Trim() != string.Empty)
        {
            baseamt = Convert.ToDouble(txtbasicamt.Text);
        }

        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvatp.Text = vatp.ToString();
                txtgvadvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}

            }
            else
            {
                txtgvvatp.Text = "0";
                txtgvadvatp.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvatp.Text = "0";
            txtgvadvatp.Text = "0";
        }

        if (txtgvvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvatp.Text);
            txtgvvat.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvatp.Text = "0";
            txtgvvat.Text = "0";
        }
        if (txtgvadvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadvatp.Text);
            txtgvadvat.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadvatp.Text = "0";
            txtgvadvat.Text = "0";
        }

        if (Convert.ToDouble(txtgvvat.Text) != 0)
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        else
        {
            if (txtgvcst.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgvcst.Text);
                txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgvcst.Text = "0";
                txtcstamount.Text = "0";
            }
        }

        vatamt = Convert.ToDouble(txtgvvat.Text);
        addvatamt = Convert.ToDouble(txtgvadvat.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        txtfvatamt.Text = (vatamt + addvatamt).ToString();
        if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        {
            vatp = Convert.ToDouble(txtfvatp.Text);
            txtfvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        else
        {
            if (txtfvatamt.Text.Trim() != string.Empty)
            {
                vatamt = Convert.ToDouble(txtfvatamt.Text);
                txtfvatp.Text = Math.Round(((100 * vatamt) / baseamt), 2).ToString();
            }
        }
        if (txtfcstp.Text.Trim() != string.Empty && txtfcstp.Text.Trim() != "0")
        {
            cstp = Convert.ToDouble(txtfcstp.Text);
            txtfcstamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        else
        {
            if (txtfcstamt.Text.Trim() != string.Empty)
            {
                cstamt = Convert.ToDouble(txtfcstamt.Text);
                txtfcstp.Text = Math.Round(((100 * cstamt) / baseamt), 2).ToString();
            }
        }
        if (gvpoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
            {
                Label lblamount = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
                totamount = totamount + Convert.ToDouble(lblamount.Text);
            }
            txtbasicamount.Text = totamount.ToString();
            //txtfvat.Text = (totvat + totaddvat).ToString();
            //txtadvat.Text = totaddvat.ToString();
            if (txtfexcisep.Text.Trim() == string.Empty)
            {
                txtfexcisep.Text = "0";
            }
            if (txtfexciseamt.Text.Trim() == string.Empty)
            {
                txtfexciseamt.Text = "0";
            }
            if (txtfcstp.Text.Trim() == string.Empty)
            {
                txtfcstp.Text = "0";
            }
            if (txtfcstamt.Text.Trim() == string.Empty)
            {
                txtfcstamt.Text = "0";
            }
            if (txtfvatp.Text.Trim() == string.Empty)
            {
                txtfvatp.Text = "0";
            }
            if (txtfvatamt.Text.Trim() == string.Empty)
            {
                txtfvatamt.Text = "0";
            }
            txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtfexciseamt.Text) + Convert.ToDouble(txtfcstamt.Text) + Convert.ToDouble(txtfvatamt.Text)).ToString();
        }
        else
        {
            txtbasicamount.Text = "0";
            //txtfvat.Text = "0";
            //txtadvat.Text = "0";
            //txtcst.Text = "0";
            txtgrandtotal.Text = "0";
        }

    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemspo"];
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double amount = 0;
        for (int c = 0; c < dtsoitem.Rows.Count; c++)
        {
            baseamt = baseamt + Convert.ToDouble(dtsoitem.Rows[c]["qty"].ToString()) * Convert.ToDouble(dtsoitem.Rows[c]["rate"].ToString());
            amount = amount + Convert.ToDouble(dtsoitem.Rows[c]["amount"].ToString());
            vatamt = vatamt + Convert.ToDouble(dtsoitem.Rows[c]["vatamt"].ToString());
            addvatamt = addvatamt + Convert.ToDouble(dtsoitem.Rows[c]["addtaxamt"].ToString());
            if (dtsoitem.Rows[c]["cstp"].ToString() != "")
            {
                cstp = cstp + Convert.ToDouble(dtsoitem.Rows[c]["cstp"].ToString());
            }
            if (dtsoitem.Rows[c]["cstamt"].ToString() != "")
            {
                cstamt = cstamt + Convert.ToDouble(dtsoitem.Rows[c]["cstamt"].ToString());
            }
            if (dtsoitem.Rows[c]["vatp"].ToString() != "")
            {
                vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["vatp"].ToString());
            }
            if (dtsoitem.Rows[c]["addtaxp"].ToString() != "")
            {
                vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["addtaxp"].ToString());
            }

        }
        txtbasicamount.Text = baseamt.ToString();
        txtfexcisep.Text = "0";
        txtfexciseamt.Text = "0";
        txtfcstp.Text = cstp.ToString();
        txtfcstamt.Text = cstamt.ToString();
        txtfvatp.Text = vatp.ToString();
        txtfvatamt.Text = (vatamt + addvatamt).ToString();
        txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtfexciseamt.Text) + Convert.ToDouble(txtfcstamt.Text) + Convert.ToDouble(txtfvatamt.Text)).ToString();
    }

    public void countgv1()
    {
        //DataTable dtsoitem = (DataTable)Session["dtpitems"];
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double fvatamt = 0;
        double faddvatamt = 0;
        double fcstamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double fbasicamt = 0;
        double amount = 0;
        for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
        {
            TextBox txtgridqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
            TextBox txtgridrate = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgvrate");
            Label lblbasicamt = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
            Label lblvatp = (Label)gvpoitemlist.Rows[c].FindControl("lblvatp");
            Label lbladdvatp = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatp");
            //Label lblcstp = (Label)gvpoitemlist.Rows[c].FindControl("lblcstp");
            TextBox txtgridcstp = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridcstp");
            Label lblvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblcstamt = (Label)gvpoitemlist.Rows[c].FindControl("lblcstamt");
            Label lblamount = (Label)gvpoitemlist.Rows[c].FindControl("lblamount");
            baseamt = Convert.ToDouble(txtgridqty.Text) * Convert.ToDouble(txtgridrate.Text);
            lblbasicamt.Text = baseamt.ToString();
            vatamt = ((Convert.ToDouble(lblvatp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            addvatamt = ((Convert.ToDouble(lbladdvatp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            cstamt = ((Convert.ToDouble(txtgridcstp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            lblvatamt.Text = vatamt.ToString();
            lbladdvatamt.Text = addvatamt.ToString();
            lblcstamt.Text = cstamt.ToString();
            amount = amount + baseamt + vatamt + addvatamt + cstamt;
            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
            fbasicamt = fbasicamt + baseamt;
            fvatamt = fvatamt + vatamt;
            faddvatamt = faddvatamt + addvatamt;
            fcstamt = fcstamt + cstamt;
            vatp = Convert.ToDouble(lblvatp.Text) + Convert.ToDouble(lbladdvatp.Text);
            //if (dtsoitem.Rows[c]["cstp"].ToString() != "")
            //{
            //    cstp = cstp + Convert.ToDouble(dtsoitem.Rows[c]["cstp"].ToString());
            //}
            //if (dtsoitem.Rows[c]["cstamt"].ToString() != "")
            //{
            //    cstamt = cstamt + Convert.ToDouble(dtsoitem.Rows[c]["cstamt"].ToString());
            //}
            //if (dtsoitem.Rows[c]["vatp"].ToString() != "")
            //{
            //    vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["vatp"].ToString());
            //}
            //if (dtsoitem.Rows[c]["addtaxp"].ToString() != "")
            //{
            //    vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["addtaxp"].ToString());
            //}

        }
        txtbasicamount.Text = fbasicamt.ToString();
        txtfexcisep.Text = "0";
        txtfexciseamt.Text = "0";
        txtfcstp.Text = cstp.ToString();
        txtfcstamt.Text = fcstamt.ToString();
        txtfvatp.Text = vatp.ToString();
        //txtfvatamt.Text = (vatamt + addvatamt).ToString();
        txtfvatamt.Text = (fvatamt + faddvatamt).ToString();
        txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtfexciseamt.Text) + Convert.ToDouble(txtfcstamt.Text) + Convert.ToDouble(txtfvatamt.Text)).ToString();
    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fpoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["vattype"].ToString();
                string vattype = dtdata.Rows[0]["vattype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvatp.Text = vattype.Split('-')[0];
                    txtgvvat.Text = "0";
                    txtgvadvatp.Text = vattype.Split('-')[1];
                    txtgvadvat.Text = "0";
                    count1();
                }
            }
            else
            {
                txtqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
    }

    protected void btnfinalsave_Click(object sender, EventArgs e)
    {
        for (int f = 0; f < gvpoitemlist.Rows.Count; f++)
        {
            Label lblitemname = (Label)gvpoitemlist.Rows[f].FindControl("lblitemname");
            TextBox txtgvdesc1 = (TextBox)gvpoitemlist.Rows[f].FindControl("txtdesc1");
            var cc = txtgvdesc1.Text.IndexOf("Size:");
            if (cc != -1)
            {
            }
            else
            {
                cc = txtgvdesc1.Text.IndexOf("Capacity:");
                if (cc != -1)
                {
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Description 1 must contain Size or Capacity so enter size in description 1 of item " + lblitemname.Text + " and try again.');", true);
                    return;
                }
            }
        }
        for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
        {
            Label lblvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblsctamt = (Label)gvpoitemlist.Rows[c].FindControl("lblcstamt");
            if ((Convert.ToDouble(lblvatamt.Text) > 0 || Convert.ToDouble(lbladdvatamt.Text) > 0) && (Convert.ToDouble(lblsctamt.Text) > 0))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Combination of both S/C GST And IGST not possible.Try Again.');", true);
                return;
            }
        }
        li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.podate <= yyyear1 && li.podate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.strpono = txtpurchaseorderno.Text;
        li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.deldays = Convert.ToInt64(txtdeliverydays.Text);
        li.ccode = Convert.ToInt64(txtclientcode.Text);
        li.followupdate = Convert.ToDateTime(txtfollowupdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.acname = drpacname.SelectedItem.Text;
        li.followupdetails = txtfollowupdetails.Text;
        li.followupdetails1 = txtfollowupdetails1.Text;
        if (txtsalesorder.Text.Trim() != string.Empty)
        {
            li.sono = Convert.ToInt64(txtsalesorder.Text);
        }
        else
        {
            li.sono = 0;
        }
        li.totbasicamount = Convert.ToDouble(txtbasicamount.Text);
        li.excisep = Convert.ToDouble(txtfexcisep.Text);
        li.exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        li.cstp = Convert.ToDouble(txtfcstp.Text);
        li.cstamt = Convert.ToDouble(txtfcstamt.Text);
        li.vatp = Convert.ToDouble(txtfvatp.Text);
        li.vatamt = Convert.ToDouble(txtfvatamt.Text);
        li.grandtotal = Convert.ToDouble(txtgrandtotal.Text);
        li.deliverysch = txtdeliverysch.Text;
        li.payment = txtpayment.Text;
        li.deliveryat = txtdeliveryat.Text;
        li.taxes = txttaxes.Text;
        li.octroi = txtoctroi.Text;
        li.excise = txtexcise.Text;
        li.form = txtform.Text;
        li.transportation = txttransportation.Text;
        li.insurance = txtinsurance.Text;
        li.packing = txtpacking.Text;
        li.descr1 = txtdescription1.Text;
        li.descr2 = txtdescription2.Text;
        li.descr3 = txtdescription3.Text;
        li.descr4 = txtdescription4.Text;
        li.descr5 = txtdescription5.Text;
        li.descr6 = txtdescription6.Text;
        li.status = drpstatus.SelectedItem.Text;
        li.type = drpordertype.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnfinalsave.Text == "Save")
        {
            if (gvpoitemlist.Rows.Count > 0)
            {
                DataTable dtcheck = new DataTable();
                dtcheck = fpoclass.selectallPurchaseorderdatafromponostring(li);
                if (dtcheck.Rows.Count == 0)
                {
                    fpoclass.insertpomasterdata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.strpono + " Purchase Order Inserted.";
                    faclass.insertactivity(li);
                }
                else
                {
                    li.strpono = txtpurchaseorderno.Text;
                    fpoclass.updateisused(li);
                    getpono();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Order No. already exist.Try Again.');", true);
                    return;
                }
                //li.pono = Convert.ToInt64(SessionMgt.voucherno);
                li.strpono = txtpurchaseorderno.Text;
                for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
                {
                    Label lblid = (Label)gvpoitemlist.Rows[c].FindControl("lblid");
                    Label lblvno = (Label)gvpoitemlist.Rows[c].FindControl("lblvno");
                    Label lblitemname = (Label)gvpoitemlist.Rows[c].FindControl("lblitemname");
                    // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                    TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                    Label lblqtyremain = (Label)gvpoitemlist.Rows[c].FindControl("lblqtyremain");
                    Label lblqtyused = (Label)gvpoitemlist.Rows[c].FindControl("lblqtyused");
                    TextBox lblunit = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgvunit");
                    Label lblrate = (Label)gvpoitemlist.Rows[c].FindControl("lblrate");
                    TextBox txtgvrate = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgvrate");
                    Label lblbasicamt = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
                    TextBox lbltaxtype = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)gvpoitemlist.Rows[c].FindControl("lblvatp");
                    Label lblvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
                    Label lbladdvatp = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridcstp");
                    Label lblsctamt = (Label)gvpoitemlist.Rows[c].FindControl("lblcstamt");
                    Label lblamount = (Label)gvpoitemlist.Rows[c].FindControl("lblamount");
                    TextBox lbldesc1 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc3");
                    li.qty = Convert.ToDouble(lblqty.Text);
                    //if (li.qty > 0)
                    //{
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
                    li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
                    li.qtyremain1 = li.qty;
                    li.qtyused1 = 0;
                    li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(txtgvrate.Text);
                    li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                    li.vattype = lbltaxtype.Text;
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                    li.vatamt = Convert.ToDouble(lblvatamt.Text);
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                    if (lblcstp.Text != string.Empty)
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.cstamt = Convert.ToDouble(lblsctamt.Text);
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    li.vnono = Convert.ToInt64(txtsalesorder.Text);
                    li.vid = Convert.ToInt64(lblid.Text);
                    li.adjqty = 0;
                    fpoclass.insertpoitemsdata(li);
                    li.id = Convert.ToInt64(lblid.Text);
                    li.vnono1 = Convert.ToInt64(lblvno.Text);
                    fpoclass.updateremainqtyinsalesorder(li);
                    //}
                }
                fpoclass.updateisused(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter purchase order items and Try Again.');", true);
                return;
            }
        }
        else
        {
            if (ViewState["pono"].ToString().Trim() != txtpurchaseorderno.Text.Trim())
            {
                li.strpono1 = txtpurchaseorderno.Text;
                li.strpono = ViewState["pono"].ToString();
                fpoclass.updatepomasterdataupdatepo(li);
                fpoclass.updatepoitemsdataupdatepo(li);
                fpoclass.deletepomasterdatastring(li);
                fpoclass.deletepoitemsdatafromponostring(li);
            }
            li.strpono = txtpurchaseorderno.Text;
            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            fpoclass.updatepomasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            fpoclass.updatepoitemsdatedataupdatepo(li);

            //update all item data together
            li.strpono = txtpurchaseorderno.Text;
            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            for (int y = 0; y < gvpoitemlist.Rows.Count; y++)
            {
                Label lblvatamt1 = (Label)gvpoitemlist.Rows[y].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatamt");
                Label lblsctamt1 = (Label)gvpoitemlist.Rows[y].FindControl("lblcstamt");
                if ((Convert.ToDouble(lblvatamt1.Text) > 0 || Convert.ToDouble(lbladdvatamt1.Text) > 0) && (Convert.ToDouble(lblsctamt1.Text) > 0))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Combination of both S/C GST And IGST not possible.Try Again.');", true);
                    return;
                }
                Label lblid = (Label)gvpoitemlist.Rows[y].FindControl("lblid");
                Label lblqty = (Label)gvpoitemlist.Rows[y].FindControl("lblqty");
                TextBox txtgvqty1 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridqty");
                li.id = Convert.ToInt64(lblid.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                DataTable dtremain = new DataTable();
                dtremain = fpoclass.selectqtyremainusedfromsono(li);
                if (Convert.ToDouble(txtgvqty1.Text) > 0)
                {
                    if (dtremain.Rows.Count > 0)
                    {
                        //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain1"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                        li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused1"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                        fpoclass.updateremainqtyinsalesorder(li);

                        //update poitems data
                        //for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
                        //{
                        Label lblitemname = (Label)gvpoitemlist.Rows[y].FindControl("lblitemname");
                        // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                        //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblunit = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvunit");
                        Label lblrate = (Label)gvpoitemlist.Rows[y].FindControl("lblrate");
                        TextBox txtgvrate = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvrate");
                        Label lblbasicamt = (Label)gvpoitemlist.Rows[y].FindControl("lblbasicamt");
                        TextBox lbltaxtype = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvpoitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvpoitemlist.Rows[y].FindControl("lblcstamt");
                        Label lblamount = (Label)gvpoitemlist.Rows[y].FindControl("lblamount");
                        TextBox lbldesc1 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc3");
                        li.qty = Convert.ToDouble(lblqty.Text);
                        li.id = Convert.ToInt64(lblid.Text);
                        //if (li.qty > 0)
                        //{
                        li.itemname = lblitemname.Text;
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        li.unit = lblunit.Text;
                        li.rate = Convert.ToDouble(txtgvrate.Text);
                        li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                        li.vattype = lbltaxtype.Text;
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                        li.vatamt = Convert.ToDouble(lblvatamt.Text);
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                        if (lblcstp.Text != "")
                        {
                            li.cstp = Convert.ToDouble(lblcstp.Text);
                        }
                        else
                        {
                            li.cstp = 0;
                        }
                        if (lblsctamt.Text != "")
                        {
                            li.cstamt = Convert.ToDouble(lblsctamt.Text);
                        }
                        else
                        {
                            li.cstamt = 0;
                        }
                        li.amount = Convert.ToDouble(lblamount.Text);
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        fpoclass.updatepoitemsdata(li);
                        //}
                        //}

                    }
                    else
                    {
                        Label lblitemname = (Label)gvpoitemlist.Rows[y].FindControl("lblitemname");
                        // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                        //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblunit = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvunit");
                        Label lblrate = (Label)gvpoitemlist.Rows[y].FindControl("lblrate");
                        TextBox txtgvrate = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvrate");
                        Label lblbasicamt = (Label)gvpoitemlist.Rows[y].FindControl("lblbasicamt");
                        TextBox lbltaxtype = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvpoitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvpoitemlist.Rows[y].FindControl("lblcstamt");
                        Label lblamount = (Label)gvpoitemlist.Rows[y].FindControl("lblamount");
                        TextBox lbldesc1 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc3");
                        li.qty = Convert.ToDouble(lblqty.Text);
                        li.id = Convert.ToInt64(lblid.Text);
                        //if (li.qty > 0)
                        //{
                        li.itemname = lblitemname.Text;
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        li.unit = lblunit.Text;
                        li.rate = Convert.ToDouble(txtgvrate.Text);
                        li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                        li.vattype = lbltaxtype.Text;
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                        li.vatamt = Convert.ToDouble(lblvatamt.Text);
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                        li.cstamt = Convert.ToDouble(lblsctamt.Text);
                        li.amount = Convert.ToDouble(lblamount.Text);
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        fpoclass.updatepoitemsdata(li);
                    }
                }
                else
                {
                    Label lblitemname = (Label)gvpoitemlist.Rows[y].FindControl("lblitemname");
                    // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                    //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblunit = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvunit");
                    Label lblrate = (Label)gvpoitemlist.Rows[y].FindControl("lblrate");
                    TextBox txtgvrate = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgvrate");
                    Label lblbasicamt = (Label)gvpoitemlist.Rows[y].FindControl("lblbasicamt");
                    TextBox lbltaxtype = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)gvpoitemlist.Rows[y].FindControl("lblvatp");
                    Label lblvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lblvatamt");
                    Label lbladdvatp = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)gvpoitemlist.Rows[y].FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)gvpoitemlist.Rows[y].FindControl("txtgridcstp");
                    Label lblsctamt = (Label)gvpoitemlist.Rows[y].FindControl("lblcstamt");
                    Label lblamount = (Label)gvpoitemlist.Rows[y].FindControl("lblamount");
                    TextBox lbldesc1 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)gvpoitemlist.Rows[y].FindControl("txtdesc3");
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    //if (li.qty > 0)
                    //{
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(txtgvrate.Text);
                    li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                    li.vattype = lbltaxtype.Text;
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                    li.vatamt = Convert.ToDouble(lblvatamt.Text);
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                    li.cstamt = Convert.ToDouble(lblsctamt.Text);
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpoclass.updatepoitemsdata(li);
                }
            }
            //

            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strpono + " Purchase Order Updated.";
            faclass.insertactivity(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            //for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
            //{
            //    Label lblid = (Label)gvpoitemlist.Rows[c].FindControl("lblid");
            //    Label lblitemname = (Label)gvpoitemlist.Rows[c].FindControl("lblitemname");
            //    // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
            //    TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
            //    TextBox lblunit = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgvunit");
            //    Label lblrate = (Label)gvpoitemlist.Rows[c].FindControl("lblrate");
            //    Label lblbasicamt = (Label)gvpoitemlist.Rows[c].FindControl("lblbasicamt");
            //    Label lblvatp = (Label)gvpoitemlist.Rows[c].FindControl("lblvatp");
            //    Label lblvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lblvatamt");
            //    Label lbladdvatp = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatp");
            //    Label lbladdvatamt = (Label)gvpoitemlist.Rows[c].FindControl("lbladdvatamt");
            //    Label lblamount = (Label)gvpoitemlist.Rows[c].FindControl("lblamount");
            //    TextBox lbldesc1 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc1");
            //    TextBox lbldesc2 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc2");
            //    TextBox lbldesc3 = (TextBox)gvpoitemlist.Rows[c].FindControl("txtdesc3");
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    li.id = Convert.ToInt64(lblid.Text);
            //    if (li.qty > 0)
            //    {
            //        li.itemname = lblitemname.Text;
            //        li.qty = Convert.ToDouble(lblqty.Text);
            //        li.unit = txtunit.Text;
            //        li.rate = Convert.ToDouble(lblrate.Text);
            //        li.basicamount = Convert.ToDouble(lblbasicamt.Text);
            //        li.vatp = Convert.ToDouble(lblvatp.Text);
            //        li.vatamt = Convert.ToDouble(lblvatamt.Text);
            //        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
            //        li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
            //        li.amount = Convert.ToDouble(lblamount.Text);
            //        li.descr1 = lbldesc1.Text;
            //        li.descr2 = lbldesc2.Text;
            //        li.descr3 = lbldesc3.Text;
            //        fpoclass.updatepoitemsdata(li);
            //    }
            //}
        }
        Response.Redirect("~/PurchaseOrderList.aspx?pagename=PurchaseOrderList");
    }

    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvatp);
    }

    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtunit);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvatp_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtgvadvatp_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtgvadvat_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtgvamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtbasicamount_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        //if (txtfexcisep.Text.Trim() != string.Empty && txtfexcisep.Text.Trim() != "0")
        //{
        //    excisep = Convert.ToDouble(txtfexcisep.Text);
        //    txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
        //    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //}
        //else
        //{
        //    if (txtfexciseamt.Text.Trim() != string.Empty)
        //    {
        //        exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //        txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfcstp.Text.Trim() != string.Empty && txtfcstp.Text.Trim() != "0")
        //{
        //    cstp = Convert.ToDouble(txtfcstp.Text);
        //    txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
        //    cstamt = Convert.ToDouble(txtfcstamt.Text);
        //}
        //else
        //{
        //    if (txtfcstamt.Text.Trim() != string.Empty)
        //    {
        //        cstamt = Convert.ToDouble(txtfcstamt.Text);
        //        txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        //{
        //    vatp = Convert.ToDouble(txtfvatp.Text);
        //    txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
        //    vatamt = Convert.ToDouble(txtfvatamt.Text);
        //}
        //else
        //{
        //    if (txtfvatamt.Text.Trim() != string.Empty)
        //    {
        //        vatamt = Convert.ToDouble(txtfvatamt.Text);
        //        txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
        //    }
        //}
        if (txtfexciseamt.Text.Trim() != string.Empty)
        {
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        if (txtfcstamt.Text.Trim() != string.Empty)
        {
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        if (txtfvatamt.Text.Trim() != string.Empty)
        {
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        txtgrandtotal.Text = (basicamt + exciseamt + cstamt + vatamt).ToString();
    }
    protected void txtfexcisep_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        if (txtfexcisep.Text.Trim() != string.Empty)
        {
            excisep = Convert.ToDouble(txtfexcisep.Text);
            txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        else
        {
            if (txtfexciseamt.Text.Trim() != string.Empty)
            {
                exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
            }
        }
        basicamt = basicamt + exciseamt;
        if (txtfcstp.Text.Trim() != string.Empty)
        {
            cstp = Convert.ToDouble(txtfcstp.Text);
            txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        else
        {
            if (txtfcstamt.Text.Trim() != string.Empty)
            {
                cstamt = Convert.ToDouble(txtfcstamt.Text);
                txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
            }
        }
        if (txtfvatp.Text.Trim() != string.Empty)
        {
            vatp = Convert.ToDouble(txtfvatp.Text);
            txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        else
        {
            if (txtfvatamt.Text.Trim() != string.Empty)
            {
                vatamt = Convert.ToDouble(txtfvatamt.Text);
                txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
            }
        }
        txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtfexciseamt);
    }
    protected void txtfexciseamt_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        //if (txtfexcisep.Text.Trim() != string.Empty && txtfexcisep.Text.Trim() != "0")
        //{
        //    excisep = Convert.ToDouble(txtfexcisep.Text);
        //    txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
        //    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //}
        //else
        //{
        //    if (txtfexciseamt.Text.Trim() != string.Empty)
        //    {
        //        exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //        txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfcstp.Text.Trim() != string.Empty && txtfcstp.Text.Trim() != "0")
        //{
        //    cstp = Convert.ToDouble(txtfcstp.Text);
        //    txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
        //    cstamt = Convert.ToDouble(txtfcstamt.Text);
        //}
        //else
        //{
        //    if (txtfcstamt.Text.Trim() != string.Empty)
        //    {
        //        cstamt = Convert.ToDouble(txtfcstamt.Text);
        //        txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        //{
        //    vatp = Convert.ToDouble(txtfvatp.Text);
        //    txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
        //    vatamt = Convert.ToDouble(txtfvatamt.Text);
        //}
        //else
        //{
        //    if (txtfvatamt.Text.Trim() != string.Empty)
        //    {
        //        vatamt = Convert.ToDouble(txtfvatamt.Text);
        //        txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
        //    }
        //}
        if (txtfexciseamt.Text.Trim() != string.Empty)
        {
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        if (txtfcstamt.Text.Trim() != string.Empty)
        {
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        if (txtfvatamt.Text.Trim() != string.Empty)
        {
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        txtgrandtotal.Text = (basicamt + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtfcstp);
    }
    protected void txtfcstp_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        if (txtfexcisep.Text.Trim() != string.Empty && txtfexcisep.Text.Trim() != "0")
        {
            excisep = Convert.ToDouble(txtfexcisep.Text);
            txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        else
        {
            if (txtfexciseamt.Text.Trim() != string.Empty)
            {
                exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
            }
        }
        basicamt = basicamt + exciseamt;
        if (txtfcstp.Text.Trim() != string.Empty)
        {
            cstp = Convert.ToDouble(txtfcstp.Text);
            txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        else
        {
            if (txtfcstamt.Text.Trim() != string.Empty)
            {
                cstamt = Convert.ToDouble(txtfcstamt.Text);
                txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
            }
        }
        if (txtfvatp.Text.Trim() != string.Empty)
        {
            vatp = Convert.ToDouble(txtfvatp.Text);
            txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        else
        {
            if (txtfvatamt.Text.Trim() != string.Empty)
            {
                vatamt = Convert.ToDouble(txtfvatamt.Text);
                txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
            }
        }
        txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtfcstamt);
    }
    protected void txtfcstamt_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        //if (txtfexcisep.Text.Trim() != string.Empty && txtfexcisep.Text.Trim() != "0")
        //{
        //    excisep = Convert.ToDouble(txtfexcisep.Text);
        //    txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
        //    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //}
        //else
        //{
        //    if (txtfexciseamt.Text.Trim() != string.Empty)
        //    {
        //        exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //        txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfcstp.Text.Trim() != string.Empty && txtfcstp.Text.Trim() != "0")
        //{
        //    cstp = Convert.ToDouble(txtfcstp.Text);
        //    txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
        //    cstamt = Convert.ToDouble(txtfcstamt.Text);
        //}
        //else
        //{
        //    if (txtfcstamt.Text.Trim() != string.Empty)
        //    {
        //        cstamt = Convert.ToDouble(txtfcstamt.Text);
        //        txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        //{
        //    vatp = Convert.ToDouble(txtfvatp.Text);
        //    txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
        //    vatamt = Convert.ToDouble(txtfvatamt.Text);
        //}
        //else
        //{
        //    if (txtfvatamt.Text.Trim() != string.Empty)
        //    {
        //        vatamt = Convert.ToDouble(txtfvatamt.Text);
        //        txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
        //    }
        //}
        if (txtfexciseamt.Text.Trim() != string.Empty)
        {
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        if (txtfcstamt.Text.Trim() != string.Empty)
        {
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        if (txtfvatamt.Text.Trim() != string.Empty)
        {
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        txtgrandtotal.Text = (basicamt + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtfvatp);
    }
    protected void txtfvatp_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        if (txtfexcisep.Text.Trim() != string.Empty)
        {
            excisep = Convert.ToDouble(txtfexcisep.Text);
            txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        else
        {
            if (txtfexciseamt.Text.Trim() != string.Empty)
            {
                exciseamt = Convert.ToDouble(txtfexciseamt.Text);
                txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
            }
        }
        basicamt = basicamt + exciseamt;
        if (txtfcstp.Text.Trim() != string.Empty)
        {
            cstp = Convert.ToDouble(txtfcstp.Text);
            txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        else
        {
            if (txtfcstamt.Text.Trim() != string.Empty)
            {
                cstamt = Convert.ToDouble(txtfcstamt.Text);
                txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
            }
        }
        if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        {
            vatp = Convert.ToDouble(txtfvatp.Text);
            txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        else
        {
            if (txtfvatamt.Text.Trim() != string.Empty)
            {
                vatamt = Convert.ToDouble(txtfvatamt.Text);
                txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
            }
        }
        txtgrandtotal.Text = (Convert.ToDouble(txtbasicamount.Text) + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtfvatamt);
    }
    protected void txtfvatamt_TextChanged(object sender, EventArgs e)
    {
        double basicamt = 0;
        double excisep = 0;
        double exciseamt = 0;
        double cstp = 0;
        double cstamt = 0;
        double vatp = 0;
        double vatamt = 0;
        if (txtbasicamount.Text.Trim() != string.Empty)
        {
            basicamt = Convert.ToDouble(txtbasicamount.Text);
        }
        //if (txtfexcisep.Text.Trim() != string.Empty && txtfexcisep.Text.Trim() != "0")
        //{
        //    excisep = Convert.ToDouble(txtfexcisep.Text);
        //    txtfexciseamt.Text = Math.Round(((basicamt * excisep) / 100), 2).ToString();
        //    exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //}
        //else
        //{
        //    if (txtfexciseamt.Text.Trim() != string.Empty)
        //    {
        //        exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        //        txtfexcisep.Text = Math.Round(((100 * exciseamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfcstp.Text.Trim() != string.Empty && txtfcstp.Text.Trim() != "0")
        //{
        //    cstp = Convert.ToDouble(txtfcstp.Text);
        //    txtfcstamt.Text = Math.Round(((basicamt * cstp) / 100), 2).ToString();
        //    cstamt = Convert.ToDouble(txtfcstamt.Text);
        //}
        //else
        //{
        //    if (txtfcstamt.Text.Trim() != string.Empty)
        //    {
        //        cstamt = Convert.ToDouble(txtfcstamt.Text);
        //        txtfcstp.Text = Math.Round(((100 * cstamt) / basicamt), 2).ToString();
        //    }
        //}
        //if (txtfvatp.Text.Trim() != string.Empty && txtfvatp.Text.Trim() != "0")
        //{
        //    vatp = Convert.ToDouble(txtfvatp.Text);
        //    txtfvatamt.Text = Math.Round(((basicamt * vatp) / 100), 2).ToString();
        //    vatamt = Convert.ToDouble(txtfvatamt.Text);
        //}
        //else
        //{
        //    if (txtfvatamt.Text.Trim() != string.Empty)
        //    {
        //        vatamt = Convert.ToDouble(txtfvatamt.Text);
        //        txtfvatp.Text = Math.Round(((100 * vatamt) / basicamt), 2).ToString();
        //    }
        //}
        if (txtfexciseamt.Text.Trim() != string.Empty)
        {
            exciseamt = Convert.ToDouble(txtfexciseamt.Text);
        }
        if (txtfcstamt.Text.Trim() != string.Empty)
        {
            cstamt = Convert.ToDouble(txtfcstamt.Text);
        }
        if (txtfvatamt.Text.Trim() != string.Empty)
        {
            vatamt = Convert.ToDouble(txtfvatamt.Text);
        }
        txtgrandtotal.Text = (basicamt + exciseamt + cstamt + vatamt).ToString();
        Page.SetFocus(txtgrandtotal);
    }
    protected void txtsalesorder_TextChanged(object sender, EventArgs e)
    {
        if (txtsalesorder.Text.Trim() != string.Empty)
        {
            Session["dtpitemspo"] = null;
            Session["dtpitemspo"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsalesorder.Text);
            DataTable dtdt = new DataTable();
            dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
            if (dtdt.Rows.Count > 0)
            {
                drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
            }
            DataTable dtdata = new DataTable();
            dtdata = fpoclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                //lblempty.Visible = false;
                //gvpoitemlist.Visible = true;
                //gvpoitemlist.DataSource = dtdata;
                //gvpoitemlist.DataBind();
                //Session["dtpitems"] = dtdata;
                for (int d = 0; d < dtdata.Rows.Count; d++)
                {
                    DataTable dt = (DataTable)Session["dtpitemspo"];
                    DataRow dr = dt.NewRow();
                    dr["id"] = dtdata.Rows[d]["id"].ToString();
                    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                    dr["qtyremain"] = dtdata.Rows[d]["qtyremain1"].ToString();
                    dr["qtyused"] = dtdata.Rows[d]["qtyused1"].ToString();
                    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                    dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                    dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                    dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                    dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                    dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                    dt.Rows.Add(dr);
                    Session["dtpitemspo"] = dt;
                    this.bindgrid();
                }
                countgv();
            }
            else
            {
                gvpoitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
        }
        else
        {
            gvpoitemlist.Visible = false;
            lblempty.Visible = false;
            txtbasicamount.Text = "0";
            txtfexcisep.Text = "0";
            txtfexciseamt.Text = "0";
            txtfcstp.Text = "0";
            txtfcstamt.Text = "0";
            txtfvatp.Text = "0";
            txtfvatamt.Text = "0";
            txtgrandtotal.Text = "0";
            fillacnamedrop();
            txtclientcode.Text = string.Empty;
        }
    }
    protected void txtgvqty_TextChanged(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            countgv();
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            li.vid = Convert.ToInt64(lblid.Text);
            DataTable dtremain = new DataTable();
            dtremain = fpoclass.selectqtyremainusedfromsono(li);
            if (dtremain.Rows.Count > 0)
            {
                li.qty = Convert.ToDouble(lblqty.Text);
                double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                if (Convert.ToDouble(txtgvqty1.Text) <= rqty)
                {
                    countgv();
                }
                else
                {
                    txtgvqty1.Text = lblqtyremain.Text;
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                    return;
                }
            }
        }
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        var cc = txtgvcst.Text.IndexOf("-");
        if (cc != -1)
        {
            txtgvcst.Text = "0";
        }
        count1();
        Page.SetFocus(txtcstamount);
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtdescription1);
    }

    protected void txtgridqty_TextChanged(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
            Label txtgvamount1 = (Label)currentRow.FindControl("lblamount");
            TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
            //if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
            //{
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                //countgv();
                countgv1();
            }
            Page.SetFocus(txtgvunit);
            //}
            //else
            //{
            //    txtgvqty1.Text = lblqtyremain.Text;
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
            //    return;
            //}
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
            Label txtgvamount1 = (Label)currentRow.FindControl("lblamount");
            TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
            li.vid = Convert.ToInt64(lblid.Text);
            DataTable dtremain = new DataTable();
            dtremain = fpoclass.selectqtyremainusedfromsono(li);
            //if (dtremain.Rows.Count > 0)
            //{
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain1"].ToString()) + Convert.ToDouble(lblqty.Text);
            //    //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
            //    if (Convert.ToDouble(txtgvqty1.Text) > rqty)
            //    {
            //        //    li.id = Convert.ToInt64(lblid.Text);
            //        //    li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
            //        //    li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
            //        //    fscclass.updateremainqtyinsalesorder(li);
            //        //}
            //        //else
            //        //{
            //        txtgvqty1.Text = lblqty.Text;
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
            //        return;
            //    }
            //    else
            //    {
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                //countgv();
                countgv1();
            }
            Page.SetFocus(txtgvunit);
            //    }
            //}
        }
    }

    protected void txtgvrate_TextChanged(object sender, EventArgs e)
    {
        //count1();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        TextBox txtgridtaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        {
            countgv1();
        }
        Page.SetFocus(txtgridtaxtype);
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            lblpopupacname.Text = txtclientcode.Text.Trim();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            DataTable dtpc = new DataTable();
            if (btnfinalsave.Text == "Save")
            {
                dtpc = fpoclass.selectallsalesorderforpopupfromccode(li);
            }
            else
            {
                if (txtsalesorder.Text != string.Empty)
                {
                    li.sono = Convert.ToInt64(txtsalesorder.Text);
                    dtpc = fpoclass.selectallsalesorderforpopupfromccodeforupdate(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Sales Order No. and Try Again.');", true);
                    return;
                }
            }
            if (dtpc.Rows.Count > 0)
            {
                lblemptyso.Visible = false;
                gvsalesorder.Visible = true;
                gvsalesorder.DataSource = dtpc;
                gvsalesorder.DataBind();
            }
            else
            {
                gvsalesorder.DataSource = null;
                gvsalesorder.DataBind();
                gvsalesorder.Visible = false;
                lblemptyso.Visible = true;
                lblemptyso.Text = "No Sales Order Found.";
            }
            btnselectitem.Visible = false;
            gvsoitemlistselection.Visible = false;
            chkall.Visible = false;
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Client Code and Try Again.');", true);
            return;
        }
    }

    protected void gvsalesorder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemspo"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(e.CommandArgument);
            txtsalesorder.Text = e.CommandArgument.ToString();
            //if (txtsalesorder.Text.Trim() != string.Empty)
            //{
            Session["dtpitemspo"] = null;
            Session["dtpitemspo"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsalesorder.Text);
            DataTable dtdt = new DataTable();
            dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
            if (dtdt.Rows.Count > 0)
            {
                //txtname.Text = dtdt.Rows[0]["acname"].ToString();
                txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
            }
            DataTable dtdata = new DataTable();
            if (btnfinalsave.Text != "Save")
            {
                string vid = "";
                for (int x = 0; x < gvpoitemlist.Rows.Count; x++)
                {
                    Label lblvid = (Label)gvpoitemlist.Rows[x].FindControl("lblvid");
                    vid = vid + lblvid.Text + ",";
                }
                li.remarks = vid;
                if (li.remarks != "")
                {
                    dtdata = fpoclass.selectallsalesorderitemdatafromsonoforupdate(li);
                }
                else
                {
                    dtdata = fpoclass.selectallsalesorderitemdatafromsono(li);
                }
            }
            else
            {
                dtdata = fpoclass.selectallsalesorderitemdatafromsono(li);
            }
            if (dtdata.Rows.Count > 0)
            {
                //lblempty.Visible = false;
                //gvpoitemlist.Visible = true;
                //gvpoitemlist.DataSource = dtdata;
                //gvpoitemlist.DataBind();
                //Session["dtpitems"] = dtdata;
                //for (int d = 0; d < dtdata.Rows.Count; d++)
                //{
                //    DataTable dt = (DataTable)Session["dtpitems"];
                //    DataRow dr = dt.NewRow();
                //    dr["id"] = dtdata.Rows[d]["id"].ToString();
                //    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                //    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                //    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                //    dr["qtyremain"] = dtdata.Rows[d]["qtyremain1"].ToString();
                //    dr["qtyused"] = dtdata.Rows[d]["qtyused1"].ToString();
                //    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                //    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                //    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                //    dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                //    dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                //    dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                //    dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                //    dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                //    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                //    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                //    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                //    dr["vid"] = 0;
                //    dt.Rows.Add(dr);
                //    Session["dtpitems"] = dt;
                //    //this.bindgrid();
                //    this.bindgrid1();
                //    gvpoitemlist.Visible = false;
                //}
                gvsoitemlistselection.Visible = true;
                Label1.Visible = false;
                chkall.Visible = true;
                gvsoitemlistselection.DataSource = dtdata;
                gvsoitemlistselection.DataBind();
                //countgv();
                lblempty.Visible = false;
            }
            else
            {
                gvsoitemlistselection.DataSource = null;
                gvsoitemlistselection.DataBind();
                //gvpoitemlist.Visible = false;
                gvsoitemlistselection.Visible = false;
                Label1.Visible = true;
                Label1.Text = "No Items Found.";
            }
            gvsalesorder.Visible = false;
            chkall.Visible = true;
            btnselectitem.Visible = true;
            hideimage();
            ModalPopupExtender2.Show();
            //}
            //else
            //{
            //    gvpoitemlist.Visible = false;
            //    lblempty.Visible = false;
            //    txtbasicamount.Text = "0";
            //    txtfexcisep.Text = "0";
            //    txtfexciseamt.Text = "0";
            //    txtfcstp.Text = "0";
            //    txtfcstamt.Text = "0";
            //    txtfvatp.Text = "0";
            //    txtfvatamt.Text = "0";
            //    txtgrandtotal.Text = "0";
            //    txtname.Text = string.Empty;
            //    txtclientcode.Text = string.Empty;
            //}
        }
        hideimage();
    }

    protected void chkreceived_CheckedChanged(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        GridViewRow currentRow = (GridViewRow)((CheckBox)sender).Parent.Parent;
        Label lblid = (Label)currentRow.FindControl("lblid");
        CheckBox chkreceived = (CheckBox)currentRow.FindControl("chkreceived");
        li.id = Convert.ToInt64(lblid.Text);
        if (chkreceived.Checked == true)
        {
            li.iscame = "Yes";
        }
        else
        {
            li.iscame = "No";
        }
        fpoclass.updateiscame(li);
        li.strpono = txtpurchaseorderno.Text;
        DataTable dtdata = new DataTable();
        dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpoitemlist.Visible = true;
            gvpoitemlist.DataSource = dtdata;
            gvpoitemlist.DataBind();
        }
        else
        {
            gvpoitemlist.DataSource = null;
            gvpoitemlist.DataBind();
            gvpoitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Order Items Found.";
        }
    }
    //protected void gvpoitemlist_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        //Find the TextBox control.
    //        Label lbliscame = (e.Row.FindControl("lbliscame") as Label);
    //        CheckBox chkreceived = (e.Row.FindControl("chkreceived") as CheckBox);
    //        //var lbliscame = (GridView)e.Row.FindControl("lbliscame");
    //        //var chkreceived = (GridView)e.Row.FindControl("chkreceived");
    //        //Label lbliscame = (Label)gvpoitemlist.Rows[e.Row.RowIndex].FindControl("lbliscame");
    //        //CheckBox chkreceived = (CheckBox)gvpoitemlist.Rows[e.Row.RowIndex].FindControl("chkreceived");
    //        if (lbliscame.Text == "Yes")
    //        {
    //            chkreceived.Checked = true;
    //        }
    //    }
    //}
    protected void txtdeliverydays_TextChanged(object sender, EventArgs e)
    {
        if (txtdeliverydays.Text != string.Empty && txtpodate.Text != string.Empty)
        {
            int cc = Convert.ToInt16(txtdeliverydays.Text);
            txtfollowupdate.Text = (Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).AddDays(cc)).ToString("dd-MM-yyyy");
        }
        Page.SetFocus(txtclientcode);
    }
    protected void txtpodate_TextChanged(object sender, EventArgs e)
    {
        if (txtdeliverydays.Text != string.Empty && txtpodate.Text != string.Empty)
        {
            int cc = Convert.ToInt16(txtdeliverydays.Text);
            txtfollowupdate.Text = (Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).AddDays(cc)).ToString("dd-MM-yyyy");
        }
    }
    protected void gvpoitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.strpono = txtpurchaseorderno.Text;
            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

            Label lblvatamt1 = (Label)currentRow.FindControl("lblvatamt");
            Label lbladdvatamt1 = (Label)currentRow.FindControl("lbladdvatamt");
            Label lblsctamt1 = (Label)currentRow.FindControl("lblcstamt");
            if ((Convert.ToDouble(lblvatamt1.Text) > 0 || Convert.ToDouble(lbladdvatamt1.Text) > 0) && (Convert.ToDouble(lblsctamt1.Text) > 0))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Combination of both S/C GST And IGST not possible.Try Again.');", true);
                return;
            }
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            li.qty = Convert.ToDouble(lblqty.Text);
            li.vid = Convert.ToInt64(e.CommandArgument);
            DataTable dtremain = new DataTable();
            dtremain = fpoclass.selectqtyremainusedfromsono(li);
            if (Convert.ToDouble(txtgvqty1.Text) > 0)
            {
                if (dtremain.Rows.Count > 0)
                {
                    //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain1"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                    li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused1"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                    fpoclass.updateremainqtyinsalesorder(li);

                    //update poitems data
                    //for (int c = 0; c < gvpoitemlist.Rows.Count; c++)
                    //{
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                    //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblunit = (TextBox)currentRow.FindControl("txtgvunit");
                    Label lblrate = (Label)currentRow.FindControl("lblrate");
                    TextBox txtgvrate = (TextBox)currentRow.FindControl("txtgvrate");
                    Label lblbasicamt = (Label)currentRow.FindControl("lblbasicamt");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    //if (li.qty > 0)
                    //{
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(txtgvrate.Text);
                    li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                    li.vattype = lbltaxtype.Text;
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                    li.vatamt = Convert.ToDouble(lblvatamt.Text);
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Convert.ToDouble(lblsctamt.Text);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpoclass.updatepoitemsdata(li);
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpoitemlist.Visible = true;
                        gvpoitemlist.DataSource = dtdata;
                        gvpoitemlist.DataBind();
                    }
                    else
                    {
                        gvpoitemlist.DataSource = null;
                        gvpoitemlist.DataBind();
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                    }
                    //}
                    //}

                }
                else
                {
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                    //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblunit = (TextBox)currentRow.FindControl("txtgvunit");
                    Label lblrate = (Label)currentRow.FindControl("lblrate");
                    TextBox txtgvrate = (TextBox)currentRow.FindControl("txtgvrate");
                    Label lblbasicamt = (Label)currentRow.FindControl("lblbasicamt");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    //if (li.qty > 0)
                    //{
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(txtgvrate.Text);
                    li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                    li.vattype = lbltaxtype.Text;
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                    li.vatamt = Convert.ToDouble(lblvatamt.Text);
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                    li.cstamt = Convert.ToDouble(lblsctamt.Text);
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpoclass.updatepoitemsdata(li);
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpoitemlist.Visible = true;
                        gvpoitemlist.DataSource = dtdata;
                        gvpoitemlist.DataBind();
                    }
                    else
                    {
                        gvpoitemlist.DataSource = null;
                        gvpoitemlist.DataBind();
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                    }
                }
            }
            else
            {
                Label lblid = (Label)currentRow.FindControl("lblid");
                Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                // Label lblqty = (Label)gvpoitemlist.Rows[c].FindControl("lblqty");
                //TextBox lblqty = (TextBox)gvpoitemlist.Rows[c].FindControl("txtgridqty");
                TextBox lblunit = (TextBox)currentRow.FindControl("txtgvunit");
                Label lblrate = (Label)currentRow.FindControl("lblrate");
                TextBox txtgvrate = (TextBox)currentRow.FindControl("txtgvrate");
                Label lblbasicamt = (Label)currentRow.FindControl("lblbasicamt");
                TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                Label lblamount = (Label)currentRow.FindControl("lblamount");
                TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                li.qty = Convert.ToDouble(lblqty.Text);
                li.id = Convert.ToInt64(lblid.Text);
                //if (li.qty > 0)
                //{
                li.itemname = lblitemname.Text;
                li.qty = Convert.ToDouble(txtgvqty1.Text);
                li.qtyremain = li.qty;
                li.qtyused = 0;
                li.unit = lblunit.Text;
                li.rate = Convert.ToDouble(txtgvrate.Text);
                li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                li.vattype = lbltaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.cstamt = Convert.ToDouble(lblsctamt.Text);
                li.amount = Convert.ToDouble(lblamount.Text);
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                fpoclass.updatepoitemsdata(li);
                DataTable dtdata = new DataTable();
                dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpoitemlist.Visible = true;
                    gvpoitemlist.DataSource = dtdata;
                    gvpoitemlist.DataBind();
                }
                else
                {
                    gvpoitemlist.DataSource = null;
                    gvpoitemlist.DataBind();
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                }
            }
        }
    }

    protected void btnselectitem_Click(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Session["dtpitemspo"] = CreateTemplate();
            if (chkall.Checked == true)
            {
                li.sono = Convert.ToInt64(txtsalesorder.Text);
                if (txtsalesorder.Text.Trim() != string.Empty)
                {
                    Session["dtpitemspo"] = null;
                    Session["dtpitemspo"] = CreateTemplate();
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.sono = Convert.ToInt64(txtsalesorder.Text);
                    DataTable dtdt = new DataTable();
                    dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
                    if (dtdt.Rows.Count > 0)
                    {
                        //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                        txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
                    }
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallsalesorderitemdatafromsonojoinitemmaster(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        //lblempty.Visible = false;
                        //gvpoitemlist.Visible = true;
                        //gvpoitemlist.DataSource = dtdata;
                        //gvpoitemlist.DataBind();
                        //Session["dtpitems"] = dtdata;
                        for (int d = 0; d < dtdata.Rows.Count; d++)
                        {
                            DataTable dt = (DataTable)Session["dtpitemspo"];
                            DataRow dr = dt.NewRow();
                            dr["id"] = dtdata.Rows[d]["id"].ToString();
                            dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                            dr["qtyremain"] = dtdata.Rows[d]["qtyremain1"].ToString();
                            dr["qtyused"] = dtdata.Rows[d]["qtyused1"].ToString();
                            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                            dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();

                            if (dtdata.Rows[d]["vattype"].ToString().IndexOf("-") != -1)
                            {
                                double basicamt = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                                double vatp = 0;
                                double addvatp = 0;
                                double cstp = 0;
                                dr["taxtype"] = dtdata.Rows[d]["vattype"].ToString();
                                dr["vatp"] = dtdata.Rows[d]["vattype"].ToString().Split('-')[0];
                                dr["addtaxp"] = dtdata.Rows[d]["vattype"].ToString().Split('-')[1];
                                vatp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[0]);
                                addvatp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[1]);
                                dr["vatamt"] = Math.Round((basicamt * vatp) / 100, 2);
                                dr["addtaxamt"] = Math.Round((basicamt * addvatp) / 100, 2);
                                dr["cstamt"] = 0;
                                dr["cstp"] = 0;
                                dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Math.Round((basicamt * vatp) / 100, 2) + Math.Round((basicamt * addvatp) / 100, 2);
                            }
                            else
                            {
                                double basicamt = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                                double cstp = 0;
                                dr["taxtype"] = "";
                                dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                dr["vatp"] = 0;
                                dr["vatamt"] = 0;
                                cstp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString());
                                dr["cstamt"] = Math.Round((basicamt * cstp) / 100, 2);
                                dr["addtaxp"] = 0;
                                dr["addtaxamt"] = 0;
                                dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Math.Round((basicamt * cstp) / 100, 2);
                            }
                            //dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                            dr["vid"] = 0;
                            dt.Rows.Add(dr);
                            Session["dtpitemspo"] = dt;
                            this.bindgrid();
                            //this.bindgrid1();
                        }
                        countgv();
                    }
                    else
                    {
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Items Found.";
                    }
                }
                else
                {
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = false;
                    txtbasicamount.Text = "0";
                    txtfexcisep.Text = "0";
                    txtfexciseamt.Text = "0";
                    txtfcstp.Text = "0";
                    txtfcstamt.Text = "0";
                    txtfvatp.Text = "0";
                    txtfvatamt.Text = "0";
                    txtgrandtotal.Text = "0";
                    fillacnamedrop();
                    txtclientcode.Text = string.Empty;
                }
            }
            else
            {
                string id = "0";
                for (int cc = 0; cc < gvsoitemlistselection.Rows.Count; cc++)
                {
                    Label lblid = (Label)gvsoitemlistselection.Rows[cc].FindControl("lblid");
                    CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[cc].FindControl("chkselect");
                    if (chkselect.Checked == true)
                    {
                        id = id + "," + lblid.Text;
                    }
                }
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(txtsalesorder.Text);
                DataTable dtdt = new DataTable();
                dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
                if (dtdt.Rows.Count > 0)
                {
                    //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                    txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
                }

                li.remarks = id;
                DataTable dtdata = new DataTable();
                dtdata = fpoclass.selectallsalesorderitemdatafromsonousinginforiteminnerjoin(li);
                if (dtdata.Rows.Count > 0)
                {
                    //lblempty.Visible = false;
                    //gvpoitemlist.Visible = true;
                    //gvpoitemlist.DataSource = dtdata;
                    //gvpoitemlist.DataBind();
                    //Session["dtpitems"] = dtdata;
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataTable dt = (DataTable)Session["dtpitemspo"];
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain1"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused1"].ToString();
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        if (dtdata.Rows[d]["vattype"].ToString().IndexOf("-") != -1)
                        {
                            double basicamt = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                            double vatp = 0;
                            double addvatp = 0;
                            double cstp = 0;
                            dr["taxtype"] = dtdata.Rows[d]["vattype"].ToString();
                            dr["vatp"] = dtdata.Rows[d]["vattype"].ToString().Split('-')[0];
                            dr["addtaxp"] = dtdata.Rows[d]["vattype"].ToString().Split('-')[1];
                            vatp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[0]);
                            addvatp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[1]);
                            dr["vatamt"] = Math.Round((basicamt * vatp) / 100, 2);
                            dr["addtaxamt"] = Math.Round((basicamt * addvatp) / 100, 2);
                            dr["cstamt"] = 0;
                            dr["cstp"] = 0;
                            dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Math.Round((basicamt * vatp) / 100, 2) + Math.Round((basicamt * addvatp) / 100, 2);
                        }
                        else
                        {
                            double basicamt = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                            double cstp = 0;
                            dr["taxtype"] = "";
                            dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                            dr["vatp"] = 0;
                            dr["vatamt"] = 0;
                            cstp = Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString());
                            dr["cstamt"] = Math.Round((basicamt * cstp) / 100, 2);
                            dr["addtaxp"] = 0;
                            dr["addtaxamt"] = 0;
                            dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Math.Round((basicamt * cstp) / 100, 2);
                        }
                        //dr["amount"] = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString()) + Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        Session["dtpitemspo"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                    }
                    countgv();
                }
                else
                {
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }

            }
            hideimage();
        }
        else
        {
            //during update
            if (chkall.Checked == true)
            {
                li.sono = Convert.ToInt64(txtsalesorder.Text);
                if (txtsalesorder.Text.Trim() != string.Empty)
                {
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.sono = Convert.ToInt64(txtsalesorder.Text);
                    DataTable dtdt = new DataTable();
                    dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
                    if (dtdt.Rows.Count > 0)
                    {
                        //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                        txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
                    }
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallsalesorderitemdatafromsonojoinitemmaster(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        //lblempty.Visible = false;
                        //gvpoitemlist.Visible = true;
                        //gvpoitemlist.DataSource = dtdata;
                        //gvpoitemlist.DataBind();
                        //Session["dtpitems"] = dtdata;
                        for (int d = 0; d < dtdata.Rows.Count; d++)
                        {
                            li.strpono = txtpurchaseorderno.Text;
                            li.vnono = Convert.ToInt64(dtdata.Rows[d]["sono"].ToString());
                            li.vid = Convert.ToInt64(dtdata.Rows[d]["id"].ToString());
                            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                            li.itemname = dtdata.Rows[d]["itemname"].ToString();
                            li.qty = Convert.ToDouble(dtdata.Rows[d]["qty"].ToString());
                            li.qtyremain1 = li.qty;
                            li.qtyused1 = 0;
                            li.unit = dtdata.Rows[d]["unit"].ToString();
                            li.rate = Convert.ToDouble(dtdata.Rows[d]["rate"].ToString());
                            li.basicamount = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                            li.vattype = dtdata.Rows[d]["taxtype"].ToString();
                            li.vatp = Convert.ToDouble(dtdata.Rows[d]["vatp"].ToString());
                            li.addtaxp = Convert.ToDouble(dtdata.Rows[d]["addtaxp"].ToString());
                            li.cstp = Convert.ToDouble(dtdata.Rows[d]["cstp"].ToString());
                            li.vatamt = Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString());
                            li.addtaxamt = Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                            li.cstamt = Convert.ToDouble(dtdata.Rows[d]["cstamt"].ToString());
                            li.amount = Convert.ToDouble(dtdata.Rows[d]["amount"].ToString());
                            li.descr1 = dtdata.Rows[d]["descr1"].ToString();
                            li.descr2 = dtdata.Rows[d]["descr2"].ToString();
                            li.descr3 = dtdata.Rows[d]["descr3"].ToString();
                            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                            li.uname = Request.Cookies["ForLogin"]["username"];
                            li.udate = System.DateTime.Now;
                            fpoclass.insertpoitemsdata(li);
                            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                            li.uname = Request.Cookies["ForLogin"]["username"];
                            li.udate = System.DateTime.Now;
                            li.activity = li.strpono + " Purchase Order Item Inserted.";
                            faclass.insertactivity(li);
                            DataTable dtdatad = new DataTable();
                            dtdatad = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                            if (dtdatad.Rows.Count > 0)
                            {
                                lblempty.Visible = false;
                                gvpoitemlist.Visible = true;
                                gvpoitemlist.DataSource = dtdatad;
                                gvpoitemlist.DataBind();
                                lblcount.Text = dtdatad.Rows.Count.ToString();
                                Session["dtpitemspo"] = dtdatad;
                            }
                            else
                            {
                                gvpoitemlist.DataSource = null;
                                gvpoitemlist.DataBind();
                                gvpoitemlist.Visible = false;
                                lblempty.Visible = true;
                                lblempty.Text = "No Purchase Order Items Found.";
                                lblcount.Text = "0";
                            }
                        }
                        countgv();
                    }
                    else
                    {
                        if (gvpoitemlist.Rows.Count == 0)
                        {
                            gvpoitemlist.Visible = false;
                            lblempty.Visible = true;
                            lblempty.Text = "No Items Found.";
                        }
                    }
                }
                else
                {
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = false;
                    txtbasicamount.Text = "0";
                    txtfexcisep.Text = "0";
                    txtfexciseamt.Text = "0";
                    txtfcstp.Text = "0";
                    txtfcstamt.Text = "0";
                    txtfvatp.Text = "0";
                    txtfvatamt.Text = "0";
                    txtgrandtotal.Text = "0";
                    fillacnamedrop();
                    txtclientcode.Text = string.Empty;
                }
            }
            else
            {
                string id = "0";
                for (int cc = 0; cc < gvsoitemlistselection.Rows.Count; cc++)
                {
                    Label lblid = (Label)gvsoitemlistselection.Rows[cc].FindControl("lblid");
                    CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[cc].FindControl("chkselect");
                    if (chkselect.Checked == true)
                    {
                        id = id + "," + lblid.Text;
                    }
                }
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(txtsalesorder.Text);
                DataTable dtdt = new DataTable();
                dtdt = fpoclass.selectallsalesordermasterdatafromsono(li);
                if (dtdt.Rows.Count > 0)
                {
                    //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                    txtclientcode.Text = dtdt.Rows[0]["ccode"].ToString();
                }

                li.remarks = id;
                DataTable dtdata = new DataTable();
                dtdata = fpoclass.selectallsalesorderitemdatafromsonousinginforiteminnerjoin(li);
                if (dtdata.Rows.Count > 0)
                {
                    //lblempty.Visible = false;
                    //gvpoitemlist.Visible = true;
                    //gvpoitemlist.DataSource = dtdata;
                    //gvpoitemlist.DataBind();
                    //Session["dtpitems"] = dtdata;
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        li.strpono = txtpurchaseorderno.Text;
                        li.vnono = Convert.ToInt64(dtdata.Rows[d]["sono"].ToString());
                        li.vid = Convert.ToInt64(dtdata.Rows[d]["id"].ToString());
                        li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        li.itemname = dtdata.Rows[d]["itemname"].ToString();
                        li.qty = Convert.ToDouble(dtdata.Rows[d]["qty"].ToString());
                        li.qtyremain1 = li.qty;
                        li.qtyused1 = 0;
                        li.unit = dtdata.Rows[d]["unit"].ToString();
                        li.rate = Convert.ToDouble(dtdata.Rows[d]["rate"].ToString());
                        li.basicamount = Convert.ToDouble(dtdata.Rows[d]["basicamount"].ToString());
                        li.vattype = dtdata.Rows[d]["taxtype"].ToString();
                        li.vatp = Convert.ToDouble(dtdata.Rows[d]["vatp"].ToString());
                        li.addtaxp = Convert.ToDouble(dtdata.Rows[d]["addtaxp"].ToString());
                        li.cstp = Convert.ToDouble(dtdata.Rows[d]["cstp"].ToString());
                        li.vatamt = Convert.ToDouble(dtdata.Rows[d]["vatamt"].ToString());
                        li.addtaxamt = Convert.ToDouble(dtdata.Rows[d]["addtaxamt"].ToString());
                        li.cstamt = Convert.ToDouble(dtdata.Rows[d]["cstamt"].ToString());
                        li.amount = Convert.ToDouble(dtdata.Rows[d]["amount"].ToString());
                        li.descr1 = dtdata.Rows[d]["descr1"].ToString();
                        li.descr2 = dtdata.Rows[d]["descr2"].ToString();
                        li.descr3 = dtdata.Rows[d]["descr3"].ToString();
                        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                        li.uname = Request.Cookies["ForLogin"]["username"];
                        li.udate = System.DateTime.Now;
                        fpoclass.insertpoitemsdata(li);
                        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                        li.uname = Request.Cookies["ForLogin"]["username"];
                        li.udate = System.DateTime.Now;
                        li.activity = li.strpono + " Purchase Order Item Inserted.";
                        faclass.insertactivity(li);
                        DataTable dtdatad = new DataTable();
                        dtdatad = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                        if (dtdata.Rows.Count > 0)
                        {
                            lblempty.Visible = false;
                            gvpoitemlist.Visible = true;
                            gvpoitemlist.DataSource = dtdatad;
                            gvpoitemlist.DataBind();
                            lblcount.Text = dtdatad.Rows.Count.ToString();
                            Session["dtpitemspo"] = dtdatad;
                        }
                        else
                        {
                            gvpoitemlist.DataSource = null;
                            gvpoitemlist.DataBind();
                            gvpoitemlist.Visible = false;
                            lblempty.Visible = true;
                            lblempty.Text = "No Purchase Order Items Found.";
                            lblcount.Text = "0";
                        }
                    }
                    //countgv();
                    //countgv1();
                }
                else
                {
                    if (gvpoitemlist.Rows.Count == 0)
                    {
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Items Found.";
                    }
                }

            }
            countgv1();

        }
        ModalPopupExtender2.Hide();
        Page.SetFocus(txtdeliverydays);
    }

    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fpoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                string vattype = dtdata.Rows[0]["gsttype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvatp.Text = vattype.Split('-')[0];
                    txtgvvat.Text = "0";
                    txtgvadvatp.Text = vattype.Split('-')[1];
                    txtgvadvat.Text = "0";
                    count1();
                }
            }
            else
            {
                txtqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        Page.SetFocus(txtqty);
    }
    protected void drpacname_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            li.acname = drpacname.SelectedItem.Text;
            SqlDataAdapter da = new SqlDataAdapter("select * from ACMaster where acname='" + drpacname.SelectedItem.Text + "'", con);
            DataTable dtd = new DataTable();
            con.Open();
            da.Fill(dtd);
            con.Close();
            if (dtd.Rows.Count > 0)
            {
                if (dtd.Rows[0]["gstno"].ToString().Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('GST No for " + drpacname.SelectedItem.Text + " is not available.');", true);
                    return;
                }
            }
        }
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from PurchaseOrder order by pono", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pono = Convert.ToInt64(dtdataq.Rows[0]["pono"].ToString());
            li.strpono = li.pono.ToString();
            ViewState["pono"] = li.strpono;
            DataTable dtso = new DataTable();
            dtso = fpoclass.selectallPurchaseorderdatafromponostring(li);
            if (dtso.Rows.Count > 0)
            {
                txtpurchaseorderno.Text = dtso.Rows[0]["pono"].ToString();
                txtpodate.Text = dtso.Rows[0]["podate"].ToString();
                txtsalesorder.Text = dtso.Rows[0]["sono"].ToString();
                txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtdeliverydays.Text = dtso.Rows[0]["deldays"].ToString();
                txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                txtfollowupdate.Text = Convert.ToDateTime(dtso.Rows[0]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                txtfollowupdetails.Text = dtso.Rows[0]["followupdetails"].ToString();
                txtfollowupdetails1.Text = dtso.Rows[0]["followupdetails1"].ToString();
                txtbasicamount.Text = dtso.Rows[0]["totbasic"].ToString();
                txtfexcisep.Text = dtso.Rows[0]["excisep"].ToString();
                txtfexciseamt.Text = dtso.Rows[0]["exciseamt"].ToString();
                txtfcstp.Text = dtso.Rows[0]["cstp"].ToString();
                txtfcstamt.Text = dtso.Rows[0]["cstamt"].ToString();
                txtfvatp.Text = dtso.Rows[0]["vatp"].ToString();
                txtfvatamt.Text = dtso.Rows[0]["vatamt"].ToString();
                txtgrandtotal.Text = dtso.Rows[0]["grandtotal"].ToString();
                txtdeliverysch.Text = dtso.Rows[0]["delivery"].ToString();
                txtpayment.Text = dtso.Rows[0]["payment"].ToString();
                txtdeliveryat.Text = dtso.Rows[0]["deliveryat"].ToString();
                txttaxes.Text = dtso.Rows[0]["taxes"].ToString();
                txtoctroi.Text = dtso.Rows[0]["octroi"].ToString();
                txtexcise.Text = dtso.Rows[0]["excise"].ToString();
                txtform.Text = dtso.Rows[0]["form"].ToString();
                txttransportation.Text = dtso.Rows[0]["transportation"].ToString();
                txtinsurance.Text = dtso.Rows[0]["insurance"].ToString();
                txtpacking.Text = dtso.Rows[0]["packing"].ToString();
                txtdescription1.Text = dtso.Rows[0]["descr1"].ToString();
                txtdescription2.Text = dtso.Rows[0]["descr2"].ToString();
                txtdescription3.Text = dtso.Rows[0]["descr3"].ToString();
                txtdescription4.Text = dtso.Rows[0]["descr4"].ToString();
                txtdescription5.Text = dtso.Rows[0]["descr5"].ToString();
                txtdescription6.Text = dtso.Rows[0]["descr6"].ToString();
                drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                drpordertype.SelectedValue = dtso.Rows[0]["type"].ToString();
                txtuser.Text = dtso.Rows[0]["uname"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpoitemlist.Visible = true;
                    gvpoitemlist.DataSource = dtdata;
                    gvpoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvpoitemlist.DataSource = null;
                    gvpoitemlist.DataBind();
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                btnfinalsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from PurchaseOrder order by pono", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pono = Convert.ToInt64(dtdataq.Rows[dtdataq.Rows.Count - 1]["pono"].ToString());
            li.strpono = li.pono.ToString();
            ViewState["pono"] = li.strpono;
            DataTable dtso = new DataTable();
            dtso = fpoclass.selectallPurchaseorderdatafromponostring(li);
            if (dtso.Rows.Count > 0)
            {
                txtpurchaseorderno.Text = dtso.Rows[0]["pono"].ToString();
                txtpodate.Text = dtso.Rows[0]["podate"].ToString();
                txtsalesorder.Text = dtso.Rows[0]["sono"].ToString();
                txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtdeliverydays.Text = dtso.Rows[0]["deldays"].ToString();
                txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                txtfollowupdate.Text = Convert.ToDateTime(dtso.Rows[0]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                txtfollowupdetails.Text = dtso.Rows[0]["followupdetails"].ToString();
                txtfollowupdetails1.Text = dtso.Rows[0]["followupdetails1"].ToString();
                txtbasicamount.Text = dtso.Rows[0]["totbasic"].ToString();
                txtfexcisep.Text = dtso.Rows[0]["excisep"].ToString();
                txtfexciseamt.Text = dtso.Rows[0]["exciseamt"].ToString();
                txtfcstp.Text = dtso.Rows[0]["cstp"].ToString();
                txtfcstamt.Text = dtso.Rows[0]["cstamt"].ToString();
                txtfvatp.Text = dtso.Rows[0]["vatp"].ToString();
                txtfvatamt.Text = dtso.Rows[0]["vatamt"].ToString();
                txtgrandtotal.Text = dtso.Rows[0]["grandtotal"].ToString();
                txtdeliverysch.Text = dtso.Rows[0]["delivery"].ToString();
                txtpayment.Text = dtso.Rows[0]["payment"].ToString();
                txtdeliveryat.Text = dtso.Rows[0]["deliveryat"].ToString();
                txttaxes.Text = dtso.Rows[0]["taxes"].ToString();
                txtoctroi.Text = dtso.Rows[0]["octroi"].ToString();
                txtexcise.Text = dtso.Rows[0]["excise"].ToString();
                txtform.Text = dtso.Rows[0]["form"].ToString();
                txttransportation.Text = dtso.Rows[0]["transportation"].ToString();
                txtinsurance.Text = dtso.Rows[0]["insurance"].ToString();
                txtpacking.Text = dtso.Rows[0]["packing"].ToString();
                txtdescription1.Text = dtso.Rows[0]["descr1"].ToString();
                txtdescription2.Text = dtso.Rows[0]["descr2"].ToString();
                txtdescription3.Text = dtso.Rows[0]["descr3"].ToString();
                txtdescription4.Text = dtso.Rows[0]["descr4"].ToString();
                txtdescription5.Text = dtso.Rows[0]["descr5"].ToString();
                txtdescription6.Text = dtso.Rows[0]["descr6"].ToString();
                drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                drpordertype.SelectedValue = dtso.Rows[0]["type"].ToString();
                txtuser.Text = dtso.Rows[0]["uname"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpoitemlist.Visible = true;
                    gvpoitemlist.DataSource = dtdata;
                    gvpoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvpoitemlist.DataSource = null;
                    gvpoitemlist.DataBind();
                    gvpoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                btnfinalsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from PurchaseOrder order by pono", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.pono = Convert.ToInt64(txtpurchaseorderno.Text) + 1;
        for (Int64 i = li.sono; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["sono"].ToString()); i++)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder where sono=" + li.sono + " order by sono", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(dtdataq.Rows[0]["sono"].ToString());
                ViewState["pono"] = li.strpono;
                DataTable dtso = new DataTable();
                dtso = fpoclass.selectallPurchaseorderdatafromponostring(li);
                if (dtso.Rows.Count > 0)
                {
                    txtpurchaseorderno.Text = dtso.Rows[0]["pono"].ToString();
                    txtpodate.Text = dtso.Rows[0]["podate"].ToString();
                    txtsalesorder.Text = dtso.Rows[0]["sono"].ToString();
                    txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtdeliverydays.Text = dtso.Rows[0]["deldays"].ToString();
                    txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                    txtfollowupdate.Text = Convert.ToDateTime(dtso.Rows[0]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                    txtfollowupdetails.Text = dtso.Rows[0]["followupdetails"].ToString();
                    txtfollowupdetails1.Text = dtso.Rows[0]["followupdetails1"].ToString();
                    txtbasicamount.Text = dtso.Rows[0]["totbasic"].ToString();
                    txtfexcisep.Text = dtso.Rows[0]["excisep"].ToString();
                    txtfexciseamt.Text = dtso.Rows[0]["exciseamt"].ToString();
                    txtfcstp.Text = dtso.Rows[0]["cstp"].ToString();
                    txtfcstamt.Text = dtso.Rows[0]["cstamt"].ToString();
                    txtfvatp.Text = dtso.Rows[0]["vatp"].ToString();
                    txtfvatamt.Text = dtso.Rows[0]["vatamt"].ToString();
                    txtgrandtotal.Text = dtso.Rows[0]["grandtotal"].ToString();
                    txtdeliverysch.Text = dtso.Rows[0]["delivery"].ToString();
                    txtpayment.Text = dtso.Rows[0]["payment"].ToString();
                    txtdeliveryat.Text = dtso.Rows[0]["deliveryat"].ToString();
                    txttaxes.Text = dtso.Rows[0]["taxes"].ToString();
                    txtoctroi.Text = dtso.Rows[0]["octroi"].ToString();
                    txtexcise.Text = dtso.Rows[0]["excise"].ToString();
                    txtform.Text = dtso.Rows[0]["form"].ToString();
                    txttransportation.Text = dtso.Rows[0]["transportation"].ToString();
                    txtinsurance.Text = dtso.Rows[0]["insurance"].ToString();
                    txtpacking.Text = dtso.Rows[0]["packing"].ToString();
                    txtdescription1.Text = dtso.Rows[0]["descr1"].ToString();
                    txtdescription2.Text = dtso.Rows[0]["descr2"].ToString();
                    txtdescription3.Text = dtso.Rows[0]["descr3"].ToString();
                    txtdescription4.Text = dtso.Rows[0]["descr4"].ToString();
                    txtdescription5.Text = dtso.Rows[0]["descr5"].ToString();
                    txtdescription6.Text = dtso.Rows[0]["descr6"].ToString();
                    drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                    drpordertype.SelectedValue = dtso.Rows[0]["type"].ToString();
                    txtuser.Text = dtso.Rows[0]["uname"].ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpoitemlist.Visible = true;
                        gvpoitemlist.DataSource = dtdata;
                        gvpoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpoitemlist.DataSource = null;
                        gvpoitemlist.DataBind();
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                    btnfinalsave.Text = "Update";
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from SalesOrder order by sono", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.sono = Convert.ToInt64(txtpurchaseorderno.Text) - 1;
        for (Int64 i = li.sono; i >= Convert.ToInt64(dtdataqa.Rows[0]["sono"].ToString()); i--)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder where sono=" + li.sono + " order by sono", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(dtdataq.Rows[0]["sono"].ToString());
                ViewState["pono"] = li.strpono;
                DataTable dtso = new DataTable();
                dtso = fpoclass.selectallPurchaseorderdatafromponostring(li);
                if (dtso.Rows.Count > 0)
                {
                    txtpurchaseorderno.Text = dtso.Rows[0]["pono"].ToString();
                    txtpodate.Text = dtso.Rows[0]["podate"].ToString();
                    txtsalesorder.Text = dtso.Rows[0]["sono"].ToString();
                    txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtdeliverydays.Text = dtso.Rows[0]["deldays"].ToString();
                    txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                    txtfollowupdate.Text = Convert.ToDateTime(dtso.Rows[0]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                    txtfollowupdetails.Text = dtso.Rows[0]["followupdetails"].ToString();
                    txtfollowupdetails1.Text = dtso.Rows[0]["followupdetails1"].ToString();
                    txtbasicamount.Text = dtso.Rows[0]["totbasic"].ToString();
                    txtfexcisep.Text = dtso.Rows[0]["excisep"].ToString();
                    txtfexciseamt.Text = dtso.Rows[0]["exciseamt"].ToString();
                    txtfcstp.Text = dtso.Rows[0]["cstp"].ToString();
                    txtfcstamt.Text = dtso.Rows[0]["cstamt"].ToString();
                    txtfvatp.Text = dtso.Rows[0]["vatp"].ToString();
                    txtfvatamt.Text = dtso.Rows[0]["vatamt"].ToString();
                    txtgrandtotal.Text = dtso.Rows[0]["grandtotal"].ToString();
                    txtdeliverysch.Text = dtso.Rows[0]["delivery"].ToString();
                    txtpayment.Text = dtso.Rows[0]["payment"].ToString();
                    txtdeliveryat.Text = dtso.Rows[0]["deliveryat"].ToString();
                    txttaxes.Text = dtso.Rows[0]["taxes"].ToString();
                    txtoctroi.Text = dtso.Rows[0]["octroi"].ToString();
                    txtexcise.Text = dtso.Rows[0]["excise"].ToString();
                    txtform.Text = dtso.Rows[0]["form"].ToString();
                    txttransportation.Text = dtso.Rows[0]["transportation"].ToString();
                    txtinsurance.Text = dtso.Rows[0]["insurance"].ToString();
                    txtpacking.Text = dtso.Rows[0]["packing"].ToString();
                    txtdescription1.Text = dtso.Rows[0]["descr1"].ToString();
                    txtdescription2.Text = dtso.Rows[0]["descr2"].ToString();
                    txtdescription3.Text = dtso.Rows[0]["descr3"].ToString();
                    txtdescription4.Text = dtso.Rows[0]["descr4"].ToString();
                    txtdescription5.Text = dtso.Rows[0]["descr5"].ToString();
                    txtdescription6.Text = dtso.Rows[0]["descr6"].ToString();
                    drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                    drpordertype.SelectedValue = dtso.Rows[0]["type"].ToString();
                    txtuser.Text = dtso.Rows[0]["uname"].ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpoitemlist.Visible = true;
                        gvpoitemlist.DataSource = dtdata;
                        gvpoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpoitemlist.DataSource = null;
                        gvpoitemlist.DataBind();
                        gvpoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                    btnfinalsave.Text = "Update";
                }
            }
        }
    }

}