﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportViewer_My.aspx.cs" Inherits="ReportViewer" Culture="hi-IN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
            RenderingDPI="100" PrintMode="ActiveX" SeparatePages="False" />
            <iframe id="fbarcode" runat="server" width="100%" height="650px" marginheight="0" marginwidth="0">
            </iframe>
    </div>
    </form>
</body>
</html>
