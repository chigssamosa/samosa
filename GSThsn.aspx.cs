﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Data.SqlTypes;

public partial class GSThsn : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillgridb2b();
            //fillgridhsn();
            //ExportGridToExcel();
            //ExportGridToExcel1();
        }
    }

    public void fillgridhsn()
    {
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select ItemMaster.hsncode,ItemMaster.itemname,ItemMaster.unitewaybill as unit,SUM(SIItems.qty) as totqty,(sum(SIItems.amount)-SUM((CASE WHEN (ACMaster.excludetax='Yes') THEN SIItems.cstamt ELSE 0 END))) as totamount,sum(SIItems.basicamount) as totbasic,sum(SIItems.vatamt) as totvat,SUM(SIItems.addtaxamt) as totadvat,SUM((CASE WHEN ((ACMaster.excludetax is null or ACMaster.excludetax='' or ACMaster.excludetax='No')) THEN SIItems.cstamt ELSE 0 END)) as totcst from SIItems inner join ItemMaster on ItemMaster.itemname=SIItems.itemname inner join SIMaster on simaster.strsino=SIItems.strsino inner join ACMaster on SIMaster.salesac=ACMaster.acname where SIItems.sidate between @fromdate and @todate group by itemmaster.hsncode,ItemMaster.itemname,ItemMaster.unitewaybill", con);//,SUM((CASE WHEN ((ACMaster.excludetax is null or ACMaster.excludetax='' or ACMaster.excludetax='No')) THEN SIItems.qty ELSE 0 END)) as totqty
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);
        if (dtdata.Rows.Count > 0)
        {
            gvhsn.DataSource = dtdata;
            gvhsn.DataBind();
        }
    }

    private void ExportGridToExcel1()
    {
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select ItemMaster.hsncode,ItemMaster.itemname,ItemMaster.unitewaybill,SUM(SIItems.qty) as totqty,(sum(SIItems.amount)-SUM((CASE WHEN (ACMaster.excludetax='Yes') THEN SIItems.cstamt ELSE 0 END))) as totamount,sum(SIItems.basicamount) as totbasic,sum(SIItems.vatamt) as totvat,SUM(SIItems.addtaxamt) as totadvat,SUM((CASE WHEN ((ACMaster.excludetax is null or ACMaster.excludetax='' or ACMaster.excludetax='No')) THEN SIItems.cstamt ELSE 0 END)) as totcst from SIItems inner join ItemMaster on ItemMaster.itemname=SIItems.itemname inner join SIMaster on simaster.strsino=SIItems.strsino inner join ACMaster on SIMaster.salesac=ACMaster.acname where SIItems.sidate between @fromdate and @todate group by itemmaster.hsncode,ItemMaster.itemname,ItemMaster.unitewaybill", con);
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);
        if (dtdata.Rows.Count > 0)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "GSTHSN" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            gvhsn.GridLines = GridLines.Both;
            gvhsn.HeaderStyle.Font.Bold = true;
            gvhsn.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No HSN data available for export.');", true);
            return;
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }

    protected void btngsthsn_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
        //{
            fillgridhsn();
            ExportGridToExcel1();
       // }
    }
}