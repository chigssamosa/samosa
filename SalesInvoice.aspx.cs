﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Net;

public partial class SalesInvoice : System.Web.UI.Page
{
    ForSalesInvoice fsiclass = new ForSalesInvoice();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                getsino();
                fillacnamedrop();
                fillsalesacnamedrop();
                fillitemnamedrop();
                fillinvtypedrop();
                fillstatusdrop();
                fillactaxdesc();
                Session["dtpitemssain12"] = null;
                Session["dtpitemssain2"] = null;
                txtsidate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemssain"] = CreateTemplate();
                Page.SetFocus(drpinvtype);
            }
            else if (Request["mode"].ToString() == "ledger")
            {
                fillacnamedrop();
                fillsalesacnamedrop();
                fillitemnamedrop();
                fillinvtypedrop();
                fillstatusdrop();
                filleditdata();
                fillactaxdesc();
                //hideimage();
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                fillacnamedrop();
                fillsalesacnamedrop();
                fillitemnamedrop();
                fillinvtypedrop();
                fillstatusdrop();
                filleditdata();
                fillactaxdesc();
                //hideimage();
            }
            //hideimage();

        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillsalesacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallsalesacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpsalesac.Items.Clear();
            drpsalesac.DataSource = dtdata;
            drpsalesac.DataTextField = "acname";
            drpsalesac.DataBind();
            drpsalesac.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpsalesac.Items.Clear();
            drpsalesac.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    private void SetTabIndexes()
    {
        short currentTabIndex = 0;
        txtamount.TabIndex = ++currentTabIndex;

        foreach (GridViewRow gvr in gvsiitemlist.Rows)
        {
            TextBox txtgridqty = (TextBox)gvr.FindControl("txtgridqty");
            txtgridqty.TabIndex = ++currentTabIndex;

            TextBox txtgvstockqty = (TextBox)gvr.FindControl("txtgvstockqty");
            txtgvstockqty.TabIndex = ++currentTabIndex;

            TextBox txtgridrate = (TextBox)gvr.FindControl("txtgridrate");
            txtgridrate.TabIndex = ++currentTabIndex;

            TextBox txtgridamount = (TextBox)gvr.FindControl("txtgridamount");
            txtgridamount.TabIndex = ++currentTabIndex;

            TextBox txtgridtaxtype = (TextBox)gvr.FindControl("txtgridtaxtype");
            txtgridtaxtype.TabIndex = ++currentTabIndex;

            TextBox txtgridcstp = (TextBox)gvr.FindControl("txtgridcstp");
            txtgridcstp.TabIndex = ++currentTabIndex;

            TextBox txtdesc1 = (TextBox)gvr.FindControl("txtdesc1");
            txtdesc1.TabIndex = ++currentTabIndex;

            TextBox txtdesc2 = (TextBox)gvr.FindControl("txtdesc2");
            txtdesc2.TabIndex = ++currentTabIndex;

            TextBox txtdesc3 = (TextBox)gvr.FindControl("txtdesc3");
            txtdesc3.TabIndex = ++currentTabIndex;

            TextBox txtgridtaxdesc = (TextBox)gvr.FindControl("txtgridtaxdesc");
            txtgridtaxdesc.TabIndex = ++currentTabIndex;
        }

        txttotaddtax.TabIndex = ++currentTabIndex;
    }

    public void hideimage()
    {
        for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvsiitemlist.Rows[c].FindControl("imgbtnselect11");
            if (btnsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillactaxdesc()
    {
        DataTable acdesc = new DataTable();
        DataTable acdesc1 = new DataTable();
        acdesc = fsiclass.selectalltaxacdesc();
        acdesc1 = fsiclass.selectalltaxacdescfrommisc();
        if (acdesc.Rows.Count > 0)
        {
            ViewState["vatdesc"] = acdesc.Rows[0]["vatdesc"].ToString();
            ViewState["addtaxdesc"] = acdesc.Rows[0]["addtaxdesc"].ToString();
            ViewState["cstdesc"] = acdesc.Rows[0]["cstdesc"].ToString();
            ViewState["servicetaxdesc"] = acdesc.Rows[0]["servicetaxdesc"].ToString();
            drptaxdesc.DataSource = acdesc1;
            drptaxdesc.DataTextField = "name";
            drptaxdesc.DataBind();
            drptaxdesc.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drptaxdesc.Items.Clear();
            drptaxdesc.Items.Insert(0, "--SELECT--");
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Account type for VAT , Addvat , CST and service tax is not avaailable so go to VAT type Master add ledger account type and try again.');", true);
            return;
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillinvtypedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallinvtype(li);
        if (dtdata.Rows.Count > 0)
        {
            drpinvtype.Items.Clear();
            drpinvtype.DataSource = dtdata;
            drpinvtype.DataTextField = "name";
            drpinvtype.DataBind();
            drpinvtype.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpinvtype.Items.Clear();
            drpinvtype.Items.Insert(0, "--SELECT--");
        }
    }

    public void getsino()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fsiclass.selectunusedsalesinvoiceno(li);
        dtvno = fsiclass.selectunusedsalesinvoicenolast(li);
        if (dtvno.Rows.Count > 0)
        {
            txtinvoiceno.Text = (Convert.ToInt64(dtvno.Rows[0]["sino"].ToString()) + 1).ToString();
        }
        else
        {
            txtinvoiceno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strsino = Request["strsino"].ToString();

        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
        if (dtdata.Rows.Count > 0)
        {
            txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
            ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
            txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
            ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
            txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
            if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
            {
                txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtscno.Text = dtdata.Rows[0]["scno"].ToString();
            txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
            if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
            {
                txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();

            li.acname = drpsalesac.SelectedItem.Text;
            DataTable dtata = fsiclass.selectallacdatefromacname(li);
            ViewState["excludetax"] = dtata.Rows[0]["excludetax"].ToString();

            txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
            txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
            txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
            txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
            txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
            txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
            txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
            txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
            txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
            txtform.Text = dtdata.Rows[0]["form"].ToString();
            txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
            txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
            txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
            //{
            //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            //}
            //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
            //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
            DataTable dtitems = new DataTable();
            dtitems = fsiclass.selectallsiitemsfromsinostring(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsiitemlist.Visible = true;
                gvsiitemlist.DataSource = dtitems;
                gvsiitemlist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Invoice Item Found.";
                lblcount.Text = "0";
            }
            btnsave.Text = "Update";
        }
    }

    public void filleditdataduringdelete()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;

        DataTable dtdata = new DataTable();
        dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
        if (dtdata.Rows.Count > 0)
        {
            txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
            ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
            txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
            ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
            txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
            if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
            {
                txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtscno.Text = dtdata.Rows[0]["scno"].ToString();
            txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
            if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
            {
                txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();
            txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
            txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
            txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
            txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
            txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
            txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
            txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
            txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
            txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
            txtform.Text = dtdata.Rows[0]["form"].ToString();
            txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
            txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
            txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
            //{
            //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            //}
            //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
            //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
            DataTable dtitems = new DataTable();
            dtitems = fsiclass.selectallsiitemsfromsinostring(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsiitemlist.Visible = true;
                gvsiitemlist.DataSource = dtitems;
                gvsiitemlist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Invoice Item Found.";
                lblcount.Text = "0";
            }
            btnsave.Text = "Update";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("vno", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("stockqty", typeof(double));
        dtpitems.Columns.Add("qtyremain", typeof(double));
        dtpitems.Columns.Add("qtyused", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("hsncode", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvatdesc(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectalltaxacdescfrommiscautocomp(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsono(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsono(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getscno(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallscno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsalesac(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsalesac(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsalesman(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsalesman(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtcode.Text.Trim() != string.Empty)
        {
            string cc = txtcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtcode.Text = string.Empty;
        }
        Page.SetFocus(txtdescription1);
    }

    public void count1()
    {
        if (txtbillqty.Text.Trim() == string.Empty)
        {
            txtbillqty.Text = "0";
        }
        if (txtrate.Text.Trim() == string.Empty)
        {
            txtrate.Text = "0";
        }
        txtbasicamt.Text = Math.Round(((Convert.ToDouble(txtbillqty.Text)) * (Convert.ToDouble(txtrate.Text))), 2).ToString();
        txtrate.Text = Math.Round(Convert.ToDouble(txtrate.Text), 2).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtbasicamt.Text);
        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvat.Text = vatp.ToString();
                txtgvadtax.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }

            }
            else
            {
                txtgvvat.Text = "0";
                txtgvadtax.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvat.Text = "0";
            txtgvadtax.Text = "0";
        }

        if (txtgvvat.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvat.Text);
            txtvat.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvat.Text = "0";
            txtvat.Text = "0";
        }
        if (txtgvadtax.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadtax.Text);
            txtaddtax.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadtax.Text = "0";
            txtaddtax.Text = "0";
        }
        if (Convert.ToDouble(txtgvvat.Text) != 0)
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        else
        {
            if (txtgvcst.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgvcst.Text);
                txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgvcst.Text = "0";
                txtcstamount.Text = "0";
            }
        }
        vatamt = Convert.ToDouble(txtvat.Text);
        addvatamt = Convert.ToDouble(txtaddtax.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        if (gvsiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                }
                if (lbladdvatamt.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                }
                if (lblamount.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + serant).ToString();
            //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 >= 0.5)
            //{
            //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            //txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }

    }

    public void countfooter()
    {
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        if (gvsiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                }
                if (lbladdvatamt.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                }
                if (lblamount.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + serant).ToString();
            //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 >= 0.5)
            //{
            //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            //txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }

    }

    public void count11()
    {
        //txtbasicamt.Text = ((Convert.ToDouble(txtbillqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        //TextBox txtgridamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
        //TextBox txtgridtaxtype = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridtaxtype");
        //TextBox txtgridcstp = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridcstp");



        //Label lblvatp = (Label)gvsiitemlist.Rows[c].FindControl("lblvatp");
        //Label lbladdvatp = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatp");
        //Label lblcstamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
        //Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
        //Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
        //Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
        //double vatp = 0;
        //double addvatp = 0;
        //double cstp = 0;
        //double vatamt = 0;
        //double addvatamt = 0;
        //double cstamt = 0;
        //double baseamt = Convert.ToDouble(txtgridamount.Text);
        //if (txtgridtaxtype.Text != string.Empty)
        //{
        //    var cc = txtgridtaxtype.Text.IndexOf("-");
        //    if (cc != -1)
        //    {
        //        li.typename = txtgridtaxtype.Text;
        //        vatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[0]);
        //        addvatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[1]);
        //        txtgvvat.Text = vatp.ToString();
        //        txtgvadtax.Text = addvatp.ToString();
        //        DataTable dtdtax = new DataTable();
        //        dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
        //        if (dtdtax.Rows.Count > 0)
        //        {
        //            txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
        //        }

        //    }
        //}

        //if (txtgvvat.Text != string.Empty)
        //{
        //    vatp = Convert.ToDouble(txtgvvat.Text);
        //    txtvat.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtgvvat.Text = "0";
        //    txtvat.Text = "0";
        //}
        //if (txtgvadtax.Text != string.Empty)
        //{
        //    addvatp = Convert.ToDouble(txtgvadtax.Text);
        //    txtaddtax.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtgvadtax.Text = "0";
        //    txtaddtax.Text = "0";
        //}
        //if (txtgvcst.Text != string.Empty)
        //{
        //    cstp = Convert.ToDouble(txtgvcst.Text);
        //    txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtgvcst.Text = "0";
        //    txtcstamount.Text = "0";
        //}
        //vatamt = Convert.ToDouble(txtvat.Text);
        //addvatamt = Convert.ToDouble(txtaddtax.Text);
        //cstamt = Convert.ToDouble(txtcstamount.Text);
        //txtamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        //if (gvsiitemlist.Rows.Count > 0)
        //{
        //    double totbasicamt = 0;
        //    double totcst = 0;
        //    double totvat = 0;
        //    double totaddvat = 0;
        //    double totamount = 0;
        //    for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
        //    {
        //        TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
        //        Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
        //        Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
        //        Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
        //        Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
        //        totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
        //        if (lblsctamt.Text != string.Empty)
        //        {
        //            totcst = totcst + Convert.ToDouble(lblsctamt.Text);
        //        }
        //        if (lblvatamt.Text != string.Empty)
        //        {
        //            totvat = totvat + Convert.ToDouble(lblvatamt.Text);
        //        }
        //        if (lbladdvatamt.Text != string.Empty)
        //        {
        //            totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
        //        }
        //        if (lblamount.Text != string.Empty)
        //        {
        //            totamount = totamount + Convert.ToDouble(lblamount.Text);
        //        }
        //    }
        //    txttotbasicamount.Text = totbasicamt.ToString();
        //    txttotvat.Text = totvat.ToString();
        //    txttotaddtax.Text = totaddvat.ToString();
        //    txttotcst.Text = totcst.ToString();
        //    double cartage = 0;
        //    if (txtcartage.Text.Trim() != string.Empty)
        //    {
        //        cartage = Convert.ToDouble(txtcartage.Text);
        //    }
        //    else
        //    {
        //        txtcartage.Text = "0";
        //    }
        //    double servp = 0;
        //    double servamt = 0;
        //    double roundoff = 0;
        //    if (txtservicep.Text.Trim() != string.Empty)
        //    {
        //        txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        //    }
        //    else
        //    {
        //        txtservicep.Text = "0";
        //        txtserviceamt.Text = "0";
        //    }
        //    if (txtroundoff.Text.Trim() != string.Empty)
        //    {
        //        roundoff = Convert.ToDouble(txtroundoff.Text);
        //    }
        //    else
        //    {
        //        txtroundoff.Text = "0";
        //    }
        //    double serant = 0;
        //    if (txtserviceamt.Text.Trim() != string.Empty)
        //    {
        //        serant = Convert.ToDouble(txtserviceamt.Text);
        //    }
        //    txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
        //}
        //else
        //{
        //    txttotbasicamount.Text = "0";
        //    txttotvat.Text = "0";
        //    txttotaddtax.Text = "0";
        //    txttotcst.Text = "0";
        //    txtbillamount.Text = "0";
        //}

    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                string vattype = dtdata.Rows[0]["gsttype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvat.Text = "0";
                    txtgvadtax.Text = vattype.Split('-')[1];
                    txtaddtax.Text = "0";
                    count1();
                }
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadtax.Text = string.Empty;
                txtvat.Text = string.Empty;
                txtaddtax.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtamount.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadtax.Text = string.Empty;
            txtvat.Text = string.Empty;
            txtaddtax.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtamount.Text = string.Empty;
        }
    }
    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvat);
    }

    protected void txtservicep_TextChanged(object sender, EventArgs e)
    {
        countgv();
        Page.SetFocus(txtserviceamt);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemssain"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvsiitemlist.Visible = true;
            lblempty.Visible = false;
            gvsiitemlist.DataSource = dtsession;
            gvsiitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvsiitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    protected void gvsiitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemssain"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemssain"] = dt;
            this.bindgrid();
            countgv();
        }
        else
        {
            if (gvsiitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblvid");
                li.id = Convert.ToInt64(lblid.Text);
                li.vid = Convert.ToInt64(lblvid.Text);
                fsiclass.deletesiitemsdatafromid(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Sales Invoice Item Deleted.";
                faclass.insertactivity(li);
                Label lblitemname = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblitemname");
                Label lblqty = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqty");
                //Label lblqtyremain = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyremain");
                //Label lblqtyused = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyused");
                Label lblvno = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblvno");

                li.itemname = lblitemname.Text;
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.scno = Convert.ToInt64(lblvno.Text);
                if (li.vid != 0)
                {
                    DataTable dtqty = new DataTable();
                    dtqty = fsiclass.selectqtyremainusedfromscno(li);
                    li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                    li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                    fsiclass.updateqtyduringdelete(li);
                    li.vid = Convert.ToInt64(lblvid.Text);
                    li.strsino = "";
                    fsiclass.updatestrsinoinscitems1(li);
                }

                //li.istype = "CR";
                //flclass.deleteledgerdata(li);
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallsiitemsfromsinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsiitemlist.Visible = true;
                    gvsiitemlist.DataSource = dtdata;
                    gvsiitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvsiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Invoice Items Found.";
                    lblcount.Text = "0";
                }
                //update all data during delete
                countfooter();
                li.totbasicamount = Convert.ToDouble(txttotbasicamount.Text);
                li.totvatamt = Convert.ToDouble(txttotvat.Text);
                li.totaddtaxamt = Convert.ToDouble(txttotaddtax.Text);
                li.totcstamt = Convert.ToDouble(txttotcst.Text);
                li.servicetaxp = Convert.ToDouble(txtservicep.Text);
                li.servicetaxamount = Convert.ToDouble(txtserviceamt.Text);
                li.cartage = Convert.ToDouble(txtcartage.Text);
                li.roundoff = Convert.ToDouble(txtroundoff.Text);
                li.billamount = Convert.ToDouble(txtbillamount.Text);
                li.remainamount = Convert.ToDouble(txtbillamount.Text);
                fsiclass.updatesimasterdataduringdelete(li);

                fsiclass.deletesalesinvoiceledgeraa(li);
                Label lblccodee = (Label)gvsiitemlist.Rows[0].FindControl("lblccode");

                //1
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = drpacname.SelectedItem.Text;
                li.creditcode = drpsalesac.SelectedItem.Text;
                li.description = "totald";
                li.istype1 = "D";
                //li.amount = Convert.ToDouble(txtbillamount.Text);
                //if (Convert.ToDouble(txtroundoff.Text) <= 0)
                //{
                if (ViewState["excludetax"].ToString() == "Yes")//if (drpsalesac.SelectedItem.Text == "Sales A/c. IGST - Export" || drpsalesac.SelectedItem.Text == "Sales A/c. - EOU")
                {
                    li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text);// + Convert.ToDouble(txtroundoff.Text)
                }
                else
                {
                    li.amount = Convert.ToDouble(txtbillamount.Text);
                }
                //}
                //else
                //{
                //    li.amount = Convert.ToDouble(txtbillamount.Text) + Convert.ToDouble(txtroundoff.Text);
                //}
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
                //2
                li.description = "";
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = drpsalesac.SelectedItem.Text;
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totalc";
                li.istype1 = "C";
                //li.amount = Convert.ToDouble(txtbillamount.Text);
                if (txtcartage.Text == string.Empty && txtcartage.Text == "")
                {
                    txtcartage.Text = "0";
                }
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text);// + Convert.ToDouble(txtroundoff.Text)
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
                //}

                //3
                if (ViewState["excludetax"].ToString() != "Yes")//if (drpsalesac.SelectedItem.Text != "Sales A/c. IGST - Export" && drpsalesac.SelectedItem.Text != "Sales A/c. - EOU")
                {
                    if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
                    {
                        li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        li.debitcode = ViewState["cstdesc"].ToString();
                        li.creditcode = drpacname.SelectedItem.Text;
                        li.description = "cst";
                        li.istype1 = "C";
                        li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                        li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        //li.voucherno = SessionMgt.voucherno;
                        li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                        li.refid = 0;
                        li.ccode = Convert.ToInt64(lblccodee.Text);
                        li.istype = "SI";
                        fsiclass.insertledgerdata(li);
                    }
                    //4
                    if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
                    {
                        li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        li.debitcode = ViewState["vatdesc"].ToString();
                        li.creditcode = drpacname.SelectedItem.Text;
                        li.description = "totvat";
                        li.istype1 = "C";
                        li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                        li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        //li.voucherno = SessionMgt.voucherno;
                        li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                        li.refid = 0;
                        li.ccode = Convert.ToInt64(lblccodee.Text);
                        li.istype = "SI";
                        fsiclass.insertledgerdata(li);
                    }
                }
                //5
                if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["servicetaxdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "setvicetax";
                    li.istype1 = "C";
                    li.amount = Convert.ToDouble(txtserviceamt.Text);
                    li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "SI";
                    fsiclass.insertledgerdata(li);
                }
                filleditdataduringdelete();
                //
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        hideimage();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.sidate <= yyyear1 && li.sidate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (btnsave.Text == "Save")
        //{
        //    for (int p = 0; p < gvsiitemlist.Rows.Count; p++)
        //    {
        //        TextBox txtgvstockqty = (TextBox)gvsiitemlist.Rows[p].FindControl("txtgvstockqty");
        //        if (Convert.ToDouble(txtgvstockqty.Text) <= 0 || txtgvstockqty.Text.Trim() == string.Empty)
        //        {
        //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Stock Qty must be greater than 0(Zero).');", true);
        //            return;
        //        }
        //    }
        //}

        //Check Item balance is same to Bill amount or not
        double itemtot = 0;
        double billtot = 0;
        for (int p = 0; p < gvsiitemlist.Rows.Count; p++)
        {
            Label lblamount = (Label)gvsiitemlist.Rows[p].FindControl("lblamount");
            itemtot = itemtot + Convert.ToDouble(lblamount.Text);
        }
        itemtot = Math.Round(itemtot, 2) + Convert.ToDouble(txtroundoff.Text);
        billtot = Convert.ToDouble(txtbillamount.Text);
        if (billtot != itemtot)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Bill Amount & Item Total Mismatch.Please Update data and try again.');", true);
            return;
        }
        //
        //Check Ledger Entry Same OR Not
        double ctot = 0;
        double dtot = 0;
        if (ViewState["excludetax"].ToString() != "Yes")//if (drpsalesac.SelectedItem.Text != "Sales A/c. IGST - Export" && drpsalesac.SelectedItem.Text != "Sales A/c. - EOU")
        {
            //if (Convert.ToDouble(txtroundoff.Text) >= 0)
            //{
            ctot = Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txtroundoff.Text)), 2);
            dtot = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
            if (ctot != dtot)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Amount Mismatch.Please Update data and try again.');", true);
                return;
            }
            //}
            //else
            //{
            //    ctot = Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text) - Convert.ToDouble(txtroundoff.Text);
            //    dtot = Convert.ToDouble(txtbillamount.Text);
            //    if (ctot != dtot)
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Amount Mismatch.Please Update data and try again.');", true);
            //        return;
            //    }
            //}
        }

        for (int w = 0; w < gvsiitemlist.Rows.Count; w++)
        {
            TextBox txttt = (TextBox)gvsiitemlist.Rows[w].FindControl("txtgridtaxdesc");
            if (txttt.Text.Trim() == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter tax desc for all item and try again.');", true);
                return;
            }
        }
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.sino = Convert.ToInt64(txtinvoiceno.Text);
        li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.invtype = drpinvtype.SelectedItem.Text;
        //if (txtorderno.Text.Trim() != string.Empty)
        //{
        li.strsono = txtorderno.Text;
        //}
        //else
        //{
        //    li.sono = 0;
        //}
        if (txtsodate.Text.Trim() != string.Empty)
        {
            li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.sodate = sqldatenull;
        }
        if (txtscno.Text.Trim() != string.Empty)
        {
            li.strscno = txtscno.Text;
        }
        else
        {
            li.strscno = "0";
        }
        li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtscno1.Text.Trim() != string.Empty)
        {
            li.strscno1 = txtscno1.Text;
        }
        else
        {
            li.strscno1 = "0";
        }
        if (txtscdate1.Text.Trim() != string.Empty)
        {
            li.scdate1 = Convert.ToDateTime(txtscdate1.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.scdate1 = sqldatenull;
        }
        li.acname = drpacname.SelectedItem.Text;
        li.salesac = drpsalesac.SelectedItem.Text;
        li.totbasicamount = Convert.ToDouble(txttotbasicamount.Text);
        li.totvatamt = Convert.ToDouble(txttotvat.Text);
        li.totaddtaxamt = Convert.ToDouble(txttotaddtax.Text);
        li.totcstamt = Convert.ToDouble(txttotcst.Text);
        li.servicetaxp = Convert.ToDouble(txtservicep.Text);
        li.servicetaxamount = Convert.ToDouble(txtserviceamt.Text);
        li.cartage = Convert.ToDouble(txtcartage.Text);
        li.roundoff = Convert.ToDouble(txtroundoff.Text);
        li.billamount = Convert.ToDouble(txtbillamount.Text);
        li.remainamount = Convert.ToDouble(txtbillamount.Text);
        li.type = "SALES";
        li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.stringscno = txtsrtringscno.Text;
        li.sbillno = txtbillno.Text;
        if (txtbilldate.Text.Trim() != string.Empty)
        {
            li.sbilldate = Convert.ToDateTime(txtbilldate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.sbilldate = sqldatenull;
        }
        li.portcode = txtportcode.Text;
        li.cmon = Convert.ToInt16(txtsidate.Text.Split('-')[1]);
        if (li.cmon == 1)
        {
            li.cmonname = "JANUARY";
        }
        else if (li.cmon == 2)
        {
            li.cmonname = "FEBRUARY";
        }
        else if (li.cmon == 3)
        {
            li.cmonname = "MARCH";
        }
        else if (li.cmon == 4)
        {
            li.cmonname = "APRIL";
        }
        else if (li.cmon == 5)
        {
            li.cmonname = "MAY";
        }
        else if (li.cmon == 6)
        {
            li.cmonname = "JUNE";
        }
        else if (li.cmon == 7)
        {
            li.cmonname = "JULY";
        }
        else if (li.cmon == 8)
        {
            li.cmonname = "AUGUST";
        }
        else if (li.cmon == 9)
        {
            li.cmonname = "SEPTEMBER";
        }
        else if (li.cmon == 10)
        {
            li.cmonname = "OCTOBER";
        }
        else if (li.cmon == 11)
        {
            li.cmonname = "NOVEMBER";
        }
        else
        {
            li.cmonname = "DECEMBER";
        }
        li.monyr = li.cmonname + "-" + txtsidate.Text.Split('-')[2];
        if (txtremakrs.Text != string.Empty)
        {
            li.remarks = txtremakrs.Text;
        }
        else
        {
            li.remarks = "IMMEDIATE";
        }
        li.transportname = txttransportname.Text;
        li.form = txtform.Text;
        li.salesmanname = txtsalesman.Text;
        li.lrno = txtlrno.Text;
        if (txtlrdate.Text != string.Empty)
        {
            li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.lrdate = sqldatenull;
        }
        if (txtduedays.Text.Trim() != string.Empty)
        {
            li.duedays = Convert.ToDouble(txtduedays.Text);
        }
        else
        {
            li.duedays = 0;
        }
        if (btnsave.Text == "Save")
        {
            li.dtors = 0;
            DataTable dtcheck = new DataTable();
            dtcheck = fsiclass.selectallsimasterdatafromsinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                fsiclass.insertsimasterdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strsino + " Sales Invoice Inserted.";
                faclass.insertactivity(li);
                li.stringscno = txtsrtringscno.Text;
                //fsiclass.updatescstatusforallchno(li);
                //if (txtscno.Text.Trim() != string.Empty)
                //{
                //    li.strscno = txtscno.Text;
                //    fsiclass.updatescstatus(li);                    
                //}
                //if (txtscno1.Text.Trim() != string.Empty)
                //{
                //    li.strscno = txtscno1.Text;
                //    fsiclass.updatescstatus(li);
                //}
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fsiclass.updateisused(li);
                getsino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Invoice No. already exist.Try Again.');", true);
                return;
            }
            //li.sino = SessionMgt.voucherno;
            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            {
                //DataTable dtpitems = new DataTable();
                //dtpitems.Columns.Add("id", typeof(Int64));
                //dtpitems.Columns.Add("itemname", typeof(string));
                //dtpitems.Columns.Add("qty", typeof(double));
                //dtpitems.Columns.Add("rate", typeof(double));
                //dtpitems.Columns.Add("basicamount", typeof(double));
                //dtpitems.Columns.Add("taxtype", typeof(string));
                //dtpitems.Columns.Add("vatp", typeof(double));
                //dtpitems.Columns.Add("vatamt", typeof(double));
                //dtpitems.Columns.Add("addtaxp", typeof(double));
                //dtpitems.Columns.Add("addtaxamt", typeof(double));
                //dtpitems.Columns.Add("cstp", typeof(double));
                //dtpitems.Columns.Add("cstamt", typeof(double));
                //dtpitems.Columns.Add("unit", typeof(string));
                //dtpitems.Columns.Add("amount", typeof(double));
                //dtpitems.Columns.Add("ccode", typeof(string));
                //dtpitems.Columns.Add("descr1", typeof(string));
                //dtpitems.Columns.Add("descr2", typeof(string));
                //dtpitems.Columns.Add("descr3", typeof(string));

                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                Label lblid = (Label)gvsiitemlist.Rows[c].FindControl("lblid");
                Label lblvno = (Label)gvsiitemlist.Rows[c].FindControl("lblvno");
                Label lblitemname = (Label)gvsiitemlist.Rows[c].FindControl("lblitemname");
                TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                TextBox txtgvstockqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgvstockqty");
                Label lblqtyremain = (Label)gvsiitemlist.Rows[c].FindControl("lblqtyremain");
                Label lblqtyused = (Label)gvsiitemlist.Rows[c].FindControl("lblqtyused");
                TextBox lblrate = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridrate");
                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                TextBox lbltaxtype = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridtaxtype");
                Label lblvatp = (Label)gvsiitemlist.Rows[c].FindControl("lblvatp");
                Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatp = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatp");
                Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                TextBox lblcstp = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridcstp");
                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblunit = (Label)gvsiitemlist.Rows[c].FindControl("lblunit");
                TextBox txtgvunit = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgvunit");
                Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                Label lblccode = (Label)gvsiitemlist.Rows[c].FindControl("lblccode");
                TextBox lbldesc1 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc3");
                TextBox lbltaxdesc = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridtaxdesc");



                li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                //if (li.qty > 0)
                //{
                li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
                li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
                li.unit = txtgvunit.Text;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.taxtype = lbltaxtype.Text;
                li.taxdesc = lbltaxdesc.Text;
                if (lblvatp.Text != "")
                {
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                }
                else
                {
                    li.vatp = 0;
                }
                if (lbladdvatp.Text != "")
                {
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                }
                else
                {
                    li.addtaxp = 0;
                }
                if (lblcstp.Text != "")
                {
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                }
                else
                {
                    li.cstp = 0;
                }
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.basicamount = Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != "")
                {
                    li.cstamt = Math.Round(Convert.ToDouble(lblsctamt.Text), 2);
                }
                else
                {
                    li.cstamt = 0;
                }
                if (lblvatamt.Text != "")
                {
                    li.vatamt = Math.Round(Convert.ToDouble(lblvatamt.Text), 2);
                }
                else
                {
                    li.vatamt = 0;
                }
                if (lbladdvatamt.Text != "")
                {
                    li.addtaxamt = Math.Round(Convert.ToDouble(lbladdvatamt.Text), 2);
                }
                else
                {
                    li.addtaxamt = 0;
                }
                if (lblamount.Text != "")
                {
                    li.amount = Convert.ToDouble(lblamount.Text);
                }
                else
                {
                    li.amount = 0;
                }
                if (lblccode.Text != "")
                {
                    li.ccode = Convert.ToInt64(lblccode.Text);
                }
                else
                {
                    li.ccode = 0;
                }
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.vid = Convert.ToInt16(lblid.Text);
                fsiclass.insertsiitemsdata(li);
                li.vnono1 = Convert.ToInt64(lblvno.Text);
                li.id = Convert.ToInt16(lblid.Text);
                fsiclass.updateremainqtyinsaleschallan(li);
                li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                fsiclass.updatestrsinoinscitems(li);
                //}
            }
            fsiclass.updateisused(li);
            //Sales Invoice Ledger Entry            
            //for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            //{          
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            fsiclass.deletesalesinvoiceledgeraa(li);
            Label lblccodee = (Label)gvsiitemlist.Rows[0].FindControl("lblccode");

            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drpsalesac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "D";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            //if (Convert.ToDouble(txtroundoff.Text) <= 0)
            //{
            if (ViewState["excludetax"].ToString() == "Yes")//if (drpsalesac.SelectedItem.Text == "Sales A/c. IGST - Export" || drpsalesac.SelectedItem.Text == "Sales A/c. - EOU")
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2);// + Convert.ToDouble(txtroundoff.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txtroundoff.Text)
            }
            else
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            //}
            //else
            //{
            //    li.amount = Convert.ToDouble(txtbillamount.Text) + Convert.ToDouble(txtroundoff.Text);
            //}
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpsalesac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "C";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text == string.Empty && txtcartage.Text == "")
            {
                txtcartage.Text = "0";
            }
            if (ViewState["excludetax"].ToString() == "Yes")//if (drpsalesac.SelectedItem.Text == "Sales A/c. IGST - Export" || drpsalesac.SelectedItem.Text == "Sales A/c. - EOU")
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2);// + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtserviceamt.Text)
            }
            else
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text);// + Convert.ToDouble(txtroundoff.Text)
            }
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //}

            //3
            if (ViewState["excludetax"].ToString() != "Yes")//if (drpsalesac.SelectedItem.Text != "Sales A/c. IGST - Export" && drpsalesac.SelectedItem.Text != "Sales A/c. - EOU")
            {
                if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["cstdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "cst";
                    li.istype1 = "C";
                    li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                    li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "SI";
                    fsiclass.insertledgerdata(li);
                }
                //4
                if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["vatdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "totvat";
                    li.istype1 = "C";
                    li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                    li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "SI";
                    fsiclass.insertledgerdata(li);
                }
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
            }
            ////////////6
            //////////if (txttotaddtax.Text != string.Empty && txttotaddtax.Text != "0" && Convert.ToDouble(txttotaddtax.Text) > 0)
            //////////{
            //////////    li.strvoucherno = "";
            //////////    li.debitcode = txtsalesac.Text;
            //////////    li.creditcode = txtname.Text;
            //////////    li.description = "totaddtax";
            //////////    li.istype1 = "C";
            //////////    li.amount = Convert.ToDouble(txttotaddtax.Text);
            //////////    li.voucherdate = Convert.ToDateTime(txtsidate.Text);
            //////////    li.voucherno = SessionMgt.voucherno;
            //////////    li.refid = 0;
            //////////    li.ccode = Convert.ToInt64(lblccodee.Text);
            //////////    li.istype = "SI";
            //////////    fsiclass.insertledgerdata(li);
            //////////}
            ////////////7
            //////////if (txtcartage.Text != string.Empty && txtcartage.Text != "0" && Convert.ToDouble(txtcartage.Text) > 0)
            //////////{
            //////////    li.strvoucherno = "";
            //////////    li.debitcode = txtsalesac.Text;
            //////////    li.creditcode = txtname.Text;
            //////////    li.description = "cartage";
            //////////    li.istype1 = "C";
            //////////    li.amount = Convert.ToDouble(txtcartage.Text);
            //////////    li.voucherdate = Convert.ToDateTime(txtsidate.Text);
            //////////    li.voucherno = SessionMgt.voucherno;
            //////////    li.refid = 0;
            //////////    li.ccode = Convert.ToInt64(lblccodee.Text);
            //////////    li.istype = "SI";
            //////////    fsiclass.insertledgerdata(li);
            //////////}
            //
        }
        else
        {
            if (ViewState["sino"].ToString().Trim() != txtinvoiceno.Text.Trim() || ViewState["invtype"].ToString().Trim() != drpinvtype.SelectedItem.Text)
            {
                li.sino1 = Convert.ToInt64(txtinvoiceno.Text);
                li.sino = Convert.ToInt64(ViewState["sino"].ToString());
                li.strsino = ViewState["invtype"].ToString() + ViewState["sino"].ToString();
                li.strsino1 = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.invtype = ViewState["invtype"].ToString();
                li.invtype1 = drpinvtype.SelectedItem.Text;
                fsiclass.updatesimasterdataupdatesistring(li);
                fsiclass.updatesiitemsdataupdatesistring(li);
                fsiclass.updateledgerdataupdatestrvouchernostring(li);
                fsiclass.deletesimasterdatafromsinostring(li);
                fsiclass.deletesiitemsdatafromsinostring(li);
            }
            if (txtdtors.Text.Trim() != string.Empty)
            {
                li.dtors = Convert.ToDouble(txtdtors.Text);
            }
            else
            {
                li.dtors = 0;
            }
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            fsiclass.deletesalesinvoiceledgeraa(li);
            li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.invtype = drpinvtype.SelectedItem.Text;
            fsiclass.updatesimasterdatastring(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            fsiclass.updatesiitemsdatedataupdatesistring(li);

            //update all data together
            for (int y = 0; y < gvsiitemlist.Rows.Count; y++)
            {
                Label lblid = (Label)gvsiitemlist.Rows[y].FindControl("lblid");
                Label lblqty = (Label)gvsiitemlist.Rows[y].FindControl("lblqty");
                TextBox txtgvqty1 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridqty");
                li.qty = Convert.ToDouble(lblqty.Text);
                li.id = Convert.ToInt64(lblid.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                DataTable dtremain = new DataTable();
                dtremain = fsiclass.selectqtyremainusedfromscno(li);
                if (Convert.ToDouble(txtgvqty1.Text) > 0)
                {
                    if (dtremain.Rows.Count > 0)
                    {
                        //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                        li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                        fsiclass.updateremainqtyinsaleschallan(li);


                        //update siitems data
                        //for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                        //{   
                        Label lblvid = (Label)gvsiitemlist.Rows[y].FindControl("lblvid");
                        Label lblitemname = (Label)gvsiitemlist.Rows[y].FindControl("lblitemname");
                        //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblrate = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridrate");
                        TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbltaxtype = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvsiitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvsiitemlist.Rows[y].FindControl("lblcstamt");
                        Label lblunit = (Label)gvsiitemlist.Rows[y].FindControl("lblunit");
                        TextBox txtgvunit = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvunit");
                        Label lblamount = (Label)gvsiitemlist.Rows[y].FindControl("lblamount");
                        Label lblccode = (Label)gvsiitemlist.Rows[y].FindControl("lblccode");
                        TextBox lbldesc1 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgridtaxdesc = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxdesc");
                        TextBox txtgvstockqty = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvstockqty");
                        li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                        li.id = Convert.ToInt64(lblid.Text);
                        li.itemname = lblitemname.Text;
                        if (txtgvstockqty.Text.Trim() != string.Empty)
                        {
                            li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        }
                        else
                        {
                            li.stockqty = 0;
                        }
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)
                        //{
                        li.unit = txtgvunit.Text;
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.taxtype = lbltaxtype.Text;
                        li.taxdesc = txtgridtaxdesc.Text;
                        if (lblvatp.Text != "")
                        {
                            li.vatp = Convert.ToDouble(lblvatp.Text);
                        }
                        else
                        {
                            li.vatp = 0;
                        }
                        if (lbladdvatp.Text != "")
                        {
                            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        }
                        else
                        {
                            li.addtaxp = 0;
                        }
                        if (lblcstp.Text != "")
                        {
                            li.cstp = Convert.ToDouble(lblcstp.Text);
                        }
                        else
                        {
                            li.cstp = 0;
                        }
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                        if (lblsctamt.Text != "")
                        {
                            li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                        }
                        else
                        {
                            li.cstamt = 0;
                        }
                        if (lblvatamt.Text != "")
                        {
                            li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                        }
                        else
                        {
                            li.vatamt = 0;
                        }
                        if (lbladdvatamt.Text != "")
                        {
                            li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                        }
                        else
                        {
                            li.addtaxamt = 0;
                        }
                        if (lblamount.Text != "")
                        {
                            li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                        }
                        else
                        {
                            li.amount = 0;
                        }
                        if (lblccode.Text != "")
                        {
                            li.ccode = Convert.ToInt64(lblccode.Text);
                        }
                        else
                        {
                            li.ccode = 0;
                        }
                        li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        fsiclass.updatesiitemsdata(li);

                        //li.vid = Convert.ToInt64(lblvid.Text);
                        //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                        //fsiclass.updatestrsinoinscitems1(li);
                        //}
                        //}


                    }
                    else
                    {
                        Label lblvid = (Label)gvsiitemlist.Rows[y].FindControl("lblvid");
                        Label lblitemname = (Label)gvsiitemlist.Rows[y].FindControl("lblitemname");
                        //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblrate = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridrate");
                        TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbltaxtype = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvsiitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvsiitemlist.Rows[y].FindControl("lblcstamt");
                        Label lblunit = (Label)gvsiitemlist.Rows[y].FindControl("lblunit");
                        TextBox txtgvunit = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvunit");
                        Label lblamount = (Label)gvsiitemlist.Rows[y].FindControl("lblamount");
                        Label lblccode = (Label)gvsiitemlist.Rows[y].FindControl("lblccode");
                        TextBox lbldesc1 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgridtaxdesc = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxdesc");
                        TextBox txtgvstockqty = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvstockqty");
                        li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                        li.id = Convert.ToInt64(lblid.Text);
                        li.itemname = lblitemname.Text;
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)
                        //{
                        li.unit = txtgvunit.Text;
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.taxtype = lbltaxtype.Text;
                        li.taxdesc = txtgridtaxdesc.Text;
                        if (lblvatp.Text != "")
                        {
                            li.vatp = Convert.ToDouble(lblvatp.Text);
                        }
                        else
                        {
                            li.vatp = 0;
                        }
                        if (lbladdvatp.Text != "")
                        {
                            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        }
                        else
                        {
                            li.addtaxp = 0;
                        }
                        if (lblcstp.Text != "")
                        {
                            li.cstp = Convert.ToDouble(lblcstp.Text);
                        }
                        else
                        {
                            li.cstp = 0;
                        }
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                        if (lblsctamt.Text != "")
                        {
                            li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                        }
                        else
                        {
                            li.cstamt = 0;
                        }
                        if (lblvatamt.Text != "")
                        {
                            li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                        }
                        else
                        {
                            li.vatamt = 0;
                        }
                        if (lbladdvatamt.Text != "")
                        {
                            li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                        }
                        else
                        {
                            li.addtaxamt = 0;
                        }
                        if (lblamount.Text != "")
                        {
                            li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                        }
                        else
                        {
                            li.amount = 0;
                        }
                        if (lblccode.Text != "")
                        {
                            li.ccode = Convert.ToInt64(lblccode.Text);
                        }
                        else
                        {
                            li.ccode = 0;
                        }
                        li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        fsiclass.updatesiitemsdata(li);

                        //li.vid = Convert.ToInt64(lblvid.Text);
                        //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                        //fsiclass.updatestrsinoinscitems1(li);
                    }
                }
                else
                {
                    Label lblvid = (Label)gvsiitemlist.Rows[y].FindControl("lblvid");
                    Label lblitemname = (Label)gvsiitemlist.Rows[y].FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)gvsiitemlist.Rows[y].FindControl("lblvatp");
                    Label lblvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lblvatamt");
                    Label lbladdvatp = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)gvsiitemlist.Rows[y].FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridcstp");
                    Label lblsctamt = (Label)gvsiitemlist.Rows[y].FindControl("lblcstamt");
                    Label lblunit = (Label)gvsiitemlist.Rows[y].FindControl("lblunit");
                    TextBox txtgvunit = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvunit");
                    Label lblamount = (Label)gvsiitemlist.Rows[y].FindControl("lblamount");
                    Label lblccode = (Label)gvsiitemlist.Rows[y].FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)gvsiitemlist.Rows[y].FindControl("txtdesc3");
                    TextBox txtgridtaxdesc = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgridtaxdesc");
                    TextBox txtgvstockqty = (TextBox)gvsiitemlist.Rows[y].FindControl("txtgvstockqty");
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    li.unit = txtgvunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    li.taxdesc = txtgridtaxdesc.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    if (lblccode.Text != "")
                    {
                        li.ccode = Convert.ToInt64(lblccode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                    li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    fsiclass.updatesiitemsdata(li);

                    //li.vid = Convert.ToInt64(lblvid.Text);
                    //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                    //fsiclass.updatestrsinoinscitems1(li);
                }
            }
            //

            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strsino + " Sales Invoice Updated.";
            faclass.insertactivity(li);
            Label lblccodee = (Label)gvsiitemlist.Rows[0].FindControl("lblccode");

            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drpsalesac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "D";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            //if (Convert.ToDouble(txtroundoff.Text) <= 0)
            //{
            if (ViewState["excludetax"].ToString() == "Yes")//if (drpsalesac.SelectedItem.Text == "Sales A/c. IGST - Export" || drpsalesac.SelectedItem.Text == "Sales A/c. - EOU")
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2);// + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtserviceamt.Text)
            }
            else
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            //}
            //else
            //{
            //    li.amount = Convert.ToDouble(txtbillamount.Text) + Convert.ToDouble(txtroundoff.Text);
            //}
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpsalesac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "C";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text == string.Empty && txtcartage.Text == "")
            {
                txtcartage.Text = "0";
            }
            if (ViewState["excludetax"].ToString() == "Yes")//if (drpsalesac.SelectedItem.Text == "Sales A/c. IGST - Export" || drpsalesac.SelectedItem.Text == "Sales A/c. - EOU")
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2);// + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text)+ Convert.ToDouble(txtserviceamt.Text)
            }
            else
            {
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text);
            }
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //}

            //3
            if (ViewState["excludetax"].ToString() != "Yes")//if (drpsalesac.SelectedItem.Text != "Sales A/c. IGST - Export" && drpsalesac.SelectedItem.Text != "Sales A/c. - EOU")
            {
                if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["cstdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "cst";
                    li.istype1 = "C";
                    li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                    li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "SI";
                    fsiclass.insertledgerdata(li);
                }
                //4
                if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["vatdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "totvat";
                    li.istype1 = "C";
                    li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                    li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "SI";
                    fsiclass.insertledgerdata(li);
                }
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
            }

            //li.uname = Request.Cookies["ForLogin"]["username"];
            //li.udate = System.DateTime.Now;
            //for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            //{
            //    li.sino = Convert.ToInt64(txtinvoiceno.Text);
            //    Label lblid = (Label)gvsiitemlist.Rows[c].FindControl("lblid");
            //    Label lblitemname = (Label)gvsiitemlist.Rows[c].FindControl("lblitemname");
            //    TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
            //    TextBox lblrate = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridrate");
            //    TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
            //    TextBox lbltaxtype = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridtaxtype");
            //    Label lblvatp = (Label)gvsiitemlist.Rows[c].FindControl("lblvatp");
            //    Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
            //    Label lbladdvatp = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatp");
            //    Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
            //    TextBox lblcstp = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridcstp");
            //    Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
            //    Label lblunit = (Label)gvsiitemlist.Rows[c].FindControl("lblunit");
            //    Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
            //    Label lblccode = (Label)gvsiitemlist.Rows[c].FindControl("lblccode");
            //    TextBox lbldesc1 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc1");
            //    TextBox lbldesc2 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc2");
            //    TextBox lbldesc3 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtdesc3");
            //    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
            //    li.id = Convert.ToInt64(lblid.Text);
            //    li.itemname = lblitemname.Text;
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    if (li.qty > 0)
            //    {
            //        li.unit = lblunit.Text;
            //        li.rate = Convert.ToDouble(lblrate.Text);
            //        li.taxtype = lbltaxtype.Text;
            //        if (lblvatp.Text != "")
            //        {
            //            li.vatp = Convert.ToDouble(lblvatp.Text);
            //        }
            //        else
            //        {
            //            li.vatp = 0;
            //        }
            //        if (lbladdvatp.Text != "")
            //        {
            //            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
            //        }
            //        else
            //        {
            //            li.addtaxp = 0;
            //        }
            //        if (lblcstp.Text != "")
            //        {
            //            li.cstp = Convert.ToDouble(lblcstp.Text);
            //        }
            //        else
            //        {
            //            li.cstp = 0;
            //        }
            //        li.descr1 = lbldesc1.Text;
            //        li.descr2 = lbldesc2.Text;
            //        li.descr3 = lbldesc3.Text;
            //        li.basicamount = Convert.ToDouble(lblbasicamount.Text);
            //        if (lblsctamt.Text != "")
            //        {
            //            li.cstamt = Convert.ToDouble(lblsctamt.Text);
            //        }
            //        else
            //        {
            //            li.cstamt = 0;
            //        }
            //        if (lblvatamt.Text != "")
            //        {
            //            li.vatamt = Convert.ToDouble(lblvatamt.Text);
            //        }
            //        else
            //        {
            //            li.vatamt = 0;
            //        }
            //        if (lbladdvatamt.Text != "")
            //        {
            //            li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
            //        }
            //        else
            //        {
            //            li.addtaxamt = 0;
            //        }
            //        if (lblamount.Text != "")
            //        {
            //            li.amount = Convert.ToDouble(lblamount.Text);
            //        }
            //        else
            //        {
            //            li.amount = 0;
            //        }
            //        if (lblccode.Text != "")
            //        {
            //            li.ccode = Convert.ToInt64(lblccode.Text);
            //        }
            //        else
            //        {
            //            li.ccode = 0;
            //        }
            //        fsiclass.updatesiitemsdata(li);
            //    }
            //}
        }
        li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        fsiclass.updatemonyrinsiitemsstring(li);
        li.stringscno = txtsrtringscno.Text;
        li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
        string[] words = li.stringscno.Split(',');
        foreach (string word in words)
        {
            li.strscno11 = word.Replace("'", "");
            fsiclass.updatestrsinoinscmaster(li);
            fsiclass.updatestrsinoinscmastera(li);
            li.strscno = word.Replace("'", "");
            fsiclass.updatescstatusforallchno(li);
            //Console.WriteLine(word);
        }
        if (Request["mode"].ToString() != "ledger")
        {
            if (drpeway.SelectedItem.Text == "YES")
            {
                if (btnsave.Text != "Update")
                {
                    txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                    txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                    txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                    txtpincode.Text = "380009";
                    txtplace.Text = "NAVRANGPURA";
                    drpfromstate.SelectedValue = "24";
                    txtgstinuin.Text = "24AADCA6797L1Z6";

                    txtconsignee.Text = drpacname.SelectedItem.Text;
                    li.acname = txtconsignee.Text;
                    DataTable dts = fsiclass.selectallacdatefromacname(li);
                    if (dts.Rows.Count > 0)
                    {
                        txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                        txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                        txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                        //txttoplace.Text="";
                        txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                        if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                        {
                            drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                        }
                        //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                    }
                }
                else
                {
                    DataTable dtdata = fsiclass.selectewaybilldatafromstrsino(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        if (dtdata.Rows[0]["distanceinkm"].ToString() != "")
                        {
                            txtewaybillno.Text = dtdata.Rows[0]["ewaybillno"].ToString();
                            txtdate1.Text = dtdata.Rows[0]["date1"].ToString();
                            txtconsolidatedewaybillno.Text = dtdata.Rows[0]["cewaybillno"].ToString();
                            txtdate2.Text = dtdata.Rows[0]["date2"].ToString();
                            drpsupplytype.SelectedValue = dtdata.Rows[0]["supplytype"].ToString();
                            drpsubtype.SelectedValue = dtdata.Rows[0]["subtype"].ToString();
                            drpdocumenttype.SelectedValue = dtdata.Rows[0]["doctype"].ToString();
                            drpmode.SelectedValue = dtdata.Rows[0]["tramode"].ToString();
                            txtdistance.Text = dtdata.Rows[0]["distanceinkm"].ToString();
                            txttransportername.Text = dtdata.Rows[0]["transportername"].ToString();
                            txtvehiclenumber.Text = dtdata.Rows[0]["vehicleno"].ToString();
                            drpvehicletype.SelectedValue = dtdata.Rows[0]["vehicletype"].ToString();
                            txtdoclandingrrairwayno.Text = dtdata.Rows[0]["trano"].ToString();
                            txtdate.Text = dtdata.Rows[0]["date3"].ToString();
                            txttransporterid.Text = dtdata.Rows[0]["traid"].ToString();
                            txtconsignor.Text = dtdata.Rows[0]["consignor"].ToString();
                            txtaddress1.Text = dtdata.Rows[0]["consignoradd1"].ToString();
                            txtaddress2.Text = dtdata.Rows[0]["consignoradd2"].ToString();
                            txtpincode.Text = dtdata.Rows[0]["consignorpincode"].ToString();
                            txtplace.Text = dtdata.Rows[0]["consignorplace"].ToString();
                            drpfromstate.SelectedValue = dtdata.Rows[0]["consignorstate"].ToString();
                            drpactualfromstate.SelectedValue = dtdata.Rows[0]["actualfromstatecode"].ToString();
                            txtgstinuin.Text = dtdata.Rows[0]["consignorgst"].ToString();

                            txtconsignee.Text = dtdata.Rows[0]["consignee"].ToString();
                            txtconsigneeaddress1.Text = dtdata.Rows[0]["consigneeadd1"].ToString();
                            txtconsigneeaddress2.Text = dtdata.Rows[0]["consigneeadd2"].ToString();
                            txtconsigneepincode.Text = dtdata.Rows[0]["consigneepincode"].ToString();
                            txttoplace.Text = dtdata.Rows[0]["consigneeplace"].ToString();
                            txtconsigneegstinuin.Text = dtdata.Rows[0]["consigneegst"].ToString();
                            drptostate.SelectedValue = dtdata.Rows[0]["consigneestate"].ToString();
                            drpactualtostate.SelectedValue = dtdata.Rows[0]["actualtostatecode"].ToString();
                        }
                        else
                        {
                            txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                            txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                            txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                            txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                            txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                            txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                            txtpincode.Text = "380009";
                            txtplace.Text = "NAVRANGPURA";
                            drpfromstate.SelectedValue = "24";
                            txtgstinuin.Text = "24AADCA6797L1Z6";

                            txtconsignee.Text = drpacname.SelectedItem.Text;
                            li.acname = txtconsignee.Text;
                            DataTable dts = fsiclass.selectallacdatefromacname(li);
                            if (dts.Rows.Count > 0)
                            {
                                txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                                txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                                txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                                //txttoplace.Text="";
                                txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                                if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                                {
                                    drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                                }
                                //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                            }
                        }
                    }
                    else
                    {
                        txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                        txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                        txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                        txtpincode.Text = "380009";
                        txtplace.Text = "NAVRANGPURA";
                        drpfromstate.SelectedValue = "24";
                        txtgstinuin.Text = "24AADCA6797L1Z6";

                        txtconsignee.Text = drpacname.SelectedItem.Text;
                        li.acname = txtconsignee.Text;
                        DataTable dts = fsiclass.selectallacdatefromacname(li);
                        if (dts.Rows.Count > 0)
                        {
                            txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                            txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                            txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                            //txttoplace.Text="";
                            txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                            if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                            {
                                drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                            }
                            //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                        }
                    }
                }
                ModalPopupExtender1.Show();
            }
            else
            {
                //ModalPopupExtender1.Hide();
                Response.Redirect("~/SalesInvoiceList.aspx?pagename=SalesInvoiceList");
            }
        }
        else
        {
            object refUrl = ViewState["RefUrl"];
            string ch = (string)refUrl;
            ch = ch.Split('?')[0] + "?acname=" + SessionMgt.acname + "&ccode=" + SessionMgt.ccode + "&fromdate=" + SessionMgt.fromdate + "&todate=" + SessionMgt.todate + "";
            SessionMgt.acname = "";
            SessionMgt.ccode = 0;
            SessionMgt.fromdate = "";
            SessionMgt.todate = "";
            if (refUrl != null)
                Response.Redirect(ch);
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemssain"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["vno"] = 0;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtbillqty.Text;
            dr["stockqty"] = txtstockqty.Text;
            dr["qtyremain"] = txtbillqty.Text;
            dr["qtyused"] = 0;
            dr["rate"] = txtrate.Text;
            dr["basicamount"] = txtbasicamt.Text;
            dr["taxtype"] = txttaxtype.Text;
            dr["taxdesc"] = drptaxdesc.SelectedItem.Text;
            dr["vatp"] = txtgvvat.Text;
            dr["vatamt"] = (Math.Round(Convert.ToDouble(txtvat.Text), 2)).ToString();
            dr["addtaxp"] = txtgvadtax.Text;
            dr["addtaxamt"] = (Math.Round(Convert.ToDouble(txtaddtax.Text), 2)).ToString();
            dr["cstp"] = txtgvcst.Text;
            dr["cstamt"] = (Math.Round(Convert.ToDouble(txtcstamount.Text), 2)).ToString();
            dr["unit"] = txtunit.Text;
            dr["amount"] = txtamount.Text;
            dr["ccode"] = txtcode.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemssain"] = dt;
            this.bindgrid();
            count1();
        }
        else
        {
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.vnono = Convert.ToInt64(txtinvoiceno.Text);
            li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.stockqty = Convert.ToDouble(txtstockqty.Text);
            li.qty = Convert.ToDouble(txtbillqty.Text);
            li.qtyremain = Convert.ToDouble(txtbillqty.Text);
            li.qtyused = 0;
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.taxtype = txttaxtype.Text;
            li.taxdesc = drptaxdesc.SelectedItem.Text;
            li.vatp = Convert.ToDouble(txtgvvat.Text);
            li.vatamt = Math.Round(Convert.ToDouble(txtvat.Text), 2);
            li.addtaxp = Convert.ToDouble(txtgvadtax.Text);
            li.addtaxamt = Math.Round(Convert.ToDouble(txtaddtax.Text), 2);
            li.cstp = Convert.ToDouble(txtgvcst.Text);
            li.cstamt = Math.Round(Convert.ToDouble(txtcstamount.Text), 2);
            li.unit = txtunit.Text;
            li.amount = Convert.ToDouble(txtamount.Text);
            li.ccode = Convert.ToInt64(txtcode.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.cmon = Convert.ToInt16(txtsidate.Text.Split('-')[1]);
            if (li.cmon == 1)
            {
                li.cmonname = "JANUARY";
            }
            else if (li.cmon == 2)
            {
                li.cmonname = "FEBRUARY";
            }
            else if (li.cmon == 3)
            {
                li.cmonname = "MARCH";
            }
            else if (li.cmon == 4)
            {
                li.cmonname = "APRIL";
            }
            else if (li.cmon == 5)
            {
                li.cmonname = "MAY";
            }
            else if (li.cmon == 6)
            {
                li.cmonname = "JUNE";
            }
            else if (li.cmon == 7)
            {
                li.cmonname = "JULY";
            }
            else if (li.cmon == 8)
            {
                li.cmonname = "AUGUST";
            }
            else if (li.cmon == 9)
            {
                li.cmonname = "SEPTEMBER";
            }
            else if (li.cmon == 10)
            {
                li.cmonname = "OCTOBER";
            }
            else if (li.cmon == 11)
            {
                li.cmonname = "NOVEMBER";
            }
            else
            {
                li.cmonname = "DECEMBER";
            }
            li.monyr = li.cmonname + "-" + txtsidate.Text.Split('-')[2];
            li.taxdesc = drptaxdesc.SelectedItem.Text;
            fsiclass.insertsiitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strsino + " Sales Invoice Item Inserted.";
            faclass.insertactivity(li);
            Label lblccodee = (Label)gvsiitemlist.Rows[0].FindControl("lblccode");
            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drpsalesac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "D";
            li.amount = Convert.ToDouble(txtbillamount.Text);
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpsalesac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "C";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text != string.Empty && txtcartage.Text != "")
            {
                txtcartage.Text = "0";
            }
            li.amount = Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txtcartage.Text);
            li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "SI";
            fsiclass.insertledgerdata(li);
            //}

            //3
            if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["cstdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "cst";
                li.istype1 = "C";
                li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
            }
            //4
            if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["vatdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totvat";
                li.istype1 = "C";
                li.amount = Math.Round(Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text), 2);
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "SI";
                fsiclass.insertledgerdata(li);
            }

            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            DataTable dtdata = new DataTable();
            dtdata = fsiclass.selectallsiitemsfromsinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsiitemlist.Visible = true;
                gvsiitemlist.DataSource = dtdata;
                gvsiitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
            count1();
        }
        fillactaxdesc();
        fillitemnamedrop();
        countgv();
        hideimage();
        txtbillqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txttaxtype.Text = string.Empty;
        txtgvvat.Text = string.Empty;
        txtgvadtax.Text = string.Empty;
        txtgvcst.Text = string.Empty;
        txtdescription1.Text = string.Empty;
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        txtvat.Text = string.Empty;
        txtaddtax.Text = string.Empty;
        txtcstamount.Text = string.Empty;
        txtamount.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtcode.Text = string.Empty;
        Page.SetFocus(drpitemname);
    }
    protected void txtbillqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtstockqty);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvadtax);
    }
    protected void txtgvadtax_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvcst);
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        var cc = txtgvcst.Text.IndexOf("-");
        if (cc != -1)
        {
            txtgvcst.Text = "0";
        }
        count1();
        Page.SetFocus(txtunit);
    }
    protected void txtvat_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtaddtax_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txttotbasicamount_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);

        //double round1 = Math.Round((bamt - Math.Round(bamt)), 2);
        //if (Math.Round(bamt) > bamt)
        //{
        //    if (round1 > 0)
        //    {
        //        if (round1 != 0.5)
        //        {
        //            txtroundoff.Text = Math.Round(round1, 2).ToString();
        //        }
        //        else
        //        {
        //            txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
        //        }
        //    }
        //    else
        //    {
        //        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
        //    }
        //}
        //else
        //{
        //    if (round1 > 0)
        //    {
        //        if (round1 != 0.5)
        //        {
        //            txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
        //        }
        //        else
        //        {
        //            txtroundoff.Text = Math.Round(round1, 2).ToString();
        //        }
        //    }
        //    else
        //    {
        //        txtroundoff.Text = Math.Round(round1, 2).ToString();
        //    }
        //}
        //roundoff = Convert.ToDouble(txtroundoff.Text);
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        //if (round1 > 0.5)
        //{
        //    //txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
        //    txtroundoff.Text = ((-1) * (Math.Round((1 - round1), 2))).ToString();
        //}
        //else
        //{
        //    txtroundoff.Text = Math.Round(round1 * -1, 2).ToString();
        //}
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txttotcst);
    }
    protected void txttotcst_TextChanged(object sender, EventArgs e)
    {
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txttotvat);
    }
    protected void txttotvat_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtservicep);
    }
    protected void txtserviceamt_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtroundoff);
    }
    protected void txtroundoff_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txttotaddtax);
    }
    protected void txttotaddtax_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtcartage);
    }
    protected void txtcartage_TextChanged(object sender, EventArgs e)
    {
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtbillamount);
    }
    protected void txtorderno_TextChanged(object sender, EventArgs e)
    {
        if (txtorderno.Text.Trim() != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtorderno.Text);
            DataTable dtdata = new DataTable();
            dtdata = fsiclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                DataTable dt = (DataTable)Session["dtpitemssain"];
                dt.Clear();
                for (int d = 0; d < dtdata.Rows.Count; d++)
                {
                    DataRow dr = dt.NewRow();
                    dr["id"] = 1;
                    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                    dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                    dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                    dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
                    dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                    dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                    dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                    dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                    dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
                    dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();
                    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                    dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                    dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                    dt.Rows.Add(dr);
                    //count1();
                }
                Session["dtpitemssain"] = dt;
                this.bindgrid();
                countgv();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
        }
        else
        {
            gvsiitemlist.Visible = false;
            lblempty.Visible = false;
            txttotbasicamount.Text = "0";
            txttotcst.Text = "0";
            txttotvat.Text = "0";
            txtservicep.Text = "0";
            txtserviceamt.Text = "0";
            txttotaddtax.Text = "0";
            txtroundoff.Text = "0";
            txtcartage.Text = "0";
            txtbillamount.Text = "0";
        }
    }

    protected void txtgvqty_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvstockqty = (TextBox)currentRow1.FindControl("txtgvstockqty");
        if (btnsave.Text == "Save")
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblvid = (Label)currentRow.FindControl("lblvid");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            if (txtgvqty1.Text != string.Empty)
            {
                if (lblvid.Text != "0")
                {
                    if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
                    {
                        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



                        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                        Label lblamount = (Label)currentRow.FindControl("lblamount");
                        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                        double vatp = 0;
                        double addvatp = 0;
                        double cstp = 0;
                        double vatamt = 0;
                        double addvatamt = 0;
                        double cstamt = 0;
                        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                        txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
                        if (lbltaxtype.Text != string.Empty)
                        {
                            var cc = lbltaxtype.Text.IndexOf("-");
                            if (cc != -1)
                            {
                                li.typename = lbltaxtype.Text;
                                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                                lblvatp.Text = vatp.ToString();
                                lbladdvatp.Text = addvatp.ToString();
                                DataTable dtdtax = new DataTable();
                                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                                if (dtdtax.Rows.Count > 0)
                                {
                                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                                }
                                else
                                {
                                    txtgridcstp.Text = "0";
                                }
                            }
                        }
                        if (lblvatp.Text != string.Empty)
                        {
                            vatp = Convert.ToDouble(lblvatp.Text);
                            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                        }
                        else
                        {
                            lblvatamt.Text = "0";
                            lblvatp.Text = "0";
                        }
                        if (lbladdvatp.Text != string.Empty)
                        {
                            addvatp = Convert.ToDouble(lbladdvatp.Text);
                            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                        }
                        else
                        {
                            lbladdvatp.Text = "0";
                            lbladdvatamt.Text = "0";
                        }
                        if (lblcstp.Text != string.Empty)
                        {
                            cstp = Convert.ToDouble(lblcstp.Text);
                            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                        }
                        else
                        {
                            lblcstp.Text = "0";
                            lblcstamt.Text = "0";
                        }
                        vatamt = Convert.ToDouble(lblvatamt.Text);
                        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                        cstamt = Convert.ToDouble(lblcstamt.Text);
                        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                        if (gvsiitemlist.Rows.Count > 0)
                        {
                            double totbasicamt = 0;
                            double totcst = 0;
                            double totvat = 0;
                            double totaddvat = 0;
                            double totamount = 0;
                            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                            {
                                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                                Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                                Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                                Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                                if (lblsctamt.Text != string.Empty)
                                {
                                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                                }
                                if (lblvatamt1.Text != string.Empty)
                                {
                                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                                }
                                if (lbladdvatamt1.Text != string.Empty)
                                {
                                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                                }
                                if (lblamount1.Text != string.Empty)
                                {
                                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                                }
                            }
                            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                            txttotvat.Text = Math.Round(totvat, 2).ToString();
                            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                            txttotcst.Text = Math.Round(totcst, 2).ToString();
                            double cartage = 0;
                            if (txtcartage.Text.Trim() != string.Empty)
                            {
                                cartage = Convert.ToDouble(txtcartage.Text);
                            }
                            else
                            {
                                txtcartage.Text = "0";
                            }
                            double servp = 0;
                            double servamt = 0;
                            double roundoff = 0;
                            if (txtservicep.Text.Trim() != string.Empty)
                            {
                                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
                            }
                            else
                            {
                                txtservicep.Text = "0";
                                txtserviceamt.Text = "0";
                            }
                            if (txtroundoff.Text.Trim() != string.Empty)
                            {
                                roundoff = Convert.ToDouble(txtroundoff.Text);
                            }
                            else
                            {
                                txtroundoff.Text = "0";
                            }
                            double serant = 0;
                            if (txtserviceamt.Text.Trim() != string.Empty)
                            {
                                serant = Convert.ToDouble(txtserviceamt.Text);
                            }
                            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                            //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
                            //double round1 = bamt - Math.Truncate(bamt);
                            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
                            //if (round1 >= 0.5)
                            //{
                            //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                            //}
                            //else
                            //{
                            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
                            //}
                            double bamt = Convert.ToDouble(txtbillamount.Text);
                            double round1 = bamt - Math.Round(bamt);
                            //txtroundoff.Text = Math.Round(round1, 2).ToString();
                            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                            {
                                if (round1 > 0)
                                {
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                                else
                                {
                                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                                }
                            }
                            else
                            {
                                if (round1 > 0)
                                {
                                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                                else
                                {
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                            }
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                            txtbillamount.Text = (Math.Round(bamt)).ToString();
                        }
                        else
                        {
                            txttotbasicamount.Text = "0";
                            txttotvat.Text = "0";
                            txttotaddtax.Text = "0";
                            txttotcst.Text = "0";
                            txtbillamount.Text = "0";
                        }
                    }
                    else
                    {
                        txtgvqty1.Text = lblqtyremain.Text;
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                        return;
                    }
                }
                else
                {
                    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                    double vatp = 0;
                    double addvatp = 0;
                    double cstp = 0;
                    double vatamt = 0;
                    double addvatamt = 0;
                    double cstamt = 0;
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                    txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
                    if (lbltaxtype.Text != string.Empty)
                    {
                        var cc = lbltaxtype.Text.IndexOf("-");
                        if (cc != -1)
                        {
                            li.typename = lbltaxtype.Text;
                            vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                            addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                            lblvatp.Text = vatp.ToString();
                            lbladdvatp.Text = addvatp.ToString();
                            DataTable dtdtax = new DataTable();
                            dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                            if (dtdtax.Rows.Count > 0)
                            {
                                txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                            }
                            else
                            {
                                txtgridcstp.Text = "0";
                            }
                        }
                    }
                    if (lblvatp.Text != string.Empty)
                    {
                        vatp = Convert.ToDouble(lblvatp.Text);
                        lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblvatamt.Text = "0";
                        lblvatp.Text = "0";
                    }
                    if (lbladdvatp.Text != string.Empty)
                    {
                        addvatp = Convert.ToDouble(lbladdvatp.Text);
                        lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lbladdvatp.Text = "0";
                        lbladdvatamt.Text = "0";
                    }
                    if (lblcstp.Text != string.Empty)
                    {
                        cstp = Convert.ToDouble(lblcstp.Text);
                        lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblcstp.Text = "0";
                        lblcstamt.Text = "0";
                    }
                    vatamt = Convert.ToDouble(lblvatamt.Text);
                    addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                    cstamt = Convert.ToDouble(lblcstamt.Text);
                    lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                    if (gvsiitemlist.Rows.Count > 0)
                    {
                        double totbasicamt = 0;
                        double totcst = 0;
                        double totvat = 0;
                        double totaddvat = 0;
                        double totamount = 0;
                        for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                        {
                            TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                            Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                            Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                            Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                            Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                            if (lblsctamt.Text != string.Empty)
                            {
                                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                            }
                            if (lblvatamt1.Text != string.Empty)
                            {
                                totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                            }
                            if (lbladdvatamt1.Text != string.Empty)
                            {
                                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                            }
                            if (lblamount1.Text != string.Empty)
                            {
                                totamount = totamount + Convert.ToDouble(lblamount1.Text);
                            }
                        }
                        txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                        txttotvat.Text = Math.Round(totvat, 2).ToString();
                        txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                        txttotcst.Text = Math.Round(totcst, 2).ToString();
                        double cartage = 0;
                        if (txtcartage.Text.Trim() != string.Empty)
                        {
                            cartage = Convert.ToDouble(txtcartage.Text);
                        }
                        else
                        {
                            txtcartage.Text = "0";
                        }
                        double servp = 0;
                        double servamt = 0;
                        double roundoff = 0;
                        if (txtservicep.Text.Trim() != string.Empty)
                        {
                            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
                        }
                        else
                        {
                            txtservicep.Text = "0";
                            txtserviceamt.Text = "0";
                        }
                        if (txtroundoff.Text.Trim() != string.Empty)
                        {
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                        }
                        else
                        {
                            txtroundoff.Text = "0";
                        }
                        double serant = 0;
                        if (txtserviceamt.Text.Trim() != string.Empty)
                        {
                            serant = Convert.ToDouble(txtserviceamt.Text);
                        }
                        //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
                        //double round1 = bamt - Math.Truncate(bamt);
                        ////txtroundoff.Text = Math.Round(round1, 2).ToString();
                        //if (round1 >= 0.5)
                        //{
                        //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                        //}
                        //else
                        //{
                        //    txtroundoff.Text = Math.Round(round1, 2).ToString();
                        //}
                        double bamt = Convert.ToDouble(txtbillamount.Text);
                        double round1 = bamt - Math.Round(bamt);
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                        {
                            if (round1 > 0)
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = Math.Round(round1, 2).ToString();
                            }
                        }
                        else
                        {
                            if (round1 > 0)
                            {
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                        }
                        roundoff = Convert.ToDouble(txtroundoff.Text);
                        txtbillamount.Text = (Math.Round(bamt)).ToString();
                    }
                    else
                    {
                        txttotbasicamount.Text = "0";
                        txttotvat.Text = "0";
                        txttotaddtax.Text = "0";
                        txttotcst.Text = "0";
                        txtbillamount.Text = "0";
                    }
                }
            }
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            li.vid = Convert.ToInt64(lblid.Text);
            if (txtgvqty1.Text != string.Empty)
            {
                if (li.vid != 0)
                {
                    DataTable dtremain = new DataTable();
                    dtremain = fsiclass.selectqtyremainusedfromscno(li);
                    if (dtremain.Rows.Count > 0)
                    {
                        li.qty = Convert.ToDouble(lblqty.Text);
                        double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                        //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                        if (Convert.ToDouble(txtgvqty1.Text) <= rqty)
                        {
                            //if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
                            //{
                            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



                            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                            TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                            Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                            Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                            Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                            Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                            Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                            Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                            Label lblamount = (Label)currentRow.FindControl("lblamount");
                            //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                            double vatp = 0;
                            double addvatp = 0;
                            double cstp = 0;
                            double vatamt = 0;
                            double addvatamt = 0;
                            double cstamt = 0;
                            //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                            double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                            txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
                            if (lbltaxtype.Text != string.Empty)
                            {
                                var cc = lbltaxtype.Text.IndexOf("-");
                                if (cc != -1)
                                {
                                    li.typename = lbltaxtype.Text;
                                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                                    lblvatp.Text = vatp.ToString();
                                    lbladdvatp.Text = addvatp.ToString();
                                    DataTable dtdtax = new DataTable();
                                    dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                                    if (dtdtax.Rows.Count > 0)
                                    {
                                        txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                                    }
                                    else
                                    {
                                        txtgridcstp.Text = "0";
                                    }
                                }
                            }
                            if (lblvatp.Text != string.Empty)
                            {
                                vatp = Convert.ToDouble(lblvatp.Text);
                                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                            }
                            else
                            {
                                lblvatamt.Text = "0";
                                lblvatp.Text = "0";
                            }
                            if (lbladdvatp.Text != string.Empty)
                            {
                                addvatp = Convert.ToDouble(lbladdvatp.Text);
                                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                            }
                            else
                            {
                                lbladdvatp.Text = "0";
                                lbladdvatamt.Text = "0";
                            }
                            if (lblcstp.Text != string.Empty)
                            {
                                cstp = Convert.ToDouble(lblcstp.Text);
                                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                            }
                            else
                            {
                                lblcstp.Text = "0";
                                lblcstamt.Text = "0";
                            }
                            vatamt = Convert.ToDouble(lblvatamt.Text);
                            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                            cstamt = Convert.ToDouble(lblcstamt.Text);
                            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                            if (gvsiitemlist.Rows.Count > 0)
                            {
                                double totbasicamt = 0;
                                double totcst = 0;
                                double totvat = 0;
                                double totaddvat = 0;
                                double totamount = 0;
                                for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                                {
                                    TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                                    Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                                    Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                                    Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                                    Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                                    totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                                    if (lblsctamt.Text != string.Empty)
                                    {
                                        totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                                    }
                                    if (lblvatamt1.Text != string.Empty)
                                    {
                                        totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                                    }
                                    if (lbladdvatamt1.Text != string.Empty)
                                    {
                                        totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                                    }
                                    if (lblamount1.Text != string.Empty)
                                    {
                                        totamount = totamount + Convert.ToDouble(lblamount1.Text);
                                    }
                                }
                                txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                                txttotvat.Text = Math.Round(totvat, 2).ToString();
                                txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                                txttotcst.Text = Math.Round(totcst, 2).ToString();
                                double cartage = 0;
                                if (txtcartage.Text.Trim() != string.Empty)
                                {
                                    cartage = Convert.ToDouble(txtcartage.Text);
                                }
                                else
                                {
                                    txtcartage.Text = "0";
                                }
                                double servp = 0;
                                double servamt = 0;
                                double roundoff = 0;
                                if (txtservicep.Text.Trim() != string.Empty)
                                {
                                    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
                                }
                                else
                                {
                                    txtservicep.Text = "0";
                                    txtserviceamt.Text = "0";
                                }
                                if (txtroundoff.Text.Trim() != string.Empty)
                                {
                                    roundoff = Convert.ToDouble(txtroundoff.Text);
                                }
                                else
                                {
                                    txtroundoff.Text = "0";
                                }
                                double serant = 0;
                                if (txtserviceamt.Text.Trim() != string.Empty)
                                {
                                    serant = Convert.ToDouble(txtserviceamt.Text);
                                }
                                //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                                txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                                //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
                                //double round1 = bamt - Math.Truncate(bamt);
                                ////txtroundoff.Text = Math.Round(round1, 2).ToString();
                                //if (round1 >= 0.5)
                                //{
                                //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                                //}
                                //else
                                //{
                                //    txtroundoff.Text = Math.Round(round1, 2).ToString();
                                //}
                                double bamt = Convert.ToDouble(txtbillamount.Text);
                                double round1 = bamt - Math.Round(bamt);
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                                {
                                    if (round1 > 0)
                                    {
                                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                    }
                                    else
                                    {
                                        txtroundoff.Text = Math.Round(round1, 2).ToString();
                                    }
                                }
                                else
                                {
                                    if (round1 > 0)
                                    {
                                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                    }
                                    else
                                    {
                                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                    }
                                }
                                roundoff = Convert.ToDouble(txtroundoff.Text);
                                txtbillamount.Text = (Math.Round(bamt)).ToString();
                            }
                            else
                            {
                                txttotbasicamount.Text = "0";
                                txttotvat.Text = "0";
                                txttotaddtax.Text = "0";
                                txttotcst.Text = "0";
                                txtbillamount.Text = "0";
                            }
                        }
                        else
                        {
                            txtgvqty1.Text = lblqty.Text;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                            return;
                        }
                    }
                }
                else
                {
                    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                    double vatp = 0;
                    double addvatp = 0;
                    double cstp = 0;
                    double vatamt = 0;
                    double addvatamt = 0;
                    double cstamt = 0;
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                    txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
                    if (lbltaxtype.Text != string.Empty)
                    {
                        var cc = lbltaxtype.Text.IndexOf("-");
                        if (cc != -1)
                        {
                            li.typename = lbltaxtype.Text;
                            vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                            addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                            lblvatp.Text = vatp.ToString();
                            lbladdvatp.Text = addvatp.ToString();
                            DataTable dtdtax = new DataTable();
                            dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                            if (dtdtax.Rows.Count > 0)
                            {
                                txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                            }
                            else
                            {
                                txtgridcstp.Text = "0";
                            }
                        }
                    }
                    if (lblvatp.Text != string.Empty)
                    {
                        vatp = Convert.ToDouble(lblvatp.Text);
                        lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblvatamt.Text = "0";
                        lblvatp.Text = "0";
                    }
                    if (lbladdvatp.Text != string.Empty)
                    {
                        addvatp = Convert.ToDouble(lbladdvatp.Text);
                        lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lbladdvatp.Text = "0";
                        lbladdvatamt.Text = "0";
                    }
                    if (lblcstp.Text != string.Empty)
                    {
                        cstp = Convert.ToDouble(lblcstp.Text);
                        lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblcstp.Text = "0";
                        lblcstamt.Text = "0";
                    }
                    vatamt = Convert.ToDouble(lblvatamt.Text);
                    addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                    cstamt = Convert.ToDouble(lblcstamt.Text);
                    lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                    if (gvsiitemlist.Rows.Count > 0)
                    {
                        double totbasicamt = 0;
                        double totcst = 0;
                        double totvat = 0;
                        double totaddvat = 0;
                        double totamount = 0;
                        for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                        {
                            TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                            Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                            Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                            Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                            Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                            if (lblsctamt.Text != string.Empty)
                            {
                                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                            }
                            if (lblvatamt1.Text != string.Empty)
                            {
                                totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                            }
                            if (lbladdvatamt1.Text != string.Empty)
                            {
                                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                            }
                            if (lblamount1.Text != string.Empty)
                            {
                                totamount = totamount + Convert.ToDouble(lblamount1.Text);
                            }
                        }
                        txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                        txttotvat.Text = Math.Round(totvat, 2).ToString();
                        txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                        txttotcst.Text = Math.Round(totcst, 2).ToString();
                        double cartage = 0;
                        if (txtcartage.Text.Trim() != string.Empty)
                        {
                            cartage = Convert.ToDouble(txtcartage.Text);
                        }
                        else
                        {
                            txtcartage.Text = "0";
                        }
                        double servp = 0;
                        double servamt = 0;
                        double roundoff = 0;
                        if (txtservicep.Text.Trim() != string.Empty)
                        {
                            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
                        }
                        else
                        {
                            txtservicep.Text = "0";
                            txtserviceamt.Text = "0";
                        }
                        if (txtroundoff.Text.Trim() != string.Empty)
                        {
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                        }
                        else
                        {
                            txtroundoff.Text = "0";
                        }
                        double serant = 0;
                        if (txtserviceamt.Text.Trim() != string.Empty)
                        {
                            serant = Convert.ToDouble(txtserviceamt.Text);
                        }
                        //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
                        //double round1 = bamt - Math.Truncate(bamt);
                        ////txtroundoff.Text = Math.Round(round1, 2).ToString();
                        //if (round1 >= 0.5)
                        //{
                        //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                        //}
                        //else
                        //{
                        //    txtroundoff.Text = Math.Round(round1, 2).ToString();
                        //}
                        double bamt = Convert.ToDouble(txtbillamount.Text);
                        double round1 = bamt - Math.Round(bamt);
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                        {
                            if (round1 > 0)
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = Math.Round(round1, 2).ToString();
                            }
                        }
                        else
                        {
                            if (round1 > 0)
                            {
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                        }
                        roundoff = Convert.ToDouble(txtroundoff.Text);
                        txtbillamount.Text = (Math.Round(bamt)).ToString();
                    }
                    else
                    {
                        txttotbasicamount.Text = "0";
                        txttotvat.Text = "0";
                        txttotaddtax.Text = "0";
                        txttotcst.Text = "0";
                        txtbillamount.Text = "0";
                    }
                }
            }
        }
        Page.SetFocus(txtgvstockqty);
    }

    protected void txtgvrate_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        if (txtgvqty1.Text != string.Empty && txtgvrate1.Text != string.Empty)
        {
            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
            TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
            Label lblvatp = (Label)currentRow.FindControl("lblvatp");
            Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
            Label lblcstp = (Label)currentRow.FindControl("lblcstp");
            Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
            Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
            Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
            Label lblamount = (Label)currentRow.FindControl("lblamount");
            //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
            double vatp = 0;
            double addvatp = 0;
            double cstp = 0;
            double vatamt = 0;
            double addvatamt = 0;
            double cstamt = 0;
            //double baseamt = Convert.ToDouble(txtgvamount1.Text);
            double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
            txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
            if (lbltaxtype.Text != string.Empty)
            {
                var cc = lbltaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = lbltaxtype.Text;
                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    DataTable dtdtax = new DataTable();
                    dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                    if (dtdtax.Rows.Count > 0)
                    {
                        txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                    }
                    else
                    {
                        txtgridcstp.Text = "0";
                    }
                }
            }
            if (lblvatp.Text != string.Empty)
            {
                vatp = Convert.ToDouble(lblvatp.Text);
                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            }
            else
            {
                lblvatamt.Text = "0";
                lblvatp.Text = "0";
            }
            if (lbladdvatp.Text != string.Empty)
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
            }
            else
            {
                lbladdvatp.Text = "0";
                lbladdvatamt.Text = "0";
            }
            if (lblcstp.Text != string.Empty)
            {
                cstp = Convert.ToDouble(lblcstp.Text);
                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                lblcstp.Text = "0";
                lblcstamt.Text = "0";
            }
            vatamt = Convert.ToDouble(lblvatamt.Text);
            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
            cstamt = Convert.ToDouble(lblcstamt.Text);
            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


            if (gvsiitemlist.Rows.Count > 0)
            {
                double totbasicamt = 0;
                double totcst = 0;
                double totvat = 0;
                double totaddvat = 0;
                double totamount = 0;
                for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                {
                    TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                    Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                    Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                    Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                    Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                    totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                    if (lblsctamt.Text != string.Empty)
                    {
                        totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                    }
                    if (lblvatamt1.Text != string.Empty)
                    {
                        totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                    }
                    if (lbladdvatamt1.Text != string.Empty)
                    {
                        totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                    }
                    if (lblamount1.Text != string.Empty)
                    {
                        totamount = totamount + Convert.ToDouble(lblamount1.Text);
                    }
                }
                txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                txttotvat.Text = Math.Round(totvat, 2).ToString();
                txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                txttotcst.Text = Math.Round(totcst, 2).ToString();
                double cartage = 0;
                if (txtcartage.Text.Trim() != string.Empty)
                {
                    cartage = Convert.ToDouble(txtcartage.Text);
                }
                else
                {
                    txtcartage.Text = "0";
                }
                double servp = 0;
                double servamt = 0;
                double roundoff = 0;
                if (txtservicep.Text.Trim() != string.Empty)
                {
                    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                }
                else
                {
                    txtservicep.Text = "0";
                    txtserviceamt.Text = "0";
                }
                if (txtroundoff.Text.Trim() != string.Empty)
                {
                    roundoff = Convert.ToDouble(txtroundoff.Text);
                }
                else
                {
                    txtroundoff.Text = "0";
                }
                double serant = 0;
                if (txtserviceamt.Text.Trim() != string.Empty)
                {
                    serant = Convert.ToDouble(txtserviceamt.Text);
                }
                //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
                //double round1 = (1 - (bamt - Math.Truncate(bamt)));
                ////txtroundoff.Text = Math.Round(round1, 2).ToString();
                //if (round1 >= 0.5)
                //{
                //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                //}
                //else
                //{
                //    txtroundoff.Text = Math.Round(round1, 2).ToString();
                //}
                double bamt = Convert.ToDouble(txtbillamount.Text);
                double round1 = bamt - Math.Round(bamt);
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                {
                    if (round1 > 0)
                    {
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                    else
                    {
                        txtroundoff.Text = Math.Round(round1, 2).ToString();
                    }
                }
                else
                {
                    if (round1 > 0)
                    {
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                    else
                    {
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                }
                roundoff = Convert.ToDouble(txtroundoff.Text);
                txtbillamount.Text = (Math.Round(bamt)).ToString();
            }
            else
            {
                txttotbasicamount.Text = "0";
                txttotvat.Text = "0";
                txttotaddtax.Text = "0";
                txttotcst.Text = "0";
                txtbillamount.Text = "0";
            }
        }
        Page.SetFocus(txtgvamount1);
    }

    protected void txtgridtaxtype_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
        txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvsiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
            //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 >= 0.5)
            //{
            //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }
        Page.SetFocus(txtgridcstp);
    }

    protected void txtgridcstp_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        TextBox txtdesc1 = (TextBox)currentRow.FindControl("txtdesc1");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");

        var cc1 = txtgridcstp.Text.IndexOf("-");
        if (cc1 != -1)
        {
            txtgridcstp.Text = "0";
        }
        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
        txtgvrate1.Text = Math.Round(Convert.ToDouble(txtgvrate1.Text), 2).ToString();
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvsiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
            //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 >= 0.5)
            //{
            //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }
        Page.SetFocus(txtdesc1);
    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemssain"];
        //DataTable dtsoitem1 = (DataTable)Session["dtpitems12"];
        //if (dtsoitem1 != null)
        //{
        //    dtsoitem.Rows.Clear();
        //    dtsoitem.Merge(dtsoitem1);
        //}
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double vatamtc = 0;
        double addvatamtc = 0;
        double cstamtc = 0;
        double baseamtc = 0;
        double amount = 0;
        for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
        {
            TextBox txtgvqty1 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridamount");
            //            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            TextBox txtgridtaxtype = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridtaxtype");
            Label lbltaxtype = (Label)gvsiitemlist.Rows[c].FindControl("lbltaxtype");
            Label lblvatp = (Label)gvsiitemlist.Rows[c].FindControl("lblvatp");
            Label lbladdvatp = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatp");
            Label lblcstp = (Label)gvsiitemlist.Rows[c].FindControl("lblcstp");
            Label lblvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvsiitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblcstamt = (Label)gvsiitemlist.Rows[c].FindControl("lblcstamt");
            Label lblamount = (Label)gvsiitemlist.Rows[c].FindControl("lblamount");




            baseamt = baseamt + Math.Round((Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text)), 2);
            baseamtc = Math.Round((Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text)), 2);
            amount = amount + baseamt;
            if (txtgridtaxtype.Text != "")
            {
                var cc = txtgridtaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = txtgridtaxtype.Text;
                    vatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[1]);
                    //txtgvvat.Text = vatp.ToString();
                    //txtgvadvat.Text = addvatp.ToString();
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lblvatamt.Text = (Math.Round(((Convert.ToDouble(lblvatp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                    lbladdvatamt.Text = (Math.Round(((Convert.ToDouble(lbladdvatp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();

                }
                else
                {
                    lblcstamt.Text = (Math.Round(((Convert.ToDouble(lblcstp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                }
            }
            else
            {
                lblcstamt.Text = (Math.Round(((Convert.ToDouble(lblcstp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
            }
            if (lblvatamt.Text != "")
            {
                vatamt = vatamt + Convert.ToDouble(lblvatamt.Text);
                vatamtc = Convert.ToDouble(lblvatamt.Text);
            }
            if (lbladdvatamt.Text != "")
            {
                addvatamt = addvatamt + Convert.ToDouble(lbladdvatamt.Text);
                addvatamtc = Convert.ToDouble(lbladdvatamt.Text);
            }
            if (lblcstp.Text != "")
            {
                cstp = cstp + Convert.ToDouble(lblcstp.Text);
            }
            if (lblcstamt.Text != "")
            {
                cstamt = cstamt + Convert.ToDouble(lblcstamt.Text);
                cstamtc = Convert.ToDouble(lblcstamt.Text);
            }
            if (lblvatp.Text != "")
            {
                vatp = Convert.ToDouble(lblvatp.Text);
            }
            if (lbladdvatp.Text != "")
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
            }
            vatp = vatp + vatp + addvatp;
            lblamount.Text = (baseamtc + vatamtc + addvatamtc + cstamtc).ToString();
        }
        txttotbasicamount.Text = Math.Round(baseamt, 2).ToString();
        //if (txtservicep.Text.Trim() != string.Empty)
        //{
        //    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtservicep.Text = "0";
        //    txtserviceamt.Text = "0";
        //}


        double cartage = 0;
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        else
        {
            txtcartage.Text = "0";
        }
        double servp = 0;
        double servamt = 0;
        double roundoff = 0;
        if (txtservicep.Text.Trim() != string.Empty)
        {
            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        }
        else
        {
            txtservicep.Text = "0";
            txtserviceamt.Text = "0";
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        else
        {
            txtroundoff.Text = "0";
        }
        double serant = 0;
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            serant = Convert.ToDouble(txtserviceamt.Text);
        }



        //txtfcstp.Text = cstp.ToString();
        txttotcst.Text = Math.Round(cstamt, 2).ToString();
        txttotvat.Text = Math.Round(vatamt, 2).ToString();
        //txtfvatp.Text = vatp.ToString();
        txttotaddtax.Text = Math.Round(addvatamt, 2).ToString();
        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotvat.Text) + cartage).ToString();
        //double bamt = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        //double round1 = bamt - Math.Truncate(bamt);
        ////txtroundoff.Text = Math.Round(round1, 2).ToString();
        //if (round1 >= 0.5)
        //{
        //    txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
        //}
        //else
        //{
        //    txtroundoff.Text = Math.Round(round1, 2).ToString();
        //}
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
    }

    //protected void txtscno_TextChanged(object sender, EventArgs e)
    //{
    //    //if (txtscno.Text.Trim() != string.Empty)
    //    //{
    //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
    //    if (txtscno.Text.Trim() != string.Empty)
    //    {
    //        li.scno = Convert.ToInt64(txtscno.Text);
    //        DataTable dtdate = new DataTable();
    //        dtdate = fsiclass.selectscdatefromscno(li);
    //        txtscdate.Text = dtdate.Rows[0]["cdate"].ToString();
    //    }
    //    else
    //    {
    //        li.scno = 0;
    //    }
    //    if (txtscno1.Text.Trim() != string.Empty)
    //    {
    //        li.scno1 = Convert.ToInt64(txtscno1.Text);
    //        DataTable dtdate = new DataTable();
    //        dtdate = fsiclass.selectscdatefromscno1(li);
    //        txtscdate1.Text = dtdate.Rows[0]["cdate"].ToString();
    //    }
    //    else
    //    {
    //        li.scno1 = 0;
    //    }
    //    li.remarks = li.scno + "," + li.scno1;
    //    DataTable dtscmaster = new DataTable();
    //    dtscmaster = fsiclass.selectallscmasterdatafromscno(li);
    //    if (dtscmaster.Rows.Count > 0)
    //    {
    //        txtorderno.Text = dtscmaster.Rows[0]["sono"].ToString();
    //        txtname.Text = dtscmaster.Rows[0]["acname"].ToString();
    //        ViewState["ccode"] = dtscmaster.Rows[0]["ccode"].ToString();
    //    }
    //    DataTable dtdata = new DataTable();
    //    dtdata = fsiclass.selectallitemdatafromscnotexhchange(li);
    //    if (dtdata.Rows.Count > 0)
    //    {
    //        //if (dtsoitem != null)
    //        //{
    //        //    if (dtsoitem.Rows.Count > 0)
    //        //    {
    //        //        dtdata.Merge(dtsoitem);
    //        //    }
    //        //}
    //        //if (dtsoitem1 != null)
    //        //{
    //        //    if (dtsoitem1.Rows.Count > 0)
    //        //    {
    //        //        dtdata.Merge(dtsoitem1);
    //        //    }
    //        //}
    //        //lblempty.Visible = false;
    //        //gvsiitemlist.Visible = true;
    //        //gvsiitemlist.DataSource = dtdata;
    //        //gvsiitemlist.DataBind();
    //        //Session["dtpitems2"] = dtdata;
    //        DataTable dt = (DataTable)Session["dtpitems"];
    //        dt.Clear();
    //        for (int d = 0; d < dtdata.Rows.Count; d++)
    //        {
    //            DataRow dr = dt.NewRow();
    //            dr["id"] = dtdata.Rows[d]["id"].ToString();
    //            dr["vno"] = dtdata.Rows[d]["vno"].ToString();
    //            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
    //            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
    //            dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
    //            dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
    //            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
    //            dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
    //            dr["taxtype"] = "";
    //            dr["vatp"] = 0;
    //            dr["vatamt"] = 0;
    //            dr["addtaxp"] = 0;
    //            dr["addtaxamt"] = 0;
    //            dr["cstp"] = 0;
    //            dr["cstamt"] = 0;
    //            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
    //            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
    //            dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
    //            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
    //            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
    //            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
    //            dt.Rows.Add(dr);
    //            //count1();
    //        }
    //        Session["dtpitems"] = dt;
    //        this.bindgrid();
    //        countgv();
    //    }
    //    else
    //    {
    //        gvsiitemlist.Visible = false;
    //        lblempty.Visible = true;
    //        lblempty.Text = "No Items Found.";
    //    }


    //    //}
    //    //else
    //    //{
    //    //    gvsiitemlist.Visible = false;
    //    //    lblempty.Visible = false;
    //    //}
    //}
    //protected void txtscno1_TextChanged(object sender, EventArgs e)
    //{
    //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
    //    if (txtscno.Text.Trim() != string.Empty)
    //    {
    //        li.scno = Convert.ToInt64(txtscno.Text);
    //        DataTable dtdate = new DataTable();
    //        dtdate = fsiclass.selectscdatefromscno(li);
    //        txtscdate.Text = dtdate.Rows[0]["cdate"].ToString();
    //    }
    //    else
    //    {
    //        li.scno = 0;
    //    }
    //    if (txtscno1.Text.Trim() != string.Empty)
    //    {
    //        li.scno1 = Convert.ToInt64(txtscno1.Text);
    //        DataTable dtdate = new DataTable();
    //        dtdate = fsiclass.selectscdatefromscno1(li);
    //        if (dtdate.Rows.Count > 0)
    //        {
    //            txtscdate1.Text = dtdate.Rows[0]["cdate"].ToString();
    //        }
    //    }
    //    else
    //    {
    //        li.scno1 = 0;
    //    }
    //    li.remarks = li.scno + "," + li.scno1;
    //    DataTable dtscmaster = new DataTable();
    //    dtscmaster = fsiclass.selectallscmasterdatafromscno(li);
    //    if (dtscmaster.Rows.Count > 0)
    //    {
    //        txtorderno.Text = dtscmaster.Rows[0]["sono"].ToString();
    //        txtname.Text = dtscmaster.Rows[0]["acname"].ToString();
    //    }
    //    DataTable dtdata = new DataTable();
    //    dtdata = fsiclass.selectallitemdatafromscnotexhchange(li);
    //    if (dtdata.Rows.Count > 0)
    //    {
    //        //if (dtsoitem != null)
    //        //{
    //        //    if (dtsoitem.Rows.Count > 0)
    //        //    {
    //        //        dtdata.Merge(dtsoitem);
    //        //    }
    //        //}
    //        //if (dtsoitem1 != null)
    //        //{
    //        //    if (dtsoitem1.Rows.Count > 0)
    //        //    {
    //        //        dtdata.Merge(dtsoitem1);
    //        //    }
    //        //}
    //        //lblempty.Visible = false;
    //        //gvsiitemlist.Visible = true;
    //        //gvsiitemlist.DataSource = dtdata;
    //        //gvsiitemlist.DataBind();
    //        //Session["dtpitems2"] = dtdata;
    //        DataTable dt = (DataTable)Session["dtpitems"];
    //        dt.Clear();
    //        for (int d = 0; d < dtdata.Rows.Count; d++)
    //        {
    //            DataRow dr = dt.NewRow();
    //            dr["id"] = dtdata.Rows[d]["id"].ToString();
    //            dr["vno"] = dtdata.Rows[d]["vno"].ToString();
    //            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
    //            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
    //            dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
    //            dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
    //            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
    //            dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
    //            dr["taxtype"] = "";
    //            dr["vatp"] = 0;
    //            dr["vatamt"] = 0;
    //            dr["addtaxp"] = 0;
    //            dr["addtaxamt"] = 0;
    //            dr["cstp"] = 0;
    //            dr["cstamt"] = 0;
    //            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
    //            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
    //            dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
    //            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
    //            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
    //            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
    //            dt.Rows.Add(dr);
    //            //count1();
    //        }
    //        Session["dtpitems"] = dt;
    //        this.bindgrid();
    //        countgv();
    //    }
    //    else
    //    {
    //        gvsiitemlist.Visible = false;
    //        lblempty.Visible = true;
    //        lblempty.Text = "No Items Found.";
    //    }
    //}
    protected void lnkopenpopup_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
    }
    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != string.Empty)
        {
            lblpopupacname.Text = drpacname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            ////DataTable dtso = new DataTable();
            ////dtso = fsiclass.selectallsalesorderforpopup(li);
            ////if (dtso.Rows.Count > 0)
            ////{
            ////    lblemptyso.Visible = false;
            ////    gvsalesorders.Visible = true;
            ////    gvsalesorders.DataSource = dtso;
            ////    gvsalesorders.DataBind();
            ////}
            ////else
            ////{
            ////    gvsalesorders.Visible = false;
            ////    lblemptyso.Visible = true;
            ////    lblemptyso.Text = "No Sales Order Found.";
            ////}

            DataTable dtsc = new DataTable();
            dtsc = fsiclass.selectallsaleschallanforpopup(li);
            if (dtsc.Rows.Count > 0)
            {
                lblemptysc.Visible = false;
                gvsaleschallans.Visible = true;
                gvsaleschallans.DataSource = dtsc;
                gvsaleschallans.DataBind();
            }
            else
            {
                gvsaleschallans.Visible = false;
                lblemptysc.Visible = true;
                lblemptysc.Text = "No Sales Challan Found.";
            }
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }
    protected void gvsaleschallans_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemssain"] = CreateTemplate();
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblstrscno = (Label)currentRow.FindControl("lblstrscno");
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = e.CommandArgument.ToString();
            if (txtscno.Text.Trim() == string.Empty)
            {
                txtscno.Text = li.strscno;
                if (txtscno.Text.Trim() != string.Empty)
                {
                    li.strscno = txtscno.Text;
                    DataTable dtdate = new DataTable();
                    dtdate = fsiclass.selectscdatefromscnostring(li);
                    txtscdate.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.strscno = "0";
                }
                if (txtscno1.Text.Trim() != string.Empty)
                {
                    li.strscno1 = txtscno1.Text;
                    DataTable dtdate = new DataTable();
                    dtdate = fsiclass.selectscdatefromscno1string(li);
                    txtscdate1.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.strscno1 = "0";
                }
                li.remarks = "'" + li.strscno + "','" + li.strscno1 + "'";
                hdnscno.Value = "'" + li.strscno + "'";
                if (li.strscno1 != "0")
                {
                    hdnscno.Value = hdnscno.Value + "','" + li.strscno1 + "'";
                }
                txtsrtringscno.Text = hdnscno.Value;
                DataTable dtscmaster = new DataTable();
                dtscmaster = fsiclass.selectallscmasterdatafromscnostring(li);
                if (dtscmaster.Rows.Count > 0)
                {
                    txtorderno.Text = dtscmaster.Rows[0]["sono"].ToString();
                    if (dtscmaster.Rows[0]["sodate"].ToString() != string.Empty)
                    {
                        txtsodate.Text = Convert.ToDateTime(dtscmaster.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    //drpacname.SelectedValue = dtscmaster.Rows[0]["acname"].ToString();
                    ViewState["ccode"] = dtscmaster.Rows[0]["ccode"].ToString();
                }
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallitemdatafromscnotexhchangestring(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int w = 0; w < dtdata.Rows.Count; w++)
                    {
                        if (dtdata.Rows[w]["gsttype"].ToString().Trim() == string.Empty || dtdata.Rows[w]["gsttype"].ToString().Trim() == "")
                        {
                            txtscno.Text = string.Empty;
                            txtsrtringscno.Text = string.Empty;
                            hdnscno.Value = string.Empty;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter gst type for " + dtdata.Rows[w]["itemname"].ToString() + " and try again.');", true);
                            return;
                        }
                    }
                    //if (dtsoitem != null)
                    //{
                    //    if (dtsoitem.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem);
                    //    }
                    //}
                    //if (dtsoitem1 != null)
                    //{
                    //    if (dtsoitem1.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem1);
                    //    }
                    //}
                    //lblempty.Visible = false;
                    //gvsiitemlist.Visible = true;
                    //gvsiitemlist.DataSource = dtdata;
                    //gvsiitemlist.DataBind();
                    //Session["dtpitems2"] = dtdata;
                    DataTable dt = (DataTable)Session["dtpitemssain"];
                    dt.Clear();
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = dtdata.Rows[d]["stockqty"].ToString();
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        if (chkigst.Checked == true && drpinvtype.SelectedItem.Text == "GST")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                                dr["taxtype"] = 0;
                                dr["taxdesc"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else if (chkigst.Checked == false && drpinvtype.SelectedItem.Text == "GST")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = 0;
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["cstp"] = 0;
                                dr["taxdesc"] = dtdata.Rows[d]["gsttype"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gsttype"].ToString();
                            }

                        }
                        dr["vatp"] = 0;
                        dr["vatamt"] = 0;
                        dr["addtaxp"] = 0;
                        dr["addtaxamt"] = 0;
                        dr["cstamt"] = 0;
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                        dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        //count1();
                    }
                    Session["dtpitemssain"] = dt;
                    this.bindgrid();
                    countgv();
                }
                else
                {
                    gvsiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            else
            {
                txtscno1.Text = li.strscno;
                if (txtscno1.Text.Trim() != string.Empty)
                {
                    txtscno1.Text = li.strscno;
                    hdnscno.Value = hdnscno.Value + ",'" + txtscno1.Text + "'";
                }
                else
                {
                    hdnscno.Value = hdnscno.Value + ",'" + e.CommandArgument.ToString() + "'";
                }
                if (txtscno.Text.Trim() != string.Empty)
                {
                    li.strscno = txtscno.Text;
                    DataTable dtdate = new DataTable();
                    dtdate = fsiclass.selectscdatefromscnostring(li);
                    txtscdate.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.scno = 0;
                }
                if (txtscno1.Text.Trim() != string.Empty)
                {
                    li.strscno1 = txtscno1.Text;
                    DataTable dtdate = new DataTable();
                    dtdate = fsiclass.selectscdatefromscno1string(li);
                    txtscdate1.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.scno1 = 0;
                }
                li.remarks = hdnscno.Value;
                txtsrtringscno.Text = hdnscno.Value;
                DataTable dtscmaster = new DataTable();
                dtscmaster = fsiclass.selectallscmasterdatafromscnostring(li);
                if (dtscmaster.Rows.Count > 0)
                {
                    txtorderno.Text = dtscmaster.Rows[0]["sono"].ToString();
                    //drpacname.SelectedValue = dtscmaster.Rows[0]["acname"].ToString();
                }
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallitemdatafromscnotexhchangestring(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int w = 0; w < dtdata.Rows.Count; w++)
                    {
                        if (dtdata.Rows[w]["gsttype"].ToString().Trim() == string.Empty || dtdata.Rows[w]["gsttype"].ToString().Trim() == "")
                        {
                            txtscno1.Text = string.Empty;
                            txtsrtringscno.Text = "'" + txtscno.Text + "'";
                            hdnscno.Value = "'" + txtscno.Text + "'";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter gst type for " + dtdata.Rows[w]["itemname"].ToString() + " and try again.');", true);
                            return;
                        }
                    }
                    //if (dtsoitem != null)
                    //{
                    //    if (dtsoitem.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem);
                    //    }
                    //}
                    //if (dtsoitem1 != null)
                    //{
                    //    if (dtsoitem1.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem1);
                    //    }
                    //}
                    //lblempty.Visible = false;
                    //gvsiitemlist.Visible = true;
                    //gvsiitemlist.DataSource = dtdata;
                    //gvsiitemlist.DataBind();
                    //Session["dtpitems2"] = dtdata;
                    DataTable dt = (DataTable)Session["dtpitemssain"];
                    dt.Clear();
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = dtdata.Rows[d]["stockqty"].ToString();
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        //if (dtdata.Rows[0]["vattype"].ToString().IndexOf("-") != -1)
                        //{
                        //    dr["taxtype"] = dtdata.Rows[d]["vattype"].ToString();
                        //    dr["cstp"] = 0;
                        //}
                        //else
                        //{
                        //    dr["taxtype"] = "";
                        //    dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                        //}
                        if (chkigst.Checked == true && drpinvtype.SelectedItem.Text == "GST")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                                dr["taxtype"] = 0;
                                dr["taxdesc"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gsttype"].ToString();
                            }
                        }
                        else
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["cstp"] = 0;
                                dr["taxdesc"] = dtdata.Rows[d]["gsttype"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gsttype"].ToString();
                            }

                        }
                        //dr["taxdesc"] = "";
                        dr["vatp"] = 0;
                        dr["vatamt"] = 0;
                        dr["addtaxp"] = 0;
                        dr["addtaxamt"] = 0;
                        //dr["cstp"] = 0;
                        dr["cstamt"] = 0;
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                        dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        //count1();
                    }
                    Session["dtpitemssain"] = dt;
                    this.bindgrid();
                    countgv();
                }
                else
                {
                    gvsiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            ModalPopupExtender2.Hide();
            hideimage();
        }
        hideimage();
        Page.SetFocus(drpsalesac);
    }
    protected void gvsalesorders_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(e.CommandArgument);
            txtorderno.Text = e.CommandArgument.ToString();
            DataTable dtdata = new DataTable();
            dtdata = fsiclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                DataTable dt = (DataTable)Session["dtpitemssain"];
                dt.Clear();
                for (int d = 0; d < dtdata.Rows.Count; d++)
                {
                    DataRow dr = dt.NewRow();
                    dr["id"] = 1;
                    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                    dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
                    dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                    dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                    dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                    dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                    dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
                    dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();
                    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                    dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                    dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                    dt.Rows.Add(dr);
                    //count1();
                }
                Session["dtpitemssain"] = dt;
                this.bindgrid();
                countgv();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            ModalPopupExtender2.Hide();
        }
    }
    protected void gvsiitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.scno = Convert.ToInt64(txtinvoiceno.Text);
            li.sidate = Convert.ToDateTime(txtsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            li.qty = Convert.ToDouble(lblqty.Text);
            li.vid = Convert.ToInt64(e.CommandArgument);
            DataTable dtremain = new DataTable();
            dtremain = fsiclass.selectqtyremainusedfromscno(li);
            if (Convert.ToDouble(txtgvqty1.Text) > 0)
            {
                if (dtremain.Rows.Count > 0)
                {
                    //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                    li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                    fsiclass.updateremainqtyinsaleschallan(li);


                    //update siitems data
                    //for (int c = 0; c < gvsiitemlist.Rows.Count; c++)
                    //{                    
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblvid = (Label)currentRow.FindControl("lblvid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblunit = (Label)currentRow.FindControl("lblunit");
                    TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgridtaxdesc = (TextBox)currentRow.FindControl("txtgridtaxdesc");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    if (txtgvstockqty.Text.Trim() != string.Empty)
                    {
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    }
                    else
                    {
                        li.stockqty = 0;
                    }
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    li.unit = txtgvunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    li.taxdesc = txtgridtaxdesc.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    if (lblccode.Text != "")
                    {
                        li.ccode = Convert.ToInt64(lblccode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                    li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    fsiclass.updatesiitemsdata(li);

                    //li.vid = Convert.ToInt64(lblvid.Text);
                    //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                    //fsiclass.updatestrsinoinscitems1(li);
                    //}
                    //}


                }
                else
                {
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblvid = (Label)currentRow.FindControl("lblvid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblunit = (Label)currentRow.FindControl("lblunit");
                    TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgridtaxdesc = (TextBox)currentRow.FindControl("txtgridtaxdesc");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    li.unit = txtgvunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    li.taxdesc = txtgridtaxdesc.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    if (lblccode.Text != "")
                    {
                        li.ccode = Convert.ToInt64(lblccode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                    li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    fsiclass.updatesiitemsdata(li);

                    //li.vid = Convert.ToInt64(lblvid.Text);
                    //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                    //fsiclass.updatestrsinoinscitems1(li);
                }
            }
            else
            {
                Label lblid = (Label)currentRow.FindControl("lblid");
                Label lblvid = (Label)currentRow.FindControl("lblvid");
                Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                //TextBox lblqty = (TextBox)gvsiitemlist.Rows[c].FindControl("txtgridqty");
                TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                Label lblunit = (Label)currentRow.FindControl("lblunit");
                TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                Label lblamount = (Label)currentRow.FindControl("lblamount");
                Label lblccode = (Label)currentRow.FindControl("lblccode");
                TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                TextBox txtgridtaxdesc = (TextBox)currentRow.FindControl("txtgridtaxdesc");
                TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(txtgvqty1.Text);
                li.qtyremain = li.qty;
                li.qtyused = 0;
                //if (li.qty > 0)
                //{
                li.unit = txtgvunit.Text;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.taxtype = lbltaxtype.Text;
                li.taxdesc = txtgridtaxdesc.Text;
                if (lblvatp.Text != "")
                {
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                }
                else
                {
                    li.vatp = 0;
                }
                if (lbladdvatp.Text != "")
                {
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                }
                else
                {
                    li.addtaxp = 0;
                }
                if (lblcstp.Text != "")
                {
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                }
                else
                {
                    li.cstp = 0;
                }
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                if (lblsctamt.Text != "")
                {
                    li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                }
                else
                {
                    li.cstamt = 0;
                }
                if (lblvatamt.Text != "")
                {
                    li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                }
                else
                {
                    li.vatamt = 0;
                }
                if (lbladdvatamt.Text != "")
                {
                    li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                }
                else
                {
                    li.addtaxamt = 0;
                }
                if (lblamount.Text != "")
                {
                    li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                }
                else
                {
                    li.amount = 0;
                }
                if (lblccode.Text != "")
                {
                    li.ccode = Convert.ToInt64(lblccode.Text);
                }
                else
                {
                    li.ccode = 0;
                }
                li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                fsiclass.updatesiitemsdata(li);

                //li.vid = Convert.ToInt64(lblvid.Text);
                //li.strsino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                //fsiclass.updatestrsinoinscitems1(li);
            }
            li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            DataTable dtitems = new DataTable();
            dtitems = fsiclass.selectallsiitemsfromsinostring(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsiitemlist.Visible = true;
                gvsiitemlist.DataSource = dtitems;
                gvsiitemlist.DataBind();
            }
            else
            {
                gvsiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Invoice Item Found.";
            }
        }
    }
    protected void txtinvoiceno_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fsiclass.selectallsimasterdatafromsinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fsiclass.updateisused(li);
                getsino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fsiclass.selectallsimasterdatafromsinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fsiclass.updateisused(li);
                //getsino();
                txtinvoiceno.Text = ViewState["sino"].ToString();
                drpinvtype.SelectedValue = ViewState["invtype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(txtorderno);
    }
    protected void drpinvtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fsiclass.selectallsimasterdatafromsinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fsiclass.updateisused(li);
                getsino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            li.strsino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fsiclass.selectallsimasterdatafromsinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fsiclass.updateisused(li);
                //getsino();
                txtinvoiceno.Text = ViewState["sino"].ToString();
                drpinvtype.SelectedValue = ViewState["invtype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(txtorderno);
    }
    protected void gvsiitemlist_DataBound(object sender, EventArgs e)
    {
        SetTabIndexes();
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                string vattype = dtdata.Rows[0]["gsttype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvat.Text = "0";
                    txtgvadtax.Text = vattype.Split('-')[1];
                    txtaddtax.Text = "0";
                    count1();
                }
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadtax.Text = string.Empty;
                txtvat.Text = string.Empty;
                txtaddtax.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtamount.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadtax.Text = string.Empty;
            txtvat.Text = string.Empty;
            txtaddtax.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtamount.Text = string.Empty;
        }
        Page.SetFocus(txtbillqty);
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' order by sino", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strsino = li.invtype + dtdataq.Rows[0]["sino"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
                ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
                txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
                ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
                if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                {
                    txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtscno.Text = dtdata.Rows[0]["scno"].ToString();
                txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
                if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
                {
                    txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();
                txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                txtform.Text = dtdata.Rows[0]["form"].ToString();
                txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
                txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
                //{
                //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                //}
                //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
                //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
                DataTable dtitems = new DataTable();
                dtitems = fsiclass.selectallsiitemsfromsinostring(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsiitemlist.Visible = true;
                    gvsiitemlist.DataSource = dtitems;
                    gvsiitemlist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvsiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Invoice Item Found.";
                    lblcount.Text = "0";
                }
                btnsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' order by sino", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strsino = li.invtype + dtdataq.Rows[dtdataq.Rows.Count - 1]["sino"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
                ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
                txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
                ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
                if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                {
                    txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtscno.Text = dtdata.Rows[0]["scno"].ToString();
                txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
                if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
                {
                    txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();
                txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                txtform.Text = dtdata.Rows[0]["form"].ToString();
                txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
                txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
                //{
                //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                //}
                //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
                //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
                DataTable dtitems = new DataTable();
                dtitems = fsiclass.selectallsiitemsfromsinostring(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsiitemlist.Visible = true;
                    gvsiitemlist.DataSource = dtitems;
                    gvsiitemlist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvsiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Invoice Item Found.";
                    lblcount.Text = "0";
                }
                btnsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' order by sino", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.sino = Convert.ToInt64(txtinvoiceno.Text) + 1;
        for (Int64 i = li.sino; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["sino"].ToString()); i++)
        {
            li.sino = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' and sino=" + li.sino + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strsino = li.invtype + dtdataq.Rows[0]["sino"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
                    ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
                    txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
                    ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                    txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
                    if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                    {
                        txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtscno.Text = dtdata.Rows[0]["scno"].ToString();
                    txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
                    if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
                    {
                        txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();
                    txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                    txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                    txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                    txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                    txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                    txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                    txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                    txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                    txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                    txtform.Text = dtdata.Rows[0]["form"].ToString();
                    txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                    txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
                    txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
                    //{
                    //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    //}
                    //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
                    //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
                    DataTable dtitems = new DataTable();
                    dtitems = fsiclass.selectallsiitemsfromsinostring(li);
                    if (dtitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvsiitemlist.Visible = true;
                        gvsiitemlist.DataSource = dtitems;
                        gvsiitemlist.DataBind();
                        lblcount.Text = dtitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvsiitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Invoice Item Found.";
                        lblcount.Text = "0";
                    }
                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' order by sino", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.sino = Convert.ToInt64(txtinvoiceno.Text) - 1;
        for (Int64 i = li.sino; i >= Convert.ToInt64(dtdataqa.Rows[0]["sino"].ToString()); i--)
        {
            li.sino = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SIMaster where invtype='" + li.invtype + "' and sino=" + li.sino + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strsino = li.invtype + dtdataq.Rows[0]["sino"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallsimasterdatafromsinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtinvoiceno.Text = dtdata.Rows[0]["sino"].ToString();
                    ViewState["sino"] = Convert.ToInt64(dtdata.Rows[0]["sino"].ToString());
                    txtsidate.Text = Convert.ToDateTime(dtdata.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpinvtype.SelectedValue = dtdata.Rows[0]["invtype"].ToString();
                    ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                    txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
                    if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                    {
                        txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtscno.Text = dtdata.Rows[0]["scno"].ToString();
                    txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtscno1.Text = dtdata.Rows[0]["scno1"].ToString();
                    if (dtdata.Rows[0]["scdate1"].ToString() != string.Empty)
                    {
                        txtscdate1.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drpsalesac.SelectedValue = dtdata.Rows[0]["salesac"].ToString();
                    txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                    txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                    txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                    txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                    txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                    txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                    txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                    txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                    txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                    txtform.Text = dtdata.Rows[0]["form"].ToString();
                    txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                    txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    txtsrtringscno.Text = dtdata.Rows[0]["stringscno"].ToString();
                    txtdtors.Text = dtdata.Rows[0]["dtors"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    //if (dtdata.Rows[0]["sbilldate"].ToString() != string.Empty)
                    //{
                    //    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["sbilldate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    //}
                    //txtbillno.Text = dtdata.Rows[0]["sbillno"].ToString();
                    //txtportcode.Text = dtdata.Rows[0]["portcode"].ToString();
                    DataTable dtitems = new DataTable();
                    dtitems = fsiclass.selectallsiitemsfromsinostring(li);
                    if (dtitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvsiitemlist.Visible = true;
                        gvsiitemlist.DataSource = dtitems;
                        gvsiitemlist.DataBind();
                        lblcount.Text = dtitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvsiitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Invoice Item Found.";
                        lblcount.Text = "0";
                    }
                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }

    protected void lnkewaypopup_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
    }
    protected void lnkewayselectionpopup_Click(object sender, EventArgs e)
    {

    }
    protected void drpeway_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpeway.SelectedItem.Text == "YES")
        {
            ModalPopupExtender1.Show();
        }
        else
        {
            ModalPopupExtender1.Hide();
        }
    }
    protected void btnsaveewaydata_Click(object sender, EventArgs e)
    {
        //if (drpfromstate.SelectedItem.Text == "--SELECT--")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select From State.');", true);
        //    return;
        //}
        //if (drptostate.SelectedItem.Text == "--SELECT--")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select To State.');", true);
        //    return;
        //}
        //if (drpactualfromstate.SelectedItem.Text == "--SELECT--")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Actual From State.');", true);
        //    return;
        //}
        //if (drpactualtostate.SelectedItem.Text == "--SELECT--")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Actual To State.');", true);
        //    return;
        //}
        li.strsino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

        li.ewaybillno = txtewaybillno.Text;
        li.ewaybilldate = Convert.ToDateTime(txtdate1.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.cewaybillno = txtconsolidatedewaybillno.Text;
        li.cewaybilldate = Convert.ToDateTime(txtdate2.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.supplytype = drpsupplytype.SelectedValue;
        li.subtype = drpsubtype.SelectedValue;
        li.doctype = drpdocumenttype.SelectedValue;

        li.tramode = drpmode.SelectedValue;
        li.distanceinkm = txtdistance.Text;
        li.transportername = txttransportername.Text;
        li.vehiclenumber = txtvehiclenumber.Text;
        li.vehicletype = drpvehicletype.SelectedValue;
        DataTable dtmainhsncode = fsiclass.selectmainhsncode(li);
        if (dtmainhsncode.Rows.Count > 0)
        {
            li.mainhsncode = dtmainhsncode.Rows[0]["hsncode"].ToString();
        }
        li.trano = txtdoclandingrrairwayno.Text;
        li.tradate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.traid = txttransporterid.Text;

        li.consignor = txtconsignor.Text;
        li.consignoradd1 = txtaddress1.Text;
        li.consignoradd2 = txtaddress2.Text;
        li.consignorplace = txtplace.Text;
        li.consignorpincode = txtpincode.Text;
        li.consignorstate = drpfromstate.SelectedValue;
        li.actualfromstate = drpactualfromstate.SelectedValue;
        li.consignorgst = txtgstinuin.Text;

        li.consignee = txtconsignee.Text;
        li.consigneeadd1 = txtconsigneeaddress1.Text;
        li.consigneeadd2 = txtconsigneeaddress2.Text;
        li.consigneeplace = txttoplace.Text;
        li.consigneepincode = txtconsigneepincode.Text;
        li.consigneestate = drptostate.SelectedValue;
        li.actualtostate = drpactualtostate.SelectedValue;
        li.consigneegst = txtconsigneegstinuin.Text;
        fsiclass.updateewaybilldetailfromstrsino(li);

        DataTable table = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select consignorgst as userGstin,supplytype as supplyType,subtype as subSupplyType,doctype as docType,strsino as docNo,CONVERT(varchar(10),sidate,103) as docDate,transType=1,consignorgst as fromGstin,consignor as fromTrdName,consignoradd1 as fromAddr1,consignoradd2 as fromAddr2,consignorplace as fromPlace,consignorpincode as fromPincode,consignorstate as fromStateCode,actualfromstatecode as actualFromStateCode,consigneegst as toGstin,consignee as toTrdName,consigneeadd1 as toAddr1,consigneeadd2 as toAddr2,consigneeplace as toPlace,consigneepincode as toPincode,consigneestate as toStateCode,actualtostatecode as actualToStateCode,totbasicamount as totalValue,totvat as cgstValue,totaddtax as sgstValue,totcst as igstValue,cessValue=0,TotNonAdvolVal=0,tramode as transMode,distanceinkm as transDistance,transportername as transporterName,traid as transporterId,trano as transDocNo,CONVERT(varchar(10),tradate,103) as transDocDate,vehicleno as vehicleNo,vehicletype as vehicleType,(totbasicamount+totvat+totaddtax+totcst) as totInvValue,OthValue=0,mainhsncode as mainHsnCode from simaster where strsino='" + li.strsino + "'", con);
        da.Fill(table);

        var JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            string cc = "";
            cc = "{" + "\"" + "version" + "\"" + ":" + "\"" + "1.0.1118" + "\"" + "," + "\"" + "billLists" + "\"" + ":";
            JSONString.Append(cc + "[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
            }
            cc = "," + "\"" + "itemList" + "\"" + ":";
            JSONString.Append(cc + "[");
            SqlDataAdapter da1 = new SqlDataAdapter("select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS ItemNo,SIItems.itemname as productName,(SIItems.descr1+SIItems.descr2+SIItems.descr3) as productDesc,ItemMaster.hsncode as hsnCode,SIItems.qty as quantity,SCItems.eunit as qtyUnit,SIItems.basicamount as taxableAmount,SIItems.vatp as sgstRate,SIItems.addtaxp as cgstRate,SIItems.cstp as igstRate,cessRate=0,cessNonAdvol=0 from SIItems inner join ItemMaster on SIItems.itemname=ItemMaster.itemname inner join SCItems on SCItems.id=SIItems.vid where SIItems.strsino='" + li.strsino + "'", con);
            DataTable dtc = new DataTable();
            da1.Fill(dtc);
            //for (int i1 = 0; i1 < dtc.Rows.Count; i1++)
            //{
            //    if (dtc.Rows[i1]["qtyUnit"].ToString() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No E-Way bill unit is available for item " + dtc.Rows[i1]["productName"].ToString() + ".');", true);
            //        return;
            //    }
            //}
            for (int i = 0; i < dtc.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < dtc.Columns.Count; j++)
                {
                    if (j < dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == dtc.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]}]}");
            string path1 = @"D:\Eway Bill JSON";
            if (!Directory.Exists(path1))
            {
                Directory.CreateDirectory(path1);
            }
            string path = @"D:\Eway Bill JSON\" + li.strsino + ".json";
            using (var file = new StreamWriter(path, false))
            {
                file.Write(JSONString);
                file.Close();
                file.Dispose();

                string strURL = Server.MapPath(@"~/Eway Bill JSON/" + li.strsino + ".json");
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + li.strsino + ".json\"");
                byte[] data = req.DownloadData(strURL);
                response.BinaryWrite(data);
                response.End();
                ModalPopupExtender1.Hide();
            }
        }
        //return JSONString.ToString();

        Response.Redirect("~/SalesInvoiceList.aspx?pagename=SalesInvoiceList");
    }
    protected void txtconsigneegstinuin_TextChanged(object sender, EventArgs e)
    {
        if (txtconsigneegstinuin.Text.Trim() != string.Empty)
        {
            drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
        }
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
        Response.Redirect("~/SalesInvoiceList.aspx?pagename=SalesInvoiceList");
    }
    protected void drpsalesac_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpsalesac.SelectedItem.Text != "--SELECT--")
        {
            li.acname = drpsalesac.SelectedItem.Text;
            DataTable dtata = fsiclass.selectallacdatefromacname(li);
            ViewState["excludetax"] = dtata.Rows[0]["excludetax"].ToString();
        }
    }
}