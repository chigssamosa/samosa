﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepLedger.aspx.cs" Inherits="RepLedger" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Ledger Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    AC Name</label></div>
            <div class="col-md-3">
                <asp:TextBox ID="txtacname" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    CCode</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtccode" runat="server" CssClass="form-control" Width="200px" AutoPostBack="True"
                    OnTextChanged="txtccode_TextChanged"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtccode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
                <%--<asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>--%>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Group Code</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtgroupcode" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-3">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsaves" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnsaves_Click" ValidationGroup="val" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnfilll" runat="server" Text="Fill Ledger" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnfilll_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkmonthwise" runat="server" Text="Monthwise Ledger" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter bank name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtacname"></asp:RequiredFieldValidator>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvledgerlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvledgerlist_RowCommand">
                        <Columns>
                            <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="process" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Change Item Name" Height="20px" Width="20px" CausesValidation="False"
                                        CommandArgument='<%# bind("strvoucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Redirect" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strvoucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher No." SortExpression="Csnm" ItemStyle-Width="80px">
                                <ItemTemplate>
                                    <asp:Label ID="lblstrvoucherno" ForeColor="Black" runat="server" Text='<%# bind("strvoucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherdate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy")%>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="Csnm" ItemStyle-Width="300px">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescription" ForeColor="Black" runat="server" Text='<%# bind("description") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ACName" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreditcode" ForeColor="Black" runat="server" Text='<%# bind("creditcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblistype" ForeColor="Black" runat="server" Text='<%# bind("istype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Debit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldebitamt" ForeColor="Black" runat="server" Text='<%# bind("debitamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Credit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreditamt" ForeColor="Black" runat="server" Text='<%# bind("creditamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closing" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclosingbal" ForeColor="Black" runat="server" Text='<%# bind("closingbal") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cr./Dr." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcrdr" ForeColor="Black" runat="server" Text='<%# bind("crdr") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                    <asp:Label ID="lblempty1" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvmonthlyledger" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvmonthlyledger_RowCommand">
                        <Columns>
                            <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="process" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Change Item Name" Height="20px" Width="20px" CausesValidation="False"
                                        CommandArgument='<%# bind("strvoucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="selectmonth" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Redirect" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("month") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Month" SortExpression="Csnm" ItemStyle-Width="80px">
                                <ItemTemplate>
                                    <asp:Label ID="lblmonth" ForeColor="Black" runat="server" Text='<%# bind("month") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Year" SortExpression="Csnm" ItemStyle-Width="80px">
                                <ItemTemplate>
                                    <asp:Label ID="lblyear" ForeColor="Black" runat="server" Text='<%# bind("year") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Opening" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblopening" ForeColor="Black" runat="server" Text='<%# bind("openingamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Debit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldebitamt" ForeColor="Black" runat="server" Text='<%# bind("debitamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Credit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreditamt" ForeColor="Black" runat="server" Text='<%# bind("creditamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closing" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclosingbal" ForeColor="Black" runat="server" Text='<%# bind("closingbal") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcrdr" ForeColor="Black" runat="server" Text='<%# bind("crdr") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
