﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PerformaInvoice.aspx.cs" Inherits="PerformaInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container" style="width: 100%">
        <span style="color: white; background-color: Red">Quotation</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Per. Inv. No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtquotationno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsodate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox></div>
                    <div class="col-md-1">
                        <asp:CheckBox ID="chkigst" runat="server" Text="IGST" AutoPostBack="True" OnCheckedChanged="chkigst_CheckedChanged" /></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" onkeypress="return checkQuote();"
                            AutoPostBack="true" OnTextChanged="txtname_TextChanged"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtclientcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            PO No.<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpono" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpodate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Size="22px"></asp:Label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsono" runat="server" CssClass="form-control" placeholder="SO No.(Alt+s)"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Unit</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Basic Amount</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST %
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Ad.Vat/CGST%
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    CST/IGST %
                </label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    CST/IGST Amt</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 3</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Add.Vat/CGST</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Amount</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                    Width="90px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtqty1" runat="server" CssClass="form-control" placeholder="Qty"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtqty1_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate1" runat="server" CssClass="form-control" placeholder="Sales Rate"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtrate1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbasicamt_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txttaxtype" runat="server" CssClass="form-control" placeholder="Tax Type"
                    Width="90px" AutoPostBack="true" OnTextChanged="txttaxtype_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttaxtype"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getvattype">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvvat" runat="server" CssClass="form-control" placeholder="VAT%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvvat_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvadvat" runat="server" CssClass="form-control" placeholder="Ad.Vat%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvadvat_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvcst" runat="server" CssClass="form-control" placeholder="CST%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvcst_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtcstamount" runat="server" CssClass="form-control" placeholder="CST"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtcstamount_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="border: 1px solid;" runat="server" visible="false">
            <div class="col-md-2" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="200px" onkeypress="return checkQuote();"></asp:TextBox>--%>
                <%--<asp:DropDownList ID="drpdescription1" runat="server" CssClass="form-control">
                </asp:DropDownList>--%>
                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtvatamt" runat="server" CssClass="form-control" placeholder="VAT"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtvatamt_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtaddtaxamt" runat="server" CssClass="form-control" placeholder="Add.Vat"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtaddtaxamt_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvamount" runat="server" CssClass="form-control" placeholder="Amount"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvamount_TextChanged" ReadOnly="true"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvsalesorder" OnClick="btnadd_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvsalesorder" />
            </div>
        </div>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvslist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvslist_RowCommand" OnRowDeleting="gvslist_RowDeleting">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="select" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvunit" runat="server" Width="120px" Text='<%# bind("unit") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="80px" AutoPostBack="True" OnTextChanged="txtqty_TextChanged"
                                        Text='<%# bind("qty") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtrate" runat="server" Width="120px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged"
                                        Text='<%# bind("rate") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtamount" runat="server" Width="120px" Text='<%# bind("basicamt") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                    <%--<asp:DropDownList ID="drpdescr1" runat="server">
                                    </asp:DropDownList>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvtaxtype" runat="server" Text='<%# bind("taxtype") %>' AutoPostBack="True"
                                        OnTextChanged="txtgvtaxtype_TextChanged"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtgvtaxtype"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getvattype">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1" runat="server" Text='<%# bind("vatp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2" runat="server" Text='<%# bind("addtaxp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3" runat="server" Text='<%# bind("cstp") %>' AutoPostBack="True"
                                        OnTextChanged="txttax3_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1amt" runat="server" Text='<%# bind("vatamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2amt" runat="server" Text='<%# bind("addtaxamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3amt" runat="server" Text='<%# bind("cstamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvamount" runat="server" Text='<%# bind("amount") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvattype" ForeColor="Black" runat="server" Text='<%# bind("taxtype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatp" ForeColor="Black" runat="server" Text='<%# bind("vatp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatp" ForeColor="Black" runat="server" Text='<%# bind("addtaxp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstp" ForeColor="Black" runat="server" Text='<%# bind("cstp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstamt" ForeColor="Black" runat="server" Text='<%# bind("cstamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatamt" ForeColor="Black" runat="server" Text='<%# bind("vatamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatamt" ForeColor="Black" runat="server" Text='<%# bind("addtaxamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldesc1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldesc2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Basic</label>
                </div>
                <div class="col-md-4">
                    <asp:TextBox ID="txtbasicamount" runat="server" CssClass="form-control" Width="150px"
                        Text="0" onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        CST</label>
                </div>
                <div class="col-md-1">
                    <asp:TextBox ID="txtcst" runat="server" CssClass="form-control" Width="150px" Text="0"
                        onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        VAT</label>
                </div>
                <div class="col-md-4">
                    <asp:TextBox ID="txtvat" runat="server" CssClass="form-control" Width="150px" Text="0"
                        onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        Total S.O.</label>
                </div>
                <div class="col-md-1">
                    <asp:TextBox ID="txttotalso" runat="server" CssClass="form-control" Width="150px"
                        Text="0" onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        AD. VAT</label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtadvat" runat="server" CssClass="form-control" Width="150px" Text="0"
                        onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Status<span style="color: Red;">*</span></label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <asp:TextBox ID="txtannexure" runat="server" placeholder="Annexure" TextMode="MultiLine"
                Height="200px" Width="90%" onkeypress="return checkQuote();"></asp:TextBox>
        </div>
        <div class="row" style="height: 10px">
        </div>
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfinalsave" runat="server" Text="Save" class="btn btn-default forbutton"
                    ValidationGroup="valsalesorder" OnClick="btnfinalsave_Click" /></div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valsalesorder" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter so date."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtsodate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="So Date must be dd-MM-yyyy" ValidationGroup="valsalesorder" ForeColor="Red"
                ControlToValidate="txtsodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Po Date must be dd-MM-yyyy" ValidationGroup="valsalesorder" ForeColor="Red"
                ControlToValidate="txtpodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="valsalesorder" InitialValue="--SELECT--" ControlToValidate="drpacname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter po no."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtpono"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter po date."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtpodate"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvsalesorder" InitialValue="--SELECT--" ControlToValidate="drpitemname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter qty."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtqty1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtrate1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter tax type."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txttaxtype"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter vat%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter ad.vat%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvadvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter cst%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Please enter cst."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtcstamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtvatamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Please enter add.vat."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtaddtaxamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtbasicamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter cst."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please enter total so."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txttotalso"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter add. vat."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtadvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valsalesorder" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
            TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
            Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>Sales Order Selection</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>A/C Name :</b></div>
                <div class="col-md-7">
                    <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
            </div>
            <div class="row">
                <asp:Label ID="lblemptyso" runat="server" ForeColor="Red"></asp:Label>
                <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                    OnClick="btnselectitem_Click" />
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                    <asp:GridView ID="gvsalesorder" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvsalesorder_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("sono") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Purchase Chlln No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("sono") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblpcdate" runat="server" ForeColor="#505050" Text='<%# bind("sodate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvsoitemlistselection" runat="server" AutoGenerateColumns="False"
                        Width="1300px" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />--%>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                                <%--<HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
            </div>
        </asp:Panel>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtsodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
