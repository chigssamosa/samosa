﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Misc : System.Web.UI.Page
{
    ForMiscMaster fmmclass = new ForMiscMaster();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillitemnamedrop();
            lblemptyitem.Visible = true;
            lblemptyitem.Text = Math.Round(0.6).ToString() + "," + Math.Truncate(0.6).ToString();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.type = drptype.SelectedItem.Text;
        DataTable dtdata = new DataTable();
        dtdata = fmmclass.selectmiscdatafromtype(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvmisclist.Visible = true;
            gvmisclist.DataSource = dtdata;
            gvmisclist.DataBind();
        }
        else
        {
            gvmisclist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Misc Data Found for this Type.";
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    protected void drptype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drptype.SelectedItem.Text != "--SELECT--")
        {
            fillgrid();
        }
        else
        {
            gvmisclist.DataSource = null;
            gvmisclist.DataBind();
            gvmisclist.Visible = false;
        }
    }
    protected void gvmisclist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.id = Convert.ToInt64(e.CommandArgument);
                fmmclass.deletemiscdata(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "Misc Item Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvmisclist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            li.type = drptype.SelectedItem.Text;
            li.name = txtname.Text;
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fmmclass.insertmiscdata(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "Misc Item Inserted.";
            faclass.insertactivity(li);
            txtname.Text = string.Empty;
            fillgrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void btnsaveitem_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            li.type = drpitemname.SelectedItem.Text;
            li.name = txtsize.Text;
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fmmclass.insertmiscdata(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "Misc1 Item Inserted.";
            faclass.insertactivity(li);
            txtsize.Text = string.Empty;
            fillgrid1();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    public void fillgrid1()
    {
        li.type = drpitemname.SelectedItem.Text;
        DataTable dtdata = new DataTable();
        dtdata = fmmclass.selectmiscdatafromtype(li);
        if (dtdata.Rows.Count > 0)
        {
            lblemptyitem.Visible = false;
            gvitem.Visible = true;
            gvitem.DataSource = dtdata;
            gvitem.DataBind();
        }
        else
        {
            gvitem.Visible = false;
            lblemptyitem.Visible = true;
            lblemptyitem.Text = "No Misc Data Found for Item.";
        }
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillgrid1();
    }
    protected void gvitem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.id = Convert.ToInt64(e.CommandArgument);
                fmmclass.deletemiscdata(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "Misc1 Item Deleted.";
                faclass.insertactivity(li);
                fillgrid1();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvitem_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}