﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlTypes;

public partial class RepBalanceSheet : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();
    public ReportDocument rptdoc;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnpreview_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (drptype.SelectedItem.Text == "Trading Account")
        //{
        //    //string Xrepname = "Sales Outstanding Report";
        //    string Xrepname = "Overall Report";
        //    li.fromdate = txtfromdate.Text;
        //    li.todate = txttodate.Text;
        //    //Int64 vno = Convert.ToInt64(txtvono.Text);
        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //}
        //else if (drptype.SelectedItem.Text == "Profit Loss Account")
        //{
        string Xrepname = "Balance Sheet Report";
        string type = drptype.SelectedItem.Text;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        string xreptype = "";
        if (rdoformat.SelectedItem.Text == "PDF")
        {
            xreptype = "PDF";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?type=" + type + "&xreptype=" + xreptype + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        else
        {
            rptdoc = new ReportDocument();
            Session["dtpitems"] = CreateTemplate1();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.fromdate = txtfromdate.Text;
            li.todate = txttodate.Text;
            li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            string ftow = "";
            xreptype = "Excel";
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            string rptname;
            rptname = Server.MapPath(@"~/Reports/BalanceSheet1.rpt");
            SqlDataAdapter dada = new SqlDataAdapter("select debitcode from Ledger where cno=" + li.cno + " and voucherdate between @fromdate and @todate group by debitcode order by debitcode", con);
            dada.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dada.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtac = new DataTable();
            dada.Fill(dtac);
            DataTable dtq = new DataTable();
            rptdoc.Load(rptname);
            for (int cc = 0; cc < dtac.Rows.Count; cc++)
            {
                li.acname = dtac.Rows[cc]["debitcode"].ToString().Trim();
                SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop = new DataTable();
                dc.Fill(dtop);

                SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop1 = new DataTable();
                dc1.Fill(dtop1);


                SqlDataAdapter dc2 = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                dc2.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                DataTable dtop2 = new DataTable();
                dc2.Fill(dtop2);

                SqlDataAdapter dc12 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                dc12.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                DataTable dtop12 = new DataTable();
                dc12.Fill(dtop12);

                SqlDataAdapter dc11 = new SqlDataAdapter("select * from ledger where cno=" + li.cno + " and debitcode=@acname and istype='OP'", con);
                dc11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                DataTable dtop11 = new DataTable();
                dc11.Fill(dtop11);

                SqlDataAdapter dc112 = new SqlDataAdapter("select * from ACMaster where cno=" + li.cno + " and acname=@acname", con);
                dc112.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                DataTable dtac1 = new DataTable();
                dc112.Fill(dtac1);
                string poscrdr = "";
                string negcrdr = "";
                string posgcode = "";
                string neggcode = "";
                string groupheadcode = "";
                string groupheadcode1 = "";
                if (dtac1.Rows.Count > 0)
                {
                    poscrdr = dtac1.Rows[0]["debitgroupcode"].ToString();
                    negcrdr = dtac1.Rows[0]["creditgroupcode"].ToString();
                }
                double opbal = 0;
                if (dtop11.Rows.Count > 0)
                {
                    if (dtop11.Rows[0]["amount"].ToString() != "" && dtop11.Rows[0]["amount"].ToString() != string.Empty)
                    {
                        opbal = Convert.ToDouble(dtop11.Rows[0]["amount"].ToString()) + Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                    }
                }
                else
                {
                    opbal = Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                }
                double zero = 0;
                if (opbal >= 0)
                {
                    dtq = (DataTable)Session["dtpitems"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    DataRow dr = dtq.NewRow();
                    dr["acname"] = li.acname;
                    dr["opening"] = opbal;
                    if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                    }
                    if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    }
                    dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    if (closingbalance >= 0)
                    {
                        if (closingbalance == 0)
                        {
                            dr["crdr"] = "";
                        }
                        else
                        {
                            dr["crdr"] = poscrdr;
                        }
                    }
                    else
                    {
                        dr["crdr"] = negcrdr;
                    }
                    if (closingbalance >= 0)
                    {
                        if (closingbalance == 0)
                        {
                            dr["crdr1"] = "";
                        }
                        else
                        {
                            dr["crdr1"] = "Dr";
                        }
                    }
                    else
                    {
                        dr["crdr1"] = "Cr";
                    }
                    dtq.Rows.Add(dr);
                    Session["dtpitems"] = dtq;
                }
                else
                {
                    dtq = (DataTable)Session["dtpitems"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    DataRow dr = dtq.NewRow();
                    dr["acname"] = li.acname;
                    dr["opening"] = opbal;
                    if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                    }
                    if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    }
                    dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                    if (closingbalance >= 0)
                    {
                        if (closingbalance == 0)
                        {
                            dr["crdr"] = "";
                        }
                        else
                        {
                            dr["crdr"] = poscrdr;
                        }
                    }
                    else
                    {
                        dr["closingbal"] = closingbalance * (-1);
                        dr["crdr"] = negcrdr;
                    }
                    if (closingbalance >= 0)
                    {
                        if (closingbalance == 0)
                        {
                            dr["crdr1"] = "";
                        }
                        else
                        {
                            dr["crdr1"] = "Dr";
                        }
                    }
                    else
                    {
                        dr["crdr1"] = "Cr";
                    }
                    dtq.Rows.Add(dr);
                    Session["dtpitems"] = dtq;
                }
            }
            SqlCommand cmd1 = new SqlCommand("delete from tempsheet", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            for (int f = 0; f < dtq.Rows.Count; f++)
            {
                li.acname = dtq.Rows[f]["acname"].ToString();
                li.closingbal = Convert.ToDouble(dtq.Rows[f]["closingbal"].ToString());
                if (li.closingbal < 0)
                {
                    li.closingbal = li.closingbal * (-1);
                }
                li.crdr = dtq.Rows[f]["crdr1"].ToString();
                li.groupcode = dtq.Rows[f]["crdr"].ToString();
                SqlCommand cmd = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values ('" + li.acname + "','" + li.groupcode + "','" + Math.Round(li.closingbal, 2) + "','" + li.crdr + "')", con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                //}
            }
            string xtitle = type + " As on " + li.todate;
            rptdoc.DataDefinition.FormulaFields["xtitle"].Text = "'" + xtitle + "'";
            if (type == "Trading Account")
            {
                DataTable dd1 = new DataTable();
                DataTable dd2 = new DataTable();
                SqlDataAdapter dabs1a = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where tempsheet.code between 100 and 199", con);
                dabs1a.Fill(dd1);
                SqlDataAdapter dabs2a = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where tempsheet.code between 200 and 299", con);
                dabs2a.Fill(dd2);
                li.amount = Math.Round((Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd1.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round((cx - Convert.ToDouble(dd1.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "GROSS LOSS";
                    li.code = "299";
                    li.type = "Cr";

                }
                else
                {
                    li.acname = "TRANSFER TO P & L A/C";
                    li.code = "199";
                    li.type = "Dr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda.Parameters.AddWithValue("@acname", li.acname);
                cmda.Parameters.AddWithValue("@code", li.code);
                cmda.Parameters.AddWithValue("@amount", li.amount);
                cmda.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda.ExecuteNonQuery();
                con.Close();
                SqlDataAdapter dabs1 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 100 and 199 order by tempsheet.code,tempsheet.acname", con);
                dabs1.Fill(imageDataSet.Tables["DataTable18"]);
                SqlDataAdapter dabs2 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 200 and 299 order by tempsheet.code,tempsheet.acname", con);
                dabs2.Fill(imageDataSet.Tables["DataTable181"]);
                double totd = 0;
                double totc = 0;
                for (int x = 0; x < imageDataSet.Tables["DataTable18"].Rows.Count; x++)
                {
                    totd = totd + Convert.ToDouble(imageDataSet.Tables["DataTable18"].Rows[x]["clbal"].ToString());
                }
                for (int x = 0; x < imageDataSet.Tables["DataTable181"].Rows.Count; x++)
                {
                    totc = totc + Convert.ToDouble(imageDataSet.Tables["DataTable181"].Rows[x]["clbal"].ToString());
                }
                rptdoc.DataDefinition.FormulaFields["titled"].Text = "'E x p e n s e s'";
                rptdoc.DataDefinition.FormulaFields["titlec"].Text = "'I n c o m e'";
                rptdoc.DataDefinition.FormulaFields["totd"].Text = "'" + totd.ToString("0.00") + "'";
                rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + totc.ToString("0.00") + "'";
                if (totc < 0)
                {
                    rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + (totc * (-1)).ToString("0.00") + "'";
                }

            }
            else if (type == "Profit Loss Account")
            {

                DataTable dd1 = new DataTable();
                DataTable dd2 = new DataTable();
                SqlDataAdapter dabs1a = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 100 and 199", con);
                dabs1a.Fill(dd1);
                SqlDataAdapter dabs2a = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 200 and 299", con);
                dabs2a.Fill(dd2);
                li.amount = Math.Round((Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd1.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd2.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd1.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "GROSS LOSS";
                    li.code = "301";
                    li.type = "Cr";
                }
                else
                {
                    li.acname = "B/F TO TRADING A/C";
                    li.code = "401";
                    li.type = "Dr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda.Parameters.AddWithValue("@acname", li.acname);
                cmda.Parameters.AddWithValue("@code", li.code);
                cmda.Parameters.AddWithValue("@amount", li.amount);
                cmda.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda.ExecuteNonQuery();
                con.Close();



                DataTable dd11 = new DataTable();
                DataTable dd21 = new DataTable();
                SqlDataAdapter dabs1a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 300 and 399", con);
                dabs1a1.Fill(dd11);
                SqlDataAdapter dabs2a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 400 and 499", con);
                dabs2a1.Fill(dd21);
                li.amount = Math.Round((Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "NET LOSS";
                    li.code = "499";
                    li.type = "Dr";
                }
                else
                {
                    li.acname = "NET PROFIT-TRF TO CAPITAL A/C";
                    li.code = "399";
                    li.type = "Cr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda1 = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda1.Parameters.AddWithValue("@acname", li.acname);
                cmda1.Parameters.AddWithValue("@code", li.code);
                cmda1.Parameters.AddWithValue("@amount", li.amount);
                cmda1.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda1.ExecuteNonQuery();
                con.Close();


                SqlDataAdapter dabs1 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 300 and 399 order by tempsheet.code,tempsheet.acname", con);
                dabs1.Fill(imageDataSet.Tables["DataTable18"]);
                SqlDataAdapter dabs2 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 400 and 499 order by tempsheet.code,tempsheet.acname", con);
                dabs2.Fill(imageDataSet.Tables["DataTable181"]);
                double totd = 0;
                double totc = 0;
                for (int x = 0; x < imageDataSet.Tables["DataTable18"].Rows.Count; x++)
                {
                    totd = totd + Convert.ToDouble(imageDataSet.Tables["DataTable18"].Rows[x]["clbal"].ToString());
                }
                for (int x = 0; x < imageDataSet.Tables["DataTable181"].Rows.Count; x++)
                {
                    totc = totc + Convert.ToDouble(imageDataSet.Tables["DataTable181"].Rows[x]["clbal"].ToString());
                }
                rptdoc.DataDefinition.FormulaFields["titled"].Text = "'E x p e n s e s'";
                rptdoc.DataDefinition.FormulaFields["titlec"].Text = "'I n c o m e'";
                rptdoc.DataDefinition.FormulaFields["totd"].Text = "'" + totd.ToString("0.00") + "'";
                rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + totc.ToString("0.00") + "'";
                if (totc < 0)
                {
                    rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + (totc * (-1)).ToString("0.00") + "'";
                }
            }
            else
            {
                DataTable dd1p = new DataTable();
                DataTable dd2p = new DataTable();
                SqlDataAdapter dabs1ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 100 and 199", con);
                dabs1ap.Fill(dd1p);
                SqlDataAdapter dabs2ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 200 and 299", con);
                dabs2ap.Fill(dd2p);
                li.amount = Math.Round((Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "GROSS LOSS";
                    li.code = "301";
                    li.type = "Cr";

                }
                else
                {
                    li.acname = "B/F TO TRADING A/C";
                    li.code = "401";
                    li.type = "Dr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmdap = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmdap.Parameters.AddWithValue("@acname", li.acname);
                cmdap.Parameters.AddWithValue("@code", li.code);
                cmdap.Parameters.AddWithValue("@amount", li.amount);
                cmdap.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmdap.ExecuteNonQuery();
                con.Close();



                DataTable dd11 = new DataTable();
                DataTable dd21 = new DataTable();
                SqlDataAdapter dabs1a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 300 and 399", con);
                dabs1a1.Fill(dd11);
                SqlDataAdapter dabs2a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 400 and 499", con);
                dabs2a1.Fill(dd21);
                li.amount = Math.Round((Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "NET LOSS";
                    li.code = "501";
                    li.type = "Dr";
                }
                else
                {
                    li.acname = "NET PROFIT";
                    li.code = "601";
                    li.type = "Cr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda1 = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda1.Parameters.AddWithValue("@acname", li.acname);
                cmda1.Parameters.AddWithValue("@code", li.code);
                cmda1.Parameters.AddWithValue("@amount", li.amount);
                cmda1.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda1.ExecuteNonQuery();
                con.Close();

                SqlDataAdapter dabs1 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 600 and 699 order by tempsheet.code,tempsheet.acname", con);
                dabs1.Fill(imageDataSet.Tables["DataTable18"]);
                SqlDataAdapter dabs2 = new SqlDataAdapter("select tempsheet.*,ACGroupMaster1.groupcode,(ACGroupMaster1.groupname) as head,ACGroupMaster.groupname from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 500 and 599 order by tempsheet.code,tempsheet.acname", con);
                dabs2.Fill(imageDataSet.Tables["DataTable181"]);
                double totd = 0;
                double totc = 0;
                for (int x = 0; x < imageDataSet.Tables["DataTable18"].Rows.Count; x++)
                {
                    totd = totd + Convert.ToDouble(imageDataSet.Tables["DataTable18"].Rows[x]["clbal"].ToString());
                }
                for (int x = 0; x < imageDataSet.Tables["DataTable181"].Rows.Count; x++)
                {
                    totc = totc + Convert.ToDouble(imageDataSet.Tables["DataTable181"].Rows[x]["clbal"].ToString());
                }
                rptdoc.DataDefinition.FormulaFields["titled"].Text = "'L i a b i l i t i e s'";
                rptdoc.DataDefinition.FormulaFields["titlec"].Text = "'A s s e t s'";
                rptdoc.DataDefinition.FormulaFields["totd"].Text = "'" + totd.ToString("0.00") + "'";
                rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + totc.ToString("0.00") + "'";
                if (totc < 0)
                {
                    rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + (totc * (-1)).ToString("0.00") + "'";
                }
            }
            rptdoc.Subreports[0].SetDataSource(imageDataSet.Tables["DataTable18"]);
            rptdoc.Subreports[1].SetDataSource(imageDataSet.Tables["DataTable181"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "BalanceSheetReport.pdf";
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            string destdir = Server.MapPath("~\\Balance Sheet");
            if (!System.IO.Directory.Exists(destdir))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~\\Balance Sheet"));
            }
            destiopt.DiskFileName = Server.MapPath(@"~/Balance Sheet/" + fname);
            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;
            if (type == "Trading Account")
            {
                fname = "Trading_" + txtfromdate.Text + "_" + txttodate.Text + ".xls";
            }
            else if (type == "Profit Loss Account")
            {
                fname = "PL_" + txtfromdate.Text + "_" + txttodate.Text + ".xls";
            }
            else
            {
                fname = "BS_" + txtfromdate.Text + "_" + txttodate.Text + ".xls";
            }
            //create instance for destination option - This one is used to set path of your pdf file save 
            destiopt.DiskFileName = Server.MapPath(@"~/Balance Sheet/" + fname);
            exprtopt.ExportFormatType = ExportFormatType.Excel;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            System.Diagnostics.Process.Start("explorer.exe", Server.MapPath("~/Balance Sheet"));
        }        
        //}
    }

    public DataTable CreateTemplate1()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("opening", typeof(double));
        dtpitems.Columns.Add("debitamt", typeof(double));
        dtpitems.Columns.Add("creditamt", typeof(double));
        dtpitems.Columns.Add("closingbal", typeof(double));
        dtpitems.Columns.Add("crdr", typeof(string));
        dtpitems.Columns.Add("crdr1", typeof(string));
        return dtpitems;
    }

}