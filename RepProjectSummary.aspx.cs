﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RepProjectSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnpreview_Click(object sender, EventArgs e)
    {
        //string Xrepname = "Purchase Outstanding Report";
        string exporttype = "";
        string Xrepname = "Project Summary Report";
        if (rdoformat.SelectedItem.Text == "PDF")
        {
            exporttype = "pdf";
        }
        else
        {
            exporttype = "excel";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?exporttype=" + exporttype + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }

}