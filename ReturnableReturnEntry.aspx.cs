﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;

public partial class ReturnableReturnEntry : System.Web.UI.Page
{
    ForReturnableReturn frrclass = new ForReturnableReturn();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "update")
            {
                filleditdata();
            }
            else
            {
                getbrno();
                txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemsrre"] = CreateTemplate();
            }

            Page.SetFocus(txtname);
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        dtvno = frrclass.selectunusedrrvno(li);
        if (dtvno.Rows.Count > 0)
        {
            txtchallanno.Text = dtvno.Rows[0]["vno"].ToString();
        }
        else
        {
            txtchallanno.Text = "";
        }
    }

    public void filleditdata()
    {
        li.challanno = Convert.ToInt64(Request["cno"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = frrclass.selectrrmasterdatafromchallanno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtchallanno.Text = dtdata.Rows[0]["challanno"].ToString();
            txtdate.Text = Convert.ToDateTime(dtdata.Rows[0]["cdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtorderno.Text = dtdata.Rows[0]["sono"].ToString();
            txtorderdate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
           
            txtdespatchedby.Text = dtdata.Rows[0]["dispatchedby"].ToString();
            txtfreightrs.Text = dtdata.Rows[0]["freight"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtitems = new DataTable();
            dtitems = frrclass.selectallrritemsdata(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitems;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
            btnsaveall.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallitemnamenotblocked(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsono(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallsonoforrr(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getacname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallacname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void rptlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemsrre"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemsrre"] = dt;
            this.bindgrid();
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
            li.id = Convert.ToInt64(lblid.Text);
            frrclass.deleterritemsdatafromid(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.id + " Returnable Return Item Deleted.";
            faclass.insertactivity(li);
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtitem = new DataTable();
            dtitem = frrclass.selectallrritemsdata(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemsrre"] as DataTable;
        GridView1.DataSource = dtsession;
        GridView1.DataBind();
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("rfs", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("descr", typeof(string));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("retremarks", typeof(string));
        return dtpitems;
    }

    public void countadd()
    {
        double qty = 0;
        double rate = 0;
        if (txtqty.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
    }

    public void countgv()
    {
        for (int c = 0; c < rptlist.Rows.Count; c++)
        {
            double qty = 0;
            double rate = 0;
            TextBox txtqtygv = (TextBox)rptlist.Rows[c].FindControl("txtgridqty");
            TextBox txtrategv = (TextBox)rptlist.Rows[c].FindControl("txtgridrate");
            Label lblamountgv = (Label)rptlist.Rows[c].FindControl("lblamount");
            if (txtqtygv.Text.Trim() != string.Empty)
            {
                qty = Convert.ToDouble(txtqtygv.Text);
            }
            if (txtrategv.Text.Trim() != string.Empty)
            {
                rate = Convert.ToDouble(txtrategv.Text);
            }
            lblamountgv.Text = Math.Round((qty * rate), 2).ToString();
        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemsrre"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["itemname"] = txtitemname.Text;
            dr["qty"] = txtqty.Text;
            dr["rate"] = txtrate.Text;
            dr["amount"] = txtamount.Text;
            dr["unit"] = txtunit.Text;
            dr["descr"] = txtdesc.Text;
            dr["descr1"] = txtdesc1.Text;
            dr["descr2"] = txtdesc2.Text;
            dt.Rows.Add(dr);
            Session["dtpitemsrre"] = dt;
            this.bindgrid();
        }
        else
        {
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            li.cdate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = txtitemname.Text;
            li.qty = Convert.ToDouble(txtqty.Text);
            li.rate = Convert.ToDouble(txtqty.Text);
            li.amount = Convert.ToDouble(txtqty.Text);
            li.unit = txtunit.Text;
            li.descr = txtdesc.Text;
            li.descr1 = txtdesc1.Text;
            li.descr2 = txtdesc2.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            frrclass.insertrritemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.challanno + " Returnable Return Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtitem = new DataTable();
            dtitem = frrclass.selectallrritemsdata(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
        txtitemname.Text = string.Empty;
        txtqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtamount.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtdesc.Text = string.Empty;
        txtdesc1.Text = string.Empty;
        txtdesc2.Text = string.Empty;
        Page.SetFocus(txtitemname);
    }
    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        SqlDateTime sqldatenull = SqlDateTime.Null;
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        li.challanno = Convert.ToInt64(txtchallanno.Text);
        li.cdate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.cdate <= yyyear1 && li.cdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.name = txtname.Text;
        if (txtccode.Text.Trim() != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtccode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        //if (txtorderno.Text.Trim() != string.Empty)
        //{
        li.strsono = txtorderno.Text;
        //}
        //else
        //{
        //    li.sono = 0;
        //}
        li.sodate = Convert.ToDateTime(txtorderdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.lrno = txtlrno.Text;
        if (txtlrdate.Text.Trim() != string.Empty)
        {
            li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.lrdate = sqldatenull;
        }
        li.dispatchedby = txtdespatchedby.Text;
        if (txtfreightrs.Text.Trim() != string.Empty)
        {
            li.freight = Convert.ToDouble(txtfreightrs.Text);
        }
        else
        {
            li.freight = 0;
        }
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsaveall.Text == "Save")
        {
            if (rptlist.Rows.Count > 0)
            {
                DataTable dtcheck = new DataTable();
                dtcheck = frrclass.selectrrmasterdatafromchallanno(li);
                if (dtcheck.Rows.Count == 0)
                {
                    frrclass.insertrrmasterdata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.challanno + " Returnable Return Inserted.";
                    faclass.insertactivity(li);
                }
                else
                {
                    li.challanno = Convert.ToInt64(txtchallanno.Text);
                    frrclass.updateisused(li);
                    getbrno();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Challan No. already exist.Try Again.');", true);
                    return;
                }
                li.challanno = Convert.ToInt64(txtchallanno.Text);
                for (int c = 0; c < rptlist.Rows.Count; c++)
                {
                    Label lblid = (Label)rptlist.Rows[c].FindControl("lblid");
                    Label lblitemname = (Label)rptlist.Rows[c].FindControl("lblitemname");
                    Label lblqty = (Label)rptlist.Rows[c].FindControl("lblqty");
                    Label lblrate = (Label)rptlist.Rows[c].FindControl("lblrate");
                    Label lblamount = (Label)rptlist.Rows[c].FindControl("lblamount");
                    Label lblunit = (Label)rptlist.Rows[c].FindControl("lblunit");
                    Label lbldescr = (Label)rptlist.Rows[c].FindControl("lbldescr");
                    Label lbldescr1 = (Label)rptlist.Rows[c].FindControl("lbldescr1");
                    Label lbldescr2 = (Label)rptlist.Rows[c].FindControl("lbldescr2");
                    TextBox txtretremarks = (TextBox)rptlist.Rows[c].FindControl("txtretremarks");
                    li.vid = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.unit = lblunit.Text;
                    li.descr = lbldescr.Text;
                    li.descr1 = lbldescr1.Text;
                    li.descr2 = lbldescr2.Text;
                    li.retremarks = txtretremarks.Text;
                    frrclass.insertrritemsdata(li);
                }
                frrclass.updateisused(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Return Data.');", true);
                return;
            }
        }
        else
        {
            li.challanno = Convert.ToInt64(Request["cno"].ToString());
            frrclass.updaterrmasterdata(li);
            frrclass.updaterritemsdatedata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.challanno + " Returnable Return Updated.";
            faclass.insertactivity(li);

            for (int c = 0; c < rptlist.Rows.Count; c++)
            {
                Label lblid = (Label)rptlist.Rows[c].FindControl("lblid");
                Label lblitemname = (Label)rptlist.Rows[c].FindControl("lblitemname");
                TextBox lblqty = (TextBox)rptlist.Rows[c].FindControl("txtgridqty");
                TextBox lblrate = (TextBox)rptlist.Rows[c].FindControl("txtgridrate");
                Label lblamount = (Label)rptlist.Rows[c].FindControl("lblamount");
                Label lblunit = (Label)rptlist.Rows[c].FindControl("lblunit");
                Label lbldescr = (Label)rptlist.Rows[c].FindControl("lbldescr");
                Label lbldescr1 = (Label)rptlist.Rows[c].FindControl("lbldescr1");
                Label lbldescr2 = (Label)rptlist.Rows[c].FindControl("lbldescr2");
                TextBox txtretremarks = (TextBox)rptlist.Rows[c].FindControl("txtretremarks");
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                li.qty = Convert.ToDouble(lblqty.Text);
                li.rate = Convert.ToDouble(lblrate.Text);
                li.amount = Convert.ToDouble(lblamount.Text);
                li.unit = lblunit.Text;
                li.descr = lbldescr.Text;
                li.descr1 = lbldescr1.Text;
                li.descr2 = lbldescr2.Text;
                li.retremarks = txtretremarks.Text;
                frrclass.updaterritemsdata(li);
            }

        }
        Response.Redirect("~/ReturnableReturnEntryList.aspx?pagename=ReturnableReturnEntryList");
    }
    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        li.itemname = txtitemname.Text;
        DataTable dtdata = new DataTable();
        dtdata = frrclass.selectalldatafromitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            txtqty.Text = "1";
            txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
            txtamount.Text = dtdata.Rows[0]["salesrate"].ToString();
            txtunit.Text = dtdata.Rows[0]["unit"].ToString();
        }
        else
        {
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtamount.Text = string.Empty;
            txtunit.Text = string.Empty;
        }
        Page.SetFocus(txtqty);
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtccode.Text.Trim() != string.Empty)
        {
            string cc = txtccode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtccode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtccode.Text = string.Empty;
        }
        Page.SetFocus(txtorderno);
    }

    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        countadd();
        Page.SetFocus(txtrate);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        countadd();
        Page.SetFocus(txtamount);
    }

    protected void txtgridqty_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double qty = 0;
        double rate = 0;
        TextBox txtqtygv = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtrategv = (TextBox)currentRow.FindControl("txtgridrate");
        Label lblamountgv = (Label)currentRow.FindControl("lblamount");
        if (txtqtygv.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqtygv.Text);
        }
        if (txtrategv.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrategv.Text);
        }
        lblamountgv.Text = Math.Round((qty * rate), 2).ToString();
        //countgv();
    }

    protected void txtgridrate_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double qty = 0;
        double rate = 0;
        TextBox txtqtygv = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtrategv = (TextBox)currentRow.FindControl("txtgridrate");
        Label lblamountgv = (Label)currentRow.FindControl("lblamount");
        if (txtqtygv.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqtygv.Text);
        }
        if (txtrategv.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrategv.Text);
        }
        lblamountgv.Text = Math.Round((qty * rate), 2).ToString();
        //countgv();
    }

    protected void txtorderno_TextChanged(object sender, EventArgs e)
    {
        if (txtorderno.Text.Trim() != string.Empty)
        {
            //li.
        }
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim() != string.Empty)
        {
            lblpopupacname.Text = txtname.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = txtname.Text;
            ////DataTable dtso = new DataTable();
            ////dtso = fsiclass.selectallsalesorderforpopup(li);
            ////if (dtso.Rows.Count > 0)
            ////{
            ////    lblemptyso.Visible = false;
            ////    gvsalesorders.Visible = true;
            ////    gvsalesorders.DataSource = dtso;
            ////    gvsalesorders.DataBind();
            ////}
            ////else
            ////{
            ////    gvsalesorders.Visible = false;
            ////    lblemptyso.Visible = true;
            ////    lblemptyso.Text = "No Sales Order Found.";
            ////}

            DataTable dtsc = new DataTable();
            dtsc = frrclass.selectallsaleschallanforpopup(li);
            if (dtsc.Rows.Count > 0)
            {
                lblemptysc.Visible = false;
                gvsaleschallans.Visible = true;
                gvsaleschallans.DataSource = dtsc;
                gvsaleschallans.DataBind();
                GridView1.Visible = false;
            }
            else
            {
                gvsaleschallans.Visible = false;
                lblemptysc.Visible = true;
                lblemptysc.Text = "No Sales Challan Found.";
                GridView1.Visible = false;
            }
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsaleschallans_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemsrre"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = e.CommandArgument.ToString();
            txtorderno.Text = li.scno.ToString();
            DataTable dtdata = new DataTable();
            dtdata = frrclass.selectdatafromscnoforrr(li);
            //if (dtdata.Rows.Count > 0)
            //{
            //    txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            //    lblempty.Visible = false;
            //    rptlist.Visible = true;
            //    rptlist.DataSource = dtdata;
            //    rptlist.DataBind();
            //}
            //else
            //{
            //    rptlist.Visible = false;
            //    lblempty.Visible = true;
            //    lblempty.Text = "No Sales Challan Data Found.";
            //}
            txtchallanno1.Text = li.strscno;
            txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            txtorderdate.Text = (Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
            DataTable dt = (DataTable)Session["dtpitemsrre"];
            for (int cc = 0; cc < dtdata.Rows.Count; cc++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[cc]["id"].ToString();
                dr["itemname"] = dtdata.Rows[cc]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[cc]["qty"].ToString();
                dr["rate"] = dtdata.Rows[cc]["rate"].ToString();
                dr["amount"] = dtdata.Rows[cc]["amount"].ToString();
                dr["unit"] = dtdata.Rows[cc]["unit"].ToString();
                dr["descr"] = dtdata.Rows[cc]["descr1"].ToString();
                dr["descr1"] = dtdata.Rows[cc]["descr2"].ToString();
                dr["descr2"] = dtdata.Rows[cc]["descr3"].ToString();
                dr["rfs"] = "0";
                dr["retremarks"] = "";
                dt.Rows.Add(dr);
                Session["dtpitemsrre"] = dt;
            }
            //this.bindgrid();
            if (dt.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsaleschallans.Visible = false;
                GridView1.Visible = true;
                GridView1.DataSource = dt;
                GridView1.DataBind();
                chkall.Visible = true;
            }
            else
            {
                gvsaleschallans.Visible = false;
                lblempty.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
                lblempty.Text = "No Items Found.";
                chkall.Visible = false;
            }
            ModalPopupExtender2.Show();
        }
    }

    protected void rptlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblitemname = (Label)currentRow.FindControl("lblitemname");
            TextBox lblqty = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
            Label lblamount = (Label)currentRow.FindControl("lblamount");
            Label lblunit = (Label)currentRow.FindControl("lblunit");
            Label lbldescr = (Label)currentRow.FindControl("lbldescr");
            Label lbldescr1 = (Label)currentRow.FindControl("lbldescr1");
            Label lbldescr2 = (Label)currentRow.FindControl("lbldescr2");
            TextBox txtretremarks = (TextBox)currentRow.FindControl("txtretremarks");
            li.itemname = lblitemname.Text;
            li.qty = Convert.ToDouble(lblqty.Text);
            li.rate = Convert.ToDouble(lblrate.Text);
            li.amount = Convert.ToDouble(lblamount.Text);
            li.unit = lblunit.Text;
            li.descr = lbldescr.Text;
            li.descr1 = lbldescr1.Text;
            li.descr2 = lbldescr2.Text;
            li.retremarks = txtretremarks.Text;
            frrclass.updaterritemsdata(li);
            DataTable dtitems = new DataTable();
            dtitems = frrclass.selectallrritemsdata(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitems;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblitemname = (Label)currentRow.FindControl("lblitemname");
            TextBox lblqty = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
            Label lblamount = (Label)currentRow.FindControl("lblamount");
            Label lblunit = (Label)currentRow.FindControl("lblunit");
            Label lbldescr = (Label)currentRow.FindControl("lbldescr");
            Label lbldescr1 = (Label)currentRow.FindControl("lbldescr1");
            Label lbldescr2 = (Label)currentRow.FindControl("lbldescr2");
            li.itemname = lblitemname.Text;
            li.qty = Convert.ToDouble(lblqty.Text);
            li.rate = Convert.ToDouble(lblrate.Text);
            li.amount = Convert.ToDouble(lblamount.Text);
            li.unit = lblunit.Text;
            li.descr = lbldescr.Text;
            li.descr1 = lbldescr1.Text;
            li.descr2 = lbldescr2.Text;
            frrclass.updaterritemsdata(li);
            DataTable dtitems = new DataTable();
            dtitems = frrclass.selectallrritemsdata(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitems;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemsrre"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemsrre"] = dt;
            this.bindgrid();
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Label lblid = (Label)GridView1.Rows[e.RowIndex].FindControl("lblid");
            li.id = Convert.ToInt64(lblid.Text);
            frrclass.deleterritemsdatafromid(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.id + " Returnable Return Item Deleted.";
            faclass.insertactivity(li);
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtitem = new DataTable();
            dtitem = frrclass.selectallrritemsdata(li);
            if (dtitem.Rows.Count > 0)
            {
                Label1.Visible = false;
                GridView1.Visible = true;
                GridView1.DataSource = dtitem;
                GridView1.DataBind();
            }
            else
            {
                GridView1.Visible = false;
                Label1.Visible = true;
                Label1.Text = "No Item Data Found.";
            }
        }
    }

    protected void btnselectitem_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            string id = "0";
            if (chkall.Checked == false)
            {
                for (int cc = 0; cc < GridView1.Rows.Count; cc++)
                {
                    Label lblid = (Label)GridView1.Rows[cc].FindControl("lblid");
                    CheckBox chkselect = (CheckBox)GridView1.Rows[cc].FindControl("chkselect");
                    if (chkselect.Checked == true)
                    {
                        id = id + "," + lblid.Text;
                    }
                }
            }
            else
            {
                for (int cc = 0; cc < GridView1.Rows.Count; cc++)
                {
                    Label lblid = (Label)GridView1.Rows[cc].FindControl("lblid");
                    id = id + "," + lblid.Text;
                }
            }
            li.remarks = id;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = txtchallanno1.Text;
            txtorderno.Text = li.scno.ToString();
            DataTable dtdata = new DataTable();
            dtdata = frrclass.selectdatafromscnoforrr12(li);
            //if (dtdata.Rows.Count > 0)
            //{
            //    txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            //    lblempty.Visible = false;
            //    rptlist.Visible = true;
            //    rptlist.DataSource = dtdata;
            //    rptlist.DataBind();
            //}
            //else
            //{
            //    rptlist.Visible = false;
            //    lblempty.Visible = true;
            //    lblempty.Text = "No Sales Challan Data Found.";
            //}
            txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            txtorderdate.Text = (Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
            Session["dtpitemsrre"] = CreateTemplate();
            DataTable dt = (DataTable)Session["dtpitemsrre"];
            for (int cc = 0; cc < dtdata.Rows.Count; cc++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[cc]["id"].ToString();
                dr["itemname"] = dtdata.Rows[cc]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[cc]["qty"].ToString();
                dr["rate"] = dtdata.Rows[cc]["rate"].ToString();
                dr["amount"] = dtdata.Rows[cc]["amount"].ToString();
                dr["unit"] = dtdata.Rows[cc]["unit"].ToString();
                dr["descr"] = dtdata.Rows[cc]["descr1"].ToString();
                dr["descr1"] = dtdata.Rows[cc]["descr2"].ToString();
                dr["descr2"] = dtdata.Rows[cc]["descr3"].ToString();
                dr["rfs"] = "0";
                dr["retremarks"] = "";
                dt.Rows.Add(dr);
                Session["dtpitemsrre"] = dt;
            }
            //this.bindgrid();
            if (dt.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.DataSource = dt;
                rptlist.DataBind();
            }
            else
            {
                lblempty.Visible = true;
                rptlist.DataSource = null;
                rptlist.DataBind();
                lblempty.Text = "No Items Found.";
            }
        }
        else
        {
            string id = "0";
            if (chkall.Checked == false)
            {
                for (int cc = 0; cc < GridView1.Rows.Count; cc++)
                {
                    Label lblid = (Label)GridView1.Rows[cc].FindControl("lblid");
                    CheckBox chkselect = (CheckBox)GridView1.Rows[cc].FindControl("chkselect");
                    if (chkselect.Checked == true)
                    {
                        id = id + "," + lblid.Text;
                    }
                }
            }
            else
            {
                for (int cc = 0; cc < GridView1.Rows.Count; cc++)
                {
                    Label lblid = (Label)GridView1.Rows[cc].FindControl("lblid");
                    id = id + "," + lblid.Text;
                }
            }
            li.remarks = id;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = txtchallanno1.Text;
            txtorderno.Text = li.scno.ToString();
            DataTable dtdata = new DataTable();
            dtdata = frrclass.selectdatafromscnoforrr12(li);
            //if (dtdata.Rows.Count > 0)
            //{
            //    txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            //    lblempty.Visible = false;
            //    rptlist.Visible = true;
            //    rptlist.DataSource = dtdata;
            //    rptlist.DataBind();
            //}
            //else
            //{
            //    rptlist.Visible = false;
            //    lblempty.Visible = true;
            //    lblempty.Text = "No Sales Challan Data Found.";
            //}
            txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            txtorderdate.Text = (Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
            //Session["dtpitemsrre"] = CreateTemplate();
            //DataTable dt = (DataTable)Session["dtpitemsrre"];
            DataTable dt = new DataTable();
            for (int cc = 0; cc < dtdata.Rows.Count; cc++)
            {
                li.challanno = Convert.ToInt64(txtchallanno.Text);
                li.cdate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.itemname = dtdata.Rows[cc]["itemname"].ToString();
                li.qty = Convert.ToDouble(dtdata.Rows[cc]["qty"].ToString());
                li.rate = Convert.ToDouble(dtdata.Rows[cc]["rate"].ToString());
                li.amount = Convert.ToDouble(dtdata.Rows[cc]["amount"].ToString());
                li.unit = dtdata.Rows[cc]["unit"].ToString();
                li.descr = dtdata.Rows[cc]["descr1"].ToString();
                li.descr1 = dtdata.Rows[cc]["descr2"].ToString();
                li.descr2 = dtdata.Rows[cc]["descr3"].ToString();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.vid = Convert.ToInt64(dtdata.Rows[cc]["id"].ToString());
                li.retremarks = "";
                frrclass.insertrritemsdata(li);
                //DataRow dr = dt.NewRow();
                //dr["id"] = dtdata.Rows[cc]["id"].ToString();
                //dr["itemname"] = dtdata.Rows[cc]["itemname"].ToString();
                //dr["qty"] = dtdata.Rows[cc]["qty"].ToString();
                //dr["rate"] = dtdata.Rows[cc]["rate"].ToString();
                //dr["amount"] = dtdata.Rows[cc]["amount"].ToString();
                //dr["unit"] = dtdata.Rows[cc]["unit"].ToString();
                //dr["descr"] = dtdata.Rows[cc]["descr1"].ToString();
                //dr["descr1"] = dtdata.Rows[cc]["descr2"].ToString();
                //dr["descr2"] = dtdata.Rows[cc]["descr3"].ToString();
                //dr["rfs"] = "0";
                //dr["retremarks"] = "";
                //dt.Rows.Add(dr);
                //Session["dtpitemsrre"] = dt;
            }
            //this.bindgrid();
            dt = frrclass.selectallrritemsdata(li);
            if (dt.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.DataSource = dt;
                rptlist.DataBind();
            }
            else
            {
                lblempty.Visible = true;
                rptlist.DataSource = null;
                rptlist.DataBind();
                lblempty.Text = "No Items Found.";
            }
        }
        ModalPopupExtender2.Hide();
    }

}