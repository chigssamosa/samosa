﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ToolsMaster : System.Web.UI.Page
{
    ForToolsMaster ftmclass = new ForToolsMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                filleditgrid();
            }
            else
            {
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
        }
    }

    public void filleditgrid()
    {
        li.id = Convert.ToInt64(Request["id"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = ftmclass.selecttoolsdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txttoolname.Text = dtdata.Rows[0]["name"].ToString();
            ViewState["name"] = dtdata.Rows[0]["name"].ToString();
            txtunit.Text = dtdata.Rows[0]["unit"].ToString();
            txtrate.Text = dtdata.Rows[0]["rate"].ToString();
            txtopeningstock.Text = dtdata.Rows[0]["opstock"].ToString();
            txtinward.Text = dtdata.Rows[0]["inward"].ToString();
            txtoutward.Text = dtdata.Rows[0]["outward"].ToString();
            txtclosingstock.Text = dtdata.Rows[0]["clstock"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            ViewState["id"] = dtdata.Rows[0]["id"].ToString();
            btnsaves.Text = "Update";
            DataTable dtstock = new DataTable();
            li.name = dtdata.Rows[0]["name"].ToString();
            dtstock = ftmclass.selecttoolsstockdatafromtoolname(li);
            if (dtstock.Rows.Count > 0)
            {
                txtopeningstock.Text = dtstock.Rows[0]["opqty"].ToString();
                txtinward.Text = dtstock.Rows[0]["totp"].ToString();
                txtoutward.Text = dtstock.Rows[0]["tots"].ToString();
                txtclosingstock.Text = dtstock.Rows[0]["totalqty"].ToString();
            }
            else
            { }
        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.name = txttoolname.Text;
        li.unit = txtunit.Text;
        if (txtrate.Text.Trim() != string.Empty)
        {
            li.rate = Convert.ToDouble(txtrate.Text);
        }
        else
        {
            li.rate = 0;
        }
        if (txtopeningstock.Text.Trim() != string.Empty)
        {
            li.opstock = Convert.ToDouble(txtopeningstock.Text);
        }
        else
        {
            li.opstock = 0;
        }
        if (txtinward.Text.Trim() != string.Empty)
        {
            li.inward = Convert.ToDouble(txtinward.Text);
        }
        else
        {
            li.inward = 0;
        }
        if (txtoutward.Text.Trim() != string.Empty)
        {
            li.outward = Convert.ToDouble(txtoutward.Text);
        }
        else
        {
            li.outward = 0;
        }
        if (txtclosingstock.Text.Trim() != string.Empty)
        {
            li.clstock = Convert.ToDouble(txtclosingstock.Text);
        }
        else
        {
            li.clstock = 0;
        }
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        if (btnsaves.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = ftmclass.checkduplicate(li);
            if (dtcheck.Rows.Count == 0)
            {
                ftmclass.inserttoolsdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.name + " Tools Master Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Tool Name already exists.Please try again with new Tool Name.');", true);
                return;
            }
        }
        else
        {
            if (txttoolname.Text.Trim() != ViewState["name"].ToString().Trim())
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtcheck = new DataTable();
                dtcheck = ftmclass.checkduplicate(li);
                if (dtcheck.Rows.Count == 0)
                {
                    li.id = Convert.ToInt64(ViewState["id"].ToString());
                    ftmclass.updatetoolsdata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Tools Master Updated.";
                    faclass.insertactivity(li);
                    //Update Tool Name in whole software
                    li.name1 = txttoolname.Text.Trim();
                    li.name = ViewState["name"].ToString().Trim();
                    ftmclass.updateToolDelMaster(li);
                    ftmclass.updateToolJVMaster(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Tool Name already exists.Please try again with new Tool Name.');", true);
                    return;
                }
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["id"].ToString());
                ftmclass.updatetoolsdata(li);
            }
        }
        Response.Redirect("~/ToolsMasterList.aspx?pagename=ToolsMasterList");
    }
}