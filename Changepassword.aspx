﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Changepassword.aspx.cs" Inherits="Changepassword" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Change Password</span><br />
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Company NO.</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtcompanyid" runat="server" CssClass="form-control" Width="250px"
                    ReadOnly="true"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    User Name</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtuname" runat="server" CssClass="form-control" Width="250px" ReadOnly="true"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Old Password</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtoldpassword" runat="server" CssClass="form-control" Width="250px"
                    TextMode="Password"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    New Password</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtnewpassword" runat="server" CssClass="form-control" Width="250px"
                    TextMode="Password"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Confirm Password</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtconfirmpassword" runat="server" CssClass="form-control" Width="250px"
                    TextMode="Password"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter user name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtuname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter old password."
                    Text="*" ValidationGroup="val" ControlToValidate="txtoldpassword"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter new password."
                    Text="*" ValidationGroup="val" ControlToValidate="txtnewpassword"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter confirm password"
                    Text="*" ValidationGroup="val" ControlToValidate="txtconfirmpassword"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Enter Same Password.Password and Confirm password is not same."
                    ControlToCompare="txtnewpassword" ControlToValidate="txtconfirmpassword" Text="*"
                    ValidationGroup="val"></asp:CompareValidator>
            </div>
        </div>
    </div>
</asp:Content>
