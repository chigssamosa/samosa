﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login"
    Culture="hi-IN" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="bootstrap/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--<script language="javascript" type="text/javascript">
        window.open("CompanySelection.aspx", "secondWindow",
  "fullscreen,scrollbars='yes',statusbar='yes',location='no'").focus();

        //set new window as the opener to bypass alert
        window.opener = 'secondWindow';

        //close original window that was the inital opener without alert
        //window.self.close();
</script>--%>
    <%--<script type="text/javascript">
        window.onload = maxWindow;

        function maxWindow() {
            window.moveTo(0, 0);


            if (document.all) {
                top.window.resizeTo(screen.availWidth, screen.availHeight);
            }

            else if (document.layers || document.getElementById) {
                if (top.window.outerHeight < screen.availHeight || top.window.outerWidth < screen.availWidth) {
                    top.window.outerHeight = screen.availHeight;
                    top.window.outerWidth = screen.availWidth;
                }
            }
        }

</script> --%>
</head>
<body>
    <form id="Form1" runat="server">
    <nav class="navbar navbar-default" style="background-color: #673AB7;">
    <div style="color: White;">
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="padding-top: 8px; color: White; font-weight: bolder;
                    font-size: x-large;">
                    ALLURE</div>
                <div class="col-md-1" style="padding-top: 12px; color: White;">
                    <asp:Label ID="lblshopid" runat="server" Text="Shop ID" Font-Bold="true" Font-Size="Larger"></asp:Label></div>
                
                <div class="col-md-2" style="padding-top: 8px; color: Black;">
                    <asp:DropDownList ID="drpacyear" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                <div class="col-md-2" style="padding-top: 8px; color: Black;">
                    <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" placeholder="User Name"></asp:TextBox></div>
                <div class="col-md-2" style="padding-top: 8px; color: Black;">
                    <asp:TextBox ID="txtpassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password"></asp:TextBox></div>
                <div class="col-md-1" style="padding-top: 8px; color: Black;">
                    <asp:Button ID="btnlogin" runat="server" Text="LogIn" OnClick="btnlogin_Click" 
                        CssClass="btn btn-default forbutton"/></div>
            </div>
        </div>
    </div>
    
    </nav>
    <div class="row table-responsive">
        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="200px" Width="98%">
            <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
            <asp:GridView ID="gvtesting" runat="server" AutoGenerateColumns="False" Width="100%"
                BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                CssClass="table table-bordered" AllowPaging="True">
                <Columns>
                    <asp:TemplateField HeaderText="Student Code" SortExpression="Csnm" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch" SortExpression="Csnm">
                        <ItemTemplate>
                            <asp:Label ID="lblbranch" ForeColor="Black" runat="server" Text='<%# bind("username") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Semister" SortExpression="companyname">
                        <ItemTemplate>
                            <asp:Label ID="lblsemister" runat="server" ForeColor="#505050" Text='<%# bind("password") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#4c4c4c" />
                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
            </asp:GridView>
        </asp:Panel>
    </div>
    <%--<iframe id="fbarcode" runat="server" width="100%" height="650px" src="~/cheque/Overall.xls">
            </iframe>--%>
    </form>
</body>
</html>
