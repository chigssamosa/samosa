﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;

public partial class ToolsReturnEntry : System.Web.UI.Page
{
    ForToolDelMaster ftdmclass = new ForToolDelMaster();
    ForToolsMaster ftmclass = new ForToolsMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                getbrno();
                txtvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemstre"] = CreateTemplate();
            }
            else
            {
                filleditdata();
            }
            
            Page.SetFocus(txtname);
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        dtvno = ftdmclass.selectunusedtooldelvno(li);
        if (dtvno.Rows.Count > 0)
        {
            txtvoucherno.Text = dtvno.Rows[0]["vno"].ToString();
        }
        else
        {
            txtvoucherno.Text = "";
        }
    }

    public void filleditdata()
    {
        li.voucherno = Convert.ToInt64(Request["vno"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = ftdmclass.selecttooldeldatafromvoucherno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
            txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            txtclientcode.Text = dtdata.Rows[0]["clientcode"].ToString();
            txtdespatchedby.Text = dtdata.Rows[0]["dispatchedby"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtitem = new DataTable();
            dtitem = ftdmclass.selecttooldelitemfromvoucherno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
            btnsaveall.Text = "Update";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("toolname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("descr", typeof(string));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetToolname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectalltoolname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getacname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallacname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemstre"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["toolname"] = txttoolname.Text;
            dr["qty"] = txtqty.Text;
            dr["unit"] = txtunit.Text;
            dr["descr"] = txtdescription.Text;
            dt.Rows.Add(dr);
            Session["dtpitemstre"] = dt;
            this.bindgrid();
        }
        else
        {
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.name = txttoolname.Text;
            li.qty = Convert.ToDouble(txtqty.Text);
            li.unit = txtunit.Text;
            li.descr = txtdescription.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.istype = "TR";
            ftdmclass.inserttooldelitemdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.voucherno + " Tools Return Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtitem = new DataTable();
            dtitem = ftdmclass.selecttooldelitemfromvoucherno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
        txttoolname.Text = string.Empty;
        txtqty.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtdescription.Text = string.Empty;
        Page.SetFocus(txttoolname);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemstre"] as DataTable;
        rptlist.DataSource = dtsession;
        rptlist.DataBind();
    }

    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
        li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.voucherdate <= yyyear1 && li.voucherdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.name = txtname.Text;
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtclientcode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        li.dispatchedby = txtdespatchedby.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.istype = "TR";
        if (btnsaveall.Text == "Save")
        {
            if (rptlist.Rows.Count > 0)
            {
                DataTable dtcheck = new DataTable();
                dtcheck = ftdmclass.selecttooldeldatafromvoucherno(li);
                if (dtcheck.Rows.Count == 0)
                {
                    ftdmclass.inserttooldeldata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.voucherno + " Tools Return Inserted.";
                    faclass.insertactivity(li);
                }
                else
                {
                    li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                    ftdmclass.updateisused(li);
                    getbrno();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Voucher No. already exist.Try Again.');", true);
                    return;
                }
                li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                for (int c = 0; c < rptlist.Rows.Count; c++)
                {
                    Label lblname = (Label)rptlist.Rows[c].FindControl("lbltoolname");
                    Label lblqty = (Label)rptlist.Rows[c].FindControl("lblqty");
                    Label lblunit = (Label)rptlist.Rows[c].FindControl("lblunit");
                    Label lbldescr = (Label)rptlist.Rows[c].FindControl("lbldescr");
                    li.name = lblname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.unit = lblunit.Text;
                    li.descr = lbldescr.Text;
                    ftdmclass.inserttooldelitemdata(li);
                }
                ftdmclass.updateisused(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Tool Data.');", true);
                return;
            }
        }
        else
        {
            li.voucherno = Convert.ToInt64(Request["vno"].ToString());
            ftdmclass.updatetooldeldata(li);
            ftdmclass.updatetooldelitemdatedata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.voucherno + " Tools Return Updated.";
            faclass.insertactivity(li);
        }
        Response.Redirect("~/ToolsReturnEntryList.aspx?pagename=ToolsReturnEntryList");
    }

    protected void rptlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemstre"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemstre"] = dt;
            this.bindgrid();
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
            li.id = Convert.ToInt64(lblid.Text);
            ftdmclass.deletetooldelitemdatafromid(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.id + " Tools Return Item Deleted.";
            faclass.insertactivity(li);
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            DataTable dtitem = new DataTable();
            dtitem = ftdmclass.selecttooldelitemfromvoucherno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
    }
    protected void txttoolname_TextChanged(object sender, EventArgs e)
    {
        li.name = txttoolname.Text;
        DataTable dtdata = new DataTable();
        dtdata = ftdmclass.selecttooldatafromtoolname(li);
        if (dtdata.Rows.Count > 0)
        {
            txtqty.Text = "1";
            txtunit.Text = dtdata.Rows[0]["unit"].ToString();
        }
        else
        {
            txtqty.Text = "1";
            txtunit.Text = string.Empty;
        }
        Page.SetFocus(txtqty);
    }
    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(txtdespatchedby);
    }

}