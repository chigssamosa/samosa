﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockAdjustment.aspx.cs" Inherits="StockAdjustment" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <link href="css/AjaxStyle.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .ajax__tab_xp .ajax__tab_tab
        {
            height: 20px !important;
            padding: 0px !important;
            margin: 0px !important;</style>
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Stock Adjustment</span><br />
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Item Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>--%>
                        <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btngetdata" runat="server" Text="Get Data" class="btn btn-default forbutton"
                            ValidationGroup="val" OnClick="btngetdata_Click" />
                    </div>
                    <div class="col-md-4">
                        <asp:HiddenField ID="hdnpc" runat="server" />
                        <asp:HiddenField ID="hdnsc" runat="server" />
                        <asp:HiddenField ID="hdnop" runat="server" />
                        <asp:HiddenField ID="hdnsjvin" runat="server" />
                        <asp:HiddenField ID="hdnsjvout" runat="server" />
                        <asp:HiddenField ID="hdnscid" runat="server" />
                        <asp:HiddenField ID="hdnpcid" runat="server" />
                        <asp:HiddenField ID="hdntype1" runat="server" />
                        <asp:HiddenField ID="hdntype2" runat="server" />
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btnrefresh" runat="server" Text="Refresh" class="btn btn-default forbutton"
                            ValidationGroup="val" OnClick="btnrefresh_Click" /></div>
                </div>
                <div class="row">
                    <span style="color: white; background-color: Red">Sales Details</span><br />
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    Challan
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">
                                    Details
                                </label>
                            </div>
                            <div class="col-md-1">
                                <label class="control-label">
                                    Quantity
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtscno" runat="server" CssClass="form-control" Width="150px" placeholder="Challan"></asp:TextBox></div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtscdescr1" runat="server" CssClass="form-control" Width="250px"
                                    placeholder="Details"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtscqty" runat="server" CssClass="form-control" Width="150px" placeholder="Quantity"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtscdescr2" runat="server" CssClass="form-control" Width="250px"
                                    placeholder="Details"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    Date
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">
                                    Bal. Quantity
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtscdate" runat="server" CssClass="form-control" Width="150px"
                                    placeholder="Date"></asp:TextBox></div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtscdescr3" runat="server" CssClass="form-control" Width="250px"
                                    placeholder="Details"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtscbalanceqty" runat="server" CssClass="form-control" Width="150px"
                                    placeholder="Bal. Quantity"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 40px;">
        </div>
        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
            <asp:TabPanel HeaderText="Sales Details" ID="TabPanel1" runat="server">
                <ContentTemplate>
                    <div class="row" style="height: 10px;">
                    </div>
                    <div class="row table-responsive">
                        <div class="col-md-12">
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                                <asp:Label ID="lblemptysc" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="gvsc" runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="Ridge"
                                    EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                    BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" GridLines="None"
                                    CellSpacing="1" OnRowCommand="gvsc_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                    ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SC NO." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("strscno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Party Name" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblacname" runat="server" ForeColor="#505050" Text='<%# bind("acname") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Challan Date" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("scdate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adj. Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lbladjqty" runat="server" ForeColor="Black" Text='<%# bind("adjqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblqty" runat="server" ForeColor="Black" Text='<%# bind("stockqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Balance Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbalanceqty" runat="server" ForeColor="Black" Text='<%# bind("balqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type" SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblatype" runat="server" ForeColor="Black" Text='<%# bind("atype") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID" SortExpression="Location" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblid" runat="server" ForeColor="Black" Text='<%# bind("id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:TabPanel>
            <asp:TabPanel HeaderText="Purchase Details" ID="TabPanel2" runat="server">
                <ContentTemplate>
                    <div class="row table-responsive">
                        <div class="col-md-12">
                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                                <asp:Label ID="lblemptypc" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="gvpc" runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="Ridge"
                                    EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                    BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" GridLines="None"
                                    CellSpacing="1" OnRowCommand="gvpc_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                    ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PC NO." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("pcno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Party Name" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblacname" runat="server" ForeColor="#505050" Text='<%# bind("acname") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Challan Date" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("pcdate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adj. Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lbladjqty" runat="server" ForeColor="Black" Text='<%# bind("adjqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblqty" runat="server" ForeColor="Black" Text='<%# bind("stockqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Balance Qty." SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbalanceqty" runat="server" ForeColor="Black" Text='<%# bind("balqty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type" SortExpression="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblatype" runat="server" ForeColor="Black" Text='<%# bind("atype") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID" SortExpression="Location" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblid" runat="server" ForeColor="Black" Text='<%# bind("id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:TabPanel>
            <asp:TabPanel HeaderText="Updation" ID="TabPanel3" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <span style="color: white; background-color: Red; text-align: center; padding-left: 20px">
                            Purchase Details</span><br />
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">
                                        Challan
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label class="control-label">
                                        Details
                                    </label>
                                </div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Bal.Quantity
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtpcno" runat="server" CssClass="form-control" Width="150px" placeholder="Challan"></asp:TextBox></div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtpcdescr1" runat="server" CssClass="form-control" Width="350px"
                                        placeholder="Details"></asp:TextBox>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtpcqty" runat="server" CssClass="form-control" Width="150px" placeholder="Quantity"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtpcdescr2" runat="server" CssClass="form-control" Width="350px"
                                        placeholder="Details"></asp:TextBox>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">
                                        Date
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtpcdate" runat="server" CssClass="form-control" Width="150px"
                                        placeholder="Date"></asp:TextBox></div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtpcdescr3" runat="server" CssClass="form-control" Width="350px"
                                        placeholder="Details"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtpcadjqty" runat="server" CssClass="form-control" Width="150px"
                                        placeholder="Add.Quantity" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="height: 20px">
                    </div>
                    <div class="row" style="padding-left: 20px">
                        <asp:Button ID="btnok" runat="server" Text="OK" Height="30px" BackColor="#F05283"
                            ValidationGroup="val" OnClick="btnok_Click" />
                        <asp:Button ID="btncancel" runat="server" Text="Cancel" Height="30px" BackColor="#F05283"
                            ValidationGroup="val" OnClick="btncancel_Click" /></div>
                </ContentTemplate>
            </asp:TabPanel>
        </asp:TabContainer>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel3"
            TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" Height="130px" align="center"
            Width="45%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>Confirmation Window</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-8">
                    <b>Are you sure to refresh this item stock??</b></div>
            </div>
            <div class="row">
                <asp:Button ID="btnyes" runat="server" Text="Yes" BackColor="Red" ForeColor="White"
                    OnClick="btnyes_Click" />
                <asp:Button ID="btnClose" runat="server" Text="No" BackColor="Red" ForeColor="White" />
                <%--<asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />--%>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
