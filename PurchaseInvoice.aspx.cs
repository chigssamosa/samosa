﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;
using System.Data.SqlClient;

public partial class PurchaseInvoice : System.Web.UI.Page
{
    ForPurchaseInvoice fpiclass = new ForPurchaseInvoice();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                fillacnamedrop();
                fillpurchaseacdrop();
                fillitemnamedrop();
                getpino();
                fillinvtypedrop();
                fillstatusdrop();
                fillactaxdesc();
                Session["dtpitemspuin"] = null;
                Session["dtpitemspuin1"] = null;
                txtpidate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspuin"] = CreateTemplate();
                Page.SetFocus(drpinvtype);
            }
            else if (Request["mode"].ToString() == "ledger")
            {
                fillacnamedrop();
                fillpurchaseacdrop();
                fillitemnamedrop();
                fillinvtypedrop();
                fillstatusdrop();
                fillactaxdesc();
                filleditdata();
                //hideimage();
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                fillacnamedrop();
                fillpurchaseacdrop();
                fillitemnamedrop();
                fillinvtypedrop();
                fillstatusdrop();
                fillactaxdesc();
                filleditdata();
                //hideimage();
            }
            //hideimage();

        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillpurchaseacdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallprchaseacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drppurchaseac.Items.Clear();
            drppurchaseac.DataSource = dtdata;
            drppurchaseac.DataTextField = "acname";
            drppurchaseac.DataBind();
            drppurchaseac.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drppurchaseac.Items.Clear();
            drppurchaseac.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvpiitemlist.Rows[c].FindControl("imgbtnselect11");
            if (btnsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillactaxdesc()
    {
        DataTable acdesc = new DataTable();
        acdesc = fpiclass.selectalltaxacdesc();
        DataTable acdesc1 = new DataTable();
        acdesc1 = fpiclass.selectalltaxacdescfrommisc();
        if (acdesc.Rows.Count > 0)
        {
            ViewState["vatdesc"] = acdesc.Rows[0]["vatdesc"].ToString();
            ViewState["addtaxdesc"] = acdesc.Rows[0]["addtaxdesc"].ToString();
            ViewState["cstdesc"] = acdesc.Rows[0]["cstdesc"].ToString();
            ViewState["servicetaxdesc"] = acdesc.Rows[0]["servicetaxdesc"].ToString();
            drptaxdesc.DataSource = acdesc1;
            drptaxdesc.DataTextField = "name";
            drptaxdesc.DataBind();
            drptaxdesc.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drptaxdesc.Items.Clear();
            drptaxdesc.Items.Insert(0, "--SELECT--");
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Account type for VAT , Addvat , CST and service tax is not avaailable so go to VAT type Master add ledger account type and try again.');", true);
            return;
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillinvtypedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallinvtype(li);
        if (dtdata.Rows.Count > 0)
        {
            drpinvtype.Items.Clear();
            drpinvtype.DataSource = dtdata;
            drpinvtype.DataTextField = "name";
            drpinvtype.DataBind();
            drpinvtype.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpinvtype.Items.Clear();
            drpinvtype.Items.Insert(0, "--SELECT--");
        }
    }

    public void getpino()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fpiclass.selectunusedpurchaseinvoiceno(li);
        dtvno = fpiclass.selectunusedpurchaseinvoicenolast(li);
        if (dtvno.Rows.Count > 0)
        {
            txtinvoiceno.Text = (Convert.ToInt64(dtvno.Rows[0]["pino"].ToString()) + 1).ToString();
        }
        else
        {
            txtinvoiceno.Text = "";
        }
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strpino = Request["strpino"].ToString();
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallpimasterdatafrompinostring(li);
        if (dtdata.Rows.Count > 0)
        {
            txtinvoiceno.Text = dtdata.Rows[0]["pino"].ToString();
            ViewState["pino"] = Convert.ToInt64(dtdata.Rows[0]["pino"].ToString());
            txtpidate.Text = Convert.ToDateTime(dtdata.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            drpinvtype.SelectedItem.Text = dtdata.Rows[0]["invtype"].ToString();
            ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
            txtbillno.Text = dtdata.Rows[0]["billno"].ToString();
            txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtchallanno.Text = dtdata.Rows[0]["pcno"].ToString();
            txtchallanno1.Text = dtdata.Rows[0]["pcno1"].ToString();
            txtchallandate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            if (dtdata.Rows[0]["pcdate1"].ToString() != string.Empty)
            {
                txtchallandate1.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            drppurchaseac.SelectedValue = dtdata.Rows[0]["purchaseac"].ToString();
            txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
            txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
            txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
            txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
            txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
            txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
            txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
            txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
            txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
            txtform.Text = dtdata.Rows[0]["form"].ToString();
            txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
            txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
            txtsrtringpcno.Text = dtdata.Rows[0]["stringpcno"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtitems = new DataTable();
            dtitems = fpiclass.selectallpiitemsfrompinostring(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpiitemlist.Visible = true;
                gvpiitemlist.DataSource = dtitems;
                gvpiitemlist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvpiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Invoice Item Found.";
                lblcount.Text = "0";
            }
            btnsave.Text = "Update";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("vno", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("stockqty", typeof(double));
        dtpitems.Columns.Add("qtyremain", typeof(double));
        dtpitems.Columns.Add("qtyused", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        //dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("ccode", typeof(string));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getpcno(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallpcno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvatdesc(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectalltaxacdescfrommiscautocomp(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getpurchaseac(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallpurchaseac(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtcode.Text.Trim() != string.Empty)
        {
            string cc = txtcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtcode.Text = string.Empty;
        }
        Page.SetFocus(txtdescription1);
    }

    //public void countgv()
    //{

    //    DataTable dtsoitem = (DataTable)Session["dtpitems"];
    //    //DataTable dtsoitem1 = (DataTable)Session["dtpitems12"];
    //    //if (dtsoitem1 != null)
    //    //{
    //    //    dtsoitem.Rows.Clear();
    //    //    dtsoitem.Merge(dtsoitem1);
    //    //}
    //    //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
    //    double vatp = 0;
    //    double qty = 0;
    //    double rate = 0;
    //    double addvatp = 0;
    //    double cstp = 0;
    //    double vatamt = 0;
    //    double addvatamt = 0;
    //    double cstamt = 0;
    //    double baseamt = 0;
    //    double amount = 0;
    //    for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
    //    {
    //        TextBox txtgvqty1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
    //        TextBox txtgvrate1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridrate");
    //        TextBox txtgvamount1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
    //        //            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
    //        Label lbltaxtype = (Label)gvpiitemlist.Rows[c].FindControl("lbltaxtype");
    //        Label lblvatp = (Label)gvpiitemlist.Rows[c].FindControl("lblvatp");
    //        Label lbladdvatp = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatp");
    //        Label lblcstp = (Label)gvpiitemlist.Rows[c].FindControl("lblcstp");
    //        Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
    //        Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
    //        Label lblcstamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
    //        Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");




    //        baseamt = baseamt + Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text);
    //        amount = amount + baseamt;
    //        if (lblvatamt.Text != "")
    //        {
    //            vatamt = vatamt + Convert.ToDouble(lblvatamt.Text);
    //        }
    //        if (lbladdvatamt.Text != "")
    //        {
    //            addvatamt = addvatamt + Convert.ToDouble(lbladdvatamt.Text);
    //        }
    //        if (lblcstp.Text != "")
    //        {
    //            cstp = cstp + Convert.ToDouble(lblcstp.Text);
    //        }
    //        if (lblcstamt.Text != "")
    //        {
    //            cstamt = cstamt + Convert.ToDouble(lblcstamt.Text);
    //        }
    //        if (lblvatp.Text != "")
    //        {
    //            vatp = Convert.ToDouble(lblvatp.Text);
    //        }
    //        if (lbladdvatp.Text != "")
    //        {
    //            addvatp = Convert.ToDouble(lbladdvatp.Text);
    //        }
    //        vatp = vatp + vatp + addvatp;

    //    }
    //    txttotbasicamount.Text = baseamt.ToString();
    //    double cartage = 0;
    //    if (txtcartage.Text.Trim() != string.Empty)
    //    {
    //        cartage = Convert.ToDouble(txtcartage.Text);
    //    }
    //    else
    //    {
    //        txtcartage.Text = "0";
    //    }
    //    double servp = 0;
    //    double servamt = 0;
    //    double roundoff = 0;
    //    if (txtservicep.Text.Trim() != string.Empty)
    //    {
    //        txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
    //    }
    //    else
    //    {
    //        txtservicep.Text = "0";
    //        txtserviceamt.Text = "0";
    //    }
    //    if (txtroundoff.Text.Trim() != string.Empty)
    //    {
    //        roundoff = Convert.ToDouble(txtroundoff.Text);
    //    }
    //    else
    //    {
    //        txtroundoff.Text = "0";
    //    }
    //    double serant = 0;
    //    if (txtserviceamt.Text.Trim() != string.Empty)
    //    {
    //        serant = Convert.ToDouble(txtserviceamt.Text);
    //    }
    //    //txtfcstp.Text = cstp.ToString();
    //    txttotcst.Text = cstamt.ToString();
    //    txttotvat.Text = vatamt.ToString();
    //    //txtfvatp.Text = vatp.ToString();
    //    txttotaddtax.Text = addvatamt.ToString();
    //    txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotvat.Text) + cartage).ToString();
    //    double bamt = Convert.ToDouble(txtbillamount.Text);
    //    double round1 = bamt - Math.Truncate(bamt);
    //    txtroundoff.Text = Math.Round(round1, 2).ToString();
    //    roundoff = Convert.ToDouble(txtroundoff.Text);
    //    txtbillamount.Text = (Math.Round(bamt)).ToString();
    //}

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemspuin"];
        //DataTable dtsoitem1 = (DataTable)Session["dtpitems12"];
        //if (dtsoitem1 != null)
        //{
        //    dtsoitem.Rows.Clear();
        //    dtsoitem.Merge(dtsoitem1);
        //}
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double vatamtc = 0;
        double addvatamtc = 0;
        double cstamtc = 0;
        double baseamtc = 0;
        double amount = 0;
        for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
        {
            TextBox txtgvqty1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
            //            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            TextBox txtgridtaxtype = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridtaxtype");
            Label lbltaxtype = (Label)gvpiitemlist.Rows[c].FindControl("lbltaxtype");
            Label lblvatp = (Label)gvpiitemlist.Rows[c].FindControl("lblvatp");
            Label lbladdvatp = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatp");
            Label lblcstp = (Label)gvpiitemlist.Rows[c].FindControl("lblcstp");
            Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblcstamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
            Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");




            baseamt = baseamt + Math.Round(Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text), 2);
            baseamtc = Math.Round(Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text), 2);
            amount = amount + baseamt;
            if (txtgridtaxtype.Text != "")
            {
                var cc = txtgridtaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = txtgridtaxtype.Text;
                    vatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[1]);
                    //txtgvvat.Text = vatp.ToString();
                    //txtgvadvat.Text = addvatp.ToString();
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lblvatamt.Text = (Math.Round(((Convert.ToDouble(lblvatp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                    lbladdvatamt.Text = (Math.Round(((Convert.ToDouble(lbladdvatp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();

                }
                else
                {
                    lblcstamt.Text = (Math.Round(((Convert.ToDouble(lblcstp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                }
            }
            else
            {
                lblcstamt.Text = (Math.Round(((Convert.ToDouble(lblcstp.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
            }
            if (lblvatamt.Text != "")
            {
                vatamt = vatamt + Convert.ToDouble(lblvatamt.Text);
                vatamtc = Convert.ToDouble(lblvatamt.Text);
            }
            if (lbladdvatamt.Text != "")
            {
                addvatamt = addvatamt + Convert.ToDouble(lbladdvatamt.Text);
                addvatamtc = Convert.ToDouble(lbladdvatamt.Text);
            }
            if (lblcstp.Text != "")
            {
                cstp = cstp + Convert.ToDouble(lblcstp.Text);
            }
            if (lblcstamt.Text != "")
            {
                cstamt = cstamt + Convert.ToDouble(lblcstamt.Text);
                cstamtc = Convert.ToDouble(lblcstamt.Text);
            }
            if (lblvatp.Text != "")
            {
                vatp = Convert.ToDouble(lblvatp.Text);
            }
            if (lbladdvatp.Text != "")
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
            }
            vatp = vatp + vatp + addvatp;
            lblamount.Text = (baseamtc + vatamtc + addvatamtc + cstamtc).ToString();
        }
        txttotbasicamount.Text = Math.Round(baseamt).ToString();
        //if (txtservicep.Text.Trim() != string.Empty)
        //{
        //    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtservicep.Text = "0";
        //    txtserviceamt.Text = "0";
        //}


        double cartage = 0;
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        else
        {
            txtcartage.Text = "0";
        }
        double servp = 0;
        double servamt = 0;
        double roundoff = 0;
        if (txtservicep.Text.Trim() != string.Empty)
        {
            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
        }
        else
        {
            txtservicep.Text = "0";
            txtserviceamt.Text = "0";
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        else
        {
            txtroundoff.Text = "0";
        }
        double serant = 0;
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            serant = Convert.ToDouble(txtserviceamt.Text);
        }



        //txtfcstp.Text = cstp.ToString();
        txttotcst.Text = Math.Round(cstamt, 2).ToString();
        txttotvat.Text = Math.Round(vatamt, 2).ToString();
        //txtfvatp.Text = vatp.ToString();
        txttotaddtax.Text = Math.Round(addvatamt, 2).ToString();
        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotvat.Text) + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //if (round1 > 0.5)
        //{
        //    txtroundoff.Text = ((-1)*(Math.Round((1 - round1), 2))).ToString();
        //}
        //else
        //{
        //    txtroundoff.Text = Math.Round(round1, 2).ToString();
        //}
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }

        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
    }

    public void count1()
    {
        if (txtbillqty.Text.Trim() == string.Empty)
        {
            txtbillqty.Text = "0";
        }
        if (txtrate.Text.Trim() == string.Empty)
        {
            txtrate.Text = "0";
        }
        txtbasicamt.Text = Math.Round(((Convert.ToDouble(txtbillqty.Text)) * (Convert.ToDouble(txtrate.Text))), 2).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtbasicamt.Text);
        //double baseamt = ((Convert.ToDouble(txtbillqty.Text)) * (Convert.ToDouble(txtrate.Text)));
        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvat.Text = vatp.ToString();
                txtgvadtax.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }

            }
            else
            {
                txtgvvat.Text = "0";
                txtgvadtax.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvat.Text = "0";
            txtgvadtax.Text = "0";
        }

        if (txtgvvat.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvat.Text);
            txtvat.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvat.Text = "0";
            txtvat.Text = "0";
        }
        if (txtgvadtax.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadtax.Text);
            txtaddtax.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadtax.Text = "0";
            txtaddtax.Text = "0";
        }
        if (txtgvcst.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgvcst.Text);
            txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        vatamt = Convert.ToDouble(txtvat.Text);
        addvatamt = Convert.ToDouble(txtaddtax.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        if (gvpiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            double roundoff = 0;
            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvpiitemlist.Rows[c].FindControl("lblbasicamount");
                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                }
                if (lbladdvatamt.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                }
                if (lblamount.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount.Text);
                }
            }
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + serant).ToString();
            //double bamt = Convert.ToDouble(txtbillamount.Text);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 > 0.5)
            //{
            //    //txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //    txtroundoff.Text = ((-1) * (Math.Round((1 - round1), 2))).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }

    }

    public void countFooter()
    {
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;                
        if (gvpiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            double roundoff = 0;
            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvpiitemlist.Rows[c].FindControl("lblbasicamount");
                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                }
                if (lbladdvatamt.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                }
                if (lblamount.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount.Text);
                }
            }
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            txtbillamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + serant).ToString();
            //double bamt = Convert.ToDouble(txtbillamount.Text);
            //double round1 = bamt - Math.Truncate(bamt);
            ////txtroundoff.Text = Math.Round(round1, 2).ToString();
            //if (round1 > 0.5)
            //{
            //    //txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
            //    txtroundoff.Text = ((-1) * (Math.Round((1 - round1), 2))).ToString();
            //}
            //else
            //{
            //    txtroundoff.Text = Math.Round(round1, 2).ToString();
            //}
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }

    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                //txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["vattype"].ToString();
                string vattype = dtdata.Rows[0]["vattype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvat.Text = "0";
                    txtgvadtax.Text = vattype.Split('-')[1];
                    txtaddtax.Text = "0";
                    count1();
                }
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                //txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadtax.Text = string.Empty;
                txtvat.Text = string.Empty;
                txtaddtax.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtamount.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            //txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadtax.Text = string.Empty;
            txtvat.Text = string.Empty;
            txtaddtax.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtamount.Text = string.Empty;
        }
    }
    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvat);
    }

    protected void txtservicep_TextChanged(object sender, EventArgs e)
    {
        countgv();
        Page.SetFocus(txtserviceamt);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemspuin"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvpiitemlist.Visible = true;
            lblempty.Visible = false;
            gvpiitemlist.DataSource = dtsession;
            gvpiitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvpiitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    protected void gvpiitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemspuin"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemspuin"] = dt;
            this.bindgrid();
            countgv();
        }
        else
        {
            if (gvpiitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvpiitemlist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvpiitemlist.Rows[e.RowIndex].FindControl("lblvid");
                li.vid = Convert.ToInt64(lblvid.Text);
                li.id = Convert.ToInt64(lblid.Text);


                Label lblitemname = (Label)gvpiitemlist.Rows[e.RowIndex].FindControl("lblitemname");
                Label lblqty = (Label)gvpiitemlist.Rows[e.RowIndex].FindControl("lblqty");
                //Label lblqtyremain = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyremain");
                //Label lblqtyused = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyused");
                Label lblvno = (Label)gvpiitemlist.Rows[e.RowIndex].FindControl("lblvno");

                li.itemname = lblitemname.Text;
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.scno = Convert.ToInt64(lblvno.Text);
                if (li.vid != 0)
                {
                    DataTable dtqty = new DataTable();
                    dtqty = fpiclass.selectqtyremainusedfromscno(li);
                    li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                    li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                    fpiclass.updateqtyduringdelete(li);
                    li.vid = Convert.ToInt64(lblvid.Text);
                    li.strpino = "";
                    fpiclass.updatestrpinoinpcitems1(li);
                }

                fpiclass.deletepiitemsdatafromid(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Purchase Invoice Item Deleted.";
                faclass.insertactivity(li);
                //li.istype = "CR";
                //flclass.deleteledgerdata(li);
                li.pino = Convert.ToInt64(txtinvoiceno.Text);
                li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallpiitemsfrompinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpiitemlist.Visible = true;
                    gvpiitemlist.DataSource = dtdata;
                    gvpiitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvpiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }

                //Update whole invoice data
                countFooter();
                Label lblccodee = (Label)gvpiitemlist.Rows[0].FindControl("lblccode");
                li.totbasicamount = Convert.ToDouble(txttotbasicamount.Text);
                li.totvatamt = Convert.ToDouble(txttotvat.Text);
                li.totaddtaxamt = Convert.ToDouble(txttotaddtax.Text);
                li.totcstamt = Convert.ToDouble(txttotcst.Text);
                li.servicetaxp = Convert.ToDouble(txtservicep.Text);
                li.servicetaxamount = Convert.ToDouble(txtserviceamt.Text);
                li.cartage = Convert.ToDouble(txtcartage.Text);
                li.roundoff = Convert.ToDouble(txtroundoff.Text);
                li.billamount = Convert.ToDouble(txtbillamount.Text);
                fpiclass.updatepimasterdataafterdelete(li);
                fpiclass.deletepurchaseinvoiceledgeraa(li);
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = drpacname.SelectedItem.Text;
                li.creditcode = drppurchaseac.SelectedItem.Text;
                li.description = "totald";
                li.istype1 = "C";
                if (Convert.ToDouble(txtroundoff.Text) <= 0)
                {
                    li.amount = Convert.ToDouble(txtbillamount.Text);
                }
                else
                {
                    li.amount = Convert.ToDouble(txtbillamount.Text);
                }
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
                //2
                li.description = "";
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = drppurchaseac.SelectedItem.Text;
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totalc";
                li.istype1 = "D";
                //li.amount = Convert.ToDouble(txtbillamount.Text);
                if (txtcartage.Text == string.Empty && txtcartage.Text == "")
                {
                    txtcartage.Text = "0";
                }
                li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
                //}

                //3
                if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["cstdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "cst";
                    li.istype1 = "D";
                    li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                    li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "PI";
                    fpiclass.insertledgerdata(li);
                }
                //4
                if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["vatdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "totvat";
                    li.istype1 = "D";
                    li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                    li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "PI";
                    fpiclass.insertledgerdata(li);
                }
                //5
                if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
                {
                    li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    li.debitcode = ViewState["servicetaxdesc"].ToString();
                    li.creditcode = drpacname.SelectedItem.Text;
                    li.description = "setvicetax";
                    li.istype1 = "D";
                    li.amount = Convert.ToDouble(txtserviceamt.Text);
                    li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //li.voucherno = SessionMgt.voucherno;
                    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                    li.refid = 0;
                    li.ccode = Convert.ToInt64(lblccodee.Text);
                    li.istype = "PI";
                    fpiclass.insertledgerdata(li);
                }

                //

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }        
        hideimage();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            for (int p = 0; p < gvpiitemlist.Rows.Count; p++)
            {
                TextBox txtgvstockqty = (TextBox)gvpiitemlist.Rows[p].FindControl("txtgvstockqty");
                if (Convert.ToDouble(txtgvstockqty.Text) <= 0 || txtgvstockqty.Text.Trim() == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Stock Qty must be greater than 0(Zero).');", true);
                    return;
                }
            }
            li.billno = Convert.ToInt64(txtbillno.Text);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtc3 = new DataTable();
            dtc3 = fpiclass.selectpifrompartybillno(li);
            if (dtc3.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Party Bill No Already used.Enter new Party Bill No and Try Again.');", true);
                return;
            }
        }
        li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.pidate <= yyyear1 && li.pidate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //Check Ledger Entry Same OR Not
        //if (Convert.ToDouble(txtroundoff.Text) >= 0)
        //{
        double ctot = 0;
        double dtot = 0;
        ctot = Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txtroundoff.Text)), 2);
        dtot = Math.Round(Convert.ToDouble(txtbillamount.Text), 2);
        if (ctot != dtot)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Amount Mismatch.Please Update data and try again.');", true);
            return;
        }
        //}
        //else
        //{
        //    double ctot = 0;
        //    double dtot = 0;
        //    ctot = Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text) - Convert.ToDouble(txtroundoff.Text);
        //    dtot = Convert.ToDouble(txtbillamount.Text);
        //    if (ctot != dtot)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Ledger Amount Mismatch.Please Update data and try again.');", true);
        //        return;
        //    }
        //}
        for (int w = 0; w < gvpiitemlist.Rows.Count; w++)
        {
            TextBox txttt = (TextBox)gvpiitemlist.Rows[w].FindControl("txtgridtaxdesc");
            if (txttt.Text.Trim() == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter tax desc for all item and try again.');", true);
                return;
            }
        }
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.pino = Convert.ToInt64(txtinvoiceno.Text);
        li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.invtype = drpinvtype.SelectedItem.Text;
        if (txtbillno.Text.Trim() != string.Empty)
        {
            li.billno = Convert.ToInt64(txtbillno.Text);
        }
        else
        {
            li.billno = 0;
        }
        li.billdate = Convert.ToDateTime(txtbilldate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtchallanno.Text.Trim() != string.Empty)
        {
            li.pcno = Convert.ToInt64(txtchallanno.Text);
        }
        else
        {
            li.pcno = 0;
        }
        li.pcdate = Convert.ToDateTime(txtchallandate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtchallanno1.Text.Trim() != string.Empty)
        {
            li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
        }
        else
        {
            li.pcno1 = 0;
        }
        if (txtchallandate1.Text.Trim() != string.Empty)
        {
            li.pcdate1 = Convert.ToDateTime(txtchallandate1.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.pcdate1 = sqldatenull;
        }
        li.acname = drpacname.SelectedItem.Text;
        li.purchaseac = drppurchaseac.SelectedItem.Text;
        li.totbasicamount = Convert.ToDouble(txttotbasicamount.Text);
        li.totvatamt = Convert.ToDouble(txttotvat.Text);
        li.totaddtaxamt = Convert.ToDouble(txttotaddtax.Text);
        li.totcstamt = Convert.ToDouble(txttotcst.Text);
        li.servicetaxp = Convert.ToDouble(txtservicep.Text);
        li.servicetaxamount = Convert.ToDouble(txtserviceamt.Text);
        li.cartage = Convert.ToDouble(txtcartage.Text);
        li.roundoff = Convert.ToDouble(txtroundoff.Text);
        li.billamount = Convert.ToDouble(txtbillamount.Text);
        li.type = "PURCHASE";
        li.remainamount = Convert.ToDouble(txtbillamount.Text);
        li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.stringpcno = txtsrtringpcno.Text;
        li.cmon = Convert.ToInt16(txtpidate.Text.Split('-')[1]);
        if (li.cmon == 1)
        {
            li.cmonname = "JANUARY";
        }
        else if (li.cmon == 2)
        {
            li.cmonname = "FEBRUARY";
        }
        else if (li.cmon == 3)
        {
            li.cmonname = "MARCH";
        }
        else if (li.cmon == 4)
        {
            li.cmonname = "APRIL";
        }
        else if (li.cmon == 5)
        {
            li.cmonname = "MAY";
        }
        else if (li.cmon == 6)
        {
            li.cmonname = "JUNE";
        }
        else if (li.cmon == 7)
        {
            li.cmonname = "JULY";
        }
        else if (li.cmon == 8)
        {
            li.cmonname = "AUGUST";
        }
        else if (li.cmon == 9)
        {
            li.cmonname = "SEPTEMBER";
        }
        else if (li.cmon == 10)
        {
            li.cmonname = "OCTOBER";
        }
        else if (li.cmon == 11)
        {
            li.cmonname = "NOVEMBER";
        }
        else
        {
            li.cmonname = "DECEMBER";
        }
        li.monyr = li.cmonname + "-" + txtpidate.Text.Split('-')[2];
        if (txtremakrs.Text != string.Empty)
        {
            li.remarks = txtremakrs.Text;
        }
        else
        {
            li.remarks = "IMMEDIATE";
        }
        li.transportname = txttransportname.Text;
        li.form = txtform.Text;
        li.salesmanname = txtsalesman.Text;
        li.lrno = txtlrno.Text;
        if (txtlrdate.Text != string.Empty)
        {
            li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.lrdate = sqldatenull;
        }
        if (txtduedays.Text.Trim() != string.Empty)
        {
            li.duedays = Convert.ToDouble(txtduedays.Text);
        }
        else
        {
            li.duedays = 0;
        }
        if (btnsave.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fpiclass.selectallpimasterdatafrompinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                fpiclass.insertpimasterdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strpino + " Purchase Invoice Inserted.";
                faclass.insertactivity(li);
                //string[] parts = txtsrtringpcno.Text.Split(',');
                //foreach (string part in parts)
                //{
                li.stringpcno = txtsrtringpcno.Text;
                //fpiclass.updatepcstatusforallchno(li);
                //}
            }
            else
            {
                li.pino = Convert.ToInt64(txtinvoiceno.Text);
                fpiclass.updateisused(li);
                getpino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Invoice No. already exist.Try Again.');", true);
                return;
            }
            //li.pino = SessionMgt.voucherno;
            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            {
                li.pino = Convert.ToInt64(txtinvoiceno.Text);
                Label lblid = (Label)gvpiitemlist.Rows[c].FindControl("lblid");
                Label lblvno = (Label)gvpiitemlist.Rows[c].FindControl("lblvno");
                Label lblitemname = (Label)gvpiitemlist.Rows[c].FindControl("lblitemname");
                TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                TextBox txtgvstockqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgvstockqty");
                Label lblqtyremain = (Label)gvpiitemlist.Rows[c].FindControl("lblqtyremain");
                Label lblqtyused = (Label)gvpiitemlist.Rows[c].FindControl("lblqtyused");
                TextBox lblrate = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridrate");
                TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                TextBox lbltaxtype = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridtaxtype");
                Label lblvatp = (Label)gvpiitemlist.Rows[c].FindControl("lblvatp");
                Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatp = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatp");
                Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                TextBox lblcstp = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridcstp");
                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                Label lblccode = (Label)gvpiitemlist.Rows[c].FindControl("lblccode");
                TextBox lbldesc1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc3");
                TextBox lbltaxdesc = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridtaxdesc");



                li.id = Convert.ToInt64(lblid.Text);
                li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                //if (li.qty > 0)
                //{
                li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
                li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
                //li.unit = lblunit.Text;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.taxtype = lbltaxtype.Text;
                li.taxdesc = lbltaxdesc.Text;
                if (lblvatp.Text != "")
                {
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                }
                else
                {
                    li.vatp = 0;
                }
                if (lbladdvatp.Text != "")
                {
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                }
                else
                {
                    li.addtaxp = 0;
                }
                if (lblcstp.Text != "")
                {
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                }
                else
                {
                    li.cstp = 0;
                }
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                if (lblbasicamount.Text != "")
                {
                    li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                }
                else
                {
                    li.basicamount = 0;
                }
                if (lblsctamt.Text != "")
                {
                    li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                }
                else
                {
                    li.cstamt = 0;
                }
                if (lblvatamt.Text != "")
                {
                    li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                }
                else
                {
                    li.vatamt = 0;
                }
                if (lbladdvatamt.Text != "")
                {
                    li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                }
                else
                {
                    li.addtaxamt = 0;
                }
                if (lblamount.Text != "")
                {
                    li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                }
                else
                {
                    li.amount = 0;
                }
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                li.ccode = Convert.ToInt64(lblccode.Text);
                fpiclass.insertpiitemsdata(li);
                li.id = Convert.ToInt64(lblid.Text);
                li.vnono1 = Convert.ToInt64(lblvno.Text);
                fpiclass.updateremainqtyinpurchasechallan(li);
                li.strpino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
                fpiclass.updatestrpinoinpcitems(li);
                //}
            }
            fpiclass.updateisused(li);

            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.invtype = drpinvtype.SelectedItem.Text;
            li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            fpiclass.deletepurchaseinvoiceledgeraa(li);
            Label lblccodee = (Label)gvpiitemlist.Rows[0].FindControl("lblccode");

            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drppurchaseac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "C";
            if (Convert.ToDouble(txtroundoff.Text) <= 0)
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            else
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drppurchaseac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "D";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text == string.Empty && txtcartage.Text == "")
            {
                txtcartage.Text = "0";
            }
            li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text);
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //}

            //3
            if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["cstdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "cst";
                li.istype1 = "D";
                li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //4
            if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["vatdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totvat";
                li.istype1 = "D";
                li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "D";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }


            //////Sales Invoice Ledger Entry
            //////1
            ////li.description = "";
            ////li.strvoucherno = "";
            ////li.debitcode = txtname.Text;
            ////li.creditcode = "0";
            ////li.description = "totalc";
            ////li.istype1 = "C";
            //////li.amount = Convert.ToDouble(txtbillamount.Text);
            ////li.amount = Convert.ToDouble(txttotbasicamount.Text);
            ////li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////li.voucherno = SessionMgt.voucherno;
            ////li.refid = 0;
            ////li.ccode = 0;
            ////li.istype = "PI";
            ////fpiclass.insertledgerdata(li);
            //////2
            ////li.strvoucherno = "";
            ////li.debitcode = "0";
            ////li.creditcode = txtname.Text;
            ////li.description = "totald";
            ////li.istype1 = "D";
            ////li.amount = Convert.ToDouble(txtbillamount.Text);
            ////li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////li.voucherno = SessionMgt.voucherno;
            ////li.refid = 0;
            ////li.ccode = 0;
            ////li.istype = "PI";
            ////fpiclass.insertledgerdata(li);
            //////3
            ////if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "cst";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotcst.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = SessionMgt.voucherno;
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.insertledgerdata(li);
            ////}
            //////4
            ////if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "totvat";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotvat.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = SessionMgt.voucherno;
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.insertledgerdata(li);
            ////}
            //////5
            ////if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "setvicetax";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txtserviceamt.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = SessionMgt.voucherno;
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.insertledgerdata(li);
            ////}
            //////6
            ////if (txttotaddtax.Text != string.Empty && txttotaddtax.Text != "0" && Convert.ToDouble(txttotaddtax.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "totaddtax";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotaddtax.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = SessionMgt.voucherno;
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.insertledgerdata(li);
            ////}
            //////7
            ////if (txtcartage.Text != string.Empty && txtcartage.Text != "0" && Convert.ToDouble(txtcartage.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "cartage";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txtcartage.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = SessionMgt.voucherno;
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.insertledgerdata(li);
            ////}
            //////

        }
        else
        {
            if (ViewState["pino"].ToString().Trim() != txtinvoiceno.Text.Trim() || ViewState["invtype"].ToString().Trim() != drpinvtype.SelectedItem.Text)
            {
                li.pino1 = Convert.ToInt64(txtinvoiceno.Text);
                li.pino = Convert.ToInt64(ViewState["pino"].ToString());
                li.strpino = ViewState["invtype"].ToString() + ViewState["pino"].ToString();
                li.strpino1 = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.invtype = ViewState["invtype"].ToString();
                li.invtype1 = drpinvtype.SelectedItem.Text;
                fpiclass.updatepimasterdataupdatepistring(li);
                fpiclass.updatepiitemsdataupdatepistring(li);
                fpiclass.updateledgerdataupdatestring(li);
                fpiclass.deletepimasterdatafrompinostring(li);
                fpiclass.deletepiitemsdatafrompinostring(li);
            }
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.invtype = drpinvtype.SelectedItem.Text;
            li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            fpiclass.deletepurchaseinvoiceledgeraa(li);
            li.invtype = drpinvtype.SelectedItem.Text;
            fpiclass.updatepimasterdatastring(li);

            //update all item data together
            for (int y = 0; y < gvpiitemlist.Rows.Count; y++)
            {
                li.pino = Convert.ToInt64(txtinvoiceno.Text);
                li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;


                Label lblid = (Label)gvpiitemlist.Rows[y].FindControl("lblid");
                Label lblqty = (Label)gvpiitemlist.Rows[y].FindControl("lblqty");
                TextBox txtgvqty1 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridqty");
                li.id = Convert.ToInt64(lblid.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                DataTable dtremain = new DataTable();
                dtremain = fpiclass.selectqtyremainusedfromscno(li);
                if (Convert.ToDouble(txtgvqty1.Text) > 0)
                {
                    if (dtremain.Rows.Count > 0)
                    {
                        //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                        li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                        fpiclass.updateremainqtyinpurchasechallan(li);


                        //update piitems data
                        //for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                        //{
                        li.pino = Convert.ToInt64(txtinvoiceno.Text);
                        li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        Label lblvno = (Label)gvpiitemlist.Rows[y].FindControl("lblvno");
                        Label lblitemname = (Label)gvpiitemlist.Rows[y].FindControl("lblitemname");
                        //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblrate = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridrate");
                        TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbltaxtype = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvpiitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvpiitemlist.Rows[y].FindControl("lblcstamt");
                        //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                        Label lblamount = (Label)gvpiitemlist.Rows[y].FindControl("lblamount");
                        Label lblccode = (Label)gvpiitemlist.Rows[y].FindControl("lblccode");
                        TextBox lbldesc1 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgvstockqty = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgvstockqty");
                        TextBox txtgridtaxdesc = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridtaxdesc");
                        li.id = Convert.ToInt64(lblid.Text);
                        li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                        li.itemname = lblitemname.Text;
                        if (txtgvstockqty.Text.Trim() != string.Empty)
                        {
                            li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        }
                        else
                        {
                            li.stockqty = 0;
                        }
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)
                        //{
                        //li.unit = lblunit.Text;
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.taxtype = lbltaxtype.Text;
                        if (lblvatp.Text != "")
                        {
                            li.vatp = Convert.ToDouble(lblvatp.Text);
                        }
                        else
                        {
                            li.vatp = 0;
                        }
                        if (lbladdvatp.Text != "")
                        {
                            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        }
                        else
                        {
                            li.addtaxp = 0;
                        }
                        if (lblcstp.Text != "")
                        {
                            li.cstp = Convert.ToDouble(lblcstp.Text);
                        }
                        else
                        {
                            li.cstp = 0;
                        }
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        if (lblbasicamount.Text != "")
                        {
                            li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                        }
                        else
                        {
                            li.basicamount = 0;
                        }
                        if (lblsctamt.Text != "")
                        {
                            li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                        }
                        else
                        {
                            li.cstamt = 0;
                        }
                        if (lblvatamt.Text != "")
                        {
                            li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                        }
                        else
                        {
                            li.vatamt = 0;
                        }
                        if (lbladdvatamt.Text != "")
                        {
                            li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                        }
                        else
                        {
                            li.addtaxamt = 0;
                        }
                        if (lblamount.Text != "")
                        {
                            li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                        }
                        else
                        {
                            li.amount = 0;
                        }
                        li.ccode = Convert.ToInt64(lblccode.Text);
                        li.taxdesc = txtgridtaxdesc.Text;
                        fpiclass.updatepiitemsdata(li);

                    }
                    else
                    {
                        li.pino = Convert.ToInt64(txtinvoiceno.Text);
                        li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                        Label lblvno = (Label)gvpiitemlist.Rows[y].FindControl("lblvno");
                        Label lblitemname = (Label)gvpiitemlist.Rows[y].FindControl("lblitemname");
                        //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblrate = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridrate");
                        TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbltaxtype = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridtaxtype");
                        Label lblvatp = (Label)gvpiitemlist.Rows[y].FindControl("lblvatp");
                        Label lblvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lblvatamt");
                        Label lbladdvatp = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatp");
                        Label lbladdvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatamt");
                        TextBox lblcstp = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridcstp");
                        Label lblsctamt = (Label)gvpiitemlist.Rows[y].FindControl("lblcstamt");
                        //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                        Label lblamount = (Label)gvpiitemlist.Rows[y].FindControl("lblamount");
                        Label lblccode = (Label)gvpiitemlist.Rows[y].FindControl("lblccode");
                        TextBox lbldesc1 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgvstockqty = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgvstockqty");
                        li.id = Convert.ToInt64(lblid.Text);
                        li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                        li.itemname = lblitemname.Text;
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)
                        //{
                        //li.unit = lblunit.Text;
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.taxtype = lbltaxtype.Text;
                        if (lblvatp.Text != "")
                        {
                            li.vatp = Convert.ToDouble(lblvatp.Text);
                        }
                        else
                        {
                            li.vatp = 0;
                        }
                        if (lbladdvatp.Text != "")
                        {
                            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                        }
                        else
                        {
                            li.addtaxp = 0;
                        }
                        if (lblcstp.Text != "")
                        {
                            li.cstp = Convert.ToDouble(lblcstp.Text);
                        }
                        else
                        {
                            li.cstp = 0;
                        }
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        if (lblbasicamount.Text != "")
                        {
                            li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                        }
                        else
                        {
                            li.basicamount = 0;
                        }
                        if (lblsctamt.Text != "")
                        {
                            li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                        }
                        else
                        {
                            li.cstamt = 0;
                        }
                        if (lblvatamt.Text != "")
                        {
                            li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                        }
                        else
                        {
                            li.vatamt = 0;
                        }
                        if (lbladdvatamt.Text != "")
                        {
                            li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                        }
                        else
                        {
                            li.addtaxamt = 0;
                        }
                        if (lblamount.Text != "")
                        {
                            li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                        }
                        else
                        {
                            li.amount = 0;
                        }
                        li.ccode = Convert.ToInt64(lblccode.Text);
                        fpiclass.updatepiitemsdata(li);
                    }
                }
                else
                {
                    li.pino = Convert.ToInt64(txtinvoiceno.Text);
                    li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    Label lblvno = (Label)gvpiitemlist.Rows[y].FindControl("lblvno");
                    Label lblitemname = (Label)gvpiitemlist.Rows[y].FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)gvpiitemlist.Rows[y].FindControl("lblvatp");
                    Label lblvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lblvatamt");
                    Label lbladdvatp = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)gvpiitemlist.Rows[y].FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgridcstp");
                    Label lblsctamt = (Label)gvpiitemlist.Rows[y].FindControl("lblcstamt");
                    //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                    Label lblamount = (Label)gvpiitemlist.Rows[y].FindControl("lblamount");
                    Label lblccode = (Label)gvpiitemlist.Rows[y].FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)gvpiitemlist.Rows[y].FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)gvpiitemlist.Rows[y].FindControl("txtgvstockqty");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.itemname = lblitemname.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    //li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    if (lblbasicamount.Text != "")
                    {
                        li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    }
                    else
                    {
                        li.basicamount = 0;
                    }
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    fpiclass.updatepiitemsdata(li);
                }
            }
            //

            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            fpiclass.updatepiitemsdatedataupdatepistring(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strpino + " Purchase Invoice Updated.";
            faclass.insertactivity(li);
            li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;


            Label lblccodee = (Label)gvpiitemlist.Rows[0].FindControl("lblccode");

            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drppurchaseac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "C";
            if (Convert.ToDouble(txtroundoff.Text) <= 0)
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            else
            {
                li.amount = Convert.ToDouble(txtbillamount.Text);
            }
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drppurchaseac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "D";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text == string.Empty && txtcartage.Text == "")
            {
                txtcartage.Text = "0";
            }
            li.amount = Math.Round(Convert.ToDouble(txttotbasicamount.Text), 2) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtroundoff.Text);
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //}

            //3
            if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["cstdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "cst";
                li.istype1 = "D";
                li.amount = Math.Round(Convert.ToDouble(txttotcst.Text), 2);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //4
            if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["vatdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totvat";
                li.istype1 = "D";
                li.amount = Math.Round((Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text)), 2);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "D";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }


            //for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            //{
            //    li.pino = Convert.ToInt64(txtinvoiceno.Text);
            //    Label lblid = (Label)gvpiitemlist.Rows[c].FindControl("lblid");
            //    Label lblvno = (Label)gvpiitemlist.Rows[c].FindControl("lblvno");
            //    Label lblitemname = (Label)gvpiitemlist.Rows[c].FindControl("lblitemname");
            //    TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
            //    TextBox lblrate = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridrate");
            //    TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
            //    TextBox lbltaxtype = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridtaxtype");
            //    Label lblvatp = (Label)gvpiitemlist.Rows[c].FindControl("lblvatp");
            //    Label lblvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
            //    Label lbladdvatp = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatp");
            //    Label lbladdvatamt = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
            //    TextBox lblcstp = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridcstp");
            //    Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
            //    //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
            //    Label lblamount = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
            //    //Label lblccode = (Label)gvpiitemlist.Rows[c].FindControl("lblccode");
            //    TextBox lbldesc1 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc1");
            //    TextBox lbldesc2 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc2");
            //    TextBox lbldesc3 = (TextBox)gvpiitemlist.Rows[c].FindControl("txtdesc3");
            //    li.id = Convert.ToInt64(lblid.Text);
            //    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
            //    li.itemname = lblitemname.Text;
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    if (li.qty > 0)
            //    {
            //        //li.unit = lblunit.Text;
            //        li.rate = Convert.ToDouble(lblrate.Text);
            //        li.taxtype = lbltaxtype.Text;
            //        if (lblvatp.Text != "")
            //        {
            //            li.vatp = Convert.ToDouble(lblvatp.Text);
            //        }
            //        else
            //        {
            //            li.vatp = 0;
            //        }
            //        if (lbladdvatp.Text != "")
            //        {
            //            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
            //        }
            //        else
            //        {
            //            li.addtaxp = 0;
            //        }
            //        if (lblcstp.Text != "")
            //        {
            //            li.cstp = Convert.ToDouble(lblcstp.Text);
            //        }
            //        else
            //        {
            //            li.cstp = 0;
            //        }
            //        li.descr1 = lbldesc1.Text;
            //        li.descr2 = lbldesc2.Text;
            //        li.descr3 = lbldesc3.Text;
            //        if (lblbasicamount.Text != "")
            //        {
            //            li.basicamount = Convert.ToDouble(lblbasicamount.Text);
            //        }
            //        else
            //        {
            //            li.basicamount = 0;
            //        }
            //        if (lblsctamt.Text != "")
            //        {
            //            li.cstamt = Convert.ToDouble(lblsctamt.Text);
            //        }
            //        else
            //        {
            //            li.cstamt = 0;
            //        }
            //        if (lblvatamt.Text != "")
            //        {
            //            li.vatamt = Convert.ToDouble(lblvatamt.Text);
            //        }
            //        else
            //        {
            //            li.vatamt = 0;
            //        }
            //        if (lbladdvatamt.Text != "")
            //        {
            //            li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
            //        }
            //        else
            //        {
            //            li.addtaxamt = 0;
            //        }
            //        if (lblamount.Text != "")
            //        {
            //            li.amount = Convert.ToDouble(lblamount.Text);
            //        }
            //        else
            //        {
            //            li.amount = 0;
            //        }
            //        //li.ccode = Convert.ToInt64(lblccode.Text);
            //        fpiclass.updatepiitemsdata(li);
            //    }
            //}

            //////Purchase Invoice Ledger Entry
            //////1

            ////li.strvoucherno = "";
            ////li.debitcode = txtname.Text;
            ////li.creditcode = "0";
            ////li.description = "totalc";
            ////li.istype1 = "C";
            //////li.amount = Convert.ToDouble(txtbillamount.Text);
            ////li.amount = Convert.ToDouble(txttotbasicamount.Text);
            ////li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////li.refid = 0;
            ////li.ccode = 0;
            ////li.istype = "PI";
            ////fpiclass.updateledgerdata(li);
            //////2
            ////li.strvoucherno = "";
            ////li.debitcode = "0";
            ////li.creditcode = txtname.Text;
            ////li.description = "totald";
            ////li.istype1 = "D";
            ////li.amount = Convert.ToDouble(txtbillamount.Text);
            ////li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////li.refid = 0;
            ////li.ccode = 0;
            ////li.istype = "PI";
            ////fpiclass.updateledgerdata(li);
            //////3
            ////if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "cst";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotcst.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.updateledgerdata(li);
            ////}
            //////4
            ////if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "totvat";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotvat.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.updateledgerdata(li);
            ////}
            //////5
            ////if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "setvicetax";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txtserviceamt.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.updateledgerdata(li);
            ////}
            //////6
            ////if (txttotaddtax.Text != string.Empty && txttotaddtax.Text != "0" && Convert.ToDouble(txttotaddtax.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "totaddtax";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txttotaddtax.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.updateledgerdata(li);
            ////}
            //////7
            ////if (txtcartage.Text != string.Empty && txtcartage.Text != "0" && Convert.ToDouble(txtcartage.Text) > 0)
            ////{
            ////    li.strvoucherno = "";
            ////    li.debitcode = "0";
            ////    li.creditcode = txtname.Text;
            ////    li.description = "cartage";
            ////    li.istype1 = "C";
            ////    li.amount = Convert.ToDouble(txtcartage.Text);
            ////    li.voucherdate = Convert.ToDateTime(txtpidate.Text);
            ////    li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            ////    li.refid = 0;
            ////    li.ccode = 0;
            ////    li.istype = "PI";
            ////    fpiclass.updateledgerdata(li);
            ////}
        }
        li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        fpiclass.updatemonyrinpiitemsstring(li);

        li.strpino = drpinvtype.SelectedItem.Text + txtinvoiceno.Text;
        li.stringpcno = txtsrtringpcno.Text;
        string[] words = li.stringpcno.Split(',');
        foreach (string word in words)
        {
            li.pcno = Convert.ToInt64(word.ToString());
            fpiclass.updatestrpinoinpcmaster(li);
            fpiclass.updatestrpinoinpcmastera(li);
            fpiclass.updatepcstatusforallchno(li);
        }
        if (Request["mode"].ToString() != "ledger")
        {
            Response.Redirect("~/PurchaseInvoiceList.aspx?pagename=PurchaseInvoiceList");
        }
        else
        {
            object refUrl = ViewState["RefUrl"];
            string ch = (string)refUrl;
            ch = ch.Split('?')[0] + "?acname=" + SessionMgt.acname + "&ccode=" + SessionMgt.ccode + "&fromdate=" + SessionMgt.fromdate + "&todate=" + SessionMgt.todate + "";
            SessionMgt.acname = "";
            SessionMgt.ccode = 0;
            SessionMgt.fromdate = "";
            SessionMgt.todate = "";
            if (refUrl != null)
                Response.Redirect(ch);
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemspuin"];
            DataRow dr = dt.NewRow();
            dr["id"] = dt.Rows.Count + 1;
            dr["vno"] = 0;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtbillqty.Text;
            dr["stockqty"] = txtstockqty.Text;
            dr["qtyremain"] = txtbillqty.Text;
            dr["qtyused"] = 0;
            dr["rate"] = txtrate.Text;
            dr["basicamount"] = txtbasicamt.Text;
            dr["taxtype"] = txttaxtype.Text;
            dr["taxdesc"] = drptaxdesc.SelectedItem.Text;
            dr["vatp"] = txtgvvat.Text;
            dr["vatamt"] = (Math.Round(Convert.ToDouble(txtvat.Text))).ToString();
            dr["addtaxp"] = txtgvadtax.Text;
            dr["addtaxamt"] = (Math.Round(Convert.ToDouble(txtaddtax.Text))).ToString();
            dr["cstp"] = txtgvcst.Text;
            dr["cstamt"] = (Math.Round(Convert.ToDouble(txtcstamount.Text))).ToString();
            //dr["unit"] = txtunit.Text;
            dr["amount"] = txtamount.Text;
            dr["ccode"] = txtcode.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemspuin"] = dt;
            this.bindgrid();
            count1();
        }
        else
        {
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.vnono = Convert.ToInt64(txtinvoiceno.Text);
            li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.stockqty = Convert.ToDouble(txtstockqty.Text);
            li.qty = Convert.ToDouble(txtbillqty.Text);
            li.qtyremain = li.qty;
            li.qtyused = 0;
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.taxtype = txttaxtype.Text;
            li.taxdesc = drptaxdesc.SelectedItem.Text;
            li.vatp = Convert.ToDouble(txtgvvat.Text);
            li.vatamt = Math.Round(Convert.ToDouble(txtvat.Text));
            li.addtaxp = Convert.ToDouble(txtgvadtax.Text);
            li.addtaxamt = Math.Round(Convert.ToDouble(txtaddtax.Text));
            li.cstp = Convert.ToDouble(txtgvcst.Text);
            li.cstamt = Math.Round(Convert.ToDouble(txtcstamount.Text));
            //li.unit = txtunit.Text;
            li.amount = Convert.ToDouble(txtamount.Text);
            li.ccode = Convert.ToInt64(txtcode.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.taxdesc = drptaxdesc.SelectedItem.Text;
            li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.cmon = Convert.ToInt16(txtpidate.Text.Split('-')[1]);
            if (li.cmon == 1)
            {
                li.cmonname = "JANUARY";
            }
            else if (li.cmon == 2)
            {
                li.cmonname = "FEBRUARY";
            }
            else if (li.cmon == 3)
            {
                li.cmonname = "MARCH";
            }
            else if (li.cmon == 4)
            {
                li.cmonname = "APRIL";
            }
            else if (li.cmon == 5)
            {
                li.cmonname = "MAY";
            }
            else if (li.cmon == 6)
            {
                li.cmonname = "JUNE";
            }
            else if (li.cmon == 7)
            {
                li.cmonname = "JULY";
            }
            else if (li.cmon == 8)
            {
                li.cmonname = "AUGUST";
            }
            else if (li.cmon == 9)
            {
                li.cmonname = "SEPTEMBER";
            }
            else if (li.cmon == 10)
            {
                li.cmonname = "OCTOBER";
            }
            else if (li.cmon == 11)
            {
                li.cmonname = "NOVEMBER";
            }
            else
            {
                li.cmonname = "DECEMBER";
            }
            li.monyr = li.cmonname + "-" + txtpidate.Text.Split('-')[2];
            fpiclass.insertpiitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strpino + " Purchase Invoice Item Inserted.";
            faclass.insertactivity(li);
            Label lblccodee = (Label)gvpiitemlist.Rows[0].FindControl("lblccode");

            //1
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drpacname.SelectedItem.Text;
            li.creditcode = drppurchaseac.SelectedItem.Text;
            li.description = "totald";
            li.istype1 = "D";
            li.amount = Convert.ToDouble(txtbillamount.Text);
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //2
            li.description = "";
            li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
            li.debitcode = drppurchaseac.SelectedItem.Text;
            li.creditcode = drpacname.SelectedItem.Text;
            li.description = "totalc";
            li.istype1 = "C";
            //li.amount = Convert.ToDouble(txtbillamount.Text);
            if (txtcartage.Text != string.Empty && txtcartage.Text != "")
            {
                txtcartage.Text = "0";
            }
            li.amount = Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txtcartage.Text);
            li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //li.voucherno = SessionMgt.voucherno;
            li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
            li.refid = 0;
            li.ccode = Convert.ToInt64(lblccodee.Text);
            li.istype = "PI";
            fpiclass.insertledgerdata(li);
            //}

            //3
            if (txttotcst.Text != string.Empty && txttotcst.Text != "0" && Convert.ToDouble(txttotcst.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["cstdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "cst";
                li.istype1 = "C";
                li.amount = Math.Round(Convert.ToDouble(txttotcst.Text));
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //4
            if (txttotvat.Text != string.Empty && txttotvat.Text != "0" && Convert.ToDouble(txttotvat.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["vatdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "totvat";
                li.istype1 = "C";
                li.amount = Math.Round(Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text));
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }
            //5
            if (txtserviceamt.Text != string.Empty && txtserviceamt.Text != "0" && Convert.ToDouble(txtserviceamt.Text) > 0)
            {
                li.strvoucherno = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                li.debitcode = ViewState["servicetaxdesc"].ToString();
                li.creditcode = drpacname.SelectedItem.Text;
                li.description = "setvicetax";
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtserviceamt.Text);
                li.voucherdate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.voucherno = SessionMgt.voucherno;
                li.voucherno = Convert.ToInt64(txtinvoiceno.Text);
                li.refid = 0;
                li.ccode = Convert.ToInt64(lblccodee.Text);
                li.istype = "PI";
                fpiclass.insertledgerdata(li);
            }

            li.sino = Convert.ToInt64(txtinvoiceno.Text);
            DataTable dtdata = new DataTable();
            dtdata = fpiclass.selectallpiitemsfrompinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpiitemlist.Visible = true;
                gvpiitemlist.DataSource = dtdata;
                gvpiitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
                hideimage();
            }
            else
            {
                gvpiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
            count1();
        }
        countgv();
        fillitemnamedrop();
        txtbillqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txttaxtype.Text = string.Empty;
        txtgvvat.Text = string.Empty;
        txtgvadtax.Text = string.Empty;
        txtgvcst.Text = string.Empty;
        txtdescription1.Text = string.Empty;
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        txtvat.Text = string.Empty;
        txtaddtax.Text = string.Empty;
        txtcstamount.Text = string.Empty;
        txtamount.Text = string.Empty;
        hideimage();
        Page.SetFocus(drpitemname);
    }
    protected void txtbillqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtstockqty);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvadtax);
    }
    protected void txtgvadtax_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvcst);
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        var cc = txtgvcst.Text.IndexOf("-");
        if (cc != -1)
        {
            txtgvcst.Text = "0";
        }
        count1();
        Page.SetFocus(txtcode);
    }
    protected void txtvat_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtaddtax_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txttotbasicamount_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txttotcst);
    }
    protected void txttotcst_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txttotvat);
    }
    protected void txttotvat_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtservicep);
    }
    protected void txtserviceamt_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtroundoff);
    }
    protected void txtroundoff_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage + roundoff).ToString();
        Page.SetFocus(txttotaddtax);
    }
    protected void txttotaddtax_TextChanged(object sender, EventArgs e)
    {
        //count1();
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtcartage);
    }
    protected void txtcartage_TextChanged(object sender, EventArgs e)
    {
        //countgv();
        double basicamount = 0;
        double vat = 0;
        double addtax = 0;
        double cst = 0;
        double servicetaxamt = 0;
        double cartage = 0;
        double roundoff = 0;
        if (txttotbasicamount.Text.Trim() != string.Empty)
        {
            basicamount = Convert.ToDouble(txttotbasicamount.Text);
        }
        if (txttotvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txttotvat.Text);
        }
        if (txttotaddtax.Text.Trim() != string.Empty)
        {
            addtax = Convert.ToDouble(txttotaddtax.Text);
        }
        if (txttotcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txttotcst.Text);
        }
        if (txtserviceamt.Text.Trim() != string.Empty)
        {
            servicetaxamt = Convert.ToDouble(txtserviceamt.Text);
        }
        if (txtcartage.Text.Trim() != string.Empty)
        {
            cartage = Convert.ToDouble(txtcartage.Text);
        }
        if (txtroundoff.Text.Trim() != string.Empty)
        {
            roundoff = Convert.ToDouble(txtroundoff.Text);
        }
        txtbillamount.Text = (basicamount + vat + addtax + cst + servicetaxamt + cartage).ToString();
        //double bamt = Convert.ToDouble(txtbillamount.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        double bamt = Convert.ToDouble(txtbillamount.Text);
        double round1 = bamt - Math.Round(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
        {
            if (round1 > 0)
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = Math.Round(round1, 2).ToString();
            }
        }
        else
        {
            if (round1 > 0)
            {
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
            else
            {
                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
            }
        }
        roundoff = Convert.ToDouble(txtroundoff.Text);
        txtbillamount.Text = (Math.Round(bamt)).ToString();
        Page.SetFocus(txtbillamount);
    }
    protected void txtchallanno_TextChanged(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        if (txtchallanno.Text.Trim() != string.Empty)
        {
            li.pcno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpiclass.selectpcdatefrompcno(li);
            txtchallandate.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pcno = 0;
        }
        if (txtchallanno1.Text.Trim() != string.Empty)
        {
            li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpiclass.selectpcdatefrompcno1(li);
            txtchallandate1.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pcno1 = 0;
        }
        li.remarks = li.pcno + "," + li.pcno1;
        DataTable dtdt = new DataTable();
        dtdt = fpiclass.selectallmasterdatafrompcnotexhchange(li);
        if (dtdt.Rows.Count > 0)
        {
            drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
        }
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallitemdatafrompcnotexhchange(li);
        if (dtdata.Rows.Count > 0)
        {
            //if (dtsoitem != null)
            //{
            //    if (dtsoitem.Rows.Count > 0)
            //    {
            //        dtdata.Merge(dtsoitem);
            //    }
            //}
            //lblempty.Visible = false;
            //gvpiitemlist.Visible = true;
            //gvpiitemlist.DataSource = dtdata;
            //gvpiitemlist.DataBind();
            //Session["dtpitems"] = dtdata;
            DataTable dt = (DataTable)Session["dtpitemspuin"];
            dt.Clear();
            for (int d = 0; d < dtdata.Rows.Count; d++)
            {
                DataRow dr = dt.NewRow();
                //dr["id"] = dt.Rows.Count + 1;
                dr["id"] = dtdata.Rows[d]["id"].ToString();
                dr["vno"] = dtdata.Rows[d]["pcno"].ToString();
                dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                dr["taxtype"] = "";
                dr["vatp"] = 0;
                dr["vatamt"] = 0;
                dr["addtaxp"] = 0;
                dr["addtaxamt"] = 0;
                dr["cstp"] = 0;
                dr["cstamt"] = 0;
                //dr["unit"] = txtunit.Text;
                dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                //dr["ccode"] = txtcode.Text;
                dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemspuin"] = dt;
            this.bindgrid();
            countgv();
        }
        else
        {
            gvpiitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            txttotbasicamount.Text = "0";
            txttotcst.Text = "0";
            txttotvat.Text = "0";
            txtservicep.Text = "0";
            txtserviceamt.Text = "0";
            txtroundoff.Text = "0";
            txttotaddtax.Text = "0";
            txtcartage.Text = "0";
            txtbillamount.Text = "0";
            fillacnamedrop();
        }
    }

    protected void txtchallanno1_TextChanged(object sender, EventArgs e)
    {

        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        if (txtchallanno.Text.Trim() != string.Empty)
        {
            li.pcno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpiclass.selectpcdatefrompcno(li);
            txtchallandate.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pcno = 0;
        }
        if (txtchallanno1.Text.Trim() != string.Empty)
        {
            li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpiclass.selectpcdatefrompcno1(li);
            txtchallandate1.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pcno1 = 0;
        }
        li.remarks = li.pcno + "," + li.pcno1;
        DataTable dtdt = new DataTable();
        dtdt = fpiclass.selectallmasterdatafrompcnotexhchange(li);
        if (dtdt.Rows.Count > 0)
        {
            drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
        }
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallitemdatafrompcnotexhchange(li);
        if (dtdata.Rows.Count > 0)
        {
            //if (dtsoitem != null)
            //{
            //    if (dtsoitem.Rows.Count > 0)
            //    {
            //        dtdata.Merge(dtsoitem);
            //    }
            //}
            //lblempty.Visible = false;
            //gvpiitemlist.Visible = true;
            //gvpiitemlist.DataSource = dtdata;
            //gvpiitemlist.DataBind();
            //Session["dtpitems"] = dtdata;
            DataTable dt = (DataTable)Session["dtpitemspuin"];
            dt.Clear();
            for (int d = 0; d < dtdata.Rows.Count; d++)
            {
                DataRow dr = dt.NewRow();
                //dr["id"] = dt.Rows.Count + 1;
                dr["id"] = dtdata.Rows[d]["id"].ToString();
                dr["vno"] = dtdata.Rows[d]["pcno"].ToString();
                dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                dr["taxtype"] = "";
                dr["vatp"] = 0;
                dr["vatamt"] = 0;
                dr["addtaxp"] = 0;
                dr["addtaxamt"] = 0;
                dr["cstp"] = 0;
                dr["cstamt"] = 0;
                //dr["unit"] = txtunit.Text;
                dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                //dr["ccode"] = txtcode.Text;
                dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemspuin"] = dt;
            this.bindgrid();
            countgv();
        }
        else
        {
            gvpiitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            txttotbasicamount.Text = "0";
            txttotcst.Text = "0";
            txttotvat.Text = "0";
            txtservicep.Text = "0";
            txtserviceamt.Text = "0";
            txtroundoff.Text = "0";
            txttotaddtax.Text = "0";
            txtcartage.Text = "0";
            txtbillamount.Text = "0";
            fillacnamedrop();
        }

        //////DataTable dt = (DataTable)Session["dtpitems"];
        //////dt.Clear();
        //////li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //////DataTable dtdt = new DataTable();
        //////dtdt = fpiclass.selectallmasterdatafrompcnotexhchange(li);
        //////if (dtdt.Rows.Count > 0)
        //////{
        //////    txtname.Text = dtdt.Rows[0]["acname"].ToString();
        //////}
        //////if (txtchallanno.Text.Trim() != string.Empty)
        //////{
        //////    li.pcno = Convert.ToInt64(txtchallanno.Text);
        //////    DataTable dtdate = new DataTable();
        //////    dtdate = fpiclass.selectpcdatefrompcno(li);
        //////    txtchallandate.Text = dtdate.Rows[0]["cdate"].ToString();
        //////    DataTable dtdata = new DataTable();
        //////    dtdata = fpiclass.selectallitemdatafrompcnotexhchangeone(li);
        //////    if (dtdata.Rows.Count > 0)
        //////    {
        //////        //if (dtsoitem != null)
        //////        //{
        //////        //    if (dtsoitem.Rows.Count > 0)
        //////        //    {
        //////        //        dtdata.Merge(dtsoitem);
        //////        //    }
        //////        //}
        //////        //lblempty.Visible = false;
        //////        //gvpiitemlist.Visible = true;
        //////        //gvpiitemlist.DataSource = dtdata;
        //////        //gvpiitemlist.DataBind();
        //////        //Session["dtpitems"] = dtdata;                
        //////        for (int d = 0; d < dtdata.Rows.Count; d++)
        //////        {
        //////            DataRow dr = dt.NewRow();
        //////            //dr["id"] = dt.Rows.Count + 1;
        //////            dr["id"] = dtdata.Rows[d]["id"].ToString();
        //////            dr["vno"] = dtdata.Rows[d]["pcno"].ToString();
        //////            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
        //////            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
        //////            dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
        //////            dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
        //////            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
        //////            dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
        //////            dr["taxtype"] = "";
        //////            dr["vatp"] = 0;
        //////            dr["vatamt"] = 0;
        //////            dr["addtaxp"] = 0;
        //////            dr["addtaxamt"] = 0;
        //////            dr["cstp"] = 0;
        //////            dr["cstamt"] = 0;
        //////            //dr["unit"] = txtunit.Text;
        //////            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
        //////            //dr["ccode"] = txtcode.Text;
        //////            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
        //////            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
        //////            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
        //////            dt.Rows.Add(dr);
        //////        }
        //////        //Session["dtpitems"] = dt;
        //////        //this.bindgrid();
        //////        //countgv();
        //////    }
        //////    //else
        //////    //{
        //////    //    gvpiitemlist.Visible = false;
        //////    //    lblempty.Visible = true;
        //////    //    lblempty.Text = "No Items Found.";
        //////    //    txttotbasicamount.Text = "0";
        //////    //    txttotcst.Text = "0";
        //////    //    txttotvat.Text = "0";
        //////    //    txtservicep.Text = "0";
        //////    //    txtserviceamt.Text = "0";
        //////    //    txtroundoff.Text = "0";
        //////    //    txttotaddtax.Text = "0";
        //////    //    txtcartage.Text = "0";
        //////    //    txtbillamount.Text = "0";
        //////    //    txtname.Text = string.Empty;
        //////    //}
        //////}
        //////else
        //////{
        //////    li.pcno = 0;
        //////}
        //////if (txtchallanno1.Text.Trim() != string.Empty)
        //////{
        //////    li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
        //////    DataTable dtdate = new DataTable();
        //////    dtdate = fpiclass.selectpcdatefrompcno1(li);
        //////    txtchallandate1.Text = dtdate.Rows[0]["cdate"].ToString();
        //////    DataTable dtdata = new DataTable();
        //////    dtdata = fpiclass.selectallitemdatafrompcnotexhchangetwo(li);
        //////    if (dtdata.Rows.Count > 0)
        //////    {
        //////        //if (dtsoitem != null)
        //////        //{
        //////        //    if (dtsoitem.Rows.Count > 0)
        //////        //    {
        //////        //        dtdata.Merge(dtsoitem);
        //////        //    }
        //////        //}
        //////        //lblempty.Visible = false;
        //////        //gvpiitemlist.Visible = true;
        //////        //gvpiitemlist.DataSource = dtdata;
        //////        //gvpiitemlist.DataBind();
        //////        //Session["dtpitems"] = dtdata;                
        //////        for (int d = 0; d < dtdata.Rows.Count; d++)
        //////        {
        //////            DataRow dr = dt.NewRow();
        //////            //dr["id"] = dt.Rows.Count + 1;
        //////            dr["id"] = dtdata.Rows[d]["id"].ToString();
        //////            dr["vno"] = dtdata.Rows[d]["pcno1"].ToString();
        //////            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
        //////            dr["qty"] = dtdata.Rows[d]["qty"].ToString();
        //////            dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
        //////            dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
        //////            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
        //////            dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
        //////            dr["taxtype"] = "";
        //////            dr["vatp"] = 0;
        //////            dr["vatamt"] = 0;
        //////            dr["addtaxp"] = 0;
        //////            dr["addtaxamt"] = 0;
        //////            dr["cstp"] = 0;
        //////            dr["cstamt"] = 0;
        //////            //dr["unit"] = txtunit.Text;
        //////            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
        //////            //dr["ccode"] = txtcode.Text;
        //////            dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
        //////            dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
        //////            dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
        //////            dt.Rows.Add(dr);
        //////        }
        //////        //Session["dtpitems"] = dt;
        //////        //this.bindgrid();
        //////        //countgv();
        //////    }
        //////    //else
        //////    //{
        //////    //    gvpiitemlist.Visible = false;
        //////    //    lblempty.Visible = true;
        //////    //    lblempty.Text = "No Items Found.";
        //////    //    txttotbasicamount.Text = "0";
        //////    //    txttotcst.Text = "0";
        //////    //    txttotvat.Text = "0";
        //////    //    txtservicep.Text = "0";
        //////    //    txtserviceamt.Text = "0";
        //////    //    txtroundoff.Text = "0";
        //////    //    txttotaddtax.Text = "0";
        //////    //    txtcartage.Text = "0";
        //////    //    txtbillamount.Text = "0";
        //////    //    txtname.Text = string.Empty;
        //////    //}
        //////}
        //////else
        //////{
        //////    li.pcno1 = 0;
        //////}
        //////if (dt.Rows.Count > 0)
        //////{
        //////    Session["dtpitems"] = dt;
        //////    this.bindgrid();
        //////    countgv();
        //////}
        //////else
        //////{
        //////    gvpiitemlist.Visible = false;
        //////    lblempty.Visible = true;
        //////    lblempty.Text = "No Items Found.";
        //////    txttotbasicamount.Text = "0";
        //////    txttotcst.Text = "0";
        //////    txttotvat.Text = "0";
        //////    txtservicep.Text = "0";
        //////    txtserviceamt.Text = "0";
        //////    txtroundoff.Text = "0";
        //////    txttotaddtax.Text = "0";
        //////    txtcartage.Text = "0";
        //////    txtbillamount.Text = "0";
        //////    txtname.Text = string.Empty;
        //////}
        ////////li.remarks = li.pcno + "," + li.pcno1;



    }

    protected void txtgvqty_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblvid = (Label)currentRow.FindControl("lblvid");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                if (lblvid.Text != "0")
                {
                    //if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
                    //{
                    txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                    txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
                    double vatp = 0;
                    double addvatp = 0;
                    double cstp = 0;
                    double vatamt = 0;
                    double addvatamt = 0;
                    double cstamt = 0;
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                    if (lbltaxtype.Text != string.Empty)
                    {
                        var cc = lbltaxtype.Text.IndexOf("-");
                        if (cc != -1)
                        {
                            li.typename = lbltaxtype.Text;
                            vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                            addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                            lblvatp.Text = vatp.ToString();
                            lbladdvatp.Text = addvatp.ToString();
                            DataTable dtdtax = new DataTable();
                            dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                            if (dtdtax.Rows.Count > 0)
                            {
                                txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                            }
                            else
                            {
                                txtgridcstp.Text = "0";
                            }
                        }
                    }
                    if (lblvatp.Text != string.Empty)
                    {
                        vatp = Convert.ToDouble(lblvatp.Text);
                        lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblvatamt.Text = "0";
                        lblvatp.Text = "0";
                    }
                    if (lbladdvatp.Text != string.Empty)
                    {
                        addvatp = Convert.ToDouble(lbladdvatp.Text);
                        lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lbladdvatp.Text = "0";
                        lbladdvatamt.Text = "0";
                    }
                    if (txtgridcstp.Text != string.Empty)
                    {
                        cstp = Convert.ToDouble(txtgridcstp.Text);
                        lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblcstp.Text = "0";
                        lblcstamt.Text = "0";
                    }
                    vatamt = Convert.ToDouble(lblvatamt.Text);
                    addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                    cstamt = Convert.ToDouble(lblcstamt.Text);
                    lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                    if (gvpiitemlist.Rows.Count > 0)
                    {
                        double totbasicamt = 0;
                        double totcst = 0;
                        double totvat = 0;
                        double totaddvat = 0;
                        double totamount = 0;
                        for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                        {
                            TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                            Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                            Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                            Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                            Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                            if (lblsctamt.Text != string.Empty)
                            {
                                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                            }
                            if (lblvatamt1.Text != string.Empty)
                            {
                                totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                            }
                            if (lbladdvatamt1.Text != string.Empty)
                            {
                                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                            }
                            if (lblamount1.Text != string.Empty)
                            {
                                totamount = totamount + Convert.ToDouble(lblamount1.Text);
                            }
                        }
                        txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                        txttotvat.Text = Math.Round(totvat, 2).ToString();
                        txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                        txttotcst.Text = Math.Round(totcst, 2).ToString();
                        double cartage = 0;
                        if (txtcartage.Text.Trim() != string.Empty)
                        {
                            cartage = Convert.ToDouble(txtcartage.Text);
                        }
                        else
                        {
                            txtcartage.Text = "0";
                        }
                        double servp = 0;
                        double servamt = 0;
                        double roundoff = 0;
                        if (txtservicep.Text.Trim() != string.Empty)
                        {
                            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                        }
                        else
                        {
                            txtservicep.Text = "0";
                            txtserviceamt.Text = "0";
                        }
                        if (txtroundoff.Text.Trim() != string.Empty)
                        {
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                        }
                        else
                        {
                            txtroundoff.Text = "0";
                        }
                        double serant = 0;
                        if (txtserviceamt.Text.Trim() != string.Empty)
                        {
                            serant = Convert.ToDouble(txtserviceamt.Text);
                        }
                        //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                        double bamt = Convert.ToDouble(txtbillamount.Text);
                        double round1 = bamt - Math.Round(bamt);
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                        {
                            if (round1 > 0)
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = Math.Round(round1, 2).ToString();
                            }
                        }
                        else
                        {
                            if (round1 > 0)
                            {
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                        }
                        //if (round1 > 0.5)
                        //{
                        //    //txtroundoff.Text = Math.Round((1 - round1), 2).ToString();
                        //    txtroundoff.Text = ((-1) * (Math.Round((1 - round1), 2))).ToString();
                        //}
                        //else
                        //{
                        //    txtroundoff.Text = Math.Round(round1 * -1, 2).ToString();
                        //}
                        roundoff = Convert.ToDouble(txtroundoff.Text);
                        txtbillamount.Text = (Math.Round(bamt)).ToString();
                    }
                    else
                    {
                        txttotbasicamount.Text = "0";
                        txttotvat.Text = "0";
                        txttotaddtax.Text = "0";
                        txttotcst.Text = "0";
                        txtbillamount.Text = "0";
                    }
                    //}
                    //else
                    //{
                    //    txtgvqty1.Text = lblqtyremain.Text;
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                    //    return;
                    //}
                }
                else
                {
                    txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                    txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
                    double vatp = 0;
                    double addvatp = 0;
                    double cstp = 0;
                    double vatamt = 0;
                    double addvatamt = 0;
                    double cstamt = 0;
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                    if (lbltaxtype.Text != string.Empty)
                    {
                        var cc = lbltaxtype.Text.IndexOf("-");
                        if (cc != -1)
                        {
                            li.typename = lbltaxtype.Text;
                            vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                            addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                            lblvatp.Text = vatp.ToString();
                            lbladdvatp.Text = addvatp.ToString();
                            DataTable dtdtax = new DataTable();
                            dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                            if (dtdtax.Rows.Count > 0)
                            {
                                txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                            }
                            else
                            {
                                txtgridcstp.Text = "0";
                            }
                        }
                    }
                    if (lblvatp.Text != string.Empty)
                    {
                        vatp = Convert.ToDouble(lblvatp.Text);
                        lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblvatamt.Text = "0";
                        lblvatp.Text = "0";
                    }
                    if (lbladdvatp.Text != string.Empty)
                    {
                        addvatp = Convert.ToDouble(lbladdvatp.Text);
                        lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lbladdvatp.Text = "0";
                        lbladdvatamt.Text = "0";
                    }
                    if (txtgridcstp.Text != string.Empty)
                    {
                        cstp = Convert.ToDouble(txtgridcstp.Text);
                        lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblcstp.Text = "0";
                        lblcstamt.Text = "0";
                    }
                    vatamt = Convert.ToDouble(lblvatamt.Text);
                    addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                    cstamt = Convert.ToDouble(lblcstamt.Text);
                    lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                    if (gvpiitemlist.Rows.Count > 0)
                    {
                        double totbasicamt = 0;
                        double totcst = 0;
                        double totvat = 0;
                        double totaddvat = 0;
                        double totamount = 0;
                        for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                        {
                            TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                            Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                            Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                            Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                            Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                            if (lblsctamt.Text != string.Empty)
                            {
                                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                            }
                            if (lblvatamt1.Text != string.Empty)
                            {
                                totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                            }
                            if (lbladdvatamt1.Text != string.Empty)
                            {
                                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                            }
                            if (lblamount1.Text != string.Empty)
                            {
                                totamount = totamount + Convert.ToDouble(lblamount1.Text);
                            }
                        }
                        txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                        txttotvat.Text = Math.Round(totvat, 2).ToString();
                        txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                        txttotcst.Text = Math.Round(totcst, 2).ToString();
                        double cartage = 0;
                        if (txtcartage.Text.Trim() != string.Empty)
                        {
                            cartage = Convert.ToDouble(txtcartage.Text);
                        }
                        else
                        {
                            txtcartage.Text = "0";
                        }
                        double servp = 0;
                        double servamt = 0;
                        double roundoff = 0;
                        if (txtservicep.Text.Trim() != string.Empty)
                        {
                            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                        }
                        else
                        {
                            txtservicep.Text = "0";
                            txtserviceamt.Text = "0";
                        }
                        if (txtroundoff.Text.Trim() != string.Empty)
                        {
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                        }
                        else
                        {
                            txtroundoff.Text = "0";
                        }
                        double serant = 0;
                        if (txtserviceamt.Text.Trim() != string.Empty)
                        {
                            serant = Convert.ToDouble(txtserviceamt.Text);
                        }
                        //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                        double bamt = Convert.ToDouble(txtbillamount.Text);
                        double round1 = bamt - Math.Round(bamt);
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                        {
                            if (round1 > 0)
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = Math.Round(round1, 2).ToString();
                            }
                        }
                        else
                        {
                            if (round1 > 0)
                            {
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                        }
                        roundoff = Convert.ToDouble(txtroundoff.Text);
                        txtbillamount.Text = (Math.Round(bamt)).ToString();
                    }
                    else
                    {
                        txttotbasicamount.Text = "0";
                        txttotvat.Text = "0";
                        txttotaddtax.Text = "0";
                        txttotcst.Text = "0";
                        txtbillamount.Text = "0";
                    }
                }
            }
            Page.SetFocus(txtgvstockqty);
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            li.vid = Convert.ToInt64(lblid.Text);
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                if (li.vid != 0)
                {
                    DataTable dtremain = new DataTable();
                    dtremain = fpiclass.selectqtyremainusedfromscno(li);
                    if (dtremain.Rows.Count > 0)
                    {
                        li.qty = Convert.ToDouble(lblqty.Text);
                        double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                        //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                        //if (Convert.ToDouble(txtgvqty1.Text) <= rqty)
                        //{
                        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



                        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                        Label lblamount = (Label)currentRow.FindControl("lblamount");
                        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                        double vatp = 0;
                        double addvatp = 0;
                        double cstp = 0;
                        double vatamt = 0;
                        double addvatamt = 0;
                        double cstamt = 0;
                        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                        txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
                        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                        if (lbltaxtype.Text != string.Empty)
                        {
                            var cc = lbltaxtype.Text.IndexOf("-");
                            if (cc != -1)
                            {
                                li.typename = lbltaxtype.Text;
                                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                                lblvatp.Text = vatp.ToString();
                                lbladdvatp.Text = addvatp.ToString();
                                DataTable dtdtax = new DataTable();
                                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                                if (dtdtax.Rows.Count > 0)
                                {
                                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                                }
                                else
                                {
                                    txtgridcstp.Text = "0";
                                }
                            }
                        }
                        if (lblvatp.Text != string.Empty)
                        {
                            vatp = Convert.ToDouble(lblvatp.Text);
                            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                        }
                        else
                        {
                            lblvatamt.Text = "0";
                            lblvatp.Text = "0";
                        }
                        if (lbladdvatp.Text != string.Empty)
                        {
                            addvatp = Convert.ToDouble(lbladdvatp.Text);
                            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                        }
                        else
                        {
                            lbladdvatp.Text = "0";
                            lbladdvatamt.Text = "0";
                        }
                        if (txtgridcstp.Text != string.Empty)
                        {
                            cstp = Convert.ToDouble(txtgridcstp.Text);
                            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                        }
                        else
                        {
                            lblcstp.Text = "0";
                            lblcstamt.Text = "0";
                        }
                        vatamt = Convert.ToDouble(lblvatamt.Text);
                        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                        cstamt = Convert.ToDouble(lblcstamt.Text);
                        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                        if (gvpiitemlist.Rows.Count > 0)
                        {
                            double totbasicamt = 0;
                            double totcst = 0;
                            double totvat = 0;
                            double totaddvat = 0;
                            double totamount = 0;
                            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                            {
                                TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                                Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                                Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                                Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                                if (lblsctamt.Text != string.Empty)
                                {
                                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                                }
                                if (lblvatamt1.Text != string.Empty)
                                {
                                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                                }
                                if (lbladdvatamt1.Text != string.Empty)
                                {
                                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                                }
                                if (lblamount1.Text != string.Empty)
                                {
                                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                                }
                            }
                            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                            txttotvat.Text = Math.Round(totvat, 2).ToString();
                            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                            txttotcst.Text = Math.Round(totcst, 2).ToString();
                            double cartage = 0;
                            if (txtcartage.Text.Trim() != string.Empty)
                            {
                                cartage = Convert.ToDouble(txtcartage.Text);
                            }
                            else
                            {
                                txtcartage.Text = "0";
                            }
                            double servp = 0;
                            double servamt = 0;
                            double roundoff = 0;
                            if (txtservicep.Text.Trim() != string.Empty)
                            {
                                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                            }
                            else
                            {
                                txtservicep.Text = "0";
                                txtserviceamt.Text = "0";
                            }
                            if (txtroundoff.Text.Trim() != string.Empty)
                            {
                                roundoff = Convert.ToDouble(txtroundoff.Text);
                            }
                            else
                            {
                                txtroundoff.Text = "0";
                            }
                            double serant = 0;
                            if (txtserviceamt.Text.Trim() != string.Empty)
                            {
                                serant = Convert.ToDouble(txtserviceamt.Text);
                            }
                            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                            double bamt = Convert.ToDouble(txtbillamount.Text);
                            double round1 = bamt - Math.Round(bamt);
                            //txtroundoff.Text = Math.Round(round1, 2).ToString();
                            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                            {
                                if (round1 > 0)
                                {
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                                else
                                {
                                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                                }
                            }
                            else
                            {
                                if (round1 > 0)
                                {
                                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                                else
                                {
                                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                                }
                            }
                            //txtroundoff.Text = Math.Round(round1, 2).ToString();
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                            txtbillamount.Text = (Math.Round(bamt)).ToString();
                        }
                        else
                        {
                            txttotbasicamount.Text = "0";
                            txttotvat.Text = "0";
                            txttotaddtax.Text = "0";
                            txttotcst.Text = "0";
                            txtbillamount.Text = "0";
                        }
                        //}
                        //else
                        //{
                        //    txtgvqty1.Text = lblqty.Text;
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                        //    return;
                        //}
                    }
                }
                else
                {
                    txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lblcstp = (Label)currentRow.FindControl("lblcstp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
                    txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
                    double vatp = 0;
                    double addvatp = 0;
                    double cstp = 0;
                    double vatamt = 0;
                    double addvatamt = 0;
                    double cstamt = 0;
                    //double baseamt = Convert.ToDouble(txtgvamount1.Text);
                    double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
                    if (lbltaxtype.Text != string.Empty)
                    {
                        var cc = lbltaxtype.Text.IndexOf("-");
                        if (cc != -1)
                        {
                            li.typename = lbltaxtype.Text;
                            vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                            addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                            lblvatp.Text = vatp.ToString();
                            lbladdvatp.Text = addvatp.ToString();
                            DataTable dtdtax = new DataTable();
                            dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                            if (dtdtax.Rows.Count > 0)
                            {
                                txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                            }
                            else
                            {
                                txtgridcstp.Text = "0";
                            }
                        }
                    }
                    if (lblvatp.Text != string.Empty)
                    {
                        vatp = Convert.ToDouble(lblvatp.Text);
                        lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblvatamt.Text = "0";
                        lblvatp.Text = "0";
                    }
                    if (lbladdvatp.Text != string.Empty)
                    {
                        addvatp = Convert.ToDouble(lbladdvatp.Text);
                        lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
                    }
                    else
                    {
                        lbladdvatp.Text = "0";
                        lbladdvatamt.Text = "0";
                    }
                    if (txtgridcstp.Text != string.Empty)
                    {
                        cstp = Convert.ToDouble(txtgridcstp.Text);
                        lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
                    }
                    else
                    {
                        lblcstp.Text = "0";
                        lblcstamt.Text = "0";
                    }
                    vatamt = Convert.ToDouble(lblvatamt.Text);
                    addvatamt = Convert.ToDouble(lbladdvatamt.Text);
                    cstamt = Convert.ToDouble(lblcstamt.Text);
                    lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


                    if (gvpiitemlist.Rows.Count > 0)
                    {
                        double totbasicamt = 0;
                        double totcst = 0;
                        double totvat = 0;
                        double totaddvat = 0;
                        double totamount = 0;
                        for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                        {
                            TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                            Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                            Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                            Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                            Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                            if (lblsctamt.Text != string.Empty)
                            {
                                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                            }
                            if (lblvatamt1.Text != string.Empty)
                            {
                                totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                            }
                            if (lbladdvatamt1.Text != string.Empty)
                            {
                                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                            }
                            if (lblamount1.Text != string.Empty)
                            {
                                totamount = totamount + Convert.ToDouble(lblamount1.Text);
                            }
                        }
                        txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                        txttotvat.Text = Math.Round(totvat, 2).ToString();
                        txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                        txttotcst.Text = Math.Round(totcst, 2).ToString();
                        double cartage = 0;
                        if (txtcartage.Text.Trim() != string.Empty)
                        {
                            cartage = Convert.ToDouble(txtcartage.Text);
                        }
                        else
                        {
                            txtcartage.Text = "0";
                        }
                        double servp = 0;
                        double servamt = 0;
                        double roundoff = 0;
                        if (txtservicep.Text.Trim() != string.Empty)
                        {
                            txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                        }
                        else
                        {
                            txtservicep.Text = "0";
                            txtserviceamt.Text = "0";
                        }
                        if (txtroundoff.Text.Trim() != string.Empty)
                        {
                            roundoff = Convert.ToDouble(txtroundoff.Text);
                        }
                        else
                        {
                            txtroundoff.Text = "0";
                        }
                        double serant = 0;
                        if (txtserviceamt.Text.Trim() != string.Empty)
                        {
                            serant = Convert.ToDouble(txtserviceamt.Text);
                        }
                        //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                        txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                        double bamt = Convert.ToDouble(txtbillamount.Text);
                        double round1 = bamt - Math.Round(bamt);
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                        {
                            if (round1 > 0)
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = Math.Round(round1, 2).ToString();
                            }
                        }
                        else
                        {
                            if (round1 > 0)
                            {
                                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                            else
                            {
                                txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                            }
                        }
                        roundoff = Convert.ToDouble(txtroundoff.Text);
                        txtbillamount.Text = (Math.Round(bamt)).ToString();
                    }
                    else
                    {
                        txttotbasicamount.Text = "0";
                        txttotvat.Text = "0";
                        txttotaddtax.Text = "0";
                        txttotcst.Text = "0";
                        txtbillamount.Text = "0";
                    }
                }
            }
            Page.SetFocus(txtgvstockqty);
        }
    }

    protected void txtgvrate_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        if (txtgvrate1.Text.Trim() != string.Empty)
        {
            txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
            TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
            Label lblvatp = (Label)currentRow.FindControl("lblvatp");
            Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
            Label lblcstp = (Label)currentRow.FindControl("lblcstp");
            Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
            Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
            Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
            Label lblamount = (Label)currentRow.FindControl("lblamount");
            //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
            txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
            double vatp = 0;
            double addvatp = 0;
            double cstp = 0;
            double vatamt = 0;
            double addvatamt = 0;
            double cstamt = 0;
            //double baseamt = Convert.ToDouble(txtgvamount1.Text);
            double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
            if (lbltaxtype.Text != string.Empty)
            {
                var cc = lbltaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = lbltaxtype.Text;
                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    DataTable dtdtax = new DataTable();
                    dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                    if (dtdtax.Rows.Count > 0)
                    {
                        txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                    }
                    else
                    {
                        txtgridcstp.Text = "0";
                    }
                }
            }
            if (lblvatp.Text != string.Empty)
            {
                vatp = Convert.ToDouble(lblvatp.Text);
                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            }
            else
            {
                lblvatamt.Text = "0";
                lblvatp.Text = "0";
            }
            if (lbladdvatp.Text != string.Empty)
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
            }
            else
            {
                lbladdvatp.Text = "0";
                lbladdvatamt.Text = "0";
            }
            if (txtgridcstp.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgridcstp.Text);
                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                lblcstp.Text = "0";
                lblcstamt.Text = "0";
            }
            vatamt = Convert.ToDouble(lblvatamt.Text);
            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
            cstamt = Convert.ToDouble(lblcstamt.Text);
            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


            if (gvpiitemlist.Rows.Count > 0)
            {
                double totbasicamt = 0;
                double totcst = 0;
                double totvat = 0;
                double totaddvat = 0;
                double totamount = 0;
                for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                {
                    TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                    Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                    Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                    Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                    Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                    totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                    if (lblsctamt.Text != string.Empty)
                    {
                        totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                    }
                    if (lblvatamt1.Text != string.Empty)
                    {
                        totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                    }
                    if (lbladdvatamt1.Text != string.Empty)
                    {
                        totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                    }
                    if (lblamount1.Text != string.Empty)
                    {
                        totamount = totamount + Convert.ToDouble(lblamount1.Text);
                    }
                }
                txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
                txttotvat.Text = Math.Round(totvat, 2).ToString();
                txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
                txttotcst.Text = Math.Round(totcst, 2).ToString();
                double cartage = 0;
                if (txtcartage.Text.Trim() != string.Empty)
                {
                    cartage = Convert.ToDouble(txtcartage.Text);
                }
                else
                {
                    txtcartage.Text = "0";
                }
                double servp = 0;
                double servamt = 0;
                double roundoff = 0;
                if (txtservicep.Text.Trim() != string.Empty)
                {
                    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
                }
                else
                {
                    txtservicep.Text = "0";
                    txtserviceamt.Text = "0";
                }
                if (txtroundoff.Text.Trim() != string.Empty)
                {
                    roundoff = Convert.ToDouble(txtroundoff.Text);
                }
                else
                {
                    txtroundoff.Text = "0";
                }
                double serant = 0;
                if (txtserviceamt.Text.Trim() != string.Empty)
                {
                    serant = Convert.ToDouble(txtserviceamt.Text);
                }
                //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
                txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
                double bamt = Convert.ToDouble(txtbillamount.Text);
                double round1 = bamt - Math.Round(bamt);
                //txtroundoff.Text = Math.Round(round1, 2).ToString();
                if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
                {
                    if (round1 > 0)
                    {
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                    else
                    {
                        txtroundoff.Text = Math.Round(round1, 2).ToString();
                    }
                }
                else
                {
                    if (round1 > 0)
                    {
                        //txtroundoff.Text = Math.Round(round1, 2).ToString();
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                    else
                    {
                        txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                    }
                }
                roundoff = Convert.ToDouble(txtroundoff.Text);
                txtbillamount.Text = (Math.Round(bamt)).ToString();
            }
            else
            {
                txttotbasicamount.Text = "0";
                txttotvat.Text = "0";
                txttotaddtax.Text = "0";
                txttotcst.Text = "0";
                txtbillamount.Text = "0";
            }
        }
        Page.SetFocus(txtgvamount1);
    }

    protected void txtgridtaxtype_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvpiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }
        Page.SetFocus(txtgridcstp);
    }

    protected void txtgridcstp_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        TextBox txtdesc1 = (TextBox)currentRow.FindControl("txtdesc1");
        //txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        txtgvrate1.Text = ((Convert.ToDouble(txtgvrate1.Text))).ToString();
        var cc1 = txtgridcstp.Text.IndexOf("-");
        if (cc1 != -1)
        {
            txtgridcstp.Text = "0";
        }
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        //double baseamt = Convert.ToDouble(txtgvamount1.Text);
        double baseamt = Math.Round(((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))), 2);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvpiitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
            {
                TextBox lblbasicamount = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridamount");
                Label lblsctamt = (Label)gvpiitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvpiitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvpiitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txttotbasicamount.Text = Math.Round(totbasicamt, 2).ToString();
            txttotvat.Text = Math.Round(totvat, 2).ToString();
            txttotaddtax.Text = Math.Round(totaddvat, 2).ToString();
            txttotcst.Text = Math.Round(totcst, 2).ToString();
            double cartage = 0;
            if (txtcartage.Text.Trim() != string.Empty)
            {
                cartage = Convert.ToDouble(txtcartage.Text);
            }
            else
            {
                txtcartage.Text = "0";
            }
            double servp = 0;
            double servamt = 0;
            double roundoff = 0;
            if (txtservicep.Text.Trim() != string.Empty)
            {
                txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100)).ToString();
            }
            else
            {
                txtservicep.Text = "0";
                txtserviceamt.Text = "0";
            }
            if (txtroundoff.Text.Trim() != string.Empty)
            {
                roundoff = Convert.ToDouble(txtroundoff.Text);
            }
            else
            {
                txtroundoff.Text = "0";
            }
            double serant = 0;
            if (txtserviceamt.Text.Trim() != string.Empty)
            {
                serant = Convert.ToDouble(txtserviceamt.Text);
            }
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txtbillamount.Text = (Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txtserviceamt.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txtcartage.Text)).ToString();
            double bamt = Convert.ToDouble(txtbillamount.Text);
            double round1 = bamt - Math.Round(bamt);
            //txtroundoff.Text = Math.Round(round1, 2).ToString();
            if (Math.Round((Convert.ToDouble(txttotbasicamount.Text) + Convert.ToDouble(txttotvat.Text) + Convert.ToDouble(txttotaddtax.Text) + Convert.ToDouble(txttotcst.Text) + Convert.ToDouble(txtcartage.Text) + Convert.ToDouble(txtserviceamt.Text)), 2) < Math.Round(Convert.ToDouble(txtbillamount.Text), 2))
            {
                if (round1 > 0)
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = Math.Round(round1, 2).ToString();
                }
            }
            else
            {
                if (round1 > 0)
                {
                    //txtroundoff.Text = Math.Round(round1, 2).ToString();
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
                else
                {
                    txtroundoff.Text = ((-1) * (Math.Round((round1), 2))).ToString();
                }
            }
            roundoff = Convert.ToDouble(txtroundoff.Text);
            txtbillamount.Text = (Math.Round(bamt)).ToString();
        }
        else
        {
            txttotbasicamount.Text = "0";
            txttotvat.Text = "0";
            txttotaddtax.Text = "0";
            txttotcst.Text = "0";
            txtbillamount.Text = "0";
        }
        Page.SetFocus(txtdesc1);
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            lblpopupacname.Text = drpacname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtpc = new DataTable();
            dtpc = fpiclass.selectallpurchasechallanforpopup(li);
            if (dtpc.Rows.Count > 0)
            {
                lblemptypc.Visible = false;
                gvpurchasechallans.Visible = true;
                gvpurchasechallans.DataSource = dtpc;
                gvpurchasechallans.DataBind();
            }
            else
            {
                gvpurchasechallans.Visible = false;
                lblemptypc.Visible = true;
                lblemptypc.Text = "No Sales Challan Found.";
            }
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvpurchasechallans_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemspuin"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pcno = Convert.ToInt64(e.CommandArgument);
            if (txtchallanno.Text.Trim() == string.Empty)
            {
                txtchallanno.Text = e.CommandArgument.ToString();
                if (txtchallanno.Text.Trim() != string.Empty)
                {
                    li.pcno = Convert.ToInt64(txtchallanno.Text);
                    DataTable dtdate = new DataTable();
                    dtdate = fpiclass.selectpcdatefrompcno(li);
                    txtchallandate.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.pcno = 0;
                }
                if (txtchallanno1.Text.Trim() != string.Empty)
                {
                    li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
                    DataTable dtdate = new DataTable();
                    dtdate = fpiclass.selectpcdatefrompcno1(li);
                    txtchallandate1.Text = dtdate.Rows[0]["cdate"].ToString();
                }
                else
                {
                    li.pcno1 = 0;
                }
                li.remarks = li.pcno + "," + li.pcno1;
                hdnpcno.Value = li.pcno.ToString();
                if (li.pcno1 != 0)
                {
                    hdnpcno.Value = hdnpcno.Value + "," + li.pcno1 + "";
                }
                txtsrtringpcno.Text = hdnpcno.Value;
                DataTable dtdt = new DataTable();
                dtdt = fpiclass.selectallmasterdatafrompcnotexhchange(li);
                if (dtdt.Rows.Count > 0)
                {
                    //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                }
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallitemdatafrompcnotexhchange(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int w = 0; w < dtdata.Rows.Count; w++)
                    {
                        if (dtdata.Rows[w]["gsttype"].ToString().Trim() == string.Empty || dtdata.Rows[w]["gsttype"].ToString().Trim() == "")
                        {
                            txtchallanno.Text = string.Empty;
                            txtsrtringpcno.Text = string.Empty;
                            hdnpcno.Value = string.Empty;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter gst type for " + dtdata.Rows[w]["itemname"].ToString() + " and try again.');", true);
                            return;
                        }
                    }
                    //if (dtsoitem != null)
                    //{
                    //    if (dtsoitem.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem);
                    //    }
                    //}
                    //lblempty.Visible = false;
                    //gvpiitemlist.Visible = true;
                    //gvpiitemlist.DataSource = dtdata;
                    //gvpiitemlist.DataBind();
                    //Session["dtpitems"] = dtdata;
                    DataTable dt = (DataTable)Session["dtpitemspuin"];
                    dt.Clear();
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        //dr["id"] = dt.Rows.Count + 1;
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["pcno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = dtdata.Rows[d]["stockqty"].ToString();
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        //if (chkigst.Checked == true && drpinvtype.SelectedItem.Text == "GST")
                        //{
                        //    if (dtdata.Rows[0]["vattype"].ToString().IndexOf("-") != -1)
                        //    {
                        //        dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[1]), 2)).ToString();
                        //        dr["taxtype"] = 0;
                        //        dr["taxdesc"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["vattype"].ToString().Split('-')[1]), 2)).ToString();
                        //    }
                        //    else
                        //    {
                        //        dr["taxtype"] = "";
                        //        dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                        //        dr["taxdesc"] = dtdata.Rows[d]["vattype"].ToString();
                        //    }
                        //}
                        //else
                        //{
                        //    if (dtdata.Rows[0]["taxtype"].ToString().IndexOf("-") != -1)
                        //    {
                        //        dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
                        //        dr["cstp"] = 0;
                        //        dr["taxdesc"] = dtdata.Rows[d]["taxtype"].ToString();
                        //    }
                        //    else
                        //    {
                        //        dr["taxtype"] = "";
                        //        dr["cstp"] = dtdata.Rows[d]["taxtype"].ToString();
                        //        dr["taxdesc"] = dtdata.Rows[d]["taxtype"].ToString();
                        //    }

                        //}  
                        if (chkigst.Checked == true && drpinvtype.SelectedItem.Text == "P")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                                dr["taxtype"] = 0;
                                dr["taxdesc"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gstdesc"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gstdesc"].ToString().Split('-')[1]), 2)).ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else if (chkigst.Checked == false && drpinvtype.SelectedItem.Text == "P")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = 0;
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else
                        {
                            if (dtdata.Rows[d]["vattype"].ToString().IndexOf("-") != -1)
                            {
                                dr["taxtype"] = dtdata.Rows[d]["vattype"].ToString();
                                dr["cstp"] = 0;
                                dr["taxdesc"] = dtdata.Rows[d]["vattype"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["vattype"].ToString() != "" && dtdata.Rows[d]["vattype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["vattype"].ToString();
                            }

                        }
                        dr["vatp"] = 0;
                        dr["vatamt"] = 0;
                        dr["addtaxp"] = 0;
                        dr["addtaxamt"] = 0;
                        //dr["cstp"] = 0;
                        dr["cstamt"] = 0;
                        //dr["unit"] = txtunit.Text;
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                        dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                    }
                    Session["dtpitemspuin"] = dt;
                    this.bindgrid();
                    countgv();
                }
                else
                {
                    gvpiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                    txttotbasicamount.Text = "0";
                    txttotcst.Text = "0";
                    txttotvat.Text = "0";
                    txtservicep.Text = "0";
                    txtserviceamt.Text = "0";
                    txtroundoff.Text = "0";
                    txttotaddtax.Text = "0";
                    txtcartage.Text = "0";
                    txtbillamount.Text = "0";
                    fillacnamedrop();
                }
            }
            else
            {
                //txtchallanno1.Text = e.CommandArgument.ToString();
                txtchallanno1.Text = li.pcno.ToString();
                if (txtchallanno1.Text.Trim() != string.Empty)
                {
                    txtchallanno1.Text = e.CommandArgument.ToString();
                    hdnpcno.Value = hdnpcno.Value + "," + txtchallanno1.Text + "";
                }
                else
                {
                    hdnpcno.Value = hdnpcno.Value + "," + e.CommandArgument.ToString() + "";
                }
                if (txtchallanno.Text.Trim() != string.Empty)
                {
                    li.pcno = Convert.ToInt64(txtchallanno.Text);
                }
                else
                {
                    li.pcno = 0;
                }
                if (txtchallanno1.Text.Trim() != string.Empty)
                {
                    li.pcno1 = Convert.ToInt64(txtchallanno1.Text);
                }
                else
                {
                    li.pcno1 = 0;
                }
                //li.remarks = li.pcno + "," + li.pcno1;
                li.remarks = hdnpcno.Value;
                txtsrtringpcno.Text = hdnpcno.Value;
                DataTable dtdt = new DataTable();
                dtdt = fpiclass.selectallmasterdatafrompcnotexhchange(li);
                if (dtdt.Rows.Count > 0)
                {
                    //drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
                }
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallitemdatafrompcnotexhchange(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int w = 0; w < dtdata.Rows.Count; w++)
                    {
                        if (dtdata.Rows[w]["gsttype"].ToString().Trim() == string.Empty || dtdata.Rows[w]["gsttype"].ToString().Trim() == "")
                        {
                            txtchallanno1.Text = string.Empty;
                            txtsrtringpcno.Text = "'" + txtchallanno.Text + "'";
                            hdnpcno.Value = "'" + txtchallanno.Text + "'";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter gst type for " + dtdata.Rows[w]["itemname"].ToString() + " and try again.');", true);
                            return;
                        }
                    }
                    //if (dtsoitem != null)
                    //{
                    //    if (dtsoitem.Rows.Count > 0)
                    //    {
                    //        dtdata.Merge(dtsoitem);
                    //    }
                    //}
                    //lblempty.Visible = false;
                    //gvpiitemlist.Visible = true;
                    //gvpiitemlist.DataSource = dtdata;
                    //gvpiitemlist.DataBind();
                    //Session["dtpitems"] = dtdata;
                    DataTable dt = (DataTable)Session["dtpitemspuin"];
                    dt.Clear();
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        //dr["id"] = dt.Rows.Count + 1;
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["pcno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = dtdata.Rows[d]["stockqty"].ToString();
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        if (chkigst.Checked == true && drpinvtype.SelectedItem.Text == "P")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                                dr["taxtype"] = 0;
                                dr["taxdesc"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gstdesc"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gstdesc"].ToString().Split('-')[1]), 2)).ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else if (chkigst.Checked == false && drpinvtype.SelectedItem.Text == "P")
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["cstp"] = 0;
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                        }
                        else
                        {
                            if (dtdata.Rows[d]["gsttype"].ToString().IndexOf("-") != -1)
                            {
                                dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                                dr["cstp"] = 0;
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }
                            else
                            {
                                dr["taxtype"] = "";
                                //dr["cstp"] = dtdata.Rows[d]["vattype"].ToString();
                                if (dtdata.Rows[d]["gsttype"].ToString() != "" && dtdata.Rows[d]["gsttype"].ToString() != string.Empty)
                                {
                                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                                }
                                else
                                {
                                    dr["cstp"] = 0;
                                }
                                dr["taxdesc"] = dtdata.Rows[d]["gstdesc"].ToString();
                            }

                        }
                        dr["vatp"] = 0;
                        dr["vatamt"] = 0;
                        dr["addtaxp"] = 0;
                        dr["addtaxamt"] = 0;
                        //dr["cstp"] = 0;
                        dr["cstamt"] = 0;
                        //dr["unit"] = txtunit.Text;
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();
                        dr["ccode"] = dtdata.Rows[d]["ccode"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                    }
                    Session["dtpitemspuin"] = dt;
                    this.bindgrid();
                    countgv();
                }
                else
                {
                    gvpiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                    txttotbasicamount.Text = "0";
                    txttotcst.Text = "0";
                    txttotvat.Text = "0";
                    txtservicep.Text = "0";
                    txtserviceamt.Text = "0";
                    txtroundoff.Text = "0";
                    txttotaddtax.Text = "0";
                    txtcartage.Text = "0";
                    txtbillamount.Text = "0";
                    fillacnamedrop();
                }
            }
            hideimage();
            ModalPopupExtender2.Hide();
        }
        hideimage();
        Page.SetFocus(drppurchaseac);
    }
    protected void gvpiitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.pidate = Convert.ToDateTime(txtpidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            li.qty = Convert.ToDouble(lblqty.Text);
            li.vid = Convert.ToInt64(e.CommandArgument);
            DataTable dtremain = new DataTable();
            dtremain = fpiclass.selectqtyremainusedfromscno(li);
            if (Convert.ToDouble(txtgvqty1.Text) > 0)
            {
                if (dtremain.Rows.Count > 0)
                {
                    //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                    li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                    fpiclass.updateremainqtyinpurchasechallan(li);


                    //update piitems data
                    //for (int c = 0; c < gvpiitemlist.Rows.Count; c++)
                    //{
                    li.pino = Convert.ToInt64(txtinvoiceno.Text);
                    li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblvno = (Label)currentRow.FindControl("lblvno");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    TextBox txtgridtaxdesc = (TextBox)currentRow.FindControl("txtgridtaxdesc");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.itemname = lblitemname.Text;
                    if (txtgvstockqty.Text.Trim() != string.Empty)
                    {
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    }
                    else
                    {
                        li.stockqty = 0;
                    }
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    //li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    if (lblbasicamount.Text != "")
                    {
                        li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    }
                    else
                    {
                        li.basicamount = 0;
                    }
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    li.taxdesc = txtgridtaxdesc.Text;
                    fpiclass.updatepiitemsdata(li);

                }
                else
                {
                    li.pino = Convert.ToInt64(txtinvoiceno.Text);
                    li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblvno = (Label)currentRow.FindControl("lblvno");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                    Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                    Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                    Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                    Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                    TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                    Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                    //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                    Label lblamount = (Label)currentRow.FindControl("lblamount");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                    li.itemname = lblitemname.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    //li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lbltaxtype.Text;
                    if (lblvatp.Text != "")
                    {
                        li.vatp = Convert.ToDouble(lblvatp.Text);
                    }
                    else
                    {
                        li.vatp = 0;
                    }
                    if (lbladdvatp.Text != "")
                    {
                        li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    }
                    else
                    {
                        li.addtaxp = 0;
                    }
                    if (lblcstp.Text != "")
                    {
                        li.cstp = Convert.ToDouble(lblcstp.Text);
                    }
                    else
                    {
                        li.cstp = 0;
                    }
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    if (lblbasicamount.Text != "")
                    {
                        li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                    }
                    else
                    {
                        li.basicamount = 0;
                    }
                    if (lblsctamt.Text != "")
                    {
                        li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                    }
                    else
                    {
                        li.cstamt = 0;
                    }
                    if (lblvatamt.Text != "")
                    {
                        li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                    }
                    else
                    {
                        li.vatamt = 0;
                    }
                    if (lbladdvatamt.Text != "")
                    {
                        li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                    }
                    else
                    {
                        li.addtaxamt = 0;
                    }
                    if (lblamount.Text != "")
                    {
                        li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                    }
                    else
                    {
                        li.amount = 0;
                    }
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    fpiclass.updatepiitemsdata(li);
                }
            }
            else
            {
                li.pino = Convert.ToInt64(txtinvoiceno.Text);
                li.strpino = drpinvtype.SelectedItem.Text + (Convert.ToInt64(txtinvoiceno.Text)).ToString();
                Label lblid = (Label)currentRow.FindControl("lblid");
                Label lblvno = (Label)currentRow.FindControl("lblvno");
                Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                //TextBox lblqty = (TextBox)gvpiitemlist.Rows[c].FindControl("txtgridqty");
                TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                TextBox lblbasicamount = (TextBox)currentRow.FindControl("txtgridamount");
                TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
                Label lblvatp = (Label)currentRow.FindControl("lblvatp");
                Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
                Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
                Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
                TextBox lblcstp = (TextBox)currentRow.FindControl("txtgridcstp");
                Label lblsctamt = (Label)currentRow.FindControl("lblcstamt");
                //Label lblunit = (Label)gvpiitemlist.Rows[c].FindControl("lblunit");
                Label lblamount = (Label)currentRow.FindControl("lblamount");
                Label lblccode = (Label)currentRow.FindControl("lblccode");
                TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                li.id = Convert.ToInt64(lblid.Text);
                li.vnono = Convert.ToInt64(txtinvoiceno.Text);
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(txtgvqty1.Text);
                li.qtyremain = li.qty;
                li.qtyused = 0;
                //if (li.qty > 0)
                //{
                //li.unit = lblunit.Text;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.taxtype = lbltaxtype.Text;
                if (lblvatp.Text != "")
                {
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                }
                else
                {
                    li.vatp = 0;
                }
                if (lbladdvatp.Text != "")
                {
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                }
                else
                {
                    li.addtaxp = 0;
                }
                if (lblcstp.Text != "")
                {
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                }
                else
                {
                    li.cstp = 0;
                }
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                if (lblbasicamount.Text != "")
                {
                    li.basicamount = Math.Round((Convert.ToDouble(lblbasicamount.Text)), 2);
                }
                else
                {
                    li.basicamount = 0;
                }
                if (lblsctamt.Text != "")
                {
                    li.cstamt = Math.Round((Convert.ToDouble(lblsctamt.Text)), 2);
                }
                else
                {
                    li.cstamt = 0;
                }
                if (lblvatamt.Text != "")
                {
                    li.vatamt = Math.Round((Convert.ToDouble(lblvatamt.Text)), 2);
                }
                else
                {
                    li.vatamt = 0;
                }
                if (lbladdvatamt.Text != "")
                {
                    li.addtaxamt = Math.Round((Convert.ToDouble(lbladdvatamt.Text)), 2);
                }
                else
                {
                    li.addtaxamt = 0;
                }
                if (lblamount.Text != "")
                {
                    li.amount = Math.Round((Convert.ToDouble(lblamount.Text)), 2);
                }
                else
                {
                    li.amount = 0;
                }
                li.ccode = Convert.ToInt64(lblccode.Text);
                fpiclass.updatepiitemsdata(li);
            }
            DataTable dtitems = new DataTable();
            dtitems = fpiclass.selectallpiitemsfrompinostring(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpiitemlist.Visible = true;
                gvpiitemlist.DataSource = dtitems;
                gvpiitemlist.DataBind();
            }
            else
            {
                gvpiitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Purchase Invoice Item Found.";
            }
            //    }
            //}


            //}
        }
    }
    protected void txtinvoiceno_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.strpino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fpiclass.selectallpimasterdatafrompinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fpiclass.updateisused(li);
                getpino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.strpino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fpiclass.selectallpimasterdatafrompinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fpiclass.updateisused(li);
                //getsino();
                txtinvoiceno.Text = ViewState["pino"].ToString();
                drpinvtype.SelectedValue = ViewState["invtype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(txtbillno);
    }
    protected void drpinvtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.strpino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fpiclass.selectallpimasterdatafrompinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fpiclass.updateisused(li);
                getpino();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pino = Convert.ToInt64(txtinvoiceno.Text);
            li.strpino = drpinvtype.SelectedItem.Text + li.sino.ToString();
            li.invtype = drpinvtype.SelectedItem.Text;
            DataTable dtcheck = new DataTable();
            dtcheck = fpiclass.selectallpimasterdatafrompinoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                //fsiclass.insertsimasterdata(li);
            }
            else
            {
                li.sino = Convert.ToInt64(txtinvoiceno.Text);
                fpiclass.updateisused(li);
                //getsino();
                txtinvoiceno.Text = ViewState["pino"].ToString();
                drpinvtype.SelectedValue = ViewState["invtype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Invoice No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(txtbillno);
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                //txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txttaxtype.Text = dtdata.Rows[0]["vattype"].ToString();
                string vattype = dtdata.Rows[0]["vattype"].ToString();
                if (vattype.IndexOf("-") != -1)
                {
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvat.Text = "0";
                    txtgvadtax.Text = vattype.Split('-')[1];
                    txtaddtax.Text = "0";
                    count1();
                }
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                //txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadtax.Text = string.Empty;
                txtvat.Text = string.Empty;
                txtaddtax.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtamount.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            //txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadtax.Text = string.Empty;
            txtvat.Text = string.Empty;
            txtaddtax.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtamount.Text = string.Empty;
        }
        Page.SetFocus(txtbillqty);
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' order by pino", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strpino = li.invtype + dtdataq.Rows[0]["pino"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fpiclass.selectallpimasterdatafrompinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                txtinvoiceno.Text = dtdata.Rows[0]["pino"].ToString();
                ViewState["pino"] = Convert.ToInt64(dtdata.Rows[0]["pino"].ToString());
                txtpidate.Text = Convert.ToDateTime(dtdata.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpinvtype.SelectedItem.Text = dtdata.Rows[0]["invtype"].ToString();
                ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                txtbillno.Text = dtdata.Rows[0]["billno"].ToString();
                txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtchallanno.Text = dtdata.Rows[0]["pcno"].ToString();
                txtchallanno1.Text = dtdata.Rows[0]["pcno1"].ToString();
                txtchallandate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                if (dtdata.Rows[0]["pcdate1"].ToString() != string.Empty)
                {
                    txtchallandate1.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drppurchaseac.SelectedValue = dtdata.Rows[0]["purchaseac"].ToString();
                txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                txtform.Text = dtdata.Rows[0]["form"].ToString();
                txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                txtsrtringpcno.Text = dtdata.Rows[0]["stringpcno"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                DataTable dtitems = new DataTable();
                dtitems = fpiclass.selectallpiitemsfrompinostring(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpiitemlist.Visible = true;
                    gvpiitemlist.DataSource = dtitems;
                    gvpiitemlist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvpiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Invoice Item Found.";
                    lblcount.Text = "0";
                }
                btnsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' order by pino", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strpino = li.invtype + dtdataq.Rows[dtdataq.Rows.Count - 1]["pino"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fpiclass.selectallpimasterdatafrompinostring(li);
            if (dtdata.Rows.Count > 0)
            {
                txtinvoiceno.Text = dtdata.Rows[0]["pino"].ToString();
                ViewState["pino"] = Convert.ToInt64(dtdata.Rows[0]["pino"].ToString());
                txtpidate.Text = Convert.ToDateTime(dtdata.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpinvtype.SelectedItem.Text = dtdata.Rows[0]["invtype"].ToString();
                ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                txtbillno.Text = dtdata.Rows[0]["billno"].ToString();
                txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtchallanno.Text = dtdata.Rows[0]["pcno"].ToString();
                txtchallanno1.Text = dtdata.Rows[0]["pcno1"].ToString();
                txtchallandate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                if (dtdata.Rows[0]["pcdate1"].ToString() != string.Empty)
                {
                    txtchallandate1.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drppurchaseac.SelectedValue = dtdata.Rows[0]["purchaseac"].ToString();
                txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                txtform.Text = dtdata.Rows[0]["form"].ToString();
                txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                txtsrtringpcno.Text = dtdata.Rows[0]["stringpcno"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                DataTable dtitems = new DataTable();
                dtitems = fpiclass.selectallpiitemsfrompinostring(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpiitemlist.Visible = true;
                    gvpiitemlist.DataSource = dtitems;
                    gvpiitemlist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvpiitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Invoice Item Found.";
                    lblcount.Text = "0";
                }
                btnsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' order by pino", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.pino = Convert.ToInt64(txtinvoiceno.Text) + 1;
        for (Int64 i = li.pino; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["pino"].ToString()); i++)
        {
            li.pino = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' and pino=" + li.pino + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strpino = li.invtype + dtdataq.Rows[0]["pino"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallpimasterdatafrompinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtinvoiceno.Text = dtdata.Rows[0]["pino"].ToString();
                    ViewState["pino"] = Convert.ToInt64(dtdata.Rows[0]["pino"].ToString());
                    txtpidate.Text = Convert.ToDateTime(dtdata.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpinvtype.SelectedItem.Text = dtdata.Rows[0]["invtype"].ToString();
                    ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                    txtbillno.Text = dtdata.Rows[0]["billno"].ToString();
                    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtchallanno.Text = dtdata.Rows[0]["pcno"].ToString();
                    txtchallanno1.Text = dtdata.Rows[0]["pcno1"].ToString();
                    txtchallandate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    if (dtdata.Rows[0]["pcdate1"].ToString() != string.Empty)
                    {
                        txtchallandate1.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drppurchaseac.SelectedValue = dtdata.Rows[0]["purchaseac"].ToString();
                    txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                    txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                    txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                    txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                    txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                    txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                    txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                    txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                    txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                    txtform.Text = dtdata.Rows[0]["form"].ToString();
                    txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                    txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                    txtsrtringpcno.Text = dtdata.Rows[0]["stringpcno"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    DataTable dtitems = new DataTable();
                    dtitems = fpiclass.selectallpiitemsfrompinostring(li);
                    if (dtitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpiitemlist.Visible = true;
                        gvpiitemlist.DataSource = dtitems;
                        gvpiitemlist.DataBind();
                        lblcount.Text = dtitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpiitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Invoice Item Found.";
                        lblcount.Text = "0";
                    }
                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.invtype = drpinvtype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' order by pino", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.pino = Convert.ToInt64(txtinvoiceno.Text) - 1;
        for (Int64 i = li.pino; i >= Convert.ToInt64(dtdataqa.Rows[0]["pino"].ToString()); i--)
        {
            li.pino = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from PIMaster where invtype='" + li.invtype + "' and pino=" + li.pino + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strpino = li.invtype + dtdataq.Rows[0]["pino"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallpimasterdatafrompinostring(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtinvoiceno.Text = dtdata.Rows[0]["pino"].ToString();
                    ViewState["pino"] = Convert.ToInt64(dtdata.Rows[0]["pino"].ToString());
                    txtpidate.Text = Convert.ToDateTime(dtdata.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpinvtype.SelectedItem.Text = dtdata.Rows[0]["invtype"].ToString();
                    ViewState["invtype"] = dtdata.Rows[0]["invtype"].ToString();
                    txtbillno.Text = dtdata.Rows[0]["billno"].ToString();
                    txtbilldate.Text = Convert.ToDateTime(dtdata.Rows[0]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtchallanno.Text = dtdata.Rows[0]["pcno"].ToString();
                    txtchallanno1.Text = dtdata.Rows[0]["pcno1"].ToString();
                    txtchallandate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    if (dtdata.Rows[0]["pcdate1"].ToString() != string.Empty)
                    {
                        txtchallandate1.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drppurchaseac.SelectedValue = dtdata.Rows[0]["purchaseac"].ToString();
                    txttotbasicamount.Text = dtdata.Rows[0]["totbasicamount"].ToString();
                    txttotvat.Text = dtdata.Rows[0]["totvat"].ToString();
                    txttotaddtax.Text = dtdata.Rows[0]["totaddtax"].ToString();
                    txttotcst.Text = dtdata.Rows[0]["totcst"].ToString();
                    txtservicep.Text = dtdata.Rows[0]["servicetaxp"].ToString();
                    txtserviceamt.Text = dtdata.Rows[0]["servicetaxamount"].ToString();
                    txtcartage.Text = dtdata.Rows[0]["cartage"].ToString();
                    txtroundoff.Text = dtdata.Rows[0]["roundoff"].ToString();
                    txtbillamount.Text = dtdata.Rows[0]["billamount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtremakrs.Text = dtdata.Rows[0]["remarks"].ToString();
                    txtform.Text = dtdata.Rows[0]["form"].ToString();
                    txttransportname.Text = dtdata.Rows[0]["transport"].ToString();
                    txtsalesman.Text = dtdata.Rows[0]["salesman"].ToString();
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
                    txtsrtringpcno.Text = dtdata.Rows[0]["stringpcno"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    DataTable dtitems = new DataTable();
                    dtitems = fpiclass.selectallpiitemsfrompinostring(li);
                    if (dtitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpiitemlist.Visible = true;
                        gvpiitemlist.DataSource = dtitems;
                        gvpiitemlist.DataBind();
                        lblcount.Text = dtitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpiitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Invoice Item Found.";
                        lblcount.Text = "0";
                    }
                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }

}