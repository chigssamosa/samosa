﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepBankBook.aspx.cs" Inherits="RepBankBook" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Bank Book Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Bank Name</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtacname" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsaves" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnsaves_Click" ValidationGroup="val" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnreconcile" runat="server" Text="Reconcile" class="btn btn-default forbutton"
                    OnClick="btnreconcile_Click" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter bank name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtacname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="row" style="height: 40px;">
            <div class="col-md-2">
                <asp:Button ID="btnprint" runat="server" Text="Print" class="btn btn-default forbutton"
                    OnClick="btnprint_Click" Visible="false" /></div>
            <div class="col-md-9" style="font-size: larger; font-weight: bolder;">
                Clear Balance :
                <asp:Label ID="lblclearbalance" runat="server"></asp:Label></div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblemptyreconcile" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvreconcile" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" Height="0px" CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete123();"
                                        CommandArgument='<%# bind("voucherno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="voucherno" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherno" ForeColor="Black" runat="server" Text='<%# bind("voucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="voucher date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherdate" ForeColor="Black" runat="server" Text='<%# bind("voucherdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AC Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblistype" ForeColor="Black" runat="server" Text='<%# bind("istype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cheque No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblchequeno" ForeColor="Black" runat="server" Text='<%# bind("chequeno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CL. Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtcldate" runat="server" Text='<%# bind("cldate") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtcldate_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
