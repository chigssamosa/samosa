﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Drawing;

public partial class AccountMasterList : System.Web.UI.Page
{
    ForAccountMaster famclass = new ForAccountMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtall = new DataTable();
        DataTable dtdata = new DataTable();
        if (Request.Cookies["ForLogin"]["username"] == "admin@airmax.so")
        {
            dtdata = famclass.selectallaccountdata();
        }
        else
        {
            dtdata = famclass.selectallaccountdatacnowise(li);
        }
        for (int i = 0; i < dtdata.Rows.Count; i++)
        {
            li.acname = dtdata.Rows[i]["acname"].ToString();
            DataTable dtsingle = new DataTable();
            dtsingle = famclass.selectopcldrcrdataacnamewise(li);
            if (dtsingle.Rows.Count > 0)
            {
                dtall.Merge(dtsingle);
            }
        }
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvaclist.Visible = true;
            gvaclist.DataSource = dtall;
            gvaclist.DataBind();
        }
        else
        {
            gvaclist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Rercord Found for Account Master.";
        }
    }

    protected void gvaclist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                //string acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                li.id = Convert.ToInt64(e.CommandArgument);
                //DataTable dtcheck = famclass.checkacyearfordata(li);
                //if (dtcheck.Rows.Count > 0)
                //{
                //    if (dtcheck.Rows[0]["acyear"].ToString() == acyear)
                //    {
                Response.Redirect("~/AccountMaster.aspx?mode=update&id=" + li.id + "");
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Account Name is fetched from Old Year Account Master so you need to go to Last Year Database and update this data from there and then fetch Account data in new year again using Year End Process.');", true);
                //    }
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.id = Convert.ToInt64(e.CommandArgument);
                DataTable dtdata = new DataTable();
                dtdata = famclass.selectaccountdatafromid(li);
                if (dtdata.Rows.Count > 0)
                {
                    li.acname = dtdata.Rows[0]["acname"].ToString();
                    DataTable dttaxmaster = new DataTable();
                    dttaxmaster = famclass.checktaxmaster(li);
                    if (dttaxmaster.Rows.Count == 0)
                    {
                        DataTable dtbankacmaster = new DataTable();
                        dtbankacmaster = famclass.checkbankacmaster(li);
                        if (dtbankacmaster.Rows.Count == 0)
                        {
                            DataTable dtbankacmaster1 = new DataTable();
                            dtbankacmaster1 = famclass.checkbankacmaster1(li);
                            if (dtbankacmaster1.Rows.Count == 0)
                            {
                                DataTable dtbankacshadow = new DataTable();
                                dtbankacshadow = famclass.checkbankacshadow(li);
                                if (dtbankacshadow.Rows.Count == 0)
                                {
                                    DataTable dtledgercreditcode = new DataTable();
                                    dtledgercreditcode = famclass.checkledgercreditcode(li);
                                    if (dtledgercreditcode.Rows.Count == 0)
                                    {
                                        DataTable dtledgerdebitcode = new DataTable();
                                        dtledgerdebitcode = famclass.checkledgerdebitcode(li);
                                        if (dtledgerdebitcode.Rows.Count == 0)
                                        {
                                            DataTable dtpcmaster = new DataTable();
                                            dtpcmaster = famclass.checkpcmaster(li);
                                            if (dtpcmaster.Rows.Count == 0)
                                            {
                                                DataTable dtperinvitems = new DataTable();
                                                dtperinvitems = famclass.checkperinvitems(li);
                                                if (dtperinvitems.Rows.Count == 0)
                                                {
                                                    DataTable dtcomminvitems = new DataTable();
                                                    dtcomminvitems = famclass.checkcomminvitems(li);
                                                    if (dtcomminvitems.Rows.Count == 0)
                                                    {
                                                        DataTable dtperinvmaster = new DataTable();
                                                        dtperinvmaster = famclass.checkperinvmaster(li);
                                                        if (dtperinvmaster.Rows.Count == 0)
                                                        {
                                                            DataTable dtcomminvmaster = new DataTable();
                                                            dtcomminvmaster = famclass.checkcomminvmaster(li);
                                                            if (dtcomminvmaster.Rows.Count == 0)
                                                            {
                                                                DataTable dtpimaster = new DataTable();
                                                                dtpimaster = famclass.checkpimaster(li);
                                                                if (dtpimaster.Rows.Count == 0)
                                                                {
                                                                    DataTable dtpmmaster = new DataTable();
                                                                    dtpmmaster = famclass.checkpmmaster(li);
                                                                    if (dtpmmaster.Rows.Count == 0)
                                                                    {
                                                                        DataTable dtPurchaseOrder = new DataTable();
                                                                        dtPurchaseOrder = famclass.checkPurchaseOrder(li);
                                                                        if (dtPurchaseOrder.Rows.Count == 0)
                                                                        {
                                                                            DataTable dtQuoItems = new DataTable();
                                                                            dtQuoItems = famclass.checkQuoItems(li);
                                                                            if (dtQuoItems.Rows.Count == 0)
                                                                            {
                                                                                DataTable dtQuoMaster = new DataTable();
                                                                                dtQuoMaster = famclass.checkQuoMaster(li);
                                                                                if (dtQuoMaster.Rows.Count == 0)
                                                                                {
                                                                                    DataTable dtrrmaster = new DataTable();
                                                                                    dtrrmaster = famclass.checkrrmaster(li);
                                                                                    if (dtrrmaster.Rows.Count == 0)
                                                                                    {
                                                                                        DataTable dtSalesOrder = new DataTable();
                                                                                        dtSalesOrder = famclass.checkSalesOrder(li);
                                                                                        if (dtSalesOrder.Rows.Count == 0)
                                                                                        {
                                                                                            DataTable dtSCMaster = new DataTable();
                                                                                            dtSCMaster = famclass.checkSCMaster(li);
                                                                                            if (dtSCMaster.Rows.Count == 0)
                                                                                            {
                                                                                                DataTable dtSIMaster = new DataTable();
                                                                                                dtSIMaster = famclass.checkSIMaster(li);
                                                                                                if (dtSIMaster.Rows.Count == 0)
                                                                                                {
                                                                                                    DataTable dtStockJVItems = new DataTable();
                                                                                                    dtStockJVItems = famclass.checkStockJVItems(li);
                                                                                                    if (dtStockJVItems.Rows.Count == 0)
                                                                                                    {
                                                                                                        DataTable dtStockJVMaster = new DataTable();
                                                                                                        dtStockJVMaster = famclass.checkStockJVMaster(li);
                                                                                                        if (dtStockJVMaster.Rows.Count == 0)
                                                                                                        {
                                                                                                            DataTable dtToolDelMaster = new DataTable();
                                                                                                            dtToolDelMaster = famclass.checkToolDelMaster(li);
                                                                                                            if (dtToolDelMaster.Rows.Count == 0)
                                                                                                            {
                                                                                                                DataTable dtClientMaster = new DataTable();
                                                                                                                dtClientMaster = famclass.checkClientMaster(li);
                                                                                                                if (dtClientMaster.Rows.Count == 0)
                                                                                                                {
                                                                                                                    famclass.deleteaccountdatafromledger(li);
                                                                                                                    famclass.deleteaccountdata(li);
                                                                                                                    fillgrid();
                                                                                                                    li.uname = Request.Cookies["ForLogin"]["username"];
                                                                                                                    li.udate = System.DateTime.Now;
                                                                                                                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                                                                                                                    li.activity = li.acname + " Account name deleted.";
                                                                                                                    faclass.insertactivity(li);
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Client Master so you cant delete this ACName.');", true);
                                                                                                                    return;
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Tools Delivery Entry so you cant delete this ACName.');", true);
                                                                                                                return;
                                                                                                            }
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Stock JV so you cant delete this ACName.');", true);
                                                                                                            return;
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Stock JV so you cant delete this ACName.');", true);
                                                                                                        return;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Sales Invoice so you cant delete this ACName.');", true);
                                                                                                    return;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Sales Challan so you cant delete this ACName.');", true);
                                                                                                return;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Sales Order so you cant delete this ACName.');", true);
                                                                                            return;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Returnable Return Entry so you cant delete this ACName.');", true);
                                                                                        return;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Bifurcation Entry so you cant delete this ACName.');", true);
                                                                                    return;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Bifurcation Entry so you cant delete this ACName.');", true);
                                                                                return;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Purchase Order so you cant delete this ACName.');", true);
                                                                            return;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Packing Material Slip so you cant delete this ACName.');", true);
                                                                        return;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Performa Invoice so you cant delete this ACName.');", true);
                                                                    return;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Commercial Invoice so you cant delete this ACName.');", true);
                                                                return;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Performa Invoice so you cant delete this ACName.');", true);
                                                            return;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Commercial Invoice so you cant delete this ACName.');", true);
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Performa Invoice so you cant delete this ACName.');", true);
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Purchase Challan so you cant delete this ACName.');", true);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Ledger Entry so you cant delete this ACName.');", true);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Ledger Entry so you cant delete this ACName.');", true);
                                        return;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Against Bill Entry so you cant delete this ACName.');", true);
                                    return;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Bank-Cash-JV Entry so you cant delete this ACName.');", true);
                                return;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Bank-Cash-JV Entry so you cant delete this ACName.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name used in Vat Type Master so you cant delete this ACName.');", true);
                        return;
                    }
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
        }
    }
    protected void gvaclist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnaccount_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/AccountMaster.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvaclist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvaclist.PageIndex = e.NewPageIndex;
        fillgrid();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (txtsearchaccountname.Text != "" && txtsearchaccountname.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = txtsearchaccountname.Text;
            DataTable dtall = new DataTable();
            DataTable dtdata = new DataTable();
            if (Request.Cookies["ForLogin"]["username"] == "admin@airmax.so")
            {
                dtdata = famclass.selectallaccountdata();
            }
            else
            {
                dtdata = famclass.selectopcldrcrdataacnamewise(li);
            }
            //for (int i = 0; i < dtdata.Rows.Count; i++)
            //{
            //    li.acname = dtdata.Rows[i]["acname"].ToString();
            //    DataTable dtsingle = new DataTable();
            //    dtsingle = famclass.selectopcldrcrdataacnamewise(li);
            //    if (dtsingle.Rows.Count > 0)
            //    {
            //        dtall.Merge(dtsingle);
            //    }
            //}
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvaclist.Visible = true;
                gvaclist.DataSource = dtdata;
                gvaclist.DataBind();
            }
            else
            {
                gvaclist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Rercord Found for Account Master.";
            }

        }
        else
        {
            fillgrid();
        }
    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        string Xrepname = "Account List Report";
        //Int64 pono = Convert.ToInt64(e.CommandArgument);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
    protected void gvaclist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //var gv = (GridView)e.Row.FindControl("gvemember");
            var lblgstno = (Label)e.Row.FindControl("lblgstno");
            var lblpan = (Label)e.Row.FindControl("lblpan");
            var lblactype = (Label)e.Row.FindControl("lblactype");
            if (lblactype.Text == "General")
            {
                Int64 aa = lblgstno.Text.Length;
                if (aa > 0)
                {
                    if (aa != 15)
                    {
                        e.Row.BackColor = Color.Orange;
                    }
                    else if (aa == 15)
                    {
                        string two = lblgstno.Text.Substring(0, 2);
                        var isValidNumber = Regex.IsMatch(two, @"^[0-9]+(\.[0-9]+)?$");
                        if (isValidNumber == false)
                        {
                            e.Row.BackColor = Color.Orange;
                        }
                        string s = lblgstno.Text.Substring(0, 2);
                        int result;
                        if (int.TryParse(s, out result))
                        {
                            int two5 = Convert.ToInt16(lblgstno.Text.Substring(0, 2));
                            if (two5 > 37 && two5 < 1)
                            {
                                e.Row.BackColor = Color.Orange;
                            }
                        }
                        else
                        {
                            e.Row.BackColor = Color.Orange;
                        }
                        if (lblpan.Text != "")
                        {
                            string two1 = lblgstno.Text.Substring(2, 10);
                            //var isValidNumber1 = Regex.IsMatch(two1, @"^[0-9]+(\.[0-9]+)?$");
                            if (two1 != lblpan.Text)
                            {
                                e.Row.BackColor = Color.Orange;
                            }
                        }
                        string two2 = lblgstno.Text.Substring(12, 1);
                        var isValidNumber1 = Regex.IsMatch(two2, @"^[0-9]+(\.[0-9]+)?$");
                        if (isValidNumber1 == false)
                        {
                            e.Row.BackColor = Color.Orange;
                        }
                        string two4 = lblgstno.Text.Substring(13, 1);
                        var isValidNumber3 = Regex.IsMatch(two4, @"^[A-Za-z]$");
                        if (isValidNumber3 == false)
                        {
                            e.Row.BackColor = Color.Orange;
                        }
                    }
                    else
                    {
                        //e.Row.BackColor = Color.Orange;
                    }
                    //string two3 = txtgstno.Text.Substring(14, 1);
                    //var isValidNumber2 = Regex.IsMatch(two3, @"^[0-9]+(\.[0-9]+)?$");
                    //if (isValidNumber2 == false)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fifteenth Letter of GST Number must be Numeric.');", true);
                    //    return;
                    //}
                }
                else if (aa == 0)
                {
                    e.Row.BackColor = Color.Red;
                }
            }
        }
    }
}