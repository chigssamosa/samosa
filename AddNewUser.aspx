﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AddNewUser.aspx.cs" Inherits="AddNewUser" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Add New User</span><br />
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Company NO.</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtcompanyid" runat="server" CssClass="form-control" Width="250px"
                    ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Company No."
                    Text="*" ControlToValidate="txtcompanyid" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    User Name</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter User Name."
                    Text="*" ControlToValidate="txtusername" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Password</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtpassword" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Enter Password."
                    Text="*" ControlToValidate="txtpassword" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Confirm Password</label></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtconfirmpassword" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Enter Confirm Password."
                    Text="*" ControlToValidate="txtconfirmpassword" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Enter Same Password.Password is different."
                    Text="*" ControlToCompare="txtpassword" ControlToValidate="txtconfirmpassword"
                    ValidationGroup="val"></asp:CompareValidator>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Role</label></div>
            <div class="col-md-4">
                <asp:DropDownList ID="drprole" runat="server" CssClass="form-control" Width="250px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Select Role."
                    Text="*" ControlToValidate="drprole" InitialValue="--SELECT--" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Active User</label></div>
            <div class="col-md-4">
                <asp:CheckBox ID="chkactive" runat="server" />
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server"  ValidationGroup="val"
                    ShowMessageBox="true" ShowSummary="false" />
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="200px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvuserlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="select" runat="server" ImageUrl="~/images/buttons/Edit.jpg"
                                        ToolTip="Delete" Height="20px" Width="20px" CommandArgument='<%# bind("id") %>'
                                        CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Code" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblusername" ForeColor="Black" runat="server" Text='<%# bind("username") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Password" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblpassword" runat="server" ForeColor="#505050" Text='<%# bind("password") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IS Active" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblisactive" runat="server" ForeColor="#505050" Text='<%# bind("isactive") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
