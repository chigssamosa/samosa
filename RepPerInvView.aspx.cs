﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RepPerInvView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string path = Request["path"].ToString();
            fbarcode.Attributes.Add("src", "performainvoice/" + path + "");//Server.MapPath("//salesinvoice//salesinvoice_50004.pdf")
        }
    }
}