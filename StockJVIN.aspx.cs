﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;

public partial class StockJVIN : System.Web.UI.Page
{
    ForSalesChallan fscclass = new ForSalesChallan();
    ForStockJV fsjvclass = new ForStockJV();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "insert")
            {
                getbrno();
                txtjvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                txtuser.Text = Request.Cookies["ForLogIn"]["username"];
                Session["dtpitemssjvin"] = CreateTemplate();
                fillitemnamedrop();
                fillacnamedrop();
            }
            else
            {
                filleditdata();
                fillitemnamedrop();
                fillacnamedrop();
            }
            //fillbillcombo();
            Page.SetFocus(drpacname);
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.inout = "IN";
        DataTable dtvno = new DataTable();
        dtvno = fsjvclass.selectchallanno(li);
        if (dtvno.Rows.Count > 0)
        {
            txtchallanno.Text = (Convert.ToInt64(dtvno.Rows[0]["challanno"].ToString()) + 1).ToString();
        }
        else
        {
            txtchallanno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.strchallanno = Request["challanno"].ToString();
        li.inout = "IN";
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        DataTable dtd = new DataTable();
        dtd = fsjvclass.selectalldatafromchallannomaster(li);
        if (dtd.Rows.Count > 0)
        {
            txtchallanno.Text = dtd.Rows[0]["challanno"].ToString();
            txtjvdate.Text = Convert.ToDateTime(dtd.Rows[0]["challandate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtuser.Text = dtd.Rows[0]["uname"].ToString();
           drpacname.SelectedValue = dtd.Rows[0]["acname"].ToString();
            btnsaveall.Text = "Update";
            DataTable dtdata = new DataTable();
            dtdata = fsjvclass.selectalldatafromchallannoitems(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvitem.Visible = true;
                gvitem.DataSource = dtdata;
                gvitem.DataBind();
            }
            else
            {
                gvitem.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Stock JV IN Data Found.";
            }
        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemssjvin"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtstockqty.Text;
            dr["rate"] = txtrate.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemssjvin"] = dt;
            this.bindgrid();
        }
        else
        {
            li.strchallanno = txtchallanno.Text;
            li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.acname = drpacname.SelectedItem.Text;
            li.itemname = drpitemname.SelectedItem.Text;
            li.qty = Convert.ToDouble(txtstockqty.Text);
            li.rate = Convert.ToDouble(txtrate.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.inout = "IN";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fsjvclass.insertitemdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strchallanno + " Stock JVIN Item inserted.";
            faclass.insertactivity(li);
            DataTable dtdata = new DataTable();
            dtdata = fsjvclass.selectalldatafromchallannoitems(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvitem.Visible = true;
                gvitem.DataSource = dtdata;
                gvitem.DataBind();
            }
            else
            {
                gvitem.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Stock JV IN Data Found.";
            }
        }
        fillitemnamedrop();
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemssjvin"] as DataTable;
        gvitem.DataSource = dtsession;
        gvitem.DataBind();
    }

    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.cdate <= yyyear1 && li.cdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (btnsaveall.Text == "Save")
        {
            if (gvitem.Rows.Count > 0)
            {
                li.strchallanno = txtchallanno.Text;
                li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.acname = drpacname.SelectedItem.Text;
                li.inout = "IN";
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                fsjvclass.insertdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strchallanno + " Stock JVIN inserted.";
                faclass.insertactivity(li);
                for (int c = 0; c < gvitem.Rows.Count; c++)
                {
                    Label lblitemname = (Label)gvitem.Rows[c].FindControl("lblitemname");
                    Label lblqty = (Label)gvitem.Rows[c].FindControl("lblqty");
                    Label lblrate = (Label)gvitem.Rows[c].FindControl("lblrate");
                    TextBox txtgvdescr1 = (TextBox)gvitem.Rows[c].FindControl("txtgvdescr1");
                    TextBox txtgvdescr2 = (TextBox)gvitem.Rows[c].FindControl("txtgvdescr2");
                    TextBox txtgvdescr3 = (TextBox)gvitem.Rows[c].FindControl("txtgvdescr3");
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.descr1 = txtdescription1.Text;
                    li.descr2 = txtdescription2.Text;
                    li.descr3 = txtdescription3.Text;
                    fsjvclass.insertitemdata(li);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Stock JV IN Data.');", true);
                return;
            }
        }
        else
        {
            li.strchallanno = txtchallanno.Text;
            li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.acname = drpacname.SelectedItem.Text;
            li.inout = "IN";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fsjvclass.updatedata(li);
            fsjvclass.updateitemsdatedata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strchallanno + " Stock JVIN Updated.";
            faclass.insertactivity(li);
        }
        Response.Redirect("~/StockJVINList.aspx?pagename=StockJVINList");
    }

    protected void gvitem_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemssjvin"] as DataTable;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

            //DataTable dtamount = new DataTable();
            //dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
            //if (dtamount.Rows.Count > 0)
            //{            
            ////////li.amount = Convert.ToDouble(dt.Rows[0]["amount"].ToString());
            ////////li.invoiceno = SessionMgt.invoiceno;
            ////////li.acname = dt.Rows[0]["acname"].ToString();
            ////////li.issipi = SessionMgt.issipi;
            ////////DataTable dtsi = new DataTable();
            ////////dtsi = fbrclass.selectamountforupdation(li);
            ////////li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
            ////////li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
            ////////fbrclass.updatesidata11(li);
            //}

            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            ////////li.voucherno = Convert.ToInt64(txtvoucherno.Text);

            ////////fbrclass.deletefromshadowtable(li);
            Session["dtpitemssjvin"] = dt;
            this.bindgrid();

        }
        else
        {
            if (gvitem.Rows.Count > 1)
            {
                Label lblid = (Label)gvitem.Rows[e.RowIndex].FindControl("lblid");
                li.id = Convert.ToInt64(lblid.Text);
                fsjvclass.deleteitemdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Stock JVIN Item Deleted.";
                faclass.insertactivity(li);
                li.strchallanno = txtchallanno.Text;
                li.inout = "IN";
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                //fsjvclass.insertitemdata(li);
                DataTable dtdata = new DataTable();
                dtdata = fsjvclass.selectalldatafromchallannoitems(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvitem.Visible = true;
                    gvitem.DataSource = dtdata;
                    gvitem.DataBind();
                }
                else
                {
                    gvitem.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Stock JV IN Data Found.";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
    }
    protected void txtchallanno_TextChanged(object sender, EventArgs e)
    {
        if (txtchallanno.Text.Trim() != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.inout = "IN";
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtvno = new DataTable();
            dtvno = fsjvclass.checkchallanno(li);
            if (dtvno.Rows.Count > 0)
            {
                getbrno();
                Page.SetFocus(txtjvdate);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Challan NO Already Exist.');", true);
                return;
            }
        }
    }
}