﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <div class="row">
            <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red" Font-Size="Larger"></asp:Label>
            <div class="col-md-12">
                <span style="color: white; background-color: Red">Pending Purchase Order</span>
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="200px" Width="98%">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblemptypendingpo" runat="server" ForeColor="Red" Font-Size="Larger"
                                Font-Bold="true"></asp:Label>
                            <asp:GridView ID="gvpendingpo" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                CssClass="table table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="PO. NO." SortExpression="companyname" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpono" runat="server" ForeColor="#505050" Text='<%# bind("pono") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Date" SortExpression="companyname" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <%#Convert.ToDateTime(Eval("podate")).ToString("dd-MM-yyyy") %>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FollowUp Date" SortExpression="companyname" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <%--<%#Convert.ToDateTime(Eval("followupdate")).ToString("dd-MM-yyyy") %>--%>
                                            <asp:TextBox ID="txtfollowupdate" runat="server" Text='<%# Convert.ToDateTime(Eval("followupdate")).ToString("dd-MM-yyyy") %>'
                                                Width="100px" AutoPostBack="True" OnTextChanged="txtfollowupdate_TextChanged"
                                                CausesValidation="true" ValidationGroup="test"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                                                ErrorMessage="Followup date must be dd-MM-yyyy" ValidationGroup="test" ForeColor="Red"
                                                ControlToValidate="txtfollowupdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details" SortExpression="companyname">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblfollowupdetails" runat="server" ForeColor="#505050" Text='<%# bind("followupdetails") %>'></asp:Label>--%>
                                            <asp:TextBox ID="txtfollowupdetails" runat="server" Text='<%# bind("followupdetails") %>'></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtfollowupdetails_TextChanged"--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details" SortExpression="companyname">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblfollowupdetails1" runat="server" ForeColor="#505050" Text='<%# bind("followupdetails1") %>'></asp:Label>--%>
                                            <asp:TextBox ID="txtfollowupdetails1" runat="server" Text='<%# bind("followupdetails1") %>'></asp:TextBox><%--AutoPostBack="true" OnTextChanged="txtfollowupdetails1_TextChanged"--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Name" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemname" runat="server" ForeColor="#505050" Text='<%# bind("itemname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty." SortExpression="companyname" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblqty" runat="server" ForeColor="#505050" Text='<%# bind("qty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <%--<span style="color: white; background-color: Red">Activity</span>--%>
                <asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick">
                </asp:Timer>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span style="color: white; background-color: Red">Negative Stock(<asp:Label ID="lbltotneg"
                    runat="server" Text="Label"></asp:Label>)</span>
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%" Height="200px">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblemptystock" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="gvstocklist" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                CssClass="table table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="item Name" SortExpression="Csnm">
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="unit" SortExpression="Csnm">
                                        <ItemTemplate>
                                            <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Op. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblopqty" ForeColor="Black" runat="server" Text='<%# bind("opqty") %>'></asp:Label>
                                            <%--<%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy")%>--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inward" SortExpression="Csnm" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpqty" ForeColor="Black" runat="server" Text='<%# bind("totp") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outward" SortExpression="Csnm" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsqty" ForeColor="Black" runat="server" Text='<%# bind("tots") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Qty." SortExpression="Csnm" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("stockqty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cl. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("totalqty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span style="color: white; background-color: Red">Recent Activity</span>
                <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Width="100%" Height="200px">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblactivity" runat="server" ForeColor="Red" Text="1"></asp:Label>
                            <asp:GridView ID="gvactivity" runat="server" AutoGenerateColumns="false" Height="0px"
                                CssClass="table table-bordered" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="activity" HeaderText="Activity" ItemStyle-Width="70%" />
                                    <asp:BoundField DataField="uname" HeaderText="User Name" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="udate" HeaderText="Date" ItemStyle-Width="15%" />
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
