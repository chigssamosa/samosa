﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepOutstandingDaywise.aspx.cs" Inherits="RepOutstandingDaywise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <asp:DropDownList ID="drptype" runat="server" CssClass="form-control">
                    <asp:ListItem>Debitors</asp:ListItem>
                    <asp:ListItem>Advance From Debitors</asp:ListItem>
                    <asp:ListItem>Creditors</asp:ListItem>
                    <asp:ListItem>Advance To Creditors</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnprint" runat="server" Text="Print" class="btn btn-default forbutton"
                    OnClick="btnprint_Click" /></div>
        </div>
    </div>
</asp:Content>
