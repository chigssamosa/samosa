﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;
using System.Data;
using System.Data.SqlTypes;

public partial class RepJV : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> GetAccountname(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallbankname(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][2].ToString());
    //    }
    //    return CountryNames;
    //}

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> Getvono(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    li.acname = SessionMgt.acname;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallvnofromjv1(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][0].ToString());
    //    }
    //    return CountryNames;
    //}
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }

        string Xrepname = "JV Report";
        //Int64 vno = Convert.ToInt64(txtvono.Text);
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////string Xrepname = "Ledger Report";
        ////////////Int64 vno = Convert.ToInt64(txtvono.Text);
        ////////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
    //protected void txtacname_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtacname.Text != string.Empty)
    //    {
    //        SessionMgt.acname = txtacname.Text;
    //    }
    //}

}