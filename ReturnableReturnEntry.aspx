﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReturnableReturnEntry.aspx.cs" Inherits="ReturnableReturnEntry" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Returnble Return Entry</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Challan No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtchallanno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getacname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtccode" runat="server" CssClass="form-control" Width="150px" AutoPostBack="True"
                            OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtccode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Order No.<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtorderno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtorderno_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtorderno"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getsono">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtorderdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            LR No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtlrno" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtlrdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Despatched By<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtdespatchedby" runat="server" CssClass="form-control" Width="393px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Freight Rs.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtfreightrs" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtchallanno1" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)" ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row" style="border: 1px solid;">
                    <div class="col-md-4 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Item Name<span style="color: Red; font-size: 20px;">*</span></label>
                    </div>
                    <div class="col-md-1 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Qty<span style="color: Red; font-size: 20px;">*</span></label>
                    </div>
                    <div class="col-md-1 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Rate<span style="color: Red; font-size: 20px;">*</span></label>
                    </div>
                    <div class="col-md-1 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Amount<span style="color: Red; font-size: 20px;">*</span></label>
                    </div>
                    <div class="col-md-1 text-center">
                        <label class="control-label">
                            Unit<span style="color: Red; font-size: 20px;">*</span>
                        </label>
                    </div>
                </div>
                <div class="row" style="border: 1px solid;">
                    <div class="col-md-4 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Description 1</label>
                    </div>
                    <div class="col-md-4 text-center" style="border-right: 1px solid;">
                        <label class="control-label">
                            Description 2</label>
                    </div>
                    <div class="col-md-3 text-center">
                        <label class="control-label">
                            Description 3</label>
                    </div>
                </div>
                <div class="row" style="border: 1px solid;">
                    <div class="col-md-4" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                            OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtitemname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getitemname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" onkeypress="javascript:return isNumber (event)"
                            AutoPostBack="True" OnTextChanged="txtqty_TextChanged"></asp:TextBox>
                    </div>
                    <div class="col-md-1" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" onkeypress="javascript:return isNumber (event)"
                            AutoPostBack="True" OnTextChanged="txtrate_TextChanged"></asp:TextBox>
                    </div>
                    <div class="col-md-1" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="border: 1px solid;">
                    <div class="col-md-4" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtdesc" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-4" style="border-right: 1px solid;">
                        <asp:TextBox ID="txtdesc1" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtdesc2" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                            ValidationGroup="valgvtool" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valgvtool" />
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="95%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                        BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" GridLines="None"
                        CellSpacing="1" OnRowDeleting="rptlist_RowDeleting" OnRowCommand="rptlist_RowCommand">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" runat="server" ForeColor="#505050" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" runat="server" ForeColor="#505050" Text='<%# bind("qty") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' AutoPostBack="true"
                                        OnTextChanged="txtgridqty_TextChanged" Width="50px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" runat="server" ForeColor="#505050" Text='<%# bind("rate") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridrate" runat="server" AutoPostBack="true" Text='<%# bind("rate") %>'
                                        OnTextChanged="txtgridrate_TextChanged" Width="80px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" runat="server" ForeColor="#505050" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Return Remarks" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtretremarks" runat="server" Text='<%# bind("retremarks") %>' Width="150px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="dispatchedby" SortExpression="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" runat="server" ForeColor="Black" Text='<%# bind("unit") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescr" runat="server" ForeColor="Black" Text='<%# bind("descr") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescr1" runat="server" ForeColor="Black" Text='<%# bind("descr1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescr2" runat="server" ForeColor="Black" Text='<%# bind("descr2") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                        <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#594B9C" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#33276A" />
                    </asp:GridView>
                </asp:Panel>
            </div>
            <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                ValidationGroup="valtool" OnClick="btnsaveall_Click" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valtool" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
                Text="*" ValidationGroup="valtool" ControlToValidate="txtdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter name."
                Text="*" ValidationGroup="valtool" ControlToValidate="txtname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valtool" ControlToValidate="txtccode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter order no."
                Text="*" ValidationGroup="valtool" ControlToValidate="txtorderno"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter despatched by."
                Text="*" ValidationGroup="valtool" ControlToValidate="txtdespatchedby"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter item name."
                Text="*" ValidationGroup="valgvtool" ControlToValidate="txtitemname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter qty."
                Text="*" ValidationGroup="valgvtool" ControlToValidate="txtqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvtool" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valgvtool" ControlToValidate="txtamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvtool" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                ControlToValidate="txtdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Order Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                ControlToValidate="txtorderdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Lr Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                ControlToValidate="txtlrdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <div class="row">
                <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
                <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                    TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                </asp:ModalPopupExtender>
                <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
                    Width="95%" Style="display: none">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <span style="color: Red;"><b>Sales Challan Selection</b></span></div>
                    </div>
                    <div class="row">
                        <hr />
                    </div>
                    <div class="row">
                        <div class="col-md-1 text-left">
                            <b>A/C Name :</b></div>
                        <div class="col-md-7">
                            <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
                    </div>
                    <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                    <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                        OnClick="btnselectitem_Click" />
                    <div class="row">
                        <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                            <asp:Label ID="lblemptysc" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="gvsaleschallans" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                CssClass="table table-bordered" OnRowCommand="gvsaleschallans_RowCommand">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strscno") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                        <ItemTemplate>
                                            <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("scno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("scdate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                        <ItemTemplate>
                                            <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" GridLines="None"
                                CellSpacing="1" OnRowDeleting="GridView1_RowDeleting" OnRowCommand="GridView1_RowCommand">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />--%>
                                            <asp:CheckBox ID="chkselect" runat="server" />
                                        </ItemTemplate>
                                        <%--<HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                                ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemname" runat="server" ForeColor="#505050" Text='<%# bind("itemname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblqty" runat="server" ForeColor="#505050" Text='<%# bind("qty") %>'></asp:Label>
                                            <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' AutoPostBack="true"
                                                OnTextChanged="txtgridqty_TextChanged" Width="50px"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rate" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrate" runat="server" ForeColor="#505050" Text='<%# bind("rate") %>'></asp:Label>
                                            <asp:TextBox ID="txtgridrate" runat="server" AutoPostBack="true" Text='<%# bind("rate") %>'
                                                OnTextChanged="txtgridrate_TextChanged" Width="80px"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblamount" runat="server" ForeColor="#505050" Text='<%# bind("amount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return Remarks" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtretremarks" runat="server" Text='<%# bind("retremarks") %>' Width="150px"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="dispatchedby" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblunit" runat="server" ForeColor="Black" Text='<%# bind("unit") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldescr" runat="server" ForeColor="Black" Text='<%# bind("descr") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description1" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldescr1" runat="server" ForeColor="Black" Text='<%# bind("descr1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description2" SortExpression="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldescr2" runat="server" ForeColor="Black" Text='<%# bind("descr2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                                ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                                CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#33276A" />
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtorderdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtorderdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtorderdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
