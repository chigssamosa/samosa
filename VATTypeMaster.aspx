﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VATTypeMaster.aspx.cs" Inherits="VATTypeMaster" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">VAT Type Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            <%--Type--%></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txttype" runat="server" CssClass="form-control" Width="150px" Visible="false"></asp:TextBox><asp:TextBox
                            ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Type Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txttypename" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-3">
                        <center>
                            <u>
                                <label class="control-label">
                                    Account Code</label></u></center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            VAT%<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvat" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvatdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Add Tax%<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddtax" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddtaxdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Central Sales Tax%</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtcentralsalestax" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtcentralsalestaxdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Sales Tax%</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtsalestax" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Surcharge%</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtsurcharge" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Service Tax %</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtservicetaxp" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtservicetaxdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Form</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtform" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valvattype" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valvattype" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter type name."
                            Text="*" ValidationGroup="valvattype" ControlToValidate="txttypename"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter vat%."
                            Text="*" ValidationGroup="valvattype" ControlToValidate="txtvat"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter add tax%."
                            Text="*" ValidationGroup="valvattype" ControlToValidate="txtaddtax"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
