﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForStockJV
/// </summary>
public class ForStockJV : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForStockJV()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectchallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from StockJVMaster where cno=@cno and inout=@inout order by convert(bigint,challanno) desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkchallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from StockJVMaster where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.challanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into StockJVMaster (challanno,challandate,acname,inout,cno,uname,udate) values (@challanno,@challandate,@acname,@inout,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVMaster set challandate=@challandate,acname=@acname,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno and inout=@inout", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateitemsdatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set challandate=@challandate,acname=@acname,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno and inout=@inout", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into StockJVItems (challanno,challandate,acname,itemname,qty,rate,descr1,descr2,descr3,inout,cno,uname,udate) values (@challanno,@challandate,@acname,@itemname,@qty,@rate,@descr1,@descr2,@descr3,@inout,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from StockJVItems where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from StockJVItems where challanno=@challanno and inout=@inout and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from StockJVMaster where challanno=@challanno and inout=@inout and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalldatafromchallannoitems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from StockJVItems where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.strchallanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromchallannomaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from StockJVMaster where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.strchallanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from StockJVMaster inner join StockJVItems on StockJVMaster.challanno=StockJVItems.challanno where StockJVMaster.cno=@cno and StockJVMaster.inout=@inout and StockJVItems.inout=@inout order by convert(bigint,StockJVMaster.challanno) desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}