﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;


/// <summary>
/// Summary description for ForPackingMaterial
/// </summary>
public class ForPackingMaterial : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForPackingMaterial()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallpmmasterdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmmaster where cno=@cno and istype=@istype order by voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpmmasterdatafromvoucherno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmmaster inner join pmitems on pmmaster.voucherno=pmitems.voucherno where pmmaster.voucherno=@voucherno and pmmaster.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpmmasterdatafromvoucherno2(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmmaster where voucherno=@voucherno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpmitemsdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmitems where voucherno=@voucherno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }


    public void insertpmmasterdeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into pmmaster (voucherno,vdate,name,clientcode,rmcno,tdcno,transportname,lrno,lrdate,vehicleno,drivername,dlicenseno,mobileno,istype,cno,uname,udate,materiall,materialr) values (@voucherno,@vdate,@name,@clientcode,@rmcno,@tdcno,@transportname,@lrno,@lrdate,@vehicleno,@drivername,@dlicenseno,@mobileno,@istype,@cno,@uname,@udate,@materiall,@materialr)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@vdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@clientcode", li.ccode);
            //cmd.Parameters.AddWithValue("@dispatchby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@rmcno", li.strrmcno);
            cmd.Parameters.AddWithValue("@tdcno", li.strtdcno);
            cmd.Parameters.AddWithValue("@transportname", li.transportname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@vehicleno", li.vehicleno);
            cmd.Parameters.AddWithValue("@drivername", li.drivername);
            cmd.Parameters.AddWithValue("@dlicenseno", li.licenseno);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@materiall", li.materiall);
            cmd.Parameters.AddWithValue("@materialr", li.materialr);
            //cmd.Parameters.AddWithValue("@remarks", li.remarks);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepmmasterdeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update pmmaster set vdate=@vdate,name=@name,clientcode=@clientcode,rmcno=@rmcno,tdcno=@tdcno,transportname=@transportname,lrno=@lrno,lrdate=@lrdate,vehicleno=@vehicleno,drivername=@drivername,dlicenseno=@dlicenseno,mobileno=@mobileno,istype=@istype,uname=@uname,udate=@udate,materiall=@materiall,materialr=@materialr where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@vdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@clientcode", li.ccode);
            //cmd.Parameters.AddWithValue("@dispatchby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@rmcno", li.strrmcno);
            cmd.Parameters.AddWithValue("@tdcno", li.strtdcno);
            cmd.Parameters.AddWithValue("@transportname", li.transportname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@vehicleno", li.vehicleno);
            cmd.Parameters.AddWithValue("@drivername", li.drivername);
            cmd.Parameters.AddWithValue("@dlicenseno", li.licenseno);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@materiall", li.materiall);
            cmd.Parameters.AddWithValue("@materialr", li.materialr);
            //cmd.Parameters.AddWithValue("@remarks", li.remarks);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepmitemsdatedeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update pmitems set vdate=@vdate,uname=@uname,udate=@udate where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@vdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            //cmd.Parameters.AddWithValue("@remarks", li.remarks);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertpmitemserdeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into pmitems (voucherno,vdate,dcno,cno,uname,udate) values (@voucherno,@vdate,@dcno,@cno,@uname,@udate)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@vdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@dcno", li.dcno1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateispackinginscmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set ispacking=@ispacking where strscno=@strscno and cno=@cno", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@strscno", li.strscno);            
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@ispacking", li.ispacking);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateispackinginscmastersetnull(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set ispacking=NULL where strscno=@strscno and cno=@cno", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            //cmd.Parameters.AddWithValue("@ispacking", li.ispacking);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateispackingintoodelmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelMaster set ispacking=@ispacking where voucherno=@voucherno and cno=@cno", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.vnono1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@ispacking", li.ispacking);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateispackingintoodelmastersetnull(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelMaster set ispacking=NULL where voucherno=@voucherno and cno=@cno", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.vnono1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            //cmd.Parameters.AddWithValue("@ispacking", li.ispacking);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepmitemsedatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from pmitems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open(); 
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepmitemsedatafromvoucherno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from pmitems where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepmmasteredatafromvoucherno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from pmmaster where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();            
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepmitemsdeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update pmitems set dcno=@dcno where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@dcno", li.dcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectunusedpmvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and ispmused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectlastpmvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmmaster where cno=@cno order by vdate desc,voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispmused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsaleschallanforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallanforpopupforrm(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where ctype='RM' and cno=@cno and acname=@acname and ispacking is null", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallanforpopupfort(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where ctype='T' and cno=@cno and acname=@acname and ispacking is null", con);// status!='Closed' and
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallanforpopupfortagain(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where cno=@cno and name=@acname and ispacking is null", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispmused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalltransportname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Transport Name'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}