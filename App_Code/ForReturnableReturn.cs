﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForReturnableReturn
/// </summary>
public class ForReturnableReturn : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForReturnableReturn()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallrrmasterdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from rrmaster where cno=@cno order by challanno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallrritemsdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from rritems where cno=@cno and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.challanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectrrmasterdatafromchallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from rrmaster where cno=@cno and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.challanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertrrmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into rrmaster (challanno,cdate,name,ccode,sono,sodate,lrno,lrdate,dispatchedby,freight,cno,uname,udate) values (@challanno,@cdate,@name,@ccode,@sono,@sodate,@lrno,@lrdate,@dispatchedby,@freight,@cno,@uname,@udate)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cdate", li.cdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@dispatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@freight", li.freight);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);            
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterrmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update rrmaster set cdate=@cdate,name=@name,ccode=@ccode,sono=@sono,sodate=@sodate,lrno=@lrno,lrdate=@lrdate,dispatchedby=@dispatchedby,freight=@freight,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cdate", li.cdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@dispatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@freight", li.freight);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterritemsdatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update rritems set cdate=@cdate,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cdate", li.cdate);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertrritemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into rritems (challanno,cdate,itemname,qty,rate,amount,unit,descr,descr1,descr2,cno,uname,udate,vid,retremarks) values (@challanno,@cdate,@itemname,@qty,@rate,@amount,@unit,@descr,@descr1,@descr2,@cno,@uname,@udate,@vid,@retremarks)", con);
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cdate", li.cdate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@retremarks", li.retremarks);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterritemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update rritems set itemname=@itemname,qty=@qty,rate=@rate,amount=@amount,unit=@unit,descr=@descr,descr1=@descr1,descr2=@descr2,uname=@uname,udate=@udate,retremarks=@retremarks where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);            
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@retremarks", li.retremarks);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleterrmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from rrmaster where challanno=@challanno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleterritemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from rritems where challanno=@challanno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.challanno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleterritemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from rritems where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalldatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from itemmaster where itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedrrvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isrrused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isrrused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.challanno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsaleschallanforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatafromscnoforrr(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCMaster.strscno=@scno and SCMaster.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.strscno);
            //da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatafromscnoforrr12(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCMaster.strscno=@scno and SCMaster.cno=@cno and SCItems.id in (" + li.remarks + ")", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.strscno);
            //da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isrrused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.challanno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}