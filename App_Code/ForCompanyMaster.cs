﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForCompanyMaster
/// </summary>
public class ForCompanyMaster
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForCompanyMaster()
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
    }

    public void insertcompanymasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into cmast (csnm,cname,address,location,emailid,contact1,contact2,faxno,tanno,vatno,cstno,eccno,servicetaxno,rocno,imexcode,yearoe,bank1,bank2,emppt,comppt,pfno,panno,gstno,uname,udate) values (@csnm,@cname,@address,@location,@emailid,@contact1,@contact2,@faxno,@tanno,@vatno,@cstno,@eccno,@servicetaxno,@rocno,@imexcode,@yearoe,@bank1,@bank2,@emppt,@comppt,@pfno,@panno,@gstno,@uname,@udate) SELECT SCOPE_IDENTITY()", con);
            cmd.Parameters.AddWithValue("@csnm", li.csnm);
            cmd.Parameters.AddWithValue("@cname", li.cname);
            cmd.Parameters.AddWithValue("@address", li.address);
            cmd.Parameters.AddWithValue("@location", li.location);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@contact1", li.contact1);
            cmd.Parameters.AddWithValue("@contact2", li.contact2);
            cmd.Parameters.AddWithValue("@faxno", li.faxno);
            cmd.Parameters.AddWithValue("@tanno", li.tanno);
            cmd.Parameters.AddWithValue("@vatno", li.vatno);
            cmd.Parameters.AddWithValue("@cstno", li.cstno);
            cmd.Parameters.AddWithValue("@eccno", li.eccno);
            cmd.Parameters.AddWithValue("@servicetaxno", li.servicetaxno);
            cmd.Parameters.AddWithValue("@rocno", li.rocno);
            cmd.Parameters.AddWithValue("@imexcode", li.imexcode);
            cmd.Parameters.AddWithValue("@yearoe", li.yearoe);
            cmd.Parameters.AddWithValue("@bank1", li.bank1);
            cmd.Parameters.AddWithValue("@bank2", li.bank2);
            cmd.Parameters.AddWithValue("@emppt", li.emppt);
            cmd.Parameters.AddWithValue("@comppt", li.comppt);
            cmd.Parameters.AddWithValue("@pfno", li.pfno);
            cmd.Parameters.AddWithValue("@panno", li.panno);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.cno = Convert.ToInt64(cc.ToString());
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatecompanymasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update cmast set csnm=@csnm,cname=@cname,address=@address,location=@location,emailid=@emailid,contact1=@contact1,contact2=@contact2,faxno=@faxno,tanno=@tanno,vatno=@vatno,cstno=@cstno,eccno=@eccno,servicetaxno=@servicetaxno,rocno=@rocno,imexcode=@imexcode,yearoe=@yearoe,bank1=@bank1,bank2=@bank2,emppt=@emppt,comppt=@comppt,pfno=@pfno,panno=@panno,gstno=@gstno,uname=@uname,udate=@udate where cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@csnm", li.csnm);
            cmd.Parameters.AddWithValue("@cname", li.cname);
            cmd.Parameters.AddWithValue("@address", li.address);
            cmd.Parameters.AddWithValue("@location", li.location);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@contact1", li.contact1);
            cmd.Parameters.AddWithValue("@contact2", li.contact2);
            cmd.Parameters.AddWithValue("@faxno", li.faxno);
            cmd.Parameters.AddWithValue("@tanno", li.tanno);
            cmd.Parameters.AddWithValue("@vatno", li.vatno);
            cmd.Parameters.AddWithValue("@cstno", li.cstno);
            cmd.Parameters.AddWithValue("@eccno", li.eccno);
            cmd.Parameters.AddWithValue("@servicetaxno", li.servicetaxno);
            cmd.Parameters.AddWithValue("@rocno", li.rocno);
            cmd.Parameters.AddWithValue("@imexcode", li.imexcode);
            cmd.Parameters.AddWithValue("@yearoe", li.yearoe);
            cmd.Parameters.AddWithValue("@bank1", li.bank1);
            cmd.Parameters.AddWithValue("@bank2", li.bank2);
            cmd.Parameters.AddWithValue("@emppt", li.emppt);
            cmd.Parameters.AddWithValue("@comppt", li.comppt);
            cmd.Parameters.AddWithValue("@pfno", li.pfno);
            cmd.Parameters.AddWithValue("@panno", li.panno);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletecompanymasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from cmast where cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallcompany()
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM cmast order by cno desc", con);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

    public DataTable selectallcompanyforselection(LogicLayer li)
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM cmast where cno in (" + li.strcno + ")", con);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

    public DataTable selectlogindatafromlogintable(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from login where username=@username", con);
            da.SelectCommand.Parameters.AddWithValue("@username", li.username);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectcompanydatafromcno(LogicLayer li)
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM cmast where cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("cno", li.cno);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

}