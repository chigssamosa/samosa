﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for ForReport
/// </summary>
public class ForReport : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForReport()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallitem()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da = new SqlDataAdapter("select ROW_NUMBER() over (order by itemname ) as srno,itemname from ItemMaster", con);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitempopup()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da = new SqlDataAdapter("select (itemname+' - '+unit+' - '+description) as sf,* from ItemMaster where isblock='N' order by itemname", con);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}