﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForProject
/// </summary>
public class ForProject : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    public ForProject()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallproject1data(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from project1 where cno=@cno order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallproject2data(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from project2 where cno=@cno order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallproject2datafromccode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from project2 where cno=@cno and ccode=@ccode order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectproject1datafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from project1 where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectproject2datafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from project2 where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertproject1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into project1 (ahuno,returnaircfm,returnairtemp,returnairgrains,freshaircfm,freshairtemp,freshairgrains,mixinaircfm,mixingairtemp,mixingairgrains,totalfancfm,beforefanairtemp,beforefanairgrains,fanstaticpressure,totalfankw,totalfanbtuhr,tempdiffdegreef,afterfabairtemp,afterfanairgrains,selecteddehcfm,actualdehaircfm,dehairtempafterchw,dehairgrainsafterchw,bypassaircfm,bypassairtemp,bypassairgrains,exhaustcfm,supplycfm,sensibleloadfromheatload,diversityofsensibleloadminload,diversityofsensibleloaddiversity,suppludb,latentload,supplygrains,hotwateroutletconditiondb,hotwateroutletconditiongrains,sensibleloadforhotwatercoil,hotwatercoilcapacityofsensibleload,checkpointforrh,checkpointfortemp,checkpointfortemp1,ccode,cno,uname,udate) values (@ahuno,@returnaircfm,@returnairtemp,@returnairgrains,@freshaircfm,@freshairtemp,@freshairgrains,@mixinaircfm,@mixingairtemp,@mixingairgrains,@totalfancfm,@beforefanairtemp,@beforefanairgrains,@fanstaticpressure,@totalfankw,@totalfanbtuhr,@tempdiffdegreef,@afterfabairtemp,@afterfanairgrains,@selecteddehcfm,@actualdehaircfm,@dehairtempafterchw,@dehairgrainsafterchw,@bypassaircfm,@bypassairtemp,@bypassairgrains,@exhaustcfm,@supplycfm,@sensibleloadfromheatload,@diversityofsensibleloadminload,@diversityofsensibleloaddiversity,@suppludb,@latentload,@supplygrains,@hotwateroutletconditiondb,@hotwateroutletconditiongrains,@sensibleloadforhotwatercoil,@hotwatercoilcapacityofsensibleload,@checkpointforrh,@checkpointfortemp,@checkpointfortemp1,@ccode,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@ahuno", li.ahuno);
            cmd.Parameters.AddWithValue("@returnaircfm", li.returnaircfm);
            cmd.Parameters.AddWithValue("@returnairtemp", li.returnairtemp);
            cmd.Parameters.AddWithValue("@returnairgrains", li.returnairgrains);
            cmd.Parameters.AddWithValue("@freshaircfm", li.freshaircfm);
            cmd.Parameters.AddWithValue("@freshairtemp", li.freshairtemp);
            cmd.Parameters.AddWithValue("@freshairgrains", li.freshairgrains);
            cmd.Parameters.AddWithValue("@mixinaircfm", li.mixinaircfm);
            cmd.Parameters.AddWithValue("@mixingairtemp", li.mixingairtemp);
            cmd.Parameters.AddWithValue("@mixingairgrains", li.mixingairgrains);
            cmd.Parameters.AddWithValue("@totalfancfm", li.totalfancfm);
            cmd.Parameters.AddWithValue("@beforefanairtemp", li.beforefanairtemp);
            cmd.Parameters.AddWithValue("@beforefanairgrains", li.beforefanairgrains);
            cmd.Parameters.AddWithValue("@fanstaticpressure", li.fanstaticpressure);
            cmd.Parameters.AddWithValue("@totalfankw", li.totalfankw);
            cmd.Parameters.AddWithValue("@totalfanbtuhr", li.totalfanbtuhr);
            cmd.Parameters.AddWithValue("@tempdiffdegreef", li.tempdiffdegreef);
            cmd.Parameters.AddWithValue("@afterfabairtemp", li.afterfabairtemp);
            cmd.Parameters.AddWithValue("@afterfanairgrains", li.afterfanairgrains);
            cmd.Parameters.AddWithValue("@selecteddehcfm", li.selecteddehcfm);
            cmd.Parameters.AddWithValue("@actualdehaircfm", li.actualdehaircfm);
            cmd.Parameters.AddWithValue("@dehairtempafterchw", li.dehairtempafterchw);
            cmd.Parameters.AddWithValue("@dehairgrainsafterchw", li.dehairgrainsafterchw);
            cmd.Parameters.AddWithValue("@bypassaircfm", li.bypassaircfm);
            cmd.Parameters.AddWithValue("@bypassairtemp", li.bypassairtemp);
            cmd.Parameters.AddWithValue("@bypassairgrains", li.bypassairgrains);
            cmd.Parameters.AddWithValue("@exhaustcfm", li.exhaustcfm);
            cmd.Parameters.AddWithValue("@supplycfm", li.supplycfm);
            cmd.Parameters.AddWithValue("@sensibleloadfromheatload", li.sensibleloadfromheatload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloadminload", li.diversityofsensibleloadminload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloaddiversity", li.diversityofsensibleloaddiversity);
            cmd.Parameters.AddWithValue("@suppludb", li.suppludb);
            cmd.Parameters.AddWithValue("@latentload", li.latentload);
            cmd.Parameters.AddWithValue("@supplygrains", li.supplygrains);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiondb", li.hotwateroutletconditiondb);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiongrains", li.hotwateroutletconditiongrains);
            cmd.Parameters.AddWithValue("@sensibleloadforhotwatercoil", li.sensibleloadforhotwatercoil);
            cmd.Parameters.AddWithValue("@hotwatercoilcapacityofsensibleload", li.hotwatercoilcapacityofsensibleload);
            cmd.Parameters.AddWithValue("@checkpointforrh", li.checkpointforrh);
            cmd.Parameters.AddWithValue("@checkpointfortemp", li.checkpointfortemp);
            cmd.Parameters.AddWithValue("@checkpointfortemp1", li.checkpointfortemp1);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertproject2data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into project2 (ahuno,returnaircfm,returnairtemp,returnairgrains,freshaircfm,freshairtemp,freshairgrains,mixinaircfm,mixingairtemp,mixingairgrains,totalfancfm,beforefanairtemp,beforefanairgrains,fanstaticpressure,totalfankw,totalfanbtuhr,tempdiffdegreef,afterfabairtemp,afterfanairgrains,selecteddehcfm,actualdehaircfm,dehairtempafterchw,dehairgrainsafterchw,bypassaircfm,bypassairtemp,bypassairgrains,exhaustcfm,supplycfm,sensibleloadfromheatload,diversityofsensibleloadminload,diversityofsensibleloaddiversity,suppludb,latentload,supplygrains,hotwateroutletconditiondb,hotwateroutletconditiongrains,sensibleloadforhotwatercoil,hotwatercoilcapacityofsensibleload,checkpointforrh,checkpointfortemp,checkpointfortemp1,ccode,cno,uname,udate) values (@ahuno,@returnaircfm,@returnairtemp,@returnairgrains,@freshaircfm,@freshairtemp,@freshairgrains,@mixinaircfm,@mixingairtemp,@mixingairgrains,@totalfancfm,@beforefanairtemp,@beforefanairgrains,@fanstaticpressure,@totalfankw,@totalfanbtuhr,@tempdiffdegreef,@afterfabairtemp,@afterfanairgrains,@selecteddehcfm,@actualdehaircfm,@dehairtempafterchw,@dehairgrainsafterchw,@bypassaircfm,@bypassairtemp,@bypassairgrains,@exhaustcfm,@supplycfm,@sensibleloadfromheatload,@diversityofsensibleloadminload,@diversityofsensibleloaddiversity,@suppludb,@latentload,@supplygrains,@hotwateroutletconditiondb,@hotwateroutletconditiongrains,@sensibleloadforhotwatercoil,@hotwatercoilcapacityofsensibleload,@checkpointforrh,@checkpointfortemp,@checkpointfortemp1,@ccode,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@ahuno", li.ahuno);
            cmd.Parameters.AddWithValue("@returnaircfm", li.returnaircfm);
            cmd.Parameters.AddWithValue("@returnairtemp", li.returnairtemp);
            cmd.Parameters.AddWithValue("@returnairgrains", li.returnairgrains);
            cmd.Parameters.AddWithValue("@freshaircfm", li.freshaircfm);
            cmd.Parameters.AddWithValue("@freshairtemp", li.freshairtemp);
            cmd.Parameters.AddWithValue("@freshairgrains", li.freshairgrains);
            cmd.Parameters.AddWithValue("@mixinaircfm", li.mixinaircfm);
            cmd.Parameters.AddWithValue("@mixingairtemp", li.mixingairtemp);
            cmd.Parameters.AddWithValue("@mixingairgrains", li.mixingairgrains);
            cmd.Parameters.AddWithValue("@totalfancfm", li.totalfancfm);
            cmd.Parameters.AddWithValue("@beforefanairtemp", li.beforefanairtemp);
            cmd.Parameters.AddWithValue("@beforefanairgrains", li.beforefanairgrains);
            cmd.Parameters.AddWithValue("@fanstaticpressure", li.fanstaticpressure);
            cmd.Parameters.AddWithValue("@totalfankw", li.totalfankw);
            cmd.Parameters.AddWithValue("@totalfanbtuhr", li.totalfanbtuhr);
            cmd.Parameters.AddWithValue("@tempdiffdegreef", li.tempdiffdegreef);
            cmd.Parameters.AddWithValue("@afterfabairtemp", li.afterfabairtemp);
            cmd.Parameters.AddWithValue("@afterfanairgrains", li.afterfanairgrains);
            cmd.Parameters.AddWithValue("@selecteddehcfm", li.selecteddehcfm);
            cmd.Parameters.AddWithValue("@actualdehaircfm", li.actualdehaircfm);
            cmd.Parameters.AddWithValue("@dehairtempafterchw", li.dehairtempafterchw);
            cmd.Parameters.AddWithValue("@dehairgrainsafterchw", li.dehairgrainsafterchw);
            cmd.Parameters.AddWithValue("@bypassaircfm", li.bypassaircfm);
            cmd.Parameters.AddWithValue("@bypassairtemp", li.bypassairtemp);
            cmd.Parameters.AddWithValue("@bypassairgrains", li.bypassairgrains);
            cmd.Parameters.AddWithValue("@exhaustcfm", li.exhaustcfm);
            cmd.Parameters.AddWithValue("@supplycfm", li.supplycfm);
            cmd.Parameters.AddWithValue("@sensibleloadfromheatload", li.sensibleloadfromheatload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloadminload", li.diversityofsensibleloadminload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloaddiversity", li.diversityofsensibleloaddiversity);
            cmd.Parameters.AddWithValue("@suppludb", li.suppludb);
            cmd.Parameters.AddWithValue("@latentload", li.latentload);
            cmd.Parameters.AddWithValue("@supplygrains", li.supplygrains);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiondb", li.hotwateroutletconditiondb);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiongrains", li.hotwateroutletconditiongrains);
            cmd.Parameters.AddWithValue("@sensibleloadforhotwatercoil", li.sensibleloadforhotwatercoil);
            cmd.Parameters.AddWithValue("@hotwatercoilcapacityofsensibleload", li.hotwatercoilcapacityofsensibleload);
            cmd.Parameters.AddWithValue("@checkpointforrh", li.checkpointforrh);
            cmd.Parameters.AddWithValue("@checkpointfortemp", li.checkpointfortemp);
            cmd.Parameters.AddWithValue("@checkpointfortemp1", li.checkpointfortemp1);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateproject1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update project1 set ahuno=@ahuno,returnaircfm=@returnaircfm,returnairtemp=@returnairtemp,returnairgrains=@returnairgrains,freshaircfm=@freshaircfm,freshairtemp=@freshairtemp,freshairgrains=@freshairgrains,mixinaircfm=@mixinaircfm,mixingairtemp=@mixingairtemp,mixingairgrains=@mixingairgrains,totalfancfm=@totalfancfm,beforefanairtemp=@beforefanairtemp,beforefanairgrains=@beforefanairgrains,fanstaticpressure=@fanstaticpressure,totalfankw=@totalfankw,totalfanbtuhr=@totalfanbtuhr,tempdiffdegreef=@tempdiffdegreef,afterfabairtemp=@afterfabairtemp,afterfanairgrains=@afterfanairgrains,selecteddehcfm=@selecteddehcfm,actualdehaircfm=@actualdehaircfm,dehairtempafterchw=@dehairtempafterchw,dehairgrainsafterchw=@dehairgrainsafterchw,bypassaircfm=@bypassaircfm,bypassairtemp=@bypassairtemp,bypassairgrains=@bypassairgrains,exhaustcfm=@exhaustcfm,supplycfm=@supplycfm,sensibleloadfromheatload=@sensibleloadfromheatload,diversityofsensibleloadminload=@diversityofsensibleloadminload,diversityofsensibleloaddiversity=@diversityofsensibleloaddiversity,suppludb=@suppludb,latentload=@latentload,supplygrains=@supplygrains,hotwateroutletconditiondb=@hotwateroutletconditiondb,hotwateroutletconditiongrains=@hotwateroutletconditiongrains,sensibleloadforhotwatercoil=@sensibleloadforhotwatercoil,hotwatercoilcapacityofsensibleload=@hotwatercoilcapacityofsensibleload,checkpointforrh=@checkpointforrh,checkpointfortemp=@checkpointfortemp,checkpointfortemp1=@checkpointfortemp1,ccode=@ccode,uname=@uname,udate=@udate where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@ahuno", li.ahuno);
            cmd.Parameters.AddWithValue("@returnaircfm", li.returnaircfm);
            cmd.Parameters.AddWithValue("@returnairtemp", li.returnairtemp);
            cmd.Parameters.AddWithValue("@returnairgrains", li.returnairgrains);
            cmd.Parameters.AddWithValue("@freshaircfm", li.freshaircfm);
            cmd.Parameters.AddWithValue("@freshairtemp", li.freshairtemp);
            cmd.Parameters.AddWithValue("@freshairgrains", li.freshairgrains);
            cmd.Parameters.AddWithValue("@mixinaircfm", li.mixinaircfm);
            cmd.Parameters.AddWithValue("@mixingairtemp", li.mixingairtemp);
            cmd.Parameters.AddWithValue("@mixingairgrains", li.mixingairgrains);
            cmd.Parameters.AddWithValue("@totalfancfm", li.totalfancfm);
            cmd.Parameters.AddWithValue("@beforefanairtemp", li.beforefanairtemp);
            cmd.Parameters.AddWithValue("@beforefanairgrains", li.beforefanairgrains);
            cmd.Parameters.AddWithValue("@fanstaticpressure", li.fanstaticpressure);
            cmd.Parameters.AddWithValue("@totalfankw", li.totalfankw);
            cmd.Parameters.AddWithValue("@totalfanbtuhr", li.totalfanbtuhr);
            cmd.Parameters.AddWithValue("@tempdiffdegreef", li.tempdiffdegreef);
            cmd.Parameters.AddWithValue("@afterfabairtemp", li.afterfabairtemp);
            cmd.Parameters.AddWithValue("@afterfanairgrains", li.afterfanairgrains);
            cmd.Parameters.AddWithValue("@selecteddehcfm", li.selecteddehcfm);
            cmd.Parameters.AddWithValue("@actualdehaircfm", li.actualdehaircfm);
            cmd.Parameters.AddWithValue("@dehairtempafterchw", li.dehairtempafterchw);
            cmd.Parameters.AddWithValue("@dehairgrainsafterchw", li.dehairgrainsafterchw);
            cmd.Parameters.AddWithValue("@bypassaircfm", li.bypassaircfm);
            cmd.Parameters.AddWithValue("@bypassairtemp", li.bypassairtemp);
            cmd.Parameters.AddWithValue("@bypassairgrains", li.bypassairgrains);
            cmd.Parameters.AddWithValue("@exhaustcfm", li.exhaustcfm);
            cmd.Parameters.AddWithValue("@supplycfm", li.supplycfm);
            cmd.Parameters.AddWithValue("@sensibleloadfromheatload", li.sensibleloadfromheatload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloadminload", li.diversityofsensibleloadminload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloaddiversity", li.diversityofsensibleloaddiversity);
            cmd.Parameters.AddWithValue("@suppludb", li.suppludb);
            cmd.Parameters.AddWithValue("@latentload", li.latentload);
            cmd.Parameters.AddWithValue("@supplygrains", li.supplygrains);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiondb", li.hotwateroutletconditiondb);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiongrains", li.hotwateroutletconditiongrains);
            cmd.Parameters.AddWithValue("@sensibleloadforhotwatercoil", li.sensibleloadforhotwatercoil);
            cmd.Parameters.AddWithValue("@hotwatercoilcapacityofsensibleload", li.hotwatercoilcapacityofsensibleload);
            cmd.Parameters.AddWithValue("@checkpointforrh", li.checkpointforrh);
            cmd.Parameters.AddWithValue("@checkpointfortemp", li.checkpointfortemp);
            cmd.Parameters.AddWithValue("@checkpointfortemp1", li.checkpointfortemp1);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
    public void updateproject2data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update project2 set ahuno=@ahuno,returnaircfm=@returnaircfm,returnairtemp=@returnairtemp,returnairgrains=@returnairgrains,freshaircfm=@freshaircfm,freshairtemp=@freshairtemp,freshairgrains=@freshairgrains,mixinaircfm=@mixinaircfm,mixingairtemp=@mixingairtemp,mixingairgrains=@mixingairgrains,totalfancfm=@totalfancfm,beforefanairtemp=@beforefanairtemp,beforefanairgrains=@beforefanairgrains,fanstaticpressure=@fanstaticpressure,totalfankw=@totalfankw,totalfanbtuhr=@totalfanbtuhr,tempdiffdegreef=@tempdiffdegreef,afterfabairtemp=@afterfabairtemp,afterfanairgrains=@afterfanairgrains,selecteddehcfm=@selecteddehcfm,actualdehaircfm=@actualdehaircfm,dehairtempafterchw=@dehairtempafterchw,dehairgrainsafterchw=@dehairgrainsafterchw,bypassaircfm=@bypassaircfm,bypassairtemp=@bypassairtemp,bypassairgrains=@bypassairgrains,exhaustcfm=@exhaustcfm,supplycfm=@supplycfm,sensibleloadfromheatload=@sensibleloadfromheatload,diversityofsensibleloadminload=@diversityofsensibleloadminload,diversityofsensibleloaddiversity=@diversityofsensibleloaddiversity,suppludb=@suppludb,latentload=@latentload,supplygrains=@supplygrains,hotwateroutletconditiondb=@hotwateroutletconditiondb,hotwateroutletconditiongrains=@hotwateroutletconditiongrains,sensibleloadforhotwatercoil=@sensibleloadforhotwatercoil,hotwatercoilcapacityofsensibleload=@hotwatercoilcapacityofsensibleload,checkpointforrh=@checkpointforrh,checkpointfortemp=@checkpointfortemp,checkpointfortemp1=@checkpointfortemp1,ccode=@ccode,uname=@uname,udate=@udate where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@ahuno", li.ahuno);
            cmd.Parameters.AddWithValue("@returnaircfm", li.returnaircfm);
            cmd.Parameters.AddWithValue("@returnairtemp", li.returnairtemp);
            cmd.Parameters.AddWithValue("@returnairgrains", li.returnairgrains);
            cmd.Parameters.AddWithValue("@freshaircfm", li.freshaircfm);
            cmd.Parameters.AddWithValue("@freshairtemp", li.freshairtemp);
            cmd.Parameters.AddWithValue("@freshairgrains", li.freshairgrains);
            cmd.Parameters.AddWithValue("@mixinaircfm", li.mixinaircfm);
            cmd.Parameters.AddWithValue("@mixingairtemp", li.mixingairtemp);
            cmd.Parameters.AddWithValue("@mixingairgrains", li.mixingairgrains);
            cmd.Parameters.AddWithValue("@totalfancfm", li.totalfancfm);
            cmd.Parameters.AddWithValue("@beforefanairtemp", li.beforefanairtemp);
            cmd.Parameters.AddWithValue("@beforefanairgrains", li.beforefanairgrains);
            cmd.Parameters.AddWithValue("@fanstaticpressure", li.fanstaticpressure);
            cmd.Parameters.AddWithValue("@totalfankw", li.totalfankw);
            cmd.Parameters.AddWithValue("@totalfanbtuhr", li.totalfanbtuhr);
            cmd.Parameters.AddWithValue("@tempdiffdegreef", li.tempdiffdegreef);
            cmd.Parameters.AddWithValue("@afterfabairtemp", li.afterfabairtemp);
            cmd.Parameters.AddWithValue("@afterfanairgrains", li.afterfanairgrains);
            cmd.Parameters.AddWithValue("@selecteddehcfm", li.selecteddehcfm);
            cmd.Parameters.AddWithValue("@actualdehaircfm", li.actualdehaircfm);
            cmd.Parameters.AddWithValue("@dehairtempafterchw", li.dehairtempafterchw);
            cmd.Parameters.AddWithValue("@dehairgrainsafterchw", li.dehairgrainsafterchw);
            cmd.Parameters.AddWithValue("@bypassaircfm", li.bypassaircfm);
            cmd.Parameters.AddWithValue("@bypassairtemp", li.bypassairtemp);
            cmd.Parameters.AddWithValue("@bypassairgrains", li.bypassairgrains);
            cmd.Parameters.AddWithValue("@exhaustcfm", li.exhaustcfm);
            cmd.Parameters.AddWithValue("@supplycfm", li.supplycfm);
            cmd.Parameters.AddWithValue("@sensibleloadfromheatload", li.sensibleloadfromheatload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloadminload", li.diversityofsensibleloadminload);
            cmd.Parameters.AddWithValue("@diversityofsensibleloaddiversity", li.diversityofsensibleloaddiversity);
            cmd.Parameters.AddWithValue("@suppludb", li.suppludb);
            cmd.Parameters.AddWithValue("@latentload", li.latentload);
            cmd.Parameters.AddWithValue("@supplygrains", li.supplygrains);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiondb", li.hotwateroutletconditiondb);
            cmd.Parameters.AddWithValue("@hotwateroutletconditiongrains", li.hotwateroutletconditiongrains);
            cmd.Parameters.AddWithValue("@sensibleloadforhotwatercoil", li.sensibleloadforhotwatercoil);
            cmd.Parameters.AddWithValue("@hotwatercoilcapacityofsensibleload", li.hotwatercoilcapacityofsensibleload);
            cmd.Parameters.AddWithValue("@checkpointforrh", li.checkpointforrh);
            cmd.Parameters.AddWithValue("@checkpointfortemp", li.checkpointfortemp);
            cmd.Parameters.AddWithValue("@checkpointfortemp1", li.checkpointfortemp1);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectdatafromccode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();

            da = new SqlDataAdapter("select * from project1 where ccode=@ccode order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdata2fromccode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();

            da = new SqlDataAdapter("select * from project2 where ccode=@ccode", con);
            da.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void deleteproject1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from project1 where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteproject2data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from project2 where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}