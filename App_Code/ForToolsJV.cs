﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForToolsJV
/// </summary>
public class ForToolsJV : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForToolsJV()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectchallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsJVMaster where cno=@cno and inout=@inout order by convert(bigint,challanno) desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkchallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsJVMaster where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.challanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ToolsJVMaster (challanno,challandate,acname,inout,cno,uname,udate) values (@challanno,@challandate,@acname,@inout,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsJVMaster set challandate=@challandate,acname=@acname,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno and inout=@inout", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateitemsdatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsJVItems set challandate=@challandate,acname=@acname,uname=@uname,udate=@udate where challanno=@challanno and cno=@cno and inout=@inout", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ToolsJVItems (challanno,challandate,acname,itemname,qty,rate,descr,inout,cno,uname,udate) values (@challanno,@challandate,@acname,@itemname,@qty,@rate,@descr,@inout,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@challandate", li.cdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolsJVItems where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolsJVItems where challanno=@challanno and inout=@inout and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolsJVMaster where challanno=@challanno and inout=@inout and cno=@cno", con);
            cmd.Parameters.AddWithValue("@challanno", li.strchallanno);
            cmd.Parameters.AddWithValue("@inout", li.inout);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalldatafromchallannoitems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsJVItems where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.strchallanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromchallannomaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsJVMaster where cno=@cno and inout=@inout and challanno=@challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.SelectCommand.Parameters.AddWithValue("@challanno", li.strchallanno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsJVMaster inner join ToolsJVItems on ToolsJVMaster.challanno=ToolsJVItems.challanno where ToolsJVMaster.cno=@cno and ToolsJVMaster.inout=@inout and ToolsJVItems.inout=@inout order by convert(bigint,ToolsJVMaster.challanno) desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@inout", li.inout);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }
}