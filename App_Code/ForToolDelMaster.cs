﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForToolDelMaster
/// </summary>
public class ForToolDelMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForToolDelMaster()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectalltooldeldata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where cno=@cno and istype='TD' order by voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltooldelreturndata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where cno=@cno and istype='TR' order by voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selecttooldeldatafromvoucherno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where voucherno=@voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selecttooldelitemfromvoucherno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelItem where voucherno=@voucherno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void inserttooldeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ToolDelMaster (voucherno,voucherdate,name,clientcode,dispatchedby,cno,uname,udate,istype,issigned,signremarks) values (@voucherno,@voucherdate,@name,@clientcode,@dispatchedby,@cno,@uname,@udate,@istype,@issigned,@signremarks)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@clientcode", li.ccode);
            cmd.Parameters.AddWithValue("@dispatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@issigned", li.issigned);
            cmd.Parameters.AddWithValue("@signremarks", li.signremark);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetooldeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelMaster set voucherdate=@voucherdate,name=@name,clientcode=@clientcode,dispatchedby=@dispatchedby,uname=@uname,udate=@udate,issigned=@issigned,signremarks=@signremarks where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@clientcode", li.ccode);
            cmd.Parameters.AddWithValue("@dispatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@issigned", li.issigned);
            cmd.Parameters.AddWithValue("@signremarks", li.signremark);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetooldelitemdatedata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelItem set voucherdate=@voucherdate,uname=@uname,udate=@udate where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetooldelitemdatedatanew(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelItem set voucherdate=@voucherdate,toolname=@toolname,qty=@qty,unit=@unit,descr=@descr,remarks=@remarks,uname=@uname,udate=@udate,rfs=@rfs where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);

            cmd.Parameters.AddWithValue("@toolname", li.name);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@rfs", li.rfs);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            //cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void inserttooldelitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ToolDelItem (voucherno,voucherdate,toolname,qty,unit,descr,cno,uname,udate,istype,remarks,rfs) values (@voucherno,@voucherdate,@toolname,@qty,@unit,@descr,@cno,@uname,@udate,@istype,@remarks,@rfs)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@toolname", li.name);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@rfs", li.rfs);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetooldelitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelItem set voucherno=@voucherno,voucherdate=@voucherdate,toolname=@toolname,qty=@qty,unit=@unit,descr=@descr,cno=@cno,uname=@uname,udate=@udate,remarks=@remarks where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@toolname", li.name);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr", li.descr);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletetooldeldata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolDelMaster where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletetooldelitemdatafromvno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolDelItem where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletetooldelitemdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolDelItem where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selecttooldatafromtoolname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where name=@name", con);
            da.SelectCommand.Parameters.AddWithValue("@name", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedtooldelvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and istdused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectlasttooldelvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where cno=@cno order by voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set istdused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set istdused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedntr(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set istrused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}