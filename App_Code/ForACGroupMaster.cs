﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForACGroupMaster
/// </summary>
public class ForACGroupMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForACGroupMaster()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectaccountgroupdatafromtype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select id,groupcode,groupname from ACGroupMaster where type=@type order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@type", li.type);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectaccountgroupdatafromtype1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select id,groupcode,groupname from ACGroupMaster1 where type=@type order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@type", li.type);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertaccountdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ACGroupMaster (id,groupcode,groupname,mastercode,type,cno,uname,udate) values (@id,@groupcode,@groupname,@mastercode,@type,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@groupcode", li.groupcode);
            cmd.Parameters.AddWithValue("@groupname", li.groupname);
            cmd.Parameters.AddWithValue("@mastercode", li.mastercode);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertaccountdata1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ACGroupMaster1 (id,groupcode,groupname,type,cno,uname,udate) values (@id,@groupcode,@groupname,@type,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@groupcode", li.groupcode);
            cmd.Parameters.AddWithValue("@groupname", li.groupname);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}