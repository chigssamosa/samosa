﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForDashboard
/// </summary>
public class ForDashboard : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForDashboard()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectgeneratedvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where mon=@mon and yr=@yr and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@mon", li.mon);
            da.SelectCommand.Parameters.AddWithValue("@yr", li.yr);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertgeneratedvnodata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into vnogen (mon,yr,vno,issoused,ispoused,isscused,ispcused,issiused,ispiused,isbrused,isbr1used,isbr2used,isbr3used,isbr4used,cno) values (@mon,@yr,@vno,@issoused,@ispoused,@isscused,@ispcused,@issiused,@ispiused,@isbrused,@isbr1used,@isbr2used,@isbr3used,@isbr4used,@cno)", con);
            cmd.Parameters.AddWithValue("@mon", li.mon);
            cmd.Parameters.AddWithValue("@yr", li.yr);
            cmd.Parameters.AddWithValue("@vno", li.vno);
            cmd.Parameters.AddWithValue("@issoused", "N");
            cmd.Parameters.AddWithValue("@ispoused", "N");
            cmd.Parameters.AddWithValue("@isscused", "N");
            cmd.Parameters.AddWithValue("@ispcused", "N");
            cmd.Parameters.AddWithValue("@issiused", "N");
            cmd.Parameters.AddWithValue("@ispiused", "N");
            cmd.Parameters.AddWithValue("@isbrused", "N");
            cmd.Parameters.AddWithValue("@isbr1used", "N");
            cmd.Parameters.AddWithValue("@isbr2used", "N");
            cmd.Parameters.AddWithValue("@isbr3used", "N");
            cmd.Parameters.AddWithValue("@isbr4used", "N");
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletegeneratedvno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from vnogen", con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallactivity(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from activity where cno=@cno order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}