﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for LogicLayer
/// </summary>
public class LogicLayer
{
    public LogicLayer()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string username { get; set; }
    public string password { get; set; }
    public string ctype { get; set; }
    public string ctype1 { get; set; }
    public string strscno { get; set; }
    public string strscno11 { get; set; }
    public Int64 cno { get; set; }
    public int mon { get; set; }
    public int yr { get; set; }
    public Int64 sono1 { get; set; }
    public Int64 pcno { get; set; }
    public DateTime pcdate { get; set; }
    public Int64 scno { get; set; }
    public DateTime scdate { get; set; }
    public Int64 deldays { get; set; }
    public string strcno { get; set; }
    public string iscame { get; set; }
    public Int64 id { get; set; }
    public Int64 scid { get; set; }
    public Int64 pcid { get; set; }
    public Int64 vid { get; set; }
    public Int64 dcno { get; set; }
    public string dcno1 { get; set; }
    public Int64 tdcno { get; set; }
    public Int64 rmcno { get; set; }
    public string strtdcno { get; set; }
    public string strrmcno { get; set; }
    public string isactive { get; set; }
    public string istype { get; set; }
    public string ispacking { get; set; }
    public string iscd { get; set; }
    public string issipi { get; set; }
    public string istype1 { get; set; }
    public string csnm { get; set; }
    public string cname { get; set; }
    public string address { get; set; }
    public string location { get; set; }
    public string emailid { get; set; }
    public string contact1 { get; set; }
    public string contact2 { get; set; }
    public string faxno { get; set; }
    public string tanno { get; set; }
    public string vatno { get; set; }
    public string cstno { get; set; }
    public string eccno { get; set; }
    public string servicetaxno { get; set; }
    public string rocno { get; set; }
    public string imexcode { get; set; }
    public string yearoe { get; set; }
    public string bank1 { get; set; }
    public string bank2 { get; set; }
    public string emppt { get; set; }
    public string comppt { get; set; }
    public string pfno { get; set; }
    public string panno { get; set; }
    public string gstno { get; set; }
    public string gstnodate { get; set; }


    public string fromitem { get; set; }
    public string toitem { get; set; }
    public string fromdate { get; set; }
    public string todate { get; set; }
    public string fromdate1 { get; set; }
    public string todate1 { get; set; }
    public DateTime fromdated { get; set; }
    public DateTime todated { get; set; }
    public DateTime fromdated1 { get; set; }
    public DateTime todated1 { get; set; }
    public DateTime date30d { get; set; }
    public DateTime date60d { get; set; }
    public DateTime date90d { get; set; }
    public DateTime date120d { get; set; }
    public DateTime date30d1 { get; set; }
    public DateTime date60d1 { get; set; }
    public DateTime date90d1 { get; set; }
    public DateTime date120d1 { get; set; }
    public double amt30d { get; set; }
    public double amt60d { get; set; }
    public double amt90d { get; set; }
    public double amt120d { get; set; }    
    public double actualtotal { get; set; }
    public double total { get; set; }
    public double totalcheck { get; set; }
    public string name { get; set; }
    public string atype { get; set; }
    public string clientname { get; set; }
    public string name1 { get; set; }
    public string salesmanname { get; set; }
    public string dispatchedby { get; set; }
    public string descr { get; set; }
    public string descr1 { get; set; }
    public string descr2 { get; set; }
    public string mobileno { get; set; }
    public string uname { get; set; }
    public string stringscno { get; set; }
    public string stringpcno { get; set; }
    public string stringpono { get; set; }
    public DateTime udate { get; set; }
    public DateTime followupdate { get; set; }
    public string followupdetails { get; set; }
    public string followupdetails1 { get; set; }
    public string acyear { get; set; }

    public string unit { get; set; }
    public string unitewaybill { get; set; }
    public string unitewaybill1 { get; set; }
    public double rate { get; set; }
    public double rate1 { get; set; }
    public double qty1 { get; set; }
    public double rfs { get; set; }
    public double opstock { get; set; }
    public double inward { get; set; }
    public double outward { get; set; }
    public double clstock { get; set; }


    public string delivery { get; set; }
    public string deliverysch { get; set; }
    public string payment { get; set; }
    public string taxes { get; set; }
    public string octroi { get; set; }
    public string excise { get; set; }
    public string insurance { get; set; }

    public string transportation { get; set; }
    public string packing { get; set; }
    public string descr4 { get; set; }
    public string descr5 { get; set; }
    public string descr6 { get; set; }



    public double grandtotal { get; set; }
    public double excisep { get; set; }
    public double exciseamt { get; set; }
    public string itemname { get; set; }
    public string itemname1 { get; set; }
    public string description { get; set; }
    public string hsncode { get; set; }
    public string hsncode1 { get; set; }
    public string isblock { get; set; }
    public double salesrate { get; set; }
    public double purchaserate { get; set; }
    public double valuationrate { get; set; }
    public string vattype { get; set; }
    public string gsttype { get; set; }
    public double oprate { get; set; }
    public double qty { get; set; }
    public double opqty { get; set; }
    public double adjqty { get; set; }
    public double qtysc { get; set; }
    public double qtypc { get; set; }
    public double qtyremain { get; set; }
    public double qtyused { get; set; }
    public double qtyremain1 { get; set; }
    public double qtyused1 { get; set; }
    public double stockqty { get; set; }
    public double amount { get; set; }
    public double receivedamount { get; set; }
    public double remainamount { get; set; }

    public string code { get; set; }
    public string add1 { get; set; }
    public string add2 { get; set; }
    public string add3 { get; set; }
    public string city { get; set; }
    public string pincode { get; set; }

    public string activity { get; set; }
    public string acname { get; set; }
    public string acname1 { get; set; }
    public string contactperson { get; set; }
    public string phone1 { get; set; }
    public string phoner1 { get; set; }
    public string phone2 { get; set; }
    public string phoner2 { get; set; }
    public string fax { get; set; }
    public string actype { get; set; }
    public string gsttinno { get; set; }
    public string date1 { get; set; }
    public string csttinno { get; set; }
    public string date2 { get; set; }
    public double duedays { get; set; }
    public double paidamount { get; set; }
    public double discp { get; set; }
    public string tan { get; set; }
    public string pan { get; set; }
    public string debitgroupcode { get; set; }
    public string creditgroupcode { get; set; }
    public string debitgroupcodename { get; set; }
    public string creditgroupcodename { get; set; }
    public double opbalance { get; set; }
    public DateTime opdate { get; set; }

    public string typename { get; set; }
    public string type { get; set; }
    public string vtype { get; set; }
    public string type1 { get; set; }
    public string groupcode { get; set; }
    public string groupname { get; set; }
    public string mastercode { get; set; }
    public double vatp { get; set; }
    public double addtaxp { get; set; }
    public double centralsalep { get; set; }
    public double selltaxp { get; set; }
    public double surchargep { get; set; }
    public string form { get; set; }
    public Int64 ccode { get; set; }
    public Int64 srno { get; set; }
    public Int64 srno1 { get; set; }
    public double quono { get; set; }
    public DateTime quodate { get; set; }
    public string vatdesc { get; set; }
    public string gstdesc { get; set; }
    public string addtaxdesc { get; set; }
    public string cstdesc { get; set; }
    public string servicetaxdesc { get; set; }

    public Int64 voucherno { get; set; }
    public string voucherstring { get; set; }
    public Int64 invoiceno { get; set; }
    public string vno { get; set; }
    public Int64 vnono { get; set; }
    public Int64 vnono1 { get; set; }
    public Int64 projectcode { get; set; }
    public Int64 refid { get; set; }
    public Int64 refid1 { get; set; }
    public string strvoucherno { get; set; }
    public string creditcode { get; set; }
    public string crdr { get; set; }
    public string debitcode { get; set; }
    public Int64 sono { get; set; }
    //public Int64 sono1 { get; set; }
    public string strsono { get; set; }
    public Int64 challanno { get; set; }
    public string strchallanno { get; set; }
    public string strbillno { get; set; }
    public string inout { get; set; }
    public DateTime voucherdate { get; set; }
    public SqlDateTime voucherdate1 { get; set; }
    public DateTime cldate { get; set; }
    public SqlDateTime sodate { get; set; }
    public DateTime cdate { get; set; }
    public SqlDateTime lrdate { get; set; }
    public Int64 billno { get; set; }
    public string agbill { get; set; }
    public string sbillno { get; set; }
    public string inexdebit { get; set; }
    public string excludetax { get; set; }
    public string inexport { get; set; }
    public SqlDateTime sbilldate { get; set; }
    public SqlDateTime pdate { get; set; }
    public string portcode { get; set; }
    public string lrno { get; set; }
    public string chequeno { get; set; }
    public string remarks { get; set; }
    public string retremarks { get; set; }
    public string referencename { get; set; }
    public string handledby { get; set; }
    public double freight { get; set; }
    public string transportname { get; set; }
    public string vehicleno { get; set; }
    public string drivername { get; set; }
    public string licenseno { get; set; }
    public string materiall { get; set; }
    public string materialr { get; set; }


    public Int64 pono { get; set; }
    public string strpono { get; set; }
    public string strpono1 { get; set; }
    public SqlDateTime podate { get; set; }
    public string strpodate { get; set; }
    public Int64 pono1 { get; set; }
    public SqlDateTime podate1 { get; set; }
    public Int64 indentno { get; set; }
    public SqlDateTime indentdate { get; set; }
    public string descr3 { get; set; }
    public string make { get; set; }
    public string deliveryat { get; set; }
    public double basicamount { get; set; }
    public double totbasicamount { get; set; }
    public string taxtype { get; set; }
    public string taxdesc { get; set; }
    public double vatamt { get; set; }
    public double addtaxamt { get; set; }
    public double cstp { get; set; }
    public double cstamt { get; set; }
    public double totvatamt { get; set; }
    public double closingbal { get; set; }
    public double totaddtaxamt { get; set; }
    public double totcstamt { get; set; }
    public double totsoamt { get; set; }

    public string delat { get; set; }
    public string delat1 { get; set; }
    public string delat2 { get; set; }
    public string inst1 { get; set; }
    public string inst2 { get; set; }
    public string inst3 { get; set; }
    public string inst4 { get; set; }
    public string inst5 { get; set; }
    public string inst6 { get; set; }
    public string inst7 { get; set; }
    public string status { get; set; }
    public string state { get; set; }
    public string annexure { get; set; }
    public string isigst { get; set; }

    public double dtors { get; set; }
    public string invtype { get; set; }
    public string invtype1 { get; set; }
    public string salesac { get; set; }
    public string purchaseac { get; set; }
    public Int64 sino { get; set; }
    public Int64 sino1 { get; set; }
    public string strsino { get; set; }
    public string strsino1 { get; set; }
    public string strpino { get; set; }
    public string strpino1 { get; set; }
    public DateTime sidate { get; set; }
    public DateTime billdate { get; set; }
    public Int64 pino { get; set; }
    public Int64 pino1 { get; set; }
    public DateTime pidate { get; set; }
    public Int64 scno1 { get; set; }
    public string strscno1 { get; set; }
    public SqlDateTime scdate1 { get; set; }
    public Int64 pcno1 { get; set; }
    public SqlDateTime pcdate1 { get; set; }
    public double servicetaxp { get; set; }
    public double servicetaxamount { get; set; }
    public double roundoff { get; set; }
    public double cartage { get; set; }
    public double billamount { get; set; }
    public string reason { get; set; }
    public double cgst { get; set; }
    public double sgst { get; set; }
    public double igst { get; set; }
    //
    //For User Rights
    //
    public string role { get; set; }
    public string Role { get; set; }
    public string UADD { get; set; }
    public string UEDIT { get; set; }
    public string UDELETE { get; set; }
    public string UPRINT { get; set; }
    public string UVIEW { get; set; }
    public string pagename { get; set; }
    public string pagename1 { get; set; }
    public string monyr { get; set; }
    public int cmon { get; set; }
    public string cmonname { get; set; }


    // For Project Report
    //
    public SqlDateTime firstquotation { get; set; }
    public SqlDateTime technicaldrawing { get; set; }
    public SqlDateTime rewisequotation { get; set; }
    public SqlDateTime finaltech { get; set; }
    public SqlDateTime lastfollowupdate { get; set; }
    public string pendingorderlying { get; set; }
    public SqlDateTime okorderdate { get; set; }
    public SqlDateTime tentativedeliverydate { get; set; }
    public SqlDateTime deliverydate { get; set; }

    public string ahuno { get; set; }
    public double returnaircfm { get; set; }
    public double returnairtemp { get; set; }
    public double returnairgrains { get; set; }
    public double freshaircfm { get; set; }
    public double freshairtemp { get; set; }
    public double freshairgrains { get; set; }
    public double mixinaircfm { get; set; }
    public double mixingairtemp { get; set; }
    public double mixingairgrains { get; set; }
    public double totalfancfm { get; set; }
    public double beforefanairtemp { get; set; }
    public double beforefanairgrains { get; set; }
    public double fanstaticpressure { get; set; }
    public double totalfankw { get; set; }
    public double totalfanbtuhr { get; set; }
    public double tempdiffdegreef { get; set; }
    public double afterfabairtemp { get; set; }
    public double afterfanairgrains { get; set; }
    public double selecteddehcfm { get; set; }
    public double actualdehaircfm { get; set; }
    public double dehairtempafterchw { get; set; }
    public double dehairgrainsafterchw { get; set; }
    public double bypassaircfm { get; set; }
    public double bypassairtemp { get; set; }
    public double bypassairgrains { get; set; }
    public double exhaustcfm { get; set; }
    public double supplycfm { get; set; }
    public double sensibleloadfromheatload { get; set; }
    public double diversityofsensibleloadminload { get; set; }
    public double diversityofsensibleloaddiversity { get; set; }
    public double suppludb { get; set; }
    public double latentload { get; set; }
    public double supplygrains { get; set; }
    public double hotwateroutletconditiondb { get; set; }
    public double hotwateroutletconditiongrains { get; set; }
    public double sensibleloadforhotwatercoil { get; set; }
    public double hotwatercoilcapacityofsensibleload { get; set; }
    public string checkpointforrh { get; set; }
    public string checkpointfortemp { get; set; }
    public string checkpointfortemp1 { get; set; }

    public string issigned { get; set; }
    public string signremark { get; set; }

    public string otype { get; set; }

    //For E-Way Bill
    public string ewaybillno { get; set; }
    public DateTime ewaybilldate { get; set; }
    public string cewaybillno { get; set; }
    public DateTime cewaybilldate { get; set; }
    public string supplytype { get; set; }
    public string subtype { get; set; }
    public string doctype { get; set; }

    public string tramode { get; set; }
    public string distanceinkm { get; set; }
    public string transportername { get; set; }
    public string vehiclenumber { get; set; }
    public string vehicletype { get; set; }
    public string mainhsncode { get; set; }
    public string trano { get; set; }
    public DateTime tradate { get; set; }
    public string traid { get; set; }

    public string consignor { get; set; }
    public string consignoradd1 { get; set; }
    public string consignoradd2 { get; set; }
    public string consignorplace { get; set; }
    public string consignorpincode { get; set; }
    public string consignorstate { get; set; }
    public string actualfromstate { get; set; }
    public string consignorgst { get; set; }

    public string consignee { get; set; }
    public string consigneeadd1 { get; set; }
    public string consigneeadd2 { get; set; }
    public string consigneeplace { get; set; }
    public string consigneepincode { get; set; }
    public string consigneestate { get; set; }
    public string actualtostate { get; set; }
    public string consigneegst { get; set; }

}