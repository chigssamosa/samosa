﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForStockSettlement
/// </summary>
public class ForStockSettlement:ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForStockSettlement()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public void deleteaccountdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ACMaster where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(SCItems.adjqty,0) as adjqty,SCMaster.strscno,SCMaster.acname,SCMaster.scdate,SCItems.stockqty,SCItems.id,((SCItems.stockqty)-(isnull(SCItems.adjqty,0))) as balqty,atype='SCHN' from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where (SCItems.scdate between @fromdate and @todate) and SCItems.itemname=@itemname and SCItems.cno=@cno and ((SCItems.stockqty)-(isnull(SCItems.adjqty,0)))>0 order by SCItems.scno", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname",li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallstockjvotdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(StockJVItems.adjqty,0) as adjqty,StockJVItems.challanno as strscno,StockJVItems.acname,StockJVItems.challandate as scdate,StockJVItems.qty as stockqty,StockJVItems.id,((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0))) as balqty,atype='SJOT' from StockJVItems where StockJVItems.itemname=@itemname and StockJVItems.cno=@cno and StockJVItems.inout='OUT' and ((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))>0 order by StockJVItems.challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallstockjvindatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(StockJVItems.adjqty,0) as adjqty,convert(bigint,StockJVItems.challanno) as pcno,StockJVItems.acname,StockJVItems.challandate as pcdate,StockJVItems.qty as stockqty,StockJVItems.id,((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0))) as balqty,atype='SJIN' from StockJVItems where StockJVItems.itemname=@itemname and StockJVItems.cno=@cno and StockJVItems.inout='IN' and ((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))>0 order by StockJVItems.challanno", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromstrscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((stockqty)-(isnull(adjqty,0))) as balqty from SCItems where id=@id", con);//strscno=@strscno and cno=@cno and itemname=@itemname
            da.SelectCommand.Parameters.AddWithValue("@id", li.scid);
            //da.SelectCommand.Parameters.AddWithValue("@cno",li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromsjvitemsid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((qty)-(isnull(adjqty,0))) as balqty from StockJVItems where id=@id", con);//strscno=@strscno and cno=@cno and itemname=@itemname
            da.SelectCommand.Parameters.AddWithValue("@id", li.scid);
            //da.SelectCommand.Parameters.AddWithValue("@cno",li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldataopdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((qty)-(isnull(adjqty,0))) as balqty from ItemMaster1 where cno=@cno and itemname=@itemname and ((qty)-(isnull(adjqty,0)))>0", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(PCItems.adjqty,0) as adjqty,PCItems.stockqty,PCItems.pcdate,PCItems.pcno,PCItems.id,PCMaster.acname,((PCItems.stockqty)-(isnull(PCItems.adjqty,0))) as balqty,atype='PCHN' from PCItems inner join PCMaster on PCMaster.pcno=PCItems.pcno where (PCItems.pcdate between @fromdate and @todate) and PCItems.itemname=@itemname and PCItems.cno=@cno and ((PCItems.stockqty)-(isnull(PCItems.adjqty,0)))>0 order by PCItems.pcno", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromstrpcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((isnull(stockqty,0))-(isnull(adjqty,0))) as balqty from PCItems where id=@id and ((isnull(stockqty,0))-(isnull(adjqty,0)))>0", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.pcid);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromstrpcnofromitemmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((qty)-(isnull(adjqty,0))) as balqty from ItemMaster1 where id=@id and ((qty)-(isnull(adjqty,0)))>0", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.pcid);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafromchallannojvin(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select isnull(adjqty,0) as adjqty,*,((qty)-(isnull(adjqty,0))) as balqty from StockJVItems where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.pcid);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateadjqtyinpcitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set adjqty=@adjqty where id=@id", con);//itemname=@itemname and cno=@cno and pcno=@pcno
            cmd.Parameters.AddWithValue("@adjqty", li.qtypc);
            cmd.Parameters.AddWithValue("@id", li.pcid);
            //cmd.Parameters.AddWithValue("@cno",li.cno);
            //cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinsjvitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set adjqty=@adjqty where id=@id", con);//itemname=@itemname and cno=@cno and pcno=@pcno
            cmd.Parameters.AddWithValue("@adjqty", li.qtypc);
            cmd.Parameters.AddWithValue("@id", li.pcid);
            //cmd.Parameters.AddWithValue("@cno",li.cno);
            //cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinsjvitems1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set adjqty=@adjqty where id=@id", con);//itemname=@itemname and cno=@cno and pcno=@pcno
            cmd.Parameters.AddWithValue("@adjqty", li.qtysc);
            cmd.Parameters.AddWithValue("@id", li.scid);
            //cmd.Parameters.AddWithValue("@cno",li.cno);
            //cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinitemmaster1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ItemMaster1 set adjqty=@adjqty where id=@id", con);//itemname=@itemname and cno=@cno and pcno=@pcno
            cmd.Parameters.AddWithValue("@adjqty", li.qtypc);
            cmd.Parameters.AddWithValue("@id", li.pcid);
            //cmd.Parameters.AddWithValue("@cno",li.cno);
            //cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinscitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set adjqty=@adjqty where id=@id", con);//itemname=@itemname and cno=@cno and strscno=@strscno
            cmd.Parameters.AddWithValue("@adjqty", li.qtysc);
            cmd.Parameters.AddWithValue("@id", li.scid);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            //cmd.Parameters.AddWithValue("@strscno", li.strscno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinpcitemsforrefresh(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set adjqty=0 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinscitemsforrefresh(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set adjqty=0 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinsjvitemsforrefresh(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set adjqty=0 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateadjqtyinitemmasteropforrefresh(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ItemMaster1 set adjqty=0 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}