﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PackingMaterialEntryList.aspx.cs" Inherits="PackingMaterialList" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Packing Material List</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnpackingmaterial" runat="server" Text="Add New Packing" class="btn btn-default forbutton"
                    OnClick="btnpackingmaterial_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpackingmateriallist" runat="server" AutoGenerateColumns="False"
                        Width="100%" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered" OnRowCommand="gvpackingmateriallist_RowCommand"
                        OnRowDeleting="gvpackingmateriallist_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("voucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherno" ForeColor="Black" runat="server" Text='<%# bind("voucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="clientcode" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclientcode" ForeColor="Black" runat="server" Text='<%# bind("clientcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="rmcno" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrmcno" ForeColor="Black" runat="server" Text='<%# bind("rmcno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tdcno" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltdcno" ForeColor="Black" runat="server" Text='<%# bind("tdcno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("voucherno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
