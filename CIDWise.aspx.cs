﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class CIDWise : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();
    public ReportDocument rptdoc;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
            //SessionMgt.FirstName = cc.Split('-')[1];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
            //SessionMgt.FirstName = string.Empty;
        }
    }

    public DataTable CreateTemplate444()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("strvoucherno", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(DateTime));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("name", typeof(string));
        dtpitems.Columns.Add("pono", typeof(string));
        dtpitems.Columns.Add("podate", typeof(DateTime));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("totvat", typeof(double));
        dtpitems.Columns.Add("totaddtax", typeof(double));
        dtpitems.Columns.Add("totcst", typeof(double));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("receivedamount", typeof(double));
        return dtpitems;
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("srno", typeof(Int64));
        dtpitems.Columns.Add("vdate", typeof(DateTime));
        dtpitems.Columns.Add("vno", typeof(string));
        dtpitems.Columns.Add("totamt", typeof(double));
        dtpitems.Columns.Add("receivedamt", typeof(double));
        dtpitems.Columns.Add("pendingamt", typeof(double));
        dtpitems.Columns.Add("taxamt", typeof(double));
        dtpitems.Columns.Add("basicamt", typeof(double));
        dtpitems.Columns.Add("remarks", typeof(string));
        return dtpitems;
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            Session["dtpitems"] = CreateTemplate444();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter daac = new SqlDataAdapter("select * from ClientMaster where code=" + txtclientcode.Text + "", con);
            DataTable dtactt = new DataTable();
            daac.Fill(dtactt);
            li.acname = dtactt.Rows[0]["acname"].ToString();
            if (li.acname == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Client Master and update Head AC Name.Try Again.');", true);
                return;
            }
            li.city = dtactt.Rows[0]["city"].ToString();
            li.ccode = Convert.ToInt64(dtactt.Rows[0]["code"].ToString());
            li.clientname = dtactt.Rows[0]["name"].ToString();
            //SqlDataAdapter da = new SqlDataAdapter("select BankACMaster.acname,ACMaster.city,ACMaster.opbalance from BankACMaster inner join ACMaster on ACMaster.acname=BankACMaster.acname where BankACMaster.voucherdate between @fromdate and @todate and BankACMaster.issipi='SI' and ACMaster.acname=@acname", con);
            //da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            //da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            //DataTable dtac = new DataTable();
            //da.Fill(dtac);
            //if (dtac.Rows.Count > 0)
            //{
            //for (int c = 0; c < dtac.Rows.Count; c++)
            //{
            DateTime dd = System.DateTime.Now;
            DateTime todd = System.DateTime.Now.AddDays(-519);

            //if (dtsi.Rows[0]["billamt"].ToString() != "0")
            //{
            DataTable dtcall = new DataTable();
            DataTable dtq = new DataTable();
            DataTable dtq1 = new DataTable();
            dtq1 = (DataTable)Session["dtpitems"];
            SqlDataAdapter dasi11 = new SqlDataAdapter("select distinct(strsino),sino from SIItems where SIItems.ccode=" + li.ccode + " order by SIItems.sino", con);
            //dasi11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dasi11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtsi11 = new DataTable();
            con.Open();
            dasi11.Fill(dtsi11);
            con.Close();
            string sino = "";
            for (int t = 0; t < dtsi11.Rows.Count; t++)
            {
                sino = sino + "'" + dtsi11.Rows[t]["strsino"].ToString() + "',";
            }
            DataTable dtsi1 = new DataTable();
            sino = sino.TrimEnd(',');
            if (sino != "")
            {
                SqlDataAdapter dasi1 = new SqlDataAdapter("select * from SIMaster where SIMaster.strsino in (" + sino + ") order by SIMaster.sino", con);
                //dasi1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dasi1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);                
                con.Open();
                dasi1.Fill(dtsi1);
                con.Close();
            }
            if (dtactt.Rows.Count > 0)
            {
                DataRow dr1 = dtq1.NewRow();
                dr1["strvoucherno"] = "0";
                dr1["sidate"] = Convert.ToDateTime(dtactt.Rows[0]["udate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["ccode"] = li.ccode;
                dr1["name"] = li.clientname;
                dr1["pono"] = "0";
                dr1["podate"] = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["totbasicamount"] = "0";
                dr1["totvat"] = "0";
                dr1["totaddtax"] = "0";
                dr1["totcst"] = "0";
                dr1["billamount"] = Convert.ToDouble(dtactt.Rows[0]["opbalance"].ToString());
                dr1["receivedamount"] = "0";
                dtq1.Rows.Add(dr1);
            }
            for (int z = 0; z < dtsi1.Rows.Count; z++)
            {
                //dtq1 = (DataTable)Session["dtpitems1"];
                DataRow dr1 = dtq1.NewRow();
                dr1["strvoucherno"] = dtsi1.Rows[z]["strsino"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtsi1.Rows[z]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["ccode"] = li.ccode;
                dr1["name"] = li.clientname;
                dr1["pono"] = "0";
                dr1["podate"] = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["totbasicamount"] = Convert.ToDouble(dtsi1.Rows[z]["totbasicamount"].ToString());
                dr1["totvat"] = Convert.ToDouble(dtsi1.Rows[z]["totvat"].ToString());
                dr1["totaddtax"] = Convert.ToDouble(dtsi1.Rows[z]["totaddtax"].ToString());
                dr1["totcst"] = Convert.ToDouble(dtsi1.Rows[z]["totcst"].ToString());
                dr1["billamount"] = Convert.ToDouble(dtsi1.Rows[z]["billamount"].ToString());
                dr1["receivedamount"] = "0";
                dtq1.Rows.Add(dr1);
            }
            //dtcall.Merge(dtsi1);

            DataTable dtsi1a = new DataTable();
            if (sino != "")
            {
                SqlDataAdapter dasi1a = new SqlDataAdapter("select isnull(sum(billamount),0) as totbillamount from SIMaster where SIMaster.strsino in (" + sino + ")", con);
                //dasi1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dasi1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);            
                con.Open();
                dasi1a.Fill(dtsi1a);
                con.Close();
            }

            SqlDataAdapter dasi1b = new SqlDataAdapter("select isnull(sum(amount),0) as totamount from Ledger where projectcode=" + li.ccode + " and debitcode='" + li.acname + "' and istype not in ('SI','PI','OP')", con);
            //dasi1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dasi1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtsi1b = new DataTable();
            con.Open();
            dasi1b.Fill(dtsi1b);
            con.Close();

            SqlDataAdapter dasi1ba = new SqlDataAdapter("select * from Ledger where projectcode=" + li.ccode + " and debitcode='" + li.acname + "' and istype not in ('SI','PI','OP')", con);
            //dasi1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dasi1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtsi1ba = new DataTable();
            con.Open();
            dasi1ba.Fill(dtsi1ba);
            con.Close();
            for (int z = 0; z < dtsi1ba.Rows.Count; z++)
            {
                //dtq1 = (DataTable)Session["dtpitems1"];
                DataRow dr1 = dtq1.NewRow();
                dr1["strvoucherno"] = dtsi1ba.Rows[z]["strvoucherno"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtsi1ba.Rows[z]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["ccode"] = dtsi1ba.Rows[z]["projectcode"].ToString();
                dr1["name"] = li.clientname;
                dr1["pono"] = "0";
                dr1["podate"] = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["totbasicamount"] = "0";
                dr1["totvat"] = "0";
                dr1["totaddtax"] = "0";
                dr1["totcst"] = "0";
                dr1["billamount"] = "0";
                dr1["receivedamount"] = Convert.ToDouble(dtsi1ba.Rows[z]["amount"].ToString());
                dtq1.Rows.Add(dr1);
            }
            Session["dtpitems1"] = dtq1;
            if (dtq1.Rows.Count > 0)
            {
                gvaclist.Visible = true;
                GridView1.Visible = false;
                Panel1.Visible = true;
                gvaclist.DataSource = dtq1;
                gvaclist.DataBind();
            }
            else
            {
                gvaclist.Visible = false;
            }

            if (dtsi1a.Rows.Count > 0)
            {
                lbltotamt.Text = Math.Round(Convert.ToDouble(dtsi1a.Rows[0]["totbillamount"].ToString()), 2).ToString();
            }
            if (dtsi1b.Rows.Count > 0)
            {
                lnkpaidamt.Text = Math.Round(Convert.ToDouble(dtsi1b.Rows[0]["totamount"].ToString()), 2).ToString();
            }
            double tot = 0;
            double paid = 0;
            double remain = 0;
            if (dtsi1a.Rows.Count > 0)
            {
                tot = Math.Round(Convert.ToDouble(dtsi1a.Rows[0]["totbillamount"].ToString()), 2);
            }
            if (dtsi1b.Rows.Count > 0)
            {
                paid = Math.Round(Convert.ToDouble(dtsi1b.Rows[0]["totamount"].ToString()), 2);
            }
            remain = tot - paid;
            lblremainamt.Text = remain.ToString();
            SqlDataAdapter daso = new SqlDataAdapter("select * from SalesOrder where ccode=" + li.ccode + "", con);
            DataTable dtso = new DataTable();
            con.Open();
            daso.Fill(dtso);
            con.Close();
            if (dtso.Rows.Count > 0)
            {
                lblpono.Text = dtso.Rows[0]["pono"].ToString();
                lblpodate.Text = dtso.Rows[0]["podate"].ToString();
            }
            else
            {
                lblpono.Text = "";
                lblpodate.Text = "";
            }
        }
        else
        {
            lblpono.Text = "";
            lblpodate.Text = "";
            gvaclist.Visible = false;
            GridView1.Visible = false;
            lbltotamt.Text = "0";
            lnkpaidamt.Text = "0";
            lblremainamt.Text = "0";
        }
    }
    protected void gvaclist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblPrice = (Label)e.Row.FindControl("lblsino");
            string CountryId = lblPrice.Text;
            con.Open();
            GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
            SqlCommand cmd = new SqlCommand("select invoiceno as strvoucherno,voucherdate,amount,istype,voucherno from BankACShadow where invoiceno='" + CountryId + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gv.DataSource = ds;
                gv.DataBind();
            }
        }
    }
    protected void lnkpaidamt_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter daac = new SqlDataAdapter("select * from ClientMaster where code=" + txtclientcode.Text + "", con);
            DataTable dtactt = new DataTable();
            daac.Fill(dtactt);
            li.acname = dtactt.Rows[0]["acname"].ToString();
            li.ccode = Convert.ToInt64(dtactt.Rows[0]["code"].ToString());
            if (lnkpaidamt.Text != "0")
            {
                SqlDataAdapter dasi1b = new SqlDataAdapter("select * from Ledger where projectcode=" + li.ccode + " and debitcode='" + li.acname + "' and istype not in ('SI','PI','OP')", con);
                //dasi1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dasi1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtsi1b = new DataTable();
                con.Open();
                dasi1b.Fill(dtsi1b);
                con.Close();
                if (dtsi1b.Rows.Count > 0)
                {
                    GridView1.Visible = true;
                    gvaclist.Visible = false;
                    Panel1.Visible = false;
                    GridView1.DataSource = dtsi1b;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.Visible = false;
                }
            }
        }
    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        string remain = "N";
        if (chkoutstanding.Checked == true)
        {
            remain = "Y";
        }
        if (chkacnamewise.Checked == false)
        {
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                if (chkagbill.Checked == true)
                {
                    string Xrepname = "CIDWise Payment Report1";
                    li.ccode = Convert.ToInt64(txtclientcode.Text);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + li.ccode + "&remain="+remain+"&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                }
                else if (chkledgercid.Checked == true)
                {
                    string Xrepname = "CIDWise Ledger Payment Report";
                    li.ccode = Convert.ToInt64(txtclientcode.Text);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + li.ccode + "&remain=" + remain + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                }
                else
                {
                    string Xrepname = "CIDWise Payment Report";
                    li.ccode = Convert.ToInt64(txtclientcode.Text);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + li.ccode + "&remain=" + remain + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                }
            }
        }
        else
        {
            string Xrepname = "ACname CIDWise Payment Report1";
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + li.ccode + "&remain=" + remain + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
}