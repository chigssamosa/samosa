﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;

public partial class ItemMasterList : System.Web.UI.Page
{
    ForItemMaster fimclass = new ForItemMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        if (Request.Cookies["ForLogin"]["username"] == "admin@airmax.so")
        {
            dtdata = fimclass.selectallitemdata();
        }
        else
        {
            dtdata = fimclass.selectallitemdatacnowise(li);
        }
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvitem.Visible = true;
            gvitem.DataSource = dtdata;
            gvitem.DataBind();
        }
        else
        {
            gvitem.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Record Found for Salesman Master.";
        }
    }

    protected void btnitem_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/ItemMaster.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }

    protected void gvitem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                string acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                li.id = Convert.ToInt64(e.CommandArgument);
                DataTable dtcheck = fimclass.checkacyearfordata(li);
                if (dtcheck.Rows.Count > 0)
                {
                    if (dtcheck.Rows[0]["acyear"].ToString() == acyear)
                    {
                        Response.Redirect("~/ItemMaster.aspx?mode=update&id=" + li.id + "");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Name is fetched from Old Year Item Master so you need to go to Last Year Database and update this data from there and then fetch Item data in new year again using Year End Process.');", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.id = Convert.ToInt64(e.CommandArgument);
                DataTable dtitem = new DataTable();
                dtitem = fimclass.selectitemdatafromid(li);
                if (dtitem.Rows.Count > 0)
                {
                    li.itemname = dtitem.Rows[0]["itemname"].ToString();
                    DataTable dtPCItems = new DataTable();
                    dtPCItems = fimclass.checkPCItems(li);
                    if (dtPCItems.Rows.Count == 0)
                    {
                        DataTable dtPerInvItems = new DataTable();
                        dtPerInvItems = fimclass.checkPerInvItems(li);
                        if (dtPerInvItems.Rows.Count == 0)
                        {
                            DataTable dtPIItems = new DataTable();
                            dtPIItems = fimclass.checkPIItems(li);
                            if (dtPIItems.Rows.Count == 0)
                            {
                                DataTable dtPurchaseOrderItems = new DataTable();
                                dtPurchaseOrderItems = fimclass.checkPurchaseOrderItems(li);
                                if (dtPurchaseOrderItems.Rows.Count == 0)
                                {
                                    DataTable dtQuoItems = new DataTable();
                                    dtQuoItems = fimclass.checkQuoItems(li);
                                    if (dtQuoItems.Rows.Count == 0)
                                    {
                                        DataTable dtrritems = new DataTable();
                                        dtrritems = fimclass.checkrritems(li);
                                        if (dtrritems.Rows.Count == 0)
                                        {
                                            DataTable dtSalesOrderItems = new DataTable();
                                            dtSalesOrderItems = fimclass.checkSalesOrderItems(li);
                                            if (dtSalesOrderItems.Rows.Count == 0)
                                            {
                                                DataTable dtSCItems = new DataTable();
                                                dtSCItems = fimclass.checkSCItems(li);
                                                if (dtSCItems.Rows.Count == 0)
                                                {
                                                    DataTable dtSIItems = new DataTable();
                                                    dtSIItems = fimclass.checkSIItems(li);
                                                    if (dtSIItems.Rows.Count == 0)
                                                    {
                                                        DataTable dtStockJVItems = new DataTable();
                                                        dtStockJVItems = fimclass.checkStockJVItems(li);
                                                        if (dtStockJVItems.Rows.Count == 0)
                                                        {
                                                            fimclass.deleteitemdata(li);
                                                            fimclass.deleteopitemdata1(li);
                                                            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                                                            li.uname = Request.Cookies["ForLogin"]["username"];
                                                            li.udate = System.DateTime.Now;
                                                            li.activity = li.itemname + " Has been Deleted.";
                                                            faclass.insertactivity(li);
                                                            fillgrid();
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Stock JV so you cant delete this Item Name.');", true);
                                                            return;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Sales Invoice so you cant delete this Item Name.');", true);
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Sales Challan so you cant delete this Item Name.');", true);
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Sales Order so you cant delete this Item Name.');", true);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Returnable Return so you cant delete this Item Name.');", true);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Bifurcation Entry so you cant delete this Item Name.');", true);
                                        return;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Purchase Order so you cant delete this Item Name.');", true);
                                    return;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Purchase Invoice so you cant delete this Item Name.');", true);
                                return;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Performa Invoice so you cant delete this Item Name.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name used in Purchase Challan so you cant delete this Item Name.');", true);
                        return;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvitem_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetItemname(string prefixText)
    {
        ForItemMaster fimclass = new ForItemMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.itemname = prefixText;
        DataTable dt = new DataTable();
        dt = fimclass.selectitemdatafromitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (txtsearchitemname.Text != "" && txtsearchitemname.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = txtsearchitemname.Text;
            DataTable dtitemname = new DataTable();
            dtitemname = fimclass.selectitemdatafromitemname(li);
            if (dtitemname.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvitem.Visible = true;
                gvitem.DataSource = dtitemname;
                gvitem.DataBind();
            }
            else
            {
                DataTable dtdt = new DataTable();
                dtdt = fimclass.checkduplicate(li);
                if (dtdt.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvitem.Visible = true;
                    gvitem.DataSource = dtdt;
                    gvitem.DataBind();
                }
                else
                {
                    gvitem.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Record Found for Salesman Master.";
                }
                //gvitem.Visible = false;
                //lblempty.Visible = true;
                //lblempty.Text = "No Record Found for Salesman Master.";
            }

        }
        else
        {
            fillgrid();
        }
    }
    protected void gvitem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvitem.PageIndex = e.NewPageIndex;
        fillgrid();
    }
    protected void btnprintitem_Click(object sender, EventArgs e)
    {
        string Xrepname = "Item List Report";
        //Int64 pono = Convert.ToInt64(e.CommandArgument);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
    protected void chkall_CheckedChanged(object sender, EventArgs e)
    {
        for (int c = 0; c < gvitem.Rows.Count; c++)
        {
            CheckBox chkall = (CheckBox)gvitem.HeaderRow.FindControl("chkall");
            CheckBox chkselect = (CheckBox)gvitem.Rows[c].FindControl("chkselect");
            if (chkall.Checked == true)
            {
                chkselect.Checked = true;
            }
            else
            {
                chkselect.Checked = false;
            }
        }
    }
    protected void btnquotation_Click(object sender, EventArgs e)
    {
        string rematk = "";
        SessionMgt.InsId = 0;
        for (int c = 0; c < gvitem.Rows.Count; c++)
        {
            CheckBox chkselect = (CheckBox)gvitem.Rows[c].FindControl("chkselect");
            Label lblid = (Label)gvitem.Rows[c].FindControl("lblid");
            if (chkselect.Checked == true)
            {
                rematk = rematk + lblid.Text + ",";
                SessionMgt.InsId = 1;
            }
        }
        SessionMgt.emailid = rematk;
        if (SessionMgt.InsId == 1)
        {
            Response.Redirect("~/Quotation.aspx?mode=direct");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('At least select one item.Try Again.');", true);
            return;
        }
    }
    protected void btnperformainvoice_Click(object sender, EventArgs e)
    {
        string rematk = "";
        SessionMgt.InsId = 0;
        for (int c = 0; c < gvitem.Rows.Count; c++)
        {
            CheckBox chkselect = (CheckBox)gvitem.Rows[c].FindControl("chkselect");
            Label lblid = (Label)gvitem.Rows[c].FindControl("lblid");
            if (chkselect.Checked == true)
            {
                rematk = rematk + lblid.Text + ",";
                SessionMgt.InsId = 1;
            }
        }
        SessionMgt.emailid = rematk;
        if (SessionMgt.InsId == 1)
        {
            Response.Redirect("~/PerformaInvoice.aspx?mode=direct");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('At least select one item.Try Again.');", true);
            return;
        }
    }
    protected void txthsncode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txthsncode = (TextBox)currentRow1.FindControl("txthsncode");
        Label lblid = (Label)currentRow1.FindControl("lblid");
        if (txthsncode.Text != string.Empty)
        {
            li.id = Convert.ToInt64(lblid.Text);
            li.hsncode = txthsncode.Text;
            fimclass.updatehsncodeinitemdata(li);
            fillgrid();
        }
    }
}