﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PurchaseInvoiceList : System.Web.UI.Page
{
    ForPurchaseInvoice fpiclass = new ForPurchaseInvoice();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpiclass.selectallpimasterdata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpurchaseinvoicelist.Visible = true;
            gvpurchaseinvoicelist.DataSource = dtdata;
            gvpurchaseinvoicelist.DataBind();
        }
        else
        {
            gvpurchaseinvoicelist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Purchase Invoice Data Found.";
        }
    }

    protected void btnpurchaseinvoice_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/PurchaseInvoice.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvpurchaseinvoicelist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.strpino = e.CommandArgument.ToString();
                Response.Redirect("~/PurchaseInvoice.aspx?mode=update&strpino=" + li.strpino + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strpino = e.CommandArgument.ToString();

                DataTable dtdata1 = new DataTable();
                dtdata1 = fpiclass.selectallpimasterdatafrompinostring(li);
                li.scno = 0;
                li.scno1 = 0;
                if (dtdata1.Rows[0]["pcno"].ToString() != "0" && dtdata1.Rows[0]["pcno"].ToString() != "")
                {
                    li.pcno = Convert.ToInt64(dtdata1.Rows[0]["pcno"].ToString());
                }
                if (dtdata1.Rows[0]["pcno1"].ToString() != "0" && dtdata1.Rows[0]["pcno1"].ToString() != "")
                {
                    li.pcno1 = Convert.ToInt64(dtdata1.Rows[0]["pcno1"].ToString());
                }
                //li.scno = Convert.ToInt64(dtdata1.Rows[0]["scno"].ToString());
                //li.vnono=
                DataTable dtdata = new DataTable();
                dtdata = fpiclass.selectallpiitemsfrompinostring(li);
                for (int c = 0; c < dtdata.Rows.Count; c++)
                {
                    li.vid = Convert.ToInt64(dtdata.Rows[c]["vid"].ToString());
                    li.id = Convert.ToInt64(dtdata.Rows[c]["id"].ToString());
                    li.itemname = dtdata.Rows[c]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtdata.Rows[c]["qty"].ToString());
                    if (li.pcno != 0 && li.pcno != null)
                    {
                        if (li.vid != 0)
                        {
                            DataTable dtqty = new DataTable();
                            dtqty = fpiclass.selectqtyremainusedfromscno1(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                li.vnono = Convert.ToInt64(dtqty.Rows[0]["pcno"].ToString());
                                fpiclass.updateqtyduringdelete(li);
                            }
                        }
                    }
                    if (li.pcno1 != 0 && li.pcno1 != null)
                    {
                        if (li.vid != 0)
                        {
                            //li.scno = li.scno1;
                            DataTable dtqty = new DataTable();
                            dtqty = fpiclass.selectqtyremainusedfromscno11(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                li.vnono = Convert.ToInt64(dtqty.Rows[0]["pcno"].ToString());
                                fpiclass.updateqtyduringdelete(li);
                            }
                        }
                    }
                }

                if (dtdata1.Rows.Count > 0)
                {
                    li.strpino = "";
                    li.stringpcno = dtdata1.Rows[0]["stringpcno"].ToString();
                    string[] words = li.stringpcno.Split(',');
                    foreach (string word in words)
                    {
                        li.pcno = Convert.ToInt64(word.ToString());
                        fpiclass.updatestrpinoinpcmaster(li);
                        fpiclass.updatestrpinoinpcmastera(li);
                        fpiclass.updatepcstatusforallchnoa1(li);
                    }
                }

                fpiclass.deletepimasterdatafrompinostring(li);
                fpiclass.deletepiitemsdatafrompinostring(li);
                fpiclass.deletepurchaseinvoiceledgeraa(li);
                li.stringpcno = dtdata1.Rows[0]["stringpcno"].ToString();
                //fpiclass.updatepcstatusforallchnoopen(li);
                fpiclass.updateisusedn(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strpino + " Purchase Invoice Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvpurchaseinvoicelist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}