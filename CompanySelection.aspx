﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CompanySelection.aspx.cs"
    Inherits="CompanySelection" Culture="hi-IN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="bootstrap/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="bootstrap/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="js/Jquery-ui.js" type="text/javascript"></script>
    <style type="text/css">
        #mydiv
        {
            top: 30%;
            right: 0%;
            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
            border: 1px solid #ccc;
            position: fixed;
            z-index: 111;
        }
        #mydivmobile
        {
            top: 30%;
            right: 0%;
            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
            border: 1px solid #ccc;
            position: fixed;
        }
        #helpdesk
        {
            height: 100%;
            position: fixed;
            background: #61A6F8;
            width: 90%;
            left: 0%;
            display: none;
            z-index: 5;
        }
        .blink {
    animation-duration: 1s;
    animation-name: blink;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    animation-timing-function: ease-in-out;
}
@keyframes blink {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //            $('a.sssss').on('click', function () {
            //                $('#helpdesk').slideToggle("slow");
            //            });
        });
    </script>
    <script type="text/javascript">
        function fullScreen() {
            window.open("purchasemaster.aspx", "", "fullscreen=yes", "width=1000px");
        }
        function closepopup() {
            self.close();
        }
    </script>
    <script type="text/javascript">

        function confirmdelete() {
            var data = confirm("Are You Sure To Delete?");
            if (data == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function confirmdelete1() {
            var data = confirm("Are You Sure To Move this person to Your Member List?");
            if (data == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        } 

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default" style="background-color: #673AB7;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="Dashboard.aspx" style="color:White !important">ALLURE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#" style="color:White !important"><asp:Label ID="lblacyear" runat="server"></asp:Label></a></li>
        <%--<li><a href="#" style="color:White !important"><asp:Label ID="lbltoperiod" runat="server" Text="To 31-03-2017"></asp:Label></a></li>--%>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:White !important"><asp:Label ID="lblcname" runat="server" Text="Shop name"></asp:Label> <span class="caret"></span></a>
          <ul class="dropdown-menu">           
           
            <li><a href="LogOut.aspx">LogOut</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="container">
        <span style="color: white; background-color: Red">Company Selection</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnnewcompany" runat="server" Text="Add New Company" class="btn btn-default forbutton"
                    PostBackUrl="~/CompanyList.aspx" Visible="false" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="480px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvcompanylist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" AllowPaging="True" OnRowCommand="gvcompanylist_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("cno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CNO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcno" ForeColor="Black" runat="server" Text='<%# bind("cno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Short Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcsnm" ForeColor="Black" runat="server" Text='<%# bind("csnm") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company Name" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblcname" runat="server" ForeColor="#505050" Text='<%# bind("cname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact No.1" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblcontact1" runat="server" ForeColor="#505050" Text='<%# bind("contact1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact No.2" SortExpression="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblcontact2" runat="server" ForeColor="Black" Text='<%# bind("contact2") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
