﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;

public partial class LogOut : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SessionMgt.UserID = 0;
            SessionMgt.ShopID = 0;
            //Session.Abandon();
            Request.Cookies["ForLogin"]["username"] = null;
            Request.Cookies["ForLogin"]["shopid"] = null;
            Request.Cookies["ForLogin"]["fromperiod"] = null;
            Request.Cookies["ForLogin"]["toperiod"] = null;
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("~/login.aspx");            
        }
    }
}