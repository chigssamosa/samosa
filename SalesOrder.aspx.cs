﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;
using System.Data.SqlClient;

public partial class SalesOrder : System.Web.UI.Page
{
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                getsono();
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                txtsodate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemsso"] = CreateTemplate();
                fillbifurcationdrop();
                Page.SetFocus(txtpartypono);
            }
            else
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                fillbifurcationdropedit();
                filleditdata();
            }
            //hideimage();

        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvsoitemlist.Rows[c].FindControl("imgbtnselect");
            if (btnfinalsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillbifurcationdrop()
    {
        DataTable dtdt = new DataTable();
        dtdt = fsoclass.selectallbifurcationo(li);
        if (dtdt.Rows.Count > 0)
        {
            drpbifurcationno.DataSource = dtdt;
            drpbifurcationno.DataTextField = "quono";
            drpbifurcationno.DataBind();
            drpbifurcationno.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpbifurcationno.Items.Clear();
            drpbifurcationno.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillbifurcationdropedit()
    {
        DataTable dtdt = new DataTable();
        dtdt = fsoclass.selectallbifurcationoedit(li);
        if (dtdt.Rows.Count > 0)
        {
            drpbifurcationno.DataSource = dtdt;
            drpbifurcationno.DataTextField = "quono";
            drpbifurcationno.DataBind();
            drpbifurcationno.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpbifurcationno.Items.Clear();
            drpbifurcationno.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void getsono()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fsoclass.selectunusedsalesorderno(li);
        dtvno = fsoclass.selectunusedsalesordernolast(li);
        if (dtvno.Rows.Count > 0)
        {
            txtsalesorderno.Text = (Convert.ToInt64(dtvno.Rows[0]["sono"].ToString()) + 1).ToString();
        }
        else
        {
            txtsalesorderno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.sono = Convert.ToInt64(Request["sono"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        ViewState["sono"] = li.sono;
        DataTable dtso = new DataTable();
        dtso = fsoclass.selectallsalesorderdatafromsono(li);
        if (dtso.Rows.Count > 0)
        {
            txtsalesorderno.Text = dtso.Rows[0]["sono"].ToString();
            txtsodate.Text = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //int zz = xxyear.IndexOf(yyyear);
            if (li.sodate <= yyyear1 && li.sodate >= yyyear)
            {

            }
            else
            {
                // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
                //  return;
                txtsodate.Enabled = false;
            }
            txtpartypono.Text = dtso.Rows[0]["pono"].ToString();
            if (dtso.Rows[0]["podate"].ToString().Trim() != string.Empty)
            {
                txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
            txtindentno.Text = dtso.Rows[0]["indentno"].ToString();
            if (dtso.Rows[0]["indentdate"].ToString().Trim() != string.Empty)
            {
                txtindentdate.Text = Convert.ToDateTime(dtso.Rows[0]["indentdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
            txtdispatchthrough.Text = dtso.Rows[0]["dispatchthrough"].ToString();
            txtdeliveryat.Text = dtso.Rows[0]["delat"].ToString();
            txtdeliveryat2.Text = dtso.Rows[0]["delat1"].ToString();
            txtdeliveryat3.Text = dtso.Rows[0]["delat2"].ToString();
            txtamount.Text = dtso.Rows[0]["totbasic"].ToString();
            txtcst.Text = dtso.Rows[0]["totcst"].ToString();
            txtvat.Text = dtso.Rows[0]["totvat"].ToString();
            txttotalso.Text = dtso.Rows[0]["totsoamt"].ToString();
            txtadvat.Text = dtso.Rows[0]["totaddvat"].ToString();
            txtinstructions1.Text = dtso.Rows[0]["inst1"].ToString();
            txtinstructions2.Text = dtso.Rows[0]["inst2"].ToString();
            txtinstructions3.Text = dtso.Rows[0]["inst3"].ToString();
            txtinstructions4.Text = dtso.Rows[0]["inst4"].ToString();
            txtinstructions5.Text = dtso.Rows[0]["inst5"].ToString();
            txtinstructions6.Text = dtso.Rows[0]["inst6"].ToString();
            txtinstructions7.Text = dtso.Rows[0]["inst7"].ToString();
            drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
            drpbifurcationno.SelectedValue = dtso.Rows[0]["quono"].ToString();
            txtuser.Text = dtso.Rows[0]["uname"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvsoitemlist.Visible = true;
                gvsoitemlist.DataSource = dtdata;
                gvsoitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvsoitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
            btnfinalsave.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(txtindentno);
    }

    [System.Web.Services.WebMethod]
    public void fetchccodedata()
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(drpacname);
    }

    public void count1()
    {
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double qty = 0;
        double rate = 0;
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        if (txtqty.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
        if (txtbasicamt.Text.Trim() != string.Empty)
        {
            baseamt = Convert.ToDouble(txtbasicamt.Text);
        }

        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvat.Text = vatp.ToString();
                txtgvadvat.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}

            }
            else
            {
                txtgvvat.Text = "0";
                txtgvadvat.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvat.Text = "0";
            txtgvadvat.Text = "0";
            txttaxtype.Text = "0";
        }

        if (txtgvvat.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvat.Text);
            txtvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvat.Text = "0";
            txtvatamt.Text = "0";
        }
        if (txtgvadvat.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadvat.Text);
            txtaddtaxamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadvat.Text = "0";
            txtaddtaxamt.Text = "0";
        }
        if (Convert.ToDouble(txtgvvat.Text) != 0)
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        else
        {
            if (txtgvcst.Text.Trim() != string.Empty)
            {
                cstp = Convert.ToDouble(txtgvcst.Text);
                txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgvcst.Text = "0";
                txtcstamount.Text = "0";
            }
        }
        vatamt = Convert.ToDouble(txtvatamt.Text);
        addvatamt = Convert.ToDouble(txtaddtaxamt.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        if (gvsoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
            {
                Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamt.Text);
                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                totamount = totamount + Convert.ToDouble(lblamount.Text);
            }
            txtamount.Text = totbasicamt.ToString();
            txtvat.Text = totvat.ToString();
            txtadvat.Text = totaddvat.ToString();
            txtcst.Text = totcst.ToString();
            txttotalso.Text = totamount.ToString();
        }
        else
        {
            txtamount.Text = "0";
            txtvat.Text = "0";
            txtadvat.Text = "0";
            txtcst.Text = "0";
            txttotalso.Text = "0";
        }

    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                if (dtdata.Rows[0]["vattype"].ToString().IndexOf("-") != -1)
                {
                    txttaxtype.Text = dtdata.Rows[0]["vattype"].ToString();
                    string vattype = dtdata.Rows[0]["vattype"].ToString();
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvatamt.Text = "0";
                    txtgvadvat.Text = vattype.Split('-')[1];
                    txtaddtaxamt.Text = "0";
                    count1();
                }
                else
                {
                    string vattype = dtdata.Rows[0]["vattype"].ToString();
                    txtgvcst.Text = vattype.ToString();
                    txtcstamount.Text = "0";
                    count1();
                }
            }
            else
            {
                fillitemnamedrop();
                txtqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtvatamt.Text = string.Empty;
                txtaddtaxamt.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            fillitemnamedrop();
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        Page.SetFocus(txtqty);
    }
    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvat);
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("otype", typeof(string));
        //dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        var cc = txtdescription1.Text.IndexOf("Size:");
        if (cc != -1)
        {
        }
        else
        {
            cc = txtdescription1.Text.IndexOf("Capacity:");
            if (cc != -1)
            {
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Description 1 must contain Size or Capacity so enter size in description 1 of item " + drpitemname.SelectedItem.Text + " and try again.');", true);
                return;
            }
        }
        if (btnadd.Text == "Add")
        {
            if (btnfinalsave.Text == "Save")
            {
                DataTable dt = (DataTable)Session["dtpitemsso"];
                DataRow dr = dt.NewRow();
                dr["id"] = gvsoitemlist.Rows.Count + 1;
                dr["itemname"] = drpitemname.SelectedItem.Text;
                dr["qty"] = txtqty.Text;
                dr["unit"] = txtunit.Text;
                dr["rate"] = txtrate.Text;
                dr["basicamount"] = txtbasicamt.Text;
                dr["taxtype"] = txttaxtype.Text;
                dr["vatp"] = txtgvvat.Text;
                dr["addtaxp"] = txtgvadvat.Text;
                dr["cstp"] = txtgvcst.Text;
                dr["vatamt"] = txtvatamt.Text;
                dr["addtaxamt"] = txtaddtaxamt.Text;
                dr["cstamt"] = txtcstamount.Text;
                dr["amount"] = txtgvamount.Text;
                dr["descr1"] = txtdescription1.Text;
                dr["descr2"] = txtdescription2.Text;
                dr["descr3"] = txtdescription3.Text;
                dr["otype"] = drpordnonord.SelectedItem.Text;
                dt.Rows.Add(dr);
                Session["dtpitemsso"] = dt;
                this.bindgrid();
                count1();
            }
            else
            {
                li.sono = Convert.ToInt64(txtsalesorderno.Text);
                li.vnono = Convert.ToInt64(txtsalesorderno.Text);
                li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.itemname = drpitemname.SelectedItem.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.qtyremain = li.qty;
                li.qtyused = 0;
                li.unit = txtunit.Text;
                li.rate = Convert.ToDouble(txtrate.Text);
                li.basicamount = Convert.ToDouble(txtbasicamt.Text);
                li.taxtype = txttaxtype.Text;
                li.vatp = Convert.ToDouble(txtgvvat.Text);
                li.addtaxp = Convert.ToDouble(txtgvadvat.Text);
                li.cstp = Convert.ToDouble(txtgvcst.Text);
                li.vatamt = Convert.ToDouble(txtvatamt.Text);
                li.addtaxamt = Convert.ToDouble(txtaddtaxamt.Text);
                li.cstamt = Convert.ToDouble(txtcstamount.Text);
                li.amount = Convert.ToDouble(txtgvamount.Text);
                li.descr1 = txtdescription1.Text;
                li.descr2 = txtdescription2.Text;
                li.descr3 = txtdescription3.Text;
                li.otype = drpordnonord.SelectedItem.Text;
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.vid = 0;
                fsoclass.insertsoitemsdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.sono + " Sales Order Item Inserted.";
                faclass.insertactivity(li);
                li.sono = Convert.ToInt64(txtsalesorderno.Text);
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsoitemlist.Visible = true;
                    gvsoitemlist.DataSource = dtdata;
                    gvsoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvsoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                count1();
            }
            fillitemnamedrop();
            hideimage();
            txtqty.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        else
        {
            if (btnfinalsave.Text == "Save")
            {
                DataTable dt = Session["dtpitemsso"] as DataTable;
                li.id = Convert.ToInt64(ViewState["gid"].ToString());
                DataRow[] result = dt.Select("id=" + li.id + "");

                result[0]["itemname"] = drpitemname.SelectedItem.Text;
                result[0]["qty"] = txtqty.Text;
                result[0]["unit"] = txtunit.Text;
                result[0]["rate"] = txtrate.Text;
                result[0]["basicamount"] = txtbasicamt.Text;
                result[0]["taxtype"] = txttaxtype.Text;
                result[0]["vatp"] = txtgvvat.Text;
                result[0]["addtaxp"] = txtgvadvat.Text;
                result[0]["cstp"] = txtgvcst.Text;
                result[0]["cstamt"] = txtcstamount.Text;
                result[0]["descr1"] = txtdescription1.Text;
                result[0]["descr2"] = txtdescription2.Text;
                result[0]["descr3"] = txtdescription3.Text;
                result[0]["vatamt"] = txtvatamt.Text;
                result[0]["addtaxamt"] = txtaddtaxamt.Text;
                result[0]["amount"] = txtgvamount.Text;
                result[0]["otype"] = drpordnonord.SelectedItem.Text;
                dt.AcceptChanges();
                Session["dtpitemsso"] = dt;
                bindgrid();
                count1();
                btnadd.Text = "Add";
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["gid"].ToString());
                li.itemname = drpitemname.SelectedItem.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.qtyremain = Convert.ToDouble(txtqty.Text);
                li.qtyused = 0;
                //li.qtyremain1 = Convert.ToDouble(lblqty.Text);
                //li.qtyused1 = 0;
                li.unit = txtunit.Text;
                li.rate = Convert.ToDouble(txtrate.Text);
                li.taxtype = txttaxtype.Text;
                li.vatp = Convert.ToDouble(txtgvvat.Text);
                li.addtaxp = Convert.ToDouble(txtgvadvat.Text);
                li.cstp = Convert.ToDouble(txtgvcst.Text);
                li.descr1 = txtdescription1.Text;
                li.descr2 = txtdescription2.Text;
                li.descr3 = txtdescription3.Text;
                li.basicamount = Convert.ToDouble(txtbasicamt.Text);
                li.cstamt = Convert.ToDouble(txtcstamount.Text);
                li.vatamt = Convert.ToDouble(txtvatamt.Text);
                li.addtaxamt = Convert.ToDouble(txtaddtaxamt.Text);
                li.amount = Convert.ToDouble(txtgvamount.Text);
                li.vnono = Convert.ToInt64(txtsalesorderno.Text);
                li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.otype = drpordnonord.SelectedItem.Text;
                fsoclass.updatesoitemsdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Sales Order Item Updated.";
                faclass.insertactivity(li);
                li.sono = Convert.ToInt64(txtsalesorderno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsoitemlist.Visible = true;
                    gvsoitemlist.DataSource = dtdata;
                    gvsoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvsoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                count1();
                btnadd.Text = "Add";
            }
            fillitemnamedrop();
            txtqty.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        hideimage();
        DataTable dtdataa = new DataTable();
        dtdataa = fsoclass.selectallitemname(li);
        if (dtdataa.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdataa;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        Page.SetFocus(drpitemname);

    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemsso"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvsoitemlist.Visible = true;
            lblempty.Visible = false;
            gvsoitemlist.DataSource = dtsession;
            gvsoitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvsoitemlist.DataSource = null;
            gvsoitemlist.DataBind();
            gvsoitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    public void count2()
    {
        double totbasicamt = 0;
        double totcst = 0;
        double totvat = 0;
        double totaddvat = 0;
        double totamount = 0;
        for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
        {
            Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
            Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
            Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
            totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamt.Text);
            totcst = totcst + Convert.ToDouble(lblsctamt.Text);
            totvat = totvat + Convert.ToDouble(lblvatamt.Text);
            totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
            totamount = totamount + Convert.ToDouble(lblamount.Text);
        }
        txtamount.Text = totbasicamt.ToString();
        txtvat.Text = totvat.ToString();
        txtadvat.Text = totaddvat.ToString();
        txtcst.Text = totcst.ToString();
        txttotalso.Text = totamount.ToString();
    }

    public void count22()
    {
        DataTable dt = Session["dtpitemsso"] as DataTable;
        double totbasicamt = 0;
        double totcst = 0;
        double totvat = 0;
        double totaddvat = 0;
        double totamount = 0;
        for (int c = 0; c < dt.Rows.Count; c++)
        {
            //Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
            //Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
            //Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
            //Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
            //Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
            totbasicamt = totbasicamt + Convert.ToDouble(dt.Rows[c]["basicamount"].ToString());
            totcst = totcst + Convert.ToDouble(dt.Rows[c]["cstamt"].ToString());
            totvat = totvat + Convert.ToDouble(dt.Rows[c]["vatamt"].ToString());
            totaddvat = totaddvat + Convert.ToDouble(dt.Rows[c]["addtaxamt"].ToString());
            totamount = totamount + Convert.ToDouble(dt.Rows[c]["amount"].ToString());
        }
        txtamount.Text = totbasicamt.ToString();
        txtvat.Text = totvat.ToString();
        txtadvat.Text = totaddvat.ToString();
        txtcst.Text = totcst.ToString();
        txttotalso.Text = totamount.ToString();
    }

    protected void btnfinalsave_Click(object sender, EventArgs e)
    {
        for (int f = 0; f < gvsoitemlist.Rows.Count; f++)
        {
            Label lblitemname = (Label)gvsoitemlist.Rows[f].FindControl("lblitemname");
            TextBox txtgvdesc1 = (TextBox)gvsoitemlist.Rows[f].FindControl("txtgvdesc1");
            var cc = txtgvdesc1.Text.IndexOf("Size:");
            if (cc != -1)
            {
            }
            else
            {
                cc = txtgvdesc1.Text.IndexOf("Capacity:");
                if (cc != -1)
                {
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Description 1 must contain Size or Capacity so enter size in description 1 of item " + lblitemname.Text + " and try again.');", true);
                    return;
                }
            }
        }
        li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        //if (li.sodate <= yyyear1 && li.sodate >= yyyear)
        //{

        //}
        //else
        //{
        //   // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
        //  //  return;
        //    txtsodate.Enabled = false;
        //}
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.sono = Convert.ToInt64(txtsalesorderno.Text);
        li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.strpono = txtpartypono.Text;
        if (txtpodate.Text.Trim() != string.Empty)
        {
            li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.podate = sqldatenull;
        }
        li.ccode = Convert.ToInt64(txtclientcode.Text);
        if (txtindentno.Text.Trim() != string.Empty)
        {
            li.indentno = Convert.ToInt64(txtindentno.Text);
        }
        else
        {
            li.indentno = 0;
        }
        if (txtindentdate.Text.Trim() != string.Empty)
        {
            li.indentdate = Convert.ToDateTime(txtindentdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.indentdate = sqldatenull;
        }
        li.acname = drpacname.SelectedItem.Text;
        li.dispatchedby = txtdispatchthrough.Text;
        li.delat = txtdeliveryat.Text;
        li.delat1 = txtdeliveryat2.Text;
        li.delat2 = txtdeliveryat3.Text;
        li.totbasicamount = Convert.ToDouble(txtamount.Text);
        li.totvatamt = Convert.ToDouble(txtvat.Text);
        li.totaddtaxamt = Convert.ToDouble(txtadvat.Text);
        li.totcstamt = Convert.ToDouble(txtcst.Text);
        li.totsoamt = Convert.ToDouble(txttotalso.Text);
        li.inst1 = txtinstructions1.Text;
        li.inst2 = txtinstructions2.Text;
        li.inst3 = txtinstructions3.Text;
        li.inst4 = txtinstructions4.Text;
        li.inst5 = txtinstructions5.Text;
        li.inst6 = txtinstructions6.Text;
        li.inst7 = txtinstructions7.Text;
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (drpbifurcationno.SelectedItem.Text != "--SELECT--")
        {
            li.quono = Convert.ToDouble(drpbifurcationno.Text);
        }
        else
        {
            li.quono = 0;
        }
        if (btnfinalsave.Text == "Save")
        {
            if (gvsoitemlist.Rows.Count > 0)
            {
                DataTable dtcheck = new DataTable();
                dtcheck = fsoclass.selectallsalesorderdatafromsono(li);
                if (dtcheck.Rows.Count == 0)
                {
                    fsoclass.insertsomasterdata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.sono + " Sales Order Inserted.";
                    faclass.insertactivity(li);
                    if (drpbifurcationno.SelectedItem.Text != "--SELECT--")
                    {
                        li.issipi = "Yes";
                        fsoclass.updateissodatainbifurcation(li);
                    }
                }
                else
                {
                    li.sono = Convert.ToInt64(txtsalesorderno.Text);
                    fsoclass.updateisused(li);
                    getsono();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Order No. already exist.Try Again.');", true);
                    return;
                }
                for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
                {
                    Label lblitemname = (Label)gvsoitemlist.Rows[c].FindControl("lblitemname");
                    Label lblqty = (Label)gvsoitemlist.Rows[c].FindControl("lblqty");
                    Label lblunit = (Label)gvsoitemlist.Rows[c].FindControl("lblunit");
                    Label lblrate = (Label)gvsoitemlist.Rows[c].FindControl("lblrate");
                    Label lblvattype = (Label)gvsoitemlist.Rows[c].FindControl("lblvattype");
                    Label lblvatp = (Label)gvsoitemlist.Rows[c].FindControl("lblvatp");
                    Label lbladdvatp = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatp");
                    Label lblcstp = (Label)gvsoitemlist.Rows[c].FindControl("lblcstp");
                    TextBox lbldesc1 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc1");
                    TextBox lbldesc2 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc2");
                    TextBox lbldesc3 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc3");
                    Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
                    Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
                    Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
                    Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
                    Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
                    Label lblotype = (Label)gvsoitemlist.Rows[c].FindControl("lblotype");
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.qtyremain = Convert.ToDouble(lblqty.Text);
                    li.qtyused = 0;
                    //li.qtyremain1 = Convert.ToDouble(lblqty.Text);
                    //li.qtyused1 = 0;
                    li.unit = lblunit.Text;
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.taxtype = lblvattype.Text;
                    li.vatp = Convert.ToDouble(lblvatp.Text);
                    li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                    li.cstp = Convert.ToDouble(lblcstp.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                    li.cstamt = Convert.ToDouble(lblsctamt.Text);
                    li.vatamt = Convert.ToDouble(lblvatamt.Text);
                    li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                    li.amount = Convert.ToDouble(lblamount.Text);
                    li.vnono = Convert.ToInt64(txtsalesorderno.Text);
                    li.otype = lblotype.Text;
                    fsoclass.insertsoitemsdata(li);
                    li.vid = SessionMgt.voucherno;
                    li.id = SessionMgt.voucherno;
                    fsoclass.updatevidinsoitems(li);
                }
                fsoclass.updateisused(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter sales order items and Try Again.');", true);
                return;
            }
        }
        else
        {
            if (ViewState["sono"].ToString().Trim() != txtsalesorderno.Text.Trim())
            {
                li.sono1 = Convert.ToInt64(txtsalesorderno.Text);
                li.sono = Convert.ToInt64(ViewState["sono"].ToString());
                fsoclass.updatesomasterdataupdateso(li);
                fsoclass.updatesoitemsdata11(li);
                fsoclass.deletesomasterdata(li);
                fsoclass.deletesoitemsdatafromsono(li);
            }
            li.sono = Convert.ToInt64(txtsalesorderno.Text);
            fsoclass.updatesomasterdata(li);
            li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            fsoclass.updatesoitemsdatedata11(li);

            //update all item data together
            for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
            {
                Label lblid = (Label)gvsoitemlist.Rows[c].FindControl("lblid");
                Label lblitemname = (Label)gvsoitemlist.Rows[c].FindControl("lblitemname");
                TextBox lblqty = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgridqty");
                TextBox lblunit = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvunit");
                TextBox lblrate = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvrate");
                TextBox lblvattype = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgridtaxtype");
                Label lblvatp = (Label)gvsoitemlist.Rows[c].FindControl("lblvatp");
                Label lbladdvatp = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatp");
                TextBox lblcstp = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgridcstp");
                TextBox lbldesc1 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc1");
                TextBox lbldesc2 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc2");
                TextBox lbldesc3 = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvdesc3");
                Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
                Label lblotype = (Label)gvsoitemlist.Rows[c].FindControl("lblotype");
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                li.qty = Convert.ToDouble(lblqty.Text);
                li.qtyremain = Convert.ToDouble(lblqty.Text);
                li.qtyused = 0;
                //li.qtyremain1 = Convert.ToDouble(lblqty.Text);
                //li.qtyused1 = 0;
                li.unit = lblunit.Text;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.taxtype = lblvattype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.basicamount = Convert.ToDouble(lblbasicamt.Text);
                li.cstamt = Convert.ToDouble(lblsctamt.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.amount = Convert.ToDouble(lblamount.Text);
                li.vnono = Convert.ToInt64(txtsalesorderno.Text);
                li.otype = lblotype.Text;
                //fsoclass.insertsoitemsdata(li);
                fsoclass.updatesoitemsdata(li);
                li.vid = SessionMgt.voucherno;
                li.id = SessionMgt.voucherno;
                fsoclass.updatevidinsoitems(li);
            }
            //

            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.sono + " Sales Order Updated.";
            faclass.insertactivity(li);
        }
        Response.Redirect("~/SalesOrderList.aspx?pagename=SalesOrderList");
    }
    protected void gvsoitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemsso"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemsso"] = dt;
            this.bindgrid();
            count2();
        }
        else
        {
            if (gvsoitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvsoitemlist.Rows[e.RowIndex].FindControl("lblid");
                li.id = Convert.ToInt64(lblid.Text);
                DataTable dtcheck1 = new DataTable();
                dtcheck1 = fsoclass.selectpodatafromsoid(li);
                if (dtcheck1.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Used in Purchase Order No. " + dtcheck1.Rows[0]["pono"].ToString() + " So You Cant delete this Item.First delete that item from challan then try to delete this item.');", true);
                    return;
                }
                DataTable dtcheck = new DataTable();
                dtcheck = fsoclass.selectchallandatafromsoid(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Used in Sales Challan No. " + dtcheck.Rows[0]["strscno"].ToString() + " So You Cant delete this Item.First delete that item from challan then try to delete this item.');", true);
                    return;
                }
                else
                {
                    fsoclass.deletesoitemsdatafromid(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Sales Order Item Deleted.";
                    faclass.insertactivity(li);
                    //li.istype = "CR";
                    //flclass.deleteledgerdata(li);
                    li.sono = Convert.ToInt64(txtsalesorderno.Text);
                    DataTable dtdata = new DataTable();
                    dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvsoitemlist.Visible = true;
                        gvsoitemlist.DataSource = dtdata;
                        gvsoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvsoitemlist.DataSource = null;
                        gvsoitemlist.DataBind();
                        gvsoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }

                    //update all data during delete
                    count2();
                    li.totbasicamount = Convert.ToDouble(txtamount.Text);
                    li.totvatamt = Convert.ToDouble(txtvat.Text);
                    li.totaddtaxamt = Convert.ToDouble(txtadvat.Text);
                    li.totcstamt = Convert.ToDouble(txtcst.Text);
                    li.totsoamt = Convert.ToDouble(txttotalso.Text);
                    fsoclass.updatesomasterdataduringdelete(li);
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        hideimage();
    }
    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtunit);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvadvat);
    }
    protected void txtgvadvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvcst);
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        var cc = txtgvcst.Text.IndexOf("-");
        if (cc != -1)
        {
            txtgvcst.Text = "0";
        }
        count1();
        Page.SetFocus(txtcstamount);
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtdescription1);
    }
    protected void txtvatamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtaddtaxamt);
    }
    protected void txtaddtaxamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvamount);
    }
    protected void txtgvamount_TextChanged(object sender, EventArgs e)
    {
        count1();
    }
    protected void txtamount_TextChanged(object sender, EventArgs e)
    {
        double basic = 0;
        double vat = 0;
        double addvat = 0;
        double cst = 0;
        double totso = 0;
        if (txtamount.Text.Trim() != string.Empty)
        {
            basic = Convert.ToDouble(txtamount.Text);
        }
        if (txtcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txtcst.Text);
        }
        if (txtvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txtvat.Text);
        }
        if (txtadvat.Text.Trim() != string.Empty)
        {
            addvat = Convert.ToDouble(txtadvat.Text);
        }
        txttotalso.Text = (basic + cst + vat + addvat).ToString();
        Page.SetFocus(txtcst);
    }
    protected void txtcst_TextChanged(object sender, EventArgs e)
    {
        double basic = 0;
        double vat = 0;
        double addvat = 0;
        double cst = 0;
        double totso = 0;
        if (txtamount.Text.Trim() != string.Empty)
        {
            basic = Convert.ToDouble(txtamount.Text);
        }
        if (txtcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txtcst.Text);
        }
        if (txtvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txtvat.Text);
        }
        if (txtadvat.Text.Trim() != string.Empty)
        {
            addvat = Convert.ToDouble(txtadvat.Text);
        }
        txttotalso.Text = (basic + cst + vat + addvat).ToString();
        Page.SetFocus(txtvat);
    }
    protected void txtvat_TextChanged(object sender, EventArgs e)
    {
        double basic = 0;
        double vat = 0;
        double addvat = 0;
        double cst = 0;
        double totso = 0;
        if (txtamount.Text.Trim() != string.Empty)
        {
            basic = Convert.ToDouble(txtamount.Text);
        }
        if (txtcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txtcst.Text);
        }
        if (txtvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txtvat.Text);
        }
        if (txtadvat.Text.Trim() != string.Empty)
        {
            addvat = Convert.ToDouble(txtadvat.Text);
        }
        txttotalso.Text = (basic + cst + vat + addvat).ToString();
        Page.SetFocus(txttotalso);
    }

    protected void txtadvat_TextChanged(object sender, EventArgs e)
    {
        double basic = 0;
        double vat = 0;
        double addvat = 0;
        double cst = 0;
        double totso = 0;
        if (txtamount.Text.Trim() != string.Empty)
        {
            basic = Convert.ToDouble(txtamount.Text);
        }
        if (txtcst.Text.Trim() != string.Empty)
        {
            cst = Convert.ToDouble(txtcst.Text);
        }
        if (txtvat.Text.Trim() != string.Empty)
        {
            vat = Convert.ToDouble(txtvat.Text);
        }
        if (txtadvat.Text.Trim() != string.Empty)
        {
            addvat = Convert.ToDouble(txtadvat.Text);
        }
        txttotalso.Text = (basic + cst + vat + addvat).ToString();
        Page.SetFocus(txtinstructions1);
    }
    protected void gvsoitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (btnfinalsave.Text == "Save")
            {
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallitemnameall(li);
                if (dtdata.Rows.Count > 0)
                {
                    drpitemname.Items.Clear();
                    drpitemname.DataSource = dtdata;
                    drpitemname.DataTextField = "itemname";
                    drpitemname.DataBind();
                    drpitemname.Items.Insert(0, "--SELECT--");
                }
                else
                {
                    drpitemname.Items.Clear();
                    drpitemname.Items.Insert(0, "--SELECT--");
                }
                DataTable dt = Session["dtpitemsso"] as DataTable;
                li.id = Convert.ToInt64(e.CommandArgument);
                DataRow[] result = dt.Select("id=" + li.id + "");
                ViewState["gid"] = result[0]["id"].ToString();
                drpitemname.SelectedValue = result[0]["itemname"].ToString();
                txtqty.Text = result[0]["qty"].ToString();
                txtunit.Text = result[0]["unit"].ToString();
                txtrate.Text = result[0]["rate"].ToString();
                txtbasicamt.Text = result[0]["basicamount"].ToString();
                txttaxtype.Text = result[0]["taxtype"].ToString();
                txtgvvat.Text = result[0]["vatp"].ToString();
                txtgvadvat.Text = result[0]["addtaxp"].ToString();
                txtgvcst.Text = result[0]["cstp"].ToString();
                txtcstamount.Text = result[0]["cstamt"].ToString();
                txtdescription1.Text = result[0]["descr1"].ToString();
                txtdescription2.Text = result[0]["descr2"].ToString();
                txtdescription3.Text = result[0]["descr3"].ToString();
                txtvatamt.Text = result[0]["vatamt"].ToString();
                txtaddtaxamt.Text = result[0]["addtaxamt"].ToString();
                txtgvamount.Text = result[0]["amount"].ToString();
                drpordnonord.SelectedValue = result[0]["otype"].ToString();
                btnadd.Text = "Update";
            }
            else
            {
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallitemnameall(li);
                if (dtdata.Rows.Count > 0)
                {
                    drpitemname.Items.Clear();
                    drpitemname.DataSource = dtdata;
                    drpitemname.DataTextField = "itemname";
                    drpitemname.DataBind();
                    drpitemname.Items.Insert(0, "--SELECT--");
                }
                else
                {
                    drpitemname.Items.Clear();
                    drpitemname.Items.Insert(0, "--SELECT--");
                }
                li.id = Convert.ToInt64(e.CommandArgument);
                ViewState["gid"] = li.id;
                DataTable dtitem = new DataTable();
                dtitem = fsoclass.selectallsalesorderitemdatafromid(li);
                if (dtitem.Rows.Count > 0)
                {
                    drpitemname.SelectedValue = dtitem.Rows[0]["itemname"].ToString();
                    txtqty.Text = dtitem.Rows[0]["qty"].ToString();
                    txtunit.Text = dtitem.Rows[0]["unit"].ToString();
                    txtrate.Text = dtitem.Rows[0]["rate"].ToString();
                    txtbasicamt.Text = dtitem.Rows[0]["basicamount"].ToString();
                    txttaxtype.Text = dtitem.Rows[0]["taxtype"].ToString();
                    txtgvvat.Text = dtitem.Rows[0]["vatp"].ToString();
                    txtgvadvat.Text = dtitem.Rows[0]["addtaxp"].ToString();
                    txtgvcst.Text = dtitem.Rows[0]["cstp"].ToString();
                    txtcstamount.Text = dtitem.Rows[0]["cstamt"].ToString();
                    txtdescription1.Text = dtitem.Rows[0]["descr1"].ToString();
                    txtdescription2.Text = dtitem.Rows[0]["descr2"].ToString();
                    txtdescription3.Text = dtitem.Rows[0]["descr3"].ToString();
                    txtvatamt.Text = dtitem.Rows[0]["vatamt"].ToString();
                    txtaddtaxamt.Text = dtitem.Rows[0]["addtaxamt"].ToString();
                    txtgvamount.Text = dtitem.Rows[0]["amount"].ToString();
                    if (dtitem.Rows[0]["otype"].ToString() != "" && dtitem.Rows[0]["otype"].ToString() != string.Empty)
                    {
                        drpordnonord.SelectedValue = dtitem.Rows[0]["otype"].ToString();
                    }
                    btnadd.Text = "Update";
                }
            }
        }
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                    string vattype = dtdata.Rows[0]["gsttype"].ToString();
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvatamt.Text = "0";
                    txtgvadvat.Text = vattype.Split('-')[1];
                    txtaddtaxamt.Text = "0";
                    count1();
                }
                else
                {
                    string vattype = dtdata.Rows[0]["vattype"].ToString();
                    txtgvcst.Text = vattype.ToString();
                    txtcstamount.Text = "0";
                    count1();
                }
            }
            else
            {
                fillitemnamedrop();
                txtqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtvatamt.Text = string.Empty;
                txtaddtaxamt.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            fillitemnamedrop();
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        Page.SetFocus(txtqty);
    }
    protected void drpbifurcationno_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpbifurcationno.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.quono = Convert.ToDouble(drpbifurcationno.SelectedItem.Text);
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectalldatafrombifurcationo(li);
            if (dtdata.Rows.Count > 0)
            {
                txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
                //drpacname.SelectedValue=dtdata.Rows[0]["acname"].ToString();
                //gvsoitemlist.DataSource = dtdata;
                //gvsoitemlist.DataBind();


                DataTable dt = (DataTable)Session["dtpitemsso"];
                for (int c = 0; c < dtdata.Rows.Count; c++)
                {
                    DataRow dr = dt.NewRow();
                    dr["id"] = gvsoitemlist.Rows.Count + 1;
                    dr["itemname"] = dtdata.Rows[c]["itemname"].ToString();
                    dr["qty"] = dtdata.Rows[c]["qty"].ToString();
                    dr["unit"] = dtdata.Rows[c]["unit"].ToString();
                    dr["rate"] = dtdata.Rows[c]["rate"].ToString();
                    dr["basicamount"] = dtdata.Rows[c]["basicamt"].ToString();
                    dr["taxtype"] = dtdata.Rows[c]["taxtype"].ToString();
                    dr["vatp"] = dtdata.Rows[c]["vatp"].ToString();
                    dr["addtaxp"] = dtdata.Rows[c]["addtaxp"].ToString();
                    dr["cstp"] = dtdata.Rows[c]["cstp"].ToString();
                    dr["vatamt"] = dtdata.Rows[c]["vatamt"].ToString();
                    dr["addtaxamt"] = dtdata.Rows[c]["addtaxamt"].ToString();
                    dr["cstamt"] = dtdata.Rows[c]["cstamt"].ToString();
                    dr["amount"] = dtdata.Rows[c]["amount"].ToString();
                    dr["descr1"] = dtdata.Rows[c]["descr1"].ToString();
                    dr["descr2"] = dtdata.Rows[c]["descr2"].ToString();
                    dr["descr3"] = dtdata.Rows[c]["descr3"].ToString();
                    dt.Rows.Add(dr);
                }
                Session["dtpitemsso"] = dt;
                this.bindgrid();
                count1();
                hideimage();
            }
        }
        else
        {
            gvsoitemlist.DataSource = null;
            gvsoitemlist.DataBind();
        }
    }
    protected void drpacname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtdt = new DataTable();
            dtdt = fsoclass.selectallbifurcationofromacname(li);
            if (dtdt.Rows.Count > 0)
            {
                drpbifurcationno.DataSource = dtdt;
                drpbifurcationno.DataTextField = "quono";
                drpbifurcationno.DataBind();
                drpbifurcationno.Items.Insert(0, "--SELECT--");
            }
            else
            {
                drpbifurcationno.Items.Clear();
                drpbifurcationno.Items.Insert(0, "--SELECT--");
            }
        }
        else
        {
            drpbifurcationno.Items.Clear();
            drpbifurcationno.Items.Insert(0, "--SELECT--");
        }
        Page.SetFocus(txtdispatchthrough);
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder order by sono", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(dtdataq.Rows[0]["sono"].ToString());
            ViewState["sono"] = li.sono;
            DataTable dtso = new DataTable();
            dtso = fsoclass.selectallsalesorderdatafromsono(li);
            if (dtso.Rows.Count > 0)
            {
                txtsalesorderno.Text = dtso.Rows[0]["sono"].ToString();
                txtsodate.Text = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpartypono.Text = dtso.Rows[0]["pono"].ToString();
                if (dtso.Rows[0]["podate"].ToString().Trim() != string.Empty)
                {
                    txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                txtindentno.Text = dtso.Rows[0]["indentno"].ToString();
                if (dtso.Rows[0]["indentdate"].ToString().Trim() != string.Empty)
                {
                    txtindentdate.Text = Convert.ToDateTime(dtso.Rows[0]["indentdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                txtdispatchthrough.Text = dtso.Rows[0]["dispatchthrough"].ToString();
                txtdeliveryat.Text = dtso.Rows[0]["delat"].ToString();
                txtdeliveryat2.Text = dtso.Rows[0]["delat1"].ToString();
                txtdeliveryat3.Text = dtso.Rows[0]["delat2"].ToString();
                txtamount.Text = dtso.Rows[0]["totbasic"].ToString();
                txtcst.Text = dtso.Rows[0]["totcst"].ToString();
                txtvat.Text = dtso.Rows[0]["totvat"].ToString();
                txttotalso.Text = dtso.Rows[0]["totsoamt"].ToString();
                txtadvat.Text = dtso.Rows[0]["totaddvat"].ToString();
                txtinstructions1.Text = dtso.Rows[0]["inst1"].ToString();
                txtinstructions2.Text = dtso.Rows[0]["inst2"].ToString();
                txtinstructions3.Text = dtso.Rows[0]["inst3"].ToString();
                txtinstructions4.Text = dtso.Rows[0]["inst4"].ToString();
                txtinstructions5.Text = dtso.Rows[0]["inst5"].ToString();
                txtinstructions6.Text = dtso.Rows[0]["inst6"].ToString();
                txtinstructions7.Text = dtso.Rows[0]["inst7"].ToString();
                drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                if (dtso.Rows[0]["quono"].ToString() != "--SELECT--" && dtso.Rows[0]["quono"].ToString() != "0")
                {
                    drpbifurcationno.SelectedValue = dtso.Rows[0]["quono"].ToString();
                }
                txtuser.Text = dtso.Rows[0]["uname"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsoitemlist.Visible = true;
                    gvsoitemlist.DataSource = dtdata;
                    gvsoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvsoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                btnfinalsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder order by sono", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(dtdataq.Rows[dtdataq.Rows.Count - 1]["sono"].ToString());
            ViewState["sono"] = li.sono;
            DataTable dtso = new DataTable();
            dtso = fsoclass.selectallsalesorderdatafromsono(li);
            if (dtso.Rows.Count > 0)
            {
                txtsalesorderno.Text = dtso.Rows[0]["sono"].ToString();
                txtsodate.Text = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpartypono.Text = dtso.Rows[0]["pono"].ToString();
                if (dtso.Rows[0]["podate"].ToString().Trim() != string.Empty)
                {
                    txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                txtindentno.Text = dtso.Rows[0]["indentno"].ToString();
                if (dtso.Rows[0]["indentdate"].ToString().Trim() != string.Empty)
                {
                    txtindentdate.Text = Convert.ToDateTime(dtso.Rows[0]["indentdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                txtdispatchthrough.Text = dtso.Rows[0]["dispatchthrough"].ToString();
                txtdeliveryat.Text = dtso.Rows[0]["delat"].ToString();
                txtdeliveryat2.Text = dtso.Rows[0]["delat1"].ToString();
                txtdeliveryat3.Text = dtso.Rows[0]["delat2"].ToString();
                txtamount.Text = dtso.Rows[0]["totbasic"].ToString();
                txtcst.Text = dtso.Rows[0]["totcst"].ToString();
                txtvat.Text = dtso.Rows[0]["totvat"].ToString();
                txttotalso.Text = dtso.Rows[0]["totsoamt"].ToString();
                txtadvat.Text = dtso.Rows[0]["totaddvat"].ToString();
                txtinstructions1.Text = dtso.Rows[0]["inst1"].ToString();
                txtinstructions2.Text = dtso.Rows[0]["inst2"].ToString();
                txtinstructions3.Text = dtso.Rows[0]["inst3"].ToString();
                txtinstructions4.Text = dtso.Rows[0]["inst4"].ToString();
                txtinstructions5.Text = dtso.Rows[0]["inst5"].ToString();
                txtinstructions6.Text = dtso.Rows[0]["inst6"].ToString();
                txtinstructions7.Text = dtso.Rows[0]["inst7"].ToString();
                drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                if (dtso.Rows[0]["quono"].ToString() != "--SELECT--" && dtso.Rows[0]["quono"].ToString() != "0")
                {
                    drpbifurcationno.SelectedValue = dtso.Rows[0]["quono"].ToString();
                }
                txtuser.Text = dtso.Rows[0]["uname"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvsoitemlist.Visible = true;
                    gvsoitemlist.DataSource = dtdata;
                    gvsoitemlist.DataBind();
                    lblcount.Text = dtdata.Rows.Count.ToString();
                }
                else
                {
                    gvsoitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Order Items Found.";
                    lblcount.Text = "0";
                }
                btnfinalsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from SalesOrder order by sono", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.sono = Convert.ToInt64(txtsalesorderno.Text) + 1;
        for (Int64 i = li.sono; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["sono"].ToString()); i++)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder where sono=" + li.sono + " order by sono", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(dtdataq.Rows[0]["sono"].ToString());
                ViewState["sono"] = li.sono;
                DataTable dtso = new DataTable();
                dtso = fsoclass.selectallsalesorderdatafromsono(li);
                if (dtso.Rows.Count > 0)
                {
                    txtsalesorderno.Text = dtso.Rows[0]["sono"].ToString();
                    txtsodate.Text = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpartypono.Text = dtso.Rows[0]["pono"].ToString();
                    if (dtso.Rows[0]["podate"].ToString().Trim() != string.Empty)
                    {
                        txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                    txtindentno.Text = dtso.Rows[0]["indentno"].ToString();
                    if (dtso.Rows[0]["indentdate"].ToString().Trim() != string.Empty)
                    {
                        txtindentdate.Text = Convert.ToDateTime(dtso.Rows[0]["indentdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                    txtdispatchthrough.Text = dtso.Rows[0]["dispatchthrough"].ToString();
                    txtdeliveryat.Text = dtso.Rows[0]["delat"].ToString();
                    txtdeliveryat2.Text = dtso.Rows[0]["delat1"].ToString();
                    txtdeliveryat3.Text = dtso.Rows[0]["delat2"].ToString();
                    txtamount.Text = dtso.Rows[0]["totbasic"].ToString();
                    txtcst.Text = dtso.Rows[0]["totcst"].ToString();
                    txtvat.Text = dtso.Rows[0]["totvat"].ToString();
                    txttotalso.Text = dtso.Rows[0]["totsoamt"].ToString();
                    txtadvat.Text = dtso.Rows[0]["totaddvat"].ToString();
                    txtinstructions1.Text = dtso.Rows[0]["inst1"].ToString();
                    txtinstructions2.Text = dtso.Rows[0]["inst2"].ToString();
                    txtinstructions3.Text = dtso.Rows[0]["inst3"].ToString();
                    txtinstructions4.Text = dtso.Rows[0]["inst4"].ToString();
                    txtinstructions5.Text = dtso.Rows[0]["inst5"].ToString();
                    txtinstructions6.Text = dtso.Rows[0]["inst6"].ToString();
                    txtinstructions7.Text = dtso.Rows[0]["inst7"].ToString();
                    drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                    if (dtso.Rows[0]["quono"].ToString() != "--SELECT--" && dtso.Rows[0]["quono"].ToString() != "0")
                    {
                        drpbifurcationno.SelectedValue = dtso.Rows[0]["quono"].ToString();
                    }
                    txtuser.Text = dtso.Rows[0]["uname"].ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvsoitemlist.Visible = true;
                        gvsoitemlist.DataSource = dtdata;
                        gvsoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvsoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                    btnfinalsave.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from SalesOrder order by sono", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.sono = Convert.ToInt64(txtsalesorderno.Text) - 1;
        for (Int64 i = li.sono; i >= Convert.ToInt64(dtdataqa.Rows[0]["sono"].ToString()); i--)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SalesOrder where sono=" + li.sono + " order by sono", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(dtdataq.Rows[0]["sono"].ToString());
                ViewState["sono"] = li.sono;
                DataTable dtso = new DataTable();
                dtso = fsoclass.selectallsalesorderdatafromsono(li);
                if (dtso.Rows.Count > 0)
                {
                    txtsalesorderno.Text = dtso.Rows[0]["sono"].ToString();
                    txtsodate.Text = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpartypono.Text = dtso.Rows[0]["pono"].ToString();
                    if (dtso.Rows[0]["podate"].ToString().Trim() != string.Empty)
                    {
                        txtpodate.Text = Convert.ToDateTime(dtso.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtclientcode.Text = dtso.Rows[0]["ccode"].ToString();
                    txtindentno.Text = dtso.Rows[0]["indentno"].ToString();
                    if (dtso.Rows[0]["indentdate"].ToString().Trim() != string.Empty)
                    {
                        txtindentdate.Text = Convert.ToDateTime(dtso.Rows[0]["indentdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtso.Rows[0]["acname"].ToString();
                    txtdispatchthrough.Text = dtso.Rows[0]["dispatchthrough"].ToString();
                    txtdeliveryat.Text = dtso.Rows[0]["delat"].ToString();
                    txtdeliveryat2.Text = dtso.Rows[0]["delat1"].ToString();
                    txtdeliveryat3.Text = dtso.Rows[0]["delat2"].ToString();
                    txtamount.Text = dtso.Rows[0]["totbasic"].ToString();
                    txtcst.Text = dtso.Rows[0]["totcst"].ToString();
                    txtvat.Text = dtso.Rows[0]["totvat"].ToString();
                    txttotalso.Text = dtso.Rows[0]["totsoamt"].ToString();
                    txtadvat.Text = dtso.Rows[0]["totaddvat"].ToString();
                    txtinstructions1.Text = dtso.Rows[0]["inst1"].ToString();
                    txtinstructions2.Text = dtso.Rows[0]["inst2"].ToString();
                    txtinstructions3.Text = dtso.Rows[0]["inst3"].ToString();
                    txtinstructions4.Text = dtso.Rows[0]["inst4"].ToString();
                    txtinstructions5.Text = dtso.Rows[0]["inst5"].ToString();
                    txtinstructions6.Text = dtso.Rows[0]["inst6"].ToString();
                    txtinstructions7.Text = dtso.Rows[0]["inst7"].ToString();
                    drpstatus.SelectedValue = dtso.Rows[0]["status"].ToString();
                    if (dtso.Rows[0]["quono"].ToString() != "--SELECT--" && dtso.Rows[0]["quono"].ToString() != "0")
                    {
                        drpbifurcationno.SelectedValue = dtso.Rows[0]["quono"].ToString();
                    }
                    txtuser.Text = dtso.Rows[0]["uname"].ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fsoclass.selectallsalesorderitemdatafromsono(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvsoitemlist.Visible = true;
                        gvsoitemlist.DataSource = dtdata;
                        gvsoitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvsoitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                    btnfinalsave.Text = "Update";
                    return;
                }
            }
        }
    }

    public void countgv1()
    {
        //DataTable dtsoitem = (DataTable)Session["dtpitems"];
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double fvatamt = 0;
        double faddvatamt = 0;
        double fcstamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double fbasicamt = 0;
        double amount = 0;
        for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
        {
            TextBox txtgridqty = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgridqty");
            TextBox txtgridrate = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgvrate");
            Label lblbasicamt = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
            Label lblvatp = (Label)gvsoitemlist.Rows[c].FindControl("lblvatp");
            Label lbladdvatp = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatp");
            //Label lblcstp = (Label)gvpoitemlist.Rows[c].FindControl("lblcstp");
            TextBox txtgridcstp = (TextBox)gvsoitemlist.Rows[c].FindControl("txtgridcstp");
            Label lblvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
            Label lbladdvatamt = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
            Label lblcstamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
            Label lblamount = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
            baseamt = Convert.ToDouble(txtgridqty.Text) * Convert.ToDouble(txtgridrate.Text);
            lblbasicamt.Text = baseamt.ToString();
            vatamt = ((Convert.ToDouble(lblvatp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            addvatamt = ((Convert.ToDouble(lbladdvatp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            cstamt = ((Convert.ToDouble(txtgridcstp.Text)) * (Convert.ToDouble(lblbasicamt.Text))) / 100;
            lblvatamt.Text = vatamt.ToString();
            lbladdvatamt.Text = addvatamt.ToString();
            lblcstamt.Text = cstamt.ToString();
            amount = amount + baseamt + vatamt + addvatamt + cstamt;
            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
            fbasicamt = fbasicamt + baseamt;
            fvatamt = fvatamt + vatamt;
            faddvatamt = faddvatamt + addvatamt;
            fcstamt = fcstamt + cstamt;
            vatp = Convert.ToDouble(lblvatp.Text) + Convert.ToDouble(lbladdvatp.Text);
            //if (dtsoitem.Rows[c]["cstp"].ToString() != "")
            //{
            //    cstp = cstp + Convert.ToDouble(dtsoitem.Rows[c]["cstp"].ToString());
            //}
            //if (dtsoitem.Rows[c]["cstamt"].ToString() != "")
            //{
            //    cstamt = cstamt + Convert.ToDouble(dtsoitem.Rows[c]["cstamt"].ToString());
            //}
            //if (dtsoitem.Rows[c]["vatp"].ToString() != "")
            //{
            //    vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["vatp"].ToString());
            //}
            //if (dtsoitem.Rows[c]["addtaxp"].ToString() != "")
            //{
            //    vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["addtaxp"].ToString());
            //}

        }
        txtamount.Text = fbasicamt.ToString();
        txtcst.Text = fcstamt.ToString();
        //txtfvatamt.Text = (vatamt + addvatamt).ToString();
        txtvat.Text = fvatamt.ToString();
        txtadvat.Text = faddvatamt.ToString();
        txttotalso.Text = (Convert.ToDouble(txtamount.Text) + Convert.ToDouble(txtcst.Text) + Convert.ToDouble(txtvat.Text) + Convert.ToDouble(txtadvat.Text)).ToString();
    }

    protected void txtgridqty_TextChanged(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
            Label txtgvamount1 = (Label)currentRow.FindControl("lblamount");
            TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
            //if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
            //{
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                //countgv();
                countgv1();
            }
            Page.SetFocus(txtgvunit);
            //}
            //else
            //{
            //    txtgvqty1.Text = lblqtyremain.Text;
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
            //    return;
            //}
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
            Label txtgvamount1 = (Label)currentRow.FindControl("lblamount");
            TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
            //li.vid = Convert.ToInt64(lblid.Text);
            //DataTable dtremain = new DataTable();
            //dtremain = fpoclass.selectqtyremainusedfromsono(li);
            //if (dtremain.Rows.Count > 0)
            //{
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain1"].ToString()) + Convert.ToDouble(lblqty.Text);
            //    //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
            //    if (Convert.ToDouble(txtgvqty1.Text) > rqty)
            //    {
            //        //    li.id = Convert.ToInt64(lblid.Text);
            //        //    li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
            //        //    li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
            //        //    fscclass.updateremainqtyinsalesorder(li);
            //        //}
            //        //else
            //        //{
            //        txtgvqty1.Text = lblqty.Text;
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
            //        return;
            //    }
            //    else
            //    {
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                //countgv();
                countgv1();
            }
            Page.SetFocus(txtgvunit);
            //    }
            //}
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getunit(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallunit(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void txtgvrate_TextChanged(object sender, EventArgs e)
    {
        //count1();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        TextBox txtgridtaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        {
            countgv1();
        }
        Page.SetFocus(txtgridtaxtype);
    }

    protected void txtgridtaxtype_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        Label txtgvamount1 = (Label)currentRow.FindControl("lblbasicamt");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
            //lbltaxtype.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvsoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txtamount.Text = totbasicamt.ToString();
            txtvat.Text = totvat.ToString();
            txtadvat.Text = totaddvat.ToString();
            txtcst.Text = totcst.ToString();
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txttotalso.Text = (Convert.ToDouble(txtamount.Text) + Convert.ToDouble(txtvat.Text) + Convert.ToDouble(txtadvat.Text) + Convert.ToDouble(txtcst.Text)).ToString();
            //countgv();
        }
        else
        {
            txtamount.Text = "0";
            txtcst.Text = "0";
            txtvat.Text = "0";
            txtadvat.Text = "0";
            txttotalso.Text = "0";
        }
        Page.SetFocus(txtgridcstp);
    }

    protected void txtgridcstp_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgvrate");
        Label txtgvamount1 = (Label)currentRow.FindControl("lblbasicamt");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lblamount");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        var cc1 = txtgridcstp.Text.IndexOf("-");
        if (cc1 != -1)
        {
            txtgridcstp.Text = "0";
        }
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        if (gvsoitemlist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
            {
                Label lblbasicamount = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
                Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
                Label lblamount1 = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
                if (lblsctamt.Text != string.Empty)
                {
                    totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                }
                if (lblvatamt1.Text != string.Empty)
                {
                    totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
                }
                if (lbladdvatamt1.Text != string.Empty)
                {
                    totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
                }
                if (lblamount1.Text != string.Empty)
                {
                    totamount = totamount + Convert.ToDouble(lblamount1.Text);
                }
            }
            txtamount.Text = totbasicamt.ToString();
            txtvat.Text = (totvat + totaddvat).ToString();
            txtadvat.Text = (totvat + totaddvat).ToString();
            txtcst.Text = totcst.ToString();
            //lblamount.Text = (totbasicamt + totvat + totaddvat + totcst + cartage + roundoff + serant).ToString();
            txttotalso.Text = (Convert.ToDouble(txtamount.Text) + Convert.ToDouble(txtvat.Text) + Convert.ToDouble(txtadvat.Text) + Convert.ToDouble(txtcst.Text)).ToString();
        }
        else
        {
            txtamount.Text = "0";
            txtcst.Text = "0";
            txtvat.Text = "0";
            txtadvat.Text = "0";
            txttotalso.Text = "0";
        }
    }


}