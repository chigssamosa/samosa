﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PettyCashExpense.aspx.cs" Inherits="PettyCashExpense" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Petty Cash Expense</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Voucher No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtvoucherno" runat="server" CssClass="form-control" Width="150px"
                    onkeypress="return checkQuote();"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Petty Cash Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-6">
                <asp:TextBox ID="txtpettycashname" runat="server" CssClass="form-control" Width="393px"
                    onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtpettycashname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetBankname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    User</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Account Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-3 text-center">
                <label class="control-label">
                    Remarks</label>
            </div>
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Client Code</label>
            </div>
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Amount<span style="color: Red;">*</span></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <%-- <asp:TextBox ID="txtaccountname" runat="server" CssClass="form-control" placeholder="Account Name"
                    onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtaccountname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" placeholder="Remarks"
                    onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" placeholder="Client Code"
                    onkeypress="return checkQuote();"></asp:TextBox><%-- AutoPostBack="True" 
                    ontextchanged="txtclientcode_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtclientcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" placeholder="Amount"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsaves" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvpetty" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvpetty" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                        OnRowDeleting="rptlist_RowDeleting" BackColor="White" BorderColor="White" BorderWidth="2px"
                        CellPadding="3" CellSpacing="1" GridLines="None">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="Csnm" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# Eval("id") %>' />
                                    <asp:TextBox ID="lblacname" runat="server" Text='<%# Eval("acname") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender25" runat="server" TargetControlID="lblacname"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="GetAccountname">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblremarks" runat="server" Text='<%# Eval("remarks") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblclientcode" runat="server" Text='<%# Eval("ccode") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="lblclientcode"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="GetClientname">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblamount" runat="server" Text='<%# Eval("amount") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Cheque No" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblchequeno" runat="server" Text='<%# Eval("chequeno") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ag.Bill" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblagbill" runat="server" Text='<%# Eval("agbill") %>' />
                                    <asp:Label ID="lblgvsino" runat="server" Visible="false" Text='<%# Eval("sino") %>' />
                                    <asp:Label ID="lblgvpaidamount" runat="server" Visible="false" Text='<%# Eval("paidamount") %>' />
                                    <asp:Label ID="lblgvsipi" runat="server" Text='<%# Eval("issipi") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>--%>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#594B9C" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#33276A" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="text-align: center; color: Red">
            <label class="control-label">
                Total Amount :
            </label>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="lbltotal" runat="server" CssClass="control-label"
                Font-Bold="true" Font-Size="15px"></asp:Label></div>
        <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
            ValidationGroup="valpetty" OnClick="btnsaveall_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="valpetty" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
            Text="*" ValidationGroup="valpetty" ControlToValidate="txtvdate"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
            ErrorMessage="Voucher Date must be dd-MM-yyyy" ValidationGroup="valpetty" ForeColor="Red"
            ControlToValidate="txtvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter petty cash name."
            Text="*" ValidationGroup="valpetty" ControlToValidate="txtpettycashname"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select account name."
            Text="*" ValidationGroup="valgvpetty" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter client code."
            Text="*" ValidationGroup="valgvpetty" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>--%>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter amount."
            Text="*" ValidationGroup="valgvpetty" ControlToValidate="txtamount"></asp:RequiredFieldValidator>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
