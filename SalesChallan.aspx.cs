﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text;

public partial class SalesChallan : System.Web.UI.Page
{
    ForSalesChallan fscclass = new ForSalesChallan();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForCommInvoice fciclass = new ForCommInvoice();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "update")
            {
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                fillchallantypedrop();
                filleditdata();
                //hideimage();
            }
            else if (Request["mode"].ToString() == "directscci")
            {
                fillacnamedrop();
                fillitemnamedrop();
                getscno();
                fillstatusdrop();
                fillchallantypedrop();
                txtscdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemssach"] = CreateTemplate();
                Page.SetFocus(drpchallantype);
                filleditdatafordirectci();
                //hideimage();
            }
            else if (Request["mode"].ToString() == "directpisc")
            {
                fillacnamedrop();
                fillitemnamedrop();
                getscno();
                fillstatusdrop();
                fillchallantypedrop();
                txtscdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemssach"] = CreateTemplate();
                Page.SetFocus(drpchallantype);
                filleditdatafordirectpisc();
                //hideimage();
            }
            else
            {
                fillacnamedrop();
                fillitemnamedrop();
                getscno();
                fillstatusdrop();
                fillchallantypedrop();
                txtscdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemssach"] = CreateTemplate();
                Page.SetFocus(drpchallantype);
            }
            //hideimage();

        }
    }

    public void filleditdatafordirectci()
    {
        txtsalesorder.Text = "0";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.invoiceno = Convert.ToInt64(Request["invno"].ToString());
        Session["dtpitemssach"] = CreateTemplate();
        DataTable dtdata = new DataTable();
        dtdata = fciclass.selectallciitemdatafordirectsc(li);
        if (dtdata.Rows.Count > 0)
        {
            DataTable dtdt = new DataTable();
            dtdt = fciclass.selectallcimasterdatafordirectsc(li);
            txtsono.Text = dtdt.Rows[0]["sono"].ToString();
            drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
            txtccode.Text = dtdt.Rows[0]["ccode"].ToString();
            txtsalesorder.Text = dtdt.Rows[0]["pono"].ToString();
            txtsodate.Text = dtdt.Rows[0]["podate"].ToString();
            DataTable dt = (DataTable)Session["dtpitemssach"];
            for (int d = 0; d < dtdata.Rows.Count; d++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[d]["vid"].ToString();
                dr["vno"] = "0";
                dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                dr["stockqty"] = "1";
                dr["qtyremain"] = "0";
                dr["qtyused"] = "0";
                if (drpchallantype.SelectedItem.Text == "RM")
                {
                    dr["rate"] = "0";
                }
                else
                {
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                }
                dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                dr["basicamount"] = dtdata.Rows[d]["basicamt"].ToString();
                dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                dr["vid"] = dtdata.Rows[d]["vid"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemssach"] = dt;
            this.bindgrid();
            //this.bindgrid1();
        }
        else
        {
            gvscitemlist.Visible = false;
            gvsoitemlistselection.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
        }
    }

    public void filleditdatafordirectpisc()
    {
        txtsalesorder.Text = "0";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.invoiceno = Convert.ToInt64(Request["invno"].ToString());
        Session["dtpitemssach"] = CreateTemplate();
        string id = Request["id"].ToString();
        li.remarks = id.TrimEnd(',');
        DataTable dtdata = new DataTable();
        dtdata = fciclass.selectallciitemdatafordirectpisc(li);
        if (dtdata.Rows.Count > 0)
        {
            DataTable dtdt = new DataTable();
            dtdt = fciclass.selectallcimasterdatafordirectpisc(li);
            txtsono.Text = dtdt.Rows[0]["sono"].ToString();
            drpacname.SelectedValue = dtdt.Rows[0]["acname"].ToString();
            txtccode.Text = dtdt.Rows[0]["ccode"].ToString();
            txtsalesorder.Text = dtdt.Rows[0]["pono"].ToString();
            txtsodate.Text = dtdt.Rows[0]["podate"].ToString();
            DataTable dt = (DataTable)Session["dtpitemssach"];
            for (int d = 0; d < dtdata.Rows.Count; d++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[d]["vid"].ToString();
                dr["vno"] = "0";
                dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                dr["stockqty"] = "1";
                dr["qtyremain"] = "0";
                dr["qtyused"] = "0";
                if (drpchallantype.SelectedItem.Text == "RM")
                {
                    dr["rate"] = "0";
                }
                else
                {
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                }
                dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                dr["basicamount"] = dtdata.Rows[d]["basicamt"].ToString();
                dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                dr["vid"] = dtdata.Rows[d]["vid"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemssach"] = dt;
            this.bindgrid();
            //this.bindgrid1();
        }
        else
        {
            gvscitemlist.Visible = false;
            gvsoitemlistselection.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvscitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvscitemlist.Rows[c].FindControl("imgbtnselect11");
            if (btnsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillchallantypedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallchallantype(li);
        if (dtdata.Rows.Count > 0)
        {
            drpchallantype.Items.Clear();
            drpchallantype.DataSource = dtdata;
            drpchallantype.DataTextField = "name";
            drpchallantype.DataBind();
            drpchallantype.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpchallantype.Items.Clear();
            drpchallantype.Items.Insert(0, "--SELECT--");
        }
    }

    public void getscno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fscclass.selectunusedsaleschallanno(li);
        dtvno = fscclass.selectunusedsaleschallannolast(li);
        if (dtvno.Rows.Count > 0)
        {
            txtchallanno.Text = (Convert.ToInt64(dtvno.Rows[0]["scno"].ToString()) + 1).ToString();
        }
        else
        {
            txtchallanno.Text = "";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("vno", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("stockqty", typeof(double));
        dtpitems.Columns.Add("qtyremain", typeof(double));
        dtpitems.Columns.Add("qtyused", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("eunit", typeof(string));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        dtpitems.Columns.Add("remarks", typeof(string));
        return dtpitems;
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.strscno = Request["strscno"].ToString();
        DataTable dtdata = new DataTable();
        dtdata = fscclass.selectallsaleschallandatafromscnostrscno(li);
        if (dtdata.Rows.Count > 0)
        {
            ViewState["scno"] = dtdata.Rows[0]["scno"].ToString();
            txtchallanno.Text = dtdata.Rows[0]["scno"].ToString();
            //string qc = dtdata.Rows[0]["scno"].ToString();
            //if (qc.Length == 1)
            //{
            //    txtchallanno.Text = "00000" + qc;
            //}
            //else if (qc.Length == 2)
            //{
            //    txtchallanno.Text = "0000" + qc;
            //}
            //else if (qc.Length == 3)
            //{
            //    txtchallanno.Text = "000" + qc;
            //}
            //else if (qc.Length == 4)
            //{
            //    txtchallanno.Text = "00" + qc;
            //}
            //else if (qc.Length == 5)
            //{
            //    txtchallanno.Text = "0" + qc;
            //}
            txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
            txtsono.Text = dtdata.Rows[0]["sono1"].ToString();
            if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
            {
                txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtdespatchedby.Text = dtdata.Rows[0]["despatchedby"].ToString();
            txtfreightrs.Text = dtdata.Rows[0]["freightrs"].ToString();
            txtsalesorder.Text = dtdata.Rows[0]["sono"].ToString();
            drpstatus.SelectedItem.Text = dtdata.Rows[0]["status"].ToString();
            drpchallantype.SelectedItem.Text = dtdata.Rows[0]["ctype"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            if (dtdata.Rows[0]["issigned"].ToString() == "Yes")
            {
                chksigned.Checked = true;
            }
            txtsignremark.Text = dtdata.Rows[0]["signremarks"].ToString();
            ViewState["ctype"] = dtdata.Rows[0]["ctype"].ToString();
            DataTable dtpcitems = new DataTable();
            dtpcitems = fscclass.selectallscitemsfromscnostring(li);
            if (dtpcitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvscitemlist.Visible = true;
                gvscitemlist.DataSource = dtpcitems;
                gvscitemlist.DataBind();
                lblcount.Text = dtpcitems.Rows.Count.ToString();
            }
            else
            {
                gvscitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Challan Items Found.";
                lblcount.Text = "0";
            }

            btnsave.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsono(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsono(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtccode.Text.Trim() != string.Empty)
        {
            string cc = txtccode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtccode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtccode.Text = string.Empty;
        }
        Page.SetFocus(txtsalesorder);
    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtstockqty.Text = "0";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txtbasicamt.Text = Math.Round((Convert.ToDouble(txtbillqty.Text) * Convert.ToDouble(txtrate.Text)), 2).ToString();
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
        }
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemssach"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvscitemlist.Visible = true;
            lblempty.Visible = false;
            gvscitemlist.DataSource = dtsession;
            gvscitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvscitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    public void bindgrid1()
    {
        DataTable dtsession = Session["dtpitemssach"] as DataTable;
        gvsoitemlistselection.Visible = true;
        chkall.Visible = true;
        gvsoitemlistselection.DataSource = dtsession;
        gvsoitemlistselection.DataBind();
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemssach"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["vno"] = txtsono.Text;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtbillqty.Text;
            dr["stockqty"] = txtstockqty.Text;
            dr["qtyremain"] = txtbillqty.Text;
            dr["qtyused"] = 0;
            if (drpchallantype.SelectedItem.Text == "RM")
            {
                dr["rate"] = "0";
            }
            else
            {
                dr["rate"] = txtrate.Text;
            }
            dr["unit"] = txtunit.Text;
            dr["basicamount"] = txtbasicamt.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dr["remarks"] = "";
            dt.Rows.Add(dr);
            Session["dtpitemssach"] = dt;
            this.bindgrid();
        }
        else
        {
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.vnono = Convert.ToInt64(txtsono.Text);
            li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.qty = Convert.ToDouble(txtbillqty.Text);
            li.stockqty = Convert.ToDouble(txtstockqty.Text);
            li.qtyremain1 = Convert.ToDouble(txtbillqty.Text);
            li.qtyused1 = 0;
            if (drpchallantype.SelectedItem.Text == "RM")
            {
                li.rate = 0;
            }
            else
            {
                li.rate = Convert.ToDouble(txtrate.Text);
            }
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.unit = txtunit.Text;
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.remarks = "";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.ccode = Convert.ToInt64(txtccode.Text);
            li.strscno = drpchallantype.SelectedItem.Text + li.scno.ToString();
            fscclass.insertscitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strscno + " Sales Challan Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallscitemsfromscnostring(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvscitemlist.Visible = true;
                gvscitemlist.DataSource = dtdata;
                gvscitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvscitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
        }
        fillitemnamedrop();
        txtbillqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txtunit.Text = string.Empty;
        txtdescription1.Text = string.Empty;
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        hideimage();
        Page.SetFocus(drpitemname);
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.scdate <= yyyear1 && li.scdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (btnsave.Text == "Save")
        {
            //for (int p = 0; p < gvscitemlist.Rows.Count; p++)
            //{
            //    TextBox txtgvstockqty = (TextBox)gvscitemlist.Rows[p].FindControl("txtgvstockqty");
            //    if (Convert.ToDouble(txtgvstockqty.Text) <= 0 || txtgvstockqty.Text.Trim() == string.Empty)
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Stock Qty must be greater than 0(Zero).');", true);
            //        return;
            //    }
            //}
        }
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.scno = Convert.ToInt64(txtchallanno.Text);
        li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.acname = drpacname.SelectedItem.Text;
        li.ccode = Convert.ToInt64(txtccode.Text);
        //if (txtsono.Text.Trim() != string.Empty)
        //{
        li.strsono = txtsalesorder.Text;
        //}
        //else
        //{
        //    li.sono = 0;
        //}
        if (txtsodate.Text.Trim() != string.Empty)
        {
            li.sodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.sodate = sqldatenull;
        }
        li.lrno = txtlrno.Text;
        if (txtlrdate.Text.Trim() != string.Empty)
        {
            li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.lrdate = sqldatenull;
        }
        li.dispatchedby = txtdespatchedby.Text;
        if (txtfreightrs.Text.Trim() != string.Empty)
        {
            li.freight = Convert.ToDouble(txtfreightrs.Text);
        }
        else
        {
            li.freight = 0;
        }
        if (txtsono.Text.Trim() != string.Empty)
        {
            li.sono1 = Convert.ToInt64(txtsono.Text);
        }
        else
        {
            li.sono1 = 0;
        }
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.ctype = drpchallantype.SelectedItem.Text;
        if (chksigned.Checked == true)
        {
            li.issigned = "Yes";
        }
        else
        {
            li.issigned = "No";
        }
        li.signremark = txtsignremark.Text;
        li.strscno = li.ctype + (Convert.ToInt64(txtchallanno.Text)).ToString();
        if (btnsave.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            //dtcheck = fscclass.selectallsaleschallandatafromscno(li);
            dtcheck = fscclass.selectallsaleschallandatafromscnoag(li);
            if (dtcheck.Rows.Count == 0)
            {
                fscclass.insertscmasterdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strscno + " Sales Challan Inserted.";
                faclass.insertactivity(li);
                //li.ctype = drpchallantype.SelectedItem.Text;

                //fscclass.updatechallantype(li);
            }
            else
            {
                li.scno = Convert.ToInt64(txtchallanno.Text);
                fscclass.updateisused(li);
                getscno();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Challan No. already exist.Try Again.');", true);
                return;
            }
            li.scno = Convert.ToInt64(txtchallanno.Text);
            //li.scno = SessionMgt.voucherno;
            for (int c = 0; c < gvscitemlist.Rows.Count; c++)
            {
                Label lblid = (Label)gvscitemlist.Rows[c].FindControl("lblid");
                Label lblvno = (Label)gvscitemlist.Rows[c].FindControl("lblvno");
                Label lblitemname = (Label)gvscitemlist.Rows[c].FindControl("lblitemname");
                TextBox txtgvstockqty = (TextBox)gvscitemlist.Rows[c].FindControl("txtgvstockqty");
                TextBox lblqty = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridqty");
                Label lblqtyremain = (Label)gvscitemlist.Rows[c].FindControl("lblqtyremain");
                Label lblqtyused = (Label)gvscitemlist.Rows[c].FindControl("lblqtyused");
                TextBox lblrate = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridrate");
                TextBox txtgridunit = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridunit");
                TextBox txtgrideunit = (TextBox)gvscitemlist.Rows[c].FindControl("txtgrideunit");
                TextBox lblamount = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridamount");

                Label lbltotamount = (Label)gvscitemlist.Rows[c].FindControl("lbltotamount");
                Label lblvatp = (Label)gvscitemlist.Rows[c].FindControl("lblvatp");
                Label lbladdvatp = (Label)gvscitemlist.Rows[c].FindControl("lbladdvatp");
                TextBox txtgridcstp = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridcstp");
                Label lblcstamt = (Label)gvscitemlist.Rows[c].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvscitemlist.Rows[c].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvscitemlist.Rows[c].FindControl("lbladdvatamt");
                TextBox txtgridtaxtype = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridtaxtype");



                TextBox lbldesc1 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc3");
                TextBox lblremarks = (TextBox)gvscitemlist.Rows[c].FindControl("txtremarks");
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                //if (li.qty > 0)
                //{
                li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
                li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
                li.qtyremain1 = li.qty;
                li.qtyused1 = 0;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.unit = txtgridunit.Text;
                li.unitewaybill = txtgrideunit.Text;
                li.basicamount = Convert.ToDouble(lblamount.Text);


                li.vattype = txtgridtaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(txtgridcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lbltotamount.Text);


                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                li.remarks = lblremarks.Text;

                fscclass.insertscitemsdata(li);
                li.id = Convert.ToInt64(lblid.Text);
                li.vnono1 = Convert.ToInt64(lblvno.Text);
                fscclass.updateremainqtyinsalesorder(li);
                li.strscno = drpchallantype.SelectedItem.Text + txtchallanno.Text;
                fscclass.updatestrscnoinsalesorderitems(li);
                //}

            }
            fscclass.updateisused(li);
        }
        else
        {
            if (ViewState["scno"].ToString().Trim() != txtchallanno.Text.Trim() || ViewState["ctype"].ToString().Trim() != drpchallantype.SelectedItem.Text)
            {
                li.scno1 = Convert.ToInt64(txtchallanno.Text);
                li.scno = Convert.ToInt64(ViewState["scno"].ToString());
                li.ctype1 = drpchallantype.SelectedItem.Text;
                li.ctype = ViewState["ctype"].ToString();
                li.strscno = ViewState["ctype"].ToString() + li.scno.ToString();
                li.strscno1 = li.ctype1 + li.scno1.ToString();
                fscclass.updatescmasterdataupdatescstring(li);
                fscclass.updatescitemsdataupdatescstring(li);
                fscclass.deletescmasterdatastring(li);
                fscclass.deletescitemsdatastring(li);
            }
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            fscclass.updatescmasterdatastring(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = drpchallantype.SelectedItem.Text + txtchallanno.Text;
            fscclass.updatescitemsdatedataupdatescstring(li);

            //Update all item together
            for (int y = 0; y < gvscitemlist.Rows.Count; y++)
            {
                Label lblqty = (Label)gvscitemlist.Rows[y].FindControl("lblqty");
                TextBox txtgvqty1 = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridqty");
                Label lblid = (Label)gvscitemlist.Rows[y].FindControl("lblid");
                Label lblvid = (Label)gvscitemlist.Rows[y].FindControl("lblvid");
                //Label lblvno = (Label)gvscitemlist.Rows[c].FindControl("lblvno");
                Label lblitemname = (Label)gvscitemlist.Rows[y].FindControl("lblitemname");
                //Label lblqtyremain = (Label)gvscitemlist.Rows[c].FindControl("lblqtyremain");
                //Label lblqtyused = (Label)gvscitemlist.Rows[c].FindControl("lblqtyused");
                TextBox lblrate = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridrate");
                TextBox txtgridunit = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridunit");
                TextBox txtgrideunit = (TextBox)gvscitemlist.Rows[y].FindControl("txtgrideunit");
                TextBox lblamount = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridamount");

                Label lbltotamount = (Label)gvscitemlist.Rows[y].FindControl("lbltotamount");
                Label lblvatp = (Label)gvscitemlist.Rows[y].FindControl("lblvatp");
                Label lbladdvatp = (Label)gvscitemlist.Rows[y].FindControl("lbladdvatp");
                TextBox txtgridcstp = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridcstp");
                Label lblcstamt = (Label)gvscitemlist.Rows[y].FindControl("lblcstamt");
                Label lblvatamt = (Label)gvscitemlist.Rows[y].FindControl("lblvatamt");
                Label lbladdvatamt = (Label)gvscitemlist.Rows[y].FindControl("lbladdvatamt");
                TextBox txtgridtaxtype = (TextBox)gvscitemlist.Rows[y].FindControl("txtgridtaxtype");

                TextBox lbldesc1 = (TextBox)gvscitemlist.Rows[y].FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)gvscitemlist.Rows[y].FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)gvscitemlist.Rows[y].FindControl("txtdesc3");
                TextBox lblremarks = (TextBox)gvscitemlist.Rows[y].FindControl("txtremarks");
                TextBox txtgvstockqty = (TextBox)gvscitemlist.Rows[y].FindControl("txtgvstockqty");
                //TextBox txtgvreturnqty = (TextBox)gvscitemlist.Rows[y].FindControl("txtgvreturnqty");
                //TextBox txtgvretremarks = (TextBox)gvscitemlist.Rows[y].FindControl("txtgvretremarks");
                li.qty = Convert.ToDouble(lblqty.Text);
                li.vid = Convert.ToInt64(lblvid.Text);
                li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                if (txtgvstockqty.Text.Trim() != string.Empty)
                {
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                }
                else
                {
                    li.stockqty = 0;
                }
                li.qty = Convert.ToDouble(txtgvqty1.Text);
                li.qtyused = 0;
                li.qtyremain = li.qty;
                //li.qtyremain1 = li.qty;
                //li.qtyused1 = 0;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.unit = txtgridunit.Text;
                li.unitewaybill = txtgrideunit.Text;
                li.basicamount = Convert.ToDouble(lblamount.Text);


                li.vattype = txtgridtaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(txtgridcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lbltotamount.Text);


                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.remarks = lblremarks.Text;
                //if (txtgvreturnqty.Text.Trim() != string.Empty)
                //{
                //    li.rfs = Convert.ToDouble(txtgvreturnqty.Text);
                //}
                //else
                //{
                li.rfs = 0;
                //}
                li.ccode = Convert.ToInt64(txtccode.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.retremarks = "";
                //li.vnono = Convert.ToInt64(lblvno.Text);
                fscclass.updatescitemsdata(li);
            }
            ///

            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strscno1 + " Sales Challan Updated.";
            faclass.insertactivity(li);
            //fscclass.updatescitemsdataupdatescstring1(li);
            //for (int c = 0; c < gvscitemlist.Rows.Count; c++)
            //{
            //    Label lblid = (Label)gvscitemlist.Rows[c].FindControl("lblid");
            //    //Label lblvno = (Label)gvscitemlist.Rows[c].FindControl("lblvno");
            //    Label lblitemname = (Label)gvscitemlist.Rows[c].FindControl("lblitemname");
            //    TextBox lblqty = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridqty");
            //    //Label lblqtyremain = (Label)gvscitemlist.Rows[c].FindControl("lblqtyremain");
            //    //Label lblqtyused = (Label)gvscitemlist.Rows[c].FindControl("lblqtyused");
            //    TextBox lblrate = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridrate");
            //    Label lblunit = (Label)gvscitemlist.Rows[c].FindControl("lblunit");
            //    TextBox lblamount = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridamount");
            //    TextBox lbldesc1 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc1");
            //    TextBox lbldesc2 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc2");
            //    TextBox lbldesc3 = (TextBox)gvscitemlist.Rows[c].FindControl("txtdesc3");
            //    li.id = Convert.ToInt64(lblid.Text);
            //    li.itemname = lblitemname.Text;
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    //if (li.qty > 0)
            //    //{
            //    //li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
            //    //li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
            //    //li.qtyremain1 = li.qty;
            //    //li.qtyused1 = 0;
            //    li.rate = Convert.ToDouble(lblrate.Text);
            //    li.unit = lblunit.Text;
            //    li.basicamount = Convert.ToDouble(lblamount.Text);
            //    li.descr1 = lbldesc1.Text;
            //    li.descr2 = lbldesc2.Text;
            //    li.descr3 = lbldesc3.Text;
            //    li.ccode = Convert.ToInt64(txtccode.Text);
            //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            //    li.uname = Request.Cookies["ForLogin"]["username"];
            //    li.udate = System.DateTime.Now;
            //    //li.vnono = Convert.ToInt64(lblvno.Text);
            //    fscclass.updatescitemsdata(li);
            //    //li.id = Convert.ToInt64(lblid.Text);
            //    //li.vnono1 = Convert.ToInt64(lblvno.Text);
            //    //fscclass.updateremainqtyinsalesorder(li);
            //    //}

            //}
        }
        li.strscno = drpchallantype.SelectedItem.Text + txtchallanno.Text;
        li.sono = Convert.ToInt64(txtsono.Text);
        fscclass.updatestrscnoinsalesorder(li);
        fscclass.updatestrscnoinsalesordera(li);
        if (drpeway.SelectedItem.Text == "YES")
        {
            if (btnsave.Text != "Update")
            {
                txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                txtpincode.Text = "380009";
                txtplace.Text = "NAVRANGPURA";
                drpfromstate.SelectedValue = "24";
                drpactualfromstate.SelectedValue = "24";
                txtgstinuin.Text = "24AADCA6797L1Z6";

                txtconsignee.Text = drpacname.SelectedItem.Text;
                li.acname = txtconsignee.Text;
                DataTable dts = fscclass.selectallacdatefromacname(li);
                if (dts.Rows.Count > 0)
                {
                    txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                    txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                    txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                    //txttoplace.Text="";
                    txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                    if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                    {
                        drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                        drpactualtostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                    }
                    //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                }
            }
            else
            {
                DataTable dtdata = fscclass.selectewaybilldatafromstrscno(li);
                if (dtdata.Rows.Count > 0)
                {
                    if (dtdata.Rows[0]["distanceinkm"].ToString() != "")
                    {
                        txtewaybillno.Text = dtdata.Rows[0]["ewaybillno"].ToString();
                        txtdate1.Text = dtdata.Rows[0]["date1"].ToString();
                        txtconsolidatedewaybillno.Text = dtdata.Rows[0]["cewaybillno"].ToString();
                        txtdate2.Text = dtdata.Rows[0]["date2"].ToString();
                        drpsupplytype.SelectedValue = dtdata.Rows[0]["supplytype"].ToString();
                        drpsubtype.SelectedValue = dtdata.Rows[0]["subtype"].ToString();
                        drpdocumenttype.SelectedValue = dtdata.Rows[0]["doctype"].ToString();
                        drpmode.SelectedValue = dtdata.Rows[0]["tramode"].ToString();
                        txtdistance.Text = dtdata.Rows[0]["distanceinkm"].ToString();
                        txttransportername.Text = dtdata.Rows[0]["transportername"].ToString();
                        txtvehiclenumber.Text = dtdata.Rows[0]["vehicleno"].ToString();
                        drpvehicletype.SelectedValue = dtdata.Rows[0]["vehicletype"].ToString();
                        txtdoclandingrrairwayno.Text = dtdata.Rows[0]["trano"].ToString();
                        txtdate.Text = dtdata.Rows[0]["date3"].ToString();
                        txttransporterid.Text = dtdata.Rows[0]["traid"].ToString();
                        txtconsignor.Text = dtdata.Rows[0]["consignor"].ToString();
                        txtaddress1.Text = dtdata.Rows[0]["consignoradd1"].ToString();
                        txtaddress2.Text = dtdata.Rows[0]["consignoradd2"].ToString();
                        txtpincode.Text = dtdata.Rows[0]["consignorpincode"].ToString();
                        txtplace.Text = dtdata.Rows[0]["consignorplace"].ToString();
                        drpfromstate.SelectedValue = dtdata.Rows[0]["consignorstate"].ToString();
                        drpactualfromstate.SelectedValue = dtdata.Rows[0]["actualfromstatecode"].ToString();
                        txtgstinuin.Text = dtdata.Rows[0]["consignorgst"].ToString();

                        txtconsignee.Text = dtdata.Rows[0]["consignee"].ToString();
                        txtconsigneeaddress1.Text = dtdata.Rows[0]["consigneeadd1"].ToString();
                        txtconsigneeaddress2.Text = dtdata.Rows[0]["consigneeadd2"].ToString();
                        txtconsigneepincode.Text = dtdata.Rows[0]["consigneepincode"].ToString();
                        txttoplace.Text = dtdata.Rows[0]["consigneeplace"].ToString();
                        txtconsigneegstinuin.Text = dtdata.Rows[0]["consigneegst"].ToString();
                        drptostate.SelectedValue = dtdata.Rows[0]["consigneestate"].ToString();
                        drpactualtostate.SelectedValue = dtdata.Rows[0]["actualtostatecode"].ToString();
                    }
                    else
                    {
                        txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                        txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                        txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                        txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                        txtpincode.Text = "380009";
                        txtplace.Text = "NAVRANGPURA";
                        drpfromstate.SelectedValue = "24";
                        drpactualfromstate.SelectedValue = "24";
                        txtgstinuin.Text = "24AADCA6797L1Z6";

                        txtconsignee.Text = drpacname.SelectedItem.Text;
                        li.acname = txtconsignee.Text;
                        DataTable dts = fscclass.selectallacdatefromacname(li);
                        if (dts.Rows.Count > 0)
                        {
                            txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                            txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                            txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                            //txttoplace.Text="";
                            txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                            if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                            {
                                drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                                drpactualtostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                            }
                            //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                        }
                    }
                }
                else
                {
                    txtdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtdate1.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtdate2.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    txtconsignor.Text = "AIRMAX (GUJARAT) PVT. LTD.";
                    txtaddress1.Text = "11-13 ADARSH COMPLEX,1st FLOOR,";
                    txtaddress2.Text = "ADARSH SOC.,SWASTIK CHAR RASTA,";
                    txtpincode.Text = "380009";
                    txtplace.Text = "NAVRANGPURA";
                    drpfromstate.SelectedValue = "24";
                    drpactualfromstate.SelectedValue = "24";
                    txtgstinuin.Text = "24AADCA6797L1Z6";

                    txtconsignee.Text = drpacname.SelectedItem.Text;
                    li.acname = txtconsignee.Text;
                    DataTable dts = fscclass.selectallacdatefromacname(li);
                    if (dts.Rows.Count > 0)
                    {
                        txtconsigneeaddress1.Text = dts.Rows[0]["add1"].ToString();
                        txtconsigneeaddress2.Text = dts.Rows[0]["add2"].ToString();
                        txtconsigneepincode.Text = dts.Rows[0]["pincode"].ToString();
                        //txttoplace.Text="";
                        txtconsigneegstinuin.Text = dts.Rows[0]["gstno"].ToString();
                        if (txtconsigneegstinuin.Text.Trim() != string.Empty)
                        {
                            drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                            drpactualtostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
                        }
                        //drptostate.SelectedValue = dts.Rows[0]["gstno"].ToString();
                    }
                }
            }
            ModalPopupExtender1.Show();
        }
        else
        {
            Response.Redirect("~/SalesChallanList.aspx?pagename=SalesChallanList");
        }
    }
    protected void gvscitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemssach"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemssach"] = dt;
            this.bindgrid();
        }
        else
        {
            if (gvscitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblvid");
                Label lblitemname = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblitemname");
                Label lblqty = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblqty");
                li.id = Convert.ToInt64(lblid.Text);
                DataTable dtcheck1 = new DataTable();
                dtcheck1 = fscclass.checkbalqty(li);
                if (dtcheck1.Rows.Count == 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data is locked because stock adjusted for this item.');", true);
                    return;
                }
                DataTable dtcheck = new DataTable();
                dtcheck = fscclass.selectbillinvoicedatafromscid(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Used in Sales Invoice No. " + dtcheck.Rows[0]["strsino"].ToString() + " So You Cant delete this Item.First delete that item from invoice then try to delete this item.');", true);
                    return;
                }
                else
                {
                    //Label lblqtyremain = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblqtyremain");
                    //Label lblqtyused = (Label)gvscitemlist.Rows[e.RowIndex].FindControl("lblqtyused");
                    li.sono = Convert.ToInt64(txtsono.Text);
                    li.vid = Convert.ToInt64(lblvid.Text);
                    if (li.vid != 0)
                    {
                        DataTable dtqty = new DataTable();
                        dtqty = fscclass.selectqtyremainusedfromsono(li);
                        li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                        li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                        li.itemname = lblitemname.Text;
                        fscclass.updateqtyduringdelete(li);
                        li.vid = Convert.ToInt64(lblvid.Text);
                        li.strscno = "";
                        fscclass.updatestrscnoinsalesorderitems1(li);
                    }
                    li.id = Convert.ToInt64(lblid.Text);
                    fscclass.deletescitemsdatafromid(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Sales Challan Item Deleted.";
                    faclass.insertactivity(li);
                    //li.istype = "CR";
                    //flclass.deleteledgerdata(li);
                    li.scno = Convert.ToInt64(txtchallanno.Text);
                    li.strscno = drpchallantype.SelectedItem.Text + li.scno.ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fscclass.selectallscitemsfromscnostring(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvscitemlist.Visible = true;
                        gvscitemlist.DataSource = dtdata;
                        gvscitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvscitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        hideimage();
    }

    public void count1()
    {
        if (txtbillqty.Text.Trim() == string.Empty)
        {
            txtbillqty.Text = "0";
        }
        if (txtrate.Text.Trim() == string.Empty)
        {
            txtrate.Text = "0";
        }
        double qty = 0;
        double rate = 0;
        double amount = 0;
        qty = Convert.ToDouble(txtbillqty.Text);
        rate = Convert.ToDouble(txtrate.Text);
        //amount = Convert.ToDouble();
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
    }

    protected void txtbillqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtstockqty);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemssach2"];
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double amount = 0;
        for (int c = 0; c < gvscitemlist.Rows.Count; c++)
        {
            TextBox txtgvqty1 = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridvatp = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridaddtaxp = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridvatamt = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridcstp = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridcstamt = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            Label lblgridaddtaxamt = (Label)gvscitemlist.Rows[c].FindControl("txtgridamount");
            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            baseamt = baseamt + Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2);
            amount = amount + Convert.ToDouble(txtgvamount1.Text);
            vatamt = vatamt + Convert.ToDouble(dtsoitem.Rows[c]["vatamt"].ToString());
            addvatamt = addvatamt + Convert.ToDouble(dtsoitem.Rows[c]["addtaxamt"].ToString());
            cstp = cstp + Convert.ToDouble(dtsoitem.Rows[c]["cstp"].ToString());
            cstamt = cstamt + Convert.ToDouble(dtsoitem.Rows[c]["cstamt"].ToString());
            vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["vatp"].ToString()) + Convert.ToDouble(dtsoitem.Rows[c]["addtaxp"].ToString());
        }

    }

    protected void txtsono_TextChanged(object sender, EventArgs e)
    {
        if (txtsono.Text.Trim() != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsono.Text);
            DataTable dtdate = new DataTable();
            dtdate = fscclass.selectpodatefrompono(li);
            DataTable dtc = new DataTable();
            dtc = fscclass.selectallsalesordermasterdatafromsono(li);
            if (dtc.Rows.Count > 0)
            {
                drpacname.SelectedValue = dtc.Rows[0]["acname"].ToString();
                txtccode.Text = dtc.Rows[0]["ccode"].ToString();
                txtsodate.Text = dtdate.Rows[0]["cdate"].ToString();
            }
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                for (int d = 0; d < dtdata.Rows.Count; d++)
                {
                    DataTable dt = (DataTable)Session["dtpitemssach"];
                    DataRow dr = dt.NewRow();
                    dr["id"] = dtdata.Rows[d]["id"].ToString();
                    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                    dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                    dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                    dt.Rows.Add(dr);
                    Session["dtpitemssach"] = dt;
                    this.bindgrid();
                }
            }
            else
            {
                gvscitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
        }
        else
        {
            gvscitemlist.Visible = false;
            lblempty.Visible = false;
        }
    }
    protected void txtgridqty_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            //GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            //TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            //TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            //TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            //TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            ////if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
            ////{
            //if (txtgvqty1.Text.Trim() != string.Empty)
            //{
            //    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            //}
            ////}
            ////else
            ////{
            ////    txtgvqty1.Text = lblqtyremain.Text;
            ////    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
            ////    return;
            ////}
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            }
            Page.SetFocus(txtstockqty);


            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
            TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
            Label lblvatp = (Label)currentRow.FindControl("lblvatp");
            Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
            Label lblcstp = (Label)currentRow.FindControl("lblcstp");
            Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
            Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
            Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
            Label lblamount = (Label)currentRow.FindControl("lbltotamount");
            //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
            }
            Page.SetFocus(txtstockqty);
            double vatp = 0;
            double addvatp = 0;
            double cstp = 0;
            double vatamt = 0;
            double addvatamt = 0;
            double cstamt = 0;
            double baseamt = Convert.ToDouble(txtgvamount1.Text);
            if (lbltaxtype.Text != string.Empty)
            {
                var cc = lbltaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = lbltaxtype.Text;
                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    //DataTable dtdtax = new DataTable();
                    //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                    //if (dtdtax.Rows.Count > 0)
                    //{
                    //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                    //}
                    //else
                    //{
                    txtgridcstp.Text = "0";
                    //}
                }
                else
                {
                    vatp = 0;
                    addvatp = 0;
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    txtgridcstp.Text = "0";
                    lbltaxtype.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                //lbltaxtype.Text = "0";
            }
            if (lblvatp.Text != string.Empty)
            {
                vatp = Convert.ToDouble(lblvatp.Text);
                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            }
            else
            {
                lblvatamt.Text = "0";
                lblvatp.Text = "0";
            }
            if (lbladdvatp.Text != string.Empty)
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
            }
            else
            {
                lbladdvatp.Text = "0";
                lbladdvatamt.Text = "0";
            }
            if (txtgridcstp.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgridcstp.Text);
                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgridcstp.Text = "0";
                lblcstp.Text = "0";
                lblcstamt.Text = "0";
            }
            vatamt = Convert.ToDouble(lblvatamt.Text);
            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
            cstamt = Convert.ToDouble(lblcstamt.Text);
            lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
            Page.SetFocus(txtgvstockqty);
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            li.vid = Convert.ToInt64(lblid.Text);
            DataTable dtremain = new DataTable();
            dtremain = fscclass.selectqtyremainusedfromsono(li);
            if (dtremain.Rows.Count > 0)
            {
                li.qty = Convert.ToDouble(lblqty.Text);
                double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                if (Convert.ToDouble(txtgvqty1.Text) > rqty)
                {
                    //    li.id = Convert.ToInt64(lblid.Text);
                    //    li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //    li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //    fscclass.updateremainqtyinsalesorder(li);
                    //}
                    //else
                    //{
                    txtgvqty1.Text = lblqty.Text;
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                    return;
                }
                else
                {
                    if (txtgvqty1.Text.Trim() != string.Empty)
                    {
                        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                    }
                }
            }
            Page.SetFocus(txtgvstockqty);
        }
    }

    protected void txtgridrate_TextChanged(object sender, EventArgs e)
    {
        //GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        //TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        //TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        //if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        //{
        //    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
        //}
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        {
            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
        }



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lbltotamount");
        //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
        if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        {
            txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        }
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
            //lbltaxtype.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        Page.SetFocus(txtgvamount1);
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            lblpopupacname.Text = drpacname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtpc = new DataTable();
            dtpc = fscclass.selectallsalesorderforpopup(li);
            if (dtpc.Rows.Count > 0)
            {
                lblemptyso.Visible = false;
                gvsalesorder.Visible = true;
                gvsalesorder.DataSource = dtpc;
                gvsalesorder.DataBind();
            }
            else
            {
                gvsalesorder.Visible = false;
                lblemptyso.Visible = true;
                lblemptyso.Text = "No Sales Order Found.";
            }
            btnselectitem.Visible = false;
            gvsoitemlistselection.Visible = false;
            chkall.Visible = false;
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsalesorder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            Session["dtpitemssach"] = CreateTemplate();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(e.CommandArgument);
            DataTable dtdate = new DataTable();
            dtdate = fscclass.selectpodatefrompono(li);
            //if (txtsono.Text.Trim() == string.Empty)
            //{
            txtsono.Text = e.CommandArgument.ToString();
            txtsalesorder.Text = dtdate.Rows[0]["pono"].ToString();
            txtsodate.Text = dtdate.Rows[0]["cdate"].ToString();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtc = new DataTable();
            dtc = fscclass.selectallsalesordermasterdatafromsono(li);
            if (dtc.Rows.Count > 0)
            {
                //drpacname.SelectedValue = dtc.Rows[0]["acname"].ToString();
                txtccode.Text = dtc.Rows[0]["ccode"].ToString();
            }
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallsalesorderitemdatafromsono(li);
            if (dtdata.Rows.Count > 0)
            {
                //for (int d = 0; d < dtdata.Rows.Count; d++)
                //{
                //    DataTable dt = (DataTable)Session["dtpitems"];
                //    DataRow dr = dt.NewRow();
                //    dr["id"] = dtdata.Rows[d]["id"].ToString();
                //    dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                //    dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                //    dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                //    dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                //    dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                //    dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                //    dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                //    dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                //    dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                //    dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                //    dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                //    dr["vid"] = 0;
                //    dt.Rows.Add(dr);
                //    Session["dtpitems"] = dt;
                //    //this.bindgrid();
                //    this.bindgrid1();                    
                //}
                gvsalesorder.Visible = false;
                //Session["dtpitems"] = null;
                //Session["dtpitems"] = CreateTemplate();
                gvsoitemlistselection.Visible = true;
                lblemptyso.Visible = false;
                gvsoitemlistselection.DataSource = dtdata;
                gvsoitemlistselection.DataBind();
                chkall.Visible = true;
                ModalPopupExtender2.Show();
                lblempty.Visible = false;
                btnselectitem.Visible = true;
            }
            else
            {
                gvscitemlist.Visible = false;
                gvsoitemlistselection.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            ModalPopupExtender2.Show();
            //}
        }
        hideimage();
    }

    protected void gvscitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            DataTable dtcheck = new DataTable();
            dtcheck = fscclass.checkbalqty(li);
            if (dtcheck.Rows.Count == 1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data is locked because stock adjusted for this item.');", true);
                return;
            }
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            li.qty = Convert.ToDouble(lblqty.Text);
            li.vid = Convert.ToInt64(e.CommandArgument);
            //DataTable dtremain = new DataTable();
            //dtremain = fscclass.selectqtyremainusedfromsono(li);
            //if (dtremain.Rows.Count > 0)
            //{
            //    li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
            //    li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
            //    fscclass.updateremainqtyinsalesorder(li);
            //update in scitems
            Label lblid = (Label)currentRow.FindControl("lblid");
            Label lblvid = (Label)currentRow.FindControl("lblvid");
            //Label lblvno = (Label)gvscitemlist.Rows[c].FindControl("lblvno");
            Label lblitemname = (Label)currentRow.FindControl("lblitemname");
            //Label lblqtyremain = (Label)gvscitemlist.Rows[c].FindControl("lblqtyremain");
            //Label lblqtyused = (Label)gvscitemlist.Rows[c].FindControl("lblqtyused");
            TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgridunit = (TextBox)currentRow.FindControl("txtgridunit");
            TextBox lblamount = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
            TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
            TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
            TextBox lblremarks = (TextBox)currentRow.FindControl("txtremarks");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            TextBox txtgvreturnqty = (TextBox)currentRow.FindControl("txtgvreturnqty");
            TextBox txtgvretremarks = (TextBox)currentRow.FindControl("txtgvretremarks");
            li.scdate = Convert.ToDateTime(txtscdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.id = Convert.ToInt64(lblid.Text);
            li.itemname = lblitemname.Text;
            if (txtgvstockqty.Text.Trim() != string.Empty)
            {
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
            }
            else
            {
                li.stockqty = 0;
            }
            li.qty = Convert.ToDouble(txtgvqty1.Text);
            li.qtyused = 0;
            li.qtyremain = li.qty;
            //li.qtyremain1 = li.qty;
            //li.qtyused1 = 0;
            li.rate = Convert.ToDouble(lblrate.Text);
            li.unit = txtgridunit.Text;
            li.basicamount = Convert.ToDouble(lblamount.Text);
            li.descr1 = lbldesc1.Text;
            li.descr2 = lbldesc2.Text;
            li.descr3 = lbldesc3.Text;
            li.remarks = lblremarks.Text;
            if (txtgvreturnqty.Text.Trim() != string.Empty)
            {
                li.rfs = Convert.ToDouble(txtgvreturnqty.Text);
            }
            else
            {
                li.rfs = 0;
            }
            li.ccode = Convert.ToInt64(txtccode.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.retremarks = txtgvretremarks.Text;
            //li.vnono = Convert.ToInt64(lblvno.Text);
            fscclass.updatescitemsdata(li);
            //li.vid = Convert.ToInt64(lblvid.Text);
            //li.strscno = drpchallantype.SelectedItem.Text + txtchallanno.Text;
            //fscclass.updatestrscnoinsalesorderitems1(li);
            //fillgrid
            li.strscno = drpchallantype.SelectedItem.Text + (Convert.ToInt64(txtchallanno.Text)).ToString();
            DataTable dtpcitems = new DataTable();
            dtpcitems = fscclass.selectallscitemsfromscnostring(li);
            if (dtpcitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvscitemlist.Visible = true;
                gvscitemlist.DataSource = dtpcitems;
                gvscitemlist.DataBind();
            }
            else
            {
                gvscitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Challan Items Found.";
            }

            //}
        }
    }

    protected void chkall_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkall = (CheckBox)gvsoitemlistselection.HeaderRow.FindControl("chkall");
        if (chkall.Checked == true)
        {
            for (int c = 0; c < gvsoitemlistselection.Rows.Count; c++)
            {
                CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[c].FindControl("chkselect");
                chkselect.Checked = true;
            }
        }
        else
        {
            for (int c = 0; c < gvsoitemlistselection.Rows.Count; c++)
            {
                CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[c].FindControl("chkselect");
                chkselect.Checked = false;
            }
        }
    }

    protected void btnselectitem_Click(object sender, EventArgs e)
    {
        Session["dtpitemssach"] = CreateTemplate();
        if (chkall.Checked == true)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsono.Text);
            if (txtsono.Text.Trim() != string.Empty)
            {
                DataTable dtdata = new DataTable();
                dtdata = fscclass.selectallsalesorderitemdatafromsono(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataTable dt = (DataTable)Session["dtpitemssach"];
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = "0";
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        if (drpchallantype.SelectedItem.Text == "RM")
                        {
                            dr["rate"] = "0";
                        }
                        else
                        {
                            dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        }

                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        li.itemname = dtdata.Rows[d]["itemname"].ToString();
                        DataTable dteunit = fscclass.selectewayunitfromitemmaster(li);
                        if (dteunit.Rows.Count > 0)
                        {
                            dr["eunit"] = dteunit.Rows[0]["unitewaybill"].ToString();
                        }
                        else
                        {
                            dr["eunit"] = "";
                        }
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();

                        dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
                        dr["taxdesc"] = dtdata.Rows[d]["taxtype"].ToString();
                        dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                        dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                        dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                        dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                        dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
                        dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();

                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        Session["dtpitemssach"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                        gvsalesorder.Visible = false;
                        ModalPopupExtender2.Show();
                    }
                }
                else
                {
                    gvscitemlist.Visible = false;
                    gvsoitemlistselection.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
        }
        else
        {
            string id = "0";
            for (int cc = 0; cc < gvsoitemlistselection.Rows.Count; cc++)
            {
                Label lblid = (Label)gvsoitemlistselection.Rows[cc].FindControl("lblid");
                CheckBox chkselect = (CheckBox)gvsoitemlistselection.Rows[cc].FindControl("chkselect");
                if (chkselect.Checked == true)
                {
                    id = id + "," + lblid.Text;
                }
            }
            li.remarks = id;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.sono = Convert.ToInt64(txtsono.Text);
            if (txtsono.Text.Trim() != string.Empty)
            {
                DataTable dtdata = new DataTable();
                dtdata = fscclass.selectallsalesorderitemdatafromsonousingin(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataTable dt = (DataTable)Session["dtpitemssach"];
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = "0";
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        li.itemname = dtdata.Rows[d]["itemname"].ToString();
                        DataTable dteunit = fscclass.selectewayunitfromitemmaster(li);
                        if (dteunit.Rows.Count > 0)
                        {
                            dr["eunit"] = dteunit.Rows[0]["unitewaybill"].ToString();
                        }
                        else
                        {
                            dr["eunit"] = "";
                        }
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();

                        dr["taxtype"] = dtdata.Rows[d]["taxtype"].ToString();
                        dr["taxdesc"] = dtdata.Rows[d]["taxtype"].ToString();
                        dr["vatp"] = dtdata.Rows[d]["vatp"].ToString();
                        dr["vatamt"] = dtdata.Rows[d]["vatamt"].ToString();
                        dr["addtaxp"] = dtdata.Rows[d]["addtaxp"].ToString();
                        dr["addtaxamt"] = dtdata.Rows[d]["addtaxamt"].ToString();
                        dr["cstp"] = dtdata.Rows[d]["cstp"].ToString();
                        dr["cstamt"] = dtdata.Rows[d]["cstamt"].ToString();
                        dr["amount"] = dtdata.Rows[d]["amount"].ToString();

                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        Session["dtpitemssach"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                        gvsalesorder.Visible = false;
                        ModalPopupExtender2.Show();
                    }
                }
                else
                {
                    gvscitemlist.Visible = false;
                    gvsoitemlistselection.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
        }
        hideimage();
        ModalPopupExtender2.Hide();
        Page.SetFocus(txtdespatchedby);
    }
    protected void drpchallantype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.ctype = drpchallantype.SelectedItem.Text;
            li.strscno = li.ctype + (Convert.ToInt64(txtchallanno.Text)).ToString();
            DataTable dtcheck = new DataTable();
            //dtcheck = fscclass.selectallsaleschallandatafromscno(li);
            dtcheck = fscclass.selectallsaleschallandatafromscnoag(li);
            if (dtcheck.Rows.Count == 0)
            {

            }
            else
            {
                li.scno = Convert.ToInt64(txtchallanno.Text);
                fscclass.updateisused(li);
                getscno();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Challan No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.ctype = drpchallantype.SelectedItem.Text;
            li.strscno = li.ctype + (Convert.ToInt64(txtchallanno.Text)).ToString();
            li.scno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtcheck = new DataTable();
            //dtcheck = fscclass.selectallsaleschallandatafromscno(li);
            dtcheck = fscclass.selectallsaleschallandatafromscnoag(li);
            if (dtcheck.Rows.Count == 0)
            {

            }
            else
            {
                li.scno = Convert.ToInt64(txtchallanno.Text);
                fscclass.updateisused(li);
                //getscno();
                txtchallanno.Text = ViewState["scno"].ToString();
                drpchallantype.SelectedValue = ViewState["ctype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Challan No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(drpacname);
    }
    protected void txtchallanno_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.ctype = drpchallantype.SelectedItem.Text;
            li.strscno = li.ctype + (Convert.ToInt64(txtchallanno.Text)).ToString();
            DataTable dtcheck = new DataTable();
            //dtcheck = fscclass.selectallsaleschallandatafromscno(li);
            dtcheck = fscclass.selectallsaleschallandatafromscnoag(li);
            if (dtcheck.Rows.Count == 0)
            {

            }
            else
            {
                li.scno = Convert.ToInt64(txtchallanno.Text);
                fscclass.updateisused(li);
                getscno();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Challan No. already exist.Try Again.');", true);
                return;
            }
        }
        else
        {
            li.scno = Convert.ToInt64(txtchallanno.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.ctype = drpchallantype.SelectedItem.Text;
            li.strscno = li.ctype + (Convert.ToInt64(txtchallanno.Text)).ToString();
            DataTable dtcheck = new DataTable();
            //dtcheck = fscclass.selectallsaleschallandatafromscno(li);
            dtcheck = fscclass.selectallsaleschallandatafromscnoag(li);
            if (dtcheck.Rows.Count == 0)
            {

            }
            else
            {
                li.scno = Convert.ToInt64(txtchallanno.Text);
                fscclass.updateisused(li);
                //getscno();
                txtchallanno.Text = ViewState["scno"].ToString();
                drpchallantype.SelectedValue = ViewState["ctype"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sales Challan No. already exist.Try Again.');", true);
                return;
            }
        }
        Page.SetFocus(drpacname);
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtstockqty.Text = "0";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txtbasicamt.Text = Math.Round((Convert.ToDouble(txtbillqty.Text) * Convert.ToDouble(txtrate.Text)), 2).ToString();
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
            }
            li.itemname = drpitemname.SelectedItem.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) - Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
            else
            {
                lblcloseqty.Text = Math.Round(0 - (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            lblcloseqty.Text = Math.Round(0 - (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
        }
        Page.SetFocus(txtbillqty);
    }
    protected void txtstockqty_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            //
            li.itemname = drpitemname.SelectedItem.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) - Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
            else
            {
                lblcloseqty.Text = Math.Round(0 - (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
        }
        else
        {
            lblcloseqty.Text = "0";
        }
        //
        Page.SetFocus(txtrate);
    }

    protected void txtgvstockqty_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblitemname = (Label)currentRow.FindControl("lblitemname");
        Label lblgvstockqtyold = (Label)currentRow.FindControl("lblgvstockqty");
        Label lblgvcloseqty = (Label)currentRow.FindControl("lblgvcloseqty");
        TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
        TextBox txtgridrate = (TextBox)currentRow.FindControl("txtgridrate");
        if (lblitemname.Text != "--SELECT--")
        {
            //
            li.itemname = lblitemname.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblgvcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) - Convert.ToDouble(txtgvstockqty.Text) + Convert.ToDouble(lblgvstockqtyold.Text)), 2).ToString();
            }
            else
            {
                lblgvcloseqty.Text = Math.Round(0 - (Convert.ToDouble(txtgvstockqty.Text)) + Convert.ToDouble(lblgvstockqtyold.Text), 2).ToString();
            }
        }
        else
        {
            lblgvcloseqty.Text = "0";
        }
        Page.SetFocus(txtgridrate);
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.ctype = drpchallantype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' order by scno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = li.ctype + dtdataq.Rows[0]["scno"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallsaleschallandatafromscnostrscno(li);
            if (dtdata.Rows.Count > 0)
            {
                ViewState["scno"] = dtdata.Rows[0]["scno"].ToString();
                txtchallanno.Text = dtdata.Rows[0]["scno"].ToString();
                txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
                txtsono.Text = dtdata.Rows[0]["sono1"].ToString();
                if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                {
                    txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtdespatchedby.Text = dtdata.Rows[0]["despatchedby"].ToString();
                txtfreightrs.Text = dtdata.Rows[0]["freightrs"].ToString();
                txtsalesorder.Text = dtdata.Rows[0]["sono"].ToString();
                drpstatus.SelectedItem.Text = dtdata.Rows[0]["status"].ToString();
                drpchallantype.SelectedItem.Text = dtdata.Rows[0]["ctype"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                ViewState["ctype"] = dtdata.Rows[0]["ctype"].ToString();
                DataTable dtpcitems = new DataTable();
                dtpcitems = fscclass.selectallscitemsfromscnostring(li);
                if (dtpcitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvscitemlist.Visible = true;
                    gvscitemlist.DataSource = dtpcitems;
                    gvscitemlist.DataBind();
                    lblcount.Text = dtpcitems.Rows.Count.ToString();
                }
                else
                {
                    gvscitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Challan Items Found.";
                    lblcount.Text = "0";
                }

                btnsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.ctype = drpchallantype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' order by scno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strscno = li.ctype + dtdataq.Rows[dtdataq.Rows.Count - 1]["scno"].ToString();
            DataTable dtdata = new DataTable();
            dtdata = fscclass.selectallsaleschallandatafromscnostrscno(li);
            if (dtdata.Rows.Count > 0)
            {
                ViewState["scno"] = dtdata.Rows[0]["scno"].ToString();
                txtchallanno.Text = dtdata.Rows[0]["scno"].ToString();
                txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
                txtsono.Text = dtdata.Rows[0]["sono1"].ToString();
                if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                {
                    txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtdespatchedby.Text = dtdata.Rows[0]["despatchedby"].ToString();
                txtfreightrs.Text = dtdata.Rows[0]["freightrs"].ToString();
                txtsalesorder.Text = dtdata.Rows[0]["sono"].ToString();
                drpstatus.SelectedItem.Text = dtdata.Rows[0]["status"].ToString();
                drpchallantype.SelectedItem.Text = dtdata.Rows[0]["ctype"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                ViewState["ctype"] = dtdata.Rows[0]["ctype"].ToString();
                DataTable dtpcitems = new DataTable();
                dtpcitems = fscclass.selectallscitemsfromscnostring(li);
                if (dtpcitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvscitemlist.Visible = true;
                    gvscitemlist.DataSource = dtpcitems;
                    gvscitemlist.DataBind();
                    lblcount.Text = dtpcitems.Rows.Count.ToString();
                }
                else
                {
                    gvscitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Sales Challan Items Found.";
                    lblcount.Text = "0";
                }

                btnsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.ctype = drpchallantype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' order by scno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.scno = Convert.ToInt64(txtchallanno.Text) + 1;
        for (Int64 i = li.scno; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["scno"].ToString()); i++)
        {
            li.scno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' and scno=" + li.scno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strscno = li.ctype + dtdataq.Rows[0]["scno"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fscclass.selectallsaleschallandatafromscnostrscno(li);
                if (dtdata.Rows.Count > 0)
                {
                    ViewState["scno"] = dtdata.Rows[0]["scno"].ToString();
                    txtchallanno.Text = dtdata.Rows[0]["scno"].ToString();
                    txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
                    txtsono.Text = dtdata.Rows[0]["sono1"].ToString();
                    if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                    {
                        txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtdespatchedby.Text = dtdata.Rows[0]["despatchedby"].ToString();
                    txtfreightrs.Text = dtdata.Rows[0]["freightrs"].ToString();
                    txtsalesorder.Text = dtdata.Rows[0]["sono"].ToString();
                    drpstatus.SelectedItem.Text = dtdata.Rows[0]["status"].ToString();
                    drpchallantype.SelectedItem.Text = dtdata.Rows[0]["ctype"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    ViewState["ctype"] = dtdata.Rows[0]["ctype"].ToString();
                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fscclass.selectallscitemsfromscnostring(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvscitemlist.Visible = true;
                        gvscitemlist.DataSource = dtpcitems;
                        gvscitemlist.DataBind();
                        lblcount.Text = dtpcitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvscitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Challan Items Found.";
                        lblcount.Text = "0";
                    }

                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.ctype = drpchallantype.SelectedItem.Text;
        SqlDataAdapter daa = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' order by scno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.scno = Convert.ToInt64(txtchallanno.Text) - 1;
        for (Int64 i = li.scno; i >= Convert.ToInt64(dtdataqa.Rows[0]["scno"].ToString()); i--)
        {
            li.scno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' and scno=" + li.scno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strscno = li.ctype + dtdataq.Rows[0]["scno"].ToString();
                DataTable dtdata = new DataTable();
                dtdata = fscclass.selectallsaleschallandatafromscnostrscno(li);
                if (dtdata.Rows.Count > 0)
                {
                    ViewState["scno"] = dtdata.Rows[0]["scno"].ToString();
                    txtchallanno.Text = dtdata.Rows[0]["scno"].ToString();
                    txtscdate.Text = Convert.ToDateTime(dtdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    txtccode.Text = dtdata.Rows[0]["ccode"].ToString();
                    txtsono.Text = dtdata.Rows[0]["sono1"].ToString();
                    if (dtdata.Rows[0]["sodate"].ToString() != string.Empty)
                    {
                        txtsodate.Text = Convert.ToDateTime(dtdata.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                    if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty)
                    {
                        txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    txtdespatchedby.Text = dtdata.Rows[0]["despatchedby"].ToString();
                    txtfreightrs.Text = dtdata.Rows[0]["freightrs"].ToString();
                    txtsalesorder.Text = dtdata.Rows[0]["sono"].ToString();
                    drpstatus.SelectedItem.Text = dtdata.Rows[0]["status"].ToString();
                    drpchallantype.SelectedItem.Text = dtdata.Rows[0]["ctype"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    ViewState["ctype"] = dtdata.Rows[0]["ctype"].ToString();
                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fscclass.selectallscitemsfromscnostring(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvscitemlist.Visible = true;
                        gvscitemlist.DataSource = dtpcitems;
                        gvscitemlist.DataBind();
                        lblcount.Text = dtpcitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvscitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Challan Items Found.";
                        lblcount.Text = "0";
                    }

                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }

    protected void lnkewaypopup_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
    }
    protected void lnkewayselectionpopup_Click(object sender, EventArgs e)
    {

    }
    protected void drpeway_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpeway.SelectedItem.Text == "YES")
        {
            ModalPopupExtender1.Show();
        }
        else
        {
            ModalPopupExtender1.Hide();
        }
    }
    protected void btnsaveewaydata_Click(object sender, EventArgs e)
    {
        li.strscno = drpchallantype.SelectedItem.Text + (Convert.ToInt64(txtchallanno.Text)).ToString();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

        li.ewaybillno = txtewaybillno.Text;
        li.ewaybilldate = Convert.ToDateTime(txtdate1.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.cewaybillno = txtconsolidatedewaybillno.Text;
        li.cewaybilldate = Convert.ToDateTime(txtdate2.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.supplytype = drpsupplytype.SelectedValue;
        li.subtype = drpsubtype.SelectedValue;
        li.doctype = drpdocumenttype.SelectedValue;

        li.tramode = drpmode.SelectedValue;
        li.distanceinkm = txtdistance.Text;
        li.transportername = txttransportername.Text;
        li.vehiclenumber = txtvehiclenumber.Text;
        li.vehicletype = drpvehicletype.SelectedValue;
        DataTable dtmainhsncode = fscclass.selectmainhsncode(li);
        if (dtmainhsncode.Rows.Count > 0)
        {
            li.mainhsncode = dtmainhsncode.Rows[0]["hsncode"].ToString();
        }
        li.trano = txtdoclandingrrairwayno.Text;
        li.tradate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.traid = txttransporterid.Text;

        li.consignor = txtconsignor.Text;
        li.consignoradd1 = txtaddress1.Text;
        li.consignoradd2 = txtaddress2.Text;
        li.consignorplace = txtplace.Text;
        li.consignorpincode = txtpincode.Text;
        li.consignorstate = drpfromstate.SelectedValue;
        li.actualfromstate = drpactualfromstate.SelectedValue;
        li.consignorgst = txtgstinuin.Text;

        li.consignee = txtconsignee.Text;
        li.consigneeadd1 = txtconsigneeaddress1.Text;
        li.consigneeadd2 = txtconsigneeaddress2.Text;
        li.consigneeplace = txttoplace.Text;
        li.consigneepincode = txtconsigneepincode.Text;
        li.consigneestate = drptostate.SelectedValue;
        li.actualtostate = drpactualtostate.SelectedValue;
        li.consigneegst = txtconsigneegstinuin.Text;
        fscclass.updateewaybilldetailfromstrscno(li);
        for (int g = 0; g < gvscitemlist.Rows.Count; g++)
        {

        }
        DataTable table = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter das = new SqlDataAdapter("select sum(vatamt) as totvat,SUM(addtaxamt) as totaddvat,SUM(cstamt) as totcst,sum(basicamount) as totbasic,SUM(amount) as totamount from SCItems where strscno='" + li.strscno + "'", con);
        DataTable dtdata = new DataTable();
        das.Fill(dtdata);
        double totbasic = Convert.ToDouble(dtdata.Rows[0]["totbasic"].ToString());
        double totvat = Convert.ToDouble(dtdata.Rows[0]["totvat"].ToString());
        double totaddvat = Convert.ToDouble(dtdata.Rows[0]["totaddvat"].ToString());
        double totcst = Convert.ToDouble(dtdata.Rows[0]["totcst"].ToString());
        double totamount = Convert.ToDouble(dtdata.Rows[0]["totamount"].ToString());
        SqlDataAdapter da = new SqlDataAdapter("select consignorgst as userGstin,supplytype as supplyType,subtype as subSupplyType,doctype as docType,strscno as docNo,CONVERT(varchar(10),scdate,103) as docDate,transType=1,consignorgst as fromGstin,consignor as fromTrdName,consignoradd1 as fromAddr1,consignoradd2 as fromAddr2,consignorplace as fromPlace,consignorpincode as fromPincode,consignorstate as fromStateCode,actualfromstatecode as actualFromStateCode,consigneegst as toGstin,consignee as toTrdName,consigneeadd1 as toAddr1,consigneeadd2 as toAddr2,consigneeplace as toPlace,consigneepincode as toPincode,consigneestate as toStateCode,actualtostatecode as actualToStateCode,totalValue=" + totbasic + ",cgstValue=" + totvat + ",sgstValue=" + totaddvat + ",igstValue=" + totcst + ",cessValue=0,TotNonAdvolVal=0,tramode as transMode,distanceinkm as transDistance,transportername as transporterName,traid as transporterId,trano as transDocNo,CONVERT(varchar(10),tradate,103) as transDocDate,vehicleno as vehicleNo,vehicletype as vehicleType,totInvValue=" + totamount + ",OthValue=0,mainhsncode as mainHsnCode from scmaster where strscno='" + li.strscno + "'", con);
        da.Fill(table);

        var JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            string cc = "";
            cc = "{" + "\"" + "version" + "\"" + ":" + "\"" + "1.0.0918" + "\"" + "," + "\"" + "billLists" + "\"" + ":";//1118
            JSONString.Append(cc + "[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
            }
            cc = "," + "\"" + "itemList" + "\"" + ":";
            JSONString.Append(cc + "[");
            SqlDataAdapter da1 = new SqlDataAdapter("select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS ItemNo,SCItems.itemname as productName,(SCItems.descr1+SCItems.descr2+SCItems.descr3) as productDesc,ItemMaster.hsncode as hsnCode,SCItems.qty as quantity,SCItems.eunit as qtyUnit,SCItems.basicamount as taxableAmount,SCItems.vatp as sgstRate,SCItems.addtaxp as cgstRate,SCItems.cstp as igstRate,cessRate=0,cessNonAdvol=0 from SCItems inner join ItemMaster on SCItems.itemname=ItemMaster.itemname where SCItems.strscno='" + li.strscno + "'", con);
            DataTable dtc = new DataTable();
            da1.Fill(dtc);
            //for (int i1 = 0; i1 < dtc.Rows.Count; i1++)
            //{
            //    if (dtc.Rows[i1]["qtyUnit"].ToString() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No E-Way bill unit is available for item " + dtc.Rows[i1]["productName"].ToString() + ".');", true);
            //        return;
            //    }
            //}
            for (int i = 0; i < dtc.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < dtc.Columns.Count; j++)
                {
                    if (j < dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == dtc.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]}]}");
            string path1 = @"D:\Eway Bill JSON";
            if (!Directory.Exists(path1))
            {
                Directory.CreateDirectory(path1);
            }
            string path = @"D:\Eway Bill JSON\" + li.strscno + ".json";
            using (var file = new StreamWriter(path, false))
            {
                file.Write(JSONString);
                file.Close();
                file.Dispose();

                //string strURL = Server.MapPath(@"~/Eway Bill JSON/" + li.strscno + ".json");
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + li.strscno + ".json\"");
                byte[] data = req.DownloadData(path);
                response.BinaryWrite(data);
                response.End();
                ModalPopupExtender1.Hide();
            }
        }
        //return JSONString.ToString();

        Response.Redirect("~/SalesChallanList.aspx?pagename=SalesInvoiceList");
    }

    protected void txtconsigneegstinuin_TextChanged(object sender, EventArgs e)
    {
        if (txtconsigneegstinuin.Text.Trim() != string.Empty)
        {
            drptostate.SelectedValue = txtconsigneegstinuin.Text.Substring(0, 2);
        }
        ModalPopupExtender1.Show();
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
        Response.Redirect("~/SalesChallanList.aspx?pagename=SalesInvoiceList");
    }

    protected void txtgridtaxtype_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        Label txtgvamount1 = (Label)currentRow.FindControl("lblamount");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lbltotamount");
        //Label lbltaxtype1 = (Label)currentRow.FindControl("lbltaxtype");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            txtgridcstp.Text = "0";
            //lbltaxtype.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        //if (gvsoitemlist.Rows.Count > 0)
        //{
        //    double totbasicamt = 0;
        //    double totcst = 0;
        //    double totvat = 0;
        //    double totaddvat = 0;
        //    double totamount = 0;
        //    for (int c = 0; c < gvsoitemlist.Rows.Count; c++)
        //    {
        //        Label lblbasicamount = (Label)gvsoitemlist.Rows[c].FindControl("lblbasicamt");
        //        Label lblsctamt = (Label)gvsoitemlist.Rows[c].FindControl("lblcstamt");
        //        Label lblvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lblvatamt");
        //        Label lbladdvatamt1 = (Label)gvsoitemlist.Rows[c].FindControl("lbladdvatamt");
        //        Label lblamount1 = (Label)gvsoitemlist.Rows[c].FindControl("lblamount");
        //        totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
        //        if (lblsctamt.Text != string.Empty)
        //        {
        //            totcst = totcst + Convert.ToDouble(lblsctamt.Text);
        //        }
        //        if (lblvatamt1.Text != string.Empty)
        //        {
        //            totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
        //        }
        //        if (lbladdvatamt1.Text != string.Empty)
        //        {
        //            totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
        //        }
        //        if (lblamount1.Text != string.Empty)
        //        {
        //            totamount = totamount + Convert.ToDouble(lblamount1.Text);
        //        }
        //    }
        //    //countgv();
        //}
        Page.SetFocus(txtgridcstp);
    }

    protected void txtgridcstp_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();



        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgridtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txtgridcstp");
        Label lblvatp = (Label)currentRow.FindControl("lblvatp");
        Label lbladdvatp = (Label)currentRow.FindControl("lbladdvatp");
        Label lblcstp = (Label)currentRow.FindControl("lblcstp");
        Label lblvatamt = (Label)currentRow.FindControl("lblvatamt");
        Label lbladdvatamt = (Label)currentRow.FindControl("lbladdvatamt");
        Label lblcstamt = (Label)currentRow.FindControl("lblcstamt");
        Label lblamount = (Label)currentRow.FindControl("lbltotamount");
        txtgvamount1.Text = ((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text))).ToString();
        var cc1 = txtgridcstp.Text.IndexOf("-");
        if (cc1 != -1)
        {
            txtgridcstp.Text = "0";
        }
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtgvamount1.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}
                //else
                //{
                txtgridcstp.Text = "0";
                //}
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (txtgridcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(txtgridcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            txtgridcstp.Text = "0";
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();


        //if (gvscitemlist.Rows.Count > 0)
        //{
        //    double totbasicamt = 0;
        //    double totcst = 0;
        //    double totvat = 0;
        //    double totaddvat = 0;
        //    double totamount = 0;
        //    for (int c = 0; c < gvscitemlist.Rows.Count; c++)
        //    {
        //        TextBox lblbasicamount = (TextBox)gvscitemlist.Rows[c].FindControl("txtgridamount");
        //        Label lblsctamt = (Label)gvscitemlist.Rows[c].FindControl("lblcstamt");
        //        Label lblvatamt1 = (Label)gvscitemlist.Rows[c].FindControl("lblvatamt");
        //        Label lbladdvatamt1 = (Label)gvscitemlist.Rows[c].FindControl("lbladdvatamt");
        //        Label lblamount1 = (Label)gvscitemlist.Rows[c].FindControl("lblamount");
        //        totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamount.Text);
        //        if (lblsctamt.Text != string.Empty)
        //        {
        //            totcst = totcst + Convert.ToDouble(lblsctamt.Text);
        //        }
        //        if (lblvatamt1.Text != string.Empty)
        //        {
        //            totvat = totvat + Convert.ToDouble(lblvatamt1.Text);
        //        }
        //        if (lbladdvatamt1.Text != string.Empty)
        //        {
        //            totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt1.Text);
        //        }
        //        if (lblamount1.Text != string.Empty)
        //        {
        //            totamount = totamount + Convert.ToDouble(lblamount1.Text);
        //        }
        //    }
        //}
    }

}