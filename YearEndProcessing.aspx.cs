﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class YearEndProcessing : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();
    ForLedger flclass = new ForLedger();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnyearend_Click(object sender, EventArgs e)
    {
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connma"].ConnectionString);
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        string olddbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        string newdbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1] + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]) + 1).ToString();
        //Check databse exist or not
        int error = 0;
        con.Open();
        DataTable dtdt = new DataTable();
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("select * FROM master.dbo.sysdatabases where name ='" + newdbname + "'", con);
            da.Fill(dtdt);
            con.Close();
            error = 1;
        }
        catch (Exception ex)
        {
            error = 0;
        }
        if (error != 1 || dtdt.Rows.Count == 0)
        {
            //Create new year's database
            SqlCommand cmdc = new SqlCommand("CREATE DATABASE " + newdbname + "", con);
            con.Open();
            cmdc.ExecuteNonQuery();
            con.Close();
            SqlConnection con111 = new SqlConnection("Data Source=CHIRAG-PC;Initial Catalog=" + newdbname + ";Integrated Security=True");

            string strsql = "CREATE TABLE [dbo].[ACGroupMaster]([id] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,[groupcode] [varchar](50) NULL,[groupname] [varchar](50) NULL,[mastercode] [varchar](50) NULL,[schedule] [varchar](50) NULL,[schno] [bigint] NULL,[type] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[ACMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[acname] [varchar](500) NULL,[contactperson] [varchar](500) NULL,[add1] [varchar](500) NULL,[add2] [varchar](500) NULL,[add3] [varchar](500) NULL,[city] [varchar](50) NULL,[pincode] [varchar](50) NULL,[phone1] [varchar](50) NULL,[phone2] [varchar](50) NULL,[phoner1] [varchar](50) NULL,[phoner2] [varchar](50) NULL,[mobileno] [varchar](50) NULL,[fax] [varchar](50) NULL,[emailid] [varchar](500) NULL,[actype] [varchar](50) NULL,[gsttinno] [varchar](50) NULL,[date1] [varchar](50) NULL,[csttinno] [varchar](50) NULL,[date2] [varchar](50) NULL,[duedays] [bigint] NULL,[discp] [float] NULL,[servicetaxno] [varchar](50) NULL,[tan] [varchar](50) NULL,[pan] [varchar](50) NULL,[debitgroupcode] [varchar](50) NULL,[creditgroupcode] [varchar](50) NULL,[opbalance] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[gstno] [varchar](50) NULL,[gstnodate] [varchar](50) NULL,[inexdebit] [varchar](50) NULL,[acyear] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[ACGroupMaster1]([id] [bigint] NOT NULL PRIMARY KEY,[groupcode] [varchar](50) NULL,[groupname] [varchar](50) NULL,[type] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[activity]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[activity] [varchar](500) NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[cno] [bigint] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[acyear]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[acyear] [varchar](50) NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[inexdebit] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[BankACMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[voucherno] [bigint] NOT NULL,[voucherdate] [datetime] NULL,[name] [varchar](500) NULL,[acname] [varchar](500) NULL,[ccode] [bigint] NULL,[remarks] [varchar](max) NULL,[amount] [float] NULL,[chequeno] [varchar](50) NULL,[agbill] [varchar](50) NULL,[istype] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[sino] [bigint] NULL,[paidamount] [float] NULL,[issipi] [varchar](50) NULL,[iscd] [varchar](50) NULL,[cldate] [datetime] NULL,[hsncode] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[BankACMaster1]([voucherno] [bigint] NOT NULL,[voucherdate] [datetime] NULL,[name] [varchar](500) NULL,[istype] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[total] [float] NULL,[type] [varchar](50) NULL,[billno] [varchar](50) NULL,[vtype] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[BankACShadow]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[voucherno] [bigint] NULL,[ccode] [varchar](50) NULL,[invoiceno] [varchar](50) NULL,[amount] [float] NULL,[istype] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[issipi] [varchar](50) NULL,[refid] [bigint] NULL,[acname] [varchar](500) NULL,[voucherdate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[ClientMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[code] [bigint] NOT NULL,[name] [varchar](500) NULL,[add1] [varchar](500) NULL,[add2] [varchar](500) NULL,[add3] [varchar](500) NULL,[city] [varchar](50) NULL,[pincode] [varchar](50) NULL,[mobileno] [varchar](50) NULL,[emailid] [varchar](500) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[opbalance] [float] NULL,[acname] [varchar](500) NULL,[referencename] [varchar](500) NULL,[handledby] [varchar](500) NULL,[status] [varchar](50) NULL,[remarks] [varchar](max) NULL,[pdate] [datetime] NULL,[acyear] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[cmast]([cno] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[csnm] [varchar](50) NULL,[cname] [varchar](500) NULL,[address] [varchar](max) NULL,[location] [varchar](50) NULL,[emailid] [varchar](500) NULL,[contact1] [varchar](50) NULL,[contact2] [varchar](50) NULL,[faxno] [varchar](50) NULL,[tanno] [varchar](50) NULL,[vatno] [varchar](50) NULL,[cstno] [varchar](50) NULL,[eccno] [varchar](50) NULL,[servicetaxno] [varchar](50) NULL,[rocno] [varchar](50) NULL,[imexcode] [varchar](50) NULL,[yearoe] [varchar](50) NULL,[bank1] [varchar](50) NULL,[bank2] [varchar](50) NULL,[emppt] [varchar](50) NULL,[comppt] [varchar](50) NULL,[pfno] [varchar](50) NULL,[panno] [varchar](50) NULL,[gstno] [varchar](50) NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[CommInvMaster]([invno] [bigint] NOT NULL PRIMARY KEY,[invdate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[basicamount] [float] NULL,[vat] [float] NULL,[advat] [float] NULL,[cst] [float] NULL,[amount] [float] NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[pono] [varchar](50) NULL,[podate] [varchar](50) NULL,[annexure] [varchar](max) NULL,[isigst] [varchar](50) NULL,[sono] [bigint] NULL,[dtors] [float] NULL,[etors] [float] NULL) CREATE TABLE [dbo].[CommInvItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[invno] [bigint] NULL,[invdate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[rate] [float] NULL,[basicamt] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[addtaxp] [float] NULL,[addtaxamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[unit] [varchar](50) NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[vid] [bigint] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[ItemMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[itemname] [varchar](500) NULL,[description] [varchar](max) NULL,[unit] [varchar](50) NULL,[hsncode] [varchar](50) NULL,[salesrate] [float] NULL,[purchaserate] [float] NULL,[valuationrate] [float] NULL,[vattype] [varchar](50) NULL,[oprate] [float] NULL,[qty] [float] NULL,[rate] [float] NULL,[amount] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[gsttype] [varchar](50) NULL,[vatdesc] [varchar](50) NULL,[gstdesc] [varchar](50) NULL,[isblock] [varchar](10) NULL,[reason] [varchar](500) NULL,[opqty] [float] NULL,[acyear] [varchar](50) NULL) CREATE TABLE [dbo].[ItemMaster1]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[itemname] [varchar](500) NULL,[opdate] [datetime] NULL,[qty] [float] NULL,[rate] [float] NULL,[amount] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[adjqty] [float] NULL,[acyear] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[Ledger]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[voucherno] [bigint] NULL,[strvoucherno] [varchar](50) NULL,[voucherdate] [datetime] NULL,[refid] [bigint] NULL,[debitcode] [varchar](500) NULL,[creditcode] [varchar](500) NULL,[description] [varchar](max) NULL,[projectcode] [varchar](50) NULL,[amount] [float] NULL,[type] [varchar](50) NULL,[istype] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[acyear] [varchar](50) NULL) CREATE TABLE [dbo].[Ledger_e]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[strvoucherno] [varchar](50) NULL,[voucherdate] [datetime] NULL,[billno] [varchar](50) NULL,[acname] [varchar](50) NULL,[gstno] [varchar](50) NULL,[hsncode] [varchar](50) NULL,[billvalue] [float] NULL,[basicamt] [float] NULL,[cgst] [float] NULL,[sgst] [float] NULL,[igst] [float] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[pagename]([id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,[pagename] [varchar](50) NULL,[page] [varchar](50) NULL,[remark] [varchar](150) NULL) CREATE TABLE [dbo].[OPBMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[name] [varchar](500) NULL,[amount] [float] NULL,[oth1] [varchar](max) NULL,[oth2] [varchar](500) NULL,[oth3] [varchar](500) NULL,[oth4] [varchar](500) NULL,[oth5] [varchar](500) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[MiscMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[type] [varchar](50) NULL,[name] [varchar](max) NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[login]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[username] [varchar](50) NULL,[password] [varchar](50) NULL,[cno] [bigint] NULL,[isactive] [varchar](5) NULL,[role] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[PCMaster]([pcno] [bigint] NOT NULL,[pcdate] [datetime] NULL,[chno] [bigint] NULL,[pono] [varchar](50) NULL,[podate] [datetime] NULL,[pono1] [varchar](50) NULL,[podate1] [datetime] NULL,[acname] [varchar](500) NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[stringpono] [varchar](max) NULL) CREATE TABLE [dbo].[PCItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[pcno] [bigint] NULL,[pcdate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[rate] [float] NULL,[basicamount] [float] NULL,[ccode] [bigint] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[addtaxp] [float] NULL,[cstp] [float] NULL,[vatamt] [float] NULL,[addtaxamt] [float] NULL,[cstamt] [float] NULL,[amount] [float] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [bigint] NULL,[vid] [bigint] NULL,[adjqty] [float] NULL,[unit] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[PerInvMaster]([invno] [bigint] NOT NULL PRIMARY KEY,[invdate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[basicamount] [float] NULL,[vat] [float] NULL,[advat] [float] NULL,[cst] [float] NULL,[amount] [float] NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[pono] [varchar](50) NULL,[podate] [varchar](50) NULL,[annexure] [varchar](max) NULL,[isigst] [varchar](50) NULL,[sono] [bigint] NULL,[dtors] [float] NULL,[etors] [float] NULL) CREATE TABLE [dbo].[PerInvItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[invno] [bigint] NULL,[invdate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[rate] [float] NULL,[basicamt] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[addtaxp] [float] NULL,[addtaxamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[unit] [varchar](50) NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[vid] [bigint] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[PIMaster]([pino] [bigint] NOT NULL PRIMARY KEY,[pidate] [datetime] NULL,[invtype] [varchar](50) NULL,[billno] [bigint] NULL,[billdate] [datetime] NULL,[pcno] [bigint] NULL,[pcdate] [datetime] NULL,[pcno1] [bigint] NULL,[pcdate1] [datetime] NULL,[acname] [varchar](500) NULL,[purchaseac] [varchar](500) NULL,[totbasicamount] [float] NULL,[totvat] [float] NULL,[totaddtax] [float] NULL,[totcst] [float] NULL,[servicetaxp] [float] NULL,[servicetaxamount] [float] NULL,[cartage] [float] NULL,[roundoff] [float] NULL,[billamount] [float] NULL,[type] [varchar](50) NULL,[ispaid] [varchar](50) NULL,[remainamount] [float] NULL,[receivedamount] [float] NULL,[strpino] [varchar](50) NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[remarks] [varchar](max) NULL,[form] [varchar](50) NULL,[transport] [varchar](500) NULL,[salesman] [varchar](500) NULL,[lrno] [varchar](50) NULL,[lrdate] [datetime] NULL,[duedays] [float] NULL,[monyr] [varchar](50) NULL,[stringpcno] [varchar](max) NULL) CREATE TABLE [dbo].[PIItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[pino] [bigint] NULL,[pidate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[rate] [float] NULL,[basicamount] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[addtaxp] [float] NULL,[addtaxamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [bigint] NULL,[vid] [bigint] NULL,[taxdesc] [varchar](500) NULL,[ccode] [bigint] NULL,[monyr] [varchar](50) NULL,[strpino] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[pmmaster]([voucherno] [bigint] NOT NULL PRIMARY KEY,[vdate] [datetime] NULL,[name] [varchar](500) NULL,[clientcode] [bigint] NULL,[rmcno] [varchar](50) NULL,[tdcno] [varchar](50) NULL,[transportname] [varchar](500) NULL,[lrno] [varchar](50) NULL,[lrdate] [datetime] NULL,[vehicleno] [varchar](50) NULL,[drivername] [varchar](500) NULL,[dlicenseno] [varchar](50) NULL,[mobileno] [varchar](50) NULL,[istype] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[materiall] [varchar](500) NULL,[materialr] [varchar](500) NULL) CREATE TABLE [dbo].[pmitems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[voucherno] [bigint] NULL,[vdate] [datetime] NULL,[dcno] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[project2]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[ahuno] [varchar](50) NULL,[returnaircfm] [float] NULL,[returnairtemp] [float] NULL,[returnairgrains] [float] NULL,[freshaircfm] [float] NULL,[freshairtemp] [float] NULL,[freshairgrains] [float] NULL,[mixinaircfm] [float] NULL,[mixingairtemp] [float] NULL,[mixingairgrains] [float] NULL,[totalfancfm] [float] NULL,[beforefanairtemp] [float] NULL,[beforefanairgrains] [float] NULL,[fanstaticpressure] [float] NULL,[totalfankw] [float] NULL,[totalfanbtuhr] [float] NULL,[tempdiffdegreef] [float] NULL,[afterfabairtemp] [float] NULL,[afterfanairgrains] [float] NULL,[selecteddehcfm] [float] NULL,[actualdehaircfm] [float] NULL,[dehairtempafterchw] [float] NULL,[dehairgrainsafterchw] [float] NULL,[bypassaircfm] [float] NULL,[bypassairtemp] [float] NULL,[bypassairgrains] [float] NULL,[exhaustcfm] [float] NULL,[supplycfm] [float] NULL,[sensibleloadfromheatload] [float] NULL,[diversityofsensibleloadminload] [float] NULL,[diversityofsensibleloaddiversity] [float] NULL,[suppludb] [float] NULL,[latentload] [float] NULL,[supplygrains] [float] NULL,[hotwateroutletconditiondb] [float] NULL,[hotwateroutletconditiongrains] [float] NULL,[sensibleloadforhotwatercoil] [float] NULL,[hotwatercoilcapacityofsensibleload] [float] NULL,[checkpointforrh] [varchar](50) NULL,[checkpointfortemp] [varchar](50) NULL,[checkpointfortemp1] [varchar](50) NULL,[ccode] [bigint] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[project1]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[ahuno] [varchar](50) NULL,[returnaircfm] [float] NULL,[returnairtemp] [float] NULL,[returnairgrains] [float] NULL,[freshaircfm] [float] NULL,[freshairtemp] [float] NULL,[freshairgrains] [float] NULL,[mixinaircfm] [float] NULL,[mixingairtemp] [float] NULL,[mixingairgrains] [float] NULL,[totalfancfm] [float] NULL,[beforefanairtemp] [float] NULL,[beforefanairgrains] [float] NULL,[fanstaticpressure] [float] NULL,[totalfankw] [float] NULL,[totalfanbtuhr] [float] NULL,[tempdiffdegreef] [float] NULL,[afterfabairtemp] [float] NULL,[afterfanairgrains] [float] NULL,[selecteddehcfm] [float] NULL,[actualdehaircfm] [float] NULL,[dehairtempafterchw] [float] NULL,[dehairgrainsafterchw] [float] NULL,[bypassaircfm] [float] NULL,[bypassairtemp] [float] NULL,[bypassairgrains] [float] NULL,[exhaustcfm] [float] NULL,[supplycfm] [float] NULL,[sensibleloadfromheatload] [float] NULL,[diversityofsensibleloadminload] [float] NULL,[diversityofsensibleloaddiversity] [float] NULL,[suppludb] [float] NULL,[latentload] [float] NULL,[supplygrains] [float] NULL,[hotwateroutletconditiondb] [float] NULL,[hotwateroutletconditiongrains] [float] NULL,[sensibleloadforhotwatercoil] [float] NULL,[hotwatercoilcapacityofsensibleload] [float] NULL,[checkpointforrh] [varchar](50) NULL,[checkpointfortemp] [varchar](50) NULL,[checkpointfortemp1] [varchar](50) NULL,[ccode] [bigint] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[PurchaseOrderItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[pono] [varchar](50) NULL,[podate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[unit] [varchar](50) NULL,[rate] [float] NULL,[basicamount] [float] NULL,[taxtype] [varchar](500) NULL,[vatp] [float] NULL,[addtaxp] [float] NULL,[cstp] [float] NULL,[vatamt] [float] NULL,[addtaxamt] [float] NULL,[cstamt] [float] NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [varchar](50) NULL,[iscame] [varchar](50) NULL,[vid] [bigint] NULL,[adjqty] [float] NULL) CREATE TABLE [dbo].[PurchaseOrder]([pono] [varchar](50) NOT NULL PRIMARY KEY,[podate] [datetime] NULL,[sono] [bigint] NULL,[deldays] [bigint] NULL,[ccode] [bigint] NULL,[followupdate] [datetime] NULL,[acname] [varchar](500) NULL,[followupdetails] [varchar](max) NULL,[followupdetails1] [varchar](max) NULL,[totbasic] [float] NULL,[excisep] [float] NULL,[exciseamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[grandtotal] [float] NULL,[delivery] [varchar](max) NULL,[payment] [varchar](max) NULL,[deliveryat] [varchar](max) NULL,[taxes] [varchar](max) NULL,[octroi] [varchar](max) NULL,[excise] [varchar](max) NULL,[form] [varchar](max) NULL,[transportation] [varchar](max) NULL,[insurance] [varchar](max) NULL,[packing] [varchar](max) NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[descr4] [varchar](max) NULL,[descr5] [varchar](max) NULL,[descr6] [varchar](max) NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[type] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[QuoMaster]([quono] [float] NOT NULL PRIMARY KEY,[quodate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[basicamount] [float] NULL,[vat] [float] NULL,[advat] [float] NULL,[cst] [float] NULL,[amount] [float] NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[annexure] [varchar](max) NULL,[isigst] [varchar](50) NULL,[isso] [varchar](50) NULL) CREATE TABLE [dbo].[QuoItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[srno] [bigint] NULL,[quono] [float] NULL,[quodate] [datetime] NULL,[ccode] [bigint] NULL,[acname] [varchar](500) NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[rate] [float] NULL,[basicamt] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[addtaxp] [float] NULL,[addtaxamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[unit] [varchar](50) NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[make] [varchar](500) NULL,[hsncode] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[SalesmanMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[name] [varchar](500) NULL,[mobileno] [varchar](50) NULL,[emailid] [varchar](500) NULL,[address] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[rrmaster]([challanno] [bigint] NOT NULL PRIMARY KEY,[cdate] [datetime] NULL,[name] [varchar](500) NULL,[ccode] [bigint] NULL,[sono] [varchar](50) NULL,[sodate] [datetime] NULL,[lrno] [varchar](50) NULL,[lrdate] [datetime] NULL,[dispatchedby] [varchar](500) NULL,[freight] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[rritems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[challanno] [bigint] NULL,[cdate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[rate] [float] NULL,[amount] [float] NULL,[unit] [varchar](50) NULL,[descr] [varchar](max) NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[vid] [bigint] NULL,[retremarks] [varchar](max) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[SalesOrderItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[sono] [bigint] NULL,[sodate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[unit] [varchar](50) NULL,[rate] [float] NULL,[basicamount] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[addtaxp] [float] NULL,[cstp] [float] NULL,[vatamt] [float] NULL,[addtaxamt] [float] NULL,[cstamt] [float] NULL,[amount] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [bigint] NULL,[qtyremain1] [float] NULL,[qtyused1] [float] NULL,[vid] [bigint] NULL,[firstquotation] [datetime] NULL,[technicaldrawing] [datetime] NULL,[rewisequotation] [datetime] NULL,[finaltech] [datetime] NULL,[lastfollowupdate] [datetime] NULL,[pendingorderlying] [varchar](50) NULL,[okorderdate] [datetime] NULL,[tentativedeliverydate] [datetime] NULL,[deliverydate] [datetime] NULL,[remarks] [varchar](max) NULL) CREATE TABLE [dbo].[SalesOrder]([sono] [bigint] NOT NULL PRIMARY KEY,[sodate] [datetime] NULL,[pono] [varchar](50) NULL,[podate] [datetime] NULL,[ccode] [bigint] NULL,[indentno] [bigint] NULL,[indentdate] [datetime] NULL,[acname] [varchar](500) NULL,[dispatchthrough] [varchar](500) NULL,[delat] [varchar](max) NULL,[delat1] [varchar](max) NULL,[delat2] [varchar](max) NULL,[totbasic] [float] NULL,[totvat] [float] NULL,[totaddvat] [float] NULL,[totcst] [float] NULL,[totsoamt] [float] NULL,[inst1] [varchar](max) NULL,[inst2] [varchar](max) NULL,[inst3] [varchar](max) NULL,[inst4] [varchar](max) NULL,[inst5] [varchar](max) NULL,[inst6] [varchar](max) NULL,[inst7] [varchar](max) NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[quono] [float] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[SCMaster]([scno] [bigint] NOT NULL,[scdate] [datetime] NULL,[acname] [varchar](500) NULL,[ccode] [bigint] NULL,[sono] [varchar](50) NULL,[sodate] [datetime] NULL,[lrno] [varchar](50) NULL,[lrdate] [datetime] NULL,[despatchedby] [varchar](500) NULL,[freightrs] [float] NULL,[sono1] [bigint] NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[ctype] [varchar](50) NULL,[strscno] [varchar](50) NULL,[ispacking] [varchar](50) NULL,[issigned] [varchar](10) NULL,[signremarks] [varchar](max) NULL) CREATE TABLE [dbo].[SCItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[scno] [bigint] NULL,[scdate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[rate] [float] NULL,[basicamount] [float] NULL,[unit] [varchar](50) NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[addtaxp] [float] NULL,[cstp] [float] NULL,[vatamt] [float] NULL,[addtaxamt] [float] NULL,[cstamt] [float] NULL,[amount] [float] NULL,[ccode] [bigint] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [bigint] NULL,[vid] [bigint] NULL,[strscno] [varchar](50) NULL,[adjqty] [float] NULL,[remarks] [varchar](max) NULL,[rfs] [float] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[SIMaster]([sino] [bigint] NOT NULL,[sidate] [datetime] NULL,[invtype] [varchar](50) NULL,[sono] [varchar](50) NULL,[sodate] [datetime] NULL,[scno] [varchar](50) NULL,[scdate] [datetime] NULL,[scno1] [varchar](50) NULL,[scdate1] [datetime] NULL,[acname] [varchar](500) NULL,[salesac] [varchar](500) NULL,[totbasicamount] [float] NULL,[totvat] [float] NULL,[totaddtax] [float] NULL,[totcst] [float] NULL,[servicetaxp] [float] NULL,[servicetaxamount] [float] NULL,[cartage] [float] NULL,[roundoff] [float] NULL,[billamount] [float] NULL,[type] [varchar](50) NULL,[ispaid] [varchar](50) NULL,[remainamount] [float] NULL,[receivedamount] [float] NULL,[strsino] [varchar](50) NULL,[status] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[remarks] [varchar](max) NULL,[form] [varchar](50) NULL,[transport] [varchar](500) NULL,[salesman] [varchar](500) NULL,[lrno] [varchar](50) NULL,[lrdate] [datetime] NULL,[duedays] [float] NULL,[monyr] [varchar](50) NULL,[stringscno] [varchar](max) NULL,[dtors] [float] NULL,[etors] [float] NULL,[portcode] [varchar](50) NULL,[sbillno] [varchar](50) NULL,[sbilldate] [datetime] NULL) CREATE TABLE [dbo].[SIItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[sino] [bigint] NULL,[sidate] [datetime] NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[stockqty] [float] NULL,[rate] [float] NULL,[basicamount] [float] NULL,[taxtype] [varchar](50) NULL,[vatp] [float] NULL,[vatamt] [float] NULL,[addtaxp] [float] NULL,[addtaxamt] [float] NULL,[cstp] [float] NULL,[cstamt] [float] NULL,[unit] [varchar](50) NULL,[amount] [float] NULL,[ccode] [bigint] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[qtyremain] [float] NULL,[qtyused] [float] NULL,[vno] [bigint] NULL,[vid] [bigint] NULL,[taxdesc] [varchar](500) NULL,[monyr] [varchar](50) NULL,[strsino] [varchar](50) NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[tempsheet]([acname] [varchar](500) NULL,[code] [varchar](50) NULL,[clbal] [float] NULL,[crdr] [varchar](50) NULL) CREATE TABLE [dbo].[tempsalestax]([taxdesc] [varchar](500) NULL,[totbasicamount] [float] NULL,[totvat] [float] NULL,[totaddtax] [float] NULL,[totcst] [float] NULL,[servicetaxamount] [float] NULL,[cartage] [float] NULL,[roundoff] [float] NULL,[billamount] [float] NULL,[monyr] [varchar](50) NULL,[sidate] [datetime] NULL) CREATE TABLE [dbo].[TaxMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[typename] [varchar](50) NULL,[vatp] [float] NULL,[addtaxp] [float] NULL,[centralsalep] [float] NULL,[selltaxp] [float] NULL,[surchargep] [float] NULL,[servicetaxp] [float] NULL,[form] [varchar](500) NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[vatdesc] [varchar](500) NULL,[addtaxdesc] [varchar](500) NULL,[cstdesc] [varchar](500) NULL,[servicetaxdesc] [varchar](500) NULL) CREATE TABLE [dbo].[StockJVMaster]([challanno] [varchar](50) NOT NULL,[challandate] [datetime] NULL,[acname] [varchar](500) NULL,[inout] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL) CREATE TABLE [dbo].[StockJVItems]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[challanno] [varchar](50) NULL,[challandate] [datetime] NULL,[acname] [varchar](500) NULL,[itemname] [varchar](500) NULL,[qty] [float] NULL,[rate] [float] NULL,[descr1] [varchar](max) NULL,[descr2] [varchar](max) NULL,[descr3] [varchar](max) NULL,[inout] [varchar](50) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[adjqty] [float] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[ToolsMaster]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[name] [varchar](500) NULL,[unit] [varchar](50) NULL,[rate] [float] NULL,[opstock] [float] NULL,[inward] [float] NULL,[outward] [float] NULL,[clstock] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[acyear] [varchar](50) NULL) CREATE TABLE [dbo].[ToolDelMaster]([voucherno] [bigint] NOT NULL PRIMARY KEY,[voucherdate] [datetime] NULL,[name] [varchar](500) NULL,[clientcode] [bigint] NULL,[dispatchedby] [varchar](500) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[istype] [varchar](50) NULL,[ispacking] [varchar](50) NULL) CREATE TABLE [dbo].[ToolDelItem]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[voucherno] [bigint] NULL,[voucherdate] [datetime] NULL,[toolname] [varchar](500) NULL,[qty] [float] NULL,[unit] [varchar](50) NULL,[descr] [varchar](max) NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[istype] [varchar](50) NULL,[remarks] [varchar](max) NULL,[rfs] [float] NULL)";
            strsql = strsql + "CREATE TABLE [dbo].[vnogen]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[mon] [int] NULL,[yr] [int] NULL,[vno] [varchar](50) NULL,[issoused] [varchar](50) NULL,[ispoused] [varchar](50) NULL,[isscused] [varchar](50) NULL,[ispcused] [varchar](50) NULL,[issiused] [varchar](50) NULL,[ispiused] [varchar](50) NULL,[isbrused] [varchar](50) NULL,[isbr1used] [varchar](50) NULL,[isbr2used] [varchar](50) NULL,[isbr3used] [varchar](50) NULL,[isbr4used] [varchar](50) NULL,[istdused] [varchar](50) NULL,[istrused] [varchar](50) NULL,[isrrused] [varchar](50) NULL,[ispmused] [varchar](50) NULL,[cno] [bigint] NULL,[isjvused] [varchar](50) NULL) CREATE TABLE [dbo].[userrights]([id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,[page] [varchar](50) NULL,[pagename] [varchar](50) NULL,[remark] [varchar](150) NULL,[uadd] [varchar](50) NULL,[uedit] [varchar](50) NULL,[udelete] [varchar](50) NULL,[uview] [varchar](50) NULL,[role] [varchar](50) NULL)";
            strsql = strsql + "";
            strsql = strsql + "";
            strsql = strsql + "";
            strsql = strsql + "";
            strsql = strsql + "";
            strsql = strsql + "";
            strsql = strsql + "";
            SqlCommand cmcmc = new SqlCommand(strsql, con111);
            con111.Open();
            cmcmc.ExecuteNonQuery();
            con111.Close();
            //////////strsql = "CREATE TABLE [dbo].[ACMaster]([id] [bigint] IDENTITY(1,1) NOT NULL,[acname] [varchar](500) NULL	[contactperson] [varchar](500) NULL,[add1] [varchar](500) NULL,[add2] [varchar](500) NULL,[add3] [varchar](500) NULL,[city] [varchar](50) NULL,[pincode] [varchar](50) NULL,[phone1] [varchar](50) NULL,[phone2] [varchar](50) NULL,[phoner1] [varchar](50) NULL,[phoner2] [varchar](50) NULL,[mobileno] [varchar](50) NULL,[fax] [varchar](50) NULL,[emailid] [varchar](500) NULL,[actype] [varchar](50) NULL,[gsttinno] [varchar](50) NULL,[date1] [varchar](50) NULL,[csttinno] [varchar](50) NULL,[date2] [varchar](50) NULL,[duedays] [bigint] NULL,[discp] [float] NULL,[servicetaxno] [varchar](50) NULL,[tan] [varchar](50) NULL,[pan] [varchar](50) NULL,[debitgroupcode] [varchar](50) NULL,[creditgroupcode] [varchar](50) NULL,[opbalance] [float] NULL,[cno] [bigint] NULL,[uname] [varchar](50) NULL,[udate] [datetime] NULL,[gstno] [varchar](50) NULL,[gstnodate] [varchar](50) NULL)";
            //////////con111.Open();
            //////////cmcmc.ExecuteNonQuery();
            //////////con111.Close();

            string alltablename = "ACGroupMaster/ACGroupMaster1/activity/acyear/ACMaster/BankACMaster/BankACMaster1/ClientMaster/cmast/CommInvItems/CommInvMaster/ItemMaster/ItemMaster1/Ledger/Ledger_e/login/MiscMaster/OPBMaster/pagename/PCItems/PCMaster/PerInvItems/PerInvMaster/PIItems/PIMaster/pmitems/pmmaster/project1/project2/PurchaseOrder/PurchaseOrderItems/QuoItems/QuoMaster/rritems/rrmaster/SalesOrder/SalesOrderItems/SCItems/SCMaster/SIItems/SIMaster/StockJVItems/StockJVMaster/TaxMaster/tempsalestax/tempsheet/ToolDelItem/ToolDelMaster/ToolsMaster/userrights/vnogen/SalesmanMaster";///ToolsJVItems/ToolsJVMaster
            string[] tablename = alltablename.Split('/');
            //foreach (string onetable in tablename)
            //{
            string sourcetable = olddbname + ".dbo.ACGroupMaster";
            string destinationtable = newdbname + ".dbo.ACGroupMaster";
            //SqlCommand cmd1 = new SqlCommand("Select * into parthvpatel.dbo.login from AllureGarment.dbo.login ", con);                
            SqlCommand cmd1;
            //SqlCommand cmdd;
            //if (onetable != "barcode")
            //{
            cmd1 = new SqlCommand("insert into " + destinationtable + " select groupcode,groupname,mastercode,schedule,schno,type,cno,uname,udate from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();

            sourcetable = olddbname + ".dbo.ACGroupMaster1";
            destinationtable = newdbname + ".dbo.ACGroupMaster1";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select * from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            string ayear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            if (chkbalancesheet.Checked == true)
            {
                sourcetable = olddbname + ".dbo.ACMaster";
                destinationtable = newdbname + ".dbo.ACMaster";
                cmd1 = new SqlCommand("insert into " + destinationtable + " select acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }
            if (chkclientmaster.Checked == true)
            {
                sourcetable = olddbname + ".dbo.ClientMaster";
                destinationtable = newdbname + ".dbo.ClientMaster";
                cmd1 = new SqlCommand("insert into " + destinationtable + " select code,name,add1,add2,add3,city,pincode,mobileno,emailid,cno,uname,udate,opbalance,acname,referencename,handledby,status,remarks,pdate,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }

            if (chkitemstock.Checked == true)
            {
                sourcetable = olddbname + ".dbo.ItemMaster";
                destinationtable = newdbname + ".dbo.ItemMaster";
                cmd1 = new SqlCommand("insert into " + destinationtable + " select itemname,description,unit,unitewaybill,hsncode,salesrate,purchaserate,valuationrate,vattype,oprate,qty,rate,amount,cno,uname,udate,gsttype,vatdesc,gstdesc,isblock,opqty,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }

            //sourcetable = olddbname + ".dbo.ItemMaster1";
            //destinationtable = newdbname + ".dbo.ItemMaster1";
            //cmd1 = new SqlCommand("insert into " + destinationtable + " select itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear from " + sourcetable + "", con);
            //con.Open();
            //cmd1.ExecuteNonQuery();
            //con.Close();

            sourcetable = olddbname + ".dbo.MiscMaster";
            destinationtable = newdbname + ".dbo.MiscMaster";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select type,name,uname,udate from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();

            sourcetable = olddbname + ".dbo.pagename";
            destinationtable = newdbname + ".dbo.pagename";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select pagename,page,remark from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();

            sourcetable = olddbname + ".dbo.TaxMaster";
            destinationtable = newdbname + ".dbo.TaxMaster";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select typename,vatp,addtaxp,centralsalep,selltaxp,surchargep,servicetaxp,form,uname,udate,vatdesc,addtaxdesc,cstdesc,servicetaxdesc from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();

            if (chktoolstock.Checked == true)
            {
                sourcetable = olddbname + ".dbo.ToolsMaster";
                destinationtable = newdbname + ".dbo.ToolsMaster";
                cmd1 = new SqlCommand("insert into " + destinationtable + " select name,unit,rate,opstock,inward,outward,clstock,cno,uname,udate,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }

            sourcetable = olddbname + ".dbo.userrights";
            destinationtable = newdbname + ".dbo.userrights";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select page,pagename,remark,uadd,uedit,udelete,uview,role from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();

            sourcetable = olddbname + ".dbo.SalesmanMaster";
            destinationtable = newdbname + ".dbo.SalesmanMaster";
            cmd1 = new SqlCommand("insert into " + destinationtable + " select name,mobileno,emailid,address,cno,uname,udate from " + sourcetable + "", con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            string ayear1 = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            SqlCommand cmdy1 = new SqlCommand("update ItemMaster set acyear='" + ayear1 + "'", con111);
            con111.Open();
            cmdy1.ExecuteNonQuery();
            con111.Close();
            SqlCommand cmdy2 = new SqlCommand("update ItemMaster1 set acyear='" + ayear1 + "'", con111);
            con111.Open();
            cmdy2.ExecuteNonQuery();
            con111.Close();
            SqlCommand cmdy3 = new SqlCommand("update ToolsMaster set acyear='" + ayear1 + "'", con111);
            con111.Open();
            cmdy3.ExecuteNonQuery();
            con111.Close();

            //return;
            //if (onetable != "customermaster" && onetable != "dealermaster" && onetable != "itemmaster" && onetable != "miscmaster" && onetable != "workermaster")
            //{
            //    cmdd = new SqlCommand("truncate table " + onetable + "", con111);
            //    con111.Open();
            //    cmdd.ExecuteNonQuery();
            //    con111.Close();
            //}
            //}
            //else
            //{
            //    SqlCommand cmd123 = new SqlCommand("Select * into " + destinationtable + " from " + sourcetable + " where issold='N'", con);
            //    con.Open();
            //    cmd123.ExecuteNonQuery();
            //    con.Close();

            //    cmdd = new SqlCommand("truncate table " + onetable + "", con111);
            //    con111.Open();
            //    cmdd.ExecuteNonQuery();
            //    con111.Close();

            //    string as1 = newdbname + ".dbo.barcode";
            //    string as2 = olddbname + ".dbo.barcode";
            //    cmd1 = new SqlCommand("INSERT INTO " + as1 + " (refid,barcodeno,voucherno,tdate,brand,product,itemname,style,size,colour,whrate,mrp,shopid,issold,periodfrom,periodto,username,udate,reason,isreturn) SELECT refid,barcodeno,voucherno,tdate,brand,product,itemname,style,size,colour,whrate,mrp,shopid,issold,periodfrom,periodto,username,udate,reason,isreturn FROM " + as2 + " where issold='N'", con);
            //    con.Open();
            //    cmd1.ExecuteNonQuery();
            //    con.Close();
            //}
            //}
            //return;
            //Truncating Table in which data not need to forward in new year
            //SqlCommand cmdsp = new SqlCommand("yearendtruncate", con111);
            //cmdsp.CommandType = CommandType.StoredProcedure;
            //con111.Open();
            //cmdsp.ExecuteNonQuery();
            //con111.Close();

            //////////SqlCommand cmdx1 = new SqlCommand("truncate table activity", con111);
            //////////con111.Open();
            //////////cmdx1.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx2 = new SqlCommand("truncate table BankACMaster", con111);
            //////////con111.Open();
            //////////cmdx2.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx3 = new SqlCommand("truncate table BankACMaster1", con111);
            //////////con111.Open();
            //////////cmdx3.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx4 = new SqlCommand("truncate table CommInvMaster", con111);
            //////////con111.Open();
            //////////cmdx4.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx5 = new SqlCommand("truncate table CommInvItems", con111);
            //////////con111.Open();
            //////////cmdx5.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx6 = new SqlCommand("truncate table ledger", con111);
            //////////con111.Open();
            //////////cmdx6.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx7 = new SqlCommand("truncate table Ledger_e", con111);
            //////////con111.Open();
            //////////cmdx7.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx8 = new SqlCommand("truncate table PCItems", con111);
            //////////con111.Open();
            //////////cmdx8.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx9 = new SqlCommand("truncate table PCMaster", con111);
            //////////con111.Open();
            //////////cmdx9.ExecuteNonQuery();
            //////////con111.Close();

            //////////SqlCommand cmdx10 = new SqlCommand("truncate table PerInvMaster", con111);
            //////////con111.Open();
            //////////cmdx10.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx11 = new SqlCommand("truncate table PerInvItems", con111);
            //////////con111.Open();
            //////////cmdx11.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx12 = new SqlCommand("truncate table PIItems", con111);
            //////////con111.Open();
            //////////cmdx12.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx13 = new SqlCommand("truncate table PIMaster", con111);
            //////////con111.Open();
            //////////cmdx13.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx14 = new SqlCommand("truncate table pmitems", con111);
            //////////con111.Open();
            //////////cmdx14.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx15 = new SqlCommand("truncate table pmmaster", con111);
            //////////con111.Open();
            //////////cmdx15.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx16 = new SqlCommand("truncate table project1", con111);
            //////////con111.Open();
            //////////cmdx16.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx17 = new SqlCommand("truncate table project2", con111);
            //////////con111.Open();
            //////////cmdx17.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx18 = new SqlCommand("truncate table PurchaseOrder", con111);
            //////////con111.Open();
            //////////cmdx18.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx19 = new SqlCommand("truncate table PurchaseOrderItems", con111);
            //////////con111.Open();
            //////////cmdx19.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx20 = new SqlCommand("truncate table QuoMaster", con111);
            //////////con111.Open();
            //////////cmdx20.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx21 = new SqlCommand("truncate table QuoItems", con111);
            //////////con111.Open();
            //////////cmdx21.ExecuteNonQuery();
            //////////con111.Close();

            //////////SqlCommand cmdx22 = new SqlCommand("truncate table rrmaster", con111);
            //////////con111.Open();
            //////////cmdx22.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx23 = new SqlCommand("truncate table rritems", con111);
            //////////con111.Open();
            //////////cmdx23.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx24 = new SqlCommand("truncate table SalesOrder", con111);
            //////////con111.Open();
            //////////cmdx24.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx25 = new SqlCommand("truncate table SalesOrderItems", con111);
            //////////con111.Open();
            //////////cmdx25.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx26 = new SqlCommand("truncate table SCMaster", con111);
            //////////con111.Open();
            //////////cmdx26.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx27 = new SqlCommand("truncate table SCItems", con111);
            //////////con111.Open();
            //////////cmdx27.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx28 = new SqlCommand("truncate table SIMaster", con111);
            //////////con111.Open();
            //////////cmdx28.ExecuteNonQuery();
            //////////con111.Close();

            //////////SqlCommand cmdx29 = new SqlCommand("truncate table SIItems", con111);
            //////////con111.Open();
            //////////cmdx29.ExecuteNonQuery();
            //////////con111.Close();

            //////////SqlCommand cmdx30 = new SqlCommand("truncate table StockJVMaster", con111);
            //////////con111.Open();
            //////////cmdx30.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx31 = new SqlCommand("truncate table StockJVItems", con111);
            //////////con111.Open();
            //////////cmdx31.ExecuteNonQuery();
            //////////con111.Close();

            //////////SqlCommand cmdx32 = new SqlCommand("truncate table ToolDelItem", con111);
            //////////con111.Open();
            //////////cmdx32.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx33 = new SqlCommand("truncate table ToolDelMaster", con111);
            //////////con111.Open();
            //////////cmdx33.ExecuteNonQuery();
            //////////con111.Close();
            ////////////SqlCommand cmdx34 = new SqlCommand("truncate table ToolsJVMaster", con111);
            ////////////con111.Open();
            ////////////cmdx34.ExecuteNonQuery();
            ////////////con111.Close();
            ////////////SqlCommand cmdx35 = new SqlCommand("truncate table ToolsJVItems", con111);
            ////////////con111.Open();
            ////////////cmdx35.ExecuteNonQuery();
            ////////////con111.Close();

            //////////SqlCommand cmdx36 = new SqlCommand("truncate table tempsalestax", con111);
            //////////con111.Open();
            //////////cmdx36.ExecuteNonQuery();
            //////////con111.Close();
            //////////SqlCommand cmdx37 = new SqlCommand("truncate table tempsheet", con111);
            //////////con111.Open();
            //////////cmdx37.ExecuteNonQuery();
            //////////con111.Close();

            //Carry forwarding Balance Sheet required data in to new year
            if (chkbalancesheet.Checked == true)
            {
                Session["dtpitemsyearend"] = CreateTemplate1();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.fromdate = "01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0];
                li.todate = "31-03-" + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]) + 1).ToString();
                li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                string ftow = "";
                SqlDataAdapter dada = new SqlDataAdapter("select debitcode from Ledger where cno=" + li.cno + " and voucherdate between @fromdate and @todate group by debitcode order by debitcode", con);
                dada.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dada.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtac = new DataTable();
                dada.Fill(dtac);
                DataTable dtq = new DataTable();
                for (int cc = 0; cc < dtac.Rows.Count; cc++)
                {
                    li.acname = dtac.Rows[cc]["debitcode"].ToString().Trim();
                    SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                    dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                    DataTable dtop = new DataTable();
                    dc.Fill(dtop);

                    SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                    dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                    DataTable dtop1 = new DataTable();
                    dc1.Fill(dtop1);


                    SqlDataAdapter dc2 = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                    dc2.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    DataTable dtop2 = new DataTable();
                    dc2.Fill(dtop2);

                    SqlDataAdapter dc12 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                    dc12.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    DataTable dtop12 = new DataTable();
                    dc12.Fill(dtop12);

                    SqlDataAdapter dc11 = new SqlDataAdapter("select * from ledger where cno=" + li.cno + " and debitcode=@acname and istype='OP'", con);
                    dc11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    DataTable dtop11 = new DataTable();
                    dc11.Fill(dtop11);

                    SqlDataAdapter dc112 = new SqlDataAdapter("select * from ACMaster where cno=" + li.cno + " and acname=@acname", con);
                    dc112.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    DataTable dtac1 = new DataTable();
                    dc112.Fill(dtac1);
                    string poscrdr = "";
                    string negcrdr = "";
                    string posgcode = "";
                    string neggcode = "";
                    string groupheadcode = "";
                    string groupheadcode1 = "";
                    if (dtac1.Rows.Count > 0)
                    {
                        poscrdr = dtac1.Rows[0]["debitgroupcode"].ToString();
                        negcrdr = dtac1.Rows[0]["creditgroupcode"].ToString();
                    }
                    double opbal = 0;
                    if (dtop11.Rows.Count > 0)
                    {
                        if (dtop11.Rows[0]["amount"].ToString() != "" && dtop11.Rows[0]["amount"].ToString() != string.Empty)
                        {
                            opbal = Convert.ToDouble(dtop11.Rows[0]["amount"].ToString()) + Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                        }
                    }
                    else
                    {
                        opbal = Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                    }
                    double zero = 0;
                    if (opbal >= 0)
                    {
                        dtq = (DataTable)Session["dtpitemsyearend"];
                        double camt = 0;
                        double damt = 0;
                        double closingbalance = opbal;
                        DataRow dr = dtq.NewRow();
                        dr["acname"] = li.acname;
                        dr["opening"] = opbal;
                        if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                            damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        }
                        if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                            camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        }
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr"] = "";
                            }
                            else
                            {
                                dr["crdr"] = poscrdr;
                            }
                        }
                        else
                        {
                            dr["crdr"] = negcrdr;
                        }
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr1"] = "";
                            }
                            else
                            {
                                dr["crdr1"] = "Dr";
                            }
                        }
                        else
                        {
                            dr["crdr1"] = "Cr";
                        }
                        dtq.Rows.Add(dr);
                        Session["dtpitemsyearend"] = dtq;
                    }
                    else
                    {
                        dtq = (DataTable)Session["dtpitemsyearend"];
                        double camt = 0;
                        double damt = 0;
                        double closingbalance = opbal;
                        DataRow dr = dtq.NewRow();
                        dr["acname"] = li.acname;
                        dr["opening"] = opbal;
                        if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                            damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        }
                        if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                            camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        }
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr"] = "";
                            }
                            else
                            {
                                dr["crdr"] = poscrdr;
                            }
                        }
                        else
                        {
                            dr["closingbal"] = closingbalance * (-1);
                            dr["crdr"] = negcrdr;
                        }
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr1"] = "";
                            }
                            else
                            {
                                dr["crdr1"] = "Dr";
                            }
                        }
                        else
                        {
                            dr["crdr1"] = "Cr";
                        }
                        dtq.Rows.Add(dr);
                        Session["dtpitemsyearend"] = dtq;
                    }
                }
                SqlCommand cmd1sa = new SqlCommand("delete from tempsheet", con);
                con.Open();
                cmd1sa.ExecuteNonQuery();
                con.Close();
                for (int f = 0; f < dtq.Rows.Count; f++)
                {
                    li.acname = dtq.Rows[f]["acname"].ToString();
                    li.closingbal = Convert.ToDouble(dtq.Rows[f]["closingbal"].ToString());
                    if (li.closingbal < 0)
                    {
                        li.closingbal = li.closingbal * (-1);
                    }
                    li.crdr = dtq.Rows[f]["crdr1"].ToString();
                    li.groupcode = dtq.Rows[f]["crdr"].ToString();
                    //if (Convert.ToDouble(li.closingbal) > 0)
                    //{
                    SqlCommand cmd = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values ('" + li.acname + "','" + li.groupcode + "','" + Math.Round(li.closingbal, 2) + "','" + li.crdr + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //}
                }
                //BS Counting
                DataTable dd1p = new DataTable();
                DataTable dd2p = new DataTable();
                SqlDataAdapter dabs1ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 100 and 199", con);
                dabs1ap.Fill(dd1p);
                SqlDataAdapter dabs2ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 200 and 299", con);
                dabs2ap.Fill(dd2p);
                li.amount = Math.Round((Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "GROSS LOSS";
                    li.code = "301";
                    li.type = "Cr";

                }
                else
                {
                    li.acname = "B/F TO TRADING A/C";
                    li.code = "401";
                    li.type = "Dr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmdap = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmdap.Parameters.AddWithValue("@acname", li.acname);
                cmdap.Parameters.AddWithValue("@code", li.code);
                cmdap.Parameters.AddWithValue("@amount", li.amount);
                cmdap.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmdap.ExecuteNonQuery();
                con.Close();



                DataTable dd11 = new DataTable();
                DataTable dd21 = new DataTable();
                SqlDataAdapter dabs1a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 300 and 399", con);
                dabs1a1.Fill(dd11);
                SqlDataAdapter dabs2a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 400 and 499", con);
                dabs2a1.Fill(dd21);
                li.amount = Math.Round((Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "NET LOSS";
                    li.code = "501";
                    li.type = "Dr";
                }
                else
                {
                    li.acname = "NET PROFIT";
                    li.code = "601";
                    li.type = "Cr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda1 = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda1.Parameters.AddWithValue("@acname", li.acname);
                cmda1.Parameters.AddWithValue("@code", li.code);
                cmda1.Parameters.AddWithValue("@amount", li.amount);
                cmda1.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda1.ExecuteNonQuery();
                con.Close();
                //
                //SqlCommand cmd1saa = new SqlCommand("delete from tempsheet", con111);
                //con111.Open();
                //cmd1saa.ExecuteNonQuery();
                //con111.Close();
                SqlDataAdapter dda = new SqlDataAdapter("select * from tempsheet where code between 500 and 699", con);// and clbal<>0
                DataTable dtolddata = new DataTable();
                dda.Fill(dtolddata);
                for (int q = 0; q < dtolddata.Rows.Count; q++)
                {
                    li.voucherno = 0;
                    //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.strvoucherno = "0";
                    li.refid = 0;
                    li.debitcode = dtolddata.Rows[q]["acname"].ToString();
                    li.creditcode = "Opening Balance";
                    li.description = "OP";
                    double camt = 0;
                    if (dtolddata.Rows[q]["acname"].ToString() == "Profit & Loss A/c.")
                    {
                        camt = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Cr")
                        {
                            camt = camt * (-1);
                        }
                        SqlDataAdapter dapl = new SqlDataAdapter("select * from tempsheet where acname='NET PROFIT'", con);
                        DataTable dtpl = new DataTable();
                        dapl.Fill(dtpl);
                        if (dtpl.Rows.Count > 0)
                        {
                            if (dtpl.Rows[0]["crdr"].ToString() == "Cr")
                            {
                                camt = camt + (Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString()) * (-1));
                            }
                            else
                            {
                                camt = camt + Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString());
                            }
                        }
                        if (camt >= 0)
                        {
                            li.istype1 = "D";
                            li.amount = camt;
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = camt;
                        }
                    }
                    else
                    {
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Dr")
                        {
                            li.istype1 = "D";
                            li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString()) * (-1);
                        }
                    }
                    li.ccode = 0;
                    li.istype = "OP";
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    //string ayear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                    if (dtolddata.Rows[q]["acname"].ToString() != "NET PROFIT")
                    {
                        SqlCommand cmdd = new SqlCommand();
                        cmdd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate,@acyear)", con111);
                        cmdd.Parameters.AddWithValue("@voucherno", li.voucherno);
                        cmdd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
                        cmdd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
                        cmdd.Parameters.AddWithValue("@refid", li.refid);
                        cmdd.Parameters.AddWithValue("@debitcode", li.debitcode);
                        cmdd.Parameters.AddWithValue("@creditcode", li.creditcode);
                        cmdd.Parameters.AddWithValue("@description", li.description);
                        cmdd.Parameters.AddWithValue("@projectcode", li.ccode);
                        cmdd.Parameters.AddWithValue("@amount", li.amount);
                        cmdd.Parameters.AddWithValue("@istype", li.istype);
                        cmdd.Parameters.AddWithValue("@type", li.istype1);
                        cmdd.Parameters.AddWithValue("@cno", li.cno);
                        cmdd.Parameters.AddWithValue("@uname", li.uname);
                        cmdd.Parameters.AddWithValue("@udate", li.udate);
                        cmdd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmdd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                string ayear1a = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                SqlDataAdapter daremain = new SqlDataAdapter("select * from acmaster where acname not in (select acname from ACMaster inner join Ledger on acmaster.acname=Ledger.debitcode where Ledger.istype='OP')", con111);
                DataTable dtremain = new DataTable();
                daremain.Fill(dtremain);
                for (int gh = 0; gh < dtremain.Rows.Count; gh++)
                {
                    li.voucherno = 0;
                    //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.strvoucherno = "0";
                    li.refid = 0;
                    li.debitcode = dtremain.Rows[gh]["acname"].ToString();
                    li.creditcode = "Opening Balance";
                    li.description = "OP";
                    li.istype1 = "D";
                    li.amount = 0;
                    li.ccode = 0;
                    li.istype = "OP";
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    SqlCommand cmdd = new SqlCommand();
                    cmdd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate,@acyear)", con111);
                    cmdd.Parameters.AddWithValue("@voucherno", li.voucherno);
                    cmdd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
                    cmdd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
                    cmdd.Parameters.AddWithValue("@refid", li.refid);
                    cmdd.Parameters.AddWithValue("@debitcode", li.debitcode);
                    cmdd.Parameters.AddWithValue("@creditcode", li.creditcode);
                    cmdd.Parameters.AddWithValue("@description", li.description);
                    cmdd.Parameters.AddWithValue("@projectcode", li.ccode);
                    cmdd.Parameters.AddWithValue("@amount", li.amount);
                    cmdd.Parameters.AddWithValue("@istype", li.istype);
                    cmdd.Parameters.AddWithValue("@type", li.istype1);
                    cmdd.Parameters.AddWithValue("@cno", li.cno);
                    cmdd.Parameters.AddWithValue("@uname", li.uname);
                    cmdd.Parameters.AddWithValue("@udate", li.udate);
                    cmdd.Parameters.AddWithValue("@acyear", ayear1a);
                    con111.Open();
                    cmdd.ExecuteNonQuery();
                    con111.Close();
                }
                SqlCommand cmdremove = new SqlCommand("delete from Ledger where debitcode not in (select acname from ACMaster inner join Ledger on acmaster.acname=Ledger.debitcode where Ledger.istype='OP') and istype='OP'", con111);
                con111.Open();
                cmdremove.ExecuteNonQuery();
                con111.Close();
            }
            string acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[1] + "-" + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]) + 1).ToString();
            SqlDataAdapter dayr = new SqlDataAdapter("select * from acyear where acyear='" + acyear + "'", con);
            DataTable dtyr = new DataTable();
            dayr.Fill(dtyr);
            if (dtyr.Rows.Count == 0)
            {
                SqlCommand cmdsa = new SqlCommand("insert into acyear (acyear,uname,udate) values (@acyear,@uname,@udate)", con);
                cmdsa.Parameters.AddWithValue("@acyear", acyear);
                cmdsa.Parameters.AddWithValue("@uname", li.uname);
                cmdsa.Parameters.AddWithValue("@udate", li.udate);
                con.Open();
                cmdsa.ExecuteNonQuery();
                con.Close();
            }
            //

            //Cary forwarding Item & Tools OP qty
            if (chkitemstock.Checked == true)
            {
                DataTable dtallstock = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter("select distinct(itemname) from itemmaster order by itemname", con);
                DataTable dtitem = new DataTable();
                da.Fill(dtitem);
                for (int c = 0; c < dtitem.Rows.Count; c++)
                {
                    li.itemname = dtitem.Rows[c]["itemname"].ToString();
                    SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
                    dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
                    DataTable dtstock = new DataTable();
                    dac.Fill(dtstock);
                    dtallstock.Merge(dtstock);
                }
                for (int f = 0; f < dtallstock.Rows.Count; f++)
                {
                    SqlCommand cmditem = new SqlCommand("update Itemmaster set opqty=" + Math.Round(Convert.ToDouble(dtallstock.Rows[f]["totalqty"].ToString()), 2) + " where itemname='" + dtallstock.Rows[f]["itemname"].ToString() + "'", con111);
                    con111.Open();
                    cmditem.ExecuteNonQuery();
                    con111.Close();
                }
            }

            if (chktoolstock.Checked == true)
            {
                DataTable dtallstockt = new DataTable();
                SqlDataAdapter dat = new SqlDataAdapter("select distinct(name) from toolsmaster order by name", con);
                DataTable dtitemt = new DataTable();
                dat.Fill(dtitemt);
                for (int c = 0; c < dtitemt.Rows.Count; c++)
                {
                    li.name = dtitemt.Rows[c]["name"].ToString();
                    SqlDataAdapter dac = new SqlDataAdapter("select name as toolname,(select isnull(sum(opstock),0) from toolsmaster where name=@itemname) as opqty,(select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname) as tots,(select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname) as totp,(((select isnull(sum(opstock),0) from ToolsMaster where name=@itemname)+(select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname))-(select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname)) as totalqty from ToolsMaster where name=@itemname", con);
                    dac.SelectCommand.Parameters.AddWithValue("@itemname", li.name);
                    DataTable dtstock = new DataTable();
                    dac.Fill(dtstock);
                    dtallstockt.Merge(dtstock);
                }
                for (int f = 0; f < dtallstockt.Rows.Count; f++)
                {
                    SqlCommand cmditem = new SqlCommand("update toolsmaster set opstock=" + Math.Round(Convert.ToDouble(dtallstockt.Rows[f]["totalqty"].ToString()), 2) + " where name='" + dtallstockt.Rows[f]["toolname"].ToString() + "'", con111);
                    con111.Open();
                    cmditem.ExecuteNonQuery();
                    con111.Close();
                }
            }
            //

            //Carry Item stock pricewise
            if (chkitemstockpricewise.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete from itemmaster1 where acyear='" + ayear + "'", con111);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter dac = new SqlDataAdapter("select PCitems.pcno,Pcitems.id,PCItems.itemname,ItemMaster.unit,((PCItems.stockqty)-(isnull(PCItems.adjqty,0))) as qty,PCItems.rate,(PCItems.rate*((PCItems.stockqty)-(isnull(PCItems.adjqty,0)))) as amount from PCItems inner join ItemMaster on ItemMaster.itemname=PCItems.itemname where ((PCItems.qty)-(isnull(PCItems.adjqty,0)))>0 and PCItems.pcdate between @fromdate and @todate order by PCItems.itemname", con);
                dac.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtstock = new DataTable();
                dac.Fill(dtstock);

                SqlDataAdapter dac11 = new SqlDataAdapter("select StockJVItems.id,StockJVItems.itemname,ItemMaster.unit,((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0))) as qty,StockJVItems.rate,(StockJVItems.rate*((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))) as amount from StockJVItems inner join ItemMaster on ItemMaster.itemname=StockJVItems.itemname where ((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))>0 and StockJVItems.inout='IN' and StockJVItems.challandate between @fromdate and @todate order by StockJVItems.itemname", con);
                dac11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtstock1 = new DataTable();
                dac11.Fill(dtstock1);
                //dtstock.Merge(dtstock1);
                //imageDataSet.Tables["DataTable16"].Merge(dtstock1);
                DataTable dtopstock = new DataTable();
                SqlDataAdapter dac1 = new SqlDataAdapter("select ItemMaster1.id,ItemMaster1.itemname,ItemMaster.unit,((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0))) as qty,ItemMaster1.rate,(ItemMaster1.rate*((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0)))) as amount from ItemMaster1 inner join ItemMaster on ItemMaster.itemname=ItemMaster1.itemname where ((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0)))>0 and ItemMaster1.opdate between @fromdate and @todate order by ItemMaster1.itemname", con);
                dac1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                dac1.Fill(dtopstock);
                for (int d = 0; d < dtstock.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.id = Convert.ToInt64(dtstock.Rows[d]["id"].ToString());
                    li.itemname = dtstock.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtstock.Rows[d]["qty"].ToString());
                    qty = Convert.ToDouble(dtstock.Rows[d]["qty"].ToString());
                    SqlDataAdapter dap = new SqlDataAdapter("select * from PIItems where vid=" + li.id + "", con);
                    DataTable dtpi = new DataTable();
                    dap.Fill(dtpi);
                    if (dtpi.Rows.Count == 0)
                    {
                        li.rate = Convert.ToDouble(dtstock.Rows[d]["rate"].ToString());
                        li.unit = dtstock.Rows[d]["unit"].ToString();
                        li.amount = Convert.ToDouble(dtstock.Rows[d]["amount"].ToString());
                    }
                    else
                    {
                        li.rate = Convert.ToDouble(dtpi.Rows[0]["rate"].ToString());
                        li.unit = dtstock.Rows[d]["unit"].ToString();
                        rate = Convert.ToDouble(dtpi.Rows[0]["rate"].ToString());
                        li.amount = Math.Round(qty * rate, 2);
                    }
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                for (int d = 0; d < dtstock1.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.id = Convert.ToInt64(dtstock1.Rows[d]["id"].ToString());
                    li.itemname = dtstock1.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtstock1.Rows[d]["qty"].ToString());
                    qty = Convert.ToDouble(dtstock1.Rows[d]["qty"].ToString());
                    li.rate = Convert.ToDouble(dtstock1.Rows[d]["rate"].ToString());
                    li.unit = "";
                    li.amount = Math.Round((qty * li.rate), 2);
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                for (int d = 0; d < dtopstock.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.itemname = dtopstock.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtopstock.Rows[d]["qty"].ToString());
                    li.rate = Convert.ToDouble(dtopstock.Rows[d]["rate"].ToString());
                    li.unit = dtopstock.Rows[d]["unit"].ToString();
                    li.amount = Convert.ToDouble(dtopstock.Rows[d]["amount"].ToString());
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
            }
            //

            //Carry Pendong PO
            if (chkpendingpo.Checked == true)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                string id = "";
                SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.cno=@cno order by PurchaseOrder.podate,PurchaseOrder.pono", con);
                da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                DataTable dtc = new DataTable();
                da11.Fill(dtc);
                string strpono = "";
                if (dtc.Rows.Count > 0)
                {
                    for (int c = 0; c < dtc.Rows.Count; c++)
                    {
                        li.id = Convert.ToInt64(dtc.Rows[c]["id"].ToString());
                        li.qty = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                        if (dtc.Rows[c]["adjqty"].ToString() != string.Empty)
                        {
                            li.adjqty = Convert.ToDouble(dtc.Rows[c]["adjqty"].ToString());
                        }
                        else
                        {
                            li.adjqty = 0;
                        }
                        SqlDataAdapter da11c = new SqlDataAdapter("select isnull(sum(qty) ,0) as totqty from PCItems where vid=@id and cno=@cno", con);
                        da11c.SelectCommand.Parameters.AddWithValue("@id", li.id);
                        da11c.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                        DataTable dtcc = new DataTable();
                        da11c.Fill(dtcc);
                        if (dtcc.Rows.Count > 0)
                        {
                            li.qtypc = Convert.ToDouble(dtcc.Rows[0]["totqty"].ToString());
                            li.qtyremain = li.qty - (li.qtypc + li.adjqty);
                            if (li.qtyremain > 0)
                            {
                                id = id + li.id + ",";
                                strpono = strpono + "'" + dtc.Rows[c]["pono"].ToString() + "',";
                            }
                        }
                        else
                        {
                            id = id + li.id + ",";
                            strpono = strpono + "'" + dtc.Rows[c]["pono"].ToString() + "',";
                        }
                        //SqlDataAdapter dac = new SqlDataAdapter("select * from PurchaseOrder where pono in (" + strpono + ")", con);
                        //DataTable dtpo1 = new DataTable();
                        //dac.Fill(dtpo1);
                        //for (int x = 0; x < dtpo1.Rows.Count; x++)
                        //{
                        //    li.strpono = dtpo1.Rows[x]["pono"].ToString();
                        //    li.podate = Convert.ToDateTime(dtpo1.Rows[x]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                        //}                        

                        //SqlDataAdapter dac1 = new SqlDataAdapter("select * from PurchaseOrderItems where id in (" + id + ")", con);
                        //DataTable dtpo11 = new DataTable();
                        //dac1.Fill(dtpo11);
                        //for (int x = 0; x < dtpo11.Rows.Count; x++)
                        //{ }
                        //SqlCommand cmd=new SqlCommand("insert into table2 select * FROM table1 where condition;",con111);
                    }
                    SqlCommand cmds = new SqlCommand("INSERT INTO dbo.PurchaseOrder SELECT * FROM " + olddbname + ".dbo.PurchaseOrder where pono in (" + strpono.TrimEnd(',') + ")", con111);
                    con111.Open();
                    cmds.ExecuteNonQuery();
                    con111.Close();

                    SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.PurchaseOrderItems SELECT pono,podate,itemname,qty,stockqty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,iscame,vid,adjqty FROM " + olddbname + ".dbo.PurchaseOrderItems where id in (" + id.TrimEnd(',') + ")", con111);
                    con111.Open();
                    cmds1.ExecuteNonQuery();
                    con111.Close();
                }
            }
            //

            //Carry Pending SC
            if (chkpendingsc.Checked == true)
            {
                string strscno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SCMaster SELECT * FROM " + olddbname + ".dbo.SCMaster where status='Open' and ctype<>'RM'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select (ctype+convert(varchar(50),scno)) as strscno from SCMaster where status='Open' and ctype<>'RM'", con);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    strscno = strscno + "'" + dtf.Rows[f]["strscno"].ToString() + "',";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SCMaster SELECT " + olddbname + ".dbo.SCMaster.* FROM " + olddbname + ".dbo.SCMaster inner join " + olddbname + ".dbo.SCItems on " + olddbname + ".dbo.SCMaster.strscno=" + olddbname + ".dbo.SCItems.strscno where " + olddbname + ".dbo.SCMaster.strscno in (" + strscno + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SCItems SELECT scno,scdate,itemname,qty,stockqty,rate,basicamount,unit,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,ccode,qtyremain,qtyused,vno,vid,strscno,adjqty,remarks,rfs FROM " + olddbname + ".dbo.SCItems where " + olddbname + ".dbo.SCItems.strscno in (" + strscno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();

            }
            //

            //Carry Pending Signed SC
            if (chkpendingsignsc.Checked == true)
            {
                string strscno = "";//select * from " + olddbname + ".dbo.SCMaster where issigned='No'
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SCMaster select * from " + olddbname + ".dbo.SCMaster where issigned='No' and status<>'Open'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select (ctype+convert(varchar(50),scno)) as strscno from SCMaster where issigned='No' and status<>'Open'", con);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    strscno = strscno + "'" + dtf.Rows[f]["strscno"].ToString() + "',";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SCMaster SELECT " + olddbname + ".dbo.SCMaster.* FROM " + olddbname + ".dbo.SCMaster inner join " + olddbname + ".dbo.SCItems on " + olddbname + ".dbo.SCMaster.strscno=" + olddbname + ".dbo.SCItems.strscno where " + olddbname + ".dbo.SCMaster.strscno in (" + strscno + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SCItems SELECT scno,scdate,itemname,qty,stockqty,rate,basicamount,unit,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,ccode,qtyremain,qtyused,vno,vid,strscno,adjqty,remarks,rfs FROM " + olddbname + ".dbo.SCItems where " + olddbname + ".dbo.SCItems.strscno in (" + strscno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();

            }
            //

            //Carry Pending PC
            if (chkpendingpc.Checked == true)
            {
                string pcno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.PCMaster SELECT * FROM " + olddbname + ".dbo.PCMaster where status='Open'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select * from PCMaster where status='Open'", con);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    pcno = pcno + dtf.Rows[f]["pcno"].ToString() + ",";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.PCMaster SELECT " + olddbname + ".dbo.PCMaster.* FROM " + olddbname + ".dbo.PCMaster inner join " + olddbname + ".dbo.PCItems on " + olddbname + ".dbo.PCMaster.pcno=" + olddbname + ".dbo.PCItems.pcno where " + olddbname + ".dbo.PCMaster.pcno in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.PCItems SELECT pcno,pcdate,itemname,qty,stockqty,rate,basicamount,ccode,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,qtyremain,qtyused,vno,vid,adjqty,unit FROM " + olddbname + ".dbo.PCItems where " + olddbname + ".dbo.PCItems.pcno in (" + pcno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();
            }
            //

            //Carry Pending PC
            if (chkpendingso.Checked == true)
            {
                string pcno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SalesOrder SELECT " + olddbname + ".dbo.SalesOrder.* FROM " + olddbname + ".dbo.SalesOrder inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.SalesOrder.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select * from SalesOrder inner join ClientMaster on SalesOrder.ccode=ClientMaster.code where ClientMaster.status='Running'", con);//SalesOrder.status='Open' and 
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    pcno = pcno + dtf.Rows[f]["sono"].ToString() + ",";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SalesOrder SELECT " + olddbname + ".dbo.SalesOrder.* FROM " + olddbname + ".dbo.SalesOrder inner join " + olddbname + ".dbo.SalesOrderItems on " + olddbname + ".dbo.SalesOrder.sono=" + olddbname + ".dbo.SalesOrderItems.sono where " + olddbname + ".dbo.SalesOrder.sono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SalesOrderItems SELECT sono,sodate,itemname,qty,stockqty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,qtyremain1,qtyused1,vid,firstquotation,technicaldrawing,rewisequotation,finaltech,lastfollowupdate,pendingorderlying,okorderdate,tentativedeliverydate,deliverydate,remarks FROM " + olddbname + ".dbo.SalesOrderItems where " + olddbname + ".dbo.SalesOrderItems.sono in (" + pcno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();
            }
            //

            //Carry Pending PC
            if (chkpendingquo.Checked == true)
            {
                string pcno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.QuoMaster.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select * from QuoMaster inner join ClientMaster on ClientMaster.code=QuoMaster.ccode where ClientMaster.status='Running'", con);//QuoMaster.status='Open' and 
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    pcno = pcno + dtf.Rows[f]["quono"].ToString() + ",";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.QuoItems on " + olddbname + ".dbo.QuoMaster.quono=" + olddbname + ".dbo.QuoItems.quono where " + olddbname + ".dbo.QuoMaster.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.QuoItems SELECT srno,quono,quodate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,make,hsncode FROM " + olddbname + ".dbo.QuoItems where " + olddbname + ".dbo.QuoItems.quono in (" + pcno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();
            }
            //

            //Carry Pending Cheques
            if (chkreconcile.Checked == true)
            {
                string pcno = "";
                //SqlCommand cmds = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.QuoMaster.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                //con111.Open();
                //cmds.ExecuteNonQuery();
                //con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select BankACMaster1.* from BankACMaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where (BankACMaster.cldate is null or BankACMaster.cldate='') and BankACMaster.istype in ('BR','BP')", con);//QuoMaster.status='Open' and 
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    li.istype = dtf.Rows[f]["istype"].ToString();
                    li.voucherno = Convert.ToInt64(dtf.Rows[f]["voucherno"].ToString());
                    SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.BankACMaster SELECT * FROM " + olddbname + ".dbo.BankACMaster where " + olddbname + ".dbo.BankACMaster.voucherno=" + li.voucherno + " and " + olddbname + ".dbo.BankACMaster.istype='" + li.istype + "'", con111);
                    con111.Open();
                    cmds11.ExecuteNonQuery();
                    con111.Close();

                    SqlCommand cmds111 = new SqlCommand("INSERT INTO dbo.BankACMaster1 SELECT * FROM " + olddbname + ".dbo.BankACMaster1 where " + olddbname + ".dbo.BankACMaster1.voucherno=" + li.voucherno + " and " + olddbname + ".dbo.BankACMaster1.istype='" + li.istype + "'", con111);
                    con111.Open();
                    cmds111.ExecuteNonQuery();
                    con111.Close();

                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.QuoItems on " + olddbname + ".dbo.QuoMaster.quono=" + olddbname + ".dbo.QuoItems.quono where " + olddbname + ".dbo.QuoMaster.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                //SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.QuoItems SELECT srno,quono,quodate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,make,hsncode FROM " + olddbname + ".dbo.QuoItems where " + olddbname + ".dbo.QuoItems.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds11.ExecuteNonQuery();
                //con111.Close();
            }
            //

            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('year End Process done successfully.');", true);
            return;
        }
        else
        {
            con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connma"].ConnectionString);
            cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            con = new SqlConnection(cz);
            olddbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            newdbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1] + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]) + 1).ToString();
            li.fromdate = "01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0];
            li.todate = "31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            string ayear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            SqlConnection con111 = new SqlConnection("Data Source=CHIRAG-PC;Initial Catalog=" + newdbname + ";Integrated Security=True");
            if (chkclientmaster.Checked == true)
            {
                string sourcetable = olddbname + ".dbo.ClientMaster";
                string destinationtable = newdbname + ".dbo.ClientMaster";

                SqlCommand cmdn = new SqlCommand("delete from " + destinationtable + " where acyear='" + ayear + "'", con111);
                con111.Open();
                cmdn.ExecuteNonQuery();
                con111.Close();

                SqlCommand cmd1 = new SqlCommand("insert into " + destinationtable + " select code,name,add1,add2,add3,city,pincode,mobileno,emailid,cno,uname,udate,opbalance,acname,referencename,handledby,status,remarks,pdate,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }
            if (chkbalancesheet.Checked == true)
            {
                string sourcetable = olddbname + ".dbo.ACMaster";
                string destinationtable = newdbname + ".dbo.ACMaster";
                //SqlCommand cmdn = new SqlCommand("delete from " + destinationtable + " where acyear='" + ayear + "'", con111);
                //con111.Open();
                //cmdn.ExecuteNonQuery();
                //con111.Close();

                //SqlCommand cmd1 = new SqlCommand("insert into " + destinationtable + " select acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear from " + sourcetable + "", con);
                //con.Open();
                //cmd1.ExecuteNonQuery();
                //con.Close();

                //SqlCommand cmda = new SqlCommand("delete from Ledger where istype='OP' and acyear='" + ayear + "'", con111);
                //con111.Open();
                //cmda.ExecuteNonQuery();
                //con111.Close();
                Session["dtpitemsyearend"] = CreateTemplate1();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                //li.fromdate = "01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0];
                //li.todate = "31-03-" + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]) + 1).ToString();
                //li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                string ftow = "";
                SqlDataAdapter dada = new SqlDataAdapter("select debitcode from Ledger where cno=" + li.cno + " and voucherdate between @fromdate and @todate group by debitcode order by debitcode", con);
                dada.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dada.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtac = new DataTable();
                dada.Fill(dtac);
                DataTable dtq = new DataTable();
                for (int cc = 0; cc < dtac.Rows.Count; cc++)
                {
                    li.acname = dtac.Rows[cc]["debitcode"].ToString().Trim();
                    if (li.acname == "INCOME TAX RECEIVABLE A/C.")
                    {
                        string errorsss = "";
                    }
                    SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                    dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                    DataTable dtop = new DataTable();
                    dc.Fill(dtop);

                    SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate between @fromdate and @todate", con);
                    dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                    DataTable dtop1 = new DataTable();
                    dc1.Fill(dtop1);


                    SqlDataAdapter dc2 = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where cno=" + li.cno + " and istype!='OP' and type='C' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                    dc2.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    DataTable dtop2 = new DataTable();
                    dc2.Fill(dtop2);

                    SqlDataAdapter dc12 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where cno=" + li.cno + " and istype!='OP' and type='D' and debitcode='" + li.acname + "' and voucherdate < @fromdate", con);
                    dc12.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                    DataTable dtop12 = new DataTable();
                    dc12.Fill(dtop12);

                    SqlDataAdapter dc11 = new SqlDataAdapter("select * from ledger where cno=" + li.cno + " and debitcode=@acname and istype='OP'", con);
                    dc11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    DataTable dtop11 = new DataTable();
                    dc11.Fill(dtop11);

                    SqlDataAdapter dc112 = new SqlDataAdapter("select * from ACMaster where cno=" + li.cno + " and acname=@acname", con);
                    dc112.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    DataTable dtac1 = new DataTable();
                    dc112.Fill(dtac1);
                    string poscrdr = "";
                    string negcrdr = "";
                    string posgcode = "";
                    string neggcode = "";
                    string groupheadcode = "";
                    string groupheadcode1 = "";
                    if (dtac1.Rows.Count > 0)
                    {
                        poscrdr = dtac1.Rows[0]["debitgroupcode"].ToString();
                        negcrdr = dtac1.Rows[0]["creditgroupcode"].ToString();
                    }
                    double opbal = 0;
                    if (dtop11.Rows.Count > 0)
                    {
                        if (dtop11.Rows[0]["amount"].ToString() != "" && dtop11.Rows[0]["amount"].ToString() != string.Empty)
                        {
                            opbal = Convert.ToDouble(dtop11.Rows[0]["amount"].ToString()) + Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                        }
                    }
                    else
                    {
                        opbal = Convert.ToDouble(dtop12.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtop2.Rows[0]["totcredit"].ToString());
                    }
                    double zero = 0;
                    if (opbal >= 0)
                    {
                        dtq = (DataTable)Session["dtpitemsyearend"];
                        double camt = 0;
                        double damt = 0;
                        double closingbalance = opbal;
                        DataRow dr = dtq.NewRow();
                        dr["acname"] = li.acname;
                        dr["opening"] = opbal;
                        if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                            damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        }
                        if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                            camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        }
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr"] = "";
                            }
                            else
                            {
                                dr["crdr"] = poscrdr;
                            }
                        }
                        else
                        {
                            dr["crdr"] = negcrdr;
                        }
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr1"] = "";
                            }
                            else
                            {
                                dr["crdr1"] = "Dr";
                            }
                        }
                        else
                        {
                            dr["crdr1"] = "Cr";
                        }
                        dtq.Rows.Add(dr);
                        Session["dtpitemsyearend"] = dtq;
                    }
                    else
                    {
                        dtq = (DataTable)Session["dtpitemsyearend"];
                        double camt = 0;
                        double damt = 0;
                        double closingbalance = opbal;
                        DataRow dr = dtq.NewRow();
                        dr["acname"] = li.acname;
                        dr["opening"] = opbal;
                        if (dtop1.Rows[0]["totdebit"].ToString() != string.Empty && dtop1.Rows[0]["totdebit"].ToString() != "" && dtop1.Rows[0]["totdebit"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                            damt = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString());
                        }
                        if (dtop.Rows[0]["totcredit"].ToString() != string.Empty && dtop.Rows[0]["totcredit"].ToString() != "" && dtop.Rows[0]["totcredit"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                            camt = Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        }
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString());
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr"] = "";
                            }
                            else
                            {
                                dr["crdr"] = poscrdr;
                            }
                        }
                        else
                        {
                            dr["closingbal"] = closingbalance * (-1);
                            dr["crdr"] = negcrdr;
                        }
                        if (closingbalance >= 0)
                        {
                            if (closingbalance == 0)
                            {
                                dr["crdr1"] = "";
                            }
                            else
                            {
                                dr["crdr1"] = "Dr";
                            }
                        }
                        else
                        {
                            dr["crdr1"] = "Cr";
                        }
                        dtq.Rows.Add(dr);
                        Session["dtpitemsyearend"] = dtq;
                    }
                }
                SqlCommand cmd1sa = new SqlCommand("delete from tempsheet", con);
                con.Open();
                cmd1sa.ExecuteNonQuery();
                con.Close();
                for (int f = 0; f < dtq.Rows.Count; f++)
                {
                    li.acname = dtq.Rows[f]["acname"].ToString();
                    li.closingbal = Convert.ToDouble(dtq.Rows[f]["closingbal"].ToString());
                    if (li.closingbal < 0)
                    {
                        li.closingbal = li.closingbal * (-1);
                    }
                    li.crdr = dtq.Rows[f]["crdr1"].ToString();
                    li.groupcode = dtq.Rows[f]["crdr"].ToString();
                    //if (Convert.ToDouble(li.closingbal) > 0)
                    //{
                    SqlCommand cmd = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values ('" + li.acname + "','" + li.groupcode + "','" + Math.Round(li.closingbal, 2) + "','" + li.crdr + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //}
                }
                //BS Counting
                DataTable dd1p = new DataTable();
                DataTable dd2p = new DataTable();
                SqlDataAdapter dabs1ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 100 and 199", con);
                dabs1ap.Fill(dd1p);
                SqlDataAdapter dabs2ap = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 200 and 299", con);
                dabs2ap.Fill(dd2p);
                li.amount = Math.Round((Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd2p.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd1p.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "GROSS LOSS";
                    li.code = "301";
                    li.type = "Cr";

                }
                else
                {
                    li.acname = "B/F TO TRADING A/C";
                    li.code = "401";
                    li.type = "Dr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmdap = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmdap.Parameters.AddWithValue("@acname", li.acname);
                cmdap.Parameters.AddWithValue("@code", li.code);
                cmdap.Parameters.AddWithValue("@amount", li.amount);
                cmdap.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmdap.ExecuteNonQuery();
                con.Close();



                DataTable dd11 = new DataTable();
                DataTable dd21 = new DataTable();
                SqlDataAdapter dabs1a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtd from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Debit' and tempsheet.code between 300 and 399", con);
                dabs1a1.Fill(dd11);
                SqlDataAdapter dabs2a1 = new SqlDataAdapter("select isnull(sum(tempsheet.clbal),0) as totamtc from tempsheet inner join ACGroupMaster on ACGroupMaster.groupcode=tempsheet.code inner join ACGroupMaster1 on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.type='Credit' and tempsheet.code between 400 and 499", con);
                dabs2a1.Fill(dd21);
                li.amount = Math.Round((Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) - Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                if (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) < 0)
                {
                    double cx = (Convert.ToDouble(dd21.Rows[0]["totamtc"].ToString()) * (-1));
                    li.amount = Math.Round(cx - (Convert.ToDouble(dd11.Rows[0]["totamtd"].ToString())), 2);
                }
                if (li.amount < 0)
                {
                    li.acname = "NET LOSS";
                    li.code = "501";
                    li.type = "Dr";
                }
                else
                {
                    li.acname = "NET PROFIT";
                    li.code = "601";
                    li.type = "Cr";
                }
                if (li.amount < 0)
                {
                    li.amount = li.amount * (-1);
                }
                SqlCommand cmda1 = new SqlCommand("insert into tempsheet (acname,code,clbal,crdr) values (@acname,@code,@amount,@type)", con);
                cmda1.Parameters.AddWithValue("@acname", li.acname);
                cmda1.Parameters.AddWithValue("@code", li.code);
                cmda1.Parameters.AddWithValue("@amount", li.amount);
                cmda1.Parameters.AddWithValue("@type", li.type);
                con.Open();
                cmda1.ExecuteNonQuery();
                con.Close();
                //
                //SqlCommand cmd1saa = new SqlCommand("delete from tempsheet", con111);
                //con111.Open();
                //cmd1saa.ExecuteNonQuery();
                //con111.Close();
                SqlDataAdapter dda = new SqlDataAdapter("select * from tempsheet where code between 500 and 699", con);// and clbal<>0// where code between 500 and 699
                DataTable dtolddata = new DataTable();
                dda.Fill(dtolddata);
                for (int q = 0; q < dtolddata.Rows.Count; q++)
                {
                    li.voucherno = 0;
                    //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.strvoucherno = "0";
                    li.refid = 0;
                    li.debitcode = dtolddata.Rows[q]["acname"].ToString();

                    SqlDataAdapter dar = new SqlDataAdapter("select * from ACMaster where acname='" + li.debitcode + "'", con111);
                    DataTable dtr = new DataTable();
                    dar.Fill(dtr);
                    if (dtr.Rows.Count == 0)
                    {
                        SqlCommand cmdr = new SqlCommand("insert into " + newdbname + ".dbo.ACMaster select acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear,excludetax,inexport from " + olddbname + ".dbo.ACMaster where " + olddbname + ".dbo.ACMaster.acname='" + li.debitcode + "'", con111);
                        con111.Open();
                        cmdr.ExecuteNonQuery();
                        con111.Close();
                    }

                    li.creditcode = "Opening Balance";
                    li.description = "OP";
                    double camt = 0;
                    if (dtolddata.Rows[q]["acname"].ToString() == "Profit & Loss A/c.")
                    {
                        camt = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Cr")
                        {
                            camt = camt * (-1);
                        }
                        SqlDataAdapter dapl = new SqlDataAdapter("select * from tempsheet where acname='NET PROFIT'", con);
                        DataTable dtpl = new DataTable();
                        dapl.Fill(dtpl);
                        if (dtpl.Rows.Count > 0)
                        {
                            if (dtpl.Rows[0]["crdr"].ToString() == "Cr")
                            {
                                camt = camt + (Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString()) * (-1));
                            }
                            else
                            {
                                camt = camt + Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString());
                            }
                        }
                        if (camt >= 0)
                        {
                            li.istype1 = "D";
                            li.amount = camt;
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = camt;
                        }
                        //if (dtolddata.Rows[q]["crdr"].ToString() == "Dr")
                        //{
                        //    li.istype1 = "D";
                        //    li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        //}
                        //else
                        //{
                        //    li.istype1 = "C";
                        //    li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString()) * (-1);
                        //}
                    }
                    else
                    {
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Dr")
                        {
                            li.istype1 = "D";
                            li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString()) * (-1);
                        }
                    }
                    li.ccode = 0;
                    li.istype = "OP";
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;

                    SqlCommand cmda = new SqlCommand("delete from Ledger where istype='OP' and debitcode='" + li.debitcode + "'", con111);
                    con111.Open();
                    cmda.ExecuteNonQuery();
                    con111.Close();

                    //string ayear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                    if (dtolddata.Rows[q]["acname"].ToString() != "NET PROFIT")
                    {
                        SqlCommand cmdd = new SqlCommand();
                        cmdd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate,@acyear)", con111);
                        cmdd.Parameters.AddWithValue("@voucherno", li.voucherno);
                        cmdd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
                        cmdd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
                        cmdd.Parameters.AddWithValue("@refid", li.refid);
                        cmdd.Parameters.AddWithValue("@debitcode", li.debitcode);
                        cmdd.Parameters.AddWithValue("@creditcode", li.creditcode);
                        cmdd.Parameters.AddWithValue("@description", li.description);
                        cmdd.Parameters.AddWithValue("@projectcode", li.ccode);
                        cmdd.Parameters.AddWithValue("@amount", li.amount);
                        cmdd.Parameters.AddWithValue("@istype", li.istype);
                        cmdd.Parameters.AddWithValue("@type", li.istype1);
                        cmdd.Parameters.AddWithValue("@cno", li.cno);
                        cmdd.Parameters.AddWithValue("@uname", li.uname);
                        cmdd.Parameters.AddWithValue("@udate", li.udate);
                        cmdd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmdd.ExecuteNonQuery();
                        con111.Close();
                    }
                }

                dda = new SqlDataAdapter("select * from tempsheet where code not between 500 and 699", con);// and clbal<>0// where code between 500 and 699
                dtolddata = new DataTable();
                dda.Fill(dtolddata);
                for (int q = 0; q < dtolddata.Rows.Count; q++)
                {


                    li.voucherno = 0;
                    //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.strvoucherno = "0";
                    li.refid = 0;
                    li.debitcode = dtolddata.Rows[q]["acname"].ToString();

                    SqlDataAdapter dar = new SqlDataAdapter("select * from ACMaster where acname='" + li.debitcode + "'", con111);
                    DataTable dtr = new DataTable();
                    dar.Fill(dtr);
                    if (dtr.Rows.Count == 0)
                    {
                        SqlCommand cmdr = new SqlCommand("insert into " + newdbname + ".dbo.ACMaster select acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear,excludetax,inexport from " + olddbname + ".dbo.ACMaster where " + olddbname + ".dbo.ACMaster.acname='" + li.debitcode + "'", con111);
                        con111.Open();
                        cmdr.ExecuteNonQuery();
                        con111.Close();
                    }

                    li.creditcode = "Opening Balance";
                    li.description = "OP";
                    double camt = 0;
                    if (dtolddata.Rows[q]["acname"].ToString() == "Profit & Loss A/c.")
                    {
                        camt = Convert.ToDouble(dtolddata.Rows[q]["clbal"].ToString());
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Cr")
                        {
                            camt = camt * (-1);
                        }
                        SqlDataAdapter dapl = new SqlDataAdapter("select * from tempsheet where acname='NET PROFIT'", con);
                        DataTable dtpl = new DataTable();
                        dapl.Fill(dtpl);
                        if (dtpl.Rows.Count > 0)
                        {
                            if (dtpl.Rows[0]["crdr"].ToString() == "Cr")
                            {
                                camt = camt + (Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString()) * (-1));
                            }
                            else
                            {
                                camt = camt + Convert.ToDouble(dtpl.Rows[0]["clbal"].ToString());
                            }
                        }
                        if (camt >= 0)
                        {
                            li.istype1 = "D";
                            li.amount = camt;
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = camt;
                        }
                        //if (dtolddata.Rows[q]["crdr"].ToString() == "Dr")
                        //{
                        //    li.istype1 = "D";
                        //    li.amount = 0;
                        //}
                        //else
                        //{
                        //    li.istype1 = "C";
                        //    li.amount = 0;
                        //}
                    }
                    else
                    {
                        if (dtolddata.Rows[q]["crdr"].ToString() == "Dr")
                        {
                            li.istype1 = "D";
                            li.amount = 0;
                        }
                        else
                        {
                            li.istype1 = "C";
                            li.amount = 0;
                        }
                    }
                    li.ccode = 0;
                    li.istype = "OP";
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;

                    SqlCommand cmda = new SqlCommand("delete from Ledger where istype='OP' and debitcode='" + li.debitcode + "'", con111);
                    con111.Open();
                    cmda.ExecuteNonQuery();
                    con111.Close();

                    //string ayear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                    if (dtolddata.Rows[q]["acname"].ToString() != "NET PROFIT")
                    {
                        SqlCommand cmdd = new SqlCommand();
                        cmdd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate,@acyear)", con111);
                        cmdd.Parameters.AddWithValue("@voucherno", li.voucherno);
                        cmdd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
                        cmdd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
                        cmdd.Parameters.AddWithValue("@refid", li.refid);
                        cmdd.Parameters.AddWithValue("@debitcode", li.debitcode);
                        cmdd.Parameters.AddWithValue("@creditcode", li.creditcode);
                        cmdd.Parameters.AddWithValue("@description", li.description);
                        cmdd.Parameters.AddWithValue("@projectcode", li.ccode);
                        cmdd.Parameters.AddWithValue("@amount", li.amount);
                        cmdd.Parameters.AddWithValue("@istype", li.istype);
                        cmdd.Parameters.AddWithValue("@type", li.istype1);
                        cmdd.Parameters.AddWithValue("@cno", li.cno);
                        cmdd.Parameters.AddWithValue("@uname", li.uname);
                        cmdd.Parameters.AddWithValue("@udate", li.udate);
                        cmdd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmdd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                //SqlDataAdapter daremain = new SqlDataAdapter("select * from acmaster where acname not in (select acname from ACMaster inner join Ledger on acmaster.acname=Ledger.debitcode where Ledger.istype='OP')", con111);
                //DataTable dtremain = new DataTable();
                //daremain.Fill(dtremain);
                //for (int gh = 0; gh < dtremain.Rows.Count; gh++)
                //{
                //    li.voucherno = 0;
                //    //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //    li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //    li.strvoucherno = "0";
                //    li.refid = 0;
                //    li.debitcode = dtremain.Rows[gh]["acname"].ToString();
                //    li.creditcode = "Opening Balance";
                //    li.description = "OP";
                //    li.istype1 = "D";
                //    li.amount = 0;
                //    li.ccode = 0;
                //    li.istype = "OP";
                //    li.uname = Request.Cookies["ForLogin"]["username"];
                //    li.udate = System.DateTime.Now;
                //    SqlCommand cmdd = new SqlCommand();
                //    cmdd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate,@acyear)", con111);
                //    cmdd.Parameters.AddWithValue("@voucherno", li.voucherno);
                //    cmdd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
                //    cmdd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
                //    cmdd.Parameters.AddWithValue("@refid", li.refid);
                //    cmdd.Parameters.AddWithValue("@debitcode", li.debitcode);
                //    cmdd.Parameters.AddWithValue("@creditcode", li.creditcode);
                //    cmdd.Parameters.AddWithValue("@description", li.description);
                //    cmdd.Parameters.AddWithValue("@projectcode", li.ccode);
                //    cmdd.Parameters.AddWithValue("@amount", li.amount);
                //    cmdd.Parameters.AddWithValue("@istype", li.istype);
                //    cmdd.Parameters.AddWithValue("@type", li.istype1);
                //    cmdd.Parameters.AddWithValue("@cno", li.cno);
                //    cmdd.Parameters.AddWithValue("@uname", li.uname);
                //    cmdd.Parameters.AddWithValue("@udate", li.udate);
                //    cmdd.Parameters.AddWithValue("@acyear", ayear);
                //    con111.Open();
                //    cmdd.ExecuteNonQuery();
                //    con111.Close();
                //}
                //SqlDataAdapter daccg = new SqlDataAdapter("select * from Ledger where debitcode not in (select acname from ACMaster inner join Ledger on acmaster.acname=Ledger.debitcode where Ledger.istype='OP') and istype='OP'", con111);
                //DataTable dtccg = new DataTable();
                //daccg.Fill(dtccg);
                //for (int b = 0; b < dtccg.Rows.Count; b++)
                //{
                //    li.acname = dtccg.Rows[b]["debitcode"].ToString();
                //    SqlDataAdapter dar = new SqlDataAdapter("select * from ACMaster where acname='" + li.acname + "'", con111);
                //    DataTable dtr = new DataTable();
                //    dar.Fill(dtr);
                //    if (dtr.Rows.Count == 0)
                //    {
                //        SqlCommand cmdr = new SqlCommand("insert into AT20182019.dbo.ACMaster select acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear,excludetax,inexport from AT20172018.dbo.ACMaster where AT20172018.dbo.ACMaster.acname='" + li.acname + "'", con111);
                //        con111.Open();
                //        cmdr.ExecuteNonQuery();
                //        con111.Close();

                //        SqlCommand cmdr1 = new SqlCommand("insert into AT20182019.dbo.Ledger select voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate,acyear from AT20172018.dbo.Ledger where AT20172018.dbo.Ledger.debitcode='" + li.acname + "' and AT20172018.dbo.Ledger.istype='OP'", con111);
                //        con111.Open();
                //        cmdr1.ExecuteNonQuery();
                //        con111.Close();

                //    }

                //}
                //SqlCommand cmdremove = new SqlCommand("delete from Ledger where debitcode not in (select acname from ACMaster inner join Ledger on acmaster.acname=Ledger.debitcode where Ledger.istype='OP') and istype='OP'", con111);
                //con111.Open();
                //cmdremove.ExecuteNonQuery();
                //con111.Close();
            }
            //Cary forwarding Item & Tools OP qty
            if (chkitemstock.Checked == true)// || chkitemstockpricewise.Checked == true
            {
                //SqlCommand cmdx = new SqlCommand("delete from itemmaster where acyear='" + ayear + "'", con111);
                //con111.Open();
                //cmdx.ExecuteNonQuery();
                //con111.Close();
                string sourcetable = olddbname + ".dbo.ItemMaster";
                string destinationtable = newdbname + ".dbo.ItemMaster";
                //SqlCommand cmd1 = new SqlCommand("insert into " + destinationtable + " select itemname,description,unit,unitewaybill,hsncode,salesrate,purchaserate,valuationrate,vattype,oprate,qty,rate,amount,cno,uname,udate,gsttype,vatdesc,gstdesc,isblock,reason,opqty,acyear from " + sourcetable + "", con);
                //con.Open();
                //cmd1.ExecuteNonQuery();
                //con.Close();
                //SqlCommand cmdy1 = new SqlCommand("update ItemMaster set acyear='" + ayear + "' where acyear is null", con111);
                //con111.Open();
                //cmdy1.ExecuteNonQuery();
                //con111.Close();
                //SqlCommand cmdx1 = new SqlCommand("delete from itemmaster1 where acyear='" + ayear + "'", con111);
                //con111.Open();
                //cmdx1.ExecuteNonQuery();
                //con111.Close();
                DataTable dtallstock = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter("select distinct(itemname) from itemmaster order by itemname", con);
                DataTable dtitem = new DataTable();
                da.Fill(dtitem);
                for (int c = 0; c < dtitem.Rows.Count; c++)
                {
                    li.itemname = dtitem.Rows[c]["itemname"].ToString();
                    SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname", con);
                    dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
                    DataTable dtstock = new DataTable();
                    dac.Fill(dtstock);
                    dtallstock.Merge(dtstock);
                }
                for (int f = 0; f < dtallstock.Rows.Count; f++)
                {
                    if (dtallstock.Rows[f]["itemname"].ToString() == "Cable Tray")
                    {
                        string cc = "";
                    }
                    SqlCommand cmditem = new SqlCommand("update Itemmaster set opqty=" + Math.Round(Convert.ToDouble(dtallstock.Rows[f]["totalqty"].ToString()), 2) + " where itemname='" + dtallstock.Rows[f]["itemname"].ToString() + "'", con111);
                    con111.Open();
                    cmditem.ExecuteNonQuery();
                    con111.Close();
                }
            }

            if (chktoolstock.Checked == true)
            {
                SqlCommand cmdx = new SqlCommand("delete from toolsmaster where acyear='" + ayear + "'", con111);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();

                string sourcetable = olddbname + ".dbo.ToolsMaster";
                string destinationtable = newdbname + ".dbo.ToolsMaster";
                SqlCommand cmd1 = new SqlCommand("insert into " + destinationtable + " select name,unit,rate,opstock,inward,outward,clstock,cno,uname,udate,acyear from " + sourcetable + "", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
                SqlCommand cmdy3 = new SqlCommand("update ToolsMaster set acyear='" + ayear + "' where acyear is null", con111);
                con111.Open();
                cmdy3.ExecuteNonQuery();
                con111.Close();

                DataTable dtallstockt = new DataTable();
                SqlDataAdapter dat = new SqlDataAdapter("select distinct(name) from toolsmaster order by name", con);
                DataTable dtitemt = new DataTable();
                dat.Fill(dtitemt);
                for (int c = 0; c < dtitemt.Rows.Count; c++)
                {
                    li.name = dtitemt.Rows[c]["name"].ToString();
                    SqlDataAdapter dac = new SqlDataAdapter("select name as toolname,(select isnull(sum(opstock),0) from toolsmaster where name=@itemname) as opqty,((select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='OUT')) as tots,((select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='IN')) as totp,(((select isnull(sum(opstock),0) from ToolsMaster where name=@itemname)+(select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='IN'))-((select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='OUT'))) as totalqty from ToolsMaster where name=@itemname", con);
                    dac.SelectCommand.Parameters.AddWithValue("@itemname", li.name);
                    DataTable dtstock = new DataTable();
                    dac.Fill(dtstock);
                    dtallstockt.Merge(dtstock);
                }
                for (int f = 0; f < dtallstockt.Rows.Count; f++)
                {
                    SqlCommand cmditem = new SqlCommand("update toolsmaster set opstock=" + Math.Round(Convert.ToDouble(dtallstockt.Rows[f]["totalqty"].ToString()), 2) + " where name='" + dtallstockt.Rows[f]["toolname"].ToString() + "'", con111);
                    con111.Open();
                    cmditem.ExecuteNonQuery();
                    con111.Close();
                }
            }
            //

            //Carry Item stock pricewise
            if (chkitemstockpricewise.Checked == true)// || chkitemstock.Checked == true
            {
                SqlCommand cmdx1 = new SqlCommand("delete from itemmaster1 where acyear='" + ayear + "'", con111);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter dac = new SqlDataAdapter("select PCitems.pcno,Pcitems.id,PCItems.itemname,ItemMaster.unit,((PCItems.stockqty)-(isnull(PCItems.adjqty,0))) as qty,PCItems.rate,(PCItems.rate*((PCItems.stockqty)-(isnull(PCItems.adjqty,0)))) as amount from PCItems inner join ItemMaster on ItemMaster.itemname=PCItems.itemname where ((PCItems.qty)-(isnull(PCItems.adjqty,0)))>0 and PCItems.pcdate between @fromdate and @todate order by PCItems.itemname", con);
                dac.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtstock = new DataTable();
                dac.Fill(dtstock);

                SqlDataAdapter dac11 = new SqlDataAdapter("select StockJVItems.id,StockJVItems.itemname,ItemMaster.unit,((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0))) as qty,StockJVItems.rate,(StockJVItems.rate*((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))) as amount from StockJVItems inner join ItemMaster on ItemMaster.itemname=StockJVItems.itemname where ((StockJVItems.qty)-(isnull(StockJVItems.adjqty,0)))>0 and StockJVItems.inout='IN' and StockJVItems.challandate between @fromdate and @todate order by StockJVItems.itemname", con);
                dac11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtstock1 = new DataTable();
                dac11.Fill(dtstock1);
                //dtstock.Merge(dtstock1);
                //imageDataSet.Tables["DataTable16"].Merge(dtstock1);
                DataTable dtopstock = new DataTable();
                SqlDataAdapter dac1 = new SqlDataAdapter("select ItemMaster1.id,ItemMaster1.itemname,ItemMaster.unit,((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0))) as qty,ItemMaster1.rate,(ItemMaster1.rate*((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0)))) as amount from ItemMaster1 inner join ItemMaster on ItemMaster.itemname=ItemMaster1.itemname where ((ItemMaster1.qty)-(isnull(ItemMaster1.adjqty,0)))>0 and ItemMaster1.opdate between @fromdate and @todate order by ItemMaster1.itemname", con);
                dac1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dac1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                dac1.Fill(dtopstock);
                for (int d = 0; d < dtstock.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.id = Convert.ToInt64(dtstock.Rows[d]["id"].ToString());
                    li.itemname = dtstock.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtstock.Rows[d]["qty"].ToString());
                    if (li.itemname == "Ball Valve" && li.qty == 2)
                    {
                        string pp = "";
                    }
                    qty = Convert.ToDouble(dtstock.Rows[d]["qty"].ToString());
                    SqlDataAdapter dap = new SqlDataAdapter("select * from PIItems where vid=" + li.id + "", con);
                    DataTable dtpi = new DataTable();
                    dap.Fill(dtpi);
                    if (dtpi.Rows.Count == 0)
                    {
                        li.rate = Convert.ToDouble(dtstock.Rows[d]["rate"].ToString());
                        li.unit = dtstock.Rows[d]["unit"].ToString();
                        li.amount = Convert.ToDouble(dtstock.Rows[d]["amount"].ToString());
                    }
                    else
                    {
                        li.rate = Convert.ToDouble(dtpi.Rows[0]["rate"].ToString());
                        li.unit = dtstock.Rows[d]["unit"].ToString();
                        rate = Convert.ToDouble(dtpi.Rows[0]["rate"].ToString());
                        li.amount = Math.Round(qty * rate, 2);
                    }
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                for (int d = 0; d < dtstock1.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.id = Convert.ToInt64(dtstock1.Rows[d]["id"].ToString());
                    li.itemname = dtstock1.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtstock1.Rows[d]["qty"].ToString());
                    if (li.itemname == "Ball Valve" && li.qty == 2)
                    {
                        string pp = "";
                    }
                    qty = Convert.ToDouble(dtstock1.Rows[d]["qty"].ToString());
                    li.rate = Convert.ToDouble(dtstock1.Rows[d]["rate"].ToString());
                    li.unit = "";
                    li.amount = Math.Round((qty * li.rate), 2);
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
                for (int d = 0; d < dtopstock.Rows.Count; d++)
                {
                    double qty = 0;
                    double rate = 0;
                    double amt = 0;
                    li.itemname = dtopstock.Rows[d]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtopstock.Rows[d]["qty"].ToString());
                    if (li.itemname == "Ball Valve" && li.qty == 2)
                    {
                        string pp = "";
                    }
                    li.rate = Convert.ToDouble(dtopstock.Rows[d]["rate"].ToString());
                    li.unit = dtopstock.Rows[d]["unit"].ToString();
                    li.amount = Convert.ToDouble(dtopstock.Rows[d]["amount"].ToString());
                    li.adjqty = 0;
                    li.opdate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    if (li.qty > 0)
                    {
                        SqlCommand cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con111);
                        cmd.Parameters.AddWithValue("@itemname", li.itemname);
                        cmd.Parameters.AddWithValue("@opdate", li.opdate);
                        cmd.Parameters.AddWithValue("@qty", li.qty);
                        cmd.Parameters.AddWithValue("@rate", li.rate);
                        cmd.Parameters.AddWithValue("@amount", li.amount);
                        cmd.Parameters.AddWithValue("@cno", li.cno);
                        cmd.Parameters.AddWithValue("@uname", li.uname);
                        cmd.Parameters.AddWithValue("@udate", li.udate);
                        cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
                        cmd.Parameters.AddWithValue("@acyear", ayear);
                        con111.Open();
                        cmd.ExecuteNonQuery();
                        con111.Close();
                    }
                }
            }
            //

            //Carry Pendong PO
            if (chkpendingpo.Checked == true)
            {
                string pono = "";
                DataTable dtpo1 = new DataTable();
                SqlDataAdapter dax = new SqlDataAdapter("select * from PurchaseOrder where podate between @fromdate and @todate or podate<@fromdate", con111);
                dax.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                dax.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                dax.Fill(dtpo1);
                for (int k = 0; k < dtpo1.Rows.Count; k++)
                {
                    li.strpono = dtpo1.Rows[k]["pono"].ToString();
                    SqlDataAdapter da1 = new SqlDataAdapter("select * from PCMaster where stringpono like '%'+@pno+'%'", con111);
                    da1.SelectCommand.Parameters.AddWithValue("@pno", li.strpono);
                    DataTable dtpo2 = new DataTable();
                    da1.Fill(dtpo2);
                    if (dtpo2.Rows.Count > 0)
                    {
                        for (int g = 0; g < dtpo2.Rows.Count; g++)
                        {
                            string[] words = dtpo2.Rows[g]["stringpono"].ToString().Split(',');
                            foreach (string word in words)
                            {
                                if ("'" + li.strpono + "'" == word)
                                {
                                    pono = pono + "'" + li.strpono + "',";
                                }
                            }
                        }
                    }
                    else
                    {
                    }
                }
                SqlCommand cmdx = new SqlCommand("delete from PurchaseOrder where (podate between @fromdate and @todate or podate<@fromdate) and pono not in (" + pono.TrimEnd(',') + ")", con111);
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx1 = new SqlCommand("delete from PurchaseOrderItems where (podate between @fromdate and @todate or podate<@fromdate) and pono not in (" + pono.TrimEnd(',') + ")", con111);
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                string id = "";

                //        //SqlDataAdapter dac = new SqlDataAdapter("select * from PurchaseOrder where pono in (" + strpono + ")", con);
                //        //DataTable dtpo1 = new DataTable();
                //        //dac.Fill(dtpo1);
                //        //for (int x = 0; x < dtpo1.Rows.Count; x++)
                //        //{
                //        //    li.strpono = dtpo1.Rows[x]["pono"].ToString();
                //        //    li.podate = Convert.ToDateTime(dtpo1.Rows[x]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                //        //}                        

                //        //SqlDataAdapter dac1 = new SqlDataAdapter("select * from PurchaseOrderItems where id in (" + id + ")", con);
                //        //DataTable dtpo11 = new DataTable();
                //        //dac1.Fill(dtpo11);
                //        //for (int x = 0; x < dtpo11.Rows.Count; x++)
                //        //{ }
                //        //SqlCommand cmd=new SqlCommand("insert into table2 select * FROM table1 where condition;",con111);
                //    }                    
                //}

                SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.cno=@cno and PurchaseOrder.pono not in (" + pono.TrimEnd(',') + ") order by PurchaseOrder.podate,PurchaseOrder.pono", con);
                da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                DataTable dtc = new DataTable();
                da11.Fill(dtc);
                string strpono = "";
                if (dtc.Rows.Count > 0)
                {
                    for (int c = 0; c < dtc.Rows.Count; c++)
                    {
                        li.id = Convert.ToInt64(dtc.Rows[c]["id"].ToString());
                        li.qty = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                        if (dtc.Rows[c]["adjqty"].ToString() != string.Empty)
                        {
                            li.adjqty = Convert.ToDouble(dtc.Rows[c]["adjqty"].ToString());
                        }
                        else
                        {
                            li.adjqty = 0;
                        }
                        SqlDataAdapter da11c = new SqlDataAdapter("select isnull(sum(qty) ,0) as totqty from PCItems where vid=@id and cno=@cno", con);
                        da11c.SelectCommand.Parameters.AddWithValue("@id", li.id);
                        da11c.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                        DataTable dtcc = new DataTable();
                        da11c.Fill(dtcc);
                        if (dtcc.Rows.Count > 0)
                        {
                            li.qtypc = Convert.ToDouble(dtcc.Rows[0]["totqty"].ToString());
                            li.qtyremain = li.qty - (li.qtypc + li.adjqty);
                            if (li.qtyremain > 0)
                            {
                                id = id + li.id + ",";
                                strpono = strpono + "'" + dtc.Rows[c]["pono"].ToString() + "',";
                                li.strpono = dtc.Rows[c]["pono"].ToString();
                                li.podate = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString());
                                li.itemname = dtc.Rows[c]["itemname"].ToString();
                                li.qty = li.qtypc;
                                li.stockqty = li.qty;//Convert.ToDouble(dtc.Rows[c]["stockqty"].ToString())
                                li.unit = dtc.Rows[c]["unit"].ToString();
                                li.rate = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                                li.basicamount = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());
                                li.taxtype = dtc.Rows[c]["taxtype"].ToString();
                                li.vatp = Convert.ToDouble(dtc.Rows[c]["vatp"].ToString());
                                li.addtaxp = Convert.ToDouble(dtc.Rows[c]["addtaxp"].ToString());
                                li.cstp = Convert.ToDouble(dtc.Rows[c]["cstp"].ToString());
                                li.vatamt = Convert.ToDouble(dtc.Rows[c]["vatamt"].ToString());
                                li.addtaxamt = Convert.ToDouble(dtc.Rows[c]["addtaxamt"].ToString());
                                li.cstamt = Convert.ToDouble(dtc.Rows[c]["cstamt"].ToString());
                                li.amount = Convert.ToDouble(dtc.Rows[c]["amount"].ToString());
                                li.descr1 = dtc.Rows[c]["descr1"].ToString();
                                li.descr2 = dtc.Rows[c]["descr2"].ToString();
                                li.descr3 = dtc.Rows[c]["descr3"].ToString();
                                li.cno = Convert.ToInt64(dtc.Rows[c]["cno"].ToString());
                                li.uname = dtc.Rows[c]["uname"].ToString();
                                li.udate = Convert.ToDateTime(dtc.Rows[c]["udate"].ToString());
                                li.qtyremain = li.qtypc;//Convert.ToDouble(dtc.Rows[c]["pono"].ToString())
                                li.qtyused = 0;
                                li.vno = dtc.Rows[c]["vno"].ToString();
                                li.iscame = dtc.Rows[c]["iscame"].ToString();
                                li.vid = Convert.ToInt64(dtc.Rows[c]["vid"].ToString());
                                li.adjqty = 0;
                                SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.PurchaseOrderItems (pono,podate,itemname,qty,stockqty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,iscame,vid,adjqty) values (@pono,@podate,@itemname,@qty,@stockqty,@unit,@rate,@basicamount,@taxtype,@vatp,@addtaxp,@cstp,@vatamt,@addtaxamt,@cstamt,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@qtyremain,@qtyused,@vno,@iscame,@vid,@adjqty)", con111);//id in (" + id.TrimEnd(',') + ")
                                cmds1.Parameters.AddWithValue("@pono", li.pono);
                                cmds1.Parameters.AddWithValue("@podate", li.podate);
                                cmds1.Parameters.AddWithValue("@itemname", li.itemname);
                                cmds1.Parameters.AddWithValue("@qty", li.qty);
                                cmds1.Parameters.AddWithValue("@stockqty", li.qty);
                                cmds1.Parameters.AddWithValue("@unit", li.unit);
                                cmds1.Parameters.AddWithValue("@rate", li.rate);
                                cmds1.Parameters.AddWithValue("@basicamount", li.basicamount);
                                cmds1.Parameters.AddWithValue("@taxtype", li.taxtype);
                                cmds1.Parameters.AddWithValue("@vatp", li.vatp);
                                cmds1.Parameters.AddWithValue("@addtaxp", li.addtaxp);
                                cmds1.Parameters.AddWithValue("@cstp", li.cstp);
                                cmds1.Parameters.AddWithValue("@vatamt", li.vatamt);
                                cmds1.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
                                cmds1.Parameters.AddWithValue("@cstamt", li.cstamt);
                                cmds1.Parameters.AddWithValue("@amount", li.amount);
                                cmds1.Parameters.AddWithValue("@descr1", li.descr1);
                                cmds1.Parameters.AddWithValue("@descr2", li.descr2);
                                cmds1.Parameters.AddWithValue("@descr3", li.descr3);
                                cmds1.Parameters.AddWithValue("@cno", li.cno);
                                cmds1.Parameters.AddWithValue("@uname", li.uname);
                                cmds1.Parameters.AddWithValue("@udate", li.udate);
                                cmds1.Parameters.AddWithValue("@qtyremain", li.qtyremain);
                                cmds1.Parameters.AddWithValue("@qtyused", li.qtyused);
                                cmds1.Parameters.AddWithValue("@vno", li.vno);
                                cmds1.Parameters.AddWithValue("@iscame", li.iscame);
                                cmds1.Parameters.AddWithValue("@vid", li.vid);
                                cmds1.Parameters.AddWithValue("@adjqty", li.adjqty);
                                con111.Open();
                                cmds1.ExecuteNonQuery();
                                con111.Close();
                            }
                        }
                        else
                        {
                            id = id + li.id + ",";
                            strpono = strpono + "'" + dtc.Rows[c]["pono"].ToString() + "',";
                        }
                    }
                }

                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.PurchaseOrder SELECT * FROM " + olddbname + ".dbo.PurchaseOrder where pono in (" + strpono.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();

                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.PurchaseOrderItems SELECT pono,podate,itemname,qty,stockqty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,iscame,vid,adjqty FROM " + olddbname + ".dbo.PurchaseOrderItems where pono in (" + strpono.TrimEnd(',') + ")", con111);//id in (" + id.TrimEnd(',') + ")
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();
            }
            //

            //Carry Pending SC
            if (chkpendingsc.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete SCItems from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCMaster.status='Open' and (SCItems.scdate between @fromdate and @todate or SCItems.scdate<@fromdate)", con111);// and (SCMaster.issigned='Yes' or SCMaster.issigned is null)
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx = new SqlCommand("delete from SCMaster where status='Open' and (scdate between @fromdate and @todate or scdate<@fromdate)", con111);// and (issigned='Yes' or issigned is null)
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                string strscno = "";
                SqlDataAdapter das = new SqlDataAdapter("SELECT * FROM " + olddbname + ".dbo.SCMaster where status='Open' and ctype<>'RM'", con);
                DataTable dtcheck = new DataTable();
                das.Fill(dtcheck);
                for (int i = 0; i < dtcheck.Rows.Count; i++)
                {
                    li.strscno = dtcheck.Rows[i]["strscno"].ToString();
                    SqlDataAdapter da2 = new SqlDataAdapter("select * from SCMaster where strscno='" + li.strscno + "'", con111);
                    DataTable dt2 = new DataTable();
                    da2.Fill(dt2);
                    if (dt2.Rows.Count == 0)
                    {
                        SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SCMaster SELECT * FROM " + olddbname + ".dbo.SCMaster where strscno='" + li.strscno + "'", con111);
                        con111.Open();
                        cmds.ExecuteNonQuery();
                        con111.Close();
                        //SqlDataAdapter daf = new SqlDataAdapter("select (ctype+convert(varchar(50),scno)) as strscno from SCMaster where status='Open' and ctype<>'RM'", con);
                        //DataTable dtf = new DataTable();
                        //daf.Fill(dtf);
                        //for (int f = 0; f < dtf.Rows.Count; f++)
                        //{
                        //    strscno = strscno + "'" + dtf.Rows[f]["strscno"].ToString() + "',";
                        //}
                        //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SCMaster SELECT " + olddbname + ".dbo.SCMaster.* FROM " + olddbname + ".dbo.SCMaster inner join " + olddbname + ".dbo.SCItems on " + olddbname + ".dbo.SCMaster.strscno=" + olddbname + ".dbo.SCItems.strscno where " + olddbname + ".dbo.SCMaster.strscno in (" + strscno + ")", con111);
                        //con111.Open();
                        //cmds1.ExecuteNonQuery();
                        //con111.Close();

                        SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SCItems SELECT scno,scdate,itemname,qty,stockqty,rate,basicamount,unit,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,ccode,qtyremain,qtyused,vno,vid,strscno,adjqty,remarks,rfs FROM " + olddbname + ".dbo.SCItems where " + olddbname + ".dbo.SCItems.strscno='" + li.strscno + "'", con111);
                        con111.Open();
                        cmds11.ExecuteNonQuery();
                        con111.Close();
                    }
                }

            }
            //

            //Carry Pending Signed SC
            if (chkpendingsignsc.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete SCItems from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCMaster.issigned='No' and SCMaster.status<>'Open' and (SCItems.scdate between @fromdate and @todate or SCItems.scdate<@fromdate)", con111);
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx = new SqlCommand("delete from SCMaster where issigned='No' and status<>'Open' and (scdate between @fromdate and @todate or scdate<@fromdate)", con111);
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                string strscno = "";//select * from " + olddbname + ".dbo.SCMaster where issigned='No'
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SCMaster select * from " + olddbname + ".dbo.SCMaster where issigned='No' and status<>'Open'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select (ctype+convert(varchar(50),scno)) as strscno from SCMaster where issigned='No' and status<>'Open'", con);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    strscno = strscno + "'" + dtf.Rows[f]["strscno"].ToString() + "',";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SCMaster SELECT " + olddbname + ".dbo.SCMaster.* FROM " + olddbname + ".dbo.SCMaster inner join " + olddbname + ".dbo.SCItems on " + olddbname + ".dbo.SCMaster.strscno=" + olddbname + ".dbo.SCItems.strscno where " + olddbname + ".dbo.SCMaster.strscno in (" + strscno + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SCItems SELECT scno,scdate,itemname,qty,stockqty,rate,basicamount,unit,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,ccode,qtyremain,qtyused,vno,vid,strscno,adjqty,remarks,rfs FROM " + olddbname + ".dbo.SCItems where " + olddbname + ".dbo.SCItems.strscno in (" + strscno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();

            }
            //

            //Carry Pending PC
            if (chkpendingpc.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete PCItems from PCItems inner join PCMaster on PCMaster.pcno=PCItems.pcno where PCMaster.status='Open' and (PCItems.pcdate between @fromdate and @todate or PCItems.pcdate<@fromdate)", con111);
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx = new SqlCommand("delete from PCMaster where status='Open' and (pcdate between @fromdate and @todate or pcdate<@fromdate)", con111);
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                string pcno = "";
                SqlDataAdapter da1 = new SqlDataAdapter("SELECT * FROM " + olddbname + ".dbo.PCMaster where status='Open'", con);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    li.pcno = Convert.ToInt64(dt1.Rows[i]["pcno"].ToString());
                    SqlDataAdapter da11 = new SqlDataAdapter("SELECT * FROM " + olddbname + ".dbo.PCMaster where pcno=" + li.pcno + "", con111);
                    DataTable dt11 = new DataTable();
                    da11.Fill(dt11);
                    if (dt11.Rows.Count == 0)
                    {
                        SqlCommand cmds = new SqlCommand("INSERT INTO dbo.PCMaster SELECT * FROM " + olddbname + ".dbo.PCMaster where pcno=" + li.pcno + "", con111);
                        con111.Open();
                        cmds.ExecuteNonQuery();
                        con111.Close();
                        //SqlDataAdapter daf = new SqlDataAdapter("select * from PCMaster where status='Open'", con);
                        //DataTable dtf = new DataTable();
                        //daf.Fill(dtf);
                        //for (int f = 0; f < dtf.Rows.Count; f++)
                        //{
                        //    pcno = pcno + dtf.Rows[f]["pcno"].ToString() + ",";
                        //}
                        //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.PCMaster SELECT " + olddbname + ".dbo.PCMaster.* FROM " + olddbname + ".dbo.PCMaster inner join " + olddbname + ".dbo.PCItems on " + olddbname + ".dbo.PCMaster.pcno=" + olddbname + ".dbo.PCItems.pcno where " + olddbname + ".dbo.PCMaster.pcno in (" + pcno.TrimEnd(',') + ")", con111);
                        //con111.Open();
                        //cmds1.ExecuteNonQuery();
                        //con111.Close();

                        SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.PCItems SELECT pcno,pcdate,itemname,qty,stockqty,rate,basicamount,ccode,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,qtyremain,qtyused,vno,vid,adjqty,unit FROM " + olddbname + ".dbo.PCItems where " + olddbname + ".dbo.PCItems.pcno=" + li.pcno + "", con111);
                        con111.Open();
                        cmds11.ExecuteNonQuery();
                        con111.Close();
                    }
                }
            }
            //

            //Carry Pending PC
            if (chkpendingso.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete from SalesOrderItems where sodate between @fromdate and @todate or sodate<@fromdate", con111);
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx = new SqlCommand("delete from SalesOrder where sodate between @fromdate and @todate or sodate<@fromdate", con111);
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                string pcno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.SalesOrder SELECT " + olddbname + ".dbo.SalesOrder.* FROM " + olddbname + ".dbo.SalesOrder inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.SalesOrder.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select * from SalesOrder inner join ClientMaster on SalesOrder.ccode=ClientMaster.code where SalesOrder.status='Open' and ClientMaster.status='Running'", con);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    pcno = pcno + dtf.Rows[f]["sono"].ToString() + ",";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.SalesOrder SELECT " + olddbname + ".dbo.SalesOrder.* FROM " + olddbname + ".dbo.SalesOrder inner join " + olddbname + ".dbo.SalesOrderItems on " + olddbname + ".dbo.SalesOrder.sono=" + olddbname + ".dbo.SalesOrderItems.sono where " + olddbname + ".dbo.SalesOrder.sono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.SalesOrderItems SELECT sono,sodate,itemname,qty,stockqty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,qtyremain1,qtyused1,vid,firstquotation,technicaldrawing,rewisequotation,finaltech,lastfollowupdate,pendingorderlying,okorderdate,tentativedeliverydate,deliverydate,remarks,otype FROM " + olddbname + ".dbo.SalesOrderItems where " + olddbname + ".dbo.SalesOrderItems.sono in (" + pcno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();
            }
            //

            //Carry Pending PC
            if (chkpendingquo.Checked == true)
            {
                SqlCommand cmdx1 = new SqlCommand("delete QuoItems from QuoItems inner join QuoMaster on QuoMaster.quono=QuoItems.quono where QuoItems.quodate between @fromdate and @todate or QuoItems.quodate<@fromdate", con111);
                cmdx1.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx1.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx1.ExecuteNonQuery();
                con111.Close();
                SqlCommand cmdx = new SqlCommand("delete from QuoMaster where quodate between @fromdate and @todate or quodate<@fromdate", con111);
                cmdx.Parameters.AddWithValue("@fromdate", li.fromdated);
                cmdx.Parameters.AddWithValue("@todate", li.todated);
                con111.Open();
                cmdx.ExecuteNonQuery();
                con111.Close();
                string pcno = "";
                SqlCommand cmds = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.QuoMaster.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                con111.Open();
                cmds.ExecuteNonQuery();
                con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select * from QuoMaster inner join ClientMaster on QuoMaster.ccode=ClientMaster.code where ClientMaster.status='Running'", con);// QuoMaster.status='Open' and
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    pcno = pcno + dtf.Rows[f]["quono"].ToString() + ",";
                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.QuoItems on " + olddbname + ".dbo.QuoMaster.quono=" + olddbname + ".dbo.QuoItems.quono where " + olddbname + ".dbo.QuoMaster.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.QuoItems SELECT srno,quono,quodate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,make,hsncode FROM " + olddbname + ".dbo.QuoItems where " + olddbname + ".dbo.QuoItems.quono in (" + pcno.TrimEnd(',') + ")", con111);
                con111.Open();
                cmds11.ExecuteNonQuery();
                con111.Close();
            }
            //

            //Carry Pending Cheques
            if (chkreconcile.Checked == true)
            {
                string pcno = "";
                //SqlCommand cmds = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.ClientMaster on " + olddbname + ".dbo.QuoMaster.ccode=" + olddbname + ".dbo.ClientMaster.code where " + olddbname + ".dbo.ClientMaster.status='Running'", con111);
                //con111.Open();
                //cmds.ExecuteNonQuery();
                //con111.Close();
                SqlDataAdapter daf = new SqlDataAdapter("select BankACMaster1.* from BankACMaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where (BankACMaster.cldate is null or BankACMaster.cldate='') and BankACMaster.istype in ('BR','BP')", con);//QuoMaster.status='Open' and 
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int f = 0; f < dtf.Rows.Count; f++)
                {
                    li.istype = dtf.Rows[f]["istype"].ToString();
                    li.voucherno = Convert.ToInt64(dtf.Rows[f]["voucherno"].ToString());
                    SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.BankACMaster SELECT voucherno,voucherdate,name,acname,ccode,remarks,amount,chequeno,agbill,istype,cno,uname,udate,sino,paidamount,issipi,iscd,cldate,hsncode FROM " + olddbname + ".dbo.BankACMaster where " + olddbname + ".dbo.BankACMaster.voucherno=" + li.voucherno + " and " + olddbname + ".dbo.BankACMaster.istype='" + li.istype + "'", con111);
                    con111.Open();
                    cmds11.ExecuteNonQuery();
                    con111.Close();

                    SqlCommand cmds111 = new SqlCommand("INSERT INTO dbo.BankACMaster1 SELECT * FROM " + olddbname + ".dbo.BankACMaster1 where " + olddbname + ".dbo.BankACMaster1.voucherno=" + li.voucherno + " and " + olddbname + ".dbo.BankACMaster1.istype='" + li.istype + "'", con111);
                    con111.Open();
                    cmds111.ExecuteNonQuery();
                    con111.Close();

                }
                //SqlCommand cmds1 = new SqlCommand("INSERT INTO dbo.QuoMaster SELECT " + olddbname + ".dbo.QuoMaster.* FROM " + olddbname + ".dbo.QuoMaster inner join " + olddbname + ".dbo.QuoItems on " + olddbname + ".dbo.QuoMaster.quono=" + olddbname + ".dbo.QuoItems.quono where " + olddbname + ".dbo.QuoMaster.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds1.ExecuteNonQuery();
                //con111.Close();

                //SqlCommand cmds11 = new SqlCommand("INSERT INTO dbo.QuoItems SELECT srno,quono,quodate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,make,hsncode FROM " + olddbname + ".dbo.QuoItems where " + olddbname + ".dbo.QuoItems.quono in (" + pcno.TrimEnd(',') + ")", con111);
                //con111.Open();
                //cmds11.ExecuteNonQuery();
                //con111.Close();
            }
            //

            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('year End Process Updated New Data successfully.');", true);
            return;
            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('year End Process already done.');", true);
            //return;
        }


    }

    public DataTable CreateTemplate1()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("opening", typeof(double));
        dtpitems.Columns.Add("debitamt", typeof(double));
        dtpitems.Columns.Add("creditamt", typeof(double));
        dtpitems.Columns.Add("closingbal", typeof(double));
        dtpitems.Columns.Add("crdr", typeof(string));
        dtpitems.Columns.Add("crdr1", typeof(string));
        return dtpitems;
    }

}