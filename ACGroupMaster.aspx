﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ACGroupMaster.aspx.cs" Inherits="ACGroupMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">A/C Group Master</span><br />
        <div class="tab-block margin-bottom-lg">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab6" data-toggle="tab">Account Group List</a></li>
                <li><a href="#tab7" data-toggle="tab"><i class="fa fa-bolt text-blue2"></i>Account Group
                    Tree</a></li>
            </ul>
            <div class="row" style="height: 10px">
            </div>
            <div class="tab-content">
                <div id="tab6" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Debit</asp:ListItem>
                                        <asp:ListItem>Credit</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Search</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control" Width="155px"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <asp:Button ID="btnsaves" runat="server" Text="Search" class="btn btn-default forbutton"
                                        ValidationGroup="val" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtgroupcode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtgroupname" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <%--<div class="col-md-2">
                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="155px"></asp:TextBox></div>--%>
                                <div class="col-md-1">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" class="btn btn-default forbutton"
                                        ValidationGroup="val" OnClick="btnsave_Click" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="row table-responsive">
                        <div class="col-md-12">
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                                <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                                    BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                    BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" CellSpacing="1"
                                    GridLines="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Group Code" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgroupcode" runat="server" Text='<%# Eval("groupcode") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Group Name" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgroupname" runat="server" Text='<%# Eval("groupname") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div id="tab7" class="tab-pane">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="row" style="height: 10px">
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkaccountledger" runat="server" Text="Account Ledger" /></div>
                                <div class="col-md-2">
                                    <asp:Button ID="btntrading" runat="server" Text="Trading Account" class="btn btn-default forbutton"
                                        ValidationGroup="val" OnClick="btntrading_Click" />
                                </div>
                                <div class="col-md-2">
                                    <asp:Button ID="btnprofit" runat="server" Text="Profit & Loss Account" class="btn btn-default forbutton"
                                        ValidationGroup="val" OnClick="btnprofit_Click" /></div>
                                <div class="col-md-2">
                                    <asp:Button ID="btnbalance" runat="server" Text="Balance Sheet" class="btn btn-default forbutton"
                                        ValidationGroup="val" OnClick="btnbalance_Click" /></div>
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="rdoexco" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="rdoexco_SelectedIndexChanged">
                                        <asp:ListItem Selected="True">Full Expand</asp:ListItem>
                                        <asp:ListItem>Full Collapse</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <asp:TreeView ID="TreeView1" runat="server" ImageSet="WindowsHelp">
                                    </asp:TreeView>
                                </div>
                                <div class="col-md-5">
                                    <asp:TreeView ID="TreeView2" runat="server" ImageSet="WindowsHelp">
                                    </asp:TreeView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
