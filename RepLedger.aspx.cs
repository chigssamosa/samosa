﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;

public partial class RepLedger : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        //li.strvoucherno = "BR100";
        //li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);

        //li.strvoucherno = "BR1004";
        //int sa = li.strvoucherno.Length;
        //li.strvoucherno = li.strvoucherno.Substring(2, 4);
        //string input = "OneTwoThree";

        //// Get first three characters.
        //string sub = input.Substring(0, input.Length-7);
        if (!IsPostBack)
        {
            if (Request["fromdate"] != null)
            {
                txtacname.Text = Request["acname"].ToString();
                txtccode.Text = Request["ccode"].ToString();
                txtfromdate.Text = Request["fromdate"].ToString();
                txttodate.Text = Request["todate"].ToString();
                abc();
            }
        }
    }

    public void abc()
    {
        li.bank1 = txtacname.Text;
        Session["dtpitemse"] = CreateTemplatec();
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        if (txtccode.Text.Trim() != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtccode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        if (li.ccode == 0)
        {
            SqlDataAdapter da11 = new SqlDataAdapter("select strvoucherno,voucherdate,creditcode,debitcode,description,istype,(CASE WHEN (type = 'D') THEN amount ELSE 0 END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE 0 END) as Creditamt from ledger where debitcode='" + li.bank1 + "' and cno=" + li.cno + " and istype!='OP' and voucherdate between @fromdate and @todate order by voucherdate,voucherno", con);
            da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable9"]);
            string rptname;
            //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
            //{
            rptname = Server.MapPath(@"~/Reports/ledger.rpt");

            //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
            SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
            dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop = new DataTable();
            dc.Fill(dtop);

            SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
            dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop1 = new DataTable();
            dc1.Fill(dtop1);
            double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));
            SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where debitcode='" + li.bank1 + "' and istype='OP'", con);
            DataTable dtac = new DataTable();
            daac.Fill(dtac);
            if (dtac.Rows.Count > 0)
            {
                if (Convert.ToDouble(dtac.Rows[0]["amount"].ToString()) >= 0)
                {
                    opbal = Math.Round(opbal + Convert.ToDouble(dtac.Rows[0]["amount"].ToString()), 2);
                }
                else
                {
                    opbal = Math.Round(opbal + (Convert.ToDouble(dtac.Rows[0]["amount"].ToString())), 2);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                return;
            }
            //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
            //}
            //else
            //{
            //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
            //    {
            //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
            //    }
            //    else
            //    {
            //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
            //    }
            //}
            double zero = 0;
            DataTable dtq = new DataTable();
            if (opbal >= 0)
            {

                dtq = (DataTable)Session["dtpitemse"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "";
                //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = opbal;
                dr1["creditamt"] = 0;
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dr1["chequeno"] = "";
                dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                        {
                            //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                        else
                        {
                            //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                            li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            DataTable dtinv = new DataTable();
                            SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                            dainv.Fill(dtinv);
                            //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                    }
                    else
                    {
                        dr["strvoucherno"] = "";
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "PI")
                    {
                        li.strvoucherno = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        DataTable dtchqno = new DataTable();
                        SqlDataAdapter dachqno = new SqlDataAdapter("select * from BankACMaster where cno=" + li.cno + " and istype+convert(varchar(50),voucherno)='" + li.strvoucherno + "'", con);
                        dachqno.Fill(dtchqno);
                        if (dtchqno.Rows.Count > 0)
                        {
                            dr["chequeno"] = dtchqno.Rows[0]["chequeno"].ToString();
                        }
                        else
                        {
                            dr["chequeno"] = "";
                        }
                    }
                    else
                    {
                        dr["chequeno"] = "";
                    }
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                    dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                    dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        if (closingbalance >= 0)
                        {
                            dr["crdr"] = "Dr";
                        }
                        else
                        {
                            dr["crdr"] = "Cr";
                        }
                    }
                    else
                    {
                        dr["closingbal"] = "0";
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        dr["crdr"] = "";
                    }

                    dtq.Rows.Add(dr);
                    //}
                    //else
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                    //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                    //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                    //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                    //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                    //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                    //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                    //    dt.Rows.Add(dr);
                    //}
                }
                Session["dtpitemse"] = dtq;
            }
            else
            {
                dtq = (DataTable)Session["dtpitemse"];
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "0";
                //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = 0;
                dr1["creditamt"] = opbal * (-1);
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dr1["chequeno"] = "";
                dtq.Rows.Add(dr1);
                //opbal = opbal * (-1);
                dtq = (DataTable)Session["dtpitemse"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;
                //DataRow dr1 = dtq.NewRow();
                //dr1["voucherno"] = "";
                //dr1["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                //dr1["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                //dr1["description"] = "";
                //dr1["debitamt"] = opbal;
                //dr1["creditamt"] = 0;
                //dr1["closingbal"] = "0";
                //dr1["crdr"] = "";
                //dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                        {
                            //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                        else
                        {
                            //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                            li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            DataTable dtinv = new DataTable();
                            SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                            dainv.Fill(dtinv);
                            //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                    }
                    else
                    {
                        dr["strvoucherno"] = "";
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "PI")
                    {
                        li.strvoucherno = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        DataTable dtchqno = new DataTable();
                        SqlDataAdapter dachqno = new SqlDataAdapter("select * from BankACMaster where cno=" + li.cno + " and istype+convert(varchar(50),voucherno)='" + li.strvoucherno + "'", con);
                        dachqno.Fill(dtchqno);
                        if (dtchqno.Rows.Count > 0)
                        {
                            dr["chequeno"] = dtchqno.Rows[0]["chequeno"].ToString();
                        }
                        else
                        {
                            dr["chequeno"] = "";
                        }
                    }
                    else
                    {
                        dr["chequeno"] = "";
                    }
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                    dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                    dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        if (closingbalance >= 0)
                        {
                            dr["crdr"] = "Dr";
                        }
                        else
                        {
                            dr["crdr"] = "Cr";
                        }
                    }
                    else
                    {
                        dr["closingbal"] = "0";
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        dr["crdr"] = "";
                    }

                    dtq.Rows.Add(dr);
                    //}
                    //else
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                    //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                    //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                    //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                    //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                    //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                    //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                    //    dt.Rows.Add(dr);
                    //}
                }
                Session["dtpitemse"] = dtq;
            }

            //////////rptdoc.SetDataSource(imageDataSet.Tables["DataTable5"]);
        }
        else//for ccode wise ledger
        {
            SqlDataAdapter dacl = new SqlDataAdapter("select * from ClientMaster where code=" + li.ccode + "", con);
            DataTable dtcl = new DataTable();
            dacl.Fill(dtcl);
            if (dtcl.Rows.Count > 0)
            {
                li.clientname = dtcl.Rows[0]["name"].ToString();
            }

            SqlDataAdapter da11 = new SqlDataAdapter("select strvoucherno,voucherdate,creditcode,debitcode,description,istype,(CASE WHEN (type = 'D') THEN amount ELSE 0 END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE 0 END) as Creditamt from ledger where debitcode='" + li.bank1 + "' and cno=" + li.cno + " and istype!='OP' and projectcode=" + li.ccode + " and voucherdate between @fromdate and @todate order by voucherdate,voucherno", con);
            da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable9"]);
            string rptname;
            //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
            //{
            rptname = Server.MapPath(@"~/Reports/ledger.rpt");

            //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
            SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and cno=" + li.cno + " and projectcode=" + li.ccode + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
            dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop = new DataTable();
            dc.Fill(dtop);

            SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and projectcode=" + li.ccode + " and istype!='OP' and voucherdate < @fromdate", con);
            dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop1 = new DataTable();
            dc1.Fill(dtop1);
            double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));
            SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where debitcode='" + li.bank1 + "' and istype='OP'", con);
            DataTable dtac = new DataTable();
            daac.Fill(dtac);
            if (dtcl.Rows.Count > 0)
            {
                if (Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString()) >= 0)
                {
                    opbal = Math.Round(opbal + Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString()), 2);
                }
                else
                {
                    opbal = Math.Round(opbal + (Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString())), 2);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                return;
            }
            //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
            //}
            //else
            //{
            //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
            //    {
            //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
            //    }
            //    else
            //    {
            //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
            //    }
            //}
            double zero = 0;
            DataTable dtq = new DataTable();
            if (opbal >= 0)
            {

                dtq = (DataTable)Session["dtpitemse"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "";
                //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = opbal;
                dr1["creditamt"] = 0;
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dr1["chequeno"] = "";
                dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                        {
                            //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                        else
                        {
                            //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                            li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            DataTable dtinv = new DataTable();
                            SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                            dainv.Fill(dtinv);
                            //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                    }
                    else
                    {
                        dr["strvoucherno"] = "";
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "PI")
                    {
                        li.strvoucherno = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        DataTable dtchqno = new DataTable();
                        SqlDataAdapter dachqno = new SqlDataAdapter("select * from BankACMaster where cno=" + li.cno + " and istype+convert(varchar(50),voucherno)='" + li.strvoucherno + "'", con);
                        dachqno.Fill(dtchqno);
                        if (dtchqno.Rows.Count > 0)
                        {
                            dr["chequeno"] = dtchqno.Rows[0]["chequeno"].ToString();
                        }
                        else
                        {
                            dr["chequeno"] = "";
                        }
                    }
                    else
                    {
                        dr["chequeno"] = "";
                    }
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                    dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                    dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        if (closingbalance >= 0)
                        {
                            dr["crdr"] = "Dr";
                        }
                        else
                        {
                            dr["crdr"] = "Cr";
                        }
                    }
                    else
                    {
                        dr["closingbal"] = "0";
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        dr["crdr"] = "";
                    }

                    dtq.Rows.Add(dr);
                    //}
                    //else
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                    //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                    //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                    //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                    //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                    //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                    //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                    //    dt.Rows.Add(dr);
                    //}
                }
                Session["dtpitemse"] = dtq;
            }
            else
            {
                dtq = (DataTable)Session["dtpitemse"];
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "0";
                //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = 0;
                dr1["creditamt"] = opbal * (-1);
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dr1["chequeno"] = "";
                dtq.Rows.Add(dr1);
                //opbal = opbal * (-1);
                dtq = (DataTable)Session["dtpitemse"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;
                //DataRow dr1 = dtq.NewRow();
                //dr1["voucherno"] = "";
                //dr1["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                //dr1["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                //dr1["description"] = "";
                //dr1["debitamt"] = opbal;
                //dr1["creditamt"] = 0;
                //dr1["closingbal"] = "0";
                //dr1["crdr"] = "";
                //dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                        {
                            //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                        else
                        {
                            //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                            li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            DataTable dtinv = new DataTable();
                            SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                            dainv.Fill(dtinv);
                            //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                            dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        }
                    }
                    else
                    {
                        dr["strvoucherno"] = "";
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI" && imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "PI")
                    {
                        li.strvoucherno = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                        DataTable dtchqno = new DataTable();
                        SqlDataAdapter dachqno = new SqlDataAdapter("select * from BankACMaster where cno=" + li.cno + " and istype+convert(varchar(50),voucherno)='" + li.strvoucherno + "'", con);
                        dachqno.Fill(dtchqno);
                        if (dtchqno.Rows.Count > 0)
                        {
                            dr["chequeno"] = dtchqno.Rows[0]["chequeno"].ToString();
                        }
                        else
                        {
                            dr["chequeno"] = "";
                        }
                    }
                    else
                    {
                        dr["chequeno"] = "";
                    }
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                    dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                    dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                    }
                    if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                    {
                        dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        if (closingbalance >= 0)
                        {
                            dr["crdr"] = "Dr";
                        }
                        else
                        {
                            dr["crdr"] = "Cr";
                        }
                    }
                    else
                    {
                        dr["closingbal"] = "0";
                        closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        dr["crdr"] = "";
                    }

                    dtq.Rows.Add(dr);
                }
                Session["dtpitemse"] = dtq;
            }
        }
        DataTable dtdtq = (DataTable)Session["dtpitemse"];
        if (dtdtq.Rows.Count > 0)
        {
            gvledgerlist.DataSource = dtdtq;
            gvledgerlist.DataBind();
        }
        else
        {
            gvledgerlist.DataSource = null;
            gvledgerlist.DataBind();
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> GetAccountname(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallbankname(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][2].ToString());
    //    }
    //    return CountryNames;
    //}

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> Getvono(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    li.acname = SessionMgt.acname;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallvnofromjv1(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][0].ToString());
    //    }
    //    return CountryNames;
    //}
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        string Xrepname = "";
        if (txtacname.Text.Trim() != string.Empty)
        {
            Xrepname = "Ledger Report";
            li.acname = txtacname.Text;
            if (txtccode.Text != string.Empty)
            {
                li.ccode = Convert.ToInt64(txtccode.Text);
            }
            else
            {
                li.ccode = 0;
            }
            SessionMgt.acname = txtacname.Text;
        }
        else if (txtgroupcode.Text.Trim() != string.Empty)
        {
            li.groupcode = txtgroupcode.Text;
            Xrepname = "Ledger Report For Group";
        }
        else
        {
            Xrepname = "Ledger Report1";
            //li.acname = txtacname.Text;
        }
        //string Xrepname = "Trial Balance Report";
        //Int64 vno = Convert.ToInt64(txtvono.Text);
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?acname=" + li.acname + "&groupcode=" + li.groupcode + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&ccode=" + li.ccode + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////string Xrepname = "Ledger Report";
        //////////Int64 vno = Convert.ToInt64(txtvono.Text);
        ////////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
    //protected void txtacname_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtacname.Text != string.Empty)
    //    {
    //        SessionMgt.acname = txtacname.Text;
    //    }
    //}

    protected void txtccode_TextChanged(object sender, EventArgs e)
    {
        if (txtccode.Text.Trim() != string.Empty)
        {
            string cc = txtccode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtccode.Text = cc.Split('-')[0];
            //SessionMgt.FirstName = cc.Split('-')[1];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtccode.Text = string.Empty;
            //SessionMgt.FirstName = string.Empty;
        }
        Page.SetFocus(txtfromdate);
    }

    public DataTable CreateTemplatec()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("strvoucherno", typeof(string));
        dtpitems.Columns.Add("voucherdate", typeof(DateTime));
        dtpitems.Columns.Add("description", typeof(string));
        dtpitems.Columns.Add("creditcode", typeof(string));
        dtpitems.Columns.Add("debitcode", typeof(string));
        dtpitems.Columns.Add("istype", typeof(string));
        dtpitems.Columns.Add("debitamt", typeof(double));
        dtpitems.Columns.Add("creditamt", typeof(double));
        dtpitems.Columns.Add("closingbal", typeof(double));
        dtpitems.Columns.Add("crdr", typeof(string));
        dtpitems.Columns.Add("chequeno", typeof(string));
        return dtpitems;
    }

    protected void btnfilll_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }

        if (chkmonthwise.Checked == false)
        {
            li.bank1 = txtacname.Text;
            Session["dtpitemse"] = CreateTemplatec();
            li.fromdate = txtfromdate.Text;
            li.todate = txttodate.Text;
            if (txtccode.Text.Trim() != string.Empty)
            {
                li.ccode = Convert.ToInt64(txtccode.Text);
            }
            else
            {
                li.ccode = 0;
            }
            li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            if (li.ccode == 0)
            {
                SqlDataAdapter da11 = new SqlDataAdapter("select strvoucherno,voucherdate,creditcode,debitcode,description,istype,(CASE WHEN (type = 'D') THEN amount ELSE 0 END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE 0 END) as Creditamt from ledger where debitcode='" + li.bank1 + "' and cno=" + li.cno + " and istype!='OP' and voucherdate between @fromdate and @todate order by voucherdate,voucherno", con);
                da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtc = new DataTable();
                da11.Fill(imageDataSet.Tables["DataTable9"]);
                string rptname;
                //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                //{
                rptname = Server.MapPath(@"~/Reports/ledger.rpt");

                //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
                SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
                dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop = new DataTable();
                dc.Fill(dtop);

                SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
                dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop1 = new DataTable();
                dc1.Fill(dtop1);
                double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));
                SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where debitcode='" + li.bank1 + "' and istype='OP'", con);
                DataTable dtac = new DataTable();
                daac.Fill(dtac);
                if (dtac.Rows.Count > 0)
                {
                    if (Convert.ToDouble(dtac.Rows[0]["amount"].ToString()) >= 0)
                    {
                        opbal = Math.Round(opbal + Convert.ToDouble(dtac.Rows[0]["amount"].ToString()), 2);
                    }
                    else
                    {
                        opbal = Math.Round(opbal + (Convert.ToDouble(dtac.Rows[0]["amount"].ToString())), 2);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                    return;
                }
                //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                //}
                //else
                //{
                //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                //    {
                //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                //    }
                //    else
                //    {
                //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                //    }
                //}
                double zero = 0;
                DataTable dtq = new DataTable();
                if (opbal >= 0)
                {

                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = opbal;
                    dr1["creditamt"] = 0;
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                        //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                        //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                        //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                        //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                        //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                        //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                        //    dt.Rows.Add(dr);
                        //}
                    }
                    Session["dtpitemse"] = dtq;
                }
                else
                {
                    dtq = (DataTable)Session["dtpitemse"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "0";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = 0;
                    dr1["creditamt"] = opbal * (-1);
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    //opbal = opbal * (-1);
                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    //DataRow dr1 = dtq.NewRow();
                    //dr1["voucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    //dr1["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    //dr1["description"] = "";
                    //dr1["debitamt"] = opbal;
                    //dr1["creditamt"] = 0;
                    //dr1["closingbal"] = "0";
                    //dr1["crdr"] = "";
                    //dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                        //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                        //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                        //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                        //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                        //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                        //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                        //    dt.Rows.Add(dr);
                        //}
                    }
                    Session["dtpitemse"] = dtq;
                }

                //////////rptdoc.SetDataSource(imageDataSet.Tables["DataTable5"]);
            }
            else//for ccode wise ledger
            {
                SqlDataAdapter dacl = new SqlDataAdapter("select * from ClientMaster where code=" + li.ccode + "", con);
                DataTable dtcl = new DataTable();
                dacl.Fill(dtcl);
                if (dtcl.Rows.Count > 0)
                {
                    li.clientname = dtcl.Rows[0]["name"].ToString();
                }

                SqlDataAdapter da11 = new SqlDataAdapter("select strvoucherno,voucherdate,creditcode,debitcode,description,istype,(CASE WHEN (type = 'D') THEN amount ELSE 0 END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE 0 END) as Creditamt from ledger where debitcode='" + li.bank1 + "' and cno=" + li.cno + " and istype!='OP' and projectcode=" + li.ccode + " and voucherdate between @fromdate and @todate order by voucherdate,voucherno", con);
                da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtc = new DataTable();
                da11.Fill(imageDataSet.Tables["DataTable9"]);
                string rptname;
                //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                //{
                rptname = Server.MapPath(@"~/Reports/ledger.rpt");

                //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
                SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and cno=" + li.cno + " and projectcode=" + li.ccode + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
                dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop = new DataTable();
                dc.Fill(dtop);

                SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and projectcode=" + li.ccode + " and istype!='OP' and voucherdate < @fromdate", con);
                dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop1 = new DataTable();
                dc1.Fill(dtop1);
                double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));
                SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where debitcode='" + li.bank1 + "' and istype='OP'", con);
                DataTable dtac = new DataTable();
                daac.Fill(dtac);
                if (dtcl.Rows.Count > 0)
                {
                    if (Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString()) >= 0)
                    {
                        opbal = Math.Round(opbal + Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString()), 2);
                    }
                    else
                    {
                        opbal = Math.Round(opbal + (Convert.ToDouble(dtcl.Rows[0]["opbalance"].ToString())), 2);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                    return;
                }
                //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                //}
                //else
                //{
                //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                //    {
                //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                //    }
                //    else
                //    {
                //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                //    }
                //}
                double zero = 0;
                DataTable dtq = new DataTable();
                if (opbal >= 0)
                {

                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = opbal;
                    dr1["creditamt"] = 0;
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                        //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                        //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                        //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                        //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                        //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                        //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                        //    dt.Rows.Add(dr);
                        //}
                    }
                    Session["dtpitemse"] = dtq;
                }
                else
                {
                    dtq = (DataTable)Session["dtpitemse"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "0";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = 0;
                    dr1["creditamt"] = opbal * (-1);
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    //opbal = opbal * (-1);
                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    //DataRow dr1 = dtq.NewRow();
                    //dr1["voucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    //dr1["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    //dr1["description"] = "";
                    //dr1["debitamt"] = opbal;
                    //dr1["creditamt"] = 0;
                    //dr1["closingbal"] = "0";
                    //dr1["crdr"] = "";
                    //dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                    }
                    Session["dtpitemse"] = dtq;
                }
            }
            DataTable dtdtq = (DataTable)Session["dtpitemse"];
            if (dtdtq.Rows.Count > 0)
            {
                gvmonthlyledger.Visible = false;
                gvledgerlist.DataSource = dtdtq;
                gvledgerlist.DataBind();
            }
            else
            {
                gvmonthlyledger.Visible = false;
                gvledgerlist.DataSource = null;
                gvledgerlist.DataBind();
            }
        }
        else
        {
            li.acname = txtacname.Text;
            ViewState["clop"] = "";
            Session["dtpitems"] = CreateTemplate();
            DataTable dtq = new DataTable();
            DataTable dtall = new DataTable();
            int fromyear = Convert.ToInt16(Request.Cookies["ForLogin"]["acyear"].Split('-')[0]);
            int toyear = Convert.ToInt16(Request.Cookies["ForLogin"]["acyear"].Split('-')[1]);
            for (int a = 4; a < 13; a++)
            {
                string cz = Request.Cookies["Forcon"]["conc"];
                cz = cz.Replace(":", ";");
                SqlConnection con = new SqlConnection(cz);
                dtq = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq.NewRow();
                dr1["month"] = a;
                dr1["year"] = fromyear;
                DataTable dtop = new DataTable();
                if (a == 4)
                {
                    SqlDataAdapter da = new SqlDataAdapter("select isnull(sum(amount),0) as opbal from ledger where istype='OP' and debitcode='" + li.acname + "'", con);
                    da.Fill(dtop);
                    dr1["openingamt"] = dtop.Rows[0]["opbal"].ToString();
                }
                else
                {
                    dr1["openingamt"] = ViewState["clop"].ToString();
                }
                SqlDataAdapter da1 = new SqlDataAdapter("select isnull(sum(amount),0) as debitbal from ledger where type='D' and istype<>'OP' and debitcode='" + li.acname + "' and month(voucherdate)=" + a + " and year(voucherdate)=" + fromyear + "", con);
                DataTable dtdebit = new DataTable();
                da1.Fill(dtdebit);
                dr1["debitamt"] = dtdebit.Rows[0]["debitbal"].ToString();
                SqlDataAdapter da2 = new SqlDataAdapter("select isnull(sum(amount),0) as creditbal from ledger where type='C' and istype<>'OP' and debitcode='" + li.acname + "' and month(voucherdate)=" + a + " and year(voucherdate)=" + fromyear + "", con);
                DataTable dtcredit = new DataTable();
                da2.Fill(dtcredit);
                dr1["creditamt"] = dtcredit.Rows[0]["creditbal"].ToString();
                double op = 0, debit = 0, credit = 0, closing = 0;
                if (a == 4)
                {
                    op = Convert.ToDouble(dtop.Rows[0]["opbal"].ToString());
                }
                else
                {
                    op = Convert.ToDouble(ViewState["clop"].ToString());
                }
                debit = Convert.ToDouble(dtdebit.Rows[0]["debitbal"].ToString());
                credit = Convert.ToDouble(dtcredit.Rows[0]["creditbal"].ToString());
                closing = op + debit - credit;
                dr1["closingbal"] = Math.Round(closing, 2);
                ViewState["clop"] = closing;
                if (closing >= 0)
                {
                    dr1["crdr"] = "Dr";
                }
                else
                {
                    dr1["crdr"] = "Cr";
                }
                if (debit > 0 || credit > 0)
                {
                    dtq.Rows.Add(dr1);
                }
            }
            for (int b = 1; b < 4; b++)
            {
                string cz = Request.Cookies["Forcon"]["conc"];
                cz = cz.Replace(":", ";");
                SqlConnection con = new SqlConnection(cz);
                dtq = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq.NewRow();
                dr1["month"] = b;
                dr1["year"] = toyear;
                //if (a == 4)
                //{
                //    SqlDataAdapter da = new SqlDataAdapter("select isnull(sum(amount),0) as opbal from ledger where istype='OP'", con);
                //    DataTable dtop = new DataTable();
                //    da.Fill(dtop);
                //    dr1["openingamt"] = dtop.Rows[0]["opbal"].ToString();
                //}
                //else
                //{
                dr1["openingamt"] = ViewState["clop"].ToString();
                //}
                SqlDataAdapter da1 = new SqlDataAdapter("select isnull(sum(amount),0) as debitbal from ledger where type='D' and istype<>'OP' and debitcode='" + li.acname + "' and month(voucherdate)=" + b + " and year(voucherdate)=" + toyear + "", con);
                DataTable dtdebit = new DataTable();
                da1.Fill(dtdebit);
                dr1["debitamt"] = dtdebit.Rows[0]["debitbal"].ToString();
                SqlDataAdapter da2 = new SqlDataAdapter("select isnull(sum(amount),0) as creditbal from ledger where type='C' and istype<>'OP' and debitcode='" + li.acname + "' and month(voucherdate)=" + b + " and year(voucherdate)=" + toyear + "", con);
                DataTable dtcredit = new DataTable();
                da2.Fill(dtcredit);
                dr1["creditamt"] = dtcredit.Rows[0]["creditbal"].ToString();
                double op = 0, debit = 0, credit = 0, closing = 0;
                op = Convert.ToDouble(ViewState["clop"].ToString());
                debit = Convert.ToDouble(dtdebit.Rows[0]["debitbal"].ToString());
                credit = Convert.ToDouble(dtcredit.Rows[0]["creditbal"].ToString());
                closing = op + debit - credit;
                dr1["closingbal"] = Math.Round(closing, 2);
                ViewState["clop"] = closing;
                if (closing >= 0)
                {
                    dr1["crdr"] = "Dr";
                }
                else
                {
                    dr1["crdr"] = "Cr";
                }
                if (debit > 0 || credit > 0)
                {
                    dtq.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq;
            if (dtq.Rows.Count > 0)
            {
                gvledgerlist.Visible = false;
                gvmonthlyledger.DataSource = dtq;
                gvmonthlyledger.DataBind();
            }
            //string olddbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        }
    }
    protected void gvledgerlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (txtccode.Text.Trim() != string.Empty)
            {
                li.ccode = Convert.ToInt64(txtccode.Text);
            }
            else
            {
                li.ccode = 0;
            }
            SessionMgt.acname = txtacname.Text;
            SessionMgt.ccode = li.ccode;
            SessionMgt.fromdate = txtfromdate.Text;
            SessionMgt.todate = txttodate.Text;
            //&acname=" + txtacname.Text + "&ccode=" + li.ccode + "&fromdate=" + txtfromdate.Text + "&todate=" + txttodate.Text + "//
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblstrvoucherno = (Label)currentRow.FindControl("lblstrvoucherno");
            Label lblistype = (Label)currentRow.FindControl("lblistype");
            li.strvoucherno = lblstrvoucherno.Text;
            li.istype = lblistype.Text;
            if (li.istype == "BR")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/BankReceipt.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "BP")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/BankPayment.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "CR")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/CashReceiptEntry.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "CP")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/CashPaymentEntry.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "PC")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/PettyCashExpense.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "JV")
            {
                li.voucherno = Convert.ToInt64(Regex.Match(li.strvoucherno, @"\d+").Value);
                Response.Redirect("~/JournalVoucher.aspx?mode=ledger&vno=" + li.voucherno + "");
            }
            else if (li.istype == "PI")
            {
                Response.Redirect("~/PurchaseInvoice.aspx?mode=ledger&strpino=" + li.strvoucherno + "");
            }
            else
            {
                Response.Redirect("~/SalesInvoice.aspx?mode=ledger&strsino=" + li.strvoucherno + "");
            }
        }
    }

    protected void gvmonthlyledger_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "selectmonth")
        {
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblmonth = (Label)currentRow.FindControl("lblmonth");
            Label lblyear = (Label)currentRow.FindControl("lblyear");
            Label lblopbal = (Label)currentRow.FindControl("lblopening");
            li.bank1 = txtacname.Text;
            Session["dtpitemse"] = CreateTemplatec();
            int frommonth = Convert.ToInt16(lblmonth.Text);
            int fromyear = Convert.ToInt16(lblyear.Text);
            if (txtccode.Text.Trim() != string.Empty)
            {
                li.ccode = Convert.ToInt64(txtccode.Text);
            }
            else
            {
                li.ccode = 0;
            }
            li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            if (li.ccode == 0)
            {
                SqlDataAdapter da11 = new SqlDataAdapter("select strvoucherno,voucherdate,creditcode,debitcode,description,istype,(CASE WHEN (type = 'D') THEN amount ELSE 0 END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE 0 END) as Creditamt from ledger where debitcode='" + li.bank1 + "' and cno=" + li.cno + " and istype!='OP' and month(voucherdate)=" + frommonth + " and year(voucherdate)=" + fromyear + " order by voucherdate,voucherno", con);
                //da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtc = new DataTable();
                da11.Fill(imageDataSet.Tables["DataTable9"]);
                string rptname;
                //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                //{
                rptname = Server.MapPath(@"~/Reports/ledger.rpt");

                //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
                SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and month(voucherdate)=" + frommonth + " and year(voucherdate)=" + fromyear + "", con);
                //dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop = new DataTable();
                dc.Fill(dtop);

                SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and month(voucherdate)=" + frommonth + " and year(voucherdate)=" + fromyear + "", con);
                //dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtop1 = new DataTable();
                dc1.Fill(dtop1);
                double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));
                SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where debitcode='" + li.bank1 + "' and istype='OP'", con);
                DataTable dtac = new DataTable();
                daac.Fill(dtac);
                //if (dtac.Rows.Count > 0)
                //{

                opbal = Math.Round((Convert.ToDouble(lblopbal.Text)), 2);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                //    return;
                //}
                //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                //}
                //else
                //{
                //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                //    {
                //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                //    }
                //    else
                //    {
                //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                //    }
                //}
                double zero = 0;
                DataTable dtq = new DataTable();
                if (opbal >= 0)
                {

                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = opbal;
                    dr1["creditamt"] = 0;
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                        //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                        //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                        //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                        //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                        //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                        //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                        //    dt.Rows.Add(dr);
                        //}
                    }
                    Session["dtpitemse"] = dtq;
                }
                else
                {
                    dtq = (DataTable)Session["dtpitemse"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["strvoucherno"] = "0";
                    //dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["voucherdate"] = Convert.ToDateTime(li.fromdated, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    dr1["description"] = "";
                    dr1["debitamt"] = 0;
                    dr1["creditamt"] = opbal * (-1);
                    dr1["closingbal"] = "0";
                    dr1["crdr"] = "";
                    dtq.Rows.Add(dr1);
                    //opbal = opbal * (-1);
                    dtq = (DataTable)Session["dtpitemse"];
                    double camt = 0;
                    double damt = 0;
                    double closingbalance = opbal;
                    //DataRow dr1 = dtq.NewRow();
                    //dr1["voucherno"] = "";
                    //dr1["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //dr1["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["debitcode"].ToString();
                    //dr1["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[0]["creditcode"].ToString();
                    //dr1["istype"] = "";
                    //dr1["description"] = "";
                    //dr1["debitamt"] = opbal;
                    //dr1["creditamt"] = 0;
                    //dr1["closingbal"] = "0";
                    //dr1["crdr"] = "";
                    //dtq.Rows.Add(dr1);
                    for (int zc = 0; zc < imageDataSet.Tables["DataTable9"].Rows.Count; zc++)
                    {
                        //if (zc == 0)
                        //{
                        DataRow dr = dtq.NewRow();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "SI")
                            {
                                //dr["voucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                            else
                            {
                                //li.invoiceno = Convert.ToInt64(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString());
                                li.strsino = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                                DataTable dtinv = new DataTable();
                                SqlDataAdapter dainv = new SqlDataAdapter("select * from SIMaster where cno=" + li.cno + " and strsino='" + li.strsino + "'", con);
                                dainv.Fill(dtinv);
                                //dr["voucherno"] = dtinv.Rows[0]["invtype"].ToString() + imageDataSet.Tables["DataTable9"].Rows[zc]["voucherno"].ToString();
                                dr["strvoucherno"] = imageDataSet.Tables["DataTable9"].Rows[zc]["strvoucherno"].ToString();
                            }
                        }
                        else
                        {
                            dr["strvoucherno"] = "";
                        }
                        dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable9"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["debitcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["debitcode"].ToString();
                        dr["creditcode"] = imageDataSet.Tables["DataTable9"].Rows[zc]["creditcode"].ToString();
                        dr["istype"] = imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString();
                        dr["description"] = imageDataSet.Tables["DataTable9"].Rows[zc]["description"].ToString();
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString() != null)
                        {
                            dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                            damt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString() != null)
                        {
                            dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            camt = Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                        }
                        if (imageDataSet.Tables["DataTable9"].Rows[zc]["istype"].ToString() != "OP")
                        {
                            dr["closingbal"] = Math.Round((closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString()), 2);
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            if (closingbalance >= 0)
                            {
                                dr["crdr"] = "Dr";
                            }
                            else
                            {
                                dr["crdr"] = "Cr";
                            }
                        }
                        else
                        {
                            dr["closingbal"] = "0";
                            closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable9"].Rows[zc]["creditamt"].ToString());
                            dr["crdr"] = "";
                        }

                        dtq.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                        //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                        //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                        //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                        //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                        //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                        //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                        //    dt.Rows.Add(dr);
                        //}
                    }
                    Session["dtpitemse"] = dtq;
                }

                //////////rptdoc.SetDataSource(imageDataSet.Tables["DataTable5"]);
            }
            DataTable dtdtq = (DataTable)Session["dtpitemse"];
            if (dtdtq.Rows.Count > 0)
            {
                gvmonthlyledger.Visible = false;
                gvledgerlist.Visible = true;
                gvledgerlist.DataSource = dtdtq;
                gvledgerlist.DataBind();
            }
            else
            {
                gvmonthlyledger.Visible = false;
                gvledgerlist.DataSource = null;
                gvledgerlist.DataBind();
            }
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("month", typeof(int));
        dtpitems.Columns.Add("year", typeof(int));
        dtpitems.Columns.Add("openingamt", typeof(double));
        dtpitems.Columns.Add("debitamt", typeof(double));
        dtpitems.Columns.Add("creditamt", typeof(double));
        dtpitems.Columns.Add("closingbal", typeof(double));
        dtpitems.Columns.Add("crdr", typeof(string));
        return dtpitems;
    }
}