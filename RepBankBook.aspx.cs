﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class RepBankBook : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountnamebankch(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }

        string Xrepname = "Bank Book Report";
        string bank = txtacname.Text;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?bank=" + bank + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////string Xrepname = "Ledger Report";
        //////////Int64 vno = Convert.ToInt64(txtvono.Text);
        ////////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }

    public DataTable CreateTemplate2()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("strvoucherno", typeof(string));
        dtpitems.Columns.Add("voucherdate", typeof(DateTime));
        dtpitems.Columns.Add("creditcode", typeof(string));
        dtpitems.Columns.Add("description", typeof(string));
        dtpitems.Columns.Add("debitamt", typeof(double));
        dtpitems.Columns.Add("creditamt", typeof(double));
        dtpitems.Columns.Add("closingbal", typeof(double));
        dtpitems.Columns.Add("crdr", typeof(string));
        return dtpitems;
    }

    protected void btnreconcile_Click(object sender, EventArgs e)
    {
        if (txtfromdate.Text != string.Empty && txtacname.Text != string.Empty)
        {
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
           // li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //int zz = xxyear.IndexOf(yyyear);
            if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear))
            {

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
                return;
            }

            DataTable dtall = new DataTable();
            li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            li.bank1 = txtacname.Text;
            SqlDataAdapter da = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype,BankACMaster.chequeno from bankacmaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where bankacmaster.istype='BR' and bankacmaster.name='" + li.bank1 + "' and bankacmaster.voucherdate between @fromdate and @todate", con);
            da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtbr = new DataTable();
            da.Fill(dtbr);
            dtall.Merge(dtbr);

            SqlDataAdapter da1 = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype,BankACMaster.chequeno from bankacmaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where bankacmaster.istype='BP' and bankacmaster.name='" + li.bank1 + "' and bankacmaster.voucherdate between @fromdate and @todate", con);
            da1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            da1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtbp = new DataTable();
            da1.Fill(dtbp);
            dtall.Merge(dtbp);
            if (dtall.Rows.Count > 0)
            {
                lblemptyreconcile.Visible = false;
                gvreconcile.Visible = true;
                gvreconcile.DataSource = dtall;
                gvreconcile.DataBind();
                btnprint.Visible = true;
            }
            else
            {
                gvreconcile.Visible = false;
                lblemptyreconcile.Visible = true;
                lblemptyreconcile.Text = "No Data Found.";
                btnprint.Visible = false;
            }

            //Code to fill Clear Balance Label
            DataTable dtallstock = new DataTable();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            DataSet1 imageDataSet = new DataSet1();
            //li.ccode = Convert.ToInt64(Request["ccode"].ToString());
            Session["dtpitems"] = CreateTemplate2();
            li.fromdate = txtfromdate.Text;
            li.todate = txttodate.Text;
            li.bank1 = txtacname.Text;
            li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

            SqlDataAdapter da11 = new SqlDataAdapter("select ledger.strvoucherno,ledger.voucherdate,ledger.creditcode,ledger.description,ledger.istype,(CASE WHEN (ledger.type = 'D') THEN ledger.amount ELSE 0 END) as Debitamt,(CASE WHEN (ledger.type = 'C') THEN ledger.amount ELSE 0 END) as Creditamt from ledger where cno=" + li.cno + " and ledger.debitcode='" + li.bank1 + "' and istype!='OP' and (ledger.voucherdate < @fromdate or convert(varchar(50),ledger.voucherdate,105) = convert(varchar(50),@fromdate,105)) order by ledger.voucherdate", con);
            da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
            //da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable5"]);

            SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where istype!='OP' and type='C' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and (voucherdate < @fromdate or convert(varchar(50),voucherdate,105) = convert(varchar(50),@fromdate,105))", con);
            dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
            //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop = new DataTable();
            dc.Fill(dtop);
            string rptname;
            rptname = Server.MapPath(@"~/Reports/Reconcile.rpt");
            SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where istype!='OP' and type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and (voucherdate < @fromdate or convert(varchar(50),voucherdate,105) = convert(varchar(50),@fromdate,105))", con);
            dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
            //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop1 = new DataTable();
            dc1.Fill(dtop1);

            double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));

            SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype='OP'", con);
            DataTable dtac = new DataTable();
            daac.Fill(dtac);
            if (dtac.Rows.Count > 0)
            {
                if (Convert.ToDouble(dtac.Rows[0]["amount"].ToString()) >= 0)
                {
                    opbal = Math.Round(opbal + Convert.ToDouble(dtac.Rows[0]["amount"].ToString()), 2);
                }
                else
                {
                    opbal = Math.Round(opbal + (Convert.ToDouble(dtac.Rows[0]["amount"].ToString())), 2);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
                return;
            }
            double zero = 0;
            DataTable dtq = new DataTable();
            if (opbal >= 0)
            {
                //rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + opbal + "'";
                //rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + zero + "'";

                dtq = (DataTable)Session["dtpitems"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "";
                dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = dtac.Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = opbal;
                dr1["creditamt"] = 0;
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable5"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    //dr["voucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable5"].Rows[zc]["voucherno"].ToString();
                    dr["strvoucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["strvoucherno"].ToString();
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable5"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["creditcode"] = imageDataSet.Tables["DataTable5"].Rows[zc]["creditcode"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable5"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                        //if (damt == 0)
                        //{
                        //    dr["debitamt"] = "";
                        //}
                        //else
                        //{
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                        //}
                    }
                    if (imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                        //if (camt == 0)
                        //{
                        //    dr["creditamt"] = "";
                        //}
                        //else
                        //{
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                        //}
                    }
                    dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    if (closingbalance >= 0)
                    {
                        dr["crdr"] = "Dr";
                    }
                    else
                    {
                        dr["crdr"] = "Cr";
                    }
                    dtq.Rows.Add(dr);
                    //}
                    //else
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                    //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                    //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                    //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                    //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                    //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                    //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                    //    dt.Rows.Add(dr);
                    //}
                }
                Session["dtpitems"] = dtq;
            }
            else
            {
                //rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + zero + "'";
                //rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + opbal * (-1) + "'";
                //opbal = opbal * (-1);
                dtq = (DataTable)Session["dtpitems"];
                double camt = 0;
                double damt = 0;
                double closingbalance = opbal;//*(-1)
                DataRow dr1 = dtq.NewRow();
                dr1["strvoucherno"] = "";
                dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //dr1["debitcode"] = dtac.Rows[0]["debitcode"].ToString();
                dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
                //dr1["istype"] = "";
                dr1["description"] = "";
                dr1["debitamt"] = 0;
                dr1["creditamt"] = opbal * (-1);
                dr1["closingbal"] = "0";
                dr1["crdr"] = "";
                dtq.Rows.Add(dr1);
                for (int zc = 0; zc < imageDataSet.Tables["DataTable5"].Rows.Count; zc++)
                {
                    //if (zc == 0)
                    //{
                    DataRow dr = dtq.NewRow();
                    //dr["voucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable5"].Rows[zc]["voucherno"].ToString();
                    dr["strvoucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["strvoucherno"].ToString();
                    dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable5"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr["creditcode"] = imageDataSet.Tables["DataTable5"].Rows[zc]["creditcode"].ToString();
                    dr["description"] = imageDataSet.Tables["DataTable5"].Rows[zc]["description"].ToString();
                    if (imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != null)
                    {
                        damt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                        //if (damt == 0)
                        //{
                        //    dr["debitamt"] = "";
                        //}
                        //else
                        //{
                        dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                        //}
                    }
                    if (imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != null)
                    {
                        camt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                        //if (camt == 0)
                        //{
                        //    dr["creditamt"] = "";
                        //}
                        //else
                        //{
                        dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                        //}
                    }
                    dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    if (closingbalance >= 0)
                    {
                        dr["crdr"] = "Dr";
                    }
                    else
                    {
                        //closingbalance = closingbalance * (-1);
                        dr["closingbal"] = closingbalance * (-1);
                        dr["crdr"] = "Cr";
                    }
                    dtq.Rows.Add(dr);
                    //}
                    //else
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                    //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                    //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                    //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                    //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                    //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                    //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                    //    dt.Rows.Add(dr);
                    //}
                }
                Session["dtpitems"] = dtq;
            }

            SqlDataAdapter dax = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where bankacmaster.istype='BR' and bankacmaster.name='" + li.bank1 + "' and (cldate is null or cldate='') and (bankacmaster.voucherdate<@fromdate or convert(varchar(50),bankacmaster.voucherdate,105)=convert(varchar(50),@fromdate,105))", con);
            dax.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
            DataTable dtbrx = new DataTable();
            dax.Fill(imageDataSet.Tables["DataTable38"]);

            SqlDataAdapter dax1 = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where bankacmaster.istype='BP' and bankacmaster.name='" + li.bank1 + "' and (cldate is null or cldate='') and (bankacmaster.voucherdate<@fromdate or convert(varchar(50),bankacmaster.voucherdate,105)=convert(varchar(50),@fromdate,105))", con);
            dax1.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
            DataTable dtbpx = new DataTable();
            dax1.Fill(imageDataSet.Tables["DataTable381"]);

            DataTable dtallx = new DataTable();
            dtallx.Merge(imageDataSet.Tables["DataTable38"]);
            dtallx.Merge(imageDataSet.Tables["DataTable381"]);
            double amt1 = 0;
            for (int c = 0; c < imageDataSet.Tables["DataTable38"].Rows.Count; c++)
            {
                amt1 = amt1 + Convert.ToDouble(imageDataSet.Tables["DataTable38"].Rows[c]["amount"].ToString());
            }
            double amt2 = 0;
            for (int c1 = 0; c1 < imageDataSet.Tables["DataTable381"].Rows.Count; c1++)
            {
                amt2 = amt2 + Convert.ToDouble(imageDataSet.Tables["DataTable381"].Rows[c1]["amount"].ToString());
            }
            double finalamt = 0;
            if (dtq.Rows.Count > 0)
            {
                finalamt = opbal + Math.Round(amt2, 2) - Math.Round(amt1, 2);
                finalamt = Math.Round(finalamt, 2);
            }
            else
            {
                finalamt = Math.Round(amt2, 2) - Math.Round(amt1, 2);
                finalamt = Math.Round(finalamt, 2);
            }
            if (finalamt >= 0)
            {
                lblclearbalance.Text = finalamt.ToString() + " Dr";
            }
            else
            {
                lblclearbalance.Text = (finalamt * (-1)).ToString() + " Cr";
            }
            //

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter From Date OR ACName and Try Again.');", true);
            return;
        }
    }

    protected void txtcldate_TextChanged(object sender, EventArgs e)
    {
        DataTable dtall = new DataTable();
        SqlDateTime sqldatenull = SqlDateTime.Null;
        SqlDateTime cldate;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblvoucherno = (Label)currentRow.FindControl("lblvoucherno");
        Label lblistype = (Label)currentRow.FindControl("lblistype");
        TextBox txtcldate = (TextBox)currentRow.FindControl("txtcldate");
        if (txtcldate.Text != string.Empty)
        {
            cldate = Convert.ToDateTime(txtcldate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            cldate = sqldatenull;
        }
        li.voucherno = Convert.ToInt64(lblvoucherno.Text);
        li.istype = lblistype.Text;
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand cmd = new SqlCommand("update bankacmaster set cldate=@cldate where voucherno=@voucherno and istype=@istype", con);
        cmd.Parameters.AddWithValue("@cldate", cldate);
        cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
        cmd.Parameters.AddWithValue("@istype", li.istype);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.bank1 = txtacname.Text;
        SqlDataAdapter da = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.voucherno=BankACMaster1.voucherno where bankacmaster.istype='BR' and bankacmaster.name='" + li.bank1 + "' and bankacmaster.voucherdate between @fromdate and @todate group by bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105),bankacmaster.istype", con);
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtbr = new DataTable();
        da.Fill(dtbr);
        dtall.Merge(dtbr);

        SqlDataAdapter da1 = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.voucherno=BankACMaster1.voucherno where bankacmaster.istype='BP' and bankacmaster.name='" + li.bank1 + "' and bankacmaster.voucherdate between @fromdate and @todate group by bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105),bankacmaster.istype", con);
        da1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtbp = new DataTable();
        da1.Fill(dtbp);
        dtall.Merge(dtbp);
        if (dtall.Rows.Count > 0)
        {
            lblemptyreconcile.Visible = false;
            gvreconcile.Visible = true;
            gvreconcile.DataSource = dtall;
            gvreconcile.DataBind();
            btnprint.Visible = true;
        }
        else
        {
            gvreconcile.Visible = false;
            lblemptyreconcile.Visible = true;
            lblemptyreconcile.Text = "No Data Found.";
            btnprint.Visible = false;
        }

        TextBox txtcldate1 = (TextBox)currentRow.FindControl("txtcldate");
        Page.SetFocus(txtcldate1);
        DataTable dtallstock = new DataTable();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        //li.ccode = Convert.ToInt64(Request["ccode"].ToString());
        Session["dtpitems"] = CreateTemplate2();
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        li.bank1 = txtacname.Text;
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

        SqlDataAdapter da11 = new SqlDataAdapter("select ledger.strvoucherno,ledger.voucherdate,ledger.creditcode,ledger.description,ledger.istype,(CASE WHEN (ledger.type = 'D') THEN ledger.amount ELSE 0 END) as Debitamt,(CASE WHEN (ledger.type = 'C') THEN ledger.amount ELSE 0 END) as Creditamt from ledger where cno=" + li.cno + " and ledger.debitcode='" + li.bank1 + "' and istype!='OP' and ledger.voucherdate < @fromdate order by ledger.voucherdate", con);
        da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
        //da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtc = new DataTable();
        da11.Fill(imageDataSet.Tables["DataTable5"]);

        SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where istype!='OP' and type='C' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
        dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
        //dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtop = new DataTable();
        dc.Fill(dtop);
        string rptname;
        rptname = Server.MapPath(@"~/Reports/Reconcile.rpt");
        SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where istype!='OP' and type='D' and cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype!='OP' and voucherdate < @fromdate", con);
        dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
        //dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtop1 = new DataTable();
        dc1.Fill(dtop1);

        double opbal = (Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString())) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()));

        SqlDataAdapter daac = new SqlDataAdapter("select * from ledger where cno=" + li.cno + " and debitcode='" + li.bank1 + "' and istype='OP'", con);
        DataTable dtac = new DataTable();
        daac.Fill(dtac);
        if (dtac.Rows.Count > 0)
        {
            if (Convert.ToDouble(dtac.Rows[0]["amount"].ToString()) >= 0)
            {
                opbal = Math.Round(opbal + Convert.ToDouble(dtac.Rows[0]["amount"].ToString()), 2);
            }
            else
            {
                opbal = Math.Round(opbal + (Convert.ToDouble(dtac.Rows[0]["amount"].ToString())), 2);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Go to Account Master.Enter Opening Balance and Try Again.');", true);
            return;
        }
        double zero = 0;
        DataTable dtq = new DataTable();
        if (opbal >= 0)
        {
            //rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + opbal + "'";
            //rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + zero + "'";

            dtq = (DataTable)Session["dtpitems"];
            double camt = 0;
            double damt = 0;
            double closingbalance = opbal;
            DataRow dr1 = dtq.NewRow();
            dr1["strvoucherno"] = "";
            dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //dr1["debitcode"] = dtac.Rows[0]["debitcode"].ToString();
            dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
            //dr1["istype"] = "";
            dr1["description"] = "";
            dr1["debitamt"] = opbal;
            dr1["creditamt"] = 0;
            dr1["closingbal"] = "0";
            dr1["crdr"] = "";
            dtq.Rows.Add(dr1);
            for (int zc = 0; zc < imageDataSet.Tables["DataTable5"].Rows.Count; zc++)
            {
                //if (zc == 0)
                //{
                DataRow dr = dtq.NewRow();
                //dr["voucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable5"].Rows[zc]["voucherno"].ToString();
                dr["strvoucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["strvoucherno"].ToString();
                dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable5"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["creditcode"] = imageDataSet.Tables["DataTable5"].Rows[zc]["creditcode"].ToString();
                dr["description"] = imageDataSet.Tables["DataTable5"].Rows[zc]["description"].ToString();
                if (imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != null)
                {
                    damt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                    //if (damt == 0)
                    //{
                    //    dr["debitamt"] = "";
                    //}
                    //else
                    //{
                    dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                    //}
                }
                if (imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != null)
                {
                    camt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    //if (camt == 0)
                    //{
                    //    dr["creditamt"] = "";
                    //}
                    //else
                    //{
                    dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    //}
                }
                dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                if (closingbalance >= 0)
                {
                    dr["crdr"] = "Dr";
                }
                else
                {
                    dr["crdr"] = "Cr";
                }
                dtq.Rows.Add(dr);
                //}
                //else
                //{
                //    DataRow dr = dt.NewRow();
                //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                //    dt.Rows.Add(dr);
                //}
            }
            Session["dtpitems"] = dtq;
        }
        else
        {
            //rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + zero + "'";
            //rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + opbal * (-1) + "'";
            //opbal = opbal * (-1);
            dtq = (DataTable)Session["dtpitems"];
            double camt = 0;
            double damt = 0;
            double closingbalance = opbal;//*(-1)
            DataRow dr1 = dtq.NewRow();
            dr1["strvoucherno"] = "";
            dr1["voucherdate"] = Convert.ToDateTime(dtac.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //dr1["debitcode"] = dtac.Rows[0]["debitcode"].ToString();
            dr1["creditcode"] = dtac.Rows[0]["creditcode"].ToString();
            //dr1["istype"] = "";
            dr1["description"] = "";
            dr1["debitamt"] = 0;
            dr1["creditamt"] = opbal * (-1);
            dr1["closingbal"] = "0";
            dr1["crdr"] = "";
            dtq.Rows.Add(dr1);
            for (int zc = 0; zc < imageDataSet.Tables["DataTable5"].Rows.Count; zc++)
            {
                //if (zc == 0)
                //{
                DataRow dr = dtq.NewRow();
                //dr["voucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["istype"].ToString() + imageDataSet.Tables["DataTable5"].Rows[zc]["voucherno"].ToString();
                dr["strvoucherno"] = imageDataSet.Tables["DataTable5"].Rows[zc]["strvoucherno"].ToString();
                dr["voucherdate"] = Convert.ToDateTime(imageDataSet.Tables["DataTable5"].Rows[zc]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["creditcode"] = imageDataSet.Tables["DataTable5"].Rows[zc]["creditcode"].ToString();
                dr["description"] = imageDataSet.Tables["DataTable5"].Rows[zc]["description"].ToString();
                if (imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString() != null)
                {
                    damt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                    //if (damt == 0)
                    //{
                    //    dr["debitamt"] = "";
                    //}
                    //else
                    //{
                    dr["debitamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString());
                    //}
                }
                if (imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != string.Empty && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != "" && imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString() != null)
                {
                    camt = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    //if (camt == 0)
                    //{
                    //    dr["creditamt"] = "";
                    //}
                    //else
                    //{
                    dr["creditamt"] = Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                    //}
                }
                dr["closingbal"] = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                closingbalance = (closingbalance + Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["debitamt"].ToString())) - Convert.ToDouble(imageDataSet.Tables["DataTable5"].Rows[zc]["creditamt"].ToString());
                if (closingbalance >= 0)
                {
                    dr["crdr"] = "Dr";
                }
                else
                {
                    //closingbalance = closingbalance * (-1);
                    dr["closingbal"] = closingbalance * (-1);
                    dr["crdr"] = "Cr";
                }
                dtq.Rows.Add(dr);
                //}
                //else
                //{
                //    DataRow dr = dt.NewRow();
                //    dr["voucherno"] = Convert.ToInt64(dtc.Rows[zc]["voucherno"].ToString());
                //    dr["voucherdate"] = Convert.ToDateTime(dtc.Rows[zc]["voucherdate"].ToString());
                //    dr["description"] = dtc.Rows[zc]["description"].ToString();

                //    dr["debitamt"] = Convert.ToDouble(dtc.Rows[zc]["debitamt"].ToString());
                //    dr["creditamt"] = Convert.ToDouble(dtc.Rows[zc]["creditamt"].ToString());
                //    dr["closingbal"] = Convert.ToDouble(dtc.Rows[zc]["closingbal"].ToString());
                //    dr["crdr"] = dtc.Rows[zc]["crdr"].ToString();
                //    dt.Rows.Add(dr);
                //}
            }
            Session["dtpitems"] = dtq;
        }

        SqlDataAdapter dax = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.voucherno=BankACMaster1.voucherno where bankacmaster.istype='BR' and bankacmaster.name='" + li.bank1 + "' and (cldate is null or cldate='') and bankacmaster.voucherdate<@fromdate group by bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105),bankacmaster.istype", con);
        dax.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
        DataTable dtbrx = new DataTable();
        dax.Fill(imageDataSet.Tables["DataTable38"]);

        SqlDataAdapter dax1 = new SqlDataAdapter("select bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105) as cldate,bankacmaster.istype from bankacmaster inner join BankACMaster1 on BankACMaster.voucherno=BankACMaster1.voucherno where bankacmaster.istype='BP' and bankacmaster.name='" + li.bank1 + "' and (cldate is null or cldate='') and bankacmaster.voucherdate<@fromdate group by bankacmaster.voucherno,bankacmaster.voucherdate,bankacmaster.acname,bankacmaster.amount,convert(varchar(20),bankacmaster.cldate,105),bankacmaster.istype", con);
        dax1.SelectCommand.Parameters.AddWithValue("@fromdate", li.todated);
        DataTable dtbpx = new DataTable();
        dax1.Fill(imageDataSet.Tables["DataTable381"]);

        DataTable dtallx = new DataTable();
        dtallx.Merge(imageDataSet.Tables["DataTable38"]);
        dtallx.Merge(imageDataSet.Tables["DataTable381"]);
        double amt1 = 0;
        for (int c = 0; c < imageDataSet.Tables["DataTable38"].Rows.Count; c++)
        {
            amt1 = amt1 + Convert.ToDouble(imageDataSet.Tables["DataTable38"].Rows[c]["amount"].ToString());
        }
        double amt2 = 0;
        for (int c1 = 0; c1 < imageDataSet.Tables["DataTable381"].Rows.Count; c1++)
        {
            amt2 = amt2 + Convert.ToDouble(imageDataSet.Tables["DataTable381"].Rows[c1]["amount"].ToString());
        }
        double finalamt = 0;
        if (dtq.Rows.Count > 0)
        {
            finalamt = opbal + Math.Round(amt2, 2) - Math.Round(amt1, 2);
            finalamt = Math.Round(finalamt, 2);
        }
        else
        {
            finalamt = Math.Round(amt2, 2) - Math.Round(amt1, 2);
            finalamt = Math.Round(finalamt, 2);
        }
        if (finalamt >= 0)
        {
            lblclearbalance.Text = finalamt.ToString() + " Dr";
        }
        else
        {
            lblclearbalance.Text = (finalamt * (-1)).ToString() + " Cr";
        }

    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        if (txtfromdate.Text != string.Empty && txtacname.Text != string.Empty)
        {
            string Xrepname = "Reconcile Report";
            li.bank1 = txtacname.Text;
            li.fromdate = txtfromdate.Text;
            li.todate = txttodate.Text;
            //Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?bankname=" + li.bank1 + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter From Date OR ACName and Try Again.');", true);
            return;
        }
    }
}