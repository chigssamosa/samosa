﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Survey.Classes;
using System.Timers;
using System.Management;

public partial class Dashboard : System.Web.UI.Page
{
    System.Timers.Timer myTimer = new System.Timers.Timer();
    ForDashboard fdclass = new ForDashboard();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Label1.Text = DateTime.Now.AddDays(365).ToString();
            //double cc = Math.Round(22.5);
            //DirectoryInfo di = new DirectoryInfo("D:\\delete testing");
            //if (di.Exists)
            //    di.Delete(true);
            //StreamReader reader = new StreamReader(Server.MapPath("~/bulkmail.htm"));
            //string readFile = reader.ReadToEnd();
            //string myString = "";
            //myString = readFile;
            //myString = myString.Replace("$$code$$", "");
            //System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add("alluresofttech@gmail.com");
            //message.From = new System.Net.Mail.MailAddress("alluresofttech@gmail.com");
            //message.Subject = "Airmax Database Back up of " + System.DateTime.Now.ToString();
            //string imageatt1 = "~\\alluredb\\AirmaxDb.Bak";
            ////System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(Server.MapPath(imageatt1));
            ////message.Attachments.Add(attachment1);

            ////System.IO.MemoryStream stream = new System.IO.MemoryStream(new byte[64000]);
            ////System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(Session["fname"].ToString()));
            ////message.Attachments.Add(attachment);
            //message.Body = "";



            ////message.AlternateViews.Add(htmlMail1);
            //message.IsBodyHtml = true;
            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
            //smtp.Port = 587;
            //smtp.Credentials = new NetworkCredential("alluresofttech@gmail.com", "parth@chirag@123");
            //smtp.EnableSsl = true;
            ////smtp.UseDefaultCredentials = true;
            //bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            //if (bb == true)
            //{
            //    smtp.Send(message);
            //}
            //Response.Write("Your Page is Loading");
            if (Request.Cookies["ForLogin"] != null)
            {
                string invtype = "TIT";
                string nn = "11";
                if (nn.Length == 1)
                {
                    nn = invtype + "00" + nn;
                }
                else if (nn.Length == 2)
                {
                    nn = invtype + "0" + nn;
                }
                else
                {
                    nn = invtype + nn;
                }

                fillpendingpodatanew();
                //updateitemopqtydata();
                fillstockgrid();
                if (Request.Cookies["ForLogin"]["username"] == "CNP" || Request.Cookies["ForLogin"]["username"] == "IRS" || Request.Cookies["ForLogin"]["username"] == "Director" || Request.Cookies["ForLogin"]["username"] == "ST")
                {
                    fillactivity();
                }
                else
                {
                    gvactivity.Visible = false;
                    lblactivity.Visible = false;
                }
            }
        }
    }

    //void myTimer_Elapsed(object sender, ElapsedEventArgs e)
    //{
        
    //        //generatevouchernos();
    //        string invtype = "TIT";
    //        string nn = "11";
    //        if (nn.Length == 1)
    //        {
    //            nn = invtype + "00" + nn;
    //        }
    //        else if (nn.Length == 2)
    //        {
    //            nn = invtype + "0" + nn;
    //        }
    //        else
    //        {
    //            nn = invtype + nn;
    //        }

    //        fillpendingpodatanew();
    //        //updateitemopqtydata();
    //        fillstockgrid();
    //        if (Request.Cookies["ForLogin"]["username"] == "CNP" || Request.Cookies["ForLogin"]["username"] == "IRS" || Request.Cookies["ForLogin"]["username"] == "Director" || Request.Cookies["ForLogin"]["username"] == "ST")
    //        {
    //            fillactivity();
    //        }
    //        else
    //        {
    //            gvactivity.Visible = false;
    //            lblactivity.Visible = false;
    //        }
    //        //fillpendingpodata();        

    //}

    public void updateitemopqtydata()
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select distinct(itemname) from ItemMaster", con);
        DataTable dtbank = new DataTable();
        da.Fill(dtbank);
        for (int c = 0; c < dtbank.Rows.Count; c++)
        {
            li.itemname = dtbank.Rows[c]["itemname"].ToString();
            SqlDataAdapter dac = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from ItemMaster1 where itemname='" + li.itemname + "'", con);
            DataTable dtbank1 = new DataTable();
            dac.Fill(dtbank1);
            li.opqty = Convert.ToDouble(dtbank1.Rows[0]["totqty"].ToString());
            SqlCommand cmd = new SqlCommand("update ItemMaster set opqty=" + li.opqty + " where itemname='" + li.itemname + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        fillpendingpodatanew();
        fillstockgrid();
        if (Request.Cookies["ForLogin"]["username"] == "CNP" || Request.Cookies["ForLogin"]["username"] == "IRS" || Request.Cookies["ForLogin"]["username"] == "Director" || Request.Cookies["ForLogin"]["username"] == "ST")
        {
            fillactivity();
        }
        else
        {
            gvactivity.Visible = false;
            lblactivity.Visible = false;
        }
    }

    public void fillstockgrid()
    {
        li.fromdate = "01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0];
        li.todate = "31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtallstock = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select distinct(itemname) from itemmaster order by itemname", con);
        DataTable dtitem = new DataTable();
        da.Fill(dtitem);
        for (int c = 0; c < dtitem.Rows.Count; c++)
        {
            li.itemname = dtitem.Rows[c]["itemname"].ToString();
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname and challandate between @fromdate and @todate))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname and challandate between @fromdate and @todate))as totp,round(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate)),2) as stockqty,round((((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname and challandate between @fromdate and @todate)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname and challandate between @fromdate and @todate)),2) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname and challandate between @fromdate and @todate)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname and challandate between @fromdate and @todate))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            dac.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dac.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                if (Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) < 0)
                {
                    dtallstock.Merge(dtstock);
                }
            }
        }
        if (dtallstock.Rows.Count > 0)
        {
            lblemptystock.Visible = false;
            gvstocklist.Visible = true;
            gvstocklist.DataSource = dtallstock;
            gvstocklist.DataBind();
            lbltotneg.Text = dtallstock.Rows.Count.ToString();
        }
        else
        {
            gvstocklist.Visible = false;
            lblemptystock.Visible = true;
            lblemptystock.Text = "No Stock Data Found.";
            lbltotneg.Text = "0";
        }

    }

    public void generatevouchernos()
    {
        int cc = System.DateTime.Now.Day;
        if (cc == 1)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.mon = System.DateTime.Now.Month;
            string smon = "";
            if (li.mon == 1)
            {
                smon = "01";
            }
            else if (li.mon == 2)
            {
                smon = "02";
            }
            else if (li.mon == 3)
            {
                smon = "03";
            }
            else if (li.mon == 4)
            {
                smon = "04";
            }
            else if (li.mon == 5)
            {
                smon = "05";
            }
            else if (li.mon == 6)
            {
                smon = "06";
            }
            else if (li.mon == 7)
            {
                smon = "07";
            }
            else if (li.mon == 8)
            {
                smon = "08";
            }
            else if (li.mon == 9)
            {
                smon = "09";
            }
            else
            {
                smon = li.mon.ToString();
            }
            li.yr = System.DateTime.Now.Year;
            DataTable dtdata = new DataTable();
            dtdata = fdclass.selectgeneratedvno(li);
            if (dtdata.Rows.Count == 0)
            {
                fdclass.deletegeneratedvno(li);
                for (int c = 1; c < 1001; c++)
                {
                    string cz = "";
                    if (c < 10)
                    {
                        cz = "000";
                    }
                    else if (c < 100)
                    {
                        cz = "00";
                    }
                    else if (c < 1000)
                    {
                        cz = "0";
                    }
                    else
                    {
                        cz = "";
                    }
                    li.vno = smon + cz + c.ToString();
                    fdclass.insertgeneratedvnodata(li);
                }
            }
        }
    }

    public void fillpendingpodata()
    {
        Session["dtpitemsdash"] = CreateTemplatepo();
        DataTable dtq = new DataTable();
        string id = "";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.todated = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrder inner join PurchaseOrderItems on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrder.status!='Closed' and PurchaseOrder.cno='" + li.cno + "' and (PurchaseOrderItems.iscame!='Yes' or PurchaseOrderItems.iscame is NULL) and PurchaseOrder.podate between @fromdate and @todate", con);
        SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate<@todate and PurchaseOrderItems.cno=@cno", con);
        da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        DataTable dtc = new DataTable();
        da11.Fill(dtc);
        for (int c = 0; c < dtc.Rows.Count; c++)
        {
            li.id = Convert.ToInt64(dtc.Rows[c]["id"].ToString());
            li.qty = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
            SqlDataAdapter da11c = new SqlDataAdapter("select * from PCItems where vid=@id and cno=@cno", con);
            da11c.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da11c.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            DataTable dtcc = new DataTable();
            da11c.Fill(dtcc);
            if (dtcc.Rows.Count > 0)
            {
                li.qtypc = Convert.ToDouble(dtcc.Rows[0]["qty"].ToString());
                li.qtyremain = li.qty - li.qtypc;
                if (li.qtyremain > 0)
                {
                    id = id + li.id + ",";
                    dtq = (DataTable)Session["dtpitemsdash"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                    dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                    dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                    dr1["name"] = dtc.Rows[c]["name"].ToString();
                    dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                    dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                    dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                    dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                    dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                    dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                    dr1["qty"] = li.qtyremain;
                    dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                    dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                    dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());

                    dtq.Rows.Add(dr1);
                }
            }
            else
            {
                id = id + li.id + ",";
                dtq = (DataTable)Session["dtpitemsdash"];
                DataRow dr1 = dtq.NewRow();
                dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                dr1["name"] = dtc.Rows[c]["name"].ToString();
                dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                dr1["qty"] = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());

                dtq.Rows.Add(dr1);
            }
        }
        if (dtq.Rows.Count > 0)
        {
            lblemptypendingpo.Visible = true;
            lblemptypendingpo.Text = "Total Pending PO :- " + dtq.Rows.Count.ToString();
            gvpendingpo.Visible = true;
            gvpendingpo.DataSource = dtq;
            gvpendingpo.DataBind();
        }
        else
        {
            gvpendingpo.Visible = false;
            lblemptypendingpo.Visible = true;
            lblemptypendingpo.Text = "No Pending Purchase Order Found.";
        }
    }

    public void fillpendingpodatanew()
    {
        Session["dtpitemsdash"] = CreateTemplatepo();
        li.acname = "";
        li.ccode = 0;
        DataTable dtq = new DataTable();
        string id = "";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //li.fromdate = Request["fromdate"].ToString();
        //li.todate = Request["todate"].ToString();
        //li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da11 = new SqlDataAdapter();
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrder inner join PurchaseOrderItems on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrder.status!='Closed' and PurchaseOrder.cno='" + li.cno + "' and (PurchaseOrderItems.iscame!='Yes' or PurchaseOrderItems.iscame is NULL) and PurchaseOrder.podate between @fromdate and @todate", con);

        da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where convert(varchar(50),PurchaseOrder.followupdate,105)=convert(varchar(50),getdate(),105) and PurchaseOrderItems.cno=@cno order by PurchaseOrder.pono", con);
        da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        DataTable dtc = new DataTable();
        da11.Fill(dtc);
        if (dtc.Rows.Count > 0)
        {
            for (int c = 0; c < dtc.Rows.Count; c++)
            {
                li.id = Convert.ToInt64(dtc.Rows[c]["id"].ToString());
                li.qty = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                SqlDataAdapter da11c = new SqlDataAdapter("select isnull(sum(qty) ,0) as totqty from PCItems where vid=@id and cno=@cno", con);
                da11c.SelectCommand.Parameters.AddWithValue("@id", li.id);
                da11c.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                DataTable dtcc = new DataTable();
                da11c.Fill(dtcc);
                if (dtcc.Rows.Count > 0)
                {
                    li.qtypc = Convert.ToDouble(dtcc.Rows[0]["totqty"].ToString());
                    li.qtyremain = li.qty - li.qtypc;
                    if (li.qtyremain > 0)
                    {
                        id = id + li.id + ",";
                        dtq = (DataTable)Session["dtpitemsdash"];
                        DataRow dr1 = dtq.NewRow();
                        dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                        dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                        dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                        dr1["name"] = dtc.Rows[c]["name"].ToString();
                        dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                        dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                        dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                        dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                        dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                        dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                        dr1["qty"] = li.qtyremain;
                        dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                        dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                        dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());

                        dtq.Rows.Add(dr1);
                    }
                }
                else
                {
                    id = id + li.id + ",";
                    dtq = (DataTable)Session["dtpitemsdash"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                    dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                    dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                    dr1["name"] = dtc.Rows[c]["name"].ToString();
                    dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                    dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                    dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                    dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                    dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                    dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                    dr1["qty"] = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                    dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                    dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                    dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());

                    dtq.Rows.Add(dr1);
                }
            }
        }
        if (dtq.Rows.Count > 0)
        {
            lblemptypendingpo.Visible = false;
            gvpendingpo.Visible = true;
            gvpendingpo.DataSource = null;
            gvpendingpo.DataSource = dtq;
            gvpendingpo.DataBind();
        }
        else
        {
            lblemptypendingpo.Visible = true;
            gvpendingpo.Visible = false;
            lblemptypendingpo.Text = "No Pending PO Found.";
        }
    }

    public DataTable CreateTemplatepo()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("pono", typeof(string));
        dtpitems.Columns.Add("podate", typeof(DateTime));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("name", typeof(string));
        dtpitems.Columns.Add("followupdate", typeof(DateTime));
        dtpitems.Columns.Add("followupdetails", typeof(string));
        dtpitems.Columns.Add("followupdetails1", typeof(string));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        return dtpitems;
    }

    protected void txtfollowupdate_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtfollowupdate = (TextBox)currentRow1.FindControl("txtfollowupdate");
        TextBox txtfollowupdetails = (TextBox)currentRow1.FindControl("txtfollowupdetails");
        TextBox txtfollowupdetails1 = (TextBox)currentRow1.FindControl("txtfollowupdetails1");
        Label lblpono = (Label)currentRow1.FindControl("lblpono");
        if (txtfollowupdate.Text != string.Empty)
        {
            li.followupdate = Convert.ToDateTime(txtfollowupdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.followupdetails = txtfollowupdetails.Text;
            li.followupdetails1 = txtfollowupdetails1.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            li.strpono = lblpono.Text;
            SqlCommand cmd = new SqlCommand("update purchaseorder set followupdate=@followupdate where pono=@pono", con);//,followupdetails=@followupdetails,followupdetails1=@followupdetails1
            cmd.Parameters.AddWithValue("@followupdate", li.followupdate);
            //cmd.Parameters.AddWithValue("@followupdetails", li.followupdetails);
            //cmd.Parameters.AddWithValue("@followupdetails1", li.followupdetails1);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "Follow Up Details Updated for PO NO - " + li.strpono;
            faclass.insertactivity(li);
        }
        fillpendingpodatanew();
    }
    protected void txtfollowupdetails_TextChanged(object sender, EventArgs e)
    {
        //GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        //TextBox txtfollowupdate = (TextBox)currentRow1.FindControl("txtfollowupdate");
        //TextBox txtfollowupdetails = (TextBox)currentRow1.FindControl("txtfollowupdetails");
        //TextBox txtfollowupdetails1 = (TextBox)currentRow1.FindControl("txtfollowupdetails1");
        //Label lblpono = (Label)currentRow1.FindControl("lblpono");
        //if (txtfollowupdate.Text != string.Empty)
        //{
        //    li.followupdate = Convert.ToDateTime(txtfollowupdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //    li.followupdetails = txtfollowupdetails.Text;
        //    li.followupdetails1 = txtfollowupdetails1.Text;
        //    string cz = Request.Cookies["Forcon"]["conc"];
        //    cz = cz.Replace(":", ";");
        //    SqlConnection con = new SqlConnection(cz);
        //    li.strpono = lblpono.Text;
        //    SqlCommand cmd = new SqlCommand("update purchaseorder set followupdate=@followupdate,followupdetails=@followupdetails,followupdetails1=@followupdetails1 where pono=@pono", con);
        //    cmd.Parameters.AddWithValue("@followupdate", li.followupdate);
        //    cmd.Parameters.AddWithValue("@followupdetails", li.followupdetails);
        //    cmd.Parameters.AddWithValue("@followupdetails1", li.followupdetails1);
        //    cmd.Parameters.AddWithValue("@pono", li.strpono);
        //    con.Open();
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //    li.uname = Request.Cookies["ForLogin"]["username"];
        //    li.udate = System.DateTime.Now;
        //    li.activity = "Follow Up Details Updated for PO NO - " + li.strpono;
        //    faclass.insertactivity(li);
        //}
        //fillpendingpodatanew();
    }
    protected void txtfollowupdetails1_TextChanged(object sender, EventArgs e)
    {
        //GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        //TextBox txtfollowupdate = (TextBox)currentRow1.FindControl("txtfollowupdate");
        //TextBox txtfollowupdetails = (TextBox)currentRow1.FindControl("txtfollowupdetails");
        //TextBox txtfollowupdetails1 = (TextBox)currentRow1.FindControl("txtfollowupdetails1");
        //Label lblpono = (Label)currentRow1.FindControl("lblpono");
        //if (txtfollowupdate.Text != string.Empty)
        //{
        //    li.followupdate = Convert.ToDateTime(txtfollowupdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //    li.followupdetails = txtfollowupdetails.Text;
        //    li.followupdetails1 = txtfollowupdetails1.Text;
        //    string cz = Request.Cookies["Forcon"]["conc"];
        //    cz = cz.Replace(":", ";");
        //    SqlConnection con = new SqlConnection(cz);
        //    li.strpono = lblpono.Text;
        //    SqlCommand cmd = new SqlCommand("update purchaseorder set followupdate=@followupdate,followupdetails=@followupdetails,followupdetails1=@followupdetails1 where pono=@pono", con);
        //    cmd.Parameters.AddWithValue("@followupdate", li.followupdate);
        //    cmd.Parameters.AddWithValue("@followupdetails", li.followupdetails);
        //    cmd.Parameters.AddWithValue("@followupdetails1", li.followupdetails1);
        //    cmd.Parameters.AddWithValue("@pono", li.strpono);
        //    con.Open();
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //    li.uname = Request.Cookies["ForLogin"]["username"];
        //    li.udate = System.DateTime.Now;
        //    li.activity = "Follow Up Details Updated for PO NO - " + li.strpono;
        //    faclass.insertactivity(li);
        //}
        //fillpendingpodatanew();
    }

    public void fillactivity()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fdclass.selectallactivity(li);
        if (dtdata.Rows.Count > 0)
        {
            lblactivity.Visible = false;
            gvactivity.Visible = true;
            gvactivity.DataSource = dtdata;
            gvactivity.DataBind();
        }
        else
        {
            gvactivity.Visible = false;
            lblactivity.Visible = true;
            lblactivity.Text = "No Activity Found.";
        }
    }

}