﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Misc.aspx.cs" Inherits="Misc" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">MISC Entry</span><br />
        <div class="row">
            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>
            <div class="col-md-2">
                <label class="control-label">
                    Type<span style="color: Red; font-size: 20px;">*</span></label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drptype" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drptype_SelectedIndexChanged">
                    <asp:ListItem>--SELECT--</asp:ListItem>
                    <asp:ListItem>Account Type</asp:ListItem>
                    <asp:ListItem>Bank Name</asp:ListItem>
                    <asp:ListItem>Cash Name</asp:ListItem>
                    <asp:ListItem>Challan Type</asp:ListItem>
                    <asp:ListItem>Client Status</asp:ListItem>
                    <asp:ListItem>Eway Bill Unit</asp:ListItem>
                    <asp:ListItem>Inv Type</asp:ListItem>
                     <asp:ListItem>Pending Order Lying</asp:ListItem>
                    <asp:ListItem>Petty Cash Name</asp:ListItem>
                    <asp:ListItem>Purchase AC</asp:ListItem>
                    <asp:ListItem>role</asp:ListItem>
                    <asp:ListItem>Sales AC</asp:ListItem>
                    <asp:ListItem>Status</asp:ListItem>
                    <asp:ListItem>Tax Type</asp:ListItem>
                    <asp:ListItem>Transport Name</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Name<span style="color: Red; font-size: 20px;">*</span></label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <%--</ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="200px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvmisclist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" PageSize="10" OnRowCommand="gvmisclist_RowCommand"
                        OnRowDeleting="gvmisclist_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Code" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltype" ForeColor="Black" runat="server" Text='<%# bind("type") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" runat="server" ForeColor="#505050" Text='<%# bind("name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select type."
                    Text="*" ValidationGroup="val" InitialValue="--SELECT--" ControlToValidate="drptype"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtname"></asp:RequiredFieldValidator>
            </div>
        </div>
        <%--</ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="drptype" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnsaves" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>--%>
    </div>
    <div class="row" style="height: 10px">
    </div>
    <div class="row" style="padding-left: 25px !important">
        <div class="col-md-2">
            <label class="control-label">
                Item Name<span style="color: Red; font-size: 20px;">*</span></label>
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="row" style="padding-left: 25px !important">
        <div class="col-md-2">
            <label class="control-label">
                Size<span style="color: Red; font-size: 20px;">*</span></label></div>
        <div class="col-md-2">
            <asp:TextBox ID="txtsize" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <label class="control-label">
            </label>
        </div>
        <div class="col-md-2">
            <asp:Button ID="btnsaveitem" runat="server" Text="Save" class="btn btn-default forbutton"
                ValidationGroup="val11" OnClick="btnsaveitem_Click" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="val11" />
        </div>
    </div>
    <div class="row table-responsive">
        <div class="col-md-12">
            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="200px" Width="98%">
                <asp:Label ID="lblemptyitem" runat="server" ForeColor="Red"></asp:Label>
                <asp:GridView ID="gvitem" runat="server" AutoGenerateColumns="False" Width="100%"
                    BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                    CssClass="table table-bordered" PageSize="10" OnRowCommand="gvitem_RowCommand"
                    OnRowDeleting="gvitem_RowDeleting">
                    <Columns>
                        <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Student Code" SortExpression="Csnm" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lbltype" ForeColor="Black" runat="server" Text='<%# bind("type") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" SortExpression="companyname">
                            <ItemTemplate>
                                <asp:Label ID="lblname" runat="server" ForeColor="#505050" Text='<%# bind("name") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                    ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                    CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#4c4c4c" />
                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select item name."
                    Text="*" ValidationGroup="val11" InitialValue="--SELECT--" ControlToValidate="drpitemname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter size."
                    Text="*" ValidationGroup="val11" ControlToValidate="txtsize"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
</asp:Content>
