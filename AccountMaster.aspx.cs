﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class AccountMaster : System.Web.UI.Page
{
    ForUserRight furclass = new ForUserRight();
    ForAccountMaster famclass = new ForAccountMaster();
    ForLedger flclass = new ForLedger();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                fillactypedrop();
                filleditdata();
            }
            else
            {
                fillactypedrop();
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
            //selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillactypedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = famclass.selectallaccounttypefrommiscdata();
        if (dtdata.Rows.Count > 0)
        {
            drpactype.Items.Clear();
            drpactype.DataSource = dtdata;
            drpactype.DataTextField = "name";
            drpactype.DataBind();
            //drpactype.Items.Insert(0,"--SELECT--");
        }
        else
        {
            drpactype.Items.Clear();
            //drpactype.Items.Insert(0, "--SELECT--");
        }
    }

    public void filleditdata()
    {
        li.id = Convert.ToInt64(Request["id"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = famclass.selectaccountdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txtaccountname.Text = dtdata.Rows[0]["acname"].ToString();
            ViewState["acname"] = dtdata.Rows[0]["acname"].ToString();
            txtcontactperson.Text = dtdata.Rows[0]["contactperson"].ToString();
            txtaddress1.Text = dtdata.Rows[0]["add1"].ToString();
            txtaddress2.Text = dtdata.Rows[0]["add2"].ToString();
            txtaddress3.Text = dtdata.Rows[0]["add3"].ToString();
            txtcity.Text = dtdata.Rows[0]["city"].ToString();
            txtphoneo.Text = dtdata.Rows[0]["phone1"].ToString();
            txtphoneo1.Text = dtdata.Rows[0]["phone2"].ToString();
            txtphoner.Text = dtdata.Rows[0]["phoner1"].ToString();
            txtphoner1.Text = dtdata.Rows[0]["phoner2"].ToString();
            txtmobile.Text = dtdata.Rows[0]["mobileno"].ToString();
            txtfax.Text = dtdata.Rows[0]["fax"].ToString();
            txtemail.Text = dtdata.Rows[0]["emailid"].ToString();
            drpactype.SelectedValue = dtdata.Rows[0]["actype"].ToString();
            drpstate.SelectedValue = dtdata.Rows[0]["state"].ToString();
            txtgsttinno.Text = dtdata.Rows[0]["gsttinno"].ToString();
            txtgstdate.Text = dtdata.Rows[0]["date1"].ToString();
            txtcsttinno.Text = dtdata.Rows[0]["csttinno"].ToString();
            txtcstdate.Text = dtdata.Rows[0]["date2"].ToString();
            txtduedays.Text = dtdata.Rows[0]["duedays"].ToString();
            txtdiscount.Text = dtdata.Rows[0]["discp"].ToString();
            txtsrtaxno.Text = dtdata.Rows[0]["servicetaxno"].ToString();
            txttanno.Text = dtdata.Rows[0]["tan"].ToString();
            txtpanno.Text = dtdata.Rows[0]["pan"].ToString();
            txtdebitegroupcode.Text = dtdata.Rows[0]["debitgroupcode"].ToString();
            txtcreditgroupcode.Text = dtdata.Rows[0]["creditgroupcode"].ToString();
            txtgstno.Text = dtdata.Rows[0]["gstno"].ToString();
            txtgstnodate.Text = dtdata.Rows[0]["gstnodate"].ToString();
            txtccode.Text = dtdata.Rows[0]["cno"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            ViewState["id"] = dtdata.Rows[0]["id"].ToString();
            li.acname = dtdata.Rows[0]["acname"].ToString();
            if (dtdata.Rows[0]["inexdebit"].ToString() == "Yes")
            {
                chkexpensedebit.Checked = true;
            }
            if (dtdata.Rows[0]["excludetax"].ToString() == "Yes")
            {
                chkexcludetax.Checked = true;
            }
            if (dtdata.Rows[0]["inexport"].ToString() == "Yes")
            {
                chkinexport.Checked = true;
            }
            DataTable dtop = new DataTable();
            dtop = famclass.selectaccountdataopfromacnaame(li);
            if (dtop.Rows.Count > 0)
            {
                double op = 0;
                double deb = 0;
                double cred = 0;
                double clo = 0;
                txtopening.Text = dtop.Rows[0]["amount"].ToString();
                txtopbalance.Text = dtop.Rows[0]["amount"].ToString();
                op = Convert.ToDouble(dtop.Rows[0]["amount"].ToString());
                if (dtop.Rows[0]["voucherdate"].ToString().Trim() != "")
                {
                    txtopdate.Text = Convert.ToDateTime(dtop.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                DataTable dtd = new DataTable();
                dtd = famclass.selectaccountdatadebitfromacnaame(li);
                if (dtd.Rows.Count > 0)
                {
                    deb = Convert.ToDouble(dtd.Rows[0]["totd"].ToString());
                }
                DataTable dtc = new DataTable();
                dtc = famclass.selectaccountdatacreditfromacnaame(li);
                if (dtc.Rows.Count > 0)
                {
                    cred = Convert.ToDouble(dtc.Rows[0]["totc"].ToString());
                }
                txtdebit.Text = deb.ToString();
                txtcredit.Text = cred.ToString();
                txtclosing.Text = Math.Round((op + deb - cred), 2).ToString();
            }
            btnsave.Text = "Update";
        }
    }

    protected void btnaccount_Click(object sender, EventArgs e)
    {
        //GST No Validation
        Int64 aa = txtgstno.Text.Length;
        if (aa > 0)
        {
            if (aa != 15)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('GST Number must contain 15 character.');", true);
                return;
            }
            string two = txtgstno.Text.Substring(0, 2);
            var isValidNumber = Regex.IsMatch(two, @"^[0-9]+(\.[0-9]+)?$");
            if (isValidNumber == false)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First Two Letter of GST Number must be Numeric.');", true);
                return;
            }
            string s = txtgstno.Text.Substring(0, 2);
            int result;
            if (int.TryParse(s, out result))
            {
                int two5 = Convert.ToInt16(txtgstno.Text.Substring(0, 2));
                if (two5 > 37 || two5 < 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First Two Letter of GST Number must be State Code.');", true);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First Two Letter of GST Number must be State Code.');", true);
                return;
            }

            string two1 = txtgstno.Text.Substring(2, 10);
            //var isValidNumber1 = Regex.IsMatch(two1, @"^[0-9]+(\.[0-9]+)?$");
            if (two1 != txtpanno.Text)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Third to Twelve Letter of GST Number must be PAN No.Check PAN No Here.');", true);
                return;
            }            
            string two2 = txtgstno.Text.Substring(12, 1);
            var isValidNumber1 = Regex.IsMatch(two2, @"^[0-9]+(\.[0-9]+)?$");
            if (isValidNumber1 == false)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Thiteenth Letter of GST Number must be Numeric.');", true);
                return;
            }
            string two4 = txtgstno.Text.Substring(13, 1);
            var isValidNumber3 = Regex.IsMatch(two4, @"^[A-Za-z]$");
            if (isValidNumber3 == false)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fourteenth Letter of GST Number must be Alphabetic.');", true);
                return;
            }
            //string two3 = txtgstno.Text.Substring(14, 1);
            //var isValidNumber2 = Regex.IsMatch(two3, @"^[0-9]+(\.[0-9]+)?$");
            //if (isValidNumber2 == false)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fifteenth Letter of GST Number must be Numeric.');", true);
            //    return;
            //}
        }
        //

        if (txtpanno.Text.Length > 0)
        {
            if (txtpanno.Text.Length != 10)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('PAN Number must contain 10 character.');", true);
                return;
            }
        }
        if (txtccode.Text.Trim() == string.Empty)
        {
            //if (txtopbalance.Text.Trim() != "0")
            //{
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter CCode.');", true);
            return;
            //}
        }
        if (txtopbalance.Text.Trim() == string.Empty)
        {
            //if (txtopbalance.Text.Trim() != "0")
            //{
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter OP Balance.');", true);
            return;
            //}
        }
        if (txtopdate.Text.Trim() == string.Empty)
        {
            //if (txtopbalance.Text.Trim() != "0")
            //{
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter OP Date.');", true);
            return;
            //}
        }
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from ACGroupMaster1 inner join ACGroupMaster on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.groupcode='" + txtdebitegroupcode.Text + "' and ACGroupMaster1.type='Debit'", con);
        DataTable dtd = new DataTable();
        da.Fill(dtd);
        SqlDataAdapter da1 = new SqlDataAdapter("select * from ACGroupMaster1 inner join ACGroupMaster on ACGroupMaster.mastercode=ACGroupMaster1.groupcode where ACGroupMaster.groupcode='" + txtcreditgroupcode.Text + "' and ACGroupMaster1.type='Credit'", con);
        DataTable dtc = new DataTable();
        da1.Fill(dtc);
        if (dtd.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Something wrong with Debit group code.Change code and try again');", true);
            return;
        }
        if (dtc.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Something wrong with Credit group code.Change code and try again');", true);
            return;
        }
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.acname = txtaccountname.Text.Trim();
        li.contactperson = txtcontactperson.Text;
        li.add1 = txtaddress1.Text;
        li.add2 = txtaddress2.Text;
        li.add3 = txtaddress3.Text;
        li.city = txtcity.Text;
        li.state = drpstate.SelectedItem.Text;
        li.pincode = "";
        li.phone1 = txtphoneo.Text;
        li.phone2 = txtphoneo1.Text;
        li.phoner1 = txtphoner.Text;
        li.phoner2 = txtphoner1.Text;
        li.mobileno = txtmobile.Text;
        li.fax = txtfax.Text;
        li.emailid = txtemail.Text;
        li.actype = drpactype.SelectedItem.Text;
        li.gsttinno = txtgsttinno.Text;
        li.date1 = txtgstdate.Text;
        li.csttinno = txtcsttinno.Text;
        li.date2 = txtcstdate.Text;
        li.gstno = txtgstno.Text;
        li.gstnodate = txtgstnodate.Text;
        if (chkexpensedebit.Checked == true)
        {
            li.inexdebit = "Yes";
        }
        else
        {
            li.inexdebit = "No";
        }
        if (chkexcludetax.Checked == true)
        {
            li.excludetax = "Yes";
        }
        else
        {
            li.excludetax = "No";
        }
        if (chkinexport.Checked == true)
        {
            li.inexport = "Yes";
        }
        else
        {
            li.inexport = "No";
        }
        if (txtduedays.Text.Trim() != string.Empty && txtduedays.Text.Trim() != "")
        {
            li.duedays = Convert.ToDouble(txtduedays.Text);
        }
        else
        {
            li.duedays = 0;
        }
        if (txtdiscount.Text.Trim() != string.Empty && txtdiscount.Text.Trim() != "")
        {
            li.discp = Convert.ToDouble(txtdiscount.Text);
        }
        else
        {
            li.discp = 0;
        }
        li.servicetaxno = txtsrtaxno.Text;
        li.tan = txttanno.Text;
        li.pan = txtpanno.Text;
        li.debitgroupcode = txtdebitegroupcode.Text;
        li.creditgroupcode = txtcreditgroupcode.Text;
        if (txtopbalance.Text.Trim() != string.Empty)
        {
            li.opbalance = Convert.ToDouble(txtopbalance.Text);
        }
        else
        {
            li.opbalance = 0;
        }
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        if (btnsave.Text == "Save")
        {
            li.acname = txtaccountname.Text.Trim();
            DataTable dtdata = new DataTable();
            dtdata = famclass.checkduplicate(li);
            if (dtdata.Rows.Count == 0)
            {
                famclass.insertaccountdata(li);
                li.activity = li.acname + " New Account Name Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                //if (Convert.ToInt64(ViewState["id"].ToString()) == Convert.ToInt64(dtdata.Rows[0]["id"].ToString()))
                //{
                //    li.id = Convert.ToInt64(ViewState["id"].ToString());
                //    famclass.updateaccountdata(li);
                //}
                //else
                //{
                li.activity = li.acname + " Tried to insert New Account Name but Account Name already exists.";
                faclass.insertactivity(li);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name already exists.Please try again with new Account Name.');", true);
                return;
                //}
            }
        }
        else
        {
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname1 = txtaccountname.Text.Trim();
            li.acname = ViewState["acname"].ToString().Trim();
            li.gstno = txtgstno.Text;
            famclass.updateledger_egstno(li);
            if (txtaccountname.Text.Trim() != ViewState["acname"].ToString().Trim())
            {
                DataTable dtdata = new DataTable();
                dtdata = famclass.checkduplicate1(li);
                if (dtdata.Rows.Count == 0)
                {
                    li.id = Convert.ToInt64(ViewState["id"].ToString());
                    famclass.updateaccountdata(li);
                    li.activity = ViewState["acname"].ToString() + " Account Name Updated to " + txtaccountname.Text + ".";
                    faclass.insertactivity(li);
                    //Update ACName in whole software
                    li.acname1 = txtaccountname.Text.Trim();
                    li.acname = ViewState["acname"].ToString().Trim();
                    famclass.updatetaxmaster1(li);
                    famclass.updatetaxmaster2(li);
                    famclass.updatetaxmaster3(li);
                    famclass.updatetaxmaster4(li);

                    famclass.updateacgroupmastermaster(li);
                    famclass.updateacgroupmaster1master(li);
                    famclass.updatebankacmaster(li);
                    famclass.updatebankacmaster2(li);
                    famclass.updatebankacmaster1(li);
                    famclass.updatebankacshadow(li);
                    famclass.updateledgercreditcode(li);
                    famclass.updateledgerdebitcode(li);
                    famclass.updateledger_eacname(li);
                    famclass.updatepcmaster(li);
                    famclass.updateperinvitems(li);
                    famclass.updateperinvmaster(li);
                    famclass.updatecomminvitems(li);
                    famclass.updatecomminvmaster(li);
                    famclass.updatepimaster(li);
                    famclass.updatepmmaster(li);
                    famclass.updatePurchaseOrder(li);
                    famclass.updateQuoItems(li);
                    famclass.updateQuoMaster(li);
                    famclass.updaterrmaster(li);
                    famclass.updateSalesOrder(li);
                    famclass.updateSCMaster(li);
                    famclass.updateSIMaster(li);
                    famclass.updateStockJVItems(li);
                    famclass.updateStockJVMaster(li);
                    famclass.updateToolDelMaster(li);
                    famclass.updateClientMaster(li);
                    famclass.updateToolsJVItems(li);
                    famclass.updateToolsJVMaster(li);
                }
                else
                {
                    if (Convert.ToInt64(ViewState["id"].ToString()) == Convert.ToInt64(dtdata.Rows[0]["id"].ToString()))
                    {
                        li.activity = txtaccountname.Text + " Account Name Updated.";
                        faclass.insertactivity(li);
                        li.id = Convert.ToInt64(ViewState["id"].ToString());
                        famclass.updateaccountdata(li);
                    }
                    else
                    {
                        li.activity = "Tried to update Account Name " + txtaccountname.Text + " but Account Name already exists.";
                        faclass.insertactivity(li);
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Account Name already exists.Please try again with new Account Name.');", true);
                        return;
                    }
                }
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["id"].ToString());
                famclass.updateaccountdata(li);
                li.activity = txtaccountname.Text + " Account Name Updated.";
                faclass.insertactivity(li);
            }
            famclass.updategstnoinledger_e(li);
        }
        if (li.opbalance != 0)
        {
            li.voucherno = 0;
            //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.strvoucherno = "0";
            li.refid = 0;
            li.debitcode = txtaccountname.Text;
            li.creditcode = "Opening Balance";
            li.description = "OP";
            if (Convert.ToDouble(txtopbalance.Text) >= 0)
            {
                li.istype1 = "D";
                li.amount = Convert.ToDouble(txtopbalance.Text);
            }
            else
            {
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtopbalance.Text);
            }
            li.ccode = Convert.ToInt64(txtccode.Text);

            li.istype = "OP";
            if (btnsave.Text == "Update")
            {
                flclass.deleteledgerdataforacmaster(li);
            }
            flclass.insertledgerdataforac(li);
        }
        else
        {
            li.voucherno = 0;
            //li.voucherdate = Convert.ToDateTime("01-04-" + (System.DateTime.Now.Year - 1).ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //if (txtopdate.Text.Trim() != string.Empty)
            //{
            li.voucherdate1 = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //}
            //else
            //{
            //    li.voucherdate1 = sqldatenull;
            //}
            li.strvoucherno = "0";
            li.refid = 0;
            li.debitcode = txtaccountname.Text;
            li.creditcode = "Opening Balance";
            li.description = "OP";
            if (Convert.ToDouble(txtopbalance.Text) >= 0)
            {
                li.istype1 = "D";
                li.amount = Convert.ToDouble(txtopbalance.Text);
            }
            else
            {
                li.istype1 = "C";
                li.amount = Convert.ToDouble(txtopbalance.Text);
            }
            li.ccode = Convert.ToInt64(txtccode.Text);

            li.istype = "OP";
            //if (btnsave.Text == "Update")
            //{
            flclass.deleteledgerdataforacmaster(li);
            //}
            flclass.insertledgerdataforac(li);
        }
        Response.Redirect("~/AccountMasterList.aspx?pagename=AccountMasterList");
    }
    protected void txtgstno_TextChanged(object sender, EventArgs e)
    {
        if (txtgstno.Text.Length == 15)
        {
            txtpanno.Text = txtgstno.Text.Substring(2, 10);
            string statecode = txtgstno.Text.Substring(0, 2);
            if (statecode == "01") { drpstate.SelectedValue = "Jammu & Kashmir"; }
            else if (statecode == "02") { drpstate.SelectedValue = "Himachal Pradesh"; }
            else if (statecode == "03") { drpstate.SelectedValue = "Punjab"; }
            else if (statecode == "04") { drpstate.SelectedValue = "Chandigarh"; }
            else if (statecode == "05") { drpstate.SelectedValue = "Uttarakhand"; }
            else if (statecode == "06") { drpstate.SelectedValue = "Haryana"; }
            else if (statecode == "07") { drpstate.SelectedValue = "Delhi"; }
            else if (statecode == "08") { drpstate.SelectedValue = "Rajasthan"; }
            else if (statecode == "09") { drpstate.SelectedValue = "Uttar Pradesh"; }
            else if (statecode == "10") { drpstate.SelectedValue = "Bihar"; }
            else if (statecode == "11") { drpstate.SelectedValue = "Sikkim"; }
            else if (statecode == "12") { drpstate.SelectedValue = "Arunachal Pradesh"; }
            else if (statecode == "13") { drpstate.SelectedValue = "Nagaland"; }
            else if (statecode == "14") { drpstate.SelectedValue = "Manipur"; }
            else if (statecode == "15") { drpstate.SelectedValue = "Mizoram"; }
            else if (statecode == "16") { drpstate.SelectedValue = "Tripura"; }
            else if (statecode == "17") { drpstate.SelectedValue = "Meghalaya"; }
            else if (statecode == "18") { drpstate.SelectedValue = "Assam"; }
            else if (statecode == "19") { drpstate.SelectedValue = "West Bengal"; }
            else if (statecode == "20") { drpstate.SelectedValue = "Jharkhand"; }
            else if (statecode == "21") { drpstate.SelectedValue = "Odisha"; }
            else if (statecode == "22") { drpstate.SelectedValue = "Chhattisgarh"; }
            else if (statecode == "23") { drpstate.SelectedValue = "Madhya Pradesh"; }
            else if (statecode == "24") { drpstate.SelectedValue = "GUJARAT"; }
            else if (statecode == "25") { drpstate.SelectedValue = "Daman & Diu"; }
            else if (statecode == "26") { drpstate.SelectedValue = "Dadra & Nagar Haveli"; }
            else if (statecode == "27") { drpstate.SelectedValue = "Maharashtra"; }
            else if (statecode == "28") { drpstate.SelectedValue = "Gujarat"; ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('28 State Code in Not Available.Try Again.');", true); return; }
            else if (statecode == "29") { drpstate.SelectedValue = "Karnataka"; }
            else if (statecode == "30") { drpstate.SelectedValue = "Goa"; }
            else if (statecode == "31") { drpstate.SelectedValue = "Lakshdweep"; }
            else if (statecode == "32") { drpstate.SelectedValue = "Kerala"; }
            else if (statecode == "33") { drpstate.SelectedValue = "Tamil Nadu"; }
            else if (statecode == "34") { drpstate.SelectedValue = "Puducherry"; }
            else if (statecode == "35") { drpstate.SelectedValue = "Andaman & Nicobar Islands"; }
            else if (statecode == "36") { drpstate.SelectedValue = "Telangana"; }
            else if (statecode == "37") { drpstate.SelectedValue = "Andhra Pradesh"; }
            else if (statecode == "97") { drpstate.SelectedValue = "Other Territory"; }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getcodename(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallcodename(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

}