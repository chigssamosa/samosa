﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepProjectSummary.aspx.cs" Inherits="RepProjectSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Project Summary Report</span><br />        
        <div class="row">
            <div class="col-md-3">
            <asp:RadioButtonList ID="rdoformat" runat="server" RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Selected="True">PDF</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnpreview" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnpreview_Click" ValidationGroup="val" />
            </div>
        </div>
    </div>
</asp:Content>
