﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GenerateEwaybill.aspx.cs" Inherits="GenerateEwaybill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Bank Book Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Bill No.</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtbillno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btngenerate" runat="server" Text="Generate Eway Bill" class="btn btn-default forbutton"
                    ValidationGroup="val" onclick="btngenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
    </div>
</asp:Content>
