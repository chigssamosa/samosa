﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Stock.aspx.cs" Inherits="Stock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Stock</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnstockform201c" runat="server" Text="Form 201C" class="btn btn-default forbutton"
                    OnClick="btnstockform201c_Click" ValidationGroup="val" />
                     <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
                    </div>
            <div class="col-md-2">
                <asp:Button ID="Button1" runat="server" Text="Print Stock" class="btn btn-default forbutton"
                    OnClick="btnstockprint_Click"/></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvbankpaymentlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField HeaderText="item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Op. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblopqty" ForeColor="Black" runat="server" Text='<%# bind("opqty") %>'></asp:Label>
                                    <%--<%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy")%>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inward" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblpqty" ForeColor="Black" runat="server" Text='<%# bind("totp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Outward" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblsqty" ForeColor="Black" runat="server" Text='<%# bind("tots") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Qty." SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("stockqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cl. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("totalqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
