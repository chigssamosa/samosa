﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;

public partial class GenerateEwaybill : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btngenerate_Click(object sender, EventArgs e)
    {
        DataTableToJSONWithStringBuilder();
    }

    public string DataTableToJSONWithStringBuilder()
    {
        DataTable table = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select sino,strsino,sono,sodate from simaster where strsino='" + txtbillno.Text + "'", con);
        da.Fill(table);

        var JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            string cc = "";
            cc = "{" + "\"" + "version" + "\"" + ":" + "\"" + "1.0.0123" + "\"" + "," + "\"" + "billLists" + "\"" + ":";
            JSONString.Append(cc + "[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
            }
            cc = "," + "\"" + "itemList" + "\"" + ":";
            JSONString.Append(cc + "[");
            SqlDataAdapter da1 = new SqlDataAdapter("select itemname,rate,basicamount from siitems where strsino='" + txtbillno.Text + "'", con);
            DataTable dtc = new DataTable();
            da1.Fill(dtc);
            for (int i = 0; i < dtc.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < dtc.Columns.Count; j++)
                {
                    if (j < dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == dtc.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + dtc.Columns[j].ColumnName.ToString() + "\":" + "\"" + dtc.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == dtc.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]}]}");
            string path = @"D:\testc.json";
            using (var file = new StreamWriter(path, false))
            {
                file.Write(JSONString);
                file.Close();
                file.Dispose();
            }
        }
        return JSONString.ToString();
    }

}