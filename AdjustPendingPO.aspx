﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AdjustPendingPO.aspx.cs" Inherits="AdjustPendingPO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Adjust Pending PO</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" AutoComplete="NO"></asp:TextBox></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control" AutoComplete="NO"></asp:TextBox></div>
            <div class="col-md-2">
                <asp:Button ID="btnadjustpo" runat="server" Text="Fill Pending PO" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnadjustpo_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkalldata" runat="server" />All PO Data</div>
        </div>
        <div class="row">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator></div>
    </div>
    <asp:GridView ID="gvaclist" runat="server" AutoGenerateColumns="False" Width="100%"
        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
        CssClass="table table-bordered">
        <Columns>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="PO NO." SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblpono" ForeColor="Black" runat="server" Text='<%# bind("pono") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PO Date" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblpodate" ForeColor="Black" runat="server" Text='<%# bind("podate") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ACName" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Contact Person" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblstrsino" ForeColor="Black" runat="server" Text='<%# bind("strsino") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="CName" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descr1" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lbldescr1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descr2" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lbldescr2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descr3" SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lbldescr3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                <ItemTemplate>
                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Adjust Qty." SortExpression="Csnm">
                <ItemTemplate>
                    <asp:TextBox ID="txtadjqty" runat="server" Text='<%# bind("adjqty") %>' AutoPostBack="true"
                        OnTextChanged="txtadjqty_TextChanged"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <FooterStyle BackColor="#4c4c4c" />
        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
    </asp:GridView>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
