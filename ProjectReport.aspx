﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProjectReport.aspx.cs" Inherits="ProjectReport" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/start/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".txtfollowupdate").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtdeliverydate").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtokorderdate").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txttentativedeliverydate").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtfirstquotation").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txttechnicaldrawing").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtrevisequotation").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtfinaltech").datepicker({
            dateFormat: 'dd-mm-yy',
            });

             $(".txtpendingorderlyingwith").datepicker({
            dateFormat: 'dd-mm-yy',
            });
        });
    </script>
</asp:Content>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Project Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Client Code</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="200px"
                    onkeypress="return checkQuote();" AutoPostBack="True" OnTextChanged="txtclientcode_TextChanged"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender33" runat="server" TargetControlID="txtclientcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <span style="color: white; background-color: Red">Project Report</span>
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="400px" Width="100%">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblempty" runat="server" ForeColor="Red" Font-Size="Larger" Font-Bold="true">
                            </asp:Label>
                            <asp:GridView ID="gvprojectreport" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                CssClass="table table-bordered" OnRowDataBound="gvprojectreport_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" SortExpression="companyname" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" runat="server" ForeColor="#505050" Text='<%# bind("id") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SO No." SortExpression="companyname" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsono" runat="server" ForeColor="#505050" Text='<%# bind("sono") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldescription" runat="server" ForeColor="#505050" Text='<%# bind("dd") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblunit" runat="server" ForeColor="#505050" Text='<%# bind("unit") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QTY" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblqty" runat="server" ForeColor="#505050" Text='<%# bind("qty") %>'
                                                Width="50px"></asp:Label>
                                            <%--<asp:TextBox ID="txtqty" runat="server" Text='<%# bind("qty") %>' Width="50px" ReadOnly="true"></asp:TextBox>--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Quotation" SortExpression="companyname" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkfirstquotation" runat="server" OnCheckedChanged="chkfirstquotation_CheckedChanged"
                                                AutoPostBack="true" />
                                            <asp:TextBox ID="txtfirstquotation" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("firstquotation") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Technical Drawing/Specs" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chktechnicaldrawing" runat="server" OnCheckedChanged="chktechnicaldrawing_CheckedChanged"
                                                AutoPostBack="true" />
                                            <asp:TextBox ID="txttechnicaldrawing" runat="server" CssClass="txttechnicaldrawing"
                                                Width="100px" Text='<%# bind("technicaldrawing") %>'></asp:TextBox>
                                            <%--OnTextChanged="txttechnicaldrawing_TextChanged"--%>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revise Quotation" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkrewisequotation" runat="server" OnCheckedChanged="chkrewisequotation_CheckedChanged"
                                                AutoPostBack="true" />
                                            <asp:TextBox ID="txtrevisequotation" runat="server" CssClass="txtrevisequotation"
                                                Width="100px" Text='<%# bind("rewisequotation") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Final Tech/Clearence Of Quote" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkfinaltech" runat="server" OnCheckedChanged="chkfinaltech_CheckedChanged"
                                                AutoPostBack="true" />
                                            <asp:TextBox ID="txtfinaltech" runat="server" CssClass="txtfinaltech" Width="100px"
                                                Text='<%# bind("finaltech") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Follow up Date With Concern Person" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtfollowupdate" runat="server" Width="100px" CausesValidation="true"
                                                ValidationGroup="test" CssClass="txtfollowupdate" Text='<%# bind("lastfollowupdate") %>'></asp:TextBox>
                                            <%-- <asp:Calendar ID="Cal1" runat="Server"></asp:Calendar>--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                                                ErrorMessage="Followup date must be dd-MM-yyyy" ValidationGroup="test" ForeColor="Red"
                                                ControlToValidate="txtfollowupdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pending Order Lying With" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drppendingorderlying" runat="server" CssClass="form-control"
                                                Width="150px">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblpending" runat="server" ForeColor="#505050" Text='<%# bind("pendingorderlying") %>'
                                                Width="100px" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ok Order Date" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtokorderdate" runat="server" Width="100px" CausesValidation="true"
                                                ValidationGroup="test" CssClass="txtokorderdate" Text='<%# bind("okorderdate") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                                                ErrorMessage="OK Order date must be dd-MM-yyyy" ValidationGroup="test" ForeColor="Red"
                                                ControlToValidate="txtokorderdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tentative Delivery Date by Office Team" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txttentativedeliverydate" runat="server" Width="100px" CausesValidation="true"
                                                ValidationGroup="test" CssClass="txttentativedeliverydate" Text='<%# bind("tentativedeliverydate") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                                                ErrorMessage="Tentative Delivery date must be dd-MM-yyyy" ValidationGroup="test"
                                                ForeColor="Red" ControlToValidate="txttentativedeliverydate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delivery Date" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtdeliverydate" runat="server" Width="100px" CausesValidation="true"
                                                ValidationGroup="test" CssClass="txtdeliverydate" Text='<%# bind("deliverydate") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Text="DD-MM-YYYY"
                                                ErrorMessage="Delivery date must be dd-MM-yyyy" ValidationGroup="test" ForeColor="Red"
                                                ControlToValidate="txtdeliverydate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}">
                                            </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtremarks" runat="server" Text='<%# bind("remarks") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <%--<span style="color: white; background-color: Red">Activity</span>--%>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnupdate" runat="server" Text="Update" class="btn btn-default forbutton"
                    OnClick="btnupdate_Click" />
                <asp:Button ID="btnprint" runat="server" Text="Print" 
                    class="btn btn-default forbutton" onclick="btnprint_Click" /></div>
        </div>
    </div>
</asp:Content>
