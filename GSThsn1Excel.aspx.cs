﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Data.SqlTypes;

public partial class GSThsn : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillgridb2b();
            //fillgridhsn();
            //ExportGridToExcel();
            //ExportGridToExcel1();
            //if (!File.Exists(Server.MapPath("~/GST Return/GSTHSN.xlsx")))
            //{
            //    var app = new Microsoft.Office.Interop.Excel.Application();
            //    var wb = app.Workbooks.Add();
            //    wb.SaveAs(Server.MapPath("~/GST Return/GSTHSN.xlsx"));
            //    wb.Close();
            //}
            //else
            //{
            //    File.Delete(Server.MapPath("~/GST Return/GSTHSN.xlsx"));
            //    var app = new Microsoft.Office.Interop.Excel.Application();
            //    var wb = app.Workbooks.Add();
            //    wb.SaveAs(Server.MapPath("~/GST Return/GSTHSN.xlsx"));
            //    wb.Close();
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
            //    //return;
            //}
        }
    }

    public void fillgridhsn()
    {
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select ItemMaster.hsncode,ItemMaster.itemname,ItemMaster.unit,SUM(SIItems.qty) as totqty,sum(SIItems.amount) as totamount,sum(SIItems.basicamount) as totbasic,sum(SIItems.vatamt) as totvat,SUM(SIItems.addtaxamt) as totadvat,SUM((CASE WHEN (SIMaster.salesac != 'Sales A/c. IGST - Export') THEN SIItems.cstamt ELSE 0 END)) as totcst from SIItems inner join ItemMaster on ItemMaster.itemname=SIItems.itemname inner join SIMaster on simaster.strsino=SIItems.strsino where SIItems.sidate between @fromdate and @todate group by itemmaster.hsncode,ItemMaster.itemname,ItemMaster.unit", con);//SUM(SIItems.cstamt) as totcst
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);
        if (dtdata.Rows.Count > 0)
        {
            gvhsn.DataSource = dtdata;
            gvhsn.DataBind();
        }
    }

    private void ExportGridToExcel1()
    {

        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string dd = "GSTR1_" + li.fromdated.Month.ToString() + "_" + li.fromdated.Year.ToString() + ".xlsx";
        if (!File.Exists(Server.MapPath("~/GST Return/" + dd + "")))
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            var wb = app.Workbooks.Add();
            wb.SaveAs(Server.MapPath("~/GST Return/" + dd + ""));
            wb.Close();
        }
        else
        {
            File.Delete(Server.MapPath("~/GST Return/" + dd + ""));
            var app = new Microsoft.Office.Interop.Excel.Application();
            var wb = app.Workbooks.Add();
            wb.SaveAs(Server.MapPath("~/GST Return/" + dd + ""));
            wb.Close();
            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
            //return;
        }


        //Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
        Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

        if (xlApp == null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not properly installed!!.');", true);
            //MessageBox.Show("Excel is not properly installed!!");
            return;
        }

        xlApp.DisplayAlerts = false;
        //string filePath = @"d:\\a4.xlsx";
        //string filePath = @"d:\\" + dd + "";
        string filePath = Server.MapPath("~/GST Return/" + dd + "");

        //1 Sheet
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        Microsoft.Office.Interop.Excel.Sheets worksheets = xlWorkBook.Worksheets;
        //1 sheet
        var xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)worksheets.Add(worksheets[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet.Name = "hsn";
        xlNewSheet.Cells[1, 1] = "HSN";
        xlNewSheet.Cells[1, 2] = "Description";
        xlNewSheet.Cells[1, 3] = "UQC";
        xlNewSheet.Cells[1, 4] = "Total Quantity";
        xlNewSheet.Cells[1, 5] = "Total Value";
        xlNewSheet.Cells[1, 6] = "Taxable Value";
        xlNewSheet.Cells[1, 7] = "Integrated Tax Amount";
        xlNewSheet.Cells[1, 8] = "Central Tax Amount";
        xlNewSheet.Cells[1, 9] = "State/UT Tax Amount";
        xlNewSheet.Cells[1, 10] = "Cess Amount";





        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select ItemMaster.hsncode,ItemMaster.itemname,ItemMaster.unit,SUM(SIItems.qty) as totqty,sum(SIItems.amount) as totamount,sum(SIItems.basicamount) as totbasic,sum(SIItems.vatamt) as totvat,SUM(SIItems.addtaxamt) as totadvat,SUM(SIItems.cstamt) as totcst from SIItems inner join ItemMaster on ItemMaster.itemname=SIItems.itemname where SIItems.sidate between @fromdate and @todate group by itemmaster.hsncode,ItemMaster.itemname,ItemMaster.unit", con);
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);

        Int64 count = 1;
        if (dtdata.Rows.Count > 0)
        {
            Int64 cc = 0;
            for (int z = 0; z < dtdata.Rows.Count; z++)
            {
                count = count + 1;
                xlNewSheet.Cells[count, 1] = dtdata.Rows[z]["hsncode"].ToString();
                xlNewSheet.Cells[count, 2] = dtdata.Rows[z]["itemname"].ToString();
                xlNewSheet.Cells[count, 3] = dtdata.Rows[z]["unit"].ToString();
                xlNewSheet.Cells[count, 4] = dtdata.Rows[z]["totqty"].ToString();
                xlNewSheet.Cells[count, 5] = dtdata.Rows[z]["totamount"].ToString();
                xlNewSheet.Cells[count, 6] = dtdata.Rows[z]["totbasic"].ToString();
                xlNewSheet.Cells[count, 7] = dtdata.Rows[z]["totcst"].ToString();
                xlNewSheet.Cells[count, 8] = dtdata.Rows[z]["totvat"].ToString();
                xlNewSheet.Cells[count, 9] = dtdata.Rows[z]["totadvat"].ToString();
                xlNewSheet.Cells[count, 10] = "0";
            }
            xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlNewSheet.Select();
        }
        //End of 1 Sheet

        //2 Sheet
        Microsoft.Office.Interop.Excel.Sheets worksheets2 = xlWorkBook.Worksheets;
        var xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets2.Add(worksheets2[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet2.Name = "b2cs";
        xlNewSheet2.Cells[1, 1] = "Invoice Number";
        xlNewSheet2.Cells[1, 2] = "Invoice date";
        xlNewSheet2.Cells[1, 3] = "Invoice Value";
        xlNewSheet2.Cells[1, 4] = "Place Of Supply";
        xlNewSheet2.Cells[1, 5] = "Rate";
        xlNewSheet2.Cells[1, 6] = "Taxable Value";
        xlNewSheet2.Cells[1, 7] = "Cess Amount";
        xlNewSheet2.Cells[1, 8] = "E-Commerce GSTIN";
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        Session["dtpitems"] = CreateTemplate1();
        DataTable dtq2 = new DataTable();
        SqlDataAdapter da2 = new SqlDataAdapter("select SIItems.strsino,SIItems.sidate,SUM(SIItems.basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+SUM((CASE WHEN (SIMaster.salesac != 'Sales A/c. IGST - Export') THEN SIItems.cstamt ELSE 0 END))) as totbillamt,SIItems.taxtype from SIItems inner join simaster on simaster.strsino=SIItems.strsino where SIMaster.sidate between @fromdate and @todate group by SIItems.taxtype,SIItems.strsino,SIItems.sidate,SIItems.sino order by SIItems.sino", con);
        da2.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da2.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata2 = new DataTable();
        da2.Fill(dtdata2);
        if (dtdata.Rows.Count > 0)
        {
            for (int c = 0; c < dtdata.Rows.Count; c++)
            {
                dtq2 = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq2.NewRow();
                dr1["strsino"] = dtdata2.Rows[c]["strsino"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtdata2.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
                dr1["totbasicamount"] = dtdata2.Rows[c]["totbasic"].ToString();

                var cc = dtdata2.Rows[c]["taxtype"].ToString().IndexOf("-");
                if (cc != -1)
                {
                    double vatp = Convert.ToDouble(dtdata2.Rows[c]["taxtype"].ToString().Split('-')[0]);
                    double addvatp = Convert.ToDouble(dtdata2.Rows[c]["taxtype"].ToString().Split('-')[1]);
                    dr1["taxdesc"] = (vatp + addvatp).ToString();
                }
                else
                {
                    dr1["taxdesc"] = dtdata2.Rows[c]["taxtype"].ToString();
                }

                li.strsino = dtdata2.Rows[c]["strsino"].ToString();
                SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
                DataTable dtsi = new DataTable();
                dasi.Fill(dtsi);
                dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
                SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and ((city not like '%'+@fullname+'%') or (city like '%'+@fullname+'%' and SIMaster.billamount<=250000)) and ACMaster.gstno=''", con);
                daa.SelectCommand.Parameters.AddWithValue("@fullname", "gujarat");
                DataTable dta = new DataTable();
                daa.Fill(dta);
                if (dta.Rows.Count > 0)
                {
                    dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
                    if (dta.Rows[0]["gstno"].ToString() != "")
                    {
                        dr1["rcm"] = "N";
                    }
                    else
                    {
                        dr1["rcm"] = "Y";
                    }
                    dr1["city"] = dta.Rows[0]["city"].ToString();
                }
                else
                {
                    dr1["gstno"] = "";
                    dr1["city"] = "";
                    dr1["rcm"] = "";
                }
                if (dta.Rows.Count > 0)
                {
                    dtq2.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq2;
            Int64 count1 = 1;
            for (int r = 0; r < dtq2.Rows.Count; r++)
            {
                count1 = count1 + 1;
                xlNewSheet2.Cells[count1, 1] = dtq2.Rows[r]["strsino"].ToString();
                xlNewSheet2.Cells[count1, 2] = dtq2.Rows[r]["sidate"].ToString();
                xlNewSheet2.Cells[count1, 3] = dtq2.Rows[r]["billamount"].ToString();
                xlNewSheet2.Cells[count1, 4] = dtq2.Rows[r]["city"].ToString();
                xlNewSheet2.Cells[count1, 5] = dtq2.Rows[r]["taxdesc"].ToString();
                xlNewSheet2.Cells[count1, 6] = dtq2.Rows[r]["totbasicamount"].ToString();
                xlNewSheet2.Cells[count1, 7] = "";
                xlNewSheet2.Cells[count1, 8] = "";
            }
            xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            xlNewSheet2.Select();


        }

        //End of 2 Sheet


        //3 Sheet
        Microsoft.Office.Interop.Excel.Sheets worksheets1 = xlWorkBook.Worksheets;
        var xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet1.Name = "b2cl";
        xlNewSheet1.Cells[1, 1] = "Invoice Number";
        xlNewSheet1.Cells[1, 2] = "Invoice date";
        xlNewSheet1.Cells[1, 3] = "Invoice Value";
        xlNewSheet1.Cells[1, 4] = "Place Of Supply";
        xlNewSheet1.Cells[1, 5] = "Rate";
        xlNewSheet1.Cells[1, 6] = "Taxable Value";
        xlNewSheet1.Cells[1, 7] = "Cess Amount";
        xlNewSheet1.Cells[1, 8] = "E-Commerce GSTIN";
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        Session["dtpitems"] = CreateTemplate();
        DataTable dtq1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter("select SIItems.strsino,SIItems.sidate,SUM(SIItems.basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+SUM((CASE WHEN (SIMaster.salesac != 'Sales A/c. IGST - Export') THEN SIItems.cstamt ELSE 0 END))) as totbillamt,SIItems.taxtype from SIItems inner join simaster on simaster.strsino=SIItems.strsino where simaster.sidate between @fromdate and @todate group by SIItems.taxtype,SIItems.strsino,SIItems.sidate,SIItems.sino order by SIItems.sino", con);
        da1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata1 = new DataTable();
        da1.Fill(dtdata1);
        if (dtdata1.Rows.Count > 0)
        {
            for (int c = 0; c < dtdata1.Rows.Count; c++)
            {
                dtq1 = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq1.NewRow();
                dr1["strsino"] = dtdata1.Rows[c]["strsino"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtdata1.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
                dr1["totbasicamount"] = dtdata1.Rows[c]["totbasic"].ToString();

                var cc = dtdata1.Rows[c]["taxtype"].ToString().IndexOf("-");
                if (cc != -1)
                {
                    double vatp = Convert.ToDouble(dtdata1.Rows[c]["taxtype"].ToString().Split('-')[0]);
                    double addvatp = Convert.ToDouble(dtdata1.Rows[c]["taxtype"].ToString().Split('-')[1]);
                    dr1["taxdesc"] = (vatp + addvatp).ToString();
                }
                else
                {
                    dr1["taxdesc"] = dtdata1.Rows[c]["taxtype"].ToString();
                }

                li.strsino = dtdata1.Rows[c]["strsino"].ToString();
                SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
                DataTable dtsi = new DataTable();
                dasi.Fill(dtsi);
                dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
                SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and city not like '%'+@fullname+'%' and SIMaster.billamount>250000 and ACMaster.gstno!=''", con);
                daa.SelectCommand.Parameters.AddWithValue("@fullname", "gujarat");
                DataTable dta = new DataTable();
                daa.Fill(dta);
                if (dta.Rows.Count > 0)
                {
                    dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
                    if (dta.Rows[0]["gstno"].ToString() != "")
                    {
                        dr1["rcm"] = "N";
                    }
                    else
                    {
                        dr1["rcm"] = "Y";
                    }
                    dr1["city"] = dta.Rows[0]["city"].ToString();
                }
                else
                {
                    dr1["gstno"] = "";
                    dr1["city"] = "";
                    dr1["rcm"] = "";
                }
                if (dta.Rows.Count > 0)
                {
                    dtq1.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq1;
            Int64 count1 = 1;
            for (int r = 0; r < dtq1.Rows.Count; r++)
            {
                count1 = count1 + 1;
                xlNewSheet1.Cells[count1, 1] = dtq1.Rows[r]["strsino"].ToString();
                xlNewSheet1.Cells[count1, 2] = dtq1.Rows[r]["sidate"].ToString();
                xlNewSheet1.Cells[count1, 3] = dtq1.Rows[r]["billamount"].ToString();
                xlNewSheet1.Cells[count1, 4] = dtq1.Rows[r]["city"].ToString();
                xlNewSheet1.Cells[count1, 5] = dtq1.Rows[r]["taxdesc"].ToString();
                xlNewSheet1.Cells[count1, 6] = dtq1.Rows[r]["totbasicamount"].ToString();
                xlNewSheet1.Cells[count1, 7] = "";
                xlNewSheet1.Cells[count1, 8] = "";
            }
            xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
            xlNewSheet1.Select();

        }
        //End of 3 Sheet


        //4 Sheet
        Microsoft.Office.Interop.Excel.Sheets worksheets3 = xlWorkBook.Worksheets;
        var xlNewSheet3 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets3.Add(worksheets3[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet3.Name = "b2b";
        xlNewSheet3.Cells[1, 1] = "GSTIN/UIN of Recipient";
        xlNewSheet3.Cells[1, 2] = "Invoice Number";
        xlNewSheet3.Cells[1, 3] = "Invoice date";
        xlNewSheet3.Cells[1, 4] = "Invoice Value";
        xlNewSheet3.Cells[1, 5] = "Place Of Supply";
        xlNewSheet3.Cells[1, 6] = "Reverse Charge";
        xlNewSheet3.Cells[1, 7] = "Invoice Type";
        xlNewSheet3.Cells[1, 8] = "E-Commerce GSTIN";
        xlNewSheet3.Cells[1, 9] = "Rate";
        xlNewSheet3.Cells[1, 10] = "Taxable Value";
        xlNewSheet3.Cells[1, 11] = "Cess Amount";
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        Session["dtpitems"] = CreateTemplate2();
        DataTable dtq3 = new DataTable();
        SqlDataAdapter da3 = new SqlDataAdapter("select strsino,sidate,SUM(basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+sum(SIItems.cstamt)) as totbillamt,taxtype from SIItems where sidate between @fromdate and @todate group by taxtype,strsino,sidate,sino order by sino", con);
        da3.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da3.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata3 = new DataTable();
        da3.Fill(dtdata3);
        if (dtdata3.Rows.Count > 0)
        {
            for (int c = 0; c < dtdata3.Rows.Count; c++)
            {
                dtq3 = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq3.NewRow();
                dr1["strsino"] = dtdata3.Rows[c]["strsino"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtdata3.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
                dr1["totbasicamount"] = dtdata3.Rows[c]["totbasic"].ToString();

                var cc = dtdata3.Rows[c]["taxtype"].ToString().IndexOf("-");
                if (cc != -1)
                {
                    double vatp = Convert.ToDouble(dtdata3.Rows[c]["taxtype"].ToString().Split('-')[0]);
                    double addvatp = Convert.ToDouble(dtdata3.Rows[c]["taxtype"].ToString().Split('-')[1]);
                    dr1["taxdesc"] = (vatp + addvatp).ToString();
                }
                else
                {
                    dr1["taxdesc"] = dtdata3.Rows[c]["taxtype"].ToString();
                }

                li.strsino = dtdata3.Rows[c]["strsino"].ToString();
                SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
                DataTable dtsi = new DataTable();
                dasi.Fill(dtsi);
                dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
                SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and ACMaster.gstno!=''", con);
                DataTable dta = new DataTable();
                daa.Fill(dta);
                if (dta.Rows.Count > 0)
                {
                    dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
                    if (dta.Rows[0]["gstno"].ToString() != "")
                    {
                        dr1["rcm"] = "N";
                    }
                    else
                    {
                        dr1["rcm"] = "Y";
                    }
                    dr1["city"] = dta.Rows[0]["city"].ToString();
                }
                else
                {
                    dr1["gstno"] = "";
                    dr1["city"] = "";
                    dr1["rcm"] = "";
                }
                if (dta.Rows.Count > 0)
                {
                    dtq3.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq3;
            Int64 count1 = 1;
            for (int r = 0; r < dtq3.Rows.Count; r++)
            {
                count1 = count1 + 1;
                xlNewSheet3.Cells[count1, 1] = dtq3.Rows[r]["gstno"].ToString();
                xlNewSheet3.Cells[count1, 2] = dtq3.Rows[r]["strsino"].ToString();
                xlNewSheet3.Cells[count1, 3] = dtq3.Rows[r]["sidate"].ToString();
                xlNewSheet3.Cells[count1, 4] = dtq3.Rows[r]["billamount"].ToString();
                xlNewSheet3.Cells[count1, 5] = dtq3.Rows[r]["city"].ToString();
                xlNewSheet3.Cells[count1, 6] = dtq3.Rows[r]["rcm"].ToString();
                xlNewSheet3.Cells[count1, 7] = "Regular";
                xlNewSheet3.Cells[count1, 8] = "";
                xlNewSheet3.Cells[count1, 9] = dtq3.Rows[r]["taxdesc"].ToString();
                xlNewSheet3.Cells[count1, 10] = dtq3.Rows[r]["totbasicamount"].ToString();
                xlNewSheet3.Cells[count1, 11] = "0";
            }
            xlNewSheet3 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(4);
            xlNewSheet3.Select();

        }
        //End of 4 Sheet


        xlWorkBook.Save();
        xlWorkBook.Close();

        releaseObject(xlNewSheet);
        releaseObject(worksheets);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        string filename = dd;
        Response.ContentType = "application/octet-stream";
        Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
        string aaa = Server.MapPath("~/GST Return/" + filename);
        Response.TransmitFile(Server.MapPath("~/GST Return/" + filename));
        Response.End();
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No HSN data available for export.');", true);
        //    return;
        //}
    }

    public DataTable CreateTemplate2()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("gstno", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(string));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("city", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("rcm", typeof(string));

        //dtpitems.Columns.Add("gstno", typeof(string));
        //dtpitems.Columns.Add("strsino", typeof(DateTime));
        //dtpitems.Columns.Add("sidate", typeof(string));
        //dtpitems.Columns.Add("billamount", typeof(double));
        //dtpitems.Columns.Add("city", typeof(double));
        //dtpitems.Columns.Add("taxdesc", typeof(double));
        //dtpitems.Columns.Add("totbasicamount", typeof(string));
        return dtpitems;
    }

    public DataTable CreateTemplate1()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("gstno", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(string));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("city", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("rcm", typeof(string));

        //dtpitems.Columns.Add("gstno", typeof(string));
        //dtpitems.Columns.Add("strsino", typeof(DateTime));
        //dtpitems.Columns.Add("sidate", typeof(string));
        //dtpitems.Columns.Add("billamount", typeof(double));
        //dtpitems.Columns.Add("city", typeof(double));
        //dtpitems.Columns.Add("taxdesc", typeof(double));
        //dtpitems.Columns.Add("totbasicamount", typeof(string));
        return dtpitems;
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("gstno", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(string));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("city", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("rcm", typeof(string));

        //dtpitems.Columns.Add("gstno", typeof(string));
        //dtpitems.Columns.Add("strsino", typeof(DateTime));
        //dtpitems.Columns.Add("sidate", typeof(string));
        //dtpitems.Columns.Add("billamount", typeof(double));
        //dtpitems.Columns.Add("city", typeof(double));
        //dtpitems.Columns.Add("taxdesc", typeof(double));
        //dtpitems.Columns.Add("totbasicamount", typeof(string));
        return dtpitems;
    }

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Exception Occured while releasing object " + ex.ToString() + "');", true);
            //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }

    protected void btngsthsn_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
        //{
            fillgridhsn();
            ExportGridToExcel1();
       // }
    }
}