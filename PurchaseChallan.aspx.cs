﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;
using System.Data.SqlClient;

public partial class PurchaseChallan : System.Web.UI.Page
{
    ForPurchaseChallan fpcclass = new ForPurchaseChallan();
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "update")
            {
                fillacnamedrop();
                fillitemnamedrop();
                fillstatusdrop();
                filleditdata();
                //hideimage();
            }
            else
            {
                fillacnamedrop();
                fillitemnamedrop();
                getpcno();
                fillstatusdrop();
                Session["dtpitemspuch"] = null;
                Session["dtpitemspuch1"] = null;
                txtpcdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspuch"] = CreateTemplate();
                Page.SetFocus(txtchallanno);
            }
            //hideimage();

        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void hideimage()
    {
        for (int c = 0; c < gvpcitemlist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvpcitemlist.Rows[c].FindControl("imgbtnselect11");
            if (btnsave.Text == "Save")
            {
                img.Visible = false;
            }
            else
            {
                img.Visible = true;
            }
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void getpcno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        //dtvno = fpcclass.selectunusedpurchasechallanno(li);
        dtvno = fpcclass.selectunusedpurchasechallannolast(li);
        if (dtvno.Rows.Count > 0)
        {
            txtpcno.Text = (Convert.ToInt64(dtvno.Rows[0]["pcno"].ToString()) + 1).ToString();
        }
        else
        {
            txtpcno.Text = "";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("vno", typeof(string));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("stockqty", typeof(double));
        dtpitems.Columns.Add("qtyremain", typeof(double));
        dtpitems.Columns.Add("qtyused", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("vid", typeof(Int64));
        return dtpitems;
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.pcno = Convert.ToInt64(Request["pcno"].ToString());
        ViewState["pcno"] = li.pcno;
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallpurchasechallandatafrompcno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtpcno.Text = dtdata.Rows[0]["pcno"].ToString();
            txtpcdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtchallanno.Text = dtdata.Rows[0]["chno"].ToString();
            txtpono.Text = dtdata.Rows[0]["pono"].ToString();
            txtpodate.Text = Convert.ToDateTime(dtdata.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtpono1.Text = dtdata.Rows[0]["pono1"].ToString();
            if (dtdata.Rows[0]["podate1"].ToString() != string.Empty)
            {
                txtpodate1.Text = Convert.ToDateTime(dtdata.Rows[0]["podate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtsrtringpono.Text = dtdata.Rows[0]["stringpono"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtpcitems = new DataTable();
            dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
            if (dtpcitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpcitemlist.Visible = true;
                gvpcitemlist.DataSource = dtpcitems;
                gvpcitemlist.DataBind();
                lblcount.Text = dtpcitems.Rows.Count.ToString();
            }
            else
            {
                gvpcitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Purchase Challan Items Found.";
                lblcount.Text = "0";
            }

            btnsave.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getitemname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallitemname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getpono(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallpono(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtcode.Text.Trim() != string.Empty)
        {
            string cc = txtcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtcode.Text = string.Empty;
        }
        Page.SetFocus(txtdescription1);
    }

    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtstockqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                //txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txtbasicamt.Text = Math.Round((Convert.ToDouble(txtbillqty.Text) * Convert.ToDouble(txtrate.Text)), 2).ToString();
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                //txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            //txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
        }
        Page.SetFocus(txtbillqty);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemspuch"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvpcitemlist.Visible = true;
            lblempty.Visible = false;
            gvpcitemlist.DataSource = dtsession;
            gvpcitemlist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvpcitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            lblcount.Text = "0";
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemspuch"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["vno"] = 0;
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["unit"] = txtunit.Text;
            dr["qty"] = txtbillqty.Text;
            dr["stockqty"] = txtstockqty.Text;
            dr["qtyremain"] = 0;
            dr["qtyused"] = 0;
            dr["rate"] = txtrate.Text;
            dr["ccode"] = txtcode.Text;
            dr["basicamount"] = txtbasicamt.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            dr["descr3"] = txtdescription3.Text;
            dt.Rows.Add(dr);
            Session["dtpitemspuch"] = dt;
            this.bindgrid();
        }
        else
        {
            li.pcno = Convert.ToInt64(txtpcno.Text);
            li.vnono = Convert.ToInt64(txtpcno.Text);
            li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.unit = txtunit.Text;
            li.stockqty = Convert.ToDouble(txtstockqty.Text);
            li.qty = Convert.ToDouble(txtbillqty.Text);
            li.qtyremain1 = Convert.ToDouble(txtbillqty.Text);
            li.qtyused1 = 0;
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.ccode = Convert.ToInt64(txtcode.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            li.descr3 = txtdescription3.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fpcclass.insertpcitemsdata(li);
            li.activity = li.pcno + " New Purchase Challan Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtdata = new DataTable();
            dtdata = fpcclass.selectallpcitemsfrompcno(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvpcitemlist.Visible = true;
                gvpcitemlist.DataSource = dtdata;
                gvpcitemlist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvpcitemlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                lblcount.Text = "0";
            }
        }
        fillitemnamedrop();
        txtbillqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txtcode.Text = string.Empty;
        txtdescription1.Text = string.Empty;
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        hideimage();
        Page.SetFocus(drpitemname);
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            //for (int p = 0; p < gvpcitemlist.Rows.Count; p++)
            //{
            //    TextBox txtgvstockqty = (TextBox)gvpcitemlist.Rows[p].FindControl("txtgvstockqty");
            //    if (Convert.ToDouble(txtgvstockqty.Text) <= 0 || txtgvstockqty.Text.Trim() == string.Empty)
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Stock Qty must be greater than 0(Zero).');", true);
            //        return;
            //    }
            //}
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtc3 = new DataTable();
            dtc3 = fpcclass.selectpcfrompartychallanno(li);
            if (dtc3.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Party Challan No Already used.Enter new Party Challan No and Try Again.');", true);
                return;
            }
        }
        li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.pcdate <= yyyear1 && li.pcdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.pcno = Convert.ToInt64(txtpcno.Text);
        li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.challanno = Convert.ToInt64(txtchallanno.Text);
        if (txtpono.Text.Trim() != string.Empty)
        {
            li.strpono = txtpono.Text;
        }
        else
        {
            li.strpono = "0";
        }
        li.podate = Convert.ToDateTime(txtpodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtpono1.Text.Trim() != string.Empty)
        {
            li.strpono1 = txtpono1.Text;
        }
        else
        {
            li.strpono1 = "0";
        }
        if (txtpodate1.Text.Trim() != string.Empty)
        {
            li.podate1 = Convert.ToDateTime(txtpodate1.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.podate1 = sqldatenull;
        }
        li.acname = drpacname.SelectedItem.Text;
        li.status = drpstatus.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.stringpono = txtsrtringpono.Text;
        if (btnsave.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fpcclass.selectallpurchasechallandatafrompcno(li);
            if (dtcheck.Rows.Count == 0)
            {
                fpcclass.insertpcmasterdata(li);
                li.activity = li.pcno + " New Purchase Challan Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                li.pcno = Convert.ToInt64(txtchallanno.Text);
                fpcclass.updateisused(li);
                getpcno();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Purchase Challan No. already exist.Try Again.');", true);
                return;
            }
            //li.pcno = SessionMgt.voucherno;
            li.pcno = Convert.ToInt64(txtpcno.Text);
            for (int c = 0; c < gvpcitemlist.Rows.Count; c++)
            {
                Label lblid = (Label)gvpcitemlist.Rows[c].FindControl("lblid");
                Label lblvno = (Label)gvpcitemlist.Rows[c].FindControl("lblvno");
                Label lblitemname = (Label)gvpcitemlist.Rows[c].FindControl("lblitemname");
                TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                TextBox txtgvstockqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgvstockqty");
                TextBox txtgvunit = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgvunit");
                Label lblqtyremain = (Label)gvpcitemlist.Rows[c].FindControl("lblqtyremain");
                Label lblqtyused = (Label)gvpcitemlist.Rows[c].FindControl("lblqtyused");
                TextBox lblrate = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridrate");
                Label lblccode = (Label)gvpcitemlist.Rows[c].FindControl("lblccode");
                TextBox lblamount = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridamount");
                TextBox lbldesc1 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc3");
                li.itemname = lblitemname.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                li.unit = txtgvunit.Text;
                //if (li.qty > 0)
                //{
                li.qtyused = Convert.ToDouble(lblqty.Text) + Convert.ToDouble(lblqtyused.Text);
                li.qtyremain = Convert.ToDouble(lblqtyremain.Text) - li.qty;
                li.qtyremain1 = li.qty;
                li.qtyused1 = 0;
                li.rate = Convert.ToDouble(lblrate.Text);
                li.ccode = Convert.ToInt64(lblccode.Text);
                li.basicamount = Convert.ToDouble(lblamount.Text);
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                li.vnono = Convert.ToInt64(lblvno.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                fpcclass.insertpcitemsdata(li);
                li.id = Convert.ToInt64(lblid.Text);
                li.vnono1 = 0;
                fpcclass.updateremainqtyinpurchaseorder(li);
                //li.pcno = Convert.ToInt64(txtpcno.Text);
                li.pcno = Convert.ToInt64(txtpcno.Text);
                fpcclass.updatepcnoinpoitems(li);
                //For Pending PO Items
                DataTable dtqty = new DataTable();
                dtqty = fpcclass.selectqtyremainusedfromscno1cc(li);
                if (dtqty.Rows[0]["qtyremain"].ToString() == "0")
                {
                    li.iscame = "Yes";
                }
                else
                {
                    li.iscame = "No";
                }
                fpcclass.updateiscame(li);
                //}
            }
            fpcclass.updateisused(li);
        }
        else
        {
            if (ViewState["pcno"].ToString().Trim() != txtpcno.Text.Trim())
            {
                li.pcno1 = Convert.ToInt64(txtpcno.Text);
                li.pcno = Convert.ToInt64(ViewState["pcno"].ToString());
                fpcclass.updatepcmasterdataupdatepc(li);
                fpcclass.updatepcitemsdataupdatepc(li);
                fpcclass.deletepcmasterdata(li);
                fpcclass.deletepcitemsdata(li);
            }
            li.pcno = Convert.ToInt64(txtpcno.Text);
            fpcclass.updatepcmasterdata(li);
            li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            fpcclass.updatepcitemsdataupdatepcitemsdate(li);

            //update all item together
            for (int y = 0; y < gvpcitemlist.Rows.Count; y++)
            {
                li.pcno = Convert.ToInt64(txtpcno.Text);
                li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                Label lblid = (Label)gvpcitemlist.Rows[y].FindControl("lblid");
                Label lblqty = (Label)gvpcitemlist.Rows[y].FindControl("lblqty");
                TextBox txtgvqty1 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridqty");
                li.id = Convert.ToInt64(lblid.Text);
                li.qty = Convert.ToDouble(lblqty.Text);
                li.vid = Convert.ToInt64(lblid.Text);
                DataTable dtremain = new DataTable();
                dtremain = fpcclass.selectqtyremainusedfromscno(li);
                if (Convert.ToDouble(txtgvqty1.Text) > 0)
                {
                    if (dtremain.Rows.Count > 0)
                    {
                        //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                        li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                        li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                        fpcclass.updateremainqtyinpurchaseorder(li);

                        Label lblitemname = (Label)gvpcitemlist.Rows[y].FindControl("lblitemname");
                        //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox txtgvunit = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvunit");
                        TextBox lblrate = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridrate");
                        Label lblccode = (Label)gvpcitemlist.Rows[y].FindControl("lblccode");
                        TextBox lblamount = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbldesc1 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgvstockqty = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvstockqty");
                        li.id = Convert.ToInt64(lblid.Text);
                        li.itemname = lblitemname.Text;
                        li.unit = txtgvunit.Text;
                        if (txtgvstockqty.Text.Trim() != string.Empty)
                        {
                            li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        }
                        else
                        {
                            li.stockqty = 0;
                        }
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)B
                        //{
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.ccode = Convert.ToInt64(lblccode.Text);
                        li.basicamount = Convert.ToDouble(lblamount.Text);
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        fpcclass.updatepcitemsdata(li);
                        //}
                        //}

                    }
                    else
                    {
                        Label lblitemname = (Label)gvpcitemlist.Rows[y].FindControl("lblitemname");
                        TextBox txtgvunit = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvunit");
                        //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                        TextBox lblrate = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridrate");
                        Label lblccode = (Label)gvpcitemlist.Rows[y].FindControl("lblccode");
                        TextBox lblamount = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridamount");
                        TextBox lbldesc1 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc1");
                        TextBox lbldesc2 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc2");
                        TextBox lbldesc3 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc3");
                        TextBox txtgvstockqty = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvstockqty");
                        li.id = Convert.ToInt64(lblid.Text);
                        li.itemname = lblitemname.Text;
                        li.unit = txtgvunit.Text;
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                        li.qty = Convert.ToDouble(txtgvqty1.Text);
                        li.qtyremain = li.qty;
                        li.qtyused = 0;
                        //if (li.qty > 0)
                        //{
                        li.rate = Convert.ToDouble(lblrate.Text);
                        li.ccode = Convert.ToInt64(lblccode.Text);
                        li.basicamount = Convert.ToDouble(lblamount.Text);
                        li.descr1 = lbldesc1.Text;
                        li.descr2 = lbldesc2.Text;
                        li.descr3 = lbldesc3.Text;
                        fpcclass.updatepcitemsdata(li);
                    }
                }
                else
                {
                    Label lblitemname = (Label)gvpcitemlist.Rows[y].FindControl("lblitemname");
                    TextBox txtgvunit = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvunit");
                    //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridrate");
                    Label lblccode = (Label)gvpcitemlist.Rows[y].FindControl("lblccode");
                    TextBox lblamount = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgridamount");
                    TextBox lbldesc1 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)gvpcitemlist.Rows[y].FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)gvpcitemlist.Rows[y].FindControl("txtgvstockqty");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.unit = txtgvunit.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    li.basicamount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpcclass.updatepcitemsdata(li);
                }
            }
            //

            li.activity = li.pcno + "Purchase Challan Updated.";
            faclass.insertactivity(li);
            li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            //for (int c = 0; c < gvpcitemlist.Rows.Count; c++)
            //{
            //    Label lblid = (Label)gvpcitemlist.Rows[c].FindControl("lblid");
            //    Label lblitemname = (Label)gvpcitemlist.Rows[c].FindControl("lblitemname");
            //    TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
            //    TextBox lblrate = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridrate");
            //    Label lblccode = (Label)gvpcitemlist.Rows[c].FindControl("lblccode");
            //    TextBox lblamount = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridamount");
            //    TextBox lbldesc1 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc1");
            //    TextBox lbldesc2 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc2");
            //    TextBox lbldesc3 = (TextBox)gvpcitemlist.Rows[c].FindControl("txtdesc3");
            //    li.id = Convert.ToInt64(lblid.Text);
            //    li.itemname = lblitemname.Text;
            //    li.qty = Convert.ToDouble(lblqty.Text);
            //    if (li.qty > 0)
            //    {
            //        li.rate = Convert.ToDouble(lblrate.Text);
            //        li.ccode = Convert.ToInt64(lblccode.Text);
            //        li.basicamount = Convert.ToDouble(lblamount.Text);
            //        li.descr1 = lbldesc1.Text;
            //        li.descr2 = lbldesc2.Text;
            //        li.descr3 = lbldesc3.Text;
            //        fpcclass.updatepcitemsdata(li);
            //    }
            //}
        }
        li.pcno = Convert.ToInt64(txtpcno.Text);
        li.stringpono = txtsrtringpono.Text;
        string[] words = li.stringpono.Split(',');
        foreach (string word in words)
        {
            li.strpono = word.Replace("'", "").ToString();
            fpcclass.updatepcnoinpomaster(li);
            fpcclass.updatepcnoinpomastera(li);
        }
        Response.Redirect("~/PurchaseChallanList.aspx?pagename=PurchaseChallanList");
    }

    protected void gvpcitemlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemspuch"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemspuch"] = dt;
            this.bindgrid();
        }
        else
        {
            if (gvpcitemlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvpcitemlist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvpcitemlist.Rows[e.RowIndex].FindControl("lblvid");
                li.id = Convert.ToInt64(lblid.Text);
                DataTable dtcheck = new DataTable();
                dtcheck = fpcclass.selectbillinvoicedatafrompcid(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Item Used in Purchase Invoice No. " + dtcheck.Rows[0]["strpino"].ToString() + " So You Cant delete this Item.First delete that item from invoice then try to delete this item.');", true);
                    return;
                }
                else
                {
                    Label lblitemname = (Label)gvpcitemlist.Rows[e.RowIndex].FindControl("lblitemname");
                    Label lblqty = (Label)gvpcitemlist.Rows[e.RowIndex].FindControl("lblqty");
                    //Label lblqtyremain = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyremain");
                    //Label lblqtyused = (Label)gvsiitemlist.Rows[e.RowIndex].FindControl("lblqtyused");
                    Label lblvno = (Label)gvpcitemlist.Rows[e.RowIndex].FindControl("lblvno");
                    li.vid = Convert.ToInt64(lblvid.Text);
                    li.itemname = lblitemname.Text;
                    li.vnono = Convert.ToInt64(lblvno.Text);
                    li.scno = Convert.ToInt64(lblvno.Text);
                    if (li.vid != 0)
                    {
                        DataTable dtqty = new DataTable();
                        dtqty = fpcclass.selectqtyremainusedfromscno(li);
                        li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                        li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                        fpcclass.updateqtyduringdelete(li);

                        //For Pending PO Items
                        DataTable dtqty1 = new DataTable();
                        dtqty1 = fpcclass.selectqtyremainusedfromscno1cc(li);
                        if (dtqty1.Rows[0]["qtyremain"].ToString() == "0")
                        {
                            li.iscame = "Yes";
                        }
                        else
                        {
                            li.iscame = "No";
                        }
                        fpcclass.updateiscame(li);
                        li.vid = Convert.ToInt64(lblvid.Text);
                        li.pcno = 0;
                        fpcclass.updatepcnoinpoitems1(li);
                    }

                    li.id = Convert.ToInt64(lblid.Text);
                    fpcclass.deletepcitemsdatafromid(li);



                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Purchase Challan Item Deleted.";
                    faclass.insertactivity(li);
                    //li.istype = "CR";
                    //flclass.deleteledgerdata(li);
                    li.pcno = Convert.ToInt64(txtpcno.Text);
                    DataTable dtdata = new DataTable();
                    dtdata = fpcclass.selectallpcitemsfrompcno(li);
                    if (dtdata.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpcitemlist.Visible = true;
                        gvpcitemlist.DataSource = dtdata;
                        gvpcitemlist.DataBind();
                        lblcount.Text = dtdata.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpcitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Sales Order Items Found.";
                        lblcount.Text = "0";
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        hideimage();
    }

    public void count1()
    {
        if (txtbillqty.Text.Trim() == string.Empty)
        {
            txtbillqty.Text = "0";
        }
        if (txtrate.Text.Trim() == string.Empty)
        {
            txtrate.Text = "0";
        }
        double qty = 0;
        double rate = 0;
        double amount = 0;
        qty = Convert.ToDouble(txtbillqty.Text);
        rate = Convert.ToDouble(txtrate.Text);
        //amount = Convert.ToDouble();
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
    }

    protected void txtbillqty_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtstockqty);
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtbasicamt);
    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemspuch"];
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double amount = 0;
        for (int c = 0; c < dtsoitem.Rows.Count; c++)
        {
            baseamt = baseamt + Convert.ToDouble(dtsoitem.Rows[c]["qty"].ToString()) * Convert.ToDouble(dtsoitem.Rows[c]["rate"].ToString());
            amount = amount + Convert.ToDouble(dtsoitem.Rows[c]["amount"].ToString());
            vatamt = vatamt + Convert.ToDouble(dtsoitem.Rows[c]["vatamt"].ToString());
            addvatamt = addvatamt + Convert.ToDouble(dtsoitem.Rows[c]["addtaxamt"].ToString());
            if (dtsoitem.Rows[c]["cstp"].ToString() != "")
            {
                cstp = cstp + Convert.ToDouble(dtsoitem.Rows[c]["cstp"].ToString());
            }
            if (dtsoitem.Rows[c]["cstamt"].ToString() != "")
            {
                cstamt = cstamt + Convert.ToDouble(dtsoitem.Rows[c]["cstamt"].ToString());
            }
            if (dtsoitem.Rows[c]["vatp"].ToString() != "")
            {
                vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["vatp"].ToString());
            }
            if (dtsoitem.Rows[c]["addtaxp"].ToString() != "")
            {
                vatp = vatp + Convert.ToDouble(dtsoitem.Rows[c]["addtaxp"].ToString());
            }
        }

    }

    protected void txtpono_TextChanged(object sender, EventArgs e)
    {
        //if (txtpono1.Text.Trim() != string.Empty)
        //{
        //Session["dtpitems"] = null;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        if (txtpono.Text.Trim() != string.Empty)
        {
            li.pono = Convert.ToInt64(txtpono.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpcclass.selectpodatefrompono(li);
            txtpodate.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pono = 0;
        }
        if (txtpono1.Text.Trim() != string.Empty)
        {
            li.pono1 = Convert.ToInt64(txtpono1.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpcclass.selectpodatefrompono1(li);
            txtpodate1.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pono1 = 0;
        }
        li.remarks = li.pono + "," + li.pono1;
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallpcitemsfromponotextchange1(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            ////if (dtsoitem != null)
            ////{
            ////    if (dtsoitem.Rows.Count > 0)
            ////    {
            ////        dtdata.Merge(dtsoitem);
            ////    }
            ////}
            ////lblempty.Visible = false;
            ////gvpcitemlist.Visible = true;
            ////gvpcitemlist.DataSource = dtdata;
            ////gvpcitemlist.DataBind();
            ////Session["dtpitems"] = dtdata;
            DataTable dt = (DataTable)Session["dtpitemspuch"];
            dt.Clear();
            for (int c = 0; c < dtdata.Rows.Count; c++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[c]["id"].ToString();
                dr["vno"] = dtdata.Rows[c]["pono"].ToString();
                dr["itemname"] = dtdata.Rows[c]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[c]["qty"].ToString();
                dr["qtyremain"] = dtdata.Rows[c]["qtyremain"].ToString();
                dr["qtyused"] = dtdata.Rows[c]["qtyused"].ToString();
                dr["rate"] = dtdata.Rows[c]["rate"].ToString();
                dr["ccode"] = dtdata.Rows[c]["ccode"].ToString();
                dr["basicamount"] = dtdata.Rows[c]["basicamount"].ToString();
                dr["descr1"] = dtdata.Rows[c]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[c]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[c]["descr3"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemspuch"] = dt;
            this.bindgrid();
        }
        else
        {
            gvpcitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
        }
        //}
        //else
        //{
        //    gvpcitemlist.Visible = false;
        //    lblempty.Visible = false;
        //}
    }
    protected void txtpono1_TextChanged(object sender, EventArgs e)
    {
        //if (txtpono1.Text.Trim() != string.Empty)
        //{
        //Session["dtpitems"] = null;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        if (txtpono.Text.Trim() != string.Empty)
        {
            li.pono = Convert.ToInt64(txtpono.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpcclass.selectpodatefrompono(li);
            txtpodate.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pono = 0;
        }
        if (txtpono1.Text.Trim() != string.Empty)
        {
            li.pono1 = Convert.ToInt64(txtpono1.Text);
            DataTable dtdate = new DataTable();
            dtdate = fpcclass.selectpodatefrompono1(li);
            txtpodate1.Text = dtdate.Rows[0]["cdate"].ToString();
        }
        else
        {
            li.pono1 = 0;
        }
        li.remarks = li.pono + "," + li.pono1;
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallpcitemsfromponotextchange1(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            ////if (dtsoitem != null)
            ////{
            ////    if (dtsoitem.Rows.Count > 0)
            ////    {
            ////        dtdata.Merge(dtsoitem);
            ////    }
            ////}
            ////lblempty.Visible = false;
            ////gvpcitemlist.Visible = true;
            ////gvpcitemlist.DataSource = dtdata;
            ////gvpcitemlist.DataBind();
            ////Session["dtpitems"] = dtdata;
            DataTable dt = (DataTable)Session["dtpitemspuch"];
            dt.Clear();
            for (int c = 0; c < dtdata.Rows.Count; c++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = dtdata.Rows[c]["id"].ToString();
                dr["vno"] = dtdata.Rows[c]["pono"].ToString();
                dr["itemname"] = dtdata.Rows[c]["itemname"].ToString();
                dr["qty"] = dtdata.Rows[c]["qty"].ToString();
                dr["qtyremain"] = dtdata.Rows[c]["qtyremain"].ToString();
                dr["qtyused"] = dtdata.Rows[c]["qtyused"].ToString();
                dr["rate"] = dtdata.Rows[c]["rate"].ToString();
                dr["ccode"] = dtdata.Rows[c]["ccode"].ToString();
                dr["basicamount"] = dtdata.Rows[c]["basicamount"].ToString();
                dr["descr1"] = dtdata.Rows[c]["descr1"].ToString();
                dr["descr2"] = dtdata.Rows[c]["descr2"].ToString();
                dr["descr3"] = dtdata.Rows[c]["descr3"].ToString();
                dt.Rows.Add(dr);
            }
            Session["dtpitemspuch"] = dt;
            this.bindgrid();
        }
        else
        {
            gvpcitemlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
        }
        //}
        //else
        //{
        //    gvpcitemlist.Visible = false;
        //    lblempty.Visible = false;
        //}


        ////////////if (txtpono.Text.Trim() != string.Empty)
        ////////////{        
        //////////DataTable dt = (DataTable)Session["dtpitems"];
        //////////dt.Clear();
        //////////li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //////////if (txtpono.Text.Trim() != string.Empty)
        //////////{
        //////////    li.pono = Convert.ToInt64(txtpono.Text);
        //////////    DataTable dtdate = new DataTable();
        //////////    dtdate = fpcclass.selectpodatefrompono(li);
        //////////    txtpodate.Text = dtdate.Rows[0]["cdate"].ToString();
        //////////    DataTable dtdata = new DataTable();
        //////////    dtdata = fpcclass.selectallpcitemsfromponotextchangeone(li);
        //////////    if (dtdata.Rows.Count > 0)
        //////////    {
        //////////        txtname.Text = dtdata.Rows[0]["acname"].ToString();
        //////////        ////if (dtsoitem != null)
        //////////        ////{
        //////////        ////    if (dtsoitem.Rows.Count > 0)
        //////////        ////    {
        //////////        ////        dtdata.Merge(dtsoitem);
        //////////        ////    }
        //////////        ////}
        //////////        ////lblempty.Visible = false;
        //////////        ////gvpcitemlist.Visible = true;
        //////////        ////gvpcitemlist.DataSource = dtdata;
        //////////        ////gvpcitemlist.DataBind();
        //////////        ////Session["dtpitems"] = dtdata;            
        //////////        for (int c = 0; c < dtdata.Rows.Count; c++)
        //////////        {
        //////////            DataRow dr = dt.NewRow();
        //////////            dr["id"] = dtdata.Rows[c]["id"].ToString();
        //////////            dr["vno"] = dtdata.Rows[c]["pono"].ToString();
        //////////            dr["itemname"] = dtdata.Rows[c]["itemname"].ToString();
        //////////            dr["qty"] = dtdata.Rows[c]["qty"].ToString();
        //////////            dr["qtyremain"] = dtdata.Rows[c]["qtyremain"].ToString();
        //////////            dr["qtyused"] = dtdata.Rows[c]["qtyused"].ToString();
        //////////            dr["rate"] = dtdata.Rows[c]["rate"].ToString();
        //////////            dr["ccode"] = dtdata.Rows[c]["ccode"].ToString();
        //////////            dr["basicamount"] = dtdata.Rows[c]["basicamount"].ToString();
        //////////            dr["descr1"] = dtdata.Rows[c]["descr1"].ToString();
        //////////            dr["descr2"] = dtdata.Rows[c]["descr2"].ToString();
        //////////            dr["descr3"] = dtdata.Rows[c]["descr3"].ToString();
        //////////            dt.Rows.Add(dr);
        //////////        }
        //////////        //Session["dtpitems"] = dt;
        //////////        //this.bindgrid();
        //////////    }
        //////////    //else
        //////////    //{
        //////////    //    gvpcitemlist.Visible = false;
        //////////    //    lblempty.Visible = true;
        //////////    //    lblempty.Text = "No Items Found.";
        //////////    //}
        //////////}
        //////////else
        //////////{
        //////////    li.pono = 0;
        //////////}
        //////////if (txtpono1.Text.Trim() != string.Empty)
        //////////{
        //////////    li.pono1 = Convert.ToInt64(txtpono1.Text);
        //////////    DataTable dtdate = new DataTable();
        //////////    dtdate = fpcclass.selectpodatefrompono1(li);
        //////////    txtpodate1.Text = dtdate.Rows[0]["cdate"].ToString();
        //////////    DataTable dtdata = new DataTable();
        //////////    dtdata = fpcclass.selectallpcitemsfromponotextchangetwo(li);
        //////////    if (dtdata.Rows.Count > 0)
        //////////    {
        //////////        txtname.Text = dtdata.Rows[0]["acname"].ToString();
        //////////        ////if (dtsoitem != null)
        //////////        ////{
        //////////        ////    if (dtsoitem.Rows.Count > 0)
        //////////        ////    {
        //////////        ////        dtdata.Merge(dtsoitem);
        //////////        ////    }
        //////////        ////}
        //////////        ////lblempty.Visible = false;
        //////////        ////gvpcitemlist.Visible = true;
        //////////        ////gvpcitemlist.DataSource = dtdata;
        //////////        ////gvpcitemlist.DataBind();
        //////////        ////Session["dtpitems"] = dtdata;            
        //////////        for (int c = 0; c < dtdata.Rows.Count; c++)
        //////////        {
        //////////            DataRow dr = dt.NewRow();
        //////////            dr["id"] = dtdata.Rows[c]["id"].ToString();
        //////////            dr["vno"] = dtdata.Rows[c]["pono1"].ToString();
        //////////            dr["itemname"] = dtdata.Rows[c]["itemname"].ToString();
        //////////            dr["qty"] = dtdata.Rows[c]["qty"].ToString();
        //////////            dr["qtyremain"] = dtdata.Rows[c]["qtyremain"].ToString();
        //////////            dr["qtyused"] = dtdata.Rows[c]["qtyused"].ToString();
        //////////            dr["rate"] = dtdata.Rows[c]["rate"].ToString();
        //////////            dr["ccode"] = dtdata.Rows[c]["ccode"].ToString();
        //////////            dr["basicamount"] = dtdata.Rows[c]["basicamount"].ToString();
        //////////            dr["descr1"] = dtdata.Rows[c]["descr1"].ToString();
        //////////            dr["descr2"] = dtdata.Rows[c]["descr2"].ToString();
        //////////            dr["descr3"] = dtdata.Rows[c]["descr3"].ToString();
        //////////            dt.Rows.Add(dr);
        //////////        }
        //////////        //Session["dtpitems"] = dt;
        //////////        //this.bindgrid();
        //////////    }
        //////////    //else
        //////////    //{
        //////////    //    gvpcitemlist.Visible = false;
        //////////    //    lblempty.Visible = true;
        //////////    //    lblempty.Text = "No Items Found.";
        //////////    //}
        //////////}
        //////////else
        //////////{
        //////////    li.pono1 = 0;
        //////////}
        //////////if (dt.Rows.Count > 0)
        //////////{
        //////////    Session["dtpitems"] = dt;
        //////////    this.bindgrid();
        //////////}
        //////////else
        //////////{
        //////////    gvpcitemlist.Visible = false;
        //////////    lblempty.Visible = true;
        //////////    lblempty.Text = "No Items Found.";
        //////////}
        ////////////li.remarks = li.pono + "," + li.pono1;

        ////////////}
        ////////////else
        ////////////{
        ////////////    gvpcitemlist.Visible = false;
        ////////////    lblempty.Visible = false;
        ////////////}
    }

    protected void txtgridqty_TextChanged(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblvid = (Label)currentRow.FindControl("lblvid");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                if (lblvid.Text != "0")
                {
                    //if (Convert.ToDouble(txtgvqty1.Text) <= Convert.ToDouble(lblqtyremain.Text))
                    //{
                    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                    //}
                    //else
                    //{
                    //    txtgvqty1.Text = lblqtyremain.Text;
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                    //    return;
                    //}
                }
                else
                {
                    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                }
            }
            Page.SetFocus(txtgvstockqty);
        }
        else
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            Label lblid = (Label)currentRow.FindControl("lblvid");
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
            TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
            TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
            li.vid = Convert.ToInt64(lblid.Text);
            if (txtgvqty1.Text.Trim() != string.Empty)
            {
                if (li.vid != 0)
                {
                    DataTable dtremain = new DataTable();
                    dtremain = fpcclass.selectqtyremainusedfromscno(li);
                    if (dtremain.Rows.Count > 0)
                    {
                        li.qty = Convert.ToDouble(lblqty.Text);
                        double rqty = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + Convert.ToDouble(lblqty.Text);
                        //double rused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - Convert.ToDouble(lblqty.Text);
                        //if (Convert.ToDouble(txtgvqty1.Text) <= rqty)
                        //{
                        txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                        //}
                        //else
                        //{
                        //    txtgvqty1.Text = lblqty.Text;
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity must be less than or equal to remain quantity.');", true);
                        //    return;
                        //}
                    }
                }
                else
                {
                    txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
                }
            }
            Page.SetFocus(txtgvstockqty);
        }
    }

    protected void txtgridrate_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
        TextBox txtgvrate1 = (TextBox)currentRow.FindControl("txtgridrate");
        TextBox txtgvamount1 = (TextBox)currentRow.FindControl("txtgridamount");
        if (txtgvqty1.Text.Trim() != string.Empty && txtgvrate1.Text.Trim() != string.Empty)
        {
            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
        }
        Page.SetFocus(txtgvamount1);
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            lblpopupacname.Text = drpacname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = drpacname.SelectedItem.Text;
            DataTable dtpc = new DataTable();
            //dtpc = fpcclass.selectallpurchaseorderforpopup(li);
            dtpc = fpcclass.selectallpurchaseorderforpopup1(li);
            if (dtpc.Rows.Count > 0)
            {
                lblemptypo.Visible = false;
                gvpurchaseorder.Visible = true;
                gvpurchaseorder.DataSource = dtpc;
                gvpurchaseorder.DataBind();
                gvpoitemlistselection.Visible = false;
            }
            else
            {
                gvpurchaseorder.Visible = false;
                lblemptypo.Visible = true;
                lblemptypo.Text = "No Purchase Order Found.";
                gvpoitemlistselection.Visible = false;
            }
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvpurchaseorder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            gvpurchaseorder.Visible = false;
            gvpoitemlistselection.Visible = true;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            if (txtpono.Text.Trim() == string.Empty)
            {
                txtpono.Text = e.CommandArgument.ToString();
            }
            else if (txtpono1.Text.Trim() == string.Empty)
            {
                txtpono1.Text = e.CommandArgument.ToString();
            }
            li.remarks = "'" + e.CommandArgument.ToString() + "'";
            txtsrtringpono.Text = txtsrtringpono.Text + li.remarks + ",";
            DataTable dtdata = new DataTable();
            dtdata = fpcclass.selectallpcitemsfromponotextchange1(li);
            if (dtdata.Rows.Count > 0)
            {
                Label1.Visible = false;
                gvpoitemlistselection.Visible = true;
                gvpoitemlistselection.DataSource = dtdata;
                gvpoitemlistselection.DataBind();
            }
            else
            {
                gvpoitemlistselection.Visible = false;
                Label1.Visible = true;
                Label1.Text = "No Purchase Order Item Found.";
            }
        }
        hideimage();
        chkall.Visible = true;
        ModalPopupExtender2.Show();
        Page.SetFocus(txtchallanno);
    }

    protected void btnselectitem_Click(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        Session["dtpitemspuch"] = CreateTemplate();
        DataTable dtpodate = new DataTable();
        if (txtpono.Text.Trim() != string.Empty)
        {
            li.strpono = txtpono.Text;
            dtpodate = fpcclass.selectpodatefromponostring(li);
            if (dtpodate.Rows.Count > 0)
            {
                txtpodate.Text = dtpodate.Rows[0]["cdate"].ToString();
            }
        }
        if (txtpono1.Text.Trim() != string.Empty)
        {
            li.strpono = txtpono1.Text;
            dtpodate = fpcclass.selectpodatefromponostring(li);
            if (dtpodate.Rows.Count > 0)
            {
                txtpodate1.Text = dtpodate.Rows[0]["cdate"].ToString();
            }
        }
        if (chkall.Checked == true)
        {
            string id = "0";
            for (int cc = 0; cc < gvpoitemlistselection.Rows.Count; cc++)
            {
                Label lblid = (Label)gvpoitemlistselection.Rows[cc].FindControl("lblid");
                //CheckBox chkselect = (CheckBox)gvpoitemlistselection.Rows[cc].FindControl("chkselect");
                //if (chkselect.Checked == true)
                //{
                id = id + "," + lblid.Text;
                //}
            }
            li.remarks = id;
            //HiddenField1.Value = id;
            //HiddenField1.Value = HiddenField1.Value + "," + id;
            if (HiddenField1.Value == "")
            {
                HiddenField1.Value = id;
            }
            else
            {
                HiddenField1.Value = HiddenField1.Value + "," + id;
            }
            li.strpono = txtpono.Text;
            if (txtpono.Text.Trim() != string.Empty)
            {
                DataTable dtdata = new DataTable();
                li.remarks = HiddenField1.Value;
                dtdata = fpcclass.selectallpurchaseorderitemdatafromponousingin(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataTable dt = (DataTable)Session["dtpitemspuch"];
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = "0";
                        if (dtpodate.Rows.Count > 0)
                        {
                            dr["ccode"] = dtpodate.Rows[0]["ccode"].ToString();
                        }
                        else
                        {
                            dr["ccode"] = "0";
                        }
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        Session["dtpitemssach"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                        gvpurchaseorder.Visible = false;
                        ModalPopupExtender2.Show();
                    }
                }
                else
                {
                    gvpcitemlist.Visible = false;
                    gvpoitemlistselection.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
        }
        else
        {
            string id = "0";
            for (int cc = 0; cc < gvpoitemlistselection.Rows.Count; cc++)
            {
                Label lblid = (Label)gvpoitemlistselection.Rows[cc].FindControl("lblid");
                CheckBox chkselect = (CheckBox)gvpoitemlistselection.Rows[cc].FindControl("chkselect");
                if (chkselect.Checked == true)
                {
                    id = id + "," + lblid.Text;
                }
            }
            li.remarks = id;
            if (HiddenField1.Value == "")
            {
                HiddenField1.Value = id;
            }
            else
            {
                HiddenField1.Value = HiddenField1.Value + "," + id;
            }
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.strpono = txtpono.Text;
            if (txtpono.Text.Trim() != string.Empty)
            {
                DataTable dtdata = new DataTable();
                li.remarks = HiddenField1.Value;
                dtdata = fpcclass.selectallpurchaseorderitemdatafromponousingin(li);
                if (dtdata.Rows.Count > 0)
                {
                    for (int d = 0; d < dtdata.Rows.Count; d++)
                    {
                        DataTable dt = (DataTable)Session["dtpitemspuch"];
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtdata.Rows[d]["id"].ToString();
                        dr["vno"] = dtdata.Rows[d]["vno"].ToString();
                        dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtdata.Rows[d]["qty"].ToString();
                        dr["stockqty"] = "0";
                        if (dtpodate.Rows.Count > 0)
                        {
                            dr["ccode"] = dtpodate.Rows[0]["ccode"].ToString();
                        }
                        else
                        {
                            dr["ccode"] = "0";
                        }
                        dr["qtyremain"] = dtdata.Rows[d]["qtyremain"].ToString();
                        dr["qtyused"] = dtdata.Rows[d]["qtyused"].ToString();
                        dr["rate"] = dtdata.Rows[d]["rate"].ToString();
                        dr["unit"] = dtdata.Rows[d]["unit"].ToString();
                        dr["basicamount"] = dtdata.Rows[d]["basicamount"].ToString();
                        dr["descr1"] = dtdata.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtdata.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtdata.Rows[d]["descr3"].ToString();
                        dr["vid"] = 0;
                        dt.Rows.Add(dr);
                        Session["dtpitemssach"] = dt;
                        this.bindgrid();
                        //this.bindgrid1();
                        gvpurchaseorder.Visible = false;
                        ModalPopupExtender2.Show();
                    }
                }
                else
                {
                    gvpcitemlist.Visible = false;
                    gvpoitemlistselection.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
        }
        hideimage();
        gvpoitemlistselection.Visible = false;
        ModalPopupExtender2.Hide();
        Page.SetFocus(txtchallanno);
    }

    protected void gvpcitemlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "update1")
        {
            li.pcno = Convert.ToInt64(txtpcno.Text);
            li.pcdate = Convert.ToDateTime(txtpcdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)currentRow.FindControl("lblqty");
            TextBox txtgvqty1 = (TextBox)currentRow.FindControl("txtgridqty");
            li.qty = Convert.ToDouble(lblqty.Text);
            li.vid = Convert.ToInt64(e.CommandArgument);
            DataTable dtremain = new DataTable();
            dtremain = fpcclass.selectqtyremainusedfromscno(li);
            if (Convert.ToDouble(txtgvqty1.Text) > 0)
            {
                if (dtremain.Rows.Count > 0)
                {
                    //li.qtyremain = Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) - (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    //li.qtyused = Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) + (Convert.ToDouble(txtgvqty1.Text) - li.qty);
                    li.qtyremain = (Convert.ToDouble(dtremain.Rows[0]["qtyremain"].ToString()) + li.qty) - (Convert.ToDouble(txtgvqty1.Text));
                    li.qtyused = (Convert.ToDouble(dtremain.Rows[0]["qtyused"].ToString()) - li.qty) + (Convert.ToDouble(txtgvqty1.Text));
                    fpcclass.updateremainqtyinpurchaseorder(li);


                    //update pcitems data
                    //for (int c = 0; c < gvpcitemlist.Rows.Count; c++)
                    //{
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lblamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.unit = txtgvunit.Text;
                    if (txtgvstockqty.Text.Trim() != string.Empty)
                    {
                        li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    }
                    else
                    {
                        li.stockqty = 0;
                    }
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)B
                    //{
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    li.basicamount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpcclass.updatepcitemsdata(li);


                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpcitemlist.Visible = true;
                        gvpcitemlist.DataSource = dtpcitems;
                        gvpcitemlist.DataBind();
                    }
                    else
                    {
                        gvpcitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Purchase Challan Items Found.";
                    }
                    //}
                    //}

                }
                else
                {
                    Label lblid = (Label)currentRow.FindControl("lblid");
                    Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                    TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                    //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                    TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                    Label lblccode = (Label)currentRow.FindControl("lblccode");
                    TextBox lblamount = (TextBox)currentRow.FindControl("txtgridamount");
                    TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                    TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                    TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                    TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                    li.id = Convert.ToInt64(lblid.Text);
                    li.itemname = lblitemname.Text;
                    li.unit = txtgvunit.Text;
                    li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                    li.qty = Convert.ToDouble(txtgvqty1.Text);
                    li.qtyremain = li.qty;
                    li.qtyused = 0;
                    //if (li.qty > 0)
                    //{
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.ccode = Convert.ToInt64(lblccode.Text);
                    li.basicamount = Convert.ToDouble(lblamount.Text);
                    li.descr1 = lbldesc1.Text;
                    li.descr2 = lbldesc2.Text;
                    li.descr3 = lbldesc3.Text;
                    fpcclass.updatepcitemsdata(li);


                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpcitemlist.Visible = true;
                        gvpcitemlist.DataSource = dtpcitems;
                        gvpcitemlist.DataBind();
                    }
                    else
                    {
                        gvpcitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Purchase Challan Items Found.";
                    }
                }
            }
            else
            {
                Label lblid = (Label)currentRow.FindControl("lblid");
                Label lblitemname = (Label)currentRow.FindControl("lblitemname");
                TextBox txtgvunit = (TextBox)currentRow.FindControl("txtgvunit");
                //TextBox lblqty = (TextBox)gvpcitemlist.Rows[c].FindControl("txtgridqty");
                TextBox lblrate = (TextBox)currentRow.FindControl("txtgridrate");
                Label lblccode = (Label)currentRow.FindControl("lblccode");
                TextBox lblamount = (TextBox)currentRow.FindControl("txtgridamount");
                TextBox lbldesc1 = (TextBox)currentRow.FindControl("txtdesc1");
                TextBox lbldesc2 = (TextBox)currentRow.FindControl("txtdesc2");
                TextBox lbldesc3 = (TextBox)currentRow.FindControl("txtdesc3");
                TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
                li.id = Convert.ToInt64(lblid.Text);
                li.itemname = lblitemname.Text;
                li.unit = txtgvunit.Text;
                li.stockqty = Convert.ToDouble(txtgvstockqty.Text);
                li.qty = Convert.ToDouble(txtgvqty1.Text);
                li.qtyremain = li.qty;
                li.qtyused = 0;
                //if (li.qty > 0)
                //{
                li.rate = Convert.ToDouble(lblrate.Text);
                li.ccode = Convert.ToInt64(lblccode.Text);
                li.basicamount = Convert.ToDouble(lblamount.Text);
                li.descr1 = lbldesc1.Text;
                li.descr2 = lbldesc2.Text;
                li.descr3 = lbldesc3.Text;
                fpcclass.updatepcitemsdata(li);


                DataTable dtpcitems = new DataTable();
                dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                if (dtpcitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpcitemlist.Visible = true;
                    gvpcitemlist.DataSource = dtpcitems;
                    gvpcitemlist.DataBind();
                }
                else
                {
                    gvpcitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Purchase Challan Items Found.";
                }
            }
        }
    }
    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtbillqty.Text = "1";
                txtstockqty.Text = "1";
                txtrate.Text = dtdata.Rows[0]["salesrate"].ToString();
                //txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txtbasicamt.Text = Math.Round((Convert.ToDouble(txtbillqty.Text) * Convert.ToDouble(txtrate.Text)), 2).ToString();
            }
            else
            {
                txtbillqty.Text = string.Empty;
                txtrate.Text = string.Empty;
                //txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txtdescription1.Text = string.Empty;
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
            }
            li.itemname = drpitemname.SelectedItem.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) + Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
            else
            {
                lblcloseqty.Text = Math.Round(0 + (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
        }
        else
        {
            txtbillqty.Text = string.Empty;
            txtrate.Text = string.Empty;
            //txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txtdescription1.Text = string.Empty;
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            lblcloseqty.Text = Math.Round(0 + (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
        }
    }

    protected void txtgvstockqty_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblitemname = (Label)currentRow.FindControl("lblitemname");
        Label lblgvstockqtyold = (Label)currentRow.FindControl("lblgvstockqty");
        Label lblgvcloseqty = (Label)currentRow.FindControl("lblgvcloseqty");
        TextBox txtgvstockqty = (TextBox)currentRow.FindControl("txtgvstockqty");
        TextBox txtgridrate = (TextBox)currentRow.FindControl("txtgridrate");
        if (lblitemname.Text != "--SELECT--")
        {
            //
            li.itemname = lblitemname.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblgvcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) + Convert.ToDouble(txtgvstockqty.Text) - Convert.ToDouble(lblgvstockqtyold.Text)), 2).ToString();
            }
            else
            {
                lblgvcloseqty.Text = Math.Round(0 + (Convert.ToDouble(txtgvstockqty.Text) - Convert.ToDouble(lblgvstockqtyold.Text)), 2).ToString();
            }
        }
        else
        {
            lblgvcloseqty.Text = "0";
        }
        Page.SetFocus(txtgridrate);
    }

    protected void txtstockqty_TextChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            //
            li.itemname = drpitemname.SelectedItem.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as totalqty from itemmaster where itemname=@itemname and (((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))<>0", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (dtstock.Rows.Count > 0)
            {
                lblcloseqty.Text = Math.Round((Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) + Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
            else
            {
                lblcloseqty.Text = Math.Round(0 + (Convert.ToDouble(txtstockqty.Text)), 2).ToString();
            }
        }
        else
        {
            lblcloseqty.Text = "0";
        }
        //
        Page.SetFocus(txtrate);
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from PCMaster order by pcno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pcno = Convert.ToInt64(dtdataq.Rows[0]["pcno"].ToString());
            ViewState["pcno"] = li.pcno;
            DataTable dtdata = new DataTable();
            dtdata = fpcclass.selectallpurchasechallandatafrompcno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtpcno.Text = dtdata.Rows[0]["pcno"].ToString();
                txtpcdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtchallanno.Text = dtdata.Rows[0]["chno"].ToString();
                txtpono.Text = dtdata.Rows[0]["pono"].ToString();
                txtpodate.Text = Convert.ToDateTime(dtdata.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpono1.Text = dtdata.Rows[0]["pono1"].ToString();
                if (dtdata.Rows[0]["podate1"].ToString() != string.Empty)
                {
                    txtpodate1.Text = Convert.ToDateTime(dtdata.Rows[0]["podate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtsrtringpono.Text = dtdata.Rows[0]["stringpono"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                DataTable dtpcitems = new DataTable();
                dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                if (dtpcitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpcitemlist.Visible = true;
                    gvpcitemlist.DataSource = dtpcitems;
                    gvpcitemlist.DataBind();
                    lblcount.Text = dtpcitems.Rows.Count.ToString();
                }
                else
                {
                    gvpcitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Purchase Challan Items Found.";
                    lblcount.Text = "0";
                }

                btnsave.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select * from PCMaster order by pcno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pcno = Convert.ToInt64(dtdataq.Rows[dtdataq.Rows.Count - 1]["pcno"].ToString());
            ViewState["pcno"] = li.pcno;
            DataTable dtdata = new DataTable();
            dtdata = fpcclass.selectallpurchasechallandatafrompcno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtpcno.Text = dtdata.Rows[0]["pcno"].ToString();
                txtpcdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtchallanno.Text = dtdata.Rows[0]["chno"].ToString();
                txtpono.Text = dtdata.Rows[0]["pono"].ToString();
                txtpodate.Text = Convert.ToDateTime(dtdata.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpono1.Text = dtdata.Rows[0]["pono1"].ToString();
                if (dtdata.Rows[0]["podate1"].ToString() != string.Empty)
                {
                    txtpodate1.Text = Convert.ToDateTime(dtdata.Rows[0]["podate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                txtsrtringpono.Text = dtdata.Rows[0]["stringpono"].ToString();
                txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                DataTable dtpcitems = new DataTable();
                dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                if (dtpcitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvpcitemlist.Visible = true;
                    gvpcitemlist.DataSource = dtpcitems;
                    gvpcitemlist.DataBind();
                    lblcount.Text = dtpcitems.Rows.Count.ToString();
                }
                else
                {
                    gvpcitemlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Purchase Challan Items Found.";
                    lblcount.Text = "0";
                }

                btnsave.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from PCMaster order by pcno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.pcno = Convert.ToInt64(txtpcno.Text) + 1;
        for (Int64 i = li.pcno; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["pcno"].ToString()); i++)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from PCMaster where pcno=" + li.pcno + " order by pcno", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.pcno = Convert.ToInt64(dtdataq.Rows[0]["pcno"].ToString());
                ViewState["pcno"] = li.pcno;
                DataTable dtdata = new DataTable();
                dtdata = fpcclass.selectallpurchasechallandatafrompcno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtpcno.Text = dtdata.Rows[0]["pcno"].ToString();
                    txtpcdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtchallanno.Text = dtdata.Rows[0]["chno"].ToString();
                    txtpono.Text = dtdata.Rows[0]["pono"].ToString();
                    txtpodate.Text = Convert.ToDateTime(dtdata.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpono1.Text = dtdata.Rows[0]["pono1"].ToString();
                    if (dtdata.Rows[0]["podate1"].ToString() != string.Empty)
                    {
                        txtpodate1.Text = Convert.ToDateTime(dtdata.Rows[0]["podate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtsrtringpono.Text = dtdata.Rows[0]["stringpono"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpcitemlist.Visible = true;
                        gvpcitemlist.DataSource = dtpcitems;
                        gvpcitemlist.DataBind();
                        lblcount.Text = dtpcitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpcitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Purchase Challan Items Found.";
                        lblcount.Text = "0";
                    }

                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from PCMaster order by pcno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.pcno = Convert.ToInt64(txtpcno.Text) - 1;
        for (Int64 i = li.pcno; i >= Convert.ToInt64(dtdataqa.Rows[0]["pcno"].ToString()); i--)
        {
            li.pcno = i;
            SqlDataAdapter da = new SqlDataAdapter("select * from PCMaster where pcno=" + li.pcno + " order by pcno", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.pcno = Convert.ToInt64(dtdataq.Rows[0]["pcno"].ToString());
                ViewState["pcno"] = li.pcno;
                DataTable dtdata = new DataTable();
                dtdata = fpcclass.selectallpurchasechallandatafrompcno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtpcno.Text = dtdata.Rows[0]["pcno"].ToString();
                    txtpcdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtchallanno.Text = dtdata.Rows[0]["chno"].ToString();
                    txtpono.Text = dtdata.Rows[0]["pono"].ToString();
                    txtpodate.Text = Convert.ToDateTime(dtdata.Rows[0]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpono1.Text = dtdata.Rows[0]["pono1"].ToString();
                    if (dtdata.Rows[0]["podate1"].ToString() != string.Empty)
                    {
                        txtpodate1.Text = Convert.ToDateTime(dtdata.Rows[0]["podate1"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    }
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtsrtringpono.Text = dtdata.Rows[0]["stringpono"].ToString();
                    txtuser.Text = dtdata.Rows[0]["uname"].ToString();
                    DataTable dtpcitems = new DataTable();
                    dtpcitems = fpcclass.selectallpcitemsfrompcno(li);
                    if (dtpcitems.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        gvpcitemlist.Visible = true;
                        gvpcitemlist.DataSource = dtpcitems;
                        gvpcitemlist.DataBind();
                        lblcount.Text = dtpcitems.Rows.Count.ToString();
                    }
                    else
                    {
                        gvpcitemlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Purchase Challan Items Found.";
                        lblcount.Text = "0";
                    }

                    btnsave.Text = "Update";
                    return;
                }
            }
        }
    }

}