﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CIDWise.aspx.cs" Inherits="CIDWise" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "minus.png";
            } else {
                div.style.display = "none";
                img.src = "plus.png";
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">CIDWise Payment List</span><br />
        <div class="row">
            <div class="col-md-3">
                <asp:CheckBox ID="chkacnamewise"
                        runat="server" Text="ACNamewise" /><asp:CheckBox ID="chkoutstanding"
                        runat="server" Text="Outstanding Only" /><asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" AutoPostBack="true"
                    OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtclientcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn btn-default forbutton"
                    OnClick="btnsearch_Click" />
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnprint" runat="server" Text="Print" 
                    class="btn btn-default forbutton" onclick="btnprint_Click" /><asp:CheckBox ID="chkagbill"
                        runat="server" Text="A/G. Bill" /><asp:CheckBox ID="chkledgercid"
                        runat="server" Text="Ledger" />
            </div>
            <div class="col-md-2">
                Po NO. :<asp:Label ID="lblpono" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="col-md-2">
                PO Date :<asp:Label ID="lblpodate" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-3">
                Total Amount :
                <asp:Label ID="lbltotamt" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="col-md-3">
                Received Amount :
                <%--<asp:Label ID="lblpaidamt" runat="server" ForeColor="Red"></asp:Label>--%>
                <asp:LinkButton ID="lnkpaidamt" runat="server" ForeColor="Red" OnClick="lnkpaidamt_Click"></asp:LinkButton>
            </div>
            <div class="col-md-3">
                Remain Amount :
                <asp:Label ID="lblremainamt" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvaclist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDataBound="gvaclist_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("sino") %>' />--%>
                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("strvoucherno") %>');">
                                        <img id="imgdiv<%# Eval("strvoucherno") %>" width="25px" border="0" src="images/plus.png" />
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inv. NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblsino" ForeColor="Black" runat="server" Text='<%# bind("strvoucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblsidate" ForeColor="Black" runat="server" Text='<%# bind("sidate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Basic Amt." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotbasicamount" ForeColor="Black" runat="server" Text='<%# bind("totbasicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SGST" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotvat" ForeColor="Black" runat="server" Text='<%# bind("totvat") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CGST" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotaddtax" ForeColor="Black" runat="server" Text='<%# bind("totaddtax") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IGST" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotcst" ForeColor="Black" runat="server" Text='<%# bind("totcst") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bill Amt." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbillamount" ForeColor="Black" runat="server" Text='<%# bind("billamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="100%">
                                            <div id="div<%# Eval("strvoucherno") %>" style="display: none; position: relative;
                                                left: 15px; overflow: auto">
                                                <asp:GridView ID="gvChildGrid" runat="server" AutoGenerateColumns="false" BorderStyle="None"
                                                    AllowSorting="false" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered"
                                                    Width="80%">
                                                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                    <RowStyle BackColor="#E1E1E1" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Inv. NO." SortExpression="Csnm">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsinochild" ForeColor="Black" runat="server" Text='<%# bind("strvoucherno") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="voucherno" HeaderText="Voucher NO." HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="strvoucherno" HeaderText="Inv. No." HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="voucherdate" HeaderText="Date" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="istype" HeaderText="Type" HeaderStyle-HorizontalAlign="Left" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDataBound="gvaclist_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Inv. NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblsino" ForeColor="Black" runat="server" Text='<%# bind("strvoucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherdate" ForeColor="Black" runat="server" Text='<%# bind("voucherdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Basic Amt." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SGST" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblistype" ForeColor="Black" runat="server" Text='<%# bind("istype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
