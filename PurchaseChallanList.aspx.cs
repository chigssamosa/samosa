﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PurchaseChallanList : System.Web.UI.Page
{
    ForPurchaseChallan fpcclass = new ForPurchaseChallan();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpcclass.selectallpurchasechallandata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpurchasechallanlist.Visible = true;
            gvpurchasechallanlist.DataSource = dtdata;
            gvpurchasechallanlist.DataBind();
        }
        else
        {
            gvpurchasechallanlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Purchase Challan Data Found.";
        }
    }

    protected void gvpurchasechallanlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.pcno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/PurchaseChallan.aspx?mode=update&pcno=" + li.pcno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.pcno = Convert.ToInt64(e.CommandArgument);
                li.stringpcno = e.CommandArgument.ToString();
                DataTable dtcheck = new DataTable();
                dtcheck = fpcclass.selectbillinvoicedatafrompcno(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Challan Used in Purchase Invoice No. " + dtcheck.Rows[0]["strpino"].ToString() + " So You Cant delete this Challan.First delete that invoice then try to delete this challan.');", true);
                    return;
                }
                else
                {
                    DataTable dtdata1 = new DataTable();
                    dtdata1 = fpcclass.selectallpurchasechallandatafrompcno(li);
                    li.scno = 0;
                    li.scno1 = 0;
                    if (dtdata1.Rows[0]["pono"].ToString() != "0" && dtdata1.Rows[0]["pono"].ToString() != "")
                    {
                        li.strpono = dtdata1.Rows[0]["pono"].ToString();
                    }
                    if (dtdata1.Rows[0]["pono1"].ToString() != "0" && dtdata1.Rows[0]["pono1"].ToString() != "")
                    {
                        li.strpono1 = dtdata1.Rows[0]["pono1"].ToString();
                    }
                    //li.scno = Convert.ToInt64(dtdata1.Rows[0]["scno"].ToString());
                    //li.vnono=
                    DataTable dtdata = new DataTable();
                    dtdata = fpcclass.selectallpcitemsfrompcno(li);
                    for (int c = 0; c < dtdata.Rows.Count; c++)
                    {
                        li.vid = Convert.ToInt64(dtdata.Rows[c]["vid"].ToString());
                        li.id = Convert.ToInt64(dtdata.Rows[c]["id"].ToString());
                        li.itemname = dtdata.Rows[c]["itemname"].ToString();
                        li.qty = Convert.ToDouble(dtdata.Rows[c]["qty"].ToString());
                        if (li.strpono != "0" && li.strpono != null && li.strpono != string.Empty)
                        {
                            if (li.vid != 0)
                            {
                                DataTable dtqty = new DataTable();
                                dtqty = fpcclass.selectqtyremainusedfromscno1(li);
                                if (dtqty.Rows.Count > 0)
                                {
                                    li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                    li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                    li.vnono = 0;
                                    fpcclass.updateqtyduringdelete(li);
                                }
                            }
                        }
                        if (li.strpono1 != "0" && li.strpono1 != null && li.strpono1 != string.Empty)
                        {
                            if (li.vid != 0)
                            {
                                //li.scno = li.scno1;
                                DataTable dtqty = new DataTable();
                                dtqty = fpcclass.selectqtyremainusedfromscno11(li);
                                if (dtqty.Rows.Count > 0)
                                {
                                    li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                    li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                    li.vnono = 0;
                                    fpcclass.updateqtyduringdelete(li);
                                }
                            }
                        }
                    }
                    if (dtdata1.Rows.Count > 0)
                    {
                        li.pcno = 0;
                        li.stringpono = dtdata1.Rows[0]["stringpono"].ToString();
                        string[] words = li.stringpono.Split(',');
                        foreach (string word in words)
                        {
                            li.strpono = word.Replace("'", "").ToString();
                            fpcclass.updatepcnoinpomaster(li);
                            fpcclass.updatepcnoinpomastera(li);
                        }
                    }
                    fpcclass.deletepcmasterdata(li);
                    fpcclass.deletepcitemsdata(li);
                    fpcclass.updateisusedn(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.pcno + " Purchase Challan Deleted.";
                    faclass.insertactivity(li);
                    fillgrid();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvpurchasechallanlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnpurchasechallan_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/PurchaseChallan.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
}