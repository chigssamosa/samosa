﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Inquiry.aspx.cs" Inherits="Inquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Inquiry Master</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Code</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Address 1<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtaddress1" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Address 2</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtaddress2" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Address 3</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtaddress3" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            City</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Email ID</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtemailid" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Mobile NO.<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtmobileno" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            REF.Name</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtrefname" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Handled By</label></div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="drphandledby" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            P.O.No.</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpono" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Description</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control" Width="200px"
                            Height="40px" TextMode="MultiLine" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Pincode</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row" style="height:10px"></div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-2">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valclient" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valclient" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter name."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter address 1."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtaddress1"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter mobile no."
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtmobileno"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter Proper Email ID. e.g. ( test@test.com )"
                            Text="*" ValidationGroup="valclient" ControlToValidate="txtemailid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <script type="text/javascript">
          Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
          function getme() {
              $("#<%= txtdate.ClientID %>").datepicker({
                  showmonth: true,
                  autoSize: true,
                  showAnim: 'slideDown',
                  duration: 'fast',
                  dateFormat: "dd-mm-yy"
              });


          }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
