﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ToolsDeliveryEntryList : System.Web.UI.Page
{
    ForToolDelMaster ftdmclass = new ForToolDelMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = ftdmclass.selectalltooldeldata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvtoolsdeliveryentrylist.Visible = true;
            gvtoolsdeliveryentrylist.DataSource = dtdata;
            gvtoolsdeliveryentrylist.DataBind();
        }
        else
        {
            gvtoolsdeliveryentrylist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Tool Deliery Data Found.";
        }
    }

    protected void btntoolsdeliveryentry_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("ToolsDeliveryEntry.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvtoolsdeliveryentrylist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("ToolsDeliveryEntry.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                ftdmclass.deletetooldeldata(li);
                ftdmclass.deletetooldelitemdatafromvno(li);
                ftdmclass.updateisusedn(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Tools Delivery Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            string Xrepname = "Tool Del challan";
            Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?voucherno=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    protected void gvtoolsdeliveryentrylist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvtoolsdeliveryentrylist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //var gv = (GridView)e.Row.FindControl("gvemember");
            var lblvoucherno = (Label)e.Row.FindControl("lblsigned");
            var chkbox = (CheckBox)e.Row.FindControl("chksigned");
            var txtremarks = (TextBox)e.Row.FindControl("txtremarks");
            if (lblvoucherno.Text == "Yes")
            {
                chkbox.Checked = true;
            }
            if (Request.Cookies["ForLogin"]["username"] != "PSP")
            {
                chkbox.Visible = false;
                txtremarks.Visible = false;
            }
        }
    }

    protected void chksigned_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chksigned = (CheckBox)currentRow1.FindControl("chksigned");
        Label lblstrscno = (Label)currentRow1.FindControl("lblvoucherno");
        string signedd = "No";
        if (chksigned.Checked == true)
        {
            signedd = "Yes";
        }
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand da = new SqlCommand("update ToolDelMaster set issigned='" + signedd + "' where voucherno='" + lblstrscno.Text + "'", con);
        con.Open();
        da.ExecuteNonQuery();
        con.Close();
        fillgrid();
        CheckBox chksigned1 = (CheckBox)gvtoolsdeliveryentrylist.Rows[currentRow1.RowIndex + 1].FindControl("chksigned");
        Page.SetFocus(chksigned1);
    }
    protected void txtremarks_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtremarks = (TextBox)currentRow1.FindControl("txtremarks");
        Label lblstrscno = (Label)currentRow1.FindControl("lblvoucherno");
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand da = new SqlCommand("update ToolDelMaster set signremarks='" + txtremarks.Text + "' where voucherno='" + lblstrscno.Text + "'", con);
        con.Open();
        da.ExecuteNonQuery();
        con.Close();
        fillgrid();
        TextBox txtremarks1 = (TextBox)gvtoolsdeliveryentrylist.Rows[currentRow1.RowIndex + 1].FindControl("txtremarks");
        Page.SetFocus(txtremarks1);
    }

}