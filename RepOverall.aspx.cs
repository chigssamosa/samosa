﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class RepOverall : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    //protected void txtccode_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtacname.Text.Trim() != string.Empty)
    //    {
    //        string cc = txtacname.Text;
    //        //txtclientcode.Text = cc.Split('-')[1];
    //        txtacname.Text = cc.Split('-')[0];
    //        //SessionMgt.FirstName = cc.Split('-')[1];
    //    }
    //    else
    //    {
    //        //txtname.Text = string.Empty;
    //        txtacname.Text = string.Empty;
    //        //SessionMgt.FirstName = string.Empty;
    //    }
    //    Page.SetFocus(txtfromdate);
    //}

    protected void btnpreview_Click(object sender, EventArgs e)
    {
        //string Xrepname = "Sales Outstanding Report";
        //string Xrepname = "Overall Report with no";
        //string Xrepname = "Overall Report";
        string Xrepname = drptype.SelectedValue;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        if (txtacname.Text.IndexOf("-") > 0)
        {
            li.acname = txtacname.Text.Split('-')[0];
            li.name = txtacname.Text.Split('-')[1];
            //Int64 vno = Convert.ToInt64(txtvono.Text);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?acname=" + li.acname + "&cname=" + li.name + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&rtype=" + RadioButtonList1.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }

}