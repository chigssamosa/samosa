﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Data.SqlTypes;

public partial class GSTB2B : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillgridb2b();
            //ExportGridToExcel();
        }
    }

    //public void fillgridb2b()
    //{
    //    //li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    //li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    //Session["dtpitems"] = CreateTemplate();
    //    //DataTable dtq = new DataTable();
    //    //string cz = Request.Cookies["Forcon"]["conc"];
    //    //cz = cz.Replace(":", ";");
    //    //SqlConnection con = new SqlConnection(cz);
    //    //SqlDataAdapter da = new SqlDataAdapter("select strsino,sidate,SUM(basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+sum(SIItems.cstamt)) as totbillamt,taxdesc from SIItems where sidate between @fromdate and @todate group by taxdesc,strsino,sidate,sino order by sino", con);
    //    //da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
    //    //da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
    //    //DataTable dtdata = new DataTable();
    //    //da.Fill(dtdata);
    //    //if (dtdata.Rows.Count > 0)
    //    //{
    //    //    for (int c = 0; c < dtdata.Rows.Count; c++)
    //    //    {
    //    //        dtq = (DataTable)Session["dtpitems"];
    //    //        DataRow dr1 = dtq.NewRow();
    //    //        dr1["strsino"] = dtdata.Rows[c]["strsino"].ToString();
    //    //        dr1["sidate"] = Convert.ToDateTime(dtdata.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
    //    //        dr1["totbasicamount"] = dtdata.Rows[c]["totbasic"].ToString();

    //    //        var cc = dtdata.Rows[c]["taxdesc"].ToString().IndexOf("-");
    //    //        if (cc != -1)
    //    //        {
    //    //            double vatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[0]);
    //    //            double addvatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[1]);
    //    //            dr1["taxdesc"] = (vatp + addvatp).ToString();
    //    //        }
    //    //        else
    //    //        {
    //    //            dr1["taxdesc"] = dtdata.Rows[c]["taxdesc"].ToString();
    //    //        }

    //    //        li.strsino = dtdata.Rows[c]["strsino"].ToString();
    //    //        SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
    //    //        DataTable dtsi = new DataTable();
    //    //        dasi.Fill(dtsi);
    //    //        dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
    //    //        SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and ACMaster.gstno!=''", con);
    //    //        DataTable dta = new DataTable();
    //    //        daa.Fill(dta);
    //    //        if (dta.Rows.Count > 0)
    //    //        {
    //    //            dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
    //    //            if (dta.Rows[0]["gstno"].ToString() != "")
    //    //            {
    //    //                dr1["rcm"] = "N";
    //    //            }
    //    //            else
    //    //            {
    //    //                dr1["rcm"] = "Y";
    //    //            }
    //    //            dr1["city"] = dta.Rows[0]["city"].ToString();
    //    //        }
    //    //        else
    //    //        {
    //    //            dr1["gstno"] = "";
    //    //            dr1["city"] = "";
    //    //            dr1["rcm"] = "";
    //    //        }
    //    //        if (dta.Rows.Count > 0)
    //    //        {
    //    //            dtq.Rows.Add(dr1);
    //    //        }
    //    //    }
    //    //    Session["dtpitems"] = dtq;
    //    //    gvaclist.DataSource = dtq;
    //    //    gvaclist.DataBind();
    //    //}
    //}



    private void ExportGridToExcel()
    {
        //string path = string.Concat(Server.MapPath("~/GST Return/a4.xlsx"));
        string dd = "a7.xlsx";
        if (!File.Exists(Server.MapPath(dd)))
        {
            //
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
            //MessageBox.Show("Excel is not properly installed!!");
            return;
            //File.Delete(Server.MapPath("~/GST Return/a4.xlsx"));
        }
        //else
        //{
        //    File.Delete(@"d:\\" + dd + "");
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
        //    return;
        //}


        //Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
        Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

        if (xlApp == null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not properly installed!!.');", true);
            //MessageBox.Show("Excel is not properly installed!!");
            return;
        }

        xlApp.DisplayAlerts = false;
        //string filePath = @"d:\\a4.xlsx";
        string filePath = Server.MapPath(dd);
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        Microsoft.Office.Interop.Excel.Sheets worksheets = xlWorkBook.Worksheets;
        
        //1 sheet
        var xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)worksheets.Add(worksheets[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet.Name = "Chigs11111";
        xlNewSheet.Cells[1, 1] = "Chirag";
        xlNewSheet.Cells[1, 2] = "Chirag 1";
        xlNewSheet.Cells[1, 3] = "Chirag 2";
        xlNewSheet.Cells[2, 1] = "Chirag";
        xlNewSheet.Cells[2, 2] = "Chirag 1";
        xlNewSheet.Cells[2, 3] = "Chirag 2";

        xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        xlNewSheet.Select();

        //2 Sheet
        Microsoft.Office.Interop.Excel.Sheets worksheets1 = xlWorkBook.Worksheets;
        var xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet1.Name = "P22222";
        xlNewSheet1.Cells[1, 1] = "P";
        xlNewSheet1.Cells[1, 2] = "P 1";
        xlNewSheet1.Cells[1, 3] = "P 2";
        xlNewSheet1.Cells[2, 1] = "P";
        xlNewSheet1.Cells[2, 2] = "P 1";
        xlNewSheet1.Cells[2, 3] = "P 2";

        //        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
        xlNewSheet1.Select();

        //3 Sheet
        Microsoft.Office.Interop.Excel.Sheets worksheets2 = xlWorkBook.Worksheets;
        var xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
        xlNewSheet2.Name = "Q22222";
        xlNewSheet2.Cells[1, 1] = "Q";
        xlNewSheet2.Cells[1, 2] = "Q 1";
        xlNewSheet2.Cells[1, 3] = "Q 2";
        xlNewSheet2.Cells[2, 1] = "Q";
        xlNewSheet2.Cells[2, 2] = "Q 1";
        xlNewSheet2.Cells[2, 3] = "Q 2";

        //        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
        xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
        xlNewSheet2.Select();

        xlApp.DisplayAlerts = false;
        for (int i = xlApp.ActiveWorkbook.Worksheets.Count; i > 0; i--)
        {
            Worksheet wkSheet = (Worksheet)xlApp.ActiveWorkbook.Worksheets[i];
            if (wkSheet.Name == "Sheet1")
            {
                wkSheet.Delete();
            }
           else if (wkSheet.Name == "Sheet2")
            {
                wkSheet.Delete();
            }
            else if (wkSheet.Name == "Sheet3")
            {
                wkSheet.Delete();
            }
            else
            { }
        }
        xlApp.DisplayAlerts = true;
        
        xlWorkBook.Save();
        xlWorkBook.Close();

        releaseObject(xlNewSheet);
        releaseObject(worksheets);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

       

        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('New Worksheet Created!');", true);
    }
        //Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
        //DataTable dtq = (DataTable)Session["dtpitems"];
        //if (gvaclist.Rows.Count > 0)
        //{
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.Charset = "";
        //    string FileName = "GSTB2B" + DateTime.Now + ".xls";
        //    StringWriter strwritter = new StringWriter();
        //    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentType = "application/vnd.ms-excel";
        //    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        //    gvaclist.GridLines = GridLines.Both;
        //    gvaclist.HeaderStyle.Font.Bold = true;
        //    gvaclist.RenderControl(htmltextwrtter);
        //    Response.Write(strwritter.ToString());
        //    Response.End();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No B2B data available for export.');", true);
        //    return;
        //}




    //    //StringBuilder sb = new StringBuilder();
    //    //string path = Path.Combine(Server.MapPath("~/GST Return"), "XYS.xlsx");
    //    //string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0 xml;HDR=Yes", path);
    //}



    //public DataTable CreateTemplate()
    //{
    //    DataTable dtpitems = new DataTable();
    //    dtpitems.Columns.Add("gstno", typeof(string));
    //    dtpitems.Columns.Add("strsino", typeof(string));
    //    dtpitems.Columns.Add("sidate", typeof(string));
    //    dtpitems.Columns.Add("billamount", typeof(double));
    //    dtpitems.Columns.Add("city", typeof(string));
    //    dtpitems.Columns.Add("taxdesc", typeof(string));
    //    dtpitems.Columns.Add("totbasicamount", typeof(double));
    //    dtpitems.Columns.Add("rcm", typeof(string));

    //    //dtpitems.Columns.Add("gstno", typeof(string));
    //    //dtpitems.Columns.Add("strsino", typeof(DateTime));
    //    //dtpitems.Columns.Add("sidate", typeof(string));
    //    //dtpitems.Columns.Add("billamount", typeof(double));
    //    //dtpitems.Columns.Add("city", typeof(double));
    //    //dtpitems.Columns.Add("taxdesc", typeof(double));
    //    //dtpitems.Columns.Add("totbasicamount", typeof(string));
    //    return dtpitems;
    //}

    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    //
    //}

    //protected void btngstb2b_Click(object sender, EventArgs e)
    //{
    //    if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
    //    {
    //        fillgridb2b();
    //        int a = 0;
    //        string log = "";
    //        for (int i = 0; i < 1; i++)
    //        {
    //            Label lblgstno = (Label)gvaclist.Rows[i].FindControl("lblgstno");
    //            //Label lblgstno = new Label();
    //            //lblgstno.Text = "24ACKPS9493D1Z";
    //            log = lblgstno.Text + "--" + System.DateTime.Now;
    //            Int64 aa = lblgstno.Text.Length;
    //            if (aa > 0)
    //            {
    //                if (aa != 15)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "GST Number must contain 15 character." + "--";
    //                }
    //                string two = lblgstno.Text.Substring(0, 2);
    //                var isValidNumber = Regex.IsMatch(two, @"^[0-9]+(\.[0-9]+)?$");
    //                if (isValidNumber == false)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "First Two Letter of GST Number must be Numeric." + "--";
    //                }

    //                int two5 = Convert.ToInt16(lblgstno.Text.Substring(0, 2));
    //                if (two5 > 37 || two5 < 1)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "First Two Letter of GST Number must be State Code." + "--";
    //                }

    //                string two1 = lblgstno.Text.Substring(2, 10);
    //                //var isValidNumber1 = Regex.IsMatch(two1, @"^[0-9]+(\.[0-9]+)?$");
    //                if (two1 != lblgstno.Text)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "Third to Twelve Letter of GST Number must be PAN No.Check PAN No Here." + "--";
    //                }
    //                string two2 = lblgstno.Text.Substring(12, 1);
    //                var isValidNumber1 = Regex.IsMatch(two2, @"^[0-9]+(\.[0-9]+)?$");
    //                if (isValidNumber1 == false)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "Thiteenth Letter of GST Number must be Numeric." + "--";
    //                }
    //                string two4 = lblgstno.Text.Substring(13, 1);
    //                var isValidNumber3 = Regex.IsMatch(two4, @"^[A-Za-z]$");
    //                if (isValidNumber3 == false)
    //                {
    //                    a = 1;
    //                    log = log + "\r\n--" + "Fourteenth Letter of GST Number must be Alphabetic." + "--";
    //                }
    //                //string two3 = txtgstno.Text.Substring(14, 1);
    //                //var isValidNumber2 = Regex.IsMatch(two3, @"^[0-9]+(\.[0-9]+)?$");
    //                //if (isValidNumber2 == false)
    //                //{
    //                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fifteenth Letter of GST Number must be Numeric.');", true);
    //                //    return;
    //                //}
    //            }
    //        }
    //        log = log + "\r\n--" + "Error In Insert Activity for assignment master--ForActivity--insertdata--";
    //        //StreamReader
    //        if (a == 1)
    //        {
    //            File.WriteAllText(HttpContext.Current.Server.MapPath("~/Errors/LogFile.txt"), "");
    //            File.AppendAllText(Server.MapPath("~/Errors/LogFile.txt"), log);
    //            string path = @"D:\Errors\LogFile.txt";
    //            using (var file = new StreamWriter(path, false))
    //            {
    //                string strURL = Server.MapPath(@"~/Errors/LogFile.txt");
    //                WebClient req = new WebClient();
    //                HttpResponse response = HttpContext.Current.Response;
    //                response.Clear();
    //                response.ClearContent();
    //                response.ClearHeaders();
    //                response.Buffer = true;
    //                response.AddHeader("Content-Disposition", "attachment;filename=\"LogFile.txt\"");
    //                byte[] data = req.DownloadData(strURL);
    //                response.BinaryWrite(data);
    //                response.End();
    //            }
    //            return;
    //        }
    //        ExportGridToExcel();
    //    }
    //}

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Exception Occured while releasing object " + ex.ToString() + "');", true);
            //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    //public void fillgridb2b()
    //{
    //    li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    Session["dtpitems"] = CreateTemplate();
    //    DataTable dtq = new DataTable();
    //    string cz = Request.Cookies["Forcon"]["conc"];
    //    cz = cz.Replace(":", ";");
    //    SqlConnection con = new SqlConnection(cz);
    //    SqlDataAdapter da = new SqlDataAdapter("select SIItems.strsino,SIItems.sidate,SUM(SIItems.basicamount) as totbasic,(CASE WHEN (ACMaster.excludetax<>'Yes') THEN (sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+sum(SIItems.cstamt)) ELSE SUM(SIItems.basicamount) END) as totbillamt,SIItems.taxdesc from SIItems inner join SIMaster on SIMaster.strsino=SIItems.strsino inner join ACMaster on SIMaster.salesac=ACMaster.acname where (ACMaster.inexport is null or ACMaster.inexport='' or ACMaster.inexport='No') and SIItems.sidate between @fromdate and @todate group by ACMaster.excludetax,SIItems.taxdesc,SIItems.strsino,SIItems.sidate,SIItems.sino order by SIItems.sino", con);
    //    da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
    //    da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
    //    DataTable dtdata = new DataTable();
    //    da.Fill(dtdata);
    //    if (dtdata.Rows.Count > 0)
    //    {
    //        for (int c = 0; c < dtdata.Rows.Count; c++)
    //        {
    //            dtq = (DataTable)Session["dtpitems"];
    //            DataRow dr1 = dtq.NewRow();
    //            dr1["strsino"] = dtdata.Rows[c]["strsino"].ToString();
    //            dr1["sidate"] = Convert.ToDateTime(dtdata.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
    //            dr1["totbasicamount"] = dtdata.Rows[c]["totbasic"].ToString();

    //            var cc = dtdata.Rows[c]["taxdesc"].ToString().IndexOf("-");
    //            if (cc != -1)
    //            {
    //                double vatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[0]);
    //                double addvatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[1]);
    //                dr1["taxdesc"] = (vatp + addvatp).ToString();
    //            }
    //            else
    //            {
    //                dr1["taxdesc"] = dtdata.Rows[c]["taxdesc"].ToString();
    //            }

    //            li.strsino = dtdata.Rows[c]["strsino"].ToString();
    //            SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
    //            DataTable dtsi = new DataTable();
    //            dasi.Fill(dtsi);
    //            dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
    //            dr1["acname"] = dtsi.Rows[0]["acname"].ToString();
    //            SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "'", con);//ACMaster.gstno!=''// and  (ACMaster.gstno is not null or ACMaster.gstno<>'')
    //            DataTable dta = new DataTable();
    //            daa.Fill(dta);
    //            if (dta.Rows.Count > 0)
    //            {
    //                dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
    //                if (dta.Rows[0]["gstno"].ToString() != "")
    //                {
    //                    dr1["rcm"] = "N";
    //                }
    //                else
    //                {
    //                    dr1["rcm"] = "Y";
    //                }
    //                string statecode=dta.Rows[0]["gstno"].ToString().Substring(0,2);
    //                if (statecode == "01") { dr1["city"] = "01-Jammu & Kashmir"; }
    //                else if (statecode == "02") { dr1["city"] = "02-Himachal Pradesh"; }
    //                else if (statecode == "03") {dr1["city"] ="03-Punjab" ;}
    //                else if (statecode == "04") {dr1["city"] = "04-Chandigarh";}
    //                else if (statecode == "05") {dr1["city"] = "05-Uttarakhand";}
    //                else if (statecode == "06") {dr1["city"] = "06-Haryana";}
    //                else if (statecode == "07") {dr1["city"] = "07-Delhi";}
    //                else if (statecode == "08") {dr1["city"] = "08-Rajasthan";}
    //                else if (statecode == "09") {dr1["city"] = "09-Uttar Pradesh";}
    //                else if (statecode == "10") {dr1["city"] = "10-Bihar";}
    //                else if (statecode == "11") {dr1["city"] = "11-Sikkim";}
    //                else if (statecode == "12") {dr1["city"] = "12-Arunachal Pradesh";}
    //                else if (statecode == "13") {dr1["city"] = "13-Nagaland";}
    //                else if (statecode == "14") {dr1["city"] = "14-Manipur";}
    //                else if (statecode == "15") {dr1["city"] = "15-Mizoram";}
    //                else if (statecode == "16") {dr1["city"] = "16-Tripura";}
    //                else if (statecode == "17") {dr1["city"] = "17-Meghalaya";}
    //                else if (statecode == "18") {dr1["city"] = "18-Assam";}
    //                else if (statecode == "19") {dr1["city"] = "19-West Bengal";}
    //                else if (statecode == "20") {dr1["city"] = "20-Jharkhand";}
    //                else if (statecode == "21") {dr1["city"] = "21-Odisha";}
    //                else if (statecode == "22") {dr1["city"] = "22-Chhattisgarh";}
    //                else if (statecode == "23") {dr1["city"] = "23-Madhya Pradesh";}
    //                else if (statecode == "24") { dr1["city"] = "24-Gujarat"; }
    //                else if (statecode == "25") { dr1["city"] = "25-Daman & Diu";}
    //                else if (statecode == "26") {dr1["city"] = "26-Dadra & Nagar Haveli"; }
    //                else if (statecode == "27") {dr1["city"] = "27-Maharashtra"; }
    //                else if (statecode == "28") {dr1["city"] = ""; }
    //                else if (statecode == "29") {dr1["city"] = "29-Karnataka"; }
    //                else if (statecode == "30") {dr1["city"] = "30-Goa"; }
    //                else if (statecode == "31") {dr1["city"] = "31-Lakshdweep"; }
    //                else if (statecode == "32") { dr1["city"] = "32-Kerala";}
    //                else if (statecode == "33") { dr1["city"] = "33-Tamil Nadu";}
    //                else if (statecode == "34") { dr1["city"] = "34-Puducherry";}
    //                else if (statecode == "35") {dr1["city"] = "35-Andaman & Nicobar Islands"; }
    //                else if (statecode == "36") {dr1["city"] = "36-Telangana"; }
    //                else if (statecode == "37") {dr1["city"] = "37-Andhra Pradesh"; }
    //                else if (statecode == "97") {dr1["city"] = "97-Other Territory"; }
    //                //dr1["city"] = dta.Rows[0]["city"].ToString();
    //            }
    //            else
    //            {
    //                dr1["gstno"] = "";
    //                dr1["city"] = "";
    //                dr1["rcm"] = "";
    //            }
    //            if (dta.Rows.Count > 0)
    //            {
    //                dtq.Rows.Add(dr1);
    //            }
    //        }
    //        Session["dtpitems"] = dtq;
    //        gvaclist.DataSource = dtq;
    //        gvaclist.DataBind();
    //    }
    //}



    //private void ExportGridToExcel()
    //{
    //    DataTable dtq = (DataTable)Session["dtpitems"];
    //    if (dtq.Rows.Count > 0)
    //    {
    //        Response.Clear();
    //        Response.Buffer = true;
    //        Response.ClearContent();
    //        Response.ClearHeaders();
    //        Response.Charset = "";
    //        string FileName = "GSTB2B" + DateTime.Now + ".xls";
    //        StringWriter strwritter = new StringWriter();
    //        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //        Response.ContentType = "application/vnd.ms-excel";
    //        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
    //        gvaclist.GridLines = GridLines.Both;
    //        gvaclist.HeaderStyle.Font.Bold = true;
    //        gvaclist.RenderControl(htmltextwrtter);
    //        Response.Write(strwritter.ToString());
    //        Response.End();
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No B2B data available for export.');", true);
    //        return;
    //    }
    //}



    //public DataTable CreateTemplate()
    //{
    //    DataTable dtpitems = new DataTable();
    //    dtpitems.Columns.Add("gstno", typeof(string));
    //    dtpitems.Columns.Add("strsino", typeof(string));
    //    dtpitems.Columns.Add("acname", typeof(string));
    //    dtpitems.Columns.Add("sidate", typeof(string));
    //    dtpitems.Columns.Add("billamount", typeof(double));
    //    dtpitems.Columns.Add("city", typeof(string));
    //    dtpitems.Columns.Add("taxdesc", typeof(string));
    //    dtpitems.Columns.Add("totbasicamount", typeof(double));
    //    dtpitems.Columns.Add("rcm", typeof(string));

    //    //dtpitems.Columns.Add("gstno", typeof(string));
    //    //dtpitems.Columns.Add("strsino", typeof(DateTime));
    //    //dtpitems.Columns.Add("sidate", typeof(string));
    //    //dtpitems.Columns.Add("billamount", typeof(double));
    //    //dtpitems.Columns.Add("city", typeof(double));
    //    //dtpitems.Columns.Add("taxdesc", typeof(double));
    //    //dtpitems.Columns.Add("totbasicamount", typeof(string));
    //    return dtpitems;
    //}

    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }

    protected void btngstb2b_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (FileUpload1.HasFile)
        {
            try
            {
                string filename = Path.GetFileName(FileUpload1.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/FileUpload/") + filename);
                //string dd = "a7.xlsx";
                if (!File.Exists(Server.MapPath("~/FileUpload/" + filename)))
                {
                    //
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
                    //MessageBox.Show("Excel is not properly installed!!");
                    return;
                    //File.Delete(Server.MapPath("~/GST Return/a4.xlsx"));
                }
                //else
                //{
                //    File.Delete(@"d:\\" + dd + "");
                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
                //    return;
                //}


                //Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

                if (xlApp == null)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not properly installed!!.');", true);
                    //MessageBox.Show("Excel is not properly installed!!");
                    return;
                }

                xlApp.DisplayAlerts = false;
                //string filePath = @"d:\\a4.xlsx";
                string filePath = Server.MapPath("~/FileUpload/" + filename);
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                Microsoft.Office.Interop.Excel.Sheets worksheets = xlWorkBook.Worksheets;

                //1 sheet
                var xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)worksheets.Add(worksheets[1], Type.Missing, Type.Missing, Type.Missing);
                xlNewSheet.Name = "Chigs11111";
                xlNewSheet.Cells[1, 1] = "Chirag";
                xlNewSheet.Cells[1, 2] = "Chirag 1";
                xlNewSheet.Cells[1, 3] = "Chirag 2";
                xlNewSheet.Cells[2, 1] = "Chirag";
                xlNewSheet.Cells[2, 2] = "Chirag 1";
                xlNewSheet.Cells[2, 3] = "Chirag 2";

                xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlNewSheet.Select();

                //2 Sheet
                Microsoft.Office.Interop.Excel.Sheets worksheets1 = xlWorkBook.Worksheets;
                var xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
                xlNewSheet1.Name = "P22222";
                xlNewSheet1.Cells[1, 1] = "P";
                xlNewSheet1.Cells[1, 2] = "P 1";
                xlNewSheet1.Cells[1, 3] = "P 2";
                xlNewSheet1.Cells[2, 1] = "P";
                xlNewSheet1.Cells[2, 2] = "P 1";
                xlNewSheet1.Cells[2, 3] = "P 2";

                //        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
                xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
                xlNewSheet1.Select();

                //3 Sheet
                Microsoft.Office.Interop.Excel.Sheets worksheets2 = xlWorkBook.Worksheets;
                var xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
                xlNewSheet2.Name = "Q22222";
                xlNewSheet2.Cells[1, 1] = "Q";
                xlNewSheet2.Cells[1, 2] = "Q 1";
                xlNewSheet2.Cells[1, 3] = "Q 2";
                xlNewSheet2.Cells[2, 1] = "Q";
                xlNewSheet2.Cells[2, 2] = "Q 1";
                xlNewSheet2.Cells[2, 3] = "Q 2";

                //        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
                xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
                xlNewSheet2.Select();

                xlApp.DisplayAlerts = false;
                for (int i = xlApp.ActiveWorkbook.Worksheets.Count; i > 0; i--)
                {
                    Worksheet wkSheet = (Worksheet)xlApp.ActiveWorkbook.Worksheets[i];
                    if (wkSheet.Name == "Sheet1")
                    {
                        wkSheet.Delete();
                    }
                    else if (wkSheet.Name == "Sheet2")
                    {
                        wkSheet.Delete();
                    }
                    else if (wkSheet.Name == "Sheet3")
                    {
                        wkSheet.Delete();
                    }
                    else
                    { }
                }
                xlApp.DisplayAlerts = true;

                xlWorkBook.Save();
                xlWorkBook.Close();

                releaseObject(xlNewSheet);
                releaseObject(worksheets);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
                System.Diagnostics.Process.Start("explorer.exe", Server.MapPath("~/FileUpload"));

                //string path = Server.MapPath("~/FileUpload/" + filename);
                //using (var file = new StreamWriter(path, false))
                //{
                //    string strURL = Server.MapPath(@"~/FileUpload/" + filename);
                //    WebClient req = new WebClient();
                //    HttpResponse response = HttpContext.Current.Response;
                //    response.Clear();
                //    response.ClearContent();
                //    response.ClearHeaders();
                //    response.Buffer = true;
                //    response.AddHeader("Content-Disposition", "attachment;filename=" + filename.Split('.')[0]+".xls");
                //    byte[] data = req.DownloadData(strURL);
                //    response.BinaryWrite(data);
                //    response.End();
                //}
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('New Worksheet Created!');", true);
            }
            catch (Exception ex)
            {
                //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Upload excel file for GST Return.');", true);
            return;
        }        
    }

}