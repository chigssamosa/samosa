﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserRights.aspx.cs" Inherits="UserRights" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">User Rights</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Role</label></div>
            <div class="col-md-4">
                <asp:DropDownList ID="drprole" runat="server" CssClass="form-control" Width="250px"
                    AutoPostBack="True" OnSelectedIndexChanged="drprole_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row table-responsive">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="100%" Height="400px">
                <asp:GridView ID="gvRightsDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="PageName" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblid2" runat="server" Text='<%#Eval("id") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblid1" runat="server" Text='<%#Eval("page") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Page Name">
                            <ItemTemplate>
                                <asp:Label ID="lblpagename" runat="server" Text='<%#Eval("pagename") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add">
                            <HeaderTemplate>
                                Add<asp:CheckBox ID="chkalladd" runat="server" AutoPostBack="true" OnCheckedChanged="chkalladd_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkadd" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Update">
                            <HeaderTemplate>
                                Update<asp:CheckBox ID="chkallupdate" runat="server" AutoPostBack="true" OnCheckedChanged="chkallupdate_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkupdate" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <HeaderTemplate>
                                Delete<asp:CheckBox ID="chkalldelete" runat="server" AutoPostBack="true" OnCheckedChanged="chkalldelete_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkdelete" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Print" Visible="False">
                                                        <HeaderTemplate>
                                                            Print<asp:CheckBox ID="chkallprint" runat="server" AutoPostBack="true" OnCheckedChanged="chkallprint_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkprint" runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="View">
                            <HeaderTemplate>
                                View<asp:CheckBox ID="chkallview" runat="server" AutoPostBack="true" OnCheckedChanged="chkallview_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkview" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#4c4c4c" />
                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnsave" runat="server" Text="Save" class="btn btn-default forbutton"
                    OnClick="btnsave_Click" /></div>
        </div>
    </div>
</asp:Content>
