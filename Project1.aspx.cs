﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;

public partial class Project1 : System.Web.UI.Page
{
    ForProject fpclass = new ForProject();
    LogicLayer li = new LogicLayer();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillgrid();
            Session["dtpitemsproj1"] = CreateTemplate();
        }
    }

    public void fillgrid()
    {
        li.ccode = Convert.ToInt64(txtclientcode.Text);
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpclass.selectdatafromccode(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvproject1.Visible = true;
            gvproject1.DataSource = dtdata;
            gvproject1.DataBind();
        }
        else
        {
            Session["dtpitemsproj1"] = CreateTemplate();
            DataTable dt = (DataTable)Session["dtpitemsproj1"];
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            lblempty.Visible = false;
            gvproject1.Visible = true;
            gvproject1.DataSource = dt;
            gvproject1.DataBind();
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            // CountryNames.Add(dt.Rows[i][1].ToString());
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void txtclientcode_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
            li.ccode = Convert.ToInt64(txtclientcode.Text.ToString());
            DataTable dtdata = new DataTable();
            dtdata = fpclass.selectdatafromccode(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvproject1.Visible = true;
                gvproject1.DataSource = dtdata;
                gvproject1.DataBind();
                btnprint.Visible = true;
            }
            else
            {
                Session["dtpitemsproj1"] = CreateTemplate();
                DataTable dt = (DataTable)Session["dtpitemsproj1"];
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                lblempty.Visible = false;
                gvproject1.Visible = true;
                gvproject1.DataSource = dt;
                gvproject1.DataBind();
                btnprint.Visible = false;
            }
        }
        else
        {
            Session["dtpitemsproj1"] = CreateTemplate();
            DataTable dt = (DataTable)Session["dtpitemsproj1"];
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            lblempty.Visible = false;
            gvproject1.Visible = true;
            gvproject1.DataSource = dt;
            gvproject1.DataBind();
            btnprint.Visible = false;
        }

    }
    protected void gvproject1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Label lblid = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblid");
        li.id = Convert.ToInt64(lblid.Text);
        fpclass.deleteproject1data(li);
        fillgrid();
    }
    protected void gvproject1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("AddNew"))
        {
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                TextBox txtahuno = (TextBox)gvproject1.FooterRow.FindControl("txtahunof");
                TextBox txtreturnaircfm = (TextBox)gvproject1.FooterRow.FindControl("txtreturnaircfmf");
                TextBox txtreturnairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairtempf");
                TextBox txtreturnairgains = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairgainsf");
                TextBox txtfreshaircfm = (TextBox)gvproject1.FooterRow.FindControl("txtfreshaircfmf");
                TextBox txtfreshairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairtempf");
                TextBox txtfreshairgrains = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairgrainsf");
                TextBox txtmixingaircfm = (TextBox)gvproject1.FooterRow.FindControl("txtmixingaircfmf");
                TextBox txtmixingairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtmixingairtempf");
                TextBox txtmixingairgrains = (TextBox)gvproject1.FooterRow.FindControl("txtmixingairgrainsf");
                TextBox txttotalfancfm = (TextBox)gvproject1.FooterRow.FindControl("txttotalfancfmf");
                TextBox txtbeforefanairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtbeforefanairtempf");
                TextBox txtbeforefanairgrains = (TextBox)gvproject1.FooterRow.FindControl("txtbeforefanairgrainsf");
                TextBox txtfanstaticpressures = (TextBox)gvproject1.FooterRow.FindControl("txtfanstaticpressuresf");
                TextBox txttotalfankw = (TextBox)gvproject1.FooterRow.FindControl("txttotalfankwf");
                TextBox txttotalfanbtuhr = (TextBox)gvproject1.FooterRow.FindControl("txttotalfanbtuhrf");
                TextBox txttempdiffdegreef = (TextBox)gvproject1.FooterRow.FindControl("txttempdiffdegreeff");
                TextBox txtafterfanairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtafterfanairtempf");
                TextBox txtafterfanairgrains = (TextBox)gvproject1.FooterRow.FindControl("txtafterfanairgrainsf");
                TextBox txtselecteddehcfm = (TextBox)gvproject1.FooterRow.FindControl("txtselecteddehcfmf");
                TextBox txtactualdehaircfm = (TextBox)gvproject1.FooterRow.FindControl("txtactualdehaircfmf");
                TextBox txtdehairtempafterchw = (TextBox)gvproject1.FooterRow.FindControl("txtdehairtempafterchwf");
                TextBox txtdehairgrainsafterchw = (TextBox)gvproject1.FooterRow.FindControl("txtdehairgrainsafterchwf");
                TextBox txtbypassaircfm = (TextBox)gvproject1.FooterRow.FindControl("txtbypassaircfmf");
                TextBox txtbypassairtemp = (TextBox)gvproject1.FooterRow.FindControl("txtbypassairtempf");
                TextBox txtbypassairgrains = (TextBox)gvproject1.FooterRow.FindControl("txtbypassairgrainsf");
                TextBox txtexhaustcfm = (TextBox)gvproject1.FooterRow.FindControl("txtexhaustcfmf");
                TextBox txtsupplycfm = (TextBox)gvproject1.FooterRow.FindControl("txtsupplycfmf");
                TextBox txtsensibleloadfromheatload = (TextBox)gvproject1.FooterRow.FindControl("txtsensibleloadfromheatloadf");
                TextBox txtdiversityofsensibleload = (TextBox)gvproject1.FooterRow.FindControl("txtdiversityofsensibleloadf");
                TextBox txtdiversifiedsensibleload = (TextBox)gvproject1.FooterRow.FindControl("txtdiversifiedsensibleloadf");
                TextBox txtsupplydb = (TextBox)gvproject1.FooterRow.FindControl("txtsupplydbf");
                TextBox txtlaterloadbtuhr = (TextBox)gvproject1.FooterRow.FindControl("txtlaterloadbtuhrf");
                TextBox txtsupplygrains = (TextBox)gvproject1.FooterRow.FindControl("txtsupplygrainsf");
                TextBox txthotwateroutletconditiondbf = (TextBox)gvproject1.FooterRow.FindControl("txthotwateroutletconditiondbf");
                TextBox txthotwateroutletconditiongrainsf = (TextBox)gvproject1.FooterRow.FindControl("txthotwateroutletconditiongrainsf");
                TextBox txtsesibleloadforhotwatercoil = (TextBox)gvproject1.FooterRow.FindControl("txtsesibleloadforhotwatercoilf");
                TextBox txthotwatercoilcapacityofsensibleload = (TextBox)gvproject1.FooterRow.FindControl("txthotwatercoilcapacityofsensibleloadf");
                TextBox txtcheckpointforrh = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointforrhf");
                TextBox txtcheckpointfortemp = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointfortempf");
                TextBox txtcheckpointfortemp2 = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointfortemp2f");
                if (txthotwateroutletconditiondbf.Text == "NaN" || txthotwateroutletconditiongrainsf.Text == "NaN" || txtsesibleloadforhotwatercoil.Text == "NaN")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Check Value of Hot Water Outlet Condition DB or Hot Water Outlet Condition Grains or Sensible Load for Hot Water Coil.');", true);
                    return;
                }
                li.ahuno = txtahuno.Text;
                li.returnaircfm = Convert.ToDouble(txtreturnaircfm.Text);
                li.returnairtemp = Convert.ToDouble(txtreturnairtemp.Text);
                li.returnairgrains = Convert.ToDouble(txtreturnairgains.Text);
                li.freshaircfm = Convert.ToDouble(txtfreshaircfm.Text);
                li.freshairtemp = Convert.ToDouble(txtfreshairtemp.Text);
                li.freshairgrains = Convert.ToDouble(txtfreshairgrains.Text);
                li.mixinaircfm = Convert.ToDouble(txtmixingaircfm.Text);
                li.mixingairtemp = Convert.ToDouble(txtmixingairtemp.Text);
                li.mixingairgrains = Convert.ToDouble(txtmixingairgrains.Text);
                li.totalfancfm = Convert.ToDouble(txttotalfancfm.Text);
                li.beforefanairtemp = Convert.ToDouble(txtbeforefanairtemp.Text);
                li.beforefanairgrains = Convert.ToDouble(txtbeforefanairgrains.Text);
                li.fanstaticpressure = Convert.ToDouble(txtfanstaticpressures.Text);
                li.totalfankw = Convert.ToDouble(txttotalfankw.Text);
                li.totalfanbtuhr = Convert.ToDouble(txttotalfanbtuhr.Text);
                li.tempdiffdegreef = Convert.ToDouble(txttempdiffdegreef.Text);
                li.afterfabairtemp = Convert.ToDouble(txtafterfanairtemp.Text);
                li.afterfanairgrains = Convert.ToDouble(txtafterfanairgrains.Text);
                li.selecteddehcfm = Convert.ToDouble(txtselecteddehcfm.Text);
                li.actualdehaircfm = Convert.ToDouble(txtactualdehaircfm.Text);
                li.dehairtempafterchw = Convert.ToDouble(txtdehairtempafterchw.Text);
                li.dehairgrainsafterchw = Convert.ToDouble(txtdehairgrainsafterchw.Text);
                li.bypassaircfm = Convert.ToDouble(txtbypassaircfm.Text);
                li.bypassairtemp = Convert.ToDouble(txtbypassairtemp.Text);
                li.bypassairgrains = Convert.ToDouble(txtbypassairgrains.Text);
                li.exhaustcfm = Convert.ToDouble(txtexhaustcfm.Text);
                li.supplycfm = Convert.ToDouble(txtsupplycfm.Text);
                li.sensibleloadfromheatload = Convert.ToDouble(txtsensibleloadfromheatload.Text);
                li.diversityofsensibleloadminload = Convert.ToDouble(txtdiversityofsensibleload.Text);
                li.diversityofsensibleloaddiversity = Convert.ToDouble(txtdiversifiedsensibleload.Text);
                li.suppludb = Convert.ToDouble(txtsupplydb.Text);
                li.latentload = Convert.ToDouble(txtlaterloadbtuhr.Text);
                li.supplygrains = Convert.ToDouble(txtsupplygrains.Text);
                li.hotwateroutletconditiondb = Convert.ToDouble(txthotwateroutletconditiondbf.Text);
                li.hotwateroutletconditiongrains = Convert.ToDouble(txthotwateroutletconditiongrainsf.Text);
                li.sensibleloadforhotwatercoil = Convert.ToDouble(txtsesibleloadforhotwatercoil.Text);
                li.hotwatercoilcapacityofsensibleload = Convert.ToDouble(txthotwatercoilcapacityofsensibleload.Text);
                li.checkpointforrh = txtcheckpointforrh.Text;
                li.checkpointfortemp = txtcheckpointfortemp.Text;
                li.checkpointfortemp1 = txtcheckpointfortemp2.Text;
                li.ccode = Convert.ToInt64(txtclientcode.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                fpclass.insertproject1data(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Client Code.');", true);
                return;
            }
        }
    }
    protected void gvproject1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            Label lblid = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblid");
            TextBox lblahuno = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtahuno");
            TextBox txtreturnaircfm = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtreturnaircfm1");
            TextBox txtreturnairtemp = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtreturnairtemp1");
            TextBox txtreturnairgains = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtreturnairgains1");
            TextBox txtfreshaircfm = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtfreshaircfm1");
            TextBox txtfreshairtemp = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtfreshairtemp1");
            TextBox txtfreshairgrains = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtfreshairgrains1");
            Label txtmixingaircfm = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblmixingaircfm");
            Label txtmixingairtemp = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblmixingairtemp");
            Label txtmixingairgrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblmixingairgrains");
            Label txttotalfancfm = (Label)gvproject1.Rows[e.RowIndex].FindControl("lbltotalfancfm");
            Label txtbeforefanairtemp = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblbeforefanairtemp");
            Label txtbeforefanairgrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblbeforefanairgrains");
            TextBox txtfanstaticpressures = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtfanstaticpressures");
            Label txttotalfankw = (Label)gvproject1.Rows[e.RowIndex].FindControl("lbltotalfankw");
            Label txttotalfanbtuhr = (Label)gvproject1.Rows[e.RowIndex].FindControl("lbltotalfanbtuhr");
            Label txttempdiffdegreef = (Label)gvproject1.Rows[e.RowIndex].FindControl("lbltempdiffdegreef");
            Label txtafterfanairtemp = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblafterfanairtemp");
            Label txtafterfanairgrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblafterfanairgrains");
            TextBox txtselecteddehcfm = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtselecteddehcfm1");
            Label txtactualdehaircfm = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblactualdehaircfm");
            TextBox txtdehairtempafterchw = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtdehairtempafterchw1");
            TextBox txtdehairgrainsafterchw = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtdehairgrainsafterchw1");
            Label txtbypassaircfm = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblbypassaircfm");
            Label txtbypassairtemp = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblbypassairtemp");
            Label txtbypassairgrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblbypassairgrains");
            TextBox txtexhaustcfm = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtexhaustcfm1");
            Label txtsupplycfm = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblsupplycfm");
            TextBox txtsensibleloadfromheatload = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtsensibleloadfromheatload1");
            TextBox txtdiversityofsensibleload = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtdiversityofsensibleload1");
            Label txtdiversifiedsensibleload = (Label)gvproject1.Rows[e.RowIndex].FindControl("lbldiversifiedsensibleload");
            Label txtsupplydb = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblsupplydb");
            TextBox txtlaterloadbtuhr = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txtlaterloadbtuhr1");
            Label txtsupplygrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblsupplygrains");
            Label txthotwateroutletconditiondb = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblhotwateroutletconditiondb");
            Label txthotwateroutletconditiongrains = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblhotwateroutletconditiongrains");
            Label txtsesibleloadforhotwatercoil = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblsesibleloadforhotwatercoil");
            TextBox txthotwatercoilcapacityofsensibleload = (TextBox)gvproject1.Rows[e.RowIndex].FindControl("txthotwatercoilcapacityofsensibleload1");
            Label txtcheckpointforrh = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblcheckpointforrh");
            Label lblcheckpointfortemp = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblcheckpointfortemp");
            Label lblcheckpointfortemp2 = (Label)gvproject1.Rows[e.RowIndex].FindControl("lblcheckpointfortemp2");
            if (txthotwateroutletconditiondb.Text == "NaN" || txthotwateroutletconditiongrains.Text == "NaN" || txtsesibleloadforhotwatercoil.Text == "NaN")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Check Value of Hot Water Outlet Condition DB or Hot Water Outlet Condition Grains or Sensible Load for Hot Water Coil.');", true);
                return;
            }
            li.id = Convert.ToInt64(lblid.Text);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.ahuno = lblahuno.Text;
            li.returnaircfm = Convert.ToDouble(txtreturnaircfm.Text);
            li.returnairtemp = Convert.ToDouble(txtreturnairtemp.Text);
            li.returnairgrains = Convert.ToDouble(txtreturnairgains.Text);
            li.freshaircfm = Convert.ToDouble(txtfreshaircfm.Text);
            li.freshairtemp = Convert.ToDouble(txtfreshairtemp.Text);
            li.freshairgrains = Convert.ToDouble(txtfreshairgrains.Text);
            li.mixinaircfm = Convert.ToDouble(txtmixingaircfm.Text);
            li.mixingairtemp = Convert.ToDouble(txtmixingairtemp.Text);
            li.mixingairgrains = Convert.ToDouble(txtmixingairgrains.Text);
            li.totalfancfm = Convert.ToDouble(txttotalfancfm.Text);
            li.beforefanairtemp = Convert.ToDouble(txtbeforefanairtemp.Text);
            li.beforefanairgrains = Convert.ToDouble(txtbeforefanairgrains.Text);
            li.fanstaticpressure = Convert.ToDouble(txtfanstaticpressures.Text);
            li.totalfankw = Convert.ToDouble(txttotalfankw.Text);
            li.totalfanbtuhr = Convert.ToDouble(txttotalfanbtuhr.Text);
            li.tempdiffdegreef = Convert.ToDouble(txttempdiffdegreef.Text);
            li.afterfabairtemp = Convert.ToDouble(txtafterfanairtemp.Text);
            li.afterfanairgrains = Convert.ToDouble(txtafterfanairgrains.Text);
            li.selecteddehcfm = Convert.ToDouble(txtselecteddehcfm.Text);
            li.actualdehaircfm = Convert.ToDouble(txtactualdehaircfm.Text);
            li.dehairtempafterchw = Convert.ToDouble(txtdehairtempafterchw.Text);
            li.dehairgrainsafterchw = Convert.ToDouble(txtdehairgrainsafterchw.Text);
            li.bypassaircfm = Convert.ToDouble(txtbypassaircfm.Text);
            li.bypassairtemp = Convert.ToDouble(txtbypassairtemp.Text);
            li.bypassairgrains = Convert.ToDouble(txtbypassairgrains.Text);
            li.exhaustcfm = Convert.ToDouble(txtexhaustcfm.Text);
            li.supplycfm = Convert.ToDouble(txtsupplycfm.Text);
            li.sensibleloadfromheatload = Convert.ToDouble(txtsensibleloadfromheatload.Text);
            li.diversityofsensibleloadminload = Convert.ToDouble(txtdiversityofsensibleload.Text);
            li.diversityofsensibleloaddiversity = Convert.ToDouble(txtdiversifiedsensibleload.Text);
            li.suppludb = Convert.ToDouble(txtsupplydb.Text);
            li.latentload = Convert.ToDouble(txtlaterloadbtuhr.Text);
            li.supplygrains = Convert.ToDouble(txtsupplygrains.Text);
            li.hotwateroutletconditiondb = Convert.ToDouble(txthotwateroutletconditiondb.Text);
            li.hotwateroutletconditiongrains = Convert.ToDouble(txthotwateroutletconditiongrains.Text);
            li.sensibleloadforhotwatercoil = Convert.ToDouble(txtsesibleloadforhotwatercoil.Text);
            li.hotwatercoilcapacityofsensibleload = Convert.ToDouble(txthotwatercoilcapacityofsensibleload.Text);
            li.checkpointforrh = txtcheckpointforrh.Text;
            li.checkpointfortemp = lblcheckpointfortemp.Text;
            li.checkpointfortemp1 = lblcheckpointfortemp2.Text;
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fpclass.updateproject1data(li);
            gvproject1.EditIndex = -1;
            fillgrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Client Code.');", true);
            return;
        }
    }
    protected void gvproject1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvproject1.EditIndex = -1;   //after cancel button want go to one index back that's y -1
        fillgrid();
    }
    protected void gvproject1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvproject1.EditIndex = e.NewEditIndex; //this open new index that is edit mode
        fillgrid();
    }
    public void footercalc()
    {
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        TextBox txt1 = (TextBox)gvproject1.FooterRow.FindControl("txtahunof");
        TextBox txt2 = (TextBox)gvproject1.FooterRow.FindControl("txtreturnaircfmf");
        TextBox txt3 = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairtempf");
        TextBox txt4 = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairgainsf");
        TextBox txt5 = (TextBox)gvproject1.FooterRow.FindControl("txtfreshaircfmf");
        TextBox txt6 = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairtempf");
        TextBox txt7 = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairgrainsf");
        TextBox txt8 = (TextBox)gvproject1.FooterRow.FindControl("txtmixingaircfmf");
        TextBox txt9 = (TextBox)gvproject1.FooterRow.FindControl("txtmixingairtempf");
        TextBox txt10 = (TextBox)gvproject1.FooterRow.FindControl("txtmixingairgrainsf");
        TextBox txt11 = (TextBox)gvproject1.FooterRow.FindControl("txttotalfancfmf");
        TextBox txt12 = (TextBox)gvproject1.FooterRow.FindControl("txtbeforefanairtempf");
        TextBox txt13 = (TextBox)gvproject1.FooterRow.FindControl("txtbeforefanairgrainsf");
        TextBox txt14 = (TextBox)gvproject1.FooterRow.FindControl("txtfanstaticpressuresf");
        TextBox txt15 = (TextBox)gvproject1.FooterRow.FindControl("txttotalfankwf");
        TextBox txt16 = (TextBox)gvproject1.FooterRow.FindControl("txttotalfanbtuhrf");
        TextBox txt17 = (TextBox)gvproject1.FooterRow.FindControl("txttempdiffdegreeff");
        TextBox txt18 = (TextBox)gvproject1.FooterRow.FindControl("txtafterfanairtempf");
        TextBox txt19 = (TextBox)gvproject1.FooterRow.FindControl("txtafterfanairgrainsf");
        TextBox txt20 = (TextBox)gvproject1.FooterRow.FindControl("txtselecteddehcfmf");
        TextBox txt21 = (TextBox)gvproject1.FooterRow.FindControl("txtactualdehaircfmf");
        TextBox txt22 = (TextBox)gvproject1.FooterRow.FindControl("txtdehairtempafterchwf");
        TextBox txt23 = (TextBox)gvproject1.FooterRow.FindControl("txtdehairgrainsafterchwf");
        TextBox txt24 = (TextBox)gvproject1.FooterRow.FindControl("txtbypassaircfmf");
        TextBox txt25 = (TextBox)gvproject1.FooterRow.FindControl("txtbypassairtempf");
        TextBox txt26 = (TextBox)gvproject1.FooterRow.FindControl("txtbypassairgrainsf");
        TextBox txt27 = (TextBox)gvproject1.FooterRow.FindControl("txtexhaustcfmf");
        TextBox txt28 = (TextBox)gvproject1.FooterRow.FindControl("txtsupplycfmf");
        TextBox txt29 = (TextBox)gvproject1.FooterRow.FindControl("txtsensibleloadfromheatloadf");
        TextBox txt30 = (TextBox)gvproject1.FooterRow.FindControl("txtdiversityofsensibleloadf");
        TextBox txt31 = (TextBox)gvproject1.FooterRow.FindControl("txtdiversifiedsensibleloadf");
        TextBox txt32 = (TextBox)gvproject1.FooterRow.FindControl("txtsupplydbf");
        TextBox txt33 = (TextBox)gvproject1.FooterRow.FindControl("txtlaterloadbtuhrf");
        TextBox txt34 = (TextBox)gvproject1.FooterRow.FindControl("txtsupplygrainsf");
        TextBox txt35 = (TextBox)gvproject1.FooterRow.FindControl("txthotwateroutletconditiondbf");

        TextBox txt36 = (TextBox)gvproject1.FooterRow.FindControl("txthotwateroutletconditiongrainsf");
        TextBox txt37 = (TextBox)gvproject1.FooterRow.FindControl("txtsesibleloadforhotwatercoilf");
        TextBox txt38 = (TextBox)gvproject1.FooterRow.FindControl("txthotwatercoilcapacityofsensibleloadf");
        TextBox txt39 = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointforrhf");
        TextBox txt40 = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointfortempf");
        TextBox txt41 = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointfortemp2f");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }

        //txt11.Text = Math.Round().ToString();

    }

    public void gvcalc(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        TextBox txt1 = (TextBox)currentRow.FindControl("txtahunof");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        Label txt14 = (Label)currentRow.FindControl("lblfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");
        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        //if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }

        //txt11.Text = Math.Round().ToString();

    }

    protected void txtreturnaircfmf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txt3 = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairtempf");
        Page.SetFocus(txt3);
    }
    protected void txtreturnairtempf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtreturnairgainsf = (TextBox)gvproject1.FooterRow.FindControl("txtreturnairgainsf");
        Page.SetFocus(txtreturnairgainsf);
    }
    protected void txtreturnairgainsf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtfreshaircfmf = (TextBox)gvproject1.FooterRow.FindControl("txtfreshaircfmf");
        Page.SetFocus(txtfreshaircfmf);
    }
    protected void txtfreshaircfmf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtfreshairtempf = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairtempf");
        Page.SetFocus(txtfreshairtempf);
    }
    protected void txtfreshairtempf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtfreshairgrainsf = (TextBox)gvproject1.FooterRow.FindControl("txtfreshairgrainsf");
        Page.SetFocus(txtfreshairgrainsf);
    }
    protected void txtfreshairgrainsf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtmixingaircfmf = (TextBox)gvproject1.FooterRow.FindControl("txtmixingaircfmf");
        Page.SetFocus(txtmixingaircfmf);
    }
    protected void txtselecteddehcfmf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtactualdehaircfmf = (TextBox)gvproject1.FooterRow.FindControl("txtactualdehaircfmf");
        Page.SetFocus(txtactualdehaircfmf);
    }
    protected void txtdehairtempafterchwf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtdehairgrainsafterchwf = (TextBox)gvproject1.FooterRow.FindControl("txtdehairgrainsafterchwf");
        Page.SetFocus(txtdehairgrainsafterchwf);
    }
    protected void txtdehairgrainsafterchwf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtbypassaircfmf = (TextBox)gvproject1.FooterRow.FindControl("txtbypassaircfmf");
        Page.SetFocus(txtbypassaircfmf);
    }
    protected void txtexhaustcfmf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtsupplycfmf = (TextBox)gvproject1.FooterRow.FindControl("txtsupplycfmf");
        Page.SetFocus(txtsupplycfmf);
    }
    protected void txtsensibleloadfromheatloadf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtdiversityofsensibleloadf = (TextBox)gvproject1.FooterRow.FindControl("txtdiversityofsensibleloadf");
        Page.SetFocus(txtdiversityofsensibleloadf);
    }
    protected void txtdiversityofsensibleloadf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtdiversifiedsensibleloadf = (TextBox)gvproject1.FooterRow.FindControl("txtdiversifiedsensibleloadf");
        Page.SetFocus(txtdiversifiedsensibleloadf);
    }
    protected void txtlaterloadbtuhrf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtsupplygrainsf = (TextBox)gvproject1.FooterRow.FindControl("txtsupplygrainsf");
        Page.SetFocus(txtsupplygrainsf);
    }
    protected void txthotwatercoilcapacityofsensibleloadf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txtcheckpointforrhf = (TextBox)gvproject1.FooterRow.FindControl("txtcheckpointforrhf");
        Page.SetFocus(txtcheckpointforrhf);
    }
    protected void txtfanstaticpressuresf_TextChanged(object sender, EventArgs e)
    {
        footercalc();
        TextBox txttotalfankwf = (TextBox)gvproject1.FooterRow.FindControl("txttotalfankwf");
        Page.SetFocus(txttotalfankwf);
    }


    
    protected void txtreturnaircfm1_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt3);
        //footercalc();
    }

    protected void txtfanstaticpressures_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt20);
        //footercalc();
    }

    protected void txtreturnairtemp1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt4);
    }
    protected void txtreturnairgains1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt5);
    }
    protected void txtfreshaircfm1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt6);
    }
    protected void txtfreshairtemp1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt7);
    }
    protected void txtfreshairgrains1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt14);
    }
    protected void txtselecteddehcfm1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt22);
    }
    protected void txtdehairtempafterchw1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt23);
    }
    protected void txtdehairgrainsafterchw1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt27);
    }
    protected void txtexhaustcfm1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt29);
    }
    protected void txtsensibleloadfromheatload1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt30);
    }
    protected void txtdiversityofsensibleload1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt33);
    }
    protected void txtlaterloadbtuhr1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
        Page.SetFocus(txt38);
    }
    protected void txthotwatercoilcapacityofsensibleload1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
    }
    protected void txtfanstaticpressures1_TextChanged(object sender, EventArgs e)
    {
        //footercalc();
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        double v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0, v17 = 0, v18 = 0, v19 = 0, v20 = 0, v21 = 0, v22 = 0, v23 = 0, v24 = 0, v25 = 0, v26 = 0, v27 = 0, v28 = 0, v29 = 0, v30 = 0, v31 = 0, v32 = 0, v33 = 0, v34 = 0, v35 = 0, v36 = 0, v37 = 0, v38 = 0, v39 = 0;
        Label txt1 = (Label)currentRow.FindControl("lblahuno");
        TextBox txt2 = (TextBox)currentRow.FindControl("txtreturnaircfm1");
        TextBox txt3 = (TextBox)currentRow.FindControl("txtreturnairtemp1");
        TextBox txt4 = (TextBox)currentRow.FindControl("txtreturnairgains1");
        TextBox txt5 = (TextBox)currentRow.FindControl("txtfreshaircfm1");
        TextBox txt6 = (TextBox)currentRow.FindControl("txtfreshairtemp1");
        TextBox txt7 = (TextBox)currentRow.FindControl("txtfreshairgrains1");
        Label txt8 = (Label)currentRow.FindControl("lblmixingaircfm");
        Label txt9 = (Label)currentRow.FindControl("lblmixingairtemp");
        Label txt10 = (Label)currentRow.FindControl("lblmixingairgrains");
        Label txt11 = (Label)currentRow.FindControl("lbltotalfancfm");
        Label txt12 = (Label)currentRow.FindControl("lblbeforefanairtemp");
        Label txt13 = (Label)currentRow.FindControl("lblbeforefanairgrains");
        TextBox txt14 = (TextBox)currentRow.FindControl("txtfanstaticpressures");
        Label txt15 = (Label)currentRow.FindControl("lbltotalfankw");
        Label txt16 = (Label)currentRow.FindControl("lbltotalfanbtuhr");
        Label txt17 = (Label)currentRow.FindControl("lbltempdiffdegreef");
        Label txt18 = (Label)currentRow.FindControl("lblafterfanairtemp");
        Label txt19 = (Label)currentRow.FindControl("lblafterfanairgrains");
        TextBox txt20 = (TextBox)currentRow.FindControl("txtselecteddehcfm1");
        Label txt21 = (Label)currentRow.FindControl("lblactualdehaircfm");
        TextBox txt22 = (TextBox)currentRow.FindControl("txtdehairtempafterchw1");
        TextBox txt23 = (TextBox)currentRow.FindControl("txtdehairgrainsafterchw1");
        Label txt24 = (Label)currentRow.FindControl("lblbypassaircfm");
        Label txt25 = (Label)currentRow.FindControl("lblbypassairtemp");
        Label txt26 = (Label)currentRow.FindControl("lblbypassairgrains");
        TextBox txt27 = (TextBox)currentRow.FindControl("txtexhaustcfm1");
        Label txt28 = (Label)currentRow.FindControl("lblsupplycfm");
        TextBox txt29 = (TextBox)currentRow.FindControl("txtsensibleloadfromheatload1");
        TextBox txt30 = (TextBox)currentRow.FindControl("txtdiversityofsensibleload1");
        Label txt31 = (Label)currentRow.FindControl("lbldiversifiedsensibleload");
        Label txt32 = (Label)currentRow.FindControl("lblsupplydb");
        TextBox txt33 = (TextBox)currentRow.FindControl("txtlaterloadbtuhr1");
        Label txt34 = (Label)currentRow.FindControl("lblsupplygrains");
        Label txt35 = (Label)currentRow.FindControl("lblhotwateroutletconditiondb");

        Label txt36 = (Label)currentRow.FindControl("lblhotwateroutletconditiongrains");
        Label txt37 = (Label)currentRow.FindControl("lblsesibleloadforhotwatercoil");
        TextBox txt38 = (TextBox)currentRow.FindControl("txthotwatercoilcapacityofsensibleload1");
        Label txt39 = (Label)currentRow.FindControl("lblcheckpointforrh");
        Label txt40 = (Label)currentRow.FindControl("lblcheckpointfortemp");
        Label txt41 = (Label)currentRow.FindControl("lblcheckpointfortemp2");
        if (txt2.Text.Trim() != string.Empty) { v2 = Convert.ToDouble(txt2.Text); } else { txt2.Text = "0"; }
        if (txt3.Text.Trim() != string.Empty) { v3 = Convert.ToDouble(txt3.Text); } else { txt3.Text = "0"; }
        if (txt4.Text.Trim() != string.Empty) { v4 = Convert.ToDouble(txt4.Text); } else { txt4.Text = "0"; }
        if (txt5.Text.Trim() != string.Empty) { v5 = Convert.ToDouble(txt5.Text); } else { txt5.Text = "0"; }
        if (txt6.Text.Trim() != string.Empty) { v6 = Convert.ToDouble(txt6.Text); } else { txt6.Text = "0"; }
        if (txt7.Text.Trim() != string.Empty) { v7 = Convert.ToDouble(txt7.Text); } else { txt7.Text = "0"; }
        if (txt8.Text.Trim() != string.Empty) { v8 = Convert.ToDouble(txt8.Text); } else { txt8.Text = "0"; }
        if (txt9.Text.Trim() != string.Empty) { v9 = Convert.ToDouble(txt9.Text); } else { txt9.Text = "0"; }
        if (txt10.Text.Trim() != string.Empty) { v10 = Convert.ToDouble(txt10.Text); } else { txt10.Text = "0"; }
        if (txt11.Text.Trim() != string.Empty) { v11 = Convert.ToDouble(txt11.Text); } else { txt11.Text = "0"; }
        if (txt12.Text.Trim() != string.Empty) { v12 = Convert.ToDouble(txt12.Text); } else { txt12.Text = "0"; }
        if (txt13.Text.Trim() != string.Empty) { v13 = Convert.ToDouble(txt13.Text); } else { txt13.Text = "0"; }
        if (txt14.Text.Trim() != string.Empty) { v14 = Convert.ToDouble(txt14.Text); } else { txt14.Text = "0"; }
        if (txt15.Text.Trim() != string.Empty) { v15 = Convert.ToDouble(txt15.Text); } else { txt15.Text = "0"; }
        if (txt16.Text.Trim() != string.Empty) { v16 = Convert.ToDouble(txt16.Text); } else { txt16.Text = "0"; }
        if (txt17.Text.Trim() != string.Empty) { v17 = Convert.ToDouble(txt17.Text); } else { txt17.Text = "0"; }
        if (txt18.Text.Trim() != string.Empty) { v18 = Convert.ToDouble(txt18.Text); } else { txt18.Text = "0"; }
        if (txt19.Text.Trim() != string.Empty) { v19 = Convert.ToDouble(txt19.Text); } else { txt19.Text = "0"; }
        if (txt20.Text.Trim() != string.Empty) { v20 = Convert.ToDouble(txt20.Text); } else { txt20.Text = "0"; }
        if (txt21.Text.Trim() != string.Empty) { v21 = Convert.ToDouble(txt21.Text); } else { txt21.Text = "0"; }
        if (txt22.Text.Trim() != string.Empty) { v22 = Convert.ToDouble(txt22.Text); } else { txt22.Text = "0"; }
        if (txt23.Text.Trim() != string.Empty) { v23 = Convert.ToDouble(txt23.Text); } else { txt23.Text = "0"; }
        if (txt24.Text.Trim() != string.Empty) { v24 = Convert.ToDouble(txt24.Text); } else { txt24.Text = "0"; }
        if (txt25.Text.Trim() != string.Empty) { v25 = Convert.ToDouble(txt25.Text); } else { txt25.Text = "0"; }
        if (txt26.Text.Trim() != string.Empty) { v26 = Convert.ToDouble(txt26.Text); } else { txt26.Text = "0"; }
        if (txt27.Text.Trim() != string.Empty) { v27 = Convert.ToDouble(txt27.Text); } else { txt27.Text = "0"; }
        if (txt28.Text.Trim() != string.Empty) { v28 = Convert.ToDouble(txt28.Text); } else { txt28.Text = "0"; }
        if (txt29.Text.Trim() != string.Empty) { v29 = Convert.ToDouble(txt29.Text); } else { txt29.Text = "0"; }
        if (txt30.Text.Trim() != string.Empty) { v30 = Convert.ToDouble(txt30.Text); } else { txt30.Text = "0"; }
        if (txt31.Text.Trim() != string.Empty) { v31 = Convert.ToDouble(txt31.Text); } else { txt31.Text = "0"; }
        if (txt32.Text.Trim() != string.Empty) { v32 = Convert.ToDouble(txt32.Text); } else { txt32.Text = "0"; }
        if (txt33.Text.Trim() != string.Empty) { v33 = Convert.ToDouble(txt33.Text); } else { txt33.Text = "0"; }
        if (txt34.Text.Trim() != string.Empty) { v34 = Convert.ToDouble(txt34.Text); } else { txt34.Text = "0"; }
        if (txt35.Text.Trim() != string.Empty) { v35 = Convert.ToDouble(txt35.Text); } else { txt35.Text = "0"; }
        if (txt36.Text.Trim() != string.Empty) { v36 = Convert.ToDouble(txt36.Text); } else { txt36.Text = "0"; }
        if (txt37.Text.Trim() != string.Empty) { v37 = Convert.ToDouble(txt37.Text); } else { txt37.Text = "0"; }
        if (txt38.Text.Trim() != string.Empty) { v38 = Convert.ToDouble(txt38.Text); } else { txt38.Text = "0"; }
        // if (txt39.Text.Trim() != string.Empty) { v39 = Convert.ToDouble(txt39.Text); } else { txt39.Text = "0"; }
        txt8.Text = Math.Round((v2 + v5), 2).ToString();
        txt9.Text = Math.Round((((v2 * v3) + (v5 * v6)) / (v2 + v5)), 2).ToString();
        txt10.Text = Math.Round((((v2 * v4) + (v5 * v7)) / (v2 + v5)), 2).ToString();
        txt11.Text = txt8.Text;
        txt12.Text = txt9.Text;
        txt13.Text = txt10.Text;
        txt15.Text = Math.Round(((((0.015673 * v11 * v14 / 25.4) / 85) * 1.25) * 0.746), 2).ToString();
        txt16.Text = Math.Round((Convert.ToDouble(txt15.Text) * 3400), 2).ToString();
        txt17.Text = Math.Round((Convert.ToDouble(txt16.Text) / (1.08 * (Convert.ToDouble(txt11.Text)))), 2).ToString();
        txt18.Text = Math.Round(((Convert.ToDouble(txt12.Text)) + (Convert.ToDouble(txt17.Text))), 2).ToString();
        txt19.Text = txt13.Text;
        txt21.Text = Math.Round((Convert.ToDouble(txt20.Text) * 0.95), 2).ToString();
        txt24.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt21.Text)), 2).ToString();
        txt25.Text = txt18.Text;
        txt26.Text = txt19.Text;
        txt28.Text = Math.Round((Convert.ToDouble(txt8.Text) - Convert.ToDouble(txt27.Text)), 2).ToString();
        txt31.Text = Math.Round(((Convert.ToDouble(txt29.Text) * Convert.ToDouble(txt30.Text)) / 100), 2).ToString();
        txt32.Text = Math.Round((Convert.ToDouble(txt3.Text) - ((Convert.ToDouble(txt31.Text) / (1.08 * (Convert.ToDouble(txt28.Text)))))), 2).ToString();
        txt34.Text = Math.Round((Convert.ToDouble(txt4.Text) - (Convert.ToDouble(txt33.Text) / (0.68 * (Convert.ToDouble(txt28.Text))))), 2).ToString();
        txt35.Text = Math.Round(((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt32.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt25.Text))) / Convert.ToDouble(txt21.Text))), 2).ToString();
        txt36.Text = Math.Round((((Convert.ToDouble(txt28.Text) * Convert.ToDouble(txt34.Text)) - (Convert.ToDouble(txt24.Text) * Convert.ToDouble(txt26.Text))) / Convert.ToDouble(txt21.Text)), 2).ToString();
        txt37.Text = Math.Round((1.08 * Convert.ToDouble(txt20.Text) * (Convert.ToDouble(txt35.Text) - Convert.ToDouble(txt22.Text))), 2).ToString();
        if (Convert.ToDouble(txt23.Text) < Convert.ToDouble(txt36.Text))
        {
            txt39.Text = "TRUE";
        }
        else
        {
            txt39.Text = "FALSE";
        }
        if (Convert.ToDouble(txt35.Text) > Convert.ToDouble(txt22.Text))
        {
            txt40.Text = "TRUE";
        }
        else
        {
            txt40.Text = "FALSE";
        }
        if (Convert.ToDouble(txt38.Text) > Convert.ToDouble(txt37.Text))
        {
            txt41.Text = "TRUE";
        }
        else
        {
            txt41.Text = "FALSE";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("ahuno", typeof(Int64));
        dtpitems.Columns.Add("returnaircfm", typeof(string));
        dtpitems.Columns.Add("returnairtemp", typeof(double));
        dtpitems.Columns.Add("returnairgrains", typeof(double));
        dtpitems.Columns.Add("freshaircfm", typeof(double));
        dtpitems.Columns.Add("freshairtemp", typeof(string));
        dtpitems.Columns.Add("freshairgrains", typeof(double));
        dtpitems.Columns.Add("mixinaircfm", typeof(double));
        dtpitems.Columns.Add("mixingairtemp", typeof(string));
        dtpitems.Columns.Add("mixingairgrains", typeof(double));
        dtpitems.Columns.Add("totalfancfm", typeof(double));
        dtpitems.Columns.Add("beforefanairtemp", typeof(double));
        dtpitems.Columns.Add("beforefanairgrains", typeof(double));
        dtpitems.Columns.Add("fanstaticpressure", typeof(double));
        dtpitems.Columns.Add("totalfankw", typeof(double));
        dtpitems.Columns.Add("totalfanbtuhr", typeof(double));
        dtpitems.Columns.Add("tempdiffdegreef", typeof(string));
        dtpitems.Columns.Add("afterfabairtemp", typeof(string));
        dtpitems.Columns.Add("afterfanairgrains", typeof(string));
        dtpitems.Columns.Add("selecteddehcfm", typeof(Int64));
        dtpitems.Columns.Add("actualdehaircfm", typeof(double));
        dtpitems.Columns.Add("dehairtempafterchw", typeof(double));
        dtpitems.Columns.Add("dehairgrainsafterchw", typeof(string));
        dtpitems.Columns.Add("bypassaircfm", typeof(double));
        dtpitems.Columns.Add("bypassairtemp", typeof(double));
        dtpitems.Columns.Add("bypassairgrains", typeof(double));
        dtpitems.Columns.Add("exhaustcfm", typeof(double));
        dtpitems.Columns.Add("supplycfm", typeof(double));
        dtpitems.Columns.Add("sensibleloadfromheatload", typeof(double));
        dtpitems.Columns.Add("diversityofsensibleloadminload", typeof(double));
        dtpitems.Columns.Add("diversityofsensibleloaddiversity", typeof(string));
        dtpitems.Columns.Add("suppludb", typeof(string));
        dtpitems.Columns.Add("latentload", typeof(string));
        dtpitems.Columns.Add("supplygrains", typeof(Int64));
        dtpitems.Columns.Add("hotwateroutletconditiondb", typeof(double));
        dtpitems.Columns.Add("hotwateroutletconditiongrains", typeof(double));
        dtpitems.Columns.Add("sensibleloadforhotwatercoil", typeof(string));
        dtpitems.Columns.Add("hotwatercoilcapacityofsensibleload", typeof(double));
        //dtpitems.Columns.Add("hotwatercoilcapacityofsensibleload", typeof(double));
        dtpitems.Columns.Add("checkpointforrh", typeof(double));
        dtpitems.Columns.Add("checkpointfortemp", typeof(double));
        dtpitems.Columns.Add("checkpointfortemp1", typeof(double));
        return dtpitems;
    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string Xrepname = "AHU_B Report";
            Int64 vno = Convert.ToInt64(txtclientcode.Text);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
}