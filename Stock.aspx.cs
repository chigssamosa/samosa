﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Data.SqlTypes;

public partial class Stock : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillstockgrid();
        }
    }

    public void fillstockgrid()
    {
        li.fromdate = "01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0];
        li.todate = "31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DataTable dtallstock = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select distinct(itemname) from itemmaster order by itemname", con);
        DataTable dtitem = new DataTable();
        da.Fill(dtitem);
        for (int c = 0; c < dtitem.Rows.Count; c++)
        {
            li.itemname = dtitem.Rows[c]["itemname"].ToString();
            SqlDataAdapter dac = new SqlDataAdapter("select itemname,unit,(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname) as opqty,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname and challandate between @fromdate and @todate))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname and challandate between @fromdate and @todate))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname and pcdate between @fromdate and @todate)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname and scdate between @fromdate and @todate))+(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname and challandate between @fromdate and @todate)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname and challandate between @fromdate and @todate)) as totalqty from itemmaster where itemname=@itemname", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            dac.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dac.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            if (Convert.ToDouble(dtstock.Rows[0]["opqty"].ToString()) > 0 || Convert.ToDouble(dtstock.Rows[0]["tots"].ToString()) > 0 || Convert.ToDouble(dtstock.Rows[0]["totp"].ToString()) > 0 || Convert.ToDouble(dtstock.Rows[0]["totalqty"].ToString()) > 0)
            {
                dtallstock.Merge(dtstock);
            }
        }
        if (dtallstock.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvbankpaymentlist.Visible = true;
            gvbankpaymentlist.DataSource = dtallstock;
            gvbankpaymentlist.DataBind();
        }
        else
        {
            gvbankpaymentlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Stock Data Found.";
        }

    }

    protected void btnstockprint_Click(object sender, EventArgs e)
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        gvbankpaymentlist.RenderControl(hw);
        string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type = 'text/javascript'>");
        sb.Append("window.onload = new function(){");
        sb.Append("var printWin = window.open('', '', 'left=0");
        sb.Append(",top=0,width=1000,height=600,status=0');");
        sb.Append("printWin.document.write(\"");
        sb.Append(gridHTML);
        sb.Append("\");");
        sb.Append("printWin.document.close();");
        sb.Append("printWin.focus();");
        sb.Append("printWin.print();");
        sb.Append("printWin.close();};");
        sb.Append("</script>");
        ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnstockform201c_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }

        string Xrepname = "Form 201C";
        //li.bank1 = txtacname.Text;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //Int64 vno = Convert.ToInt64(e.CommandArgument);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);

    }
}